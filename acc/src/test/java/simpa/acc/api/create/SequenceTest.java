/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api.create;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.junit.jupiter.api.Test;

import simpa.core.api.FileNamingConventions;
import simpa.core.api.Profile;
import simpa.core.api.SystemConstants;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.utils.FileUtils;


public class SequenceTest {

	private File dir= new File("/tmp/SequenceTest");
	
	public SequenceTest() {
		
		if (!dir.exists()){
		    dir.mkdirs();
		}
		
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH +"QUAD-SOLUTION.txt", dir+"/QUAD-SOLUTION.txt");
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH +"ELBHZ-SOLUTION.txt", dir+"/ELBHZ-SOLUTION.txt");
	}
	
	@Test
	public void testCircular() throws OutOfApertureException, FileNotFoundException {
		double deflection = Math.PI / 3;
		double tilt = 0;
		Map<Double, Profile> vc = getElenaBendingChamber();
		Sequence seq = new Sequence("test",true,null,null);

		for (int i = 0; i < 6; i++) {
			seq.addStraightElementAt(0.3 + 3.0 * i, "QUAD" + i, 1.0,
					"QUAD" + FileNamingConventions.getSolutionFileExtension(), tilt, "QUADS", Rotation.IDENTITY, null);

			seq.addBendingElementAt(1.5 + 3.0 * i, "ELBHZ" + i, 1.0,
					"ELBHZ" + FileNamingConventions.getSolutionFileExtension(), 0.9707, deflection, tilt, "BENDINGS",
					Rotation.IDENTITY, vc,true);

		}
		seq.getReferenceOrbit().writeToFile(dir+"/orbit.txt");	
		seq.sourcePositionsToFile(dir+"/allpos.txt");
		System.out.println("orbit length: " +  seq.getReferenceOrbit().getOrbitLength());
		createBeamRegion(dir+"/beamregion.stl", seq);
	}

	@Test
	public void testLine() throws OutOfApertureException, FileNotFoundException {
		testLine(true);
		testLine(false);
	}

	private void testLine(boolean clockwise) throws OutOfApertureException, FileNotFoundException {
		double deflection = Math.PI / 3;
		Rotation rot = Rotation.IDENTITY;
		Map<Double, Profile> vc = getElenaBendingChamber();
		Map<Double, Profile> qc=getElenaQuadChamber();
		if (!clockwise) {
			deflection = -deflection;
		}

		Sequence seq = new Sequence("test",false,null,null);
		String elbhzName;
		if (clockwise)
			elbhzName = "ELBHZ_CW"; 
		else
			elbhzName = "ELBHZ_ACW";
		
		for (int i = 0; i < 6; i++) {
			double tilt = i * Math.PI / 8;
			seq.addStraightElementAt(0.1 + 3.0 * i, "QUAD" + i, 1.0,
					"QUAD" + FileNamingConventions.getSolutionFileExtension(), tilt, "QUADS", Rotation.IDENTITY, qc);


			seq.addBendingElementAt(1.5 + 3.0 * i, elbhzName + i, 1.0,
					elbhzName + FileNamingConventions.getSolutionFileExtension(), 0.9707, deflection, tilt, "BENDINGS",
					rot, vc,true);

		}
		seq.addMarkerElement(seq.getCurrentLength()+ 1.0, "end",AccElement.ElementType.MARKER,null);
		String filename;
		if (clockwise) {
			seq.getReferenceOrbit().writeToFile(dir+"/lineOrbit.txt");
			seq.sourcePositionsToFile(dir+"/lineAllPos.txt");
			filename = dir+"/tline_cw.stl";
			// FileUtils.writeTextFile("localframes.txt",seq.localFrames.toString());
		} else {
			filename = dir+"/tline_acw.stl";
			seq.getReferenceOrbit().writeToFile(dir+"/lineOrbit_acw.txt");
			seq.sourcePositionsToFile(dir+"/lineAllPos_acw.txt");
			// FileUtils.writeTextFile("localframes_acw.txt",seq.localFrames.toString());
		}

		System.out.println("orbit length: " + seq.getReferenceOrbit().getOrbitLength());
		createBeamRegion(filename, seq);
	}

	private void createBeamRegion(String filename, Sequence seq) {

		final double distBetweenProfiles = 0.2;
		Profile depaultProfile = new Profile(0.03, 0.03, 16, 0);
		BeamRegion brf = new BeamRegion(seq, distBetweenProfiles, depaultProfile, 0.05);
		brf.writeSTLTextFile(filename);
	}

	private Map<Double, Profile> getElenaBendingChamber() throws FileNotFoundException {

		double height = 0.05;
		double width = 0.0935;
		Profile profile = new Profile(-width / 2, width / 2, -height / 2, height / 2, 4, 4);
		Profile endProfile = new Profile(0.03, 0.03, 16, 0);

		Map<Double, Profile> map = new HashMap<>();
		map.put(-2.35 / 2.0, endProfile);
		for (int i = -3; i <= 3; i++)
			map.put(i * 0.45 / 2, profile);
		map.put(2.35 / 2, endProfile);

		return map;
	}
	private Map<Double, Profile> getElenaQuadChamber() throws FileNotFoundException {

		
		Profile pr = new Profile(0.04, 0.04, 16, 0);
		Profile endProfile = new Profile(0.03, 0.03, 16, 0);
		Map<Double, Profile> map = new HashMap<>();
		map.put(-0.15 , endProfile);	
		map.put(-0.1 , pr);	
		map.put(0.1 , pr);
		map.put(-0.15 , endProfile);	
		return map;
	}
}
