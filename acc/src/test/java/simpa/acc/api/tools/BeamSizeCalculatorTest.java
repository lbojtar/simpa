package simpa.acc.api.tools;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.utils.BeamSizeCalculator;
import simpa.core.api.utils.LocalFramesCalculator;

public class BeamSizeCalculatorTest {
    
    @Test
    public void test(){  
        String dir="/home/lbojtar/git/simpa/examples/elena_lines/asacusa1/wdir/";  
        ReferenceOrbit designOrbit = new ReferenceOrbit(dir+"asacusa1-ref-orbit.txt",false,LocalFramesCalculator.Strategy.MINIMIZE_TORSION,Vector3D.PLUS_J);    
        BeamSizeCalculator bsc = new BeamSizeCalculator(dir+"ASA1Beam-BEAM-TRAJECTORY.txt", designOrbit, 0.1);
        bsc.writeSigmas(dir+"sigmas.txt");

    }
}
