package simpa.acc.api.tools;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import simpa.acc.api.Tune;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.acc.impl.TuneTwissInterpolator;
import simpa.core.api.utils.FileUtils;

public class TuneTwissInterpolatorTest {

	@Test
	public void test() {
		Tune t0 = new Tune(0, 0);
		Tune t1 = new Tune(1, 0);
		Tune t2 = new Tune(0, 1);
		Tune t3 = new Tune(0.5, 0.5);

		PhaseSpaceEllipse he = new PhaseSpaceEllipse(0, 1, 2, 3, 4);
		PhaseSpaceEllipse ve = new PhaseSpaceEllipse(0, 5, 6, 7, 8);
		TwissParameters tw0 = TwissParameters.builder().hEllipse(he).vEllipse(ve).muH(0).muV(0).dispH(0).dispV(0).dispPrimeH(0)
				.dispPrimeV(0).build();

		he = new PhaseSpaceEllipse(0, 1.1, 2.1, 3.1, 4.1);
		ve = new PhaseSpaceEllipse(0, 5.1, 6.1, 7.1, 8.1);
		TwissParameters tw1 = TwissParameters.builder().hEllipse(he).vEllipse(ve).muH(0).muV(0).dispH(0).dispV(0).dispPrimeH(0)
				.dispPrimeV(0).build();

		
		he = new PhaseSpaceEllipse(0, 1.2, 2.2, 3.2, 4.2);
		ve = new PhaseSpaceEllipse(0, 5.2, 6.2, 7.2, 8.2);
		TwissParameters tw2 = TwissParameters.builder().hEllipse(he).vEllipse(ve).muH(0).muV(0).dispH(0).dispV(0).dispPrimeH(0)
				.dispPrimeV(0).build();

		he = new PhaseSpaceEllipse(0, 1.3, 2.3, 3.3, 4.3);
		ve = new PhaseSpaceEllipse(0, 5.3, 6.3, 7.3, 8.3);
		TwissParameters tw3 = TwissParameters.builder().hEllipse(he).vEllipse(ve).muH(0).muV(0).dispH(0).dispV(0).dispPrimeH(0)
				.dispPrimeV(0).build();

		Map<Tune, TwissParameters> map = new HashMap<>();
		map.put(t0, tw0);
		map.put(t1, tw1);
		map.put(t2, tw2);
		map.put(t3, tw3);

		TuneTwissInterpolator m = new TuneTwissInterpolator(map);

		// See with gnuplot if it make sense
		// for example: splot "tuneTwissModelTest.txt" u 1:2:4 w d,
		// "tuneTwissModelTestRef.txt" u 1:2:4
		StringBuilder sb = new StringBuilder();
		sb.append("qh qv " + TwissParameters.getHeaderString(false));
		sb.append(t0.qh() + " " + t0.qv() + " " + tw0.toString());
		sb.append(t1.qh() + " " + t1.qv() + " " + tw1.toString());
		sb.append(t2.qh() + " " + t2.qv() + " " + tw2.toString());
		sb.append(t3.qh() + " " + t3.qv() + " " + tw3.toString());
		FileUtils.writeTextFile("/tmp/tuneTwissModelTestRef.txt", sb.toString());

		sb = new StringBuilder();
		sb.append("qh qv " + TwissParameters.getHeaderString(false));

		for (int i = 0; i < 1000; i++) {
			Tune t = new Tune(Math.random(), Math.random());
			TwissParameters tw = m.getTwissParameters(t);
			sb.append(t.qh() + " " + t.qv() + " " + tw.toString());
		}
		FileUtils.writeTextFile("/tmp/tuneTwissModelTest.txt", sb.toString());
	}

}
