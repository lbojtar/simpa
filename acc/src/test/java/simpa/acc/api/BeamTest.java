package simpa.acc.api;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import simpa.core.api.utils.FileUtils;

public class BeamTest {

	
//////////////////////////////////////////////////////////////////////////////////////////////////////

	// These are not test just to generate a grid beam
	// @Test
	public void gridToFileTest() {
		createSquareBeam("/tmp/xy.txt", 0.03, 0.03, 60, "xy");
		createSquareBeam("/tmp/xxp.txt", 0.03, 0.06, 60, "xxp");
		createSquareBeam("/tmp/yyp.txt", 0.03, 0.06, 60, "yyp");

	}

	// @Test
	public void collectTest() {
		// we run this after tracking
		List<String> names = collectNames("testbeamyyp-PHS-AT_6.71.txt");
		this.collectLines(names, "testbeamyyp-PHS-AT_0.txt", "testbeamyyp-PHS-0_survived.txt");

		names = collectNames("testbeamxxp-PHS-AT_6.71.txt");
		this.collectLines(names, "testbeamxxp-PHS-AT_0.txt", "testbeamxxp-PHS-0_survived.txt");

		names = collectNames("testbeamxy-PHS-AT_6.71.txt");
		this.collectLines(names, "testbeamxy-PHS-AT_0.txt", "testbeamxy-PHS-0_survived.txt");
	}

	private void createSquareBeam(String file, double extent1, double extent2, int steps, String type) {
		double dx = 2 * extent1 / steps;
		double dy = 2 * extent2 / steps;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < steps; i++) {
			for (int j = 0; j < steps; j++) {
				double x = i * dx - extent1;
				double y = j * dy - extent2;
				if (type.equals("xy"))
					sb.append(x + " 0 " + y + " 0 0 0" + System.lineSeparator());
				if (type.equals("xxp"))
					sb.append(x + " " + y + " 0 0 0 0" + System.lineSeparator());
				if (type.equals("yyp"))
					sb.append("0 0 " + x + " " + y + "  0 0" + System.lineSeparator());
			}
		}
		FileUtils.writeTextFile(file, sb.toString());
	}

	/**
	 * Collects the names of the particles from a file containing the beam phase
	 * space parameters. The particle names are in column 7.
	 * 
	 * @param file the file containing the beam phase space parameters
	 * @return the list of particle names
	 */
	private List<String> collectNames(String file) {
		List<String> names = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line = br.readLine();
			while (line != null) {
				String[] tokens = line.split(" ");
				if (tokens.length == 7) {
					names.add(tokens[6]);
				}
				line = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return names;

	}

	/**
	 * Collect lines from a file containing the beam phase space parameters. Only
	 * those lines are collected that contain the names of the particles. The
	 * loccation of the names is in column 7. The result collection is written to a
	 * file.
	 * 
	 * @param names      the list of particle names
	 * @param inputFile  the file containing the beam phase space parameters
	 * @param outputFile the file to write the result collection with the lines
	 */
	private void collectLines(List<String> names, String inputFile, String outputFile) {
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader br = new BufferedReader(new FileReader(inputFile));
			String line = br.readLine();
			while (line != null) {
				String[] tokens = line.split(" ");
				if (tokens.length == 7) {
					if (names.contains(tokens[6])) {
						sb.append(line + System.lineSeparator());
					}
				}

				line = br.readLine();
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		FileUtils.writeTextFile(outputFile, sb.toString());

	}
}
