package simpa.acc.api.track;


import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.core.api.PhysicsConstants;
import simpa.core.api.track.Particle;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.CalculatorUtils;
import simpa.core.api.utils.LocalFramesCalculator;

public class TLPhaseSpaceObserverTest {

	private static List<Vector3D> l;

	@BeforeAll
	public static void init() {
		l = new ArrayList<>();
		l.add(new Vector3D(0, 0, 0.9));
		l.add(new Vector3D(0, 0, 1));
		l.add(new Vector3D(0, 0, 1.1));
	}

	@Test
	public void testForward() {

		ReferenceOrbit orb = new ReferenceOrbit(l, false, LocalFramesCalculator.DEFAULT_STRATEGY, Vector3D.PLUS_J);
		TLPhaseSpaceObserver pso = new TLPhaseSpaceObserver(1, 0.1, orb, 1, false);

		Vector3D pos = new Vector3D(0.0, 0.0, 0.9);
		double p0 = CalculatorUtils.getMomentumFromGeVoC(1);
		Vector3D mom = new Vector3D(p0 * 0.01, p0 * 0.01, p0 * 1.0);
		Particle p = new Particle(pos.getX(), pos.getY(), pos.getZ(), mom.getX(), mom.getY(), mom.getZ(), 0, 0, 0,
				PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE, PhysicsConstants.PROTON_MASS);
		p.setPreviousPosition(pos);
		p.setPreviousKineticMomentum(mom);
		double ds = 0.2;
		p.updatePosition(p.getX() + ds * p.getPx() / p0, p.getY() + ds * p.getPy() / p0,
				p.getZ() + ds * p.getPz() / p0);
		pso.observe(p);
		PhaseSpaceCoordinates c = pso.getMap().get(p);
		// check signs
		assertTrue(c.x() > 0);
		assertTrue(c.y() > 0);
		assertTrue(c.xp() > 0);
		assertTrue(c.yp() > 0);

	}

	@Test
	public void testBackward() {

		ReferenceOrbit orb = new ReferenceOrbit(l, false, LocalFramesCalculator.DEFAULT_STRATEGY, Vector3D.PLUS_J);
		TLPhaseSpaceObserver pso = new TLPhaseSpaceObserver(0.9, 0.1, orb, 1, true);

		Vector3D pos = new Vector3D(0.05, 0.05, 1.1);
		double p0 = CalculatorUtils.getMomentumFromGeVoC(1);
		// negative momentum, going backward
		Vector3D mom = new Vector3D(-p0 * 0.01, -p0 * 0.01, -p0 * 1.0);
		Particle p = new Particle(pos.getX(), pos.getY(), pos.getZ(), mom.getX(), mom.getY(), mom.getZ(), 0, 0, 0,
				PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE, PhysicsConstants.PROTON_MASS);
		p.setPreviousPosition(pos);
		p.setPreviousKineticMomentum(mom);
		double ds = 0.2;
		p.updatePosition(p.getX() + ds * p.getPx() / p0, p.getY() + ds * p.getPy() / p0,
				p.getZ() + ds * p.getPz() / p0);
		pso.observe(p);
		PhaseSpaceCoordinates c = pso.getMap().get(p);
		// check signs
		assertTrue(c.x() > 0);
		assertTrue(c.y() > 0);
		assertTrue(c.xp() > 0);
		assertTrue(c.yp() > 0);

	}
}
