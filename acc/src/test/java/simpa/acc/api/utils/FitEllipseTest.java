/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api.utils;



import java.util.Arrays;

import org.junit.jupiter.api.Test;

public class FitEllipseTest {
	double[] x= {-0.0024, -0.0024, 0.006020489622152184, -0.010820489622152183, 0.006114693182963201, -0.0109146931829632};
	double[] y= {0.005872202195147035, -0.005872202195147035, 0.0, 0.0, 8.808303292720551E-4, -8.808303292720551E-4};
	
	@Test
	public void test_fitEllipse()  {		
		FitEllipse	ellipse= new FitEllipse();
		ellipse.fitData(x,y);
		double[] d=ellipse.getDimensions();
		Arrays.stream(d).forEach(ed->{System.out.println(ed);});
	}
	

}
