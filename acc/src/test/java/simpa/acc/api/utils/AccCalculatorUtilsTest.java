package simpa.acc.api.utils;


import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import simpa.core.api.PhysicsConstants;
import simpa.core.api.utils.CalculatorUtils;

public class AccCalculatorUtilsTest {

	@Test //Joules to GeV/c
	public void testJoulesToGevOc() {
		double gevoc=CalculatorUtils.getMomentumInGeVoC(7.32E-21);
		//System.out.println(gevoc);
		assertTrue(gevoc==0.013696871892500972);
	}

	@Test //momentum to energy conversion
	public void testMomToEn() {
		double pInGev = 0.0137;
		double ev = AccCalculatorUtils.getKineticEnergy(pInGev, PhysicsConstants.PROTON_MASS);
		System.out.println("The momentum = " + pInGev + " [GeV/c] corresponds to " + ev + " [eV] kinetic energy for a proton.");
		double evOc = AccCalculatorUtils.getMomentumFromEKin(ev, PhysicsConstants.PROTON_MASS);
		assertTrue(Math.abs((pInGev - evOc / 1E9) / pInGev) < 1E-6);
	}
}
