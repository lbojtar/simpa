package simpa.acc.api.utils;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Test;

import simpa.acc.api.TwissParameters;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.FileUtils;

public class EmittanceDefinitionsTest {

	double emH = 1e-6;
	double emV = 2e-6;
	double lpos = 0;
	double alphaH = 3;
	double betaH = 4;
	double posH = 5e-3;
	double angleH = 6e-3;
	double alphaV = 7;
	double betaV = 8;
	double posV = 10e-3;
	double angleV = 0.0;

	double dpOp = 4E-3;
	double emL = dpOp * dpOp;// with betaL=1
	int nSamples = 100000;

	// Our single particle emmittance definition is: The area of the phase space ellipse
	// divided by PI
	// Emmittance is in units of [m]
	// With this definition and the and em=1e-6 and beta=1 we have a circle in phase
	// space with 1 [mm] radius.
	
	@Test
	public void testSingleEmDef() throws IOException {
		String f="/tmp/circle.txt";
		PhaseSpaceEllipse he = new PhaseSpaceEllipse( 1.0, 0.0, 1.0, 0.0, 0.0);
		PhaseSpaceEllipse ve = new PhaseSpaceEllipse( 1.0, 0.0, 1.0, 0.0, 0.0);
		TwissParameters tp = TwissParameters.builder().muH(0).muV(0).dispH(0).dispV(0).dispPrimeH(0).dispPrimeV(0)
				.hEllipse(he).vEllipse(ve).build();

		EllipseBeamSampler s= new EllipseBeamSampler(tp);
		List<PhaseSpaceCoordinates> l = s.sample(100);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < l.size(); i++) {
			PhaseSpaceCoordinates phs = l.get(i);
			sb.append(phs.toString() + System.lineSeparator());
		}
		FileUtils.writeTextFile(f, sb.toString());
		
		TwissParameters t = PhaseSpaceEllipse.getTwissParameters(f);
		assertEquals(t.hEllipse().em(), 1.0, 1e-3);
		assertEquals(t.vEllipse().em(), 1.0, 1e-3);
	}
	
	@Test
	public void testSingleEm() throws IOException {

		String f0 = "/tmp/ellipse_dpOp_0.txt";
		String f1 = "/tmp/ellipse_dpOp_0.001.txt";
		writeEllipse(f0, 0);
		writeEllipse(f1, 0.001);
		TwissParameters t = PhaseSpaceEllipse.getTwissParameters(f0);

		assertEquals(t.hEllipse().em(), emH, 1e-3);
		assertEquals(t.hEllipse().alpha(), alphaH, 1e-3);
		assertEquals(t.hEllipse().beta(), betaH, 1e-3);
		assertEquals(t.hEllipse().pos(), posH, 1e-3);
		assertEquals(t.hEllipse().angle(), angleH, 1e-3);

		assertEquals(t.vEllipse().em(), emV, 1e-3);
		assertEquals(t.vEllipse().alpha(), alphaV, 1e-3);
		assertEquals(t.vEllipse().beta(), betaV, 1e-3);
		assertEquals(t.vEllipse().pos(), posV, 1e-3);
		assertEquals(t.vEllipse().angle(), angleV, 1e-3);

		PhaseSpaceEllipse.printTwiss(f0);
	}
    
	// The RMS emittance is defined by the second order of the distribution.
	// For a Gaussian beam this is the phase space area containg 37% of the particles in the beam (1 sigma)
	// To compare the phase space ellipse single particle emittance with the RMS emittance use the gnuplot command:
	//plot "sigmaTest.txt" u 1:2,"ellipse_dpOp_0.txt" u 1:2
	@Test
	public void testRmsEm() throws IOException {
		String fn = "/tmp/sigmaTest.txt";

		PhaseSpaceEllipse he = new PhaseSpaceEllipse( emH, alphaH, betaH, posH, angleH);
		PhaseSpaceEllipse ve = new PhaseSpaceEllipse( emV, alphaV, betaV, posV, angleV);

		TwissParameters twp = TwissParameters.builder().hEllipse(he).vEllipse(ve).muH(0).muV(0).dispH(0).dispV(0)
				.dispPrimeH(0).dispPrimeV(0).dpOp(emL).build();

		GaussianBeamSampler bs = new GaussianBeamSampler(twp);

		FileUtils.writeTextFile(fn, sampleToString(bs));
		TwissParameters twp2 = BeamMatrix.getTwissParameters(fn);
		TwissParameters.printTwiss(twp2);
		assertEquals(alphaH, twp2.hEllipse().alpha(), 0.1);
		assertEquals(betaH, twp2.hEllipse().beta(), 0.1);
		assertEquals(posH, twp2.hEllipse().pos(), 0.1);
		assertEquals(angleH, twp2.hEllipse().angle(), 0.1);
		assertEquals(emH, twp2.hEllipse().em(), 0.1);

		assertEquals(alphaV, twp2.vEllipse().alpha(), 0.1);
		assertEquals(betaV, twp2.vEllipse().beta(), 0.1);
		assertEquals(posV, twp2.vEllipse().pos(), 0.1);
		assertEquals(angleV, twp2.vEllipse().angle(), 0.1);
		assertEquals(emV, twp2.vEllipse().em(), 0.1);
	}

	private void writeEllipse(String fileName, double dpOp) {
		EllipseBeamSampler bs = getSampler(dpOp);
		List<PhaseSpaceCoordinates> l = bs.sample(100);
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < l.size(); i++) {
			PhaseSpaceCoordinates phs = l.get(i);
			sb.append(phs.toString() + System.lineSeparator());
		}
		FileUtils.writeTextFile(fileName, sb.toString());
	}

	private EllipseBeamSampler getSampler(double dpOp) {

		PhaseSpaceEllipse he = new PhaseSpaceEllipse(emH, alphaH, betaH, posH, angleH);
		PhaseSpaceEllipse ve = new PhaseSpaceEllipse(emV, alphaV, betaV, posV, angleV);
		TwissParameters tp = TwissParameters.builder().muH(0).muV(0).dispH(0).dispV(0).dispPrimeH(0).dispPrimeV(0)
				.hEllipse(he).vEllipse(ve).dpOp(dpOp).build();

		return new EllipseBeamSampler(tp);
	}

	private String sampleToString(GaussianBeamSampler bs) {
		StringBuffer sb = new StringBuffer();
		List<PhaseSpaceCoordinates> l = bs.sample(nSamples);
		for (int i = 0; i < nSamples; i++) {
			PhaseSpaceCoordinates phs = l.get(i);
			sb.append(phs.toString() + System.lineSeparator());
		}
		return sb.toString();
	}
}
