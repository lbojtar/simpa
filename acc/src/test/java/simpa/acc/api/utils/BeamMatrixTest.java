package simpa.acc.api.utils;

import java.io.IOException;
import java.util.List;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import simpa.acc.api.TwissParameters;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.FileUtils;

public class BeamMatrixTest {


	int nSamples = 10000;

	// sampler from file
	@Test
	//@Disabled
	public void test2() throws IOException {
		double emL=1e-4;	
		String file= "/home/lbojtar/git/simpa/examples/lns_line/wdir/tomobeam2023-backward-PHS-AT_0.txt";
		BeamMatrix bm = new BeamMatrix(file);
		GaussianBeamSampler bs = new GaussianBeamSampler(bm);

		FileUtils.writeTextFile("/tmp/tomobeam_from_covm.txt",sampleToString(bs) );
		TwissParameters twp0= BeamMatrix.getTwissParameters(file);
		TwissParameters twp= TwissParameters.builder().copyOf(twp0).dpOp(emL).build();
		bs = new GaussianBeamSampler(twp);
		FileUtils.writeTextFile("/tmp/tomobeam_from_twiss.txt",sampleToString(bs) );
		TwissParameters.printTwiss(twp);

	}

	private String sampleToString(GaussianBeamSampler bs) {
		StringBuffer sb = new StringBuffer();
		List<PhaseSpaceCoordinates> l = bs.sample(nSamples);
		for (int i = 0; i < nSamples; i++) {
			PhaseSpaceCoordinates phs = l.get(i);
			sb.append(phs.toString() + System.lineSeparator());
		}
		return sb.toString();
	}
}
