package simpa.acc.api.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.acc.api.create.AccElement;
import simpa.acc.api.create.Sequence;
import simpa.core.api.SystemConstants;
import simpa.core.api.utils.FileUtils;

class SurveyTest {

	// test shift and theta rotation when the parameters are given at the simpa
	// coordinate system origin.
	
	@Test
	void test1() {

		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + "ELBHZ-SOLUTION.txt",
				"/tmp/ELBHZ-SOLUTION.txt");

		Sequence seq = new Sequence("test", false, null, null);
		seq.addMarkerElement(0, "s1", AccElement.ElementType.MARKER,null);

		seq.addBendingElementAt(1, "bend", 0.0, "/tmp/ELBHZ-SOLUTION.txt", 1.0471975333, 0.9708, 0.0, "ELBHZ",
				Rotation.IDENTITY, null, true);

		seq.addMarkerElement(2, "s3", AccElement.ElementType.MARKER,null);
		seq.getReferenceOrbit().writeMaptoFileWithS("/tmp/centers.txt", seq.getLocalOrigins());
		seq.getReferenceOrbit().writeToFile("/tmp/orbit.txt");

		// madx survey coordinates of a simple sequence with 45 degree orientation
		// difference compared to the simpa sequence
		double x0 = 3;
		double y0 = 0;
		double z0 = 3;
		double theta0 = Math.PI / 4.0;
		double phi0 = 0;
		double psi0 = 0;

		Survey surv = new Survey(seq, x0, y0, z0, theta0, phi0, psi0, "s1");
		Vector3D v = surv.getCenters().get("s3");
		System.out.println(v);
		surv.centersToFile("/tmp/ccsCenters.txt");
		surv.orbitToFile("/tmp/ccsOrbit.txt");
		assertEquals(v.getX(), 3.5465021975e+00, 1e-6);
		assertEquals(v.getZ(), 4.7667045274e+00, 1e-6);
	}
	
	// when the parameters are given NOT at the simpa
	// coordinate system origin.
	@Test
	void test2() {

		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + "ELBHZ-SOLUTION.txt",
				"/tmp/ELBHZ-SOLUTION.txt");

		Sequence seq = new Sequence("test", false, null, null);
		seq.addMarkerElement(0, "s1", AccElement.ElementType.MARKER,null);

		seq.addBendingElementAt(1, "bend", 0.0, "/tmp/ELBHZ-SOLUTION.txt", 1.0471975333, 0.9708, 0.0, "ELBHZ",
				Rotation.IDENTITY, null, true);

		seq.addMarkerElement(2, "s3", AccElement.ElementType.MARKER,null);
		seq.getReferenceOrbit().writeMaptoFileWithS("/tmp/centers.txt", seq.getLocalOrigins());
		seq.getReferenceOrbit().writeToFile("/tmp/orbit.txt");

		// madx survey coordinates of a simple sequence with 45 degree orientation
		// difference compared to the simpa sequence
		double x0 = 3.5465021975e+00;
		double y0 = 0;
		double z0 =  4.7667045274e+00;
		double theta0 = -1.8540183660e-01;
		double phi0 = 0;
		double psi0 = 0;

		Survey surv = new Survey(seq, x0, y0, z0, theta0, phi0, psi0, "s3");
		Vector3D v = surv.getCenters().get("s1");
		System.out.println(v);
		surv.centersToFile("/tmp/ccsCenters.txt");
		surv.orbitToFile("/tmp/ccsOrbit.txt");
		assertEquals(v.getX(), 3, 1e-6);
		assertEquals(v.getZ(),3, 1e-6);
	}
}

//mad-x commands to be checked against
/*
 * 
 * beam, particle=antiproton;
 * 
 * mass=beam->mass;
 * 
 * Ekin=0.0001; ! 100 Kev
 * 
 * gamman=(Ekin/mass)+1; beta=sqrt(-((1/gamman)^2)+1); value,beta;
 * pcn=sqrt((mass^2)*((gamman^2)-1));
 * 
 * beam, particle=antiproton,pc=pcn,exn=6E-6/6,eyn=4E-6/6;
 * 
 * Option, RBARC=false;
 * 
 * TEST_SEQ: SEQUENCE, REFER=CENTRE, l=2.0; p1: MARKER, AT = 0.0; p2: SBEND,
 * ANGLE=0.9708, AT=1.0,L=1.0471975333; p3: MARKER, AT = 2.0; ENDSEQUENCE;
 * 
 * 
 * use, sequence= TEST_SEQ;
 * 
 * set, format="22.10e"; select, flag=survey,clear; select, flag=survey,
 * column=X,Y,Z,THETA,PHI,PSI,S,NAME;
 * 
 * survey, x0 = 3, y0 = 0, z0 = 3, theta0 = PI/4.0, phi0 =0, psi0 =0, file=
 * /tmp/madxSurvey.txt;
 */
