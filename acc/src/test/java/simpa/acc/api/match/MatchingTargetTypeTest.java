package simpa.acc.api.match;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;


public class MatchingTargetTypeTest {

	@Test
	public void test() {
		assertTrue(MatchingTargetType.valueOf("H_BETA")==MatchingTargetType.H_BETA);
	}

}
