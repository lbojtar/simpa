/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.impl;


import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;


public class JNaffTest {

	@Test
	public void test() {
		int maxFrequencies = 2;
		int points = 128;
		double[] data = new double[points]; /* data to be analyzed */

		double tune1=0.445;
		double tune2=0.05;
		
		for (int i = 0; i < data.length; i++) {
			data[i] = Math.sin(tune1*Math.PI * 2 *i)+0.5*Math.sin(tune2*Math.PI * 2 *i);
		//	System.out.println(i + ", " + data[i]);
		}

		JNaff naff = new JNaff(maxFrequencies);		
		naff.calculate(data,1.0);

		for (int i = 0; i < maxFrequencies; i++) {
			System.out.println( naff.getFrequencies()[i] + ", " + naff.getAmplitudes()[i] + ", "
					+ naff.getPhases()[i] + ", " + naff.getSignificances()[i]);

		}
		assertTrue(Math.abs(naff.getFrequencies()[0]-tune1)< 1E-5);
		assertTrue(Math.abs(naff.getFrequencies()[1]-tune2)< 1E-5);
	}
	

}
