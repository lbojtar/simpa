package simpa.acc.impl;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class MethodInvoker {

    /**
     * Invoke the method with the given name and parameters on the given object
     * Convert the parameters to the correct type if possible.
     * 
     * @param clazz      The class to get the method from
     * @param methodName The name of the method to get
     * @param parameters The parameters to convert or keep as they are.
     * @return The converted parameters
     * @throws InvocationTargetException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException     If no method can be found with the given
     */
    public static Object callMethod(Object obj, String methodName, Object[] parameters)
            throws NoSuchMethodError, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
    
        Class<?> clazz = obj.getClass();
        // Get all methods with the given name
        Method[] methods = Arrays.stream(clazz.getMethods())
                .filter(m -> m.getName().equals(methodName))
                .filter(m -> checkParameters(parameters, m.getParameterTypes()))
                .toArray(Method[]::new);

        if (methods.length == 0)
            throw new NoSuchMethodError("No method with the name " + methodName
                    + " and the given parameters can be found in the class " + clazz.getName());
        if (methods.length > 1)
            throw new NoSuchMethodError("More than one method with the name " + methodName
                    + " and the given parameters can be found in the class " + clazz.getName());

        Method m = methods[0];
        
        return m.invoke(obj, parameters);
    }

    /**
     * Check if the given parameters can be converted to the given parameter types
     * 
     * @param parameters     The parameters to convert or keep as they are.
     * @param parameterTypes The parameter types to convert to
     * @return True if the parameters given and the parameter types are compatible.
     */
    private static boolean checkParameters(Object[] parameters, Class<?>[] parameterTypes) {

        // Check if the method can be called with the given parameters
        if (parameters.length != parameterTypes.length)
            return false;
        for (int i = 0; i < parameters.length; i++) {
            Class<?> pt=   parameterTypes[i];
            if (!pt.isAssignableFrom(parameters[i].getClass())){
                if (pt.isPrimitive()){
                    if (pt == int.class && parameters[i].getClass() == Integer.class)
                        continue;
                    if (pt == double.class && parameters[i].getClass() == Double.class)
                        continue;
                    if (pt == float.class && parameters[i].getClass() == Float.class)
                        continue;
                    if (pt == long.class && parameters[i].getClass() == Long.class)
                        continue;
                    if (pt == short.class && parameters[i].getClass() == Short.class)
                        continue;
                    if (pt == byte.class && parameters[i].getClass() == Byte.class)
                        continue;
                    if (pt == char.class && parameters[i].getClass() == Character.class)
                        continue;
                    if (pt == boolean.class && parameters[i].getClass() == Boolean.class)
                        continue;
                    return false;
                       
               }
            }             
        }

        return true;
    }
}
