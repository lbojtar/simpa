/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Authors : Lajos Bojtar & Sidney Goossens
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.acc.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.utils.AccCalculatorUtils;
import simpa.acc.api.utils.IntersectionCalculator;
import simpa.acc.api.utils.IntersectionData;
import simpa.core.api.SystemConstants;
import simpa.core.api.track.ObserverDisk;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.track.TrajectoryData;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.OcTree;

/**
 * Class to calculate intersections of a trajectory with many planes along a
 * reference orbit for a single particle. A list of lists can be obtained with
 * the phase space coordinates @see getPhaseSpaceCoordinates().
 * 
 * There are two use cases foreseen:
 * 
 * 1. Single particle tracking in a ring. In this case we calculate the
 * intersection of one particle in a number of turns with the planes placed
 * along the reference orbit.
 * 
 * 2. Single or multiple particle tracking in a transfer line. In this case
 * there is one intersection for a particle on each disk unless the particle is
 * lost before.
 * 
 * In both cases the the inner lists contain all the intersections or phase
 * space coordinates on a single disk.
 * 
 * @author lbojtar , S. Goosens
 *
 */
public class IntersectionCalculatorImpl implements IntersectionCalculator {

	private static final Logger logger = LogManager.getLogger(IntersectionCalculatorImpl.class);

	private List<IntersectionData> isData;

	private List<ObserverDisk> disks = null;
	private OcTree diskTree = null;
	private List<Vector3D> trajPositions;
	private List<Vector3D> momentums;
	private Vector3D lastPos;
	private final ReferenceOrbit designOrbit;
	private double p0;
	private final double diskRadius;
	private double minx, miny, minz, maxx, maxy, maxz;
	private boolean backward;
	private String particleName;

	/**
	 * Construct a IntersectionCalculator from trajectory data. It is used to
	 * calculate intersections of a trajectory with many planes along a reference
	 * orbit. It calls the processData method to do the actual calculation with the
	 * data from the trajectory file.
	 * 
	 * @param designOrbit    the reference orbit
	 * @param trajectoryData The trajectory data obtained from a single particle
	 *                       tracking in a ring or a transfer line.
	 * @param diskRadius     Radius of the observer disks. It should be bigger than
	 *                       the aperture of the machine at that point, but should
	 *                       not intersect two times the machine.
	 * @param backward       Must be set true when tracking backward.
	 * 
	 * @throws IOException
	 */
	public IntersectionCalculatorImpl(ReferenceOrbit designOrbit, List<TrajectoryData> trajectoryData,
			double diskRadius, boolean backward) throws IOException {
		this.diskRadius = diskRadius;
		this.designOrbit = designOrbit;
		this.backward = backward;

		init();
		processTrajectoryData(trajectoryData, "p"); // for single particle we don't care about the particle name
	}

	/**
	 * Construct a IntersectionCalculator from a trajectory file obtained from a
	 * single particle tracking in a ring or a transfer line. It is used to
	 * calculate intersections of a trajectory with many planes along a reference
	 * orbit. It calls the processData method to do the actual calculation with the
	 * data from the trajectory file.
	 * 
	 * @param designOrbit        the reference orbit
	 * @param trajectoryFileName The trajectory file name obtained from a single
	 *                           particle tracking in a ring or a transfer line.
	 * @param diskRadius         Radius of the observer disks. It should be bigger
	 *                           than the aperture of the machine at that point, but
	 *                           should not intersect two times the machine.
	 * @param backward           Must be set true when tracking backward.
	 * 
	 * @throws IOException
	 */
	public IntersectionCalculatorImpl(ReferenceOrbit designOrbit, String trajectoryFileName, double diskRadius,
			boolean backward) throws IOException {
		this.diskRadius = diskRadius;
		this.designOrbit = designOrbit;
		this.backward = backward;

		init();

		List<TrajectoryData> trajectoryData = TrajectoryData.readParticleTrajectoryFile(trajectoryFileName);
		processTrajectoryData(trajectoryData, "p"); // for single particle we don't care about the particle name
	}

	/**
	 * Construct a IntersectionCalculator without a trajectory file. It is used to
	 * calculate intersections of a trajectory with many planes along a reference
	 * orbit. It does not call the processData method to do the actual calculation.
	 * This constructor is used when the trajectory data is obtained from a beam
	 * tracking in a transfer line.
	 * 
	 * @param designOrbit the reference orbit
	 * @param diskRadius  Radius of the observer disks. It should be bigger than the
	 *                    aperture of the machine at that point, but should not
	 *                    intersect two times the machine.
	 * @param backward    Must be set true when tracking backward.
	 * 
	 * @throws IOException
	 */
	protected IntersectionCalculatorImpl(ReferenceOrbit designOrbit, double diskRadius, boolean backward)
			throws IOException {
		this.diskRadius = diskRadius;
		this.designOrbit = designOrbit;
		this.backward = backward;
		init();

	}

	/**
	 * Do the calculation of the intersections and phase space coordinates.
	 *
	 * @param trajectoryData The trajectory data belonging to a single particle.
	 * @throws IOException
	 */
	protected void processTrajectoryData(List<TrajectoryData> trajectoryData, String pName) throws IOException {

		this.particleName = pName;
		trajPositions = new ArrayList<>();
		momentums = new ArrayList<>();

		int n = 0;
		double momSum = 0;
		for (int i = 0; i < trajectoryData.size(); i++) {
			TrajectoryData td = trajectoryData.get(i);
			if (td == null) {
				break; // TODO can this happen ?
			}
			trajPositions.add(new Vector3D(td.x(), td.y(), td.z()));
			Vector3D mom = new Vector3D(td.px(), td.py(), td.pz());
			momentums.add(mom);
			momSum += mom.getNorm();
			n++;
		}

		p0 = momSum / n; // take the average of momentums as P0

		calculate();

	}

	/**
	 * Writes the phase space coordinates along the orbit to the given file. The
	 * first column is the longitudinal position.
	 * 
	 * @param fileName The output file to be written.
	 */
	public void phaseSpaceToFile(String fileName) {
	
		StringBuffer sb = new StringBuffer();
		sb.append("#s x xp y yp ds dp" + System.lineSeparator());

		for (IntersectionData isd : isData) {
			double lpos = designOrbit.getLongitudinalPosition(isd.orbitIndex());
			PhaseSpaceCoordinates phs = isd.phaseSpace();
			if (phs != null)
				sb.append(lpos + " " + phs.toString() + System.lineSeparator());
		}

		FileUtils.writeTextFile(fileName,sb.toString());

	}

	/**
	 * @return A list of observer disks for each points along the reference orbit.
	 */
	public List<ObserverDisk> getDisks() {
		return disks;
	}

	/**
	 * @return Lists of intersection data.
	 */
	public List<IntersectionData> getIntersections() {
		return isData;
	}

	private void init() {

		isData = new ArrayList<>();
		disks = new ArrayList<>();

		// create disks for a turn
		for (int i = 0; i < designOrbit.getOrbitPoints().size(); i++) {
			Vector3D p = designOrbit.getOrbitPoints().get(i);
			setBounds(p);
			ObserverDisk od = new ObserverDisk(p, designOrbit.getLocalFrames().get(i), diskRadius, backward);
			disks.add(od);

		}

		setupOctree();

	}

	/**
	 * Sets up the OcTree and adds the disks to it.
	 */
	private void setupOctree() {

		for (int i = 0; i < disks.size(); i++) {
			ObserverDisk d = disks.get(i);
			setBounds(d.getCenter());
		}

		diskTree = new OcTree(minx, miny, minz, maxx, maxy, maxz);

		for (int i = 0; i < disks.size(); i++) {
			ObserverDisk d = disks.get(i);
			diskTree.insert(d);
		}
	}

	/**
	 * Sets the boundaries for the OcTree.
	 *
	 * @param point a point to be checked.
	 */
	private void setBounds(Vector3D point) {

		if (point.getX() < minx)
			minx = point.getX();
		else if (point.getX() > maxx)
			maxx = point.getX();

		if (point.getY() < miny)
			miny = point.getY();
		else if (point.getY() > maxy)
			maxy = point.getY();

		if (point.getZ() < minz)
			minz = point.getZ();
		else if (point.getZ() > maxz)
			maxz = point.getZ();
	}

	/**
	 * It calculates the intersections of the trajectory with the disks for all
	 * turns. It puts the given number of disks at trajectory segments for the first
	 * turn. The disks are oriented such a way that the trajectory is normal to the
	 * disks at the first turn. Also calculates the trajectory directions at each
	 * planes for all turns. The intersections and directions can be recovered later
	 * with the getter methods. This is useful for constructing the closed orbit or
	 * calculating the optical functions from a trajectory file.
	 */
	private void calculate() {

		for (int i = 0; i < trajPositions.size(); i++) {
			// one time step can pierce several disks
			HashMap<ObserverDisk, Vector3D> isMap = findIntersections(i);

			// store intersection and direction only if the ObserverDisk intersects with the
			// segment
			if (isMap.size() > 0) {
				Vector3D pos = trajPositions.get(i);
				Vector3D mom = momentums.get(i).scalarMultiply(1 / p0);

				for (ObserverDisk disk : isMap.keySet()) {
					Vector3D is = isMap.get(disk);
					// check if this point is the same as the last intersection to avoid duplicate
					// points
					if (lastPos != null
							&& is.subtract(lastPos).getNorm() < SystemConstants.PLANE_AND_LINE_TOLERANCE * 100.0)
						continue;

					Vector3D momAtIs;
					if (i > 0) {
						Vector3D prevMom = momentums.get(i - 1).scalarMultiply(1 / p0);
						momAtIs = AccCalculatorUtils.interpolateMomentum(is, mom, prevMom, pos,
								trajPositions.get(i - 1));
					} else
						momAtIs = mom; // no momentum interpolation at the first point

					PhaseSpaceCoordinates phs = disk.getPhaseSpaceCoordinates(is, momAtIs);

					int diskIndex = disks.indexOf(disk);

					isData.add(new IntersectionData(diskIndex, particleName, phs));

				}
				lastPos = pos;
			}
		}

	}

	/**
	 * Looks for an intersection between the current and last trajectory point using
	 * an OcTree as underlying data structure for finding the nearest orbit points.
	 *
	 * @param trajIndex Index of the current trajectory point.
	 * @return The locations of the intersections. It might be several disk
	 *         intersecting with the trajectory at the current step.
	 */
	private HashMap<ObserverDisk, Vector3D> findIntersections(int trajIndex) {

		Vector3D point = trajPositions.get(trajIndex);
		Vector3D prevPoint;
		if (trajIndex == 0) {
			// extrapolate backward a small step to initialize the previous step
			Vector3D ds = trajPositions.get(1).subtract(trajPositions.get(0)).scalarMultiply(0.001);
			prevPoint = trajPositions.get(0).subtract(ds);
		} else
			prevPoint = trajPositions.get(trajIndex - 1);

		// TODO: This is not fail safe !!
		// If the disk diameter is large compared to the orbit step, still can fail..
		double searchRange = 5 * designOrbit.getOrbitPoints().get(1).distance(designOrbit.getOrbitPoints().get(0));

		List<ObserverDisk> closest = diskTree.findInRange(point, searchRange).stream().map(item -> (ObserverDisk) item)
				.toList();

		HashMap<ObserverDisk, Vector3D> foundIntersections = new HashMap<>();
		for (ObserverDisk disk : closest) {

			Vector3D is = disk.getIntersection(prevPoint, point);
			if (is != null) {
				foundIntersections.put(disk, is);
			}
		}

		return foundIntersections;
	}

}
