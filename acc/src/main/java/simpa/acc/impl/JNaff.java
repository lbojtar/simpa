/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.impl;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

/**
 * @author Reimplementation of the Laskar's NAFF algorithm by Pierre Freyermuth.
 * Modified by Lajos Bojtar
 */

public class JNaff {

    private int naffPoints;
    private double naffDt;
    private double[] naffData;

    private final double[] frequencies;
    private final double[] amplitudes;
    private final double[] phases;
    private final double[] significances;
    private final int maxFrequencies;

    public JNaff(int maxFrequencies) {
        this.maxFrequencies = maxFrequencies;
        frequencies = new double[maxFrequencies];
        amplitudes = new double[maxFrequencies];
        phases = new double[maxFrequencies];
        significances = new double[maxFrequencies];
    }

    public double[] calculate(double[] data, double dt) {
        double t0 = 0.0;

        /* these control termination of the iteration for frequencies: */
        /* min acceptable contribution of frequency */
        double fracRMSChangeLimit = 0.0;
        /* maximum number of frequencies */
        /* these control the accuracy of each frequency: */
        /* maximum iteractions of parabolic optimizer */
        double freqCycleLimit = 100;
        /* acceptable fractional accuracy of frequency */
        double fracFreqAccuracyLimit = 0.00001;

        /* search only for frequencies between these limits */
        double lowerFreqLimit = 0;
        double upperFreqLimit = data.length;

        performNAFF(frequencies, amplitudes, phases, significances, t0, dt, data, data.length, fracRMSChangeLimit,
                maxFrequencies, freqCycleLimit, fracFreqAccuracyLimit, lowerFreqLimit, upperFreqLimit);

        return frequencies;

    }

    public double[] getFrequencies() {
        return frequencies;
    }

    public double[] getAmplitudes() {
        return amplitudes;
    }

    public double[] getPhases() {
        return phases;
    }

    public double[] getSignificances() {
        return significances;
    }

    private int performNAFF(double[] frequency, /* return or input frequencies */
                            double[] amplitude, /* return amplitudes */
                            double[] phase, /* return phases */
                            double[] significance, /* return "significance" (0=very, large=not) */
                            double t0, /* initial "time" or independent value */
                            double dt, /* spacing of time points */
                            double[] data, /* data to be analyzed */
                            int points, /* number of data points */
            				/* these control termination of the iteration for frequencies: */
            				/* min acceptable contribution of frequency */
                            double fracRMSChangeLimit,
            				/* maximum number of frequencies */
                            long maxFrequencies,
            				/* these control the accuracy of each frequency: */
            				/* maximum iteractions of parabolic optimizer */
                            double freqCycleLimit,
            				/* acceptable fractional accuracy of frequency */
                            double fracFreqAccuracyLimit,
            				/* search only for frequencies between these limits */
                            double lowerFreqLimit, double upperFreqLimit) {

        double rmsOrig, rmsLast, rmsNow, mean;
        int i, freqsFound = 0, FFTFreqs;
        double wStart, freqSpacing;
        int iBest, code, trys;
        double maxMag2;
        double[] sine = new double[points];
        double[] cosine = new double[points];
        double[] magnitude2 = new double[points];
        double[] hanning = new double[points];
        naffData = new double[points];

        if (points < 2) {
            return -1;
        }

        freqSpacing = 1. / (points * dt);
        naffDt = dt;

        /* subtract off mean and apply the Hanning window */
        mean = arithmeticAverage(data, points);
        for (i = 0; i < points; i++) {
            hanning[i] = (1 - Math.cos(Math.PI * 2 * i / (points - 1.0))) / 2;
            naffData[i] = (data[i] - mean) * hanning[i];
        }

        rmsOrig = 0;
        if (fracRMSChangeLimit != 0) {
            for (i = 0; i < points; i++)
                rmsOrig += Math.pow(naffData[i], 2);
            rmsOrig = Math.sqrt(rmsOrig / points);
        }
        rmsLast = rmsOrig;

        FFTFreqs = points / 2 - 1;
        naffPoints = points;
        for (i = 0; i < maxFrequencies; i++)
            amplitude[i] = phase[i] = significance[i] = -1;

        for (i = 0; i < maxFrequencies; i++)
            frequency[i] = -1;

        while (freqsFound < maxFrequencies) {
            simpleFFT(magnitude2, naffData, points);
            maxMag2 = 0;
            iBest = 0;
            for (i = 0; i < FFTFreqs; i++) {
                if (magnitude2[i] > maxMag2) {
                    if (lowerFreqLimit < upperFreqLimit
                            && (i * freqSpacing < lowerFreqLimit || i * freqSpacing > upperFreqLimit))
                        continue;
                    iBest = i;
                    maxMag2 = magnitude2[i];
                }
            }

            if (iBest == 0)
                break;

            wStart = frequency[freqsFound] = iBest * freqSpacing * Math.PI * 2;
            //double scale = naffFunc(wStart);
            for (trys = 0; trys < 2; trys++) {
                code = oneDParabolicOptimization(freqsFound, amplitude, frequency, Math.PI * 2 * freqSpacing, 0.0,
                        Math.PI / dt, freqCycleLimit, fracFreqAccuracyLimit * Math.PI / dt, 0.0, true);
                if (code < 0) {
                    /* amplitude[freqsFound] = frequency[freqsFound] = -1; */
                    frequency[freqsFound] = wStart;
                    amplitude[freqsFound] = maxMag2;
                    break;
                }
            }

            calculatePhaseAndAmplitudeFromFreq(hanning, points, naffDt, frequency[freqsFound], t0, freqsFound, phase,
                    amplitude, significance, cosine, sine);

            frequency[freqsFound] /= Math.PI * 2;
            freqsFound++;

            rmsNow = 0;

            if (fracRMSChangeLimit != 0) {
                /* determine if residual is too small to bother with */
                for (i = 0; i < points; i++)
                    rmsNow += Math.pow(naffData[i], 2);

                rmsNow = Math.sqrt(rmsNow / points);

                if ((rmsLast - rmsNow) / rmsOrig < fracRMSChangeLimit)
                    break;
            }

            rmsLast = rmsNow;
        }
        return freqsFound;
    }

    private int simpleFFT(double[] magnitude2, double[] data, int points) {
        int sizeLimit = points + 2;
        int FFTFreqs, i;

        if (sizeLimit < (points + 2))
            return 0;

        FastFourierTransformer fastFourierTransformer = new FastFourierTransformer(DftNormalization.STANDARD);
        Complex[] complex = fastFourierTransformer.transform(data, TransformType.FORWARD);

        FFTFreqs = points / 2 + 1;

        for (i = 0; i < FFTFreqs; i++) {
            if (i != 0 && !(i == (FFTFreqs - 1) && points % 2 == 0)) {
                magnitude2[i] = Math.pow(complex[i].getReal() * 2, 2) + Math.pow(complex[i].getImaginary() * 2, 2);
            } else {
                magnitude2[i] = Math.pow(complex[i].getReal(), 2) + Math.pow(complex[i].getImaginary(), 2);
            }

        }
        return FFTFreqs;
    }

    private double naffFunc(double omega) {
        int i;
        double sum1 = 0, sum2 = 0, cosine, sine;

        for (i = 0; i < naffPoints; i++) {
            cosine = Math.cos(omega * i * naffDt);
            sine = Math.sin(omega * i * naffDt);
            sum1 += cosine * naffData[i];
            sum2 += sine * naffData[i];
        }

        return Math.pow(sum1, 2) + Math.pow(sum2, 2);
    }

    private double arithmeticAverage(double[] y, int n) {
        int i;
        double sum = 0;

        for (i = 0; i < n; i++)
            sum += y[i];

        return (sum / n);
    }

    private int oneDParabolicOptimization(int index, double[] yReturn, double[] xGuess, double dx, double xLower,
                                          double xUpper,
                                          // double (*func)(double x, long *invalid),
                                          double freqCycleLimit, double dxLimit, double tolerance, boolean maximize) {

        double maxFactor, fBest, xBest;
        long cycle;
        double x0, x1 = 0, x2, f0, f1 = 0, f2;
        double tmp;

        maxFactor = maximize ? -1 : 1;

        x0 = xGuess[index];
        f0 = maxFactor * naffFunc(x0);
        xBest = x0;
        fBest = f0;

        yReturn[index] = maxFactor * f0;

        /* find direction in which function decreases */
        for (cycle = 0; cycle < 2 * freqCycleLimit; cycle++) {
            x1 = x0 + dx;

            if (x1 == x0)
                break;
            if (x1 > xUpper || x1 < xLower)
                return -2;

            f1 = maxFactor * naffFunc(x1);

            if (f1 < fBest) {
                fBest = f1;
                xBest = x1;
            }

            if (f1 < f0) {
                break;
            }

            dx = dx * (cycle % 2 == 0 ? -1 : -0.5);
        }

        if (x1 == x0)
            return 1;
        if (cycle == 2 * freqCycleLimit) {
            if (Math.abs(dx) < dxLimit)
                return 1;
            return -3;
        }

        /* take steps until passing through minimum */
        while (true) {
            x2 = x1 + dx;
            if (x2 > xUpper || x2 < xLower)
                return -4;
            f2 = maxFactor * naffFunc(x2);
            if (f2 < fBest) {
                fBest = f2;
                xBest = x2;
            }
            if (f2 > f1)
                break;
            if (x1 == x2)
                break;
            x0 = x1;
            f0 = f1;
            x1 = x2;
            f1 = f2;
        }

        /* arrange in increasing order */
        if (x0 > x2) {
            tmp = x0;
            x0 = x2;
            x2 = tmp;
            tmp = f0;
            f0 = f2;
            f2 = tmp;
        }

        /* now f0 > f1 and f2 > f1 */
        for (cycle = 0; cycle < freqCycleLimit; cycle++) {
            double numer, denom, x3, f3, scale;
            int failed;

            if (x2 == x0 || (x2 - x0) < dxLimit || (((f2 < f0) ? f0 : f2) - f1) < tolerance)
                break;

            /* try parabolic interpolation */
            numer = Math.pow(x1 - x0, 2) * (f1 - f2) - Math.pow(x1 - x2, 2) * (f1 - f0);
            denom = (x1 - x0) * (f1 - f2) - (x1 - x2) * (f1 - f0);
            x3 = x1 - numer / denom / 2.0;
            failed = 1;
            scale = x2 - x0;

            if (!Double.isInfinite(x3) && x0 < x3 && x3 < x2 && Math.abs(x3 - x0) > 1e-6 * scale
                    && Math.abs(x3 - x1) > 1e-6 * scale && Math.abs(x3 - x2) > 1e-6 * scale) {

                /* evaluate at parabolic interpolation point */
                failed = 0;
                f3 = maxFactor * naffFunc(x3);

                if (f3 < fBest) {
                    fBest = f3;
                    xBest = x3;
                }

                if (f3 < f1) {
                    /* replace point 1 */
                    f1 = f3;
                    x1 = x3;
                } else if (f2 > f0 && f3 < f2) {
                    /* replace point 2 */
                    f2 = f3;
                    x2 = x3;
                    if (x2 < x1) {
                        tmp = x1;
                        x1 = x2;
                        x2 = tmp;
                        tmp = f1;
                        f1 = f2;
                        f2 = tmp;
                    }
                } else if (f2 < f0 && f3 < f0) {
                    /* replace point 0 */
                    f0 = f3;
                    x0 = x3;
                    if (x0 > x1) {
                        tmp = x0;
                        x0 = x1;
                        x1 = tmp;
                        tmp = f0;
                        f0 = f1;
                        f1 = tmp;
                    }
                } else
                    failed = 1;

            }

            if (failed == 1) {
                long right, other;
                for (other = 0; other < 2; other++) {
                    /* try dividing one of the intervals */
                    if (Math.abs(x0 - x1) < Math.abs(x1 - x2)) {
                        if (other == 0) {
                            x3 = (x1 + x2) / 2;
                            right = 1;
                        } else {
                            x3 = (x0 + x1) / 2;
                            right = 0;
                        }
                    } else {
                        if (other == 0) {
                            x3 = (x0 + x1) / 2;
                            right = 0;
                        } else {
                            x3 = (x1 + x2) / 2;
                            right = 1;
                        }
                    }
                    f3 = maxFactor * naffFunc(x3);
                    if (f3 < fBest) {
                        fBest = f3;
                        xBest = x3;
                    }
                    if (f3 < f1) {
                        f1 = f3;
                        x1 = x3;
                        break;
                    }
                    if (right != 0 && f3 < f2) {
                        /* replace point 2 */
                        f2 = f3;
                        x2 = x3;
                        if (x2 < x1) {
                            tmp = x1;
                            x1 = x2;
                            x2 = tmp;
                            tmp = f1;
                            f1 = f2;
                            f2 = tmp;
                        }
                        break;
                    } else if (right == 0 && f3 < f0) {
                        /* replace point 0 */
                        f0 = f3;
                        x0 = x3;
                        if (x0 > x1) {
                            tmp = x0;
                            x0 = x1;
                            x1 = tmp;
                            tmp = f0;
                            f0 = f1;
                            f1 = tmp;
                        }
                        break;
                    }
                }
            }
        }

        yReturn[index] = maxFactor * fBest;
        xGuess[index] = xBest;
        return 1;

    }

    private long calculatePhaseAndAmplitudeFromFreq(double[] hanning, int points, double NAFFdt, double frequency,
                                                    double t0, int index, double[] phase, double[] amplitude, double[] significance, double[] cosine,
                                                    double[] sine) {

        int i;
        double sum_ee1, sum_ee2, sum_ef1, sum_ef2;
        double sum1 = 0, sum2 = 0, freq0;

        freq0 = frequency;

        sum_ee1 = sum_ee2 = sum_ef1 = sum_ef2 = 0;

        for (i = 0; i < points; i++) {
            cosine[i] = Math.cos(freq0 * i * NAFFdt);
            sine[i] = Math.sin(freq0 * i * NAFFdt);
            /* this gives normalization of the overlap sums */
            sum_ee1 += Math.pow(cosine[i], 2) * hanning[i];
            sum_ee2 += Math.pow(sine[i], 2) * hanning[i];
            /* these are the overlap sums */
            sum_ef1 += cosine[i] * naffData[i];
            sum_ef2 += sine[i] * naffData[i];
        }

        for (i = 0; i < points; i++)
            sum1 += Math.pow(naffData[i], 2);

        for (i = 0; i < points; i++)
            naffData[i] -= (sum_ef1 / sum_ee1 * cosine[i] + sum_ef2 / sum_ee2 * sine[i]) * hanning[i];

        for (i = 0; i < points; i++)
            sum2 += Math.pow(naffData[i], 2);

        if (sum1 > 0)
            significance[index] = sum2 / sum1;
        else
            significance[index] = -1;

        freq0 = frequency / Math.PI * 2;

        amplitude[index] = Math.sqrt(Math.pow(sum_ef1 / sum_ee1, 2) + Math.pow(sum_ef2 / sum_ee2, 2));

        /* compute the phase and ensure that it is in the range [-PI, PI] */
        phase[index] = (Math.atan2(-sum_ef2 / sum_ee2, sum_ef1 / sum_ee1) + freq0 * t0 * Math.PI * 2 % Math.PI * 2);

        if (phase[index] < -Math.PI)
            phase[index] += Math.PI * 2;

        if (phase[index] > Math.PI)
            phase[index] -= Math.PI * 2;

        return 0;
    }


}