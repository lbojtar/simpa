package simpa.acc.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.utils.IntersectionCalculator;
import simpa.acc.api.utils.IntersectionData;
import simpa.core.api.SystemConstants;
import simpa.core.api.track.ObserverDisk;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.track.TrajectoryData;
import simpa.core.api.utils.FileUtils;

/**
 * This class is used to calculate intersections of a beam trajectory with many
 * planes along a reference orbit.
 */
public class BeamIntersectionCalculatorImpl implements IntersectionCalculator {

	private IntersectionCalculatorImpl isc;
	private final ReferenceOrbit designOrbit;

	/**
	 * Construct a BeamIntersectionCalculator from a trajectory file obtained from a
	 * beam tracking in a transfer line. It is used to calculate intersections of a
	 * trajectory with many planes along a reference orbit.
	 * 
	 * @param beamTrajFile The trajectory file name obtained from tracking a beam in
	 *                     a transfer line.
	 * @param designOrbit  the reference orbit
	 * 
	 * @param diskRadius   Radius of the observer disks.If the trajectory intersects
	 *                     with the plane, but the intersection point is further
	 *                     from the plane center than this radius, the intersection
	 *                     will be ignored.
	 * @param backward     Must be set true when tracking backward.
	 * 
	 * @throws IOException
	 */
	public BeamIntersectionCalculatorImpl(String beamTrajFile, ReferenceOrbit designOrbit, double diskRadius,
			boolean backward) throws IOException {
		this.designOrbit = designOrbit;
		Map<String, List<TrajectoryData>> trajectoryMap = TrajectoryData.readBeamTrajectoryFile(beamTrajFile);
		process(trajectoryMap, designOrbit, diskRadius, backward);
	}

	/**
	 * Construct a BeamIntersectionCalculator from a trajectory data map obtained
	 * from a beam tracking in a transfer line. It is used to calculate
	 * intersections of a trajectory with many planes along a reference orbit.
	 * 
	 * @param trajectoryMap The trajectory data map obtained from tracking a beam in
	 *                      a transfer line.
	 * @param designOrbit   the reference orbit
	 * 
	 * @param diskRadius    Radius of the observer disks.If the trajectory
	 *                      intersects with the plane, but the intersection point is
	 *                      further from the plane center than this radius, the
	 *                      intersection will be ignored.
	 * @param backward      Must be set true when tracking backward.
	 * 
	 * @throws IOException
	 */
	public BeamIntersectionCalculatorImpl(Map<String, List<TrajectoryData>> trajectoryMap, ReferenceOrbit designOrbit,
			double diskRadius, boolean backward) throws IOException {
		this.designOrbit = designOrbit;
		process(trajectoryMap, designOrbit, diskRadius, backward);
	}

	@Override
	public List<IntersectionData> getIntersections() {
		return isc.getIntersections();
	}

	@Override
	public List<ObserverDisk> getDisks() {
		return isc.getDisks();
	}

	@Override
	public void phaseSpaceToFile(String fileName) {

		StringBuffer sb = new StringBuffer();
		sb.append("#s x xp y yp ds dp" + System.lineSeparator());

		Map<String, List<IntersectionData>> nMap = IntersectionCalculator.getNameMap(isc.getIntersections());
		for (String pName : nMap.keySet()) {
			sb.append(System.lineSeparator()+SystemConstants.PARTICLE_SEPARATOR_STRING + " particle name: " + pName + System.lineSeparator());
			List<IntersectionData> isList = nMap.get(pName);
			for (IntersectionData isd : isList) {
				double lpos = designOrbit.getLongitudinalPosition(isd.orbitIndex());
				PhaseSpaceCoordinates phs = isd.phaseSpace();
				if (phs != null)
					sb.append(lpos + " " + phs.toString() + System.lineSeparator());
			}
		}
		sb.append( System.lineSeparator());
		FileUtils.writeTextFile(fileName,sb.toString());
	}

	private void process(Map<String, List<TrajectoryData>> trajectoryMap, ReferenceOrbit designOrbit, double diskRadius,
			boolean backward) throws IOException {

		isc = new IntersectionCalculatorImpl(designOrbit, diskRadius, backward);

		for (String pName : trajectoryMap.keySet()) {
			List<TrajectoryData> l = trajectoryMap.get(pName);
			isc.processTrajectoryData(l, pName);
		}
	}

}
