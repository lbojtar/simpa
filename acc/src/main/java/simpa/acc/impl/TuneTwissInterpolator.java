package simpa.acc.impl;

import java.util.Map;

import org.apache.commons.math3.analysis.interpolation.InterpolatingMicrosphere2D;

import simpa.acc.api.Tune;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.acc.api.utils.FrequencyAnalysis;

/**
 * This class interpolates the Twiss parameters using a number of {@link Tune},
 * {@link TwissParameters} samples. The implementation uses the
 * {@link InterpolatingMicrosphere2D} class.
 * 
 * @author lbojtar
 *
 */
public class TuneTwissInterpolator {

	private double longiPos;

	private InterpolatingMicrosphere2D hPosInt, vPosInt, hAngleInt, vAngleInt, hAlphaInt, vAlphaInt, hBetaInt, vBetaInt;

	private double[] hPosArr, vPosArr, hAngleArr, vAngleArr, hAlphaArr, vAlphaArr, hBetaArr, vBetaArr;
	private double[][] sampleTunes; // [number of sample tunes][qh,qv]

	/**
	 * Constructor using the sample points in the given map.
	 * 
	 * @param tunemap A map of Tune, TwissParameters pairs. The
	 *                {@link InterpolatingMicrosphere2D} will be constructed using
	 *                these pairs. The tunes in the map should correspond to working
	 *                points of the machine free of resonances. It doesn't need to
	 *                be equally spaced, but should sample reasonably well each
	 *                region of the tune space which will be scanned by the
	 *                {@link FrequencyAnalysis} class. The
	 *                {@link InterpolatingMicrosphere2D} class can extrapolate, so
	 *                the sample tunes don't need to be exactly in the corners of
	 *                the scanned tune space.
	 */
	public TuneTwissInterpolator(Map<Tune, TwissParameters> tunemap) {

		// check if longitudinal positions are the same for all TwissParameters
		longiPos = tunemap.values().stream().mapToDouble(t -> t.longiPos()).findFirst().getAsDouble();
		if (tunemap.values().stream().mapToDouble(t -> t.longiPos()).anyMatch(p -> p != longiPos)) {
			throw new IllegalArgumentException("All TwissParameters must have the same longitudinal position.");
		}

		int elements = 100;
		double maxDarkFraction = 0.5;
		double darkThreshold = 0;
		double background = 0;

		hPosInt = new InterpolatingMicrosphere2D(elements, maxDarkFraction, darkThreshold, background);
		vPosInt = new InterpolatingMicrosphere2D(elements, maxDarkFraction, darkThreshold, background);
		hAngleInt = new InterpolatingMicrosphere2D(elements, maxDarkFraction, darkThreshold, background);
		vAngleInt = new InterpolatingMicrosphere2D(elements, maxDarkFraction, darkThreshold, background);
		hAlphaInt = new InterpolatingMicrosphere2D(elements, maxDarkFraction, darkThreshold, background);
		vAlphaInt = new InterpolatingMicrosphere2D(elements, maxDarkFraction, darkThreshold, background);
		hBetaInt = new InterpolatingMicrosphere2D(elements, maxDarkFraction, darkThreshold, background);
		vBetaInt = new InterpolatingMicrosphere2D(elements, maxDarkFraction, darkThreshold, background);

		hPosArr = new double[tunemap.size()];
		vPosArr = new double[tunemap.size()];
		hAngleArr = new double[tunemap.size()];
		vAngleArr = new double[tunemap.size()];
		hAlphaArr = new double[tunemap.size()];
		vAlphaArr = new double[tunemap.size()];
		hBetaArr = new double[tunemap.size()];
		vBetaArr = new double[tunemap.size()];

		sampleTunes = new double[tunemap.size()][2];

		int i = 0;
		for (Map.Entry<Tune, TwissParameters> e : tunemap.entrySet()) {
			TwissParameters tp = e.getValue();
			hPosArr[i] = tp.hEllipse().pos();
			vPosArr[i] = tp.vEllipse().pos();
			hAngleArr[i] = tp.hEllipse().angle();
			vAngleArr[i] = tp.vEllipse().angle();
			hAlphaArr[i] = tp.hEllipse().alpha();
			vAlphaArr[i] = tp.vEllipse().alpha();
			hBetaArr[i] = tp.hEllipse().beta();
			vBetaArr[i] = tp.vEllipse().beta();

			sampleTunes[i][0] = e.getKey().qh();
			sampleTunes[i][1] = e.getKey().qv();
			i++;
		}

	}

	/**
	 * Get the Twiss parameters describing the optics at the given working point.
	 * During the the scan made by the {@link FrequencyAnalysis} class the emittance
	 * should be kept approximately constant and to achieve that the knowledge of
	 * the Twiss parameters at different working points are needed.
	 * 
	 * @param tune A tune object.
	 * 
	 * @return The Twiss parameters at the working point. Only the relevant part of
	 *         the Twiss parameters are set, i.e. the horizontal and vertical
	 *         alpha,beta,position and angle. The other fields in the Twiss
	 *         parameters left zero.
	 */

	public TwissParameters getTwissParameters(Tune tune) {
		double[] point = new double[2];
		double exponent = 2;
		double noInterpolationTolerance = 1E-5;

		point[0] = tune.qh();
		point[1] = tune.qv();

		double x = hPosInt.value(point, sampleTunes, hPosArr, exponent, noInterpolationTolerance);
		double y = vPosInt.value(point, sampleTunes, vPosArr, exponent, noInterpolationTolerance);
		double xp = hAngleInt.value(point, sampleTunes, hAngleArr, exponent, noInterpolationTolerance);
		double yp = vAngleInt.value(point, sampleTunes, vAngleArr, exponent, noInterpolationTolerance);
		double hAlpha = hAlphaInt.value(point, sampleTunes, hAlphaArr, exponent, noInterpolationTolerance);
		double vAlpha = vAlphaInt.value(point, sampleTunes, vAlphaArr, exponent, noInterpolationTolerance);
		double hBeta = hBetaInt.value(point, sampleTunes, hBetaArr, exponent, noInterpolationTolerance);
		double vBeta = vBetaInt.value(point, sampleTunes, vBetaArr, exponent, noInterpolationTolerance);

		PhaseSpaceEllipse he = new PhaseSpaceEllipse( 0, hAlpha, hBeta, x, xp);
		PhaseSpaceEllipse ve = new PhaseSpaceEllipse( 0, vAlpha, vBeta, y, yp);
		
		//mu and dispersion we don't care, we scan with nominal momentum
		return TwissParameters.builder().hEllipse(he).vEllipse(ve).muH(0).muV(0).dispH(0).dispV(0).dispPrimeH(0)
				.dispPrimeV(0).build();

	}

}
