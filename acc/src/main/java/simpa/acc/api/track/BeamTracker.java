/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.acc.api.track;

import java.io.IOException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.Beam;
import simpa.core.api.track.ParallelTracker;
import simpa.core.api.track.ParticleTrackerTask;

/**
 * Abstract class for beam tracking.
 * 
 * @author lbojtar
 *
 */
public abstract class BeamTracker {
	private static final Logger logger = LogManager.getLogger(BeamTracker.class);

	protected ArrayList<ParticleTrackerTask> trackerTasks;

	private ParallelTracker parallelTracker; // this must be created by the subclass

	public BeamTracker() throws IOException {
		parallelTracker = new ParallelTracker();
	}

	public BeamTracker(int nThreads) throws IOException {
		parallelTracker = new ParallelTracker(nThreads);
	}

	/**
	 * Tracks the beam.
	 * 
	 * @throws IOException When some output file can't be written.
	 */
	public void track() throws IOException {
		logger.info("Started tracking beam with " + trackerTasks.size() + " particles.");
		for (ParticleTrackerTask task : trackerTasks) {
			parallelTracker.addCopyToQueue(task);
		}

		parallelTracker.waitTermination();
		finish();
		parallelTracker.shutdown();
	}


	/**
	 * The class implementing this interface must set up the tracking. It must
	 * create the list of tracker tasks and add the observers to the tasks. The
	 * observers must be added to the tasks before calling the {@link track() }
	 * method.
	 * 
	 * @param beam      The beam to track.
	 * @param turns     Number of turns to track.
	 * @param maxradius Maximum radius for the phase space observer.
	 * @param stepSize  Step size for the tracking.
	 * @param backward  Set true if tracking backward.
	 * 
	 * @throws IOException
	 */
	protected abstract void setupTracking(Beam beam, int turns, double maxradius, double stepSize, boolean backward)
			throws IOException;

	/**
	 * This method is called when all particles has been tracked. It should write
	 * any observer data to file(s) which was stored during the tracking. This is
	 * typically the phase space and trajectory data.
	 */
	public abstract void finish();
}
