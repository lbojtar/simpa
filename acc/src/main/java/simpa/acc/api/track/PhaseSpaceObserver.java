/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 * <p>
 * Author : Lajos Bojtar
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.acc.api.track;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.utils.AccCalculatorUtils;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.SystemConstants;
import simpa.core.api.TangentVector3D;
import simpa.core.api.track.TrackingObserver;
import simpa.core.api.track.ObserverDisk;
import simpa.core.api.track.Particle;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.LocalFrame;
import simpa.core.api.utils.PathUtils;

/**
 * Observer that can be added to a particle tracking task for observation of the
 * phase space coordinates when it passes the observer disk.
 */
public class PhaseSpaceObserver implements TrackingObserver {
	protected final ReferenceOrbit orbit;

	private static final Logger logger = LogManager.getLogger(PhaseSpaceObserver.class);
	private static int flushPerTurns = 20;
	private final ObserverDisk disk;
	private final double nominalMomentum;
	private final Map<String, BufferedWriter> phaseSpaceWriters = new HashMap<>();// holds phase space writers for each
																					// particle
	private String outDirectory = "./";
	private int turns;
	private BufferedWriter outputStream;
	private NumberFormat numberFormat = NumberFormat.getInstance();
	private boolean backward;
	protected double longiPos;

	/**
	 * Constructs a disk with the given radius at the given index of the reference
	 * orbit and writes the particle's phase space coordinates into the given file
	 * each time it is going through the disk.
	 * 
	 * @param orbit           The reference orbit.
	 * @param orbitIndex      The index of the orbit point at which the observer
	 *                        will be added.
	 * @param radius          Radius of the observation plane. This should be big
	 *                        enough to cover the aperture, but smaller than the
	 *                        diameter of the machine, otherwise the particle
	 *                        trajectory would traverse two times the observation
	 *                        plane.
	 * 
	 * @param nominalMomentum The nominal momentum.
	 * @param backward        Must be set true when tracking backward.
	 */
	public PhaseSpaceObserver(ReferenceOrbit orbit, int orbitIndex, double radius, double nominalMomentum,
			boolean backward) {
		this.orbit = orbit;
		this.nominalMomentum = nominalMomentum;
		this.backward=backward;
		TangentVector3D tv = orbit.getTangentVectorAt(orbitIndex);
		Vector3D center = tv.location();
		logger.debug("Phase space observer plane centre placed at :" + center + " [m]");

		// construct the observation disk
		LocalFrame lf = orbit.getLocalFrames().get(orbitIndex);
	
		// When tracking backward we have a LEFT coordinate system
		// Tracking backward inverts time, the charge is the same, so we must have a
		// mirroring to keep CPT symmetry valid
		// Which makes our original RIGH handed coordinate system into a LEFT handed
		// one.
		LocalFrame reversedFrame = new LocalFrame(lf.lx().scalarMultiply(1.0), lf.ly().scalarMultiply(1.0),
				lf.lz().scalarMultiply(-1.0));
		
		if (backward)
			this.disk = new ObserverDisk(center, reversedFrame, radius,backward);
		else
			this.disk = new ObserverDisk(center, lf, radius,backward);

		longiPos = orbit.getLongitudinalPosition(orbitIndex);
	}

	/**
	 * Constructs a disk with the given radius at the longitudinal position and
	 * writes the particle's phase space coordinates into the given file each time
	 * it is going through the disk.
	 *
	 * @param longitudinalPos Longitudinal position of the observation plane
	 * @param radius          Radius of the observation plane. This should be big
	 *                        enough to cover the aperture, but smaller than the
	 *                        diameter of the machine, otherwise the particle
	 *                        trajectory would traverse two times the observation
	 *                        plane.
	 *
	 * @param nominalMomentum The nominal momentum in [GeV/c].
	 * @param orbit           The reference orbit.
	 * @param backward        Must be set true when tracking backward.
	 */
	public PhaseSpaceObserver(double longitudinalPos, double radius, double nominalMomentum, ReferenceOrbit orbit,
			boolean backward) {
		this.longiPos = longitudinalPos;
		this.orbit = orbit;
		this.nominalMomentum = nominalMomentum;
		this.backward=backward;
		
		int orbitIndex = PathUtils.getClosestIndex(orbit.getOrbitPoints(), longiPos, orbit.isCircular());
		Vector3D center = orbit.getOrbitPoints().get(orbitIndex);
		// construct the observation disk
		LocalFrame lf = orbit.getLocalFrames().get(orbitIndex);

		// When tracking backward we have a LEFT coordinate system
		// Tracking backward inverts time, the charge is the same, so we must have a
		// mirroring to keep CPT symmetry valid
		// Which makes our original RIGH handed coordinate system into a LEFT handed
		// one.
		LocalFrame reversedFrame = new LocalFrame(lf.lx().scalarMultiply(1.0), lf.ly().scalarMultiply(1.0),
				lf.lz().scalarMultiply(-1.0));
		if (backward)
			this.disk = new ObserverDisk(center, reversedFrame, radius,backward);
		else
			this.disk = new ObserverDisk(center, lf, radius,backward);
	}

	/**
	 * Gets the amount of turns between flushes.
	 *
	 * @return The amount of turns between flushes.
	 */
	public static int getFlushPerTurns() {
		return flushPerTurns;
	}

	/**
	 * Updates/sets the amount of turns between flushes.
	 *
	 * @param fpt The amount of turns per flush.
	 */
	public static void setFlushPerTurns(int fpt) {
		flushPerTurns = fpt;
	}

	/**
	 * Observes the location and angle of the particle as it passes through the
	 * disk.
	 *
	 * @param p Particle to observe
	 */
	@Override
	public void observe(Particle p) {
		Vector3D pos = new Vector3D(p.getX(), p.getY(), p.getZ());
		Vector3D previousPos = p.getPreviousPosition();

		numberFormat.setMinimumFractionDigits(SystemConstants.OBSERVER_POSITION_DIGITS);

		if (previousPos != null) {
			Line l = new Line(pos, previousPos, SystemConstants.PLANE_AND_LINE_TOLERANCE);
			Vector3D is = disk.getPlane().intersection(l);
			// Check if the line going through the disk
			if (is != null && Math.abs(is.subtract(disk.getCenter()).getNorm()) < disk.getDiskRadius()) {

				// Check if the intersection point is between the current position and the
				// previous position
				double ds = Math.abs(pos.subtract(previousPos).getNorm());

				if (Math.abs(is.subtract(previousPos).getNorm()) < ds && Math.abs(is.subtract(pos).getNorm()) < ds) {

					PhaseSpaceCoordinates c = getPhaseSpaceCoordinates(is, p);

					String s = c.toString() + System.lineSeparator();

					String fileName = outDirectory
							+ FileNamingConventions.getPhaseSpaceFileName(p.getName(), longiPos, numberFormat);

					outputStream = phaseSpaceWriters.get(fileName);

					try {

						if (outputStream == null) {
							outputStream = new BufferedWriter(new FileWriter(fileName, false));
							outputStream.append(getHeader());
							phaseSpaceWriters.put(fileName, outputStream);
						}

						outputStream.append(s, 0, s.length());

						if (turns % flushPerTurns == 0) {
							outputStream.flush();
						}

					} catch (IOException e) {
						logger.error("Something went wrong  while PhaseSpaceObserver writing to file.");
						logger.debug(Arrays.toString(e.getStackTrace()));
					}
					turns++;
				}
			}
		}
	}

	@Override
	public void finish(Particle p) {
		if (outputStream == null) {
			return;
		}

		try {
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			logger.error("IOException happened");
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

	}

	/**
	 * Gets the disk of this PhaseSpaceObserver
	 *
	 * @return ObserverDisk
	 */
	public ObserverDisk getDisk() {
		return disk;
	}

	/**
	 * Gets the output directory.
	 *
	 * @return Local path of the output directory.
	 */
	public String getOutDirectory() {
		return outDirectory;
	}

	/**
	 * Sets the output directory.
	 *
	 * @param outDirectory The output directory.
	 */
	public void setOutDirectory(String outDirectory) {
		this.outDirectory = outDirectory;
	}

	/**
	 * @return The longitudinal position of the observer along the reference orbit
	 *         [m].
	 */
	public double getLongitudinalPosition() {
		return longiPos;
	}

	/**
	 * Gets the phase space coordinates.
	 *
	 * @param is
	 * @param p  particle
	 * @return The phase space coordinates.
	 */
	public PhaseSpaceCoordinates getPhaseSpaceCoordinates(Vector3D is, Particle p) {
		if (nominalMomentum == 0) {
			throw new IllegalArgumentException("The nominal momentum   is not set !!");
		}

		// We have to interpolate the momentum to the location of the disk otherwise in
		// bending we would have a large
		// error

		double p0 = AccCalculatorUtils.getMomentumFromGeVoC(nominalMomentum);
		Vector3D prevKinMom = p.getPreviousKineticMomentum().scalarMultiply(1 / p0);
		Vector3D kinMom = p.getKineticMomentum().scalarMultiply(1 / p0);
		Vector3D pos = new Vector3D(p.getX(), p.getY(), p.getZ());
		Vector3D prevPos = p.getPreviousPosition();
		Vector3D momAtIs = AccCalculatorUtils.interpolateMomentum(is, kinMom, prevKinMom, pos, prevPos);
		double v = AccCalculatorUtils.getVelocity(nominalMomentum, p.getMass());
		double trev = v / orbit.getOrbitLength();
		return getPhaseSpaceCoordinates(is, momAtIs, p.getTime(), v, trev);
	}

	private String getHeader() {
		return "#x xp y yp z zp" + System.lineSeparator() + "#LONGITUDINAL_POSITION=" + longiPos
				+ System.lineSeparator();
	}

	private PhaseSpaceCoordinates getPhaseSpaceCoordinates(Vector3D is, Vector3D mom, double time,
			double nominalVelocity, double trev) {
		if (is.subtract(disk.getCenter()).getNorm() > disk.getDiskRadius()) {
			return null;
		}

		// intersection in local coords.
		Vector3D lis = is.subtract(disk.getCenter());
		double sign = 1.0;
		if (backward)
			sign = -1.0;
	
		double y = disk.getYAxis().dotProduct(lis);
		double x = disk.getXAxis().dotProduct(lis);
		double yp = sign*disk.getYAxis().dotProduct(mom);
		double xp = sign*disk.getXAxis().dotProduct(mom);

		double ds, dpOp;
		if (trev != 0) {
			double dt = time % trev;
			// make it +- 0.5*trev around zero
			if (dt > trev / 2.0)
				dt = dt - trev;

			ds = nominalVelocity * dt;
			dpOp = mom.getNorm() - 1;
		} else {
			ds = 0;
			dpOp = 0;
		}
		return new PhaseSpaceCoordinates(x, xp, y, yp, ds, dpOp);
	}

}
