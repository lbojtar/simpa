/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api.track;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.TwissParameters;
import simpa.acc.api.utils.FitEllipse;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.FileUtils;

public record PhaseSpaceEllipse(double em, double alpha, double beta, double pos, double angle) {

	private static final Logger logger = LogManager.getLogger(PhaseSpaceEllipse.class);

	/**
	 * @return Deep copy of this ellipse.
	 */
	public PhaseSpaceEllipse copy() {
		return new PhaseSpaceEllipse(em(), alpha(), beta(), pos(), angle());
	}

	/**
	 * @return The ellipse fit belonging to this instance.
	 * 
	 */
	public FitEllipse getEllipse() {
		return getEllipse(em, alpha, beta, pos, angle);
	}

	/**
	 * Constructs an ellipse with the given Twiss parameters.The Ellipse can be used
	 * to sample points on it.
	 *
	 * @param em    Emittance [mm mrad]
	 * @param alpha Twiss parameter alpha
	 * @param beta  Twiss parameter beta [m]
	 * @param pos   x coordinate of the ellipse center [m]
	 * @param angle y coordinate of the ellipse center [m]
	 * @return The ellipse fit.
	 */
	public static FitEllipse getEllipse(double em, double alpha, double beta, double pos, double angle) {
		double gamma = (1 + alpha * alpha) / beta;

		double[] x = new double[6];
		double[] y = new double[6];
		// select 6 point on the ellipse and fit, the fit will be exact
		// https://indico.cern.ch/event/528094/contributions/2213316/attachments/1322590/1984069/L3-4-5_-_Transverse_Beam_Dynamics.pdf
		x[0] = pos;
		y[0] = angle + Math.sqrt(em / beta);

		x[1] = pos;
		y[1] = angle - Math.sqrt(em / beta);

		x[2] = pos + Math.sqrt(em / gamma);
		y[2] = angle;

		x[3] = pos - Math.sqrt(em / gamma);
		y[3] = angle;

		x[4] = pos + Math.sqrt(em * beta);
		y[4] = angle - alpha * Math.sqrt(em / beta);

		x[5] = pos - Math.sqrt(em * beta);
		y[5] = angle + alpha * Math.sqrt(em / beta);

		FitEllipse ellipse = new FitEllipse();
		ellipse.fitData(x, y);
		return ellipse;
	}

	/**
	 * Fits a phase space ellipse to the list of phase space coordinates. At least 6
	 * points must be in the list.
	 *
	 * @param phsc  A list of phase space coordinates.
	 * @param plane The phase space plane to get information from about the phase
	 *              space.
	 * @return The ellipse fit found at the index.
	 */
	public static PhaseSpaceEllipse calculatePhaseSpaceEllipse(double lpos, List<PhaseSpaceCoordinates> phsc,
			PhaseSpacePlane plane) {

		int n = phsc.size();

		double[] x = new double[n];
		double[] y = new double[n];

		for (int i = 0; i < n; i++) {

			PhaseSpaceCoordinates c = phsc.get(i);
			if (plane == PhaseSpacePlane.HORIZONTAL) {
				x[i] = c.x();
				y[i] = c.xp();
			}

			if (plane == PhaseSpacePlane.VERTICAL) {
				x[i] = c.y();
				y[i] = c.yp();
			}
		}

		FitEllipse ef = new FitEllipse();
		ef.fitData(x, y);

		PhaseSpaceEllipse e = calculatePhaseSpaceEllipse(lpos, ef);

		return e;
	}

	/**
	 * Calculates the phase space ellipse parameters from phase space coordinates
	 * file,then print it to the stdout. This is a subset of Twiss parameters, only
	 * those which can be calculated from a single phase space file. It does not
	 * contain the dispersion and the phase advances. The input file should contain
	 * the phase space coordinates at a some longitudinal position.
	 *
	 * @param phaseSpaceFile Input file containing the phase space coordinates for
	 *                       at least 6 machine turns. In case of a transfer line
	 *                       the file should contain the phase space coordinates of
	 *                       at least 6 particles. The initial phase space
	 *                       coordinates at the beginning of the transfer line
	 *                       should be on an ellipse, otherwise the fit might fail.
	 * @return Those Twiss parameters which can be calculated from a phase space
	 *         file.
	 * @throws IOException When the file can't be read.
	 */
	public static TwissParameters getTwissParameters(String phaseSpaceFile) throws IOException {
		String key = "#LONGITUDINAL_POSITION=";
		String line = FileUtils.getKeyLineFromFile(phaseSpaceFile, key);
		String s = line.replace(key, "");
		double longiPos = 0;
		try {
			longiPos = Double.parseDouble(s);
		} catch (NumberFormatException e) {
			logger.error("Can't parse longitudinal position from file: " + phaseSpaceFile + " set to zero");
		}

		double[][] da = FileUtils.read2DArray(phaseSpaceFile, 6);

		List<FitEllipse> fits = getFit(da, 0, da.length);
		PhaseSpaceEllipse he = PhaseSpaceEllipse.calculatePhaseSpaceEllipse(longiPos, fits.get(0));
		PhaseSpaceEllipse ve = PhaseSpaceEllipse.calculatePhaseSpaceEllipse(longiPos, fits.get(1));

		// mu and dispersion is not contained in a single phase space file
		return TwissParameters.builder().hEllipse(he).vEllipse(ve).longiPos(longiPos).muH(0).muV(0).dispH(0).dispV(0)
				.dispPrimeH(0)
				.dispPrimeV(0).build();

	}

	/**
	 * Prints those Twiss parameters which can be calculated from a phase space
	 * file. These are the parameters of the horizontal and vertical phase space
	 * ellipses.
	 * 
	 * @param phaseSpaceFile Input file containing the phase space coordinates for
	 *                       at least 6 machine turns. In case of a transfer line
	 *                       the file should contain the phase space coordinates of
	 *                       at least 6 particles. The initial phase space
	 *                       coordinates at the beginning of the transfer line
	 *                       should be on an ellipse, otherwise the fit will fail.
	 * @return The TwissParameters
	 */
	public static TwissParameters printTwiss(String phaseSpaceFile) throws IOException {
		logger.info("TWISS parameters for phase space coordinates file: " + phaseSpaceFile + System.lineSeparator());
		TwissParameters twp = getTwissParameters(phaseSpaceFile);
		TwissParameters.printTwiss(twp);
		return twp;
	}

	private static List<FitEllipse> getFit(double[][] da, int start, int end) {
		if (da.length < 6) {
			throw new IllegalArgumentException("Not enough points to fit an ellipse (minimum is 6)");
		}

		int n = end - start;
		double[] xh = new double[n];
		double[] yh = new double[n];
		double[] xv = new double[n];
		double[] yv = new double[n];

		for (int i = 0; i < n; i++) {
			xh[i] = da[start + i][0];
			yh[i] = da[start + i][1];
			xv[i] = da[start + i][2];
			yv[i] = da[start + i][3];
		}

		List<FitEllipse> res = new ArrayList<>();
		FitEllipse fe = new FitEllipse();
		fe.fitData(xh, yh);
		res.add(fe);

		fe = new FitEllipse();
		fe.fitData(xv, yv);
		res.add(fe);

		return res;
	}

	/**
	 * Calculates the transverse parameters of an ellipse fit.
	 *
	 * @param ef Ellipse fit.
	 * @return The PhaseSpaceEllipse object calculated from the given ellipse fit.
	 */
	private static PhaseSpaceEllipse calculatePhaseSpaceEllipse(double lpos, FitEllipse ef) {

		// 1. emmitance
		double em = ef.getDimensions()[2] * ef.getDimensions()[3];

		// 2. beta
		// Sqrt[beta * em]= xmax for a centered ellipse
		double pos = (ef.getXMax() + ef.getXMin()) / 2.0;
		double x = ef.getXMax() - pos;
		double beta = x * x / em; // <x,x> = beta

		// 3. alpha
		double angle = (ef.getYMax() + ef.getYMin()) / 2.0;
		double alpha = -(ef.getYatXmax() - angle) / (Math.sqrt(em / beta)); // <x,x'> = -alpha

		return new PhaseSpaceEllipse(em, alpha, beta, pos, angle);
	}

}
