/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.acc.api.track;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.core.api.SystemConstants;
import simpa.core.api.track.TrackingObserver;
import simpa.core.api.track.ObserverDisk;
import simpa.core.api.track.Particle;

/**
 * An observer that counts the amount of turns a particle makes in the machine.
 */
public class TurnObserver implements TrackingObserver {
	private static final Logger logger = LogManager.getLogger(TurnObserver.class);

	private ObserverDisk disk;
	private int printPerTurns = 1;
	private long maximumTurns = Long.MAX_VALUE;
	private ReferenceOrbit refOrbit;


	private final Map<String, Integer> turnsMap = new HashMap<>(); // used for bunch tracking

	/**
	 * @param refOrbit - A reference orbit belonging to a circular machine or a
	 *                 transfer line.
	 * @param radius   - Radius of the observation plane. This should be big enough
	 *                 to cover the aperture, but smaller than the diameter of the
	 *                 machine, otherwise the particle trajectory would traverse two
	 *                 times the observation plane.
	 * @param backward If true, the observer is placed at the first point of the
	 *                 orbit. This should be set true when tracking backward.
	 */
	public TurnObserver(ReferenceOrbit refOrbit, double radius, boolean backward) {
		this.refOrbit = refOrbit;
		Vector3D center;
		List<Vector3D> orbit = refOrbit.getOrbitPoints();
		
		int index;
		if (backward)
			index = 0;
		else
			index = refOrbit.getOrbitPoints().size() - 1; // at the end of sequence
		center = orbit.get(index);
		this.disk = new ObserverDisk(center, refOrbit.getLocalFrames().get(index), radius, backward);

	}

	@Override
	public void observe(Particle p) {

		// we return quickly in most cases when the intersection is not possible
		Vector3D pos = new Vector3D(p.getX(), p.getY(), p.getZ());
		if (disk.getCenter().distance(pos) > disk.getDiskRadius())
			return;

		Vector3D previousPos = p.getPreviousPosition();

		if (previousPos != null) {
			Line l = new Line(pos, previousPos, SystemConstants.PLANE_AND_LINE_TOLERANCE);
			Vector3D is = disk.getPlane().intersection(l);

			// Check if the line going through the disk
			if (is != null && Math.abs(is.subtract(disk.getCenter()).getNorm()) < disk.getDiskRadius()) {

				// Check if the intersection point is between the current position and the
				// previous position
				double ds = Math.abs(pos.subtract(previousPos).getNorm());
				if (Math.abs(is.subtract(previousPos).getNorm()) < ds && Math.abs(is.subtract(pos).getNorm()) < ds) {

					Integer turn = turnsMap.get(p.getName());
					if (turn == null) {
						if (refOrbit.isCircular()) {
							turnsMap.put(p.getName(), 1);
							return;
						} else {
							p.setTerminated(true);
							// logger.info("Particle " + p.getName() + " has reached the end of the line");
							return;
						}
					}

					assert (refOrbit.isCircular());

					if (turn == maximumTurns)
						p.setTerminated(true);

					if (turn % printPerTurns == 0) {
						logger.info(p.getName() + " Turn: " + turn);
					}

					turnsMap.replace(p.getName(), turn + 1);

				}
			}
		}
	}

	/**
	 * 
	 * @return The observer disk
	 */
	public ObserverDisk getDisk() {
		return disk;
	}
	
	@Override
	public void finish(Particle p) {

	}

	/**
	 * Gets the amount of turns before information is logged to the console.
	 *
	 * @return The amount of turns before information is logged to the console.
	 */
	public int getPrintPerTurns() {
		return printPerTurns;
	}

	/**
	 * Sets the amount of turns before information is logged to the console.
	 *
	 * @param printPerTurns The amount of turns before information is logged to the
	 *                      console.
	 */
	public void setPrintPerTurns(int printPerTurns) {
		this.printPerTurns = printPerTurns;
	}

	/**
	 * 
	 * @return Returns the maximum number of turns after which the particle tracking
	 *         ends. By default it is {@link Long#MAX_VALUE}
	 */
	public long getMaximumTurns() {
		return maximumTurns;
	}

	/**
	 * Set the maximum number of turns. After this number the observer will set the
	 * terminated flag and the tracking for the particle ends.
	 * 
	 * @param maximumTurns
	 */
	public void setMaximumTurns(long maximumTurns) {
		this.maximumTurns = maximumTurns;
	}

}
