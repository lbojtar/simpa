package simpa.acc.api.track;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.Beam;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.core.api.track.BeamLossObserver;
import simpa.core.api.track.Particle;
import simpa.core.api.track.ParticleTrackerTask;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.PathUtils;

/**
 * This class is for tracking a beam in a ring. It is a subclass of BeamTracker
 * The reason we need two classes is that the tracking in a ring is different
 * from the tracking in a line. In a ring for each particle we need to save the
 * phase space coordinates in a separate file. In a transfer line we save all
 * the phase space coordinates in one file for each observer position.
 */
public class RingBeamTracker extends BeamTracker {

	private static final Logger logger = LogManager.getLogger(RingBeamTracker.class);
	private BeamLossObserver lossObserver;
	private List<Double> phsObsLocations;

	public RingBeamTracker(Beam beam, int turns, double maxradius, double stepSize, int nThreads, boolean backward) throws IOException {
		super(nThreads);		
		phsObsLocations = new ArrayList<>();
		phsObsLocations.add(0.0); //default phs observer 
		setupTracking(beam, turns, maxradius, stepSize, backward);
	}

	public RingBeamTracker(Beam beam, int turns, double maxradius, double stepSize, int nThreads, boolean backward,
			List<Double> positions) throws IOException {
		super(nThreads);
		this.phsObsLocations = positions;
		setupTracking(beam, turns, maxradius, stepSize, backward);
	}

	public int getLossCount() {
		return lossObserver.getLossCount();
	}

	@Override
	protected void setupTracking(Beam beam, int turns, double maxradius, double stepSize, boolean backward)
			throws IOException {

		if (beam == null)
			throw new IllegalArgumentException("Beam  is null !");
		if (turns < 1)
			throw new IllegalArgumentException("Turn number must be at least 1, but it is: " + turns);

		trackerTasks = new ArrayList<>();
		lossObserver = new BeamLossObserver();

		String outDir = "./" + beam.getName() + "/";

		// check if the output directory outDir exist,if not create it
		if (!Files.isDirectory(Paths.get(outDir)))
			Files.createDirectories(Paths.get(outDir));

		List<String> existingFiles = FileUtils.scanDirectory(outDir);

		for (Particle particle : beam.getParticles()) {
			String file = particle.getName();

			if (existingFiles.contains(file) && FileUtils.countLines(outDir + file) >= turns) {
				logger.info("Skipping existing file: " + file);

			} else {

				TurnObserver to = new TurnObserver(beam.getReferenceOrbit(), maxradius, backward);
				to.setMaximumTurns(turns);
				to.setPrintPerTurns(10);
				ParticleTrackerTask task = new ParticleTrackerTask(beam.getPotentialProvider(), particle,
						Long.MAX_VALUE, stepSize, backward);

				
				for (Double pos : phsObsLocations) {
					ReferenceOrbit orbit = beam.getReferenceOrbit();
					int orbitIndex = PathUtils.getClosestIndex(orbit.getOrbitPoints(), pos, orbit.isCircular());
					PhaseSpaceObserver pho = new PhaseSpaceObserver(orbit, orbitIndex, maxradius,
							beam.getNominalMomentum(), backward);
			
					pho.setOutDirectory(outDir);
					task.addObserver(pho);
				}
				
				task.addObserver(to);			
				task.addObserver(lossObserver);
				trackerTasks.add(task);
			}
		}

	}

	@Override
	public void finish() {
		// TODO Auto-generated method stub

	}
}
