package simpa.acc.api.track;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.core.api.SystemConstants;
import simpa.core.api.track.Particle;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.FileUtils;

/**
 * A phase space observer which extends the {@link PhaseSpaceObserver} class.
 * This class is used to register the phase space coordinates of particles in a
 * beam hitting this observer. It is used by the {@link TLBeamTracker} class.
 * The phase space coordinates are written to a file when the tracking is
 * finished. This is done by @TLBeamTracker} class.
 */
public class TLPhaseSpaceObserver extends PhaseSpaceObserver {

	
	private Map<Particle, PhaseSpaceCoordinates> map = new HashMap<>();

	/**
	 * Constructor for a phase space observer in a transfer line.
	 * 
	 * @param longiPos        Longitudinal position from the beginning of the line
	 *                        [m].
	 * @param radius          Radius of the disk. Should be big enough to cover the
	 *                        aperture.
	 * @param orbit           The reference orbit
	 * @param nominalMomentum The nominal momentum [GeV/c]	
	 */
	public TLPhaseSpaceObserver(double longiPos, double radius, ReferenceOrbit orbit, double nominalMomentum,
			boolean backward) {
		super(longiPos, radius, nominalMomentum, orbit,backward);
	}

	@Override
	public synchronized void observe(Particle p) {
		Vector3D pos = new Vector3D(p.getX(), p.getY(), p.getZ());
		Vector3D previousPos = p.getPreviousPosition();
		Line l = new Line(pos, previousPos, SystemConstants.PLANE_AND_LINE_TOLERANCE);
		Vector3D is = getDisk().getPlane().intersection(l);
		// Check if the line going through the disk
		if (is != null && Math.abs(is.subtract(getDisk().getCenter()).getNorm()) < getDisk().getDiskRadius()) {
			// Check if the intersection point is between the current position and the
			// previous position
			double ds = Math.abs(pos.subtract(previousPos).getNorm());

			if (Math.abs(is.subtract(previousPos).getNorm()) < ds && Math.abs(is.subtract(pos).getNorm()) <= ds) {
				PhaseSpaceCoordinates c = getPhaseSpaceCoordinates(is, p);
				if (c != null) {
					map.put(p, c);
				}
			}
		}

	}

	@Override
	public void finish(Particle p) {
	}

	/**
	 * Write the phase space coordinates of the particle in a beam hitting this
	 * observer to a file.
	 * 
	 * @param file The file to write to.
	 */
	public void writeToFile(String file) {
		StringBuffer sb = new StringBuffer();
		sb.append(getHeader());
		for (Particle p : map.keySet()) {
			PhaseSpaceCoordinates c = map.get(p);
			sb.append(c.toString() + " " + p.getName() + System.lineSeparator());
		}
		FileUtils.writeTextFile(file, sb.toString());
	}

	public Map<Particle, PhaseSpaceCoordinates> getMap() {
		return map;
	}
	
	private String getHeader() {
		return "#x xp y yp z zp particleName" + System.lineSeparator() + "#LONGITUDINAL_POSITION=" + longiPos
				+ System.lineSeparator();
	}
}
