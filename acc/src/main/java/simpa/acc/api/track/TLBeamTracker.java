package simpa.acc.api.track;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.Beam;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.track.BeamLossObserver;
import simpa.core.api.track.BeamTrajectoryObserver;
import simpa.core.api.track.Particle;
import simpa.core.api.track.ParticleTrackerTask;

/**
 * This class is for tracking a beam in a line. It is a subclass of BeamTracker
 * The reason we need two classes is that the tracking in a ring is different
 * from the tracking in a line. In a ring for each particle we need to save
 * the phase space coordinates in a separate file. In a transfer line we save
 * all the phase space coordinates in one file for each observer position.
 */
public class TLBeamTracker extends BeamTracker {

	private static final Logger logger = LogManager.getLogger(TLBeamTracker.class);
	private BeamTrajectoryObserver trajObserver;
	private List<Double> phsObsLocations;
	private List<TLPhaseSpaceObserver> phsObservers;
	private BeamLossObserver lossObserver;
	private Beam beam;
	private boolean writePhsTofile = true;

	public TLBeamTracker(Beam beam, double maxradius, double stepSize, List<Double> phsObsLocations, boolean backward)
			throws IOException {
		super();
		if (backward)
			beam.setName(beam.getName() + "-backward");
		this.phsObsLocations = phsObsLocations;
		this.setupTracking(beam, 1, maxradius, stepSize, backward);
	}

	public TLBeamTracker(Beam beam, double maxradius, double stepSize, int nThreads, List<Double> phsObsLocations,
			boolean backward)
			throws IOException {
		super(nThreads);
		if (backward)
			beam.setName(beam.getName() + "-backward");
		this.phsObsLocations = phsObsLocations;
		this.setupTracking(beam, 1, maxradius, stepSize, backward);
	}

	/**
	 * 
	 * @param writePhsTofile If true phase space files and lost particle files are written to disk. True by
	 *                       default.
	 */
	public void setWritePhsTofile(boolean writePhsTofile) {
		this.writePhsTofile = writePhsTofile;
	}

	/**
	 * 
	 * @return Number of lost particles.
	 */
	public int getLossCount() {
		return lossObserver.getLossCount();
	}

	@Override
	protected void setupTracking(Beam beam, int turns, double maxradius, double stepSize, boolean backward)
			throws IOException {

		if (beam == null)
			throw new IllegalArgumentException("Beam  is null !");
		this.beam = beam;

		trackerTasks = new ArrayList<>();
		lossObserver = new BeamLossObserver();
		phsObservers = new ArrayList<>();

		for (Double pos : phsObsLocations) {
			TLPhaseSpaceObserver pho = new TLPhaseSpaceObserver(pos, maxradius, beam.getReferenceOrbit(),
					beam.getNominalMomentum(), backward);
			phsObservers.add(pho);
		}

		for (Particle particle : beam.getParticles()) {

			ParticleTrackerTask task = new ParticleTrackerTask(beam.getPotentialProvider(), particle, Long.MAX_VALUE,
					stepSize, backward);

			for (TLPhaseSpaceObserver pho : phsObservers)
				task.addObserver(pho);

			task.addObserver(lossObserver);

			TurnObserver to = new TurnObserver(beam.getReferenceOrbit(), maxradius, backward);
			to.setMaximumTurns(turns);
			to.setPrintPerTurns(10);
			task.addObserver(to);

			trackerTasks.add(task);

		}

	}

	/**
	 * @return The phase space observers.
	 */
	public List<TLPhaseSpaceObserver> getPhaseSpaceObservers() {
		return phsObservers;
	}

	/**
	 * Called after the tracking is finished by the {@link BeamTracker } track()
	 * Writes the output files.
	 */
	@Override
	public void finish() {
		logger.info("Number of particles lost: " + lossObserver.getLossCount());
		
		if (writePhsTofile) {
			if (lossObserver.getLossCount() > 0)
				lossObserver.writeToFile(FileNamingConventions.getBeamLossFileName(beam.getName()));

			for (TLPhaseSpaceObserver pho : phsObservers) {
				String file = FileNamingConventions.getTLBeamPhaseSpaceFileName(beam.getName(),
						pho.getLongitudinalPosition());
				pho.writeToFile(file);
			}
		}

		if (trajObserver != null)
			trajObserver.writeToFile(FileNamingConventions.getBeamTrajectoryFileName(beam.getName()));

	}

	/**
	 * Adds a beam trajectory observer to all tracker tasks.
	 * 
	 * @param bto The observer to add.
	 */
	public void addTrajectoryObserver(BeamTrajectoryObserver bto) {
		for (ParticleTrackerTask task : trackerTasks) {
			task.addObserver(bto);
		}
		this.trajObserver = bto;
	}
}
