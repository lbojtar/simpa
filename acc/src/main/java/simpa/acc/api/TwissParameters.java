package simpa.acc.api;

import java.io.IOException;

import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.core.api.track.PhaseSpaceCoordinates;

/**
 * Record to hold the Twiss parameters. The constructor should not be called
 * directly, because this record likely to be extended. Use the builder() to
 * construct an instance.
 * 
 * @author lbojtar
 *
 */
public record TwissParameters(double longiPos, PhaseSpaceEllipse hEllipse, PhaseSpaceEllipse vEllipse, double muH,
		double muV, double dispH, double dispV, double dispPrimeH, double dispPrimeV, double dpOp) {

	private static final Logger logger = LogManager.getLogger(TwissParameters.class);

	public static Builder builder() {
		return new Builder();
	}

	//@formatter:off
	/**
	 * Gets the 5D phase space coordinates on the phase space ellipse defined by the
	 * given emittances, phase and the Twiss parameters of this instance. The longitudinal z
	 * coordinate is zero, but z' which is the dpOp is taken into account.
	 * The phase space coordinates are calculated for example in the H plane as:
	 *  
	 * x  = a * sin(hPhi)+Cx
	 * x' = b * cos(hPhi)+Cy
	 *	
	 * where a and b are the radiuses of the ellipse and Cx and Cy are the coordinates
	 * of the center point in the phase space.
	 *	
	 * @param hEm  Hor. emittance [pi*mm*mrad]
	 * @param hPhi Angle for the H plane. [radians]
	 * @param vEm  Vert. emittance [pi*mm*mrad]
	 * @param vPhi Angle for the H plane. [radians]
	 * @param z   Longitudinal position [m] phase space coordinate.
	 * @param dpOp  Relative momentum deviation from the reference momentum.
	 * @return The phase space coordinates.
	 */
	//@formatter:on
	public PhaseSpaceCoordinates getPhaseSpaceCoordinates(double hEm, double hPhi, double vEm, double vPhi, double z,
			double dpOp) {

		Vector2D hp = PhaseSpaceEllipse.getEllipse(hEm, hEllipse.alpha(), hEllipse.beta(),
				hEllipse.pos() + dispH * dpOp, hEllipse.angle() + dispPrimeH * dpOp).getPointOnEllipse(hPhi);
		Vector2D vp = PhaseSpaceEllipse.getEllipse(vEm, vEllipse.alpha(), vEllipse.beta(),
				vEllipse.pos() + dispV * dpOp, vEllipse.angle() + dispPrimeV * dpOp).getPointOnEllipse(vPhi);

		return new PhaseSpaceCoordinates(hp.getX(), hp.getY(), vp.getX(), vp.getY(), z, dpOp);

	}

	/**
	 * Prints the given Twiss parameters
	 * 
	 * @param twp The Twiss parameters
	 * @throws IOException
	 */
	public static void printTwiss(TwissParameters twp) throws IOException {
		PhaseSpaceEllipse he = twp.hEllipse();
		PhaseSpaceEllipse ve = twp.vEllipse();

		logger.info("Hor. Em= " + he.em() + "[ m ]");
		logger.info("Hor. beta= " + he.beta());
		logger.info("Hor. alpha= " + he.alpha());
		logger.info("Hor. position= " + he.pos());
		logger.info("Hor. angle= " + he.angle() + System.lineSeparator());

		logger.info("Vert. Em= " + ve.em() + "[ m ]");
		logger.info("Vert. beta= " + ve.beta());
		logger.info("Vert. alpha= " + ve.alpha());
		logger.info("Vert. position= " + ve.pos());
		logger.info("Vert. angle= " + ve.angle() + System.lineSeparator());
	}

	/**
	 * Header info, useful when writing this object into a file
	 * 
	 * @param withBeamSize If true, the beam size will be included in the header.
	 * @return The header string.
	 */
	public static String getHeaderString(boolean withBeamSize) {
		String s = "#1:longiPos 2:hPos 3:vPos 4:hAngle 5:vAngle 6:alphaH 7:alphaV  8:betaH  9:betaV 10:muH  11:muV 12:dispH 13:dispV 14:dispPrimeH 15:dispPrimeV";
		if (withBeamSize)
			s = s + " 16:sigmaH 17:sigmaV";
		s = s + System.lineSeparator();
		return s;
	}

	/**
	 * 
	 * @return The horizontal beam size in [m] .If the dpOp is not set, it will
	 *         return
	 *         infinity.
	 */
	public double getSigmaH() {
		return Math.sqrt(hEllipse.beta() * hEllipse.em()) + dispH * dpOp;
	}

	/**
	 * 
	 * @return The vertical beam size in [m] .If the dpOp is not set, it will return
	 *         infinity.
	 */
	public double getSigmaV() {
		return Math.sqrt(vEllipse.beta() * vEllipse.em()) + dispV * dpOp;
	}

	public String toString() {

		String s = longiPos + " " + hEllipse.pos() + " " + vEllipse.pos() + " " + hEllipse.angle() + " "
				+ vEllipse.angle() + " " + hEllipse.alpha() + " " + vEllipse.alpha() + " " + hEllipse.beta() + " "
				+ vEllipse.beta() + " " + muH + " " + muV + " " + dispH + " " + dispV + " " + dispPrimeH + " "
				+ dispPrimeV;

		if (dpOp != Double.POSITIVE_INFINITY) {
			s += " " + getSigmaH() + " " + getSigmaV();
		}
		s = s + System.lineSeparator();
		return s;
	}

	// Builder
	public final static class Builder {

		double longiPos = Double.POSITIVE_INFINITY;// to indicate it is not set
		double dispH = Double.POSITIVE_INFINITY;// to indicate it is not set
		double dispV = Double.POSITIVE_INFINITY;// to indicate it is not set
		double dispPrimeH = Double.POSITIVE_INFINITY;// to indicate it is not set
		double dispPrimeV = Double.POSITIVE_INFINITY;// to indicate it is not set
		double muH = Double.POSITIVE_INFINITY;// to indicate it is not set
		double muV = Double.POSITIVE_INFINITY;// to indicate it is not set

		double dpOp = 0.0; //default value, it is not always needed.

		PhaseSpaceEllipse hEllipse, vEllipse;

		public Builder longiPos(double lp) {
			this.longiPos = lp;
			return this;
		}

		public Builder muH(double muH) {
			this.muH = muH;
			return this;
		}

		public Builder hEllipse(PhaseSpaceEllipse hEllipse) {
			this.hEllipse = hEllipse;
			return this;
		}

		public Builder vEllipse(PhaseSpaceEllipse vEllipse) {
			this.vEllipse = vEllipse;
			return this;
		}

		public Builder muV(double muV) {
			this.muV = muV;
			return this;
		}

		public Builder dispH(double dispH) {
			this.dispH = dispH;
			return this;
		}

		public Builder dispV(double dispV) {
			this.dispV = dispV;
			return this;
		}

		public Builder dispPrimeH(double dispPrimeH) {
			this.dispPrimeH = dispPrimeH;
			return this;
		}

		public Builder dispPrimeV(double dispPrimeV) {
			this.dispPrimeV = dispPrimeV;
			return this;
		}

		/**
		 * Copy the given Twiss parameters to this builder.
		 * 
		 * @param tp The Twiss parameters to be copied.
		 * @return The builder with the copied Twiss parameters.
		 */
		public Builder copyOf(TwissParameters tp) {
			this.hEllipse = tp.hEllipse.copy();
			this.vEllipse = tp.vEllipse.copy();
			this.longiPos = tp.longiPos;
			this.muH = tp.muH;
			this.muV = tp.muV;
			this.dispH = tp.dispH;
			this.dispV = tp.dispV;
			this.dispPrimeH = tp.dispPrimeH;
			this.dispPrimeV = tp.dispPrimeV;
			this.dpOp = tp.dpOp;

			return this;
		}

		/**
		 * Sets the relative momentum deviation from the reference momentum. This is
		 * used in the calculation of the beam size. It is not always needed, so it can
		 * be left not set.
		 * 
		 * @param dpOp relative momentum deviation from the reference momentum.
		 * @return this
		 */
		public Builder dpOp(double dpOp) {
			this.dpOp = dpOp;
			return this;
		}

		public TwissParameters build() {

			if (hEllipse == null)
				throw new IllegalStateException("hEllipse is not set");
			if (vEllipse == null)
				throw new IllegalStateException("vEllipse is not set");
			if (muH == Double.POSITIVE_INFINITY)
				throw new IllegalStateException("muH is not set");
			if (muV == Double.POSITIVE_INFINITY)
				throw new IllegalStateException("muV is not set");
			if (dispH == Double.POSITIVE_INFINITY)
				throw new IllegalStateException("dispH is not set");
			if (dispV == Double.POSITIVE_INFINITY)
				throw new IllegalStateException("dispV is not set");
			if (dispPrimeH == Double.POSITIVE_INFINITY)
				throw new IllegalStateException("dispPrimeH is not set");
			if (dispPrimeV == Double.POSITIVE_INFINITY)
				throw new IllegalStateException("dispPrimeV is not set");

			return new TwissParameters(longiPos, hEllipse, vEllipse, muH, muV, dispH, dispV, dispPrimeH, dispPrimeV,
					dpOp);

		}

	}
}
