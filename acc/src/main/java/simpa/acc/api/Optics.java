package simpa.acc.api;

import java.util.List;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.utils.OpticsCalculator;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.PathUtils;

/**
 * This class holds optics data calculated by the {@link OpticsCalculator}. It
 * it is a list of {@link TwissParameters} and a {@link ReferenceOrbit}.
 * 
 * @author lbojtar
 *
 */
public class Optics {

	private List<TwissParameters> twissPars;
	private ReferenceOrbit refOrbit;

	//global parameters
	private double maxHorBeta = Double.MIN_VALUE;
	private double maxVertBeta = Double.MIN_VALUE;
	private double maxHorDisp = Double.MIN_VALUE;
	private double maxVertDisp = Double.MIN_VALUE;
	private double minHorDisp = Double.MAX_VALUE;
	private double minVertDisp = Double.MAX_VALUE;
	private double maxHorAbsPos = Double.MIN_VALUE;
	private double maxVertAbsPos = Double.MIN_VALUE;

	/**
	 * Constructor for Optics .
	 * 
	 * @param twissPars List of all Twiss Parameters
	 * @param refOrbit  The reference orbit.
	 */
	public Optics(List<TwissParameters> twissPars, ReferenceOrbit refOrbit) {
		this.twissPars = twissPars;
		this.refOrbit = refOrbit;
		calcMaxValues();
	}

	/**
	 * 
	 * @return List Twiss parameters at all calculated longitudinal positions.
	 */
	public List<TwissParameters> getAllTwissParametrs() {
		return twissPars;
	}

	/**
	 * 
	 * @return Maximum value of the horizontal beta along the sequence.
	 */
	public double getMaxHorBeta() {
		return maxHorBeta;
	}

	/**
	 * 
	 * @return Maximum value of the vertical beta along the sequence.
	 */
	public double getMaxVertBeta() {
		return maxVertBeta;
	}

	/**
	 * 
	 * @return Maximum value of the horizontal dispersion along the sequence.
	 */
	public double getMaxHorDisp() {
		return maxHorDisp;
	}

	/**
	 * 
	 * @return Maximum value of the vertical dispersion along the sequence.
	 */
	public double getMaxVertDisp() {
		return maxVertDisp;
	}

	/**
	 * 
	 * @return Minimum value of the horizontal dispersion along the sequence. Useful
	 *         when matching negative dispersion.
	 */
	public double getMinHorDisp() {
		return minHorDisp;
	}

	/**
	 * 
	 * @return Minimum value of the vertical dispersion along the sequence. Useful
	 *         when matching negative dispersion.
	 */
	public double getMinVertDisp() {
		return minVertDisp;
	}

	/**
	 * Writes the Optics to a file, but only the Twiss parameters.
	 * 
	 * @param outFileName The name of the output file.
	 */
	public void writeToFile(String outFileName) {

		StringBuilder sb = new StringBuilder();

		String header = TwissParameters.getHeaderString(true);
		for (TwissParameters tw : twissPars) {
			sb.append(tw.toString());
		}

		FileUtils.writeTextFile(outFileName, header + sb);

	}

	/**
	 * Gets the Twiss parameter closest to the given longitudinal position.
	 * 
	 * @param longiPos Longitudinal position.
	 * @return Twiss parameters
	 */
	public TwissParameters getTwissParametersAt(double longiPos) {
		int idx = PathUtils.getClosestIndex(refOrbit.getOrbitPoints(), longiPos, refOrbit.isCircular());
		return twissPars.get(idx);
	}

	private void calcMaxValues() {
		for (TwissParameters p : twissPars) {
			if (p.hEllipse().beta() > maxHorBeta)
				maxHorBeta = p.hEllipse().beta();

			if (p.vEllipse().beta() > maxVertBeta)
				maxVertBeta = p.vEllipse().beta();

			if (p.dispH() > maxHorDisp)
				maxHorDisp = p.dispH();

			if (p.dispV() > maxVertDisp)
				maxVertDisp = p.dispV();

			if (p.dispH() < minHorDisp)
				minHorDisp = p.dispH();

			if (p.dispV() < minVertDisp)
				minVertDisp = p.dispV();
			
			if (Math.abs(p.hEllipse().pos() ) > maxHorAbsPos)
				maxHorAbsPos= Math.abs(p.hEllipse().pos() );
			
			if (Math.abs(p.vEllipse().pos() ) > maxVertAbsPos)
				maxVertAbsPos= Math.abs(p.vEllipse().pos() );
		}
	}

	public double getMaxHorAbsPos() {
		return maxHorAbsPos;
	}

	public double getMaxVertAbsPos() {
		return maxVertAbsPos;
	}
}
