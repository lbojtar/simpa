package simpa.acc.api;

import java.util.ArrayList;
import java.util.List;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.utils.EllipseBeamSampler;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.track.Particle;

/**
 * Special beam mainly for optics calculations. All particles lie on the same
 * phase space ellipse.
 * 
 * @author lbojtar
 *
 */
public class EllipseBeam extends Beam {

	private EllipseBeamSampler beamSampler;

	public EllipseBeam(EllipseBeamSampler beamSampler, double nomMomentum, ReferenceOrbit refOrbit, double longiPos,
			PotentialProvider potProvider, double mass, double charge, int nbOfSamples, String name)
			throws KnownFatalException {

		super(beamSampler, nomMomentum, refOrbit, longiPos, potProvider, mass, charge, nbOfSamples, name);
		this.beamSampler = beamSampler;
		if (nbOfSamples < 6)
			throw new IllegalArgumentException("At least 6 particles are needed to define an ellipse");

	}

	public EllipseBeam(List<Particle> particles, ReferenceOrbit refOrbit, PotentialProvider potProvider, String name) {
		super(particles, refOrbit, potProvider, name);

		if (particles.size() < 6)
			throw new IllegalArgumentException("At least 6 particles are needed to define an ellipse");
	}

	/**
	 * 
	 * @return A Deep copy of of all particles in the beam, but the tracking state
	 *         is freshly created. The vector potential of the copy will be the
	 *         potential provider. The beam sampler is shallow copied. This method
	 *         is tipically used to create a new beam with the same particles for
	 *         new tracking with a different potential provider.
	 * 
	 */
	@Override
	public Beam copy(Beam b, PotentialProvider pp) {
		List<Particle> l = new ArrayList<>();
		for (Particle p : b.getParticles()) {
			l.add(p.copy(p, pp));
		}

		EllipseBeam c = new EllipseBeam(l, b.getReferenceOrbit(), pp, b.getName());
		c.beamSampler = this.getBeamSampler();
		return c;
	}

	/**
	 * 
	 * @return The beam sampler used to generate the particles in the beam. It might
	 *         be null if the beam was created from a list of particles and nor
	 *         using the constructor with the beam sampler.
	 */
	public EllipseBeamSampler getBeamSampler() {
		return beamSampler;
	}

}
