package simpa.acc.api.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.Optics;
import simpa.acc.api.ParticleFactory;
import simpa.acc.api.Tune;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.track.PhaseSpaceObserver;
import simpa.acc.api.track.TurnObserver;
import simpa.acc.impl.TuneTwissInterpolator;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.PotentialProvider;
import simpa.core.api.SHCombiner;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.Aperture;
import simpa.core.api.track.ParallelTracker;
import simpa.core.api.track.Particle;
import simpa.core.api.track.ParticleTrackerTask;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.CalculatorUtils;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.PathUtils;

public class FrequencyAnalysis {

	private static final Logger logger = LogManager.getLogger(FrequencyAnalysis.class);

	private ParallelTracker parallelTracker;
	private Particle prototypeParticle;
	private String outDir;
	private TuneModel tuneModel;
	private ReferenceOrbit referenceOrbit;
	private Map<String, Double> scalings;
	private double startQh, endQh, startQv, endQv, hEmittance, vEmittance, diskRadius, dpOp;
	private int stepsQh, stepsQv;
	private int turns;
	private List<Tune> sampleTunes;
	private double geVoc, mass, charge;

	/**
	 * Creates a {@link FrequencyAnalysisBuilder} object to set up the parameters
	 * for the frequency analysis.
	 * 
	 * @return The builder object.
	 */
	public static FrequencyAnalysisBuilder builder() {
		return new FrequencyAnalysisBuilder();
	}

	private FrequencyAnalysis(FrequencyAnalysisBuilder b) throws IOException {
		this.referenceOrbit = b.referenceOrbit;
		this.prototypeParticle = b.prototypeParticle;
		this.outDir = b.outDir;
		this.scalings = b.scalings;
		this.tuneModel = b.tuneModel;
		this.sampleTunes = b.sampleTunes;
		this.hEmittance = b.hEmittance;
		this.vEmittance = b.vEmittance;
		this.startQh = b.startQh;
		this.endQh = b.endQh;
		this.startQv = b.startQv;
		this.endQv = b.endQv;
		this.stepsQh = b.stepsQh;
		this.stepsQv = b.stepsQv;
		this.diskRadius = b.diskRadius;
		this.turns = b.turns;
		this.dpOp = b.dpOp;
		this.geVoc = CalculatorUtils.getMomentumInGeVoC(prototypeParticle.getKineticMomentum().getNorm());
		this.mass = prototypeParticle.getMass();
		this.charge = prototypeParticle.getCharge();

		parallelTracker = new ParallelTracker(b.nThreads);

	}

	/**
	 * Executes the tune scan by tracking particles for the given number of turns
	 * for each point in the tune diagram. The output files containing the phase
	 * space coordinates for each particle will be written into the output
	 * directory.
	 * 
	 * @param stepSize The step size for the tracking.
	 * 
	 * @throws IOException
	 * @throws OutOfApertureException
	 */
	public void scanTunes2D(double stepSize) throws IOException, KnownFatalException, OutOfApertureException {

		TuneTwissInterpolator tuneTwissInterpolator = prepareInterpolator(stepSize);

		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(4);

		double dQh = (endQh - startQh) / stepsQh;
		double dQv = (endQv - startQv) / stepsQv;

		ParticleFactory pf = new ParticleFactory(referenceOrbit);
		SHCombiner combiner = SHCombiner.getUniqueInstance();

		new File(outDir).mkdir();

		for (int idx1 = 0; idx1 < stepsQh; idx1++) {
			for (int idx2 = 0; idx2 < stepsQv; idx2++) {
				double qh = startQh + idx1 * dQh;
				double qv = startQv + idx2 * dQv;
				Tune tune = new Tune(qh, qv);
				TwissParameters twp = tuneTwissInterpolator.getTwissParameters(tune);
				double longiPos = twp.longiPos();
				PhaseSpaceCoordinates phs = twp.getPhaseSpaceCoordinates(hEmittance, 0, vEmittance, 0, 0, 0);

				Map<String, Double> tuneElementsMap = tuneModel.getScalingFactors(tune, geVoc);
				updateMap(tuneElementsMap);
				logger.info("Preparing field map for Qh: " + qh + " Qv: " + qv);

				PotentialProvider pp = combiner.getEvaluator(scalings);

				Particle p;

				try {

					p = pf.getParticle(geVoc, phs, longiPos, pp, mass, charge);
					String name = nf.format(qh) + "_" + nf.format(qv);
					p.setName(name);
					track(p, turns, stepSize, pp);
				} catch (OutOfApertureException e) {
					logger.error(
							"The initial position of the particle is out of aperture, you may need to reduce the emittances.");
				}
			}
		}
	}

	/**
	 * Calculates the diffusion coefficients for each tracking file. This file is
	 * the main output of frequency analysis. It is usually plotted as a heat map
	 * plot.
	 * 
	 * @param resultFile The output file with the estimated and precisely calculated
	 *                   fractional tunes and the diffusion coefficients calculated
	 *                   from them.
	 * 
	 * @throws FileNotFoundException
	 */
	public void calculate(String resultFile) throws FileNotFoundException {
		File[] files = new File(outDir).listFiles();

		int halfOfTurns = turns / 2;

		StringBuffer sb = new StringBuffer();
		sb.append("Model_qh Model_qv qh qv diffusion" + System.lineSeparator());
		for (File file : files) {
			if (file.isFile()) {
				System.out.println("Processing " + file.getName());
				StringTokenizer st = new StringTokenizer(file.getName(), "_");

				// these tunes are estimated by the TuneModel implementation
				double qhExpected = Double.parseDouble(st.nextToken());
				StringTokenizer st2 = new StringTokenizer(st.nextToken(),
						FileNamingConventions.PHASE_SPACE_FILE_EXTENSION);
				double qvExpected = Double.parseDouble(st2.nextToken());

				TuneCalculator tc = new TuneCalculator(file.getAbsolutePath());
				double qh1 = tc.getHorizontalTune(0, halfOfTurns);
				double qh2 = tc.getHorizontalTune(halfOfTurns, halfOfTurns);
				double qv1 = tc.getVerticalTune(0, halfOfTurns);
				double qv2 = tc.getVerticalTune(halfOfTurns, halfOfTurns);
				double dqh = qh2 - qh1;
				double dqv = qv2 - qv1;
				double diff = Math.sqrt(dqh * dqh + dqv * dqv);
				sb.append(qhExpected + " " + qvExpected + " " + qh1 + " " + qv1 + " " + diff + System.lineSeparator());
			}
		}
		FileUtils.writeTextFile(resultFile, sb.toString());
	}

	// A builder class for FrequencyAnalysis
	public static class FrequencyAnalysisBuilder {

		private String outDir;
		private TuneModel tuneModel;
		private ReferenceOrbit referenceOrbit;
		private Map<String, Double> scalings;
		private double startQh, endQh, startQv, endQv, hEmittance, vEmittance, diskRadius, dpOp;
		private int stepsQh, stepsQv;
		private int nThreads = Runtime.getRuntime().availableProcessors();
		private int turns = 256;
		private Aperture aperture;
		private List<Tune> sampleTunes;
		private Particle prototypeParticle;

		/**
		 * @param dpop The momentum spread to be used while determining the optics for
		 *             the interpolation of optics parameters.
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setDpOp(double dpop) {
			this.dpOp = dpop;
			return this;
		}

		/**
		 * This method is used for setting a list of tunes corresponding to resonance
		 * free working points in the tune diagram. The tunes are used to calculate an
		 * interpolation table which is used to calculate the initial conditions for the
		 * tracking, such that the initial single particle emittances are equal to the
		 * specified values approximately.
		 *
		 * @param sampleTunes A list of tunes corresponding to resonance free working
		 *                    points in the tune diagram
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setSampleTunes(List<Tune> sampleTunes) {
			this.sampleTunes = sampleTunes;
			return this;
		}

		/**
		 * @param aperture The physical aperture to be used during the pracking.
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setAperture(Aperture aperture) {
			this.aperture = aperture;
			return this;
		}

		/**
		 * @param prototypeParticle A prototype for particles to be tracked. The phase
		 *                          space variables will be used only at the stage of
		 *                          constructing the Tune- TwissParameters interpolation
		 *                          table.
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setPrototypeParticle(Particle prototypeParticle) {
			this.prototypeParticle = prototypeParticle;
			return this;
		}

		/**
		 * @param turns Number of turns to be used for the tracking. Must be a power of
		 *              two. The default is 256.
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setTurns(int turns) {
			this.turns = turns;
			return this;
		}

		/**
		 * By default all threads are used. Since the scan must keep a field map in
		 * memory for each thread, the memory consumption can be very high. This method
		 * can be used to keep memory consumption within the available memory size.
		 * 
		 * @param nThreads Number of threads to be used during the scan.
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setNumberOfThreadsUsed(int nThreads) {
			this.nThreads = nThreads;
			return this;
		}

		/**
		 * Sets the output directory for the phase space coordinate files produced by
		 * the tracking during the tune scan.
		 * 
		 * @param outdir The output directory
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setOutDir(String outdir) {
			this.outDir = outdir;
			return this;
		}

		/**
		 * Set the disk radius for the observer disk. Should be big enough to cover the
		 * aperture of the ring, but small enough to not intersect the beam region
		 * twice.
		 * 
		 * @param diskRadius - The radius of the observer disk.
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setDiskRadius(double diskRadius) {
			this.diskRadius = diskRadius;
			return this;
		}

		/**
		 * Sets the reference orbit of the ring.
		 * 
		 * @param referenceOrbit The reference orbit
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setReferenceOrbit(ReferenceOrbit referenceOrbit) {
			this.referenceOrbit = referenceOrbit;
			return this;
		}

		/**
		 * Sets an the tune model to be used for the frequency analysis.
		 * 
		 * @param tuneModel An implementation of the {@link TuneModel} interface.
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setTuneModel(TuneModel tuneModel) {
			this.tuneModel = tuneModel;
			return this;
		}

		/**
		 * Set the scaling map containing the elements in the ring. During the tune scan
		 * the scaling factor of the elements which are part of the {@link TuneModel}
		 * will be replaced by the scaling factors given by the {@link TuneModel}, so
		 * their value is irrelevant ( you can set to zero for example). However these
		 * elements should be also present in the map. You can save quite some memory to
		 * prepare a field map containing all elements except the elements in the
		 * {@link TuneModel} already scaled properly and add this field map to the
		 * scaling map with scaling factor one.
		 * 
		 * @param scalings Field map containing all elements.
		 * 
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setScalings(Map<String, Double> scalings) {
			this.scalings = scalings;
			return this;
		}

		/**
		 * @param startQh The staring horizontal tune of the tune scan. This can be the
		 *                fractional of the full tune, depending how the
		 *                {@link TuneModel} is implemented.
		 * 
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setStartQh(double startQh) {
			this.startQh = startQh;
			return this;
		}

		/**
		 * @param endQh The end horizontal tune of the tune scan.This can be the
		 *              fractional of the full tune, depending how the {@link TuneModel}
		 *              is implemented.
		 * 
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setEndQh(double endQh) {
			this.endQh = endQh;
			return this;
		}

		/**
		 * @param startQv The staring vertical tune of the tune scan. This can be the
		 *                fractional of the full tune, depending how the
		 *                {@link TuneModel} is implemented.
		 * 
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setStartQv(double startQv) {
			this.startQv = startQv;
			return this;
		}

		/**
		 * @param endQv The staring vertical tune of the tune scan. This can be the
		 *              fractional of the full tune, depending how the {@link TuneModel}
		 *              is implemented.
		 * 
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setEndQv(double endQv) {
			this.endQv = endQv;
			return this;
		}

		/**
		 * @param stepsQh Number of horizontal steps during the tune scan.
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setStepsQh(int stepsQh) {
			this.stepsQh = stepsQh;
			return this;
		}

		/**
		 * @param stepsQv Number of vertical steps during the tune scan.
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setStepsQv(int stepsQv) {
			this.stepsQv = stepsQv;
			return this;
		}

		/**
		 * @param hEmittance The the single particle horizontal emittance for the tune
		 *                   scan. This value will be kept approximately constant during
		 *                   the tune scan.
		 * 
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setHEmittance(double hEmittance) {
			this.hEmittance = hEmittance;
			return this;
		}

		/**
		 * @param vEmittance The the single particle vertical emittance for the tune
		 *                   scan.This value will be kept approximately constant during
		 *                   the tune scan.
		 * 
		 * @return The builder object.
		 */
		public FrequencyAnalysisBuilder setVEmittance(double vEmittance) {
			this.vEmittance = vEmittance;
			return this;
		}

		/**
		 * Builds the FrequencyAnalysis instance.
		 * 
		 * @return The FrequencyAnalysis instance.
		 * @throws IOException
		 */
		public FrequencyAnalysis build() throws IOException {
			if (dpOp == 0)
				throw new IllegalArgumentException("The dpOp must be set");
			if (sampleTunes == null)
				throw new IllegalArgumentException("The sample tunes must be set");
			if (aperture == null)
				throw new IllegalArgumentException("The aperture must be set");
			if (diskRadius <= 0)
				throw new IllegalArgumentException("diskRadius must be set to a positive number");
			if (prototypeParticle == null)
				throw new IllegalArgumentException("prototypeParticle must be set");
			if (referenceOrbit == null)
				throw new IllegalArgumentException("referenceOrbit must be set");
			if (outDir == null)
				throw new IllegalArgumentException("outDir must be set");
			if (tuneModel == null)
				throw new IllegalArgumentException("tuneModel must be set");
			if (scalings == null)
				throw new IllegalArgumentException("The scaling map of elements must be set");
			if (startQh == 0)
				throw new IllegalArgumentException("startQh must be set");
			if (endQh == 0)
				throw new IllegalArgumentException("endQh must be set");
			if (startQv == 0)
				throw new IllegalArgumentException("startQv must be set");
			if (endQv == 0)
				throw new IllegalArgumentException("endQv must be set");
			if (stepsQh == 0)
				throw new IllegalArgumentException("stepsQh must be set");
			if (stepsQv == 0)
				throw new IllegalArgumentException("stepsQv must be set");
			if (hEmittance <= 0)
				throw new IllegalArgumentException("hEmittance must be set to a positive value");
			if (vEmittance <= 0)
				throw new IllegalArgumentException("vEmittance must be set to a positive value");
			if (startQh > endQh)
				throw new IllegalArgumentException("startQh must be less than endQh");
			if (startQv > endQv)
				throw new IllegalArgumentException("startQv must be less than endQv");
			if (stepsQh < 1)
				throw new IllegalArgumentException("stepsQh must be greater than 1");
			if (stepsQv < 1)
				throw new IllegalArgumentException("stepsQv must be greater than 1");

			return new FrequencyAnalysis(this);
		}

	}

	private void updateMap(Map<String, Double> tuneElementsMap) {
		// replace the scaling of the elements with the new values
		for (String elementName : tuneElementsMap.keySet()) {
			double scaling = tuneElementsMap.get(elementName);
			scalings.replace(elementName, scaling);
		}
	}

	private TuneTwissInterpolator prepareInterpolator(double stepSize)
			throws IOException, OutOfApertureException, KnownFatalException {

		double momNorm = prototypeParticle.getKineticMomentum().getNorm();
		Vector3D pk = prototypeParticle.getKineticMomentum();

		PhaseSpaceCoordinates phs0 = new PhaseSpaceCoordinates(prototypeParticle.getX(), pk.getX() / momNorm,
				prototypeParticle.getY(), pk.getY() / momNorm, 0, 0);

		PhaseSpaceCoordinates phs1 = new PhaseSpaceCoordinates(prototypeParticle.getX(), pk.getX() / momNorm,
				prototypeParticle.getY(), pk.getY() / momNorm, 0, dpOp);

		int idx = PathUtils.getClosestIndex(referenceOrbit.getOrbitPoints(),
				new Vector3D(prototypeParticle.getX(), prototypeParticle.getY(), prototypeParticle.getZ()));
		double longiPos = referenceOrbit.getLongitudinalPosition(idx);

		Map<Tune, TwissParameters> twissMap = new HashMap<>();

		OpticsCalculator oc = new OpticsCalculator(referenceOrbit);

		for (Tune tune : sampleTunes) {
			logger.info("Preparing tune-Twiss interpolator for tune " + tune);
			Map<String, Double> tuneElementsMap = tuneModel.getScalingFactors(tune, geVoc);
			updateMap(tuneElementsMap);
			PotentialProvider pp = SHCombiner.getUniqueInstance().getEvaluator(scalings);
			ParticleFactory pf = new ParticleFactory(referenceOrbit);
			Particle p0 = pf.getParticle(geVoc, phs0, longiPos, pp, mass, charge);
			Particle p1 = pf.getParticle(geVoc, phs1, longiPos, pp, mass, charge);
			p0.setName("p0");
			p1.setName("p1");
			Optics opt = null;
			try {
				opt = oc.trackInRingAndCalculate(p0, p1, pp, stepSize, diskRadius,false);
			} catch (IOException | OpticsCalculationException e) {
				logger.error(e.toString());
				System.exit(-1);
			}
			twissMap.put(tune, opt.getTwissParametersAt(longiPos));
		}

		return new TuneTwissInterpolator(twissMap);

	}

	private void track(Particle p, int turns, double stepSize, PotentialProvider pp) {

		ParticleTrackerTask task = new ParticleTrackerTask(pp, p, Long.MAX_VALUE, stepSize, false);

		PhaseSpaceObserver pho = new PhaseSpaceObserver(referenceOrbit, 0, diskRadius, geVoc, false);

		pho.setOutDirectory(outDir);

		TurnObserver to = new TurnObserver(referenceOrbit, diskRadius, false);
		to.setPrintPerTurns(turns);// to reduce logger output
		to.setMaximumTurns(turns);

		task.addObserver(to);
		task.addObserver(pho);

		parallelTracker.addToQueue(task);
	}
}
