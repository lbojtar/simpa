package simpa.acc.api.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.impl.BeamIntersectionCalculatorImpl;
import simpa.acc.impl.IntersectionCalculatorImpl;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.track.ObserverDisk;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.track.TrajectoryData;

/**
 * Interface for the IntersectionCalculator implementations. The implementation
 * of this interface takes trajectory data of a single particle in a ring or a
 * beam in a transfer line and provides method to calculate intersections with
 * disks placed along the reference orbit provided. From the intersections the
 * phase space coordinates are calculated.
 * 
 * @author lbojtar
 *
 */
public interface IntersectionCalculator {

	/**
	 * Gets an implementation of this interface. The file name parameter must follow
	 * the {@link FileNamingConventions}, since the instance returned depends on the
	 * file name.
	 * 
	 * @param fileName    A file name for a single particle or a beam trajectory
	 *                    according to the {@link FileNamingConventions}.
	 * @param designOrbit The design orbit of the machine or transfer line.
	 * @param diskRadius  Radius of the observer disks to be used. See
	 *                    {@link ObserverDisk}.
	 * @param backward    Must be set true when tracking backward.
	 * 
	 * @return An instance for a single particle or a beam, depending of the
	 *         fileName parameter.
	 * @throws IOException
	 */
	public static IntersectionCalculator getInstance(String fileName, ReferenceOrbit designOrbit, double diskRadius,
			boolean backward) throws IOException {

		if (fileName.contains(FileNamingConventions.BEAM_TRAJECTORY_FILE_EXTENSION)) {
			if (designOrbit.isCircular())
				throw new IllegalArgumentException(
						"The given file name belongs to a beam trajectory file, but the orbit is circular!");
			return new BeamIntersectionCalculatorImpl(fileName, designOrbit, diskRadius, backward);
		}
		if (fileName.contains(FileNamingConventions.TRAJECTORY_FILE_EXTENSION)) {
			return new IntersectionCalculatorImpl(designOrbit, fileName, diskRadius, backward);
		}
		throw new IllegalArgumentException(
				"The file  is neither a single particle trajectory nor a beam trajectory according to the file naming conventions.");

	}

	/**
	 * Gets an implementation of this interface for a single particle trajectory.
	 * 
	 * @param trajectoryData The trajectory data obtained from a single particle
	 *                       tracking in a ring or a transfer line.
	 * @param designOrbit    The design orbit belonging to the ring or transfer
	 *                       line.
	 * @param diskRadius     Observer disk radius. It must be big enough t cover the
	 *                       aperture, but should not intersect it twice. ( This
	 *                       might happen with a big radius in a ring.)
	 * @param backward       Must be set true when tracking backward.
	 * 
	 * @return An implementation of this interface.
	 * @throws IOException
	 */
	public static IntersectionCalculator getInstanceForParticle(List<TrajectoryData> trajectoryData,
			ReferenceOrbit designOrbit, double diskRadius, boolean backward) throws IOException {
		return new IntersectionCalculatorImpl(designOrbit, trajectoryData, diskRadius, backward);
	}

	/**
	 * Gets an implementation of this interface for a single particle trajectory.
	 * 
	 * @param particleName Particle name. A trajectory file belonging to the
	 *                     particle named according to the
	 *                     {@link FileNamingConventions} must be present.
	 * @param designOrbit  The design orbit belonging to the ring or transfer line.
	 * @param diskRadius   Observer disk radius. It must be big enough t cover the
	 *                     aperture, but should not intersect it twice. ( This might
	 *                     happen with a big radius in a ring.)
	 * @param backward     Must be set true when tracking backward.
	 * 
	 * @return An implementation of this interface.
	 * @throws IOException
	 */
	public static IntersectionCalculator getInstanceForParticle(String particleName, ReferenceOrbit designOrbit,
			double diskRadius, boolean backward) throws IOException {

		return new IntersectionCalculatorImpl(designOrbit, FileNamingConventions.getTrajectoryFileName(particleName),
				diskRadius, backward);
	}

	/**
	 * Gets an implementation of this interface for a beam trajectory.
	 * 
	 * @param beamName    Beam name. A trajectory file belonging to the beam named
	 *                    according to the {@link FileNamingConventions} must be
	 *                    present.
	 * @param designOrbit The design orbit belonging to a transfer line. Trajectory
	 *                    observers are not supported for rings.
	 * @param diskRadius  Observer disk radius. It must be big enough t cover the
	 *                    aperture, but should not intersect it twice.
	 * @param backward    Must be set true when tracking backward.
	 * 
	 * @return An implementation of this interface.
	 * @throws IOException
	 */

	public static IntersectionCalculator getInstanceForBeam(String beamName, ReferenceOrbit designOrbit,
			double diskRadius, boolean backward) throws IOException {

		return new BeamIntersectionCalculatorImpl(FileNamingConventions.getBeamTrajectoryFileName(beamName),
				designOrbit, diskRadius, backward);
	}

	/**
	 * Gets an implementation of this interface for a beam trajectory.
	 * 
	 * @param trajectoryMap The trajectory data map obtained from tracking a beam in
	 *                      a transfer line.
	 * @param designOrbit   The design orbit belonging to a transfer line.
	 *                      Trajectory observers are not supported for rings.
	 * @param diskRadius    Observer disk radius. It must be big enough t cover the
	 *                      aperture, but should not intersect it twice.
	 * @param backward      Must be set true when tracking backward.
	 * 
	 * @return An implementation of this interface.
	 * @throws IOException
	 */

	public static IntersectionCalculator getInstanceForBeam(Map<String, List<TrajectoryData>> trajectoryMap,
			ReferenceOrbit designOrbit, double diskRadius, boolean backward) throws IOException {

		return new BeamIntersectionCalculatorImpl(trajectoryMap, designOrbit, diskRadius, backward);
	}

	/**
	 * A helper method to organize the intsections along the orbit.
	 * 
	 * @param isData List of intersections.
	 * 
	 * @return A map of PhaseSpaceCoordinates. Key is the orbit index, value is a
	 *         list of phase space coordinates at that index. The key is sorted in
	 *         ascending order.
	 */
	public static Map<Integer, List<PhaseSpaceCoordinates>> getPhaseSpaceCoordinates(List<IntersectionData> isData) {

		Map<Integer, List<PhaseSpaceCoordinates>> m = new TreeMap<>();

		for (IntersectionData isd : isData) {
			Integer idx = isd.orbitIndex();
			List<PhaseSpaceCoordinates> l = m.get(idx);
			if (l == null) {
				l = new ArrayList<>();
				m.put(idx, l);
			}

			l.add(isd.phaseSpace());
		}
		return m;
	}

	/**
	 * Helper method to organize intersection data by particle name
	 * 
	 * @param isData List of intersections.
	 * @return Map of intersection data. The key is the particle name, value is a
	 *         list of intersections belonging to the particle name. The lists are
	 *         sorted in ascending order according to the orbit index.
	 */
	public static Map<String, List<IntersectionData>> getNameMap(List<IntersectionData> isData) {
		Map<String, List<IntersectionData>> m = new HashMap<>();

		for (IntersectionData isd : isData) {
			String name = isd.particleName();
			List<IntersectionData> l = m.get(name);
			if (l == null) {
				l = new ArrayList<>();
				m.put(name, l);
			}

			l.add(isd);
		}
		// sort the lists according to the orbit index in ascending order
		for (List<IntersectionData> l : m.values()) {
			l.sort((IntersectionData o1, IntersectionData o2) -> o1.orbitIndex() - o2.orbitIndex());
		}

		return m;
	}

	/**
	 * Get a list of intersection data along the reference orbit. The data in the
	 * lists belong to intersection points with a beam in case of a transfer line or
	 * intersections with a single particle during consecutive turns in a ring.
	 * 
	 * @return List of intersection data.
	 */
	public List<IntersectionData> getIntersections();

	/**
	 * Get the observer disks along the reference orbit. There is one disk at each
	 * point of the reference orbit.
	 * 
	 * @return observer disks.
	 */
	public List<ObserverDisk> getDisks();

	/**
	 * Writes the phase space coordinates to a file. In case of a ring the
	 * consecutive turns in a ring or the particles in a beam are separated with
	 * empty lines.
	 * 
	 * @param fileName Output file name.
	 */
	public void phaseSpaceToFile(String fileName);
}
