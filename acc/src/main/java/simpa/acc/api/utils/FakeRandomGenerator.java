package simpa.acc.api.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.random.JDKRandomGenerator;

/**
 * Imitates the random generator sampling, but actually returns fixed values
 * which repeats after n samples. Only the nextGaussian() method is implemented.
 */
public class FakeRandomGenerator extends JDKRandomGenerator {

    private List<Double> values= new ArrayList<>();
    private int pointer = 0;
    private boolean replay = false;

    public boolean isReplay() {
        return replay;
    }

    /*
     * Set the replay mode. If replay is set to true, the nextGaussian() method will
     * return the values from the list in order. If replay is set to false, the
     * nextGaussian() method will generate new random values and store them in the
     * list. In any case the pointer will be reset to 0.
     * 
     * @param replay The replay mode.
     */
    public void setReplay(boolean replay) {
        this.replay = replay;
        pointer = 0;
    }

    /**
     * Returns the next value from the stored values or generates a new one,
     * depending on the replay mode.
     * 
     * @return The next value from the stored values.
     */
    public double nextGaussian() {
        if (replay) {
            double d = values.get(pointer);
            pointer++;
            return d;
        } else {
            double d = super.nextGaussian();
            values.add(d);
            return d;
        }
    }
}
