package simpa.acc.api.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.create.AccElement;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.create.Sequence;
import simpa.core.api.SystemConstants;
import simpa.core.api.TangentVector3D;


/**
 * This utility class is for verification of a sequence against MAD-X SURVEY
 * command output.
 */
public class Survey {

	private Map<String, Vector3D> ccsCenters; //in CERN Coordinate System
	private ReferenceOrbit ccsOrbit;          //in CERN Coordinate System
	
	/**
	 * Construct a survey object from the given sequence and parameters. The center
	 * coordinates of each element will be rotated and translated and stored. The
	 * resulting coordinates can be later written to a file or compared to a MAD-X
	 * survey output file.
	 * 
	 * @param seq         Sequence to be surveyed.
	 * @param x0          Starting X coordinate in the cern coordinate system.
	 * @param y0          Starting Y coordinate in the cern coordinate system.
	 * @param z0          Starting Z coordinate in the cern coordinate system.
	 * @param theta0      Rotation defined according to MAD-X manual p. 13
	 * @param phi0        Rotation defined according to MAD-X manual p. 13
	 * @param psi0        Rotation defined according to MAD-X manual p. 13
	 * @param elementName Name of the element in the simpa sequence where the mad-x
	 *                    coordinates and angles are given. It can be null, in that
	 *                    case the start of the simpa sequence is taken. The map
	 *                    returned by the getCenters() method will contain an entry
	 *                    with the elementName as a key and the value is a Vector3D
	 *                    object having the same x,y,z values as the x0,y0,z0
	 *                    parameters.
	 */
	public Survey(Sequence seq, double x0, double y0, double z0, double theta0, double phi0, double psi0,
			String elementName) {

		
		// construct rotations according to the MAD-X manual p.13
		double c = Math.cos(theta0);
		double s = Math.sin(theta0);
		double[][] m1 = { { c, 0, s }, { 0, 1, 0 }, { -s, 0, c } };
		Rotation theta = new Rotation(m1, 1e-12);

		c = Math.cos(phi0);
		s = Math.sin(phi0);
		double[][] m2 = { { 1, 0, 0 }, { 0, c, s }, { 0, -s, c } };
		Rotation phi = new Rotation(m2, 1e-12);

		c = Math.cos(phi0);
		s = Math.sin(phi0);
		double[][] m3 = { { c, -s, 0 }, { s, c, 0 }, { 0, 0, 1 } };
		Rotation psi = new Rotation(m3, 1e-12);

		Rotation rot = theta.applyTo(phi.applyTo(psi));
		
		Vector3D p; 
		
		
		if(elementName!=null) {
			AccElement ace=seq.getAccElements().get(elementName);
			if(ace==null) {
				throw new IllegalArgumentException("Error at survey: The element "+elementName+" does not exist in the sequence");
			}
			
			TangentVector3D tv= seq.getReferenceOrbit().getTangentVectorAt(ace.getLongitudinalPosition());
			if(Math.abs(tv.direction().getY()) > SystemConstants.GEOMETRY_TOLERANCE) {
				throw new IllegalArgumentException("Error at survey: The orbit at the reference element does not lie in the horizontal plane");
			}
			
			Rotation rot2= new Rotation(tv.direction(), Vector3D.PLUS_J,Vector3D.PLUS_K,Vector3D.PLUS_J);
			rot =rot.applyTo(rot2);
			p= seq.getLocalOrigins().get(elementName);
			
		}
		else p= Vector3D.ZERO;
		
		p=rot.applyTo(p);
		Vector3D transl = (new Vector3D(x0, y0, z0)).subtract(p);

		ccsCenters = new HashMap<>();
		
		for (String name : seq.getLocalOrigins().keySet()) {
			Vector3D ov = seq.getLocalOrigins().get(name);
			Vector3D v = rot.applyTo(ov).add(transl);
			ccsCenters.put(name, v);
		}
		
		ccsOrbit= new ReferenceOrbit(seq.getReferenceOrbit(), rot, transl);
	}

	/**
	 * Writes the CERN coordinates calculated by the constructor to a file.
	 * 
	 * @param file Output file to be written.
	 */
	public void centersToFile(String file) {
		ccsOrbit.writeMaptoFileWithS(file, ccsCenters);		
	}
	
	public void orbitToFile(String file) {
		ccsOrbit.writeToFile(file);		
	}

	/**
	 * 
	 * @return Get a map with the elements names as keys and center coordinates in the
	 *         CERN coordinate system as values.
	 */
	public Map<String, Vector3D> getCenters() {
		return ccsCenters;
	}
}
