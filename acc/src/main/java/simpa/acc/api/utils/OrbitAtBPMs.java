/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import simpa.acc.api.Optics;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.AccElement;
import simpa.acc.api.create.Sequence;
import simpa.acc.api.track.PhaseSpacePlane;
import simpa.core.api.utils.FileUtils;

public class OrbitAtBPMs {

	private Sequence seq;

	public OrbitAtBPMs(Sequence seq) {
		if (seq.getOptics() == null)
			throw new IllegalArgumentException("The optics must be set for the sequence !");
		this.seq = seq;
	}

	/**
	 * Write the orbit positions to a file at each BPM in the sequence.
	 * 
	 * @param fileName The output file name.
	 * @param plane    Horizontal or vertical plane.
	 */
	public void writeOrbitAtBPMPositons(String fileName, PhaseSpacePlane plane) {

		StringBuffer sb = new StringBuffer();
		sb.append("# Orbit positions on the BPM's in sequence: ").append(seq.getName()).append(System.lineSeparator());
		sb.append("#BPM_NAME   S   POSITION").append(System.lineSeparator());
		Map<String, Double> hp = getOrbitAtBPMPositons(plane);
		Map<String, Double> vp = getOrbitAtBPMPositons(plane);

		if (plane == PhaseSpacePlane.HORIZONTAL) {
			sb.append("# HORIZIONTAL BPM's: ").append(System.lineSeparator());
			final Map<String, Double> hbpms =  getBpms(plane);

			Stream<Map.Entry<String, Double>> sorted = hbpms.entrySet().stream().sorted(Map.Entry.comparingByValue());
			sorted.forEach(entry -> sb.append(entry.getKey()).append(" ").append(entry.getValue()).append(" ")
					.append(hp.get(entry.getKey())).append(System.lineSeparator()));
		} else if (plane == PhaseSpacePlane.VERTICAL) {
			sb.append("# VERTICAL BPM's: ").append(System.lineSeparator());
			final Map<String, Double> vbpms = getBpms(plane);
			Stream<Map.Entry<String, Double>> sorted = vbpms.entrySet().stream().sorted(Map.Entry.comparingByValue());
			sorted.forEach(entry -> sb.append(entry.getKey()).append(" ").append(entry.getValue()).append(" ")
					.append(vp.get(entry.getKey())).append(System.lineSeparator()));
		}

		FileUtils.writeTextFile(fileName, sb.toString());
	}

	private Map<String, Double> getOrbitAtBPMPositons(PhaseSpacePlane plane) {
		Map<String, Double> res = new HashMap<>();
		Map<String, Double> bpms= getBpms(plane);

		Stream<Map.Entry<String, Double>> sorted = bpms.entrySet().stream().sorted(Map.Entry.comparingByValue());
		Optics opt = seq.getOptics();
		sorted.forEach(entry -> {
			double bpmPos = bpms.get(entry.getKey());
			TwissParameters twiss = opt.getTwissParametersAt(bpmPos);
			if (plane == PhaseSpacePlane.HORIZONTAL)
				res.put(entry.getKey(), twiss.hEllipse().pos());
			else
				res.put(entry.getKey(), twiss.vEllipse().pos());
		});

		return res;
	}

	private Map<String, Double> getBpms( PhaseSpacePlane plane ){
		Map<String, Double> bpms;
		if (plane == PhaseSpacePlane.HORIZONTAL)
			bpms = seq.getAccElements().entrySet().stream()
					.filter(e -> e.getValue().getType() == AccElement.ElementType.H_PICKUP)
					.collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getLongitudinalPosition()));
		else if (plane == PhaseSpacePlane.VERTICAL)
			bpms = seq.getAccElements().entrySet().stream()
					.filter(e -> e.getValue().getType() == AccElement.ElementType.V_PICKUP)
					.collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getLongitudinalPosition()));
		else
			throw new IllegalArgumentException("Unsupported phase space plane: " + plane);
		
		return bpms;
	}
}
