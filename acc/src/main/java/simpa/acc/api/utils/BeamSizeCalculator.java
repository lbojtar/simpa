package simpa.acc.api.utils;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.track.TrajectoryData;
import simpa.core.api.utils.FileUtils;
import simpa.acc.api.Optics;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.ReferenceOrbit;

/**
 * This class calculates the beam sizes along the sequence.
 */
public class BeamSizeCalculator {

    private static final Logger logger = LogManager.getLogger(BeamSizeCalculator.class);

    private Map<Double, Double> hsigma = new TreeMap<>();
    private Map<Double, Double> vsigma = new TreeMap<>();

    /**
     * Constructor with a given optics and emittances. It calculates the beam
     * sizes along the sequence from the Twiss parameters in the optics.
     * 
     * @param opt           The optics data.
     * @param horEmittance  The horizontal emittance.[m.rad]
     * @param vertEmittance The vertical emittance.[m.rad]
     * @param dpOp          Relative momentum spread.
     */
    public BeamSizeCalculator(Optics opt, double horEmittance, double vertEmittance, double dpOp) {

        for (TwissParameters twiss : opt.getAllTwissParametrs()) {
            if (twiss.longiPos() == Double.POSITIVE_INFINITY)
                throw new IllegalArgumentException("Longitudinal position is not set");
            double sh = Math.abs(twiss.dispH() * dpOp) + Math.sqrt(horEmittance * twiss.hEllipse().beta());
            hsigma.put(twiss.longiPos(), sh);
            double sv = Math.abs(twiss.dispV() * dpOp) + Math.sqrt(vertEmittance * twiss.vEllipse().beta());
            vsigma.put(twiss.longiPos(), sv);
        }

    }

    /**
     * Constructor with a given trajectory file and design orbit. It calculates the
     * beam sizes along the sequence from the trajectory file.
     * 
     * @param trajectoryFile The trajectory file.
     * @param designOrbit    The design orbit.
     * @param maxAperture    The maximum aperture. It is used for determine the
     *                       observer disks radius This number is in meters.
     */
    public BeamSizeCalculator(String trajectoryFile, ReferenceOrbit designOrbit, double maxAperture) {
        try {
            IntersectionCalculator isc = IntersectionCalculator.getInstance(trajectoryFile, designOrbit, maxAperture,
                    false);
            init(isc, designOrbit);

        } catch (IOException e) {
            logger.error(trajectoryFile + " not found", e);
        }

    }

    /**
     * Constructor with a given trajectory map and design orbit. It calculates the
     * beam sizes along the sequence from the trajectory map.
     * 
     * @param trajMap     The trajectory map. Keys are the particle names in the
     *                    beam.
     * @param designOrbit The design orbit.
     * @param maxAperture The maximum aperture. It is used for determine the
     * 
     */
    public BeamSizeCalculator(Map<String, List<TrajectoryData>> trajMap, ReferenceOrbit designOrbit,
            double maxAperture) {

        IntersectionCalculator isc;
        try {
            isc = IntersectionCalculator.getInstanceForBeam(trajMap, designOrbit, maxAperture,
                    false);
            init(isc, designOrbit);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

    }

    /**
     * 
     * @return The horizontal beam size.
     */
    public Map<Double, Double> getHorizontalBeamSize() {
        return hsigma;
    }

    /**
     * 
     * @return The vertical beam size.
     */
    public Map<Double, Double> getVerticalBeamSize(Optics opt, double vertEmittance, double dpOp) {
        return vsigma;
    }

    /**
     * This method writes the horizontal and vertical beam sizes to a file.
     * 
     * @param optics        The optics data.
     * @param horEmittance  The horizontal emittance.[m.rad]
     * @param vertEmittance The vertical emittance.[m.rad]
     * @param dpOp          Relative momentum spread.
     * @param outFile       The output file.
     */
    public void writeSigmas(String outFile) {

        StringBuffer sb = new StringBuffer();
        sb.append("LongiPos[m] SigmaHor[m]  SigmaVert[m]" + System.lineSeparator());

        for (Double lp : hsigma.keySet()) {
            double sh = hsigma.get(lp);
            double sv = vsigma.get(lp);
            sb.append(String.format("%f %f %f", lp, sh, sv) + System.lineSeparator());
        }

        FileUtils.writeTextFile(outFile, sb.toString());
    }

    private void init(IntersectionCalculator isc, ReferenceOrbit designOrbit) {

        Map<Integer, List<PhaseSpaceCoordinates>> phsMap = IntersectionCalculator
                .getPhaseSpaceCoordinates(isc.getIntersections());

        for (Integer index : phsMap.keySet()) {
            List<PhaseSpaceCoordinates> phsList = phsMap.get(index);
            double averageX = 0;
            double averageY = 0;
            for (PhaseSpaceCoordinates phs : phsList) {
                averageX += phs.x();
                averageY += phs.y();
            }
            averageX /= phsList.size();
            averageY /= phsList.size();

            double sh = 0;
            double sv = 0;
            for (PhaseSpaceCoordinates phs : phsList) {
                sh += (phs.x() - averageX) * (phs.x() - averageX);
                sv += (phs.y() - averageY) * (phs.y() - averageY);
            }
            sh = Math.sqrt(sh / phsList.size());
            sv = Math.sqrt(sv / phsList.size());
            double lp = designOrbit.getLongitudinalPosition(index);
            hsigma.put(lp, sh);
            vsigma.put(lp, sv);
        }

    }
}
