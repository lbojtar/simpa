/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api.utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.impl.JNaff;
import simpa.core.api.utils.FileUtils;

/**
 * Class for calculating the tune of an accelerator machine.
 */
public class TuneCalculator {

	private static final Logger logger = LogManager.getLogger(TuneCalculator.class);
	
	private final double[][] phs; // phase space
	JNaff naff;

	/**
	 * construct the tune calculator using and input file with phase space
	 * coordinates.
	 *
	 * @param file input file with phase space coordinates.
	 */
	public TuneCalculator(String file) throws FileNotFoundException {
		phs = FileUtils.read2DArray(file, 6);
		naff = new JNaff(1);
	}

	/**
	 * Gets the horizontal tune starting from a specific turn and measured for a
	 * certain amount of turns.
	 *
	 * @param startTurns the turn to start the measurement
	 * @param nTurns     the amount of turns to measure
	 * @return the horizontal tune
	 */
	public double getHorizontalTune(int startTurns, int nTurns) {
		return getTune(0, startTurns, nTurns);
	}

	/**
	 * Gets the vertical tune starting from a specific turn and measured for a
	 * certain amount of turns.
	 *
	 * @param startTurns the turn to start the measurement
	 * @param nTurns     the amount of turns to measure
	 * @return the vertical tune
	 */
	public double getVerticalTune(int startTurns, int nTurns) {
		return getTune(2, startTurns, nTurns);
	}

	/**
	 * Reads all phase space files in the given directory and calculate the tunes
	 * for each of them by taking the given number of turns from each file from the
	 * given starting turn. Then it writes the result into the given output file.
	 * 
	 * @param directory  the directory where the phase space files are located
	 * @param turns      the number of turns to take from each file
	 * @param startTurns the turn to start the measurement from, usually zero.
	 * @param outfile    the output file where the tunes are written
	 * @throws FileNotFoundException
	 */
	public static void calculateAllTunes(String directory, int startTurns, int turns, String outfile)
			throws FileNotFoundException {
		// get all files in the directory
		ArrayList<String> files = FileUtils.scanDirectory(directory);
		StringBuffer sb = new StringBuffer();
		sb.append("#hTune vTune filename" + System.lineSeparator());

		// calculate the tunes for each file
		for (String f : files) {
			String file = directory + "/" + f;
			if (file.contains("PHS")) {
				TuneCalculator tc = new TuneCalculator(file);
				double hTune = tc.getHorizontalTune(startTurns, turns);
				double vTune = tc.getVerticalTune(startTurns, turns);
				sb.append(hTune + " " + vTune + " " + file + System.lineSeparator());
			}else logger.info("Ignored file: "+file+"because file name does not contain the string 'PHS'");
		}
		FileUtils.writeTextFile(outfile, sb.toString());
	}

	private double getTune(int index, int startTurns, int nTurns) {
		if (!isPowerOfTwo(nTurns))
			throw new IllegalArgumentException("TuneCalculator: The number of turns must be a power of 2");

		if (phs.length < startTurns + nTurns) {
			// particle lost
			return 1.0;
		}

		double[] data = new double[nTurns]; /* data to be analyzed */
		for (int i = startTurns; i < startTurns + nTurns; i++) {
			data[i - startTurns] = phs[i][index];
		}
		naff.calculate(data, 1.0);
		return naff.getFrequencies()[0];
	}

	// Function to check if x is power of 2
	private boolean isPowerOfTwo(int n) {
		if (n == 0)
			return false;
		while (n != 1) {
			if (n % 2 != 0)
				return false;
			n = n / 2;
		}
		return true;
	}
}
