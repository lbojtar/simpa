/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.PhysicsConstants;
import simpa.core.api.utils.CalculatorUtils;

/**
 * Utilities specific for calculating in the accelerator library.
 */
public class AccCalculatorUtils {

	public static double getGamma(double pInGev, double m0) {
		double v = getVelocity(pInGev, m0);
		return 1.0 / Math.sqrt(1 - (v * v) / (PhysicsConstants.SPEED_OF_LIGHT * PhysicsConstants.SPEED_OF_LIGHT));
	}

	/**
	 *
	 * @param pInGev -Momentum in [GeV/c]
	 * @param m0     -Rest mass in [kg]
	 * @return - Total energy in [kg*m^2/s^2]
	 */
	// TODO CHECK IT, NOT USED AT THE MOMENT
	public static double getTotalEnergy(double pInGev, double m0) {
		return getGamma(pInGev, m0) * m0 * PhysicsConstants.SPEED_OF_LIGHT * PhysicsConstants.SPEED_OF_LIGHT;
	}

	/**
	 * Convert momentum from [GeV/c] to [kg*m/s]
	 * 
	 * @param pInGev -momentum in [GeV/c]
	 * @return - momentum in [kg*m/s]
	 */
	public static double getMomentumFromGeVoC(double pInGev) {
		return pInGev * PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE / (PhysicsConstants.SPEED_OF_LIGHT * 1E-9);
	}

	/**
	 * @param pInGev - Momentum in GeV/c
	 * @return - The velocity of a particle [m/s].
	 */
	public static double getVelocity(double pInGev, double m0) {
		double p = getMomentumFromGeVoC(pInGev);
		return p * PhysicsConstants.SPEED_OF_LIGHT
				/ Math.sqrt(PhysicsConstants.SPEED_OF_LIGHT * PhysicsConstants.SPEED_OF_LIGHT * m0 * m0 + p * p);

	}

	/**
	 * @param pInGev - momentum in[GeV/c]
	 * @param m0     - mass [kg]
	 * @return -Kinetic energy [eV]
	 */
	public static double getKineticEnergy(double pInGev, double m0) {
		double mom = getMomentumFromGeVoC(pInGev);
		double cc = PhysicsConstants.SPEED_OF_LIGHT * PhysicsConstants.SPEED_OF_LIGHT;
		double eTot = Math.sqrt(
				cc * m0 * cc * m0 + (mom * PhysicsConstants.SPEED_OF_LIGHT) * (mom * PhysicsConstants.SPEED_OF_LIGHT));

		double ekin = eTot - m0 * PhysicsConstants.SPEED_OF_LIGHT * PhysicsConstants.SPEED_OF_LIGHT;
		return CalculatorUtils.joulesToEv(ekin);
	}

	/**
	 * @param evKin - kinetic energy [eV]
	 * @param m0    - rest mass [kg]
	 * @return - Momentum in [eV/c]
	 */
	public static double getMomentumFromEKin(double evKin, double m0) {

		double cc = PhysicsConstants.SPEED_OF_LIGHT * PhysicsConstants.SPEED_OF_LIGHT;
		double eTot = CalculatorUtils.evToJoules(evKin) + m0 * cc;

		double mom = Math.sqrt(eTot * eTot - cc * cc * m0 * m0) / PhysicsConstants.SPEED_OF_LIGHT;
		return getMomentumInEVoC(mom);
	}

	/**
	 * Get the norm of the momentum in [eV/c]
	 * 
	 * @param mom - momentum in [kg*m/s]
	 * @return - momentum in [eV/c]
	 */
	public static double getMomentumInEVoC(double mom) {
		return mom * PhysicsConstants.SPEED_OF_LIGHT / PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE;
	}

	public static double getBrhoFromGevOc(double gevOc) {
		return gevOc / 0.2997925;
	}

	/**
	 * Interpolates the kinetic momentum using two position and momentum pairs
	 * 
	 * @param point      A point the momentum is calculated for.
	 * @param kinMom     Current kinetic momentum.
	 * @param prevKinMom Previous kinetic momentum.
	 * @param pos        Current position.
	 * @param prevPos    Previous position
	 * @return The interpolated momentum for the given point.
	 */
	public static Vector3D interpolateMomentum(Vector3D point, Vector3D kinMom, Vector3D prevKinMom, Vector3D pos,
			Vector3D prevPos) {
		double distFromPev = point.distance(prevPos);
		double dist = prevPos.distance(pos);

		Vector3D dMom = kinMom.subtract(prevKinMom).scalarMultiply(distFromPev / dist);
		return prevKinMom.add(dMom);
	}
}
