package simpa.acc.api.utils;

import java.util.Map;

import simpa.acc.api.Tune;

/**
 * Interface to be implemented to describe some optical features of a circular
 * machine at different working points. An implementation is required by the
 * constructor of the {@link FrequencyAnalysis} class.
 * 
 * @author lbojtar
 *
 */
public interface TuneModel {

	/**
	 * Get a map of all accelerator element currents needed to obtain the given
	 * tunes at the given momentum. The return list includes only the elements
	 * affecting the tunes. These are usually a number of quadrupoles.
	 * 
	 * @param tune     A tune object.
	 * @param momentum The momentum of the machine [GeV/c]
	 * 
	 * @return A map of scaling factors. The key is the binary file name for
	 *         the element and the value is usually the current [A] or voltage [V].
	 */
	public Map<String, Double> getScalingFactors(Tune tune, double momentum);

}
