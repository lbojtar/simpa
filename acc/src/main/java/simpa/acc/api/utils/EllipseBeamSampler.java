package simpa.acc.api.utils;

import java.util.ArrayList;
import java.util.List;

import simpa.acc.api.TwissParameters;
import simpa.core.api.track.PhaseSpaceCoordinates;

/**
 * This class can be used to generate the phase space coordinates for the
 * particles in a beam such that the particles are distributed on an ellipse.
 */
public class EllipseBeamSampler implements BeamSampler {

    private final TwissParameters twissParameters;

    /**
     * This constructor creates an instance of the EllipseBeamSampler class.
     * 
     * @param tp Twiss parameters describing the phase space ellipse.
     */
    public EllipseBeamSampler(TwissParameters tp) {
        this.twissParameters = tp;
    }

    /**
     * This method can be used to generate the phase space coordinated for the
     * particles in a beam such that the particles are distributed in a ellipse.
     * The particles are distributed both in the H and V plane on an ellipse with
     * the given emittance. If you want to discard the effect of coupling two
     * ellipse
     * should be generated for tracking. Once the H emittance should be zero, then
     * the V emittance should be zero. The particles are distributed in the
     * longitudinal plane with a normal distribution with the given dpOp (1 sigma).
     * The time delay of the particles is set to zero.
     * 
     * @param n number of particles
     * @return list of phase space coordinates
     */
    @Override
    public List<PhaseSpaceCoordinates> sample(int n) {
        double dfi = 2 * Math.PI / n;
        List<PhaseSpaceCoordinates> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            PhaseSpaceCoordinates phs = twissParameters.getPhaseSpaceCoordinates(twissParameters.hEllipse().em(),
                    dfi * i, twissParameters.vEllipse().em(),
                    (dfi * i) + Math.PI / 2.0, 0, twissParameters.dpOp());
            list.add(phs);
        }
        return list;
    }

    /**
     * 
     * @return The Twiss parameters describing the phase space ellipse.
     */
    public TwissParameters getTwissParameters() {
        return twissParameters;
    }


}
