/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 * <p>
 * Author : Lajos Bojtar
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.acc.api.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.EllipseBeam;
import simpa.acc.api.Optics;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.acc.api.track.PhaseSpaceObserver;
import simpa.acc.api.track.PhaseSpacePlane;
import simpa.acc.api.track.TLBeamTracker;
import simpa.acc.api.track.TurnObserver;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.PotentialProvider;
import simpa.core.api.track.BeamTrajectoryObserver;
import simpa.core.api.track.TrackingObserver.ObserverBehaviour;
import simpa.core.api.track.ObserverDisk;
import simpa.core.api.track.Particle;
import simpa.core.api.track.ParticleTrackerTask;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.track.TrajectoryData;
import simpa.core.api.track.TrajectoryObserver;
import simpa.core.api.utils.CalculatorUtils;

/**
 * This class calculates the optical functions from trajectory files generated
 * during two particles or beams. It calculates the intersections of the
 * trajectories with {@link ObserverDisk} along the orbit, then at each orbit
 * point fits an ellipse to the {@link PhaseSpaceCoordinates}.
 * 
 * @author lbojtar
 *
 */
public class OpticsCalculator {

	private static final Logger logger = LogManager.getLogger(OpticsCalculator.class);

	private final ReferenceOrbit designOrbit;
	private int turns = 64;
	private boolean partialOpticsAllowed = true;

	/**
	 * Constructor with the reference obit .
	 * 
	 * @param refOrbit The reference orbit.
	 */
	public OpticsCalculator(ReferenceOrbit refOrbit) {
		this.designOrbit = refOrbit;
	}

	/**
	 * Tracks the two particles in a ring given as parameters, then calculates the
	 * optical functions from two trajectory files. The first particle should have
	 * the nominal energy and the second have some dp/p deviation, for example 1E-3.
	 * This momentum deviation is used for calculating the dispersion.
	 * 
	 * @param p0                  Particle with nominal momentum.
	 * @param p1                  Particle with with some momentum deviation.
	 * @param pp                  A potential provider object.
	 * @param stepsize            Step size for the tracking.
	 * @param diskradius          The disk radius for the observers.
	 * @param writeTrajectoryFile If true, two trajectory files will be written to
	 *                            the disk and the optics will be calculated from
	 *                            these files. It is useful for debugging if
	 *                            something goes wrong. When false tha trajectory
	 *                            data stored internally and the optics calculated
	 *                            from that data without writing it to the disk.
	 *                            This should be the case when doing matching, for
	 *                            example.
	 * @return The calculated optics.
	 * @throws IOException
	 * @throws OpticsCalculationException
	 */
	public Optics trackInRingAndCalculate(Particle p0, Particle p1, PotentialProvider pp, double stepsize,
			double diskradius, boolean writeTrajectoryFile) throws IOException, OpticsCalculationException {
		double mom0 = CalculatorUtils.getMomentumInGeVoC(p0.getKineticMomentum().getNorm());
		double mom1 = CalculatorUtils.getMomentumInGeVoC(p1.getKineticMomentum().getNorm());
		double dpop = (mom1 - mom0) / mom0;
		logger.info("Momentum difference of the  two particles Dp/p =" + dpop);
		List<TrajectoryData> trd1 = trackParticle(writeTrajectoryFile, p0, stepsize, pp, diskradius, false);
		List<TrajectoryData> trd2 = trackParticle(writeTrajectoryFile, p1, stepsize, pp, diskradius, false);

		if (writeTrajectoryFile)
			return calculateOpticsFunctions(FileNamingConventions.getTrajectoryFileName(p0.getName()),
					FileNamingConventions.getTrajectoryFileName(p1.getName()), dpop, diskradius, false);
		else
			return calculateRingOptics(trd1, trd2, dpop, diskradius, false);

	}

	/**
	 * Tracks the two beams in a transfer line, then calculates the optical
	 * functions from two beam trajectory files. It is highly recommended to
	 * generate the beams with the {@link EllipseBeamSampler}. A Gaussian beam would
	 * make the Ellipse fitting more inefficient. The first beam should have the
	 * nominal energy and the second have some dp/p deviation. This momentum
	 * deviation is used for calculating the dispersion.
	 * 
	 * @param writeTrajectoryFile If true, two trajectory files will be written to
	 *                            the disk and the optics will be calculated from
	 *                            these files. It is useful for debugging if
	 *                            something goes wrong. When false, the trajectory
	 *                            data stored internally and the optics calculated
	 *                            from that data without writing it to the disk.
	 *                            This should be the case when doing matching, for
	 *                            example.
	 * @param b0                  Beam with nominal momentum.
	 * @param b1                  Beam with with some momentum deviation.
	 * @param pp                  A potential provider object.
	 * @param stepsize            Step size for the tracking.
	 * @param diskradius          The disk radius for the observers.
	 * @param backward            If true, the optics will be calculated by tracking
	 *                            backward. Useful when the Twiss parameters are
	 *                            known at the end of a transfer line.
	 * @return The calculated optics.
	 * @throws IOException
	 * @throws OpticsCalculationException
	 */
	public Optics trackInLineAndCalculate(boolean writeTrajectoryFile, EllipseBeam b0, EllipseBeam b1,
			PotentialProvider pp, double stepsize, double diskradius, boolean backward)
			throws IOException, OpticsCalculationException {

		double mom0 = b0.getNominalMomentum();
		double mom1 = b1.getNominalMomentum();
		double dpop = (mom1 - mom0) / mom0;
		if (dpop == 0)
			throw new IllegalArgumentException("The momentum difference of the two beams is not allowed to be zero !");
		logger.info("Momentum difference of the  two beams Dp/p =" + dpop);

		Map<String, List<TrajectoryData>> trajectoryMap1 = trackBeam(writeTrajectoryFile, b0, stepsize, pp, diskradius,
				backward);
		Map<String, List<TrajectoryData>> trajectoryMap2 = trackBeam(writeTrajectoryFile, b1, stepsize, pp, diskradius,
				backward);
		if (writeTrajectoryFile)
			return calculateOpticsFunctions(FileNamingConventions.getBeamTrajectoryFileName(b0.getName()),
					FileNamingConventions.getBeamTrajectoryFileName(b1.getName()), dpop, diskradius, backward);
		else
			return calculateTLineOptics(trajectoryMap1, trajectoryMap2, dpop, diskradius, backward);

	}

	/**
	 * Set the number of turns for the tracking of the particles used for optics
	 * calculation.
	 * 
	 * @param turns Number of turns for the tracking.
	 */
	public void setTurns(int turns) {
		this.turns = turns;
	}

	/**
	 * Calculates the optical functions from two trajectory files in a transfer line
	 * or a ring. The first tracking file should belong to the nominal energy and
	 * the second with a dp/p deviation as given in the dpop parameter. The
	 * trajectory file names must conform to the {@link FileNamingConventions}, so
	 * the method can decide from the file name it belongs to a single particle in a
	 * ring or a beam in a transfer line.
	 * 
	 * @param trajectoryFile1 File containing a trajectory at least 6 machine turns
	 *                        in case of a ring or 6 particles in case of a transfer
	 *                        line, otherwise the ellipse fit will fail. This data
	 *                        is used for dispersion calculation. The tracking
	 *                        producing this trajectory must have been made with the
	 *                        nominal energy.
	 * @param trajectoryFile2 File containing a trajectory at least 6 machine turns
	 *                        in case of a ring or 6 particles in case of a transfer
	 *                        line, otherwise the ellipse fit will fail. This data
	 *                        is used for dispersion calculation. The tracking
	 *                        producing this trajectory must have been made with
	 *                        some non zero dp/p.
	 * @param dpop            Dp/p of the particle used for the second tracking.
	 * @param maxAperture     Maximum aperture of the machine. To calculate the
	 *                        optics this class places disks along the orbit and
	 *                        calculate the intersections with the trajectory. The
	 *                        disk diameter should be big enough to cover the
	 *                        aperture, but should not be too big to intersect more
	 *                        than once the orbit of the machine.
	 * @param backward        Must be set true when the trajectory files belong to
	 *                        tracking backward.
	 * @return An optics object containing the Twiss parameters at each orbit point.
	 * @throws IOException
	 */
	public Optics calculateOpticsFunctions(String trajectoryFile1, String trajectoryFile2, double dpop,
			double maxAperture, boolean backward) throws IOException, OpticsCalculationException {

		IntersectionCalculator isc1 = IntersectionCalculator.getInstance(trajectoryFile1, designOrbit, maxAperture,
				backward);

		IntersectionCalculator isc2 = IntersectionCalculator.getInstance(trajectoryFile2, designOrbit, maxAperture,
				backward);
		return calculateOptics(isc1, isc2, dpop, maxAperture, backward);
	}

	/**
	 * Calculates the optical functions from two trajectory data lists in a ring.
	 * The first tracking data should belong to the nominal momentum and the second
	 * with a dp/p deviation as given in the dpop parameter.
	 * 
	 * @param trajectoryData1 The trajectory data obtained from a single particle
	 *                        tracking in a ring with the nominal momentum.
	 * @param trajectoryData2 The trajectory data obtained from a single particle
	 *                        tracking in a ring with the dp/p momntum deviation.
	 * 
	 * @param dpop            Dp/p of the particle used for the second tracking.
	 * @param maxAperture     Maximum aperture of the machine. To calculate the
	 *                        optics this class places disks along the orbit and
	 *                        calculate the intersections with the trajectory. The
	 *                        disk diameter should be big enough to cover the
	 *                        aperture, but should not be too big to intersect more
	 *                        than once the orbit of the machine.
	 * @param backward        Must be set true when the trajectory files belong to
	 *                        tracking backward.
	 * @return An optics object containing the Twiss parameters at each orbit point.
	 * @throws IOException
	 */
	public Optics calculateRingOptics(List<TrajectoryData> trajectoryData1, List<TrajectoryData> trajectoryData2,
			double dpop, double maxAperture, boolean backward) throws IOException, OpticsCalculationException {

		IntersectionCalculator isc1 = IntersectionCalculator.getInstanceForParticle(trajectoryData1, designOrbit,
				maxAperture, backward);

		IntersectionCalculator isc2 = IntersectionCalculator.getInstanceForParticle(trajectoryData2, designOrbit,
				maxAperture, backward);
		return calculateOptics(isc1, isc2, dpop, maxAperture, backward);
	}

	/**
	 * Calculates the optical functions from two trajectory data maps in a transfer
	 * line. The first tracking data should belong to the nominal momentum and the
	 * second with a dp/p deviation as given in the dpop parameter.
	 * 
	 * @param trajectoryMap1 The trajectory data obtained from a beam tracking in a
	 *                       transfer line with the nominal momentum.
	 * @param trajectoryMap2 The trajectory data obtained from a beam tracking in a
	 *                       ring with the dp/p momntum deviation.
	 * 
	 * @param dpop           Dp/p of the particle used for the second tracking.
	 * @param maxAperture    Maximum aperture of the machine. To calculate the
	 *                       optics this class places disks along the orbit and
	 *                       calculate the intersections with the trajectory. The
	 *                       disk diameter should be big enough to cover the
	 *                       aperture, but should not be too big to intersect more
	 *                       than once the orbit of the machine.
	 * @param backward       Must be set true when the trajectory files belong to
	 *                       tracking backward.
	 * @return An optics object containing the Twiss parameters at each orbit point.
	 * @throws IOException
	 */
	public Optics calculateTLineOptics(Map<String, List<TrajectoryData>> trajectoryMap1,
			Map<String, List<TrajectoryData>> trajectoryMap2, double dpop, double maxAperture, boolean backward)
			throws IOException, OpticsCalculationException {

		IntersectionCalculator isc1 = IntersectionCalculator.getInstanceForBeam(trajectoryMap1, designOrbit,
				maxAperture, backward);

		IntersectionCalculator isc2 = IntersectionCalculator.getInstanceForBeam(trajectoryMap2, designOrbit,
				maxAperture, backward);
		return calculateOptics(isc1, isc2, dpop, maxAperture, backward);
	}

	/**
	 * @return True if the optics calculation is allowed to be partial. That can
	 *         happen when there are losses before the end of a transfer line. If
	 *         false, an OpticsCalculationException is thrown when an error occurs
	 *         during the optics calculation.
	 */
	public boolean isPartialOpticsAllowed() {
		return partialOpticsAllowed;
	}

	/**
	 * Set if the optics calculation is allowed to be partial.
	 * 
	 * @param partialOpticsAllowed Setting this true allows the optics calculation
	 *                             in
	 *                             a transfer line to be partial. That can happen
	 *                             when there are losses before the end of a
	 *                             transfer line. If false, an
	 *                             OpticsCalculationException is
	 *                             thrown when there is particle loss or an error
	 *                             occurs during the optics calculation.
	 */
	public void setPartialOpticsAllowed(boolean partialOpticsAllowed) {
		this.partialOpticsAllowed = partialOpticsAllowed;
	}

	private Optics calculateOptics(IntersectionCalculator isc1, IntersectionCalculator isc2, double dpop,
			double maxAperture, boolean backward) throws IOException, OpticsCalculationException {

		Map<Integer, List<PhaseSpaceCoordinates>> phsc1 = IntersectionCalculator
				.getPhaseSpaceCoordinates(isc1.getIntersections());
		Map<Integer, List<PhaseSpaceCoordinates>> phsc2 = IntersectionCalculator
				.getPhaseSpaceCoordinates(isc2.getIntersections());

		List<TwissParameters> twissList = new ArrayList<>();

		double lpos = 0;
		double muH = 0;
		double muV = 0;

		for (int plIdx : phsc1.keySet()) {

			double ds = designOrbit.getOrbitPoints().get(plIdx).distance(designOrbit.getPreviousPoint(plIdx));
			lpos += ds;
			try {
				PhaseSpaceEllipse hp = PhaseSpaceEllipse.calculatePhaseSpaceEllipse(lpos, phsc1.get(plIdx),
						PhaseSpacePlane.HORIZONTAL);
				PhaseSpaceEllipse vp = PhaseSpaceEllipse.calculatePhaseSpaceEllipse(lpos, phsc1.get(plIdx),
						PhaseSpacePlane.VERTICAL);

				// for dispersion
				PhaseSpaceEllipse hpd = PhaseSpaceEllipse.calculatePhaseSpaceEllipse(lpos, phsc2.get(plIdx),
						PhaseSpacePlane.HORIZONTAL);
				PhaseSpaceEllipse vpd = PhaseSpaceEllipse.calculatePhaseSpaceEllipse(lpos, phsc2.get(plIdx),
						PhaseSpacePlane.VERTICAL);

				double dx = (hpd.pos() - hp.pos()) / dpop; // hor dispersion
				double dy = (vpd.pos() - vp.pos()) / dpop; // vert dispersion

				double dpx = (hpd.angle() - hp.angle()) / dpop; // hor dispersion prime
				double dpy = (vpd.angle() - vp.angle()) / dpop; // vert dispersion prime

				muH += ds / (2 * Math.PI * hp.beta());
				muV += ds / (2 * Math.PI * vp.beta());
				TwissParameters twp = TwissParameters.builder().longiPos(lpos).hEllipse(hp).vEllipse(vp).dispH(dx)
						.dispV(dy)
						.dispPrimeH(dpx).dispPrimeV(dpy).muH(muH).muV(muV).dpOp(dpop).build();
				twissList.add(twp);

			} catch (Exception e) {
				if (!partialOpticsAllowed)
					throw new OpticsCalculationException(
							"Error at optics calculation at orbit point " + plIdx + " " + e.getMessage(), 1);
				logger.error("Error at optics calcultion at orbit point " + plIdx + " " + e.getMessage());
				logger.error("Partial optics is calculated until this point.");
				return new Optics(twissList, this.designOrbit);
			}

		}

		return new Optics(twissList, this.designOrbit);
	}

	private List<TrajectoryData> trackParticle(boolean writeTrajectoryFile, Particle p, double stepsize,
			PotentialProvider pp, double diskRadius, boolean backward) throws OpticsCalculationException {

		ParticleTrackerTask ptt;
		double p0InGeV = CalculatorUtils.getMomentumInGeVoC(p.getKineticMomentum().getNorm());

		ptt = new ParticleTrackerTask(pp, p, Integer.MAX_VALUE, stepsize, backward);
		ptt.addObserver(new PhaseSpaceObserver(designOrbit, 0, diskRadius, p0InGeV, backward));
		TurnObserver to = new TurnObserver(designOrbit, diskRadius, backward);
		to.setMaximumTurns(turns);
		ptt.addObserver(to);

		TrajectoryObserver tro;

		if (writeTrajectoryFile)
			tro = new TrajectoryObserver(ObserverBehaviour.STORE_AND_WRITE, false);
		else
			tro = new TrajectoryObserver(ObserverBehaviour.STORE_ONLY, false);

		ptt.addObserver(tro);

		ptt.run();

		if (p.isLost())
			throw new OpticsCalculationException(
					"The particle was lost, can't calculate optics. Revise the initial conditions and settings.", 1);

		return tro.getTrajectoryDatas();
	}

	private Map<String, List<TrajectoryData>> trackBeam(boolean writeTrajectoryFile, EllipseBeam beam, double stepSize,
			PotentialProvider pp, double diskRadius, boolean backward) throws IOException, OpticsCalculationException {

		TLBeamTracker tlbt = new TLBeamTracker(beam, diskRadius, stepSize, new ArrayList<Double>(), backward);
		BeamTrajectoryObserver btro;

		if (writeTrajectoryFile)
			btro = new BeamTrajectoryObserver(ObserverBehaviour.STORE_AND_WRITE);
		else
			btro = new BeamTrajectoryObserver(ObserverBehaviour.STORE_ONLY);

		tlbt.addTrajectoryObserver(btro);
		tlbt.track();
		if (tlbt.getLossCount() > 0)
			throw new OpticsCalculationException(
					"Nonzero lost particles, can't calculate optics. Revise the initial conditions and settings.",
					tlbt.getLossCount());

		return btro.getTrajectoryData();
	}

}
