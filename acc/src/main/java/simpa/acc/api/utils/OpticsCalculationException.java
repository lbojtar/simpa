package simpa.acc.api.utils;

import simpa.acc.api.match.Matcher;

/**
 * Exception is thrown when the optics calculation failed. This can happen when
 * particles used for the calculation are lost. It is used by the {@link Matcher} class * 
 * 
 * @author lbojtar
 *
 */
public class OpticsCalculationException extends Exception {

	private static final long serialVersionUID = 1L;
	private	int lostCount;
	
	public OpticsCalculationException(String s,int lostCount) {
		super(s);
		this.lostCount=lostCount;
	}

	public int getLostCount() {
		return lostCount;
	}

	
}
