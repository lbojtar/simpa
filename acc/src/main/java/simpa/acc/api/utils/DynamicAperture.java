package simpa.acc.api.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.Beam;
import simpa.acc.api.ParticleFactory;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.track.BeamTracker;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.acc.api.track.PhaseSpaceObserver;
import simpa.acc.api.track.RingBeamTracker;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.Particle;
import simpa.core.api.track.ParticleType;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.FileUtils;

public class DynamicAperture {

	private static final Logger logger = LogManager.getLogger(DynamicAperture.class);

	// some default values
	private int turns = 10000;
	private int nThreads = Runtime.getRuntime().availableProcessors();
	private double stepSize = 0.01;
	private double momentum;
	private int angleSteps = 20;
	private int radialSteps = 20;
	private double emMin = 20E-6;
	private double emMax = 120E-6;

	private PotentialProvider potProvider;
	private String outDir;
	private Beam beam;
	private ReferenceOrbit refOrbit;
	private TwissParameters twiss;
	private double diskRadius;

	/**
	 * Constructor with the reference orbit, potential provider, output directory,
	 * disk radius, twiss parameters and momentum. The reference orbit must be
	 * closed for dynamic aperture. The additinal parameters has some resonable
	 * default values whivh can be changed.
	 * 
	 * @param refOrbit    A closed`reference orbit.
	 * @param potProvider A potential provider for the tracking.
	 * @param outDir      Output directory. The phase space observer will write
	 *                    files into this directory.
	 * @param diskRadius  The disk radius for the observer.
	 * @param twiss       The twiss parameters at the longitudinal position of the
	 *                    reference orbit, where theparticles are inserted into the
	 *                    ring. The twiss pparameters object containt a field for
	 *                    the longitudinal position which will be used to determine
	 *                    the insertion point.
	 * @param momentum
	 * @throws Exception
	 */
	public DynamicAperture(ReferenceOrbit refOrbit, PotentialProvider potProvider, String outDir, double diskRadius,
			TwissParameters twiss, double momentum) throws Exception {
		if (!refOrbit.isCircular())
			throw new IllegalAccessException("Reference orbit must be closed for dynamic aperture !");

		// check if the last character in outDir is a backslash, if yes, remove it
		if (outDir.charAt(outDir.length() - 1) == File.separatorChar)
			outDir = outDir.substring(0, outDir.length() - 1);

		this.momentum = momentum;
		this.refOrbit = refOrbit;
		this.potProvider = potProvider;
		this.outDir = outDir;
		this.diskRadius = diskRadius;
		this.twiss = twiss;
	}

	/**
	 * First it sets up the particle for the frequency analysys, then tracks
	 * particles for the specified number of turns.
	 * 	
	 * @throws FileNotFoundException If some input file is missing.
	 */

	public void track(int turns)
			throws IOException, OutOfApertureException, KnownFatalException {
		this.turns=turns;
		beam = getBeamForDA(twiss);
		BeamTracker beamTracking = new RingBeamTracker(beam, turns, diskRadius, stepSize, nThreads,false);
		PhaseSpaceObserver.setFlushPerTurns(100);
		beamTracking.track();
		//calculate();
	}

	/**
	 * Before calling this method the tracking must be done with the track method.
	 *
	 * Calculates the dynamic aperture using the normal form formula B5 in:
	 * https://link.aps.org/doi/10.1103/PhysRevE.53.4067
	 * 
	 * The method used here a bit different,we get the emittance with phase space
	 * ellipse fitting. The invariant rho in B5 eq. is half of the emittance.
	 * It writes a file named lastConnectedEmittances.txt into the output directory
	 * containing the last connected emittances for each angle step.
	 * 
	 * @return
	 * @throws IOException
	 */
	public double calculate() throws IOException {
		Map<String, Double> fl = getFileNames();

		double sum = 0;
		String s = "#Horizontal_emittance Vectical_emittance[m]" + System.lineSeparator();
		for (String f : fl.keySet()) {
			double alphak = fl.get(f);

			TwissParameters tp = PhaseSpaceEllipse.getTwissParameters(outDir + File.separator + f);

			double emh = tp.hEllipse().em(); // [pi m rad]
			double emv = tp.vEllipse().em();

			System.out.println("Calculated Hor. Em.= " + emh + " Vert. Em. =" + emv);

			s = s + emh + " " + emv + System.lineSeparator();
			sum += (emh + emv) * (emh + emv) * Math.sin(2 * alphak);
		}
		double av = Math.PI * sum / (2 * angleSteps);
		double dynap = Math.pow(av, 1 / 4.0);
		FileUtils.writeTextFile(outDir + File.separator + "lastConnectedEmittances.txt", s);
		logger.info("Dynamic aperture = " + dynap);

		return dynap;
	}

	public int getTurns() {
		return turns;
	}

	public void setTurns(int turns) {
		this.turns = turns;
	}

	public int getnThreads() {
		return nThreads;
	}

	public void setnThreads(int nThreads) {
		this.nThreads = nThreads;
	}

	public double getStepSize() {
		return stepSize;
	}

	public void setStepSize(double stepSize) {
		this.stepSize = stepSize;
	}

	public int getAngleSteps() {
		return angleSteps;
	}

	public void setAngleSteps(int angleSteps) {
		this.angleSteps = angleSteps;
	}

	public int getRadialSteps() {
		return radialSteps;
	}

	public void setRadialSteps(int radialSteps) {
		this.radialSteps = radialSteps;
	}

	public double getEmMin() {
		return emMin;
	}

	public void setEmMin(double emMin) {
		this.emMin = emMin;
	}

	public double getEmMax() {
		return emMax;
	}

	public void setEmMax(double emMax) {
		this.emMax = emMax;
	}

	// returns the file names for each angle with the last connected radial value
	private Map<String, Double> getFileNames() {

		File[] files = new File(outDir).listFiles();
		List<String> fileList = new ArrayList<>();
		for (File f : files) {
			fileList.add(f.getName());
		}

		Map<String, Double> names = new HashMap<>();

		double dEm = (emMax - emMin) / radialSteps;
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		String previous = null;
		double prevfi = 0;
		for (int ai = 1; ai < angleSteps; ai++) {
			for (int ri = 0; ri < radialSteps; ri++) {

				double fi = ai * Math.PI / (2 * angleSteps);
				double horAmpl = Math.sin(fi);
				double vertAmpl = Math.cos(fi);
				double hEm = horAmpl * (emMin + ri * dEm);
				double vEm = vertAmpl * (emMin + ri * dEm);

				String hs = nf.format(hEm * 1E6); // [pi mm mrad]
				String vs = nf.format(vEm * 1E6);
				String last = hs + "_" + vs;

				boolean lost = true;
				String filename = null;
				for (String key : fileList) {
					if (key.contains(last)) {
						int t = FileUtils.countLines(outDir + File.separator + key);
						if (t >= turns) {
							lost = false;
							filename = key;
							break;
						}
					}
				}

				if (lost) {
					names.put(previous, prevfi);
					break;
				}

				// last radial step
				if (ri == radialSteps - 1) {
					names.put(filename, fi);
					break;
				}

				previous = filename;
				prevfi = fi;
			}
		}
		Map<String, Double> sorted = names.entrySet().stream().sorted(Map.Entry.comparingByValue()).collect(Collectors
				.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));

		return sorted;
	}

	private Beam getBeamForDA(TwissParameters twissPars)
			throws IOException, OutOfApertureException, KnownFatalException {

		List<Particle> particles = new ArrayList<>();

		double dEm = (emMax - emMin) / radialSteps;
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);

		ParticleFactory pf = new ParticleFactory(refOrbit);

		for (int ri = 0; ri < radialSteps; ri++) {
			for (int ai = 1; ai < angleSteps; ai++) {

				double fi = ai * Math.PI / (2 * angleSteps);
				double horAmpl = Math.sin(fi);
				double vertAmpl = Math.cos(fi);
				double hEm = horAmpl * (emMin + ri * dEm);
				double vEm = vertAmpl * (emMin + ri * dEm);

				PhaseSpaceCoordinates phsCoords = twissPars.getPhaseSpaceCoordinates(hEm, 0, vEm, 0,0,0);

				String hs = nf.format(hEm * 1E6); // [pi mm mrad]
				String vs = nf.format(vEm * 1E6);
				Particle p = pf.getParticle(momentum, phsCoords, 0, potProvider, ParticleType.ANTIPROTON);
				p.setName(hs + "_" + vs);
				particles.add(p);

			}
		}

		Beam b = new Beam(particles,  refOrbit, potProvider, outDir);
		return b;
	}

	 
}
