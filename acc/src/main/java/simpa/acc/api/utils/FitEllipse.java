/**
 * FitEllipse Copyright 2009 2010 Michael Doube This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version. This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details. You should have received a copy of the GNU General Public
 * License along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package simpa.acc.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.util.ArrayList;
import java.util.List;

/**
 * Ellipse-fitting methods.
 *
 * @author Michael Doube, modified by Lajos Bojtar
 */
public class FitEllipse {

	private double[] fitresult; /// Algebraic parameters of the fitted ellipse
	private double xmax, xmin, ymax, ymin, yAtXmax, xAtYmax;

	/**
	 * Fits an ellipse to all the points given in the x and y arrays.
	 *
	 * @param xa X points.
	 * @param ya Y points.
	 */
	public void fitData(final double[] xa, final double[] ya) {
		fitData(xa, ya, 0, xa.length);
	}

	/**
	 * Fits an ellipse to the points given in the x and y arrays starting from
	 * startIndex until endIndex exclusive.
	 *
	 * @param xa         X points.
	 * @param ya         Y points.
	 * @param startIndex index to start from (inclusive)
	 * @param endIndex   index to end (exclusive)
	 */
	public void fitData(final double[] xa, final double[] ya, int startIndex, int endIndex) {
		int n = endIndex - startIndex;

		double[][] points = new double[n][2];
		for (int i = 0; i < n; i++) {
			points[i][0] = xa[startIndex + i];
			points[i][1] = ya[startIndex + i];
		}

		fitData(points);
	

	}

	/**
	 * <p>
	 * Convert variables a, b, c, d, f, g from the general ellipse equation ax^2 + b
	 * y^2 + c xy +dx + fy + g = 0 into useful geometric parameters semi-axis
	 * lengths, centre and angle of rotation.
	 * </p>
	 *
	 * @return
	 *         <p>
	 *         array containing centroid coordinates, axis lengths and angle of
	 *         rotation of the ellipse specified by the input variables.
	 *         </p>
	 *         <p>
	 *         Eq. 19-23 at
	 *         <a href="http://mathworld.wolfram.com/Ellipse.html">Wolfram Mathworld
	 *         Ellipse</a>.
	 *         </p>
	 */
	public double[] getDimensions() {

		final double a = fitresult[0];
		final double b = fitresult[1] / 2;
		final double c = fitresult[2];
		final double d = fitresult[3] / 2;
		final double f = fitresult[4] / 2;
		final double g = fitresult[5];

		// centre
		final double cX = (c * d - b * f) / (b * b - a * c);
		final double cY = (a * f - b * d) / (b * b - a * c);

		// semiaxis length
		final double af = 2 * (a * f * f + c * d * d + g * b * b - 2 * b * d * f - a * c * g);
		final double aL = Math.sqrt((af) / ((b * b - a * c) * (Math.sqrt((a - c) * (a - c) + 4 * b * b) - (a + c))));
		final double bL = Math.sqrt((af) / ((b * b - a * c) * (-Math.sqrt((a - c) * (a - c) + 4 * b * b) - (a + c))));

		double phi = 0;
		if (b == 0) {
			if (a <= c)
				phi = 0;
			else
				phi = Math.PI / 2;
		} else {
			if (a < c)
				phi = Math.atan(2 * b / (a - c)) / 2;
			else if (a > c)
				phi = Math.atan(2 * b / (a - c)) / 2 + Math.PI / 2;
		}
		return new double[] { cX, cY, aL, bL, phi };
	}

	public List<Vector2D> getPlotData(int n) {
		List<Vector2D> l = new ArrayList<>();
		double dfi = 2 * Math.PI / n;
		for (int i = 0; i < n; i++) {
			l.add(getPointOnEllipse(dfi * i));
		}
		return l;
	}

	/**
	 * Gets a point on the ellipse with the given phase
	 *
	 * @param fi phase
	 * @return A Vector2D object containing the coordinates of the point.
	 *
	 */
	public Vector2D getPointOnEllipse(double fi) {
		double[] dims = getDimensions();

		Rotation rot = new Rotation(Vector3D.PLUS_K, dims[4], RotationConvention.VECTOR_OPERATOR);

		double x = dims[2] * Math.sin(fi);
		double y = dims[3] * Math.cos(fi);
		Vector3D v = new Vector3D(x, y, 0);
		v = rot.applyTo(v);
		Vector3D center = new Vector3D(dims[0], dims[1], 0);
		v = v.add(center);

		return new Vector2D(v.getX(), v.getY());

	}

	/**
	 * @return Maximum Y value of the fitted ellipse
	 */
	public double getYMax() {
		return ymax;
	}

	/**
	 * @return Minimum Y value of the fitted ellipse
	 */
	public double getYMin() {
		return ymin;
	}

	/**
	 * @return Maximum X value of the fitted ellipse
	 */
	public double getXMax() {
		return xmax;
	}

	/**
	 * @return Minimum X value of the fitted ellipse
	 */
	public double getXMin() {
		return xmin;
	}

	/**
	 * @return Y value of the fitted ellipse at Xmax
	 */
	public double getYatXmax() {
		return yAtXmax;
	}

	/**
	 * @return X value of the fitted ellipse at Ymax
	 */
	public double getXatYmax() {
		return xAtYmax;
	}

	private void calculateXExtreems() {

		double[] res = new double[2];

		double a = fitresult[0];
		double b = fitresult[1];
		double c = fitresult[2];
		double d = fitresult[3];
		double f = fitresult[4];
		double g = fitresult[5];

		double b2 = b * b;
		double c2 = c * c;
		double d2 = d * d;

		double sqrt = Math
				.sqrt(b2 * c2 * d2 - b2 * b * c * d * f + a * b2 * c * f * f + b2 * b2 * c * g - 4 * a * b2 * c2 * g);

		// Y at maximum or minimum X
		double y1 = (b * c * d - 2 * a * c * f - sqrt) / (-b2 * c + 4 * a * c2);
		double y2 = (b * c * d - 2 * a * c * f + sqrt) / (-b2 * c + 4 * a * c2);

		res[0] = (-f - 2 * c * y1) / b;
		res[1] = (-f - 2 * c * y2) / b;

		if (res[0] >= res[1]) {
			xmax = res[0];
			xmin = res[1];
			yAtXmax = y1;
		} else {
			xmax = res[1];
			xmin = res[0];
			yAtXmax = y2;
		}

	}

	/**
	 * We got these formulas by this Mathematica code: elleq = a x x + b x y + c y y
	 * + d x + f y + g; soly = Solve[D[elleq, x] == 0 , y] xAtYminmax = Solve[elleq
	 * == 0 /. soly, x]
	 *
	 */
	private void calculateYExtreems() {

		double[] res = new double[2];

		double a = fitresult[0];
		double b = fitresult[1];
		double c = fitresult[2];
		double d = fitresult[3];
		double f = fitresult[4];
		double g = fitresult[5];

		double a2 = a * a;
		double b2 = b * b;
		double d2 = d * d;

		double sqrt = Math
				.sqrt(a * b2 * c * d2 - a * b2 * b * d * f + a2 * b2 * f * f + a * b2 * b2 * g - 4 * a2 * b2 * c * g);
		// X at maximum or minimum Y
		double x1 = (-2 * a * c * d + a * b * f - sqrt) / (-a * b2 + 4 * a2 * c);
		double x2 = (-2 * a * c * d + a * b * f + sqrt) / (-a * b2 + 4 * a2 * c);

		res[0] = (-d - 2 * a * x1) / b;
		res[1] = (-d - 2 * a * x2) / b;

		if (res[0] >= res[1]) {
			ymax = res[0];
			ymin = res[1];
			xAtYmax = x1;
		} else {
			ymax = res[1];
			ymin = res[0];
			xAtYmax = x2;
		}
	}

	private RealMatrix arrayTimes(RealMatrix A, RealMatrix B) {
		double[][] C = B.getData();

		for (int i = 0; i < B.getRowDimension(); i++) {
			for (int j = 0; j < B.getColumnDimension(); j++) {
				C[i][j] = A.getEntry(i, j) * B.getEntry(i, j);
			}
		}

		return new Array2DRowRealMatrix(C);
	}

	private RealMatrix setMatrix(RealMatrix A, int i0, int i1, int j0, int j1, RealMatrix X) {
		try {
			for (int i = i0; i <= i1; i++) {
				for (int j = j0; j <= j1; j++) {
					A.setEntry(i, j, X.getEntry(i - i0, j - j0));
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			throw new ArrayIndexOutOfBoundsException("Submatrix indices");
		}
		return A;
	}

	private double[] getColumnPackedCopyOfMatrix(RealMatrix A) {
		int m = A.getRowDimension();
		int n = A.getColumnDimension();

		double[] vals = new double[m * n];
		for (int i = 0; i < m; i++) {
			for (int j = 0; j < n; j++) {
				vals[i + j * m] = A.getEntry(i, j);
			}
		}
		return vals;
	}

	/**
	 * Java port of Chernov's MATLAB implementation of the direct ellipse fit
	 *
	 * @param points n * 2 array of 2D coordinates.
	 * @see <a href=
	 *      "http://www.mathworks.co.uk/matlabcentral/fileexchange/22684-ellipse-fit-direct-method"
	 *      >MATLAB script</a>
	 */
	private void fitData(final double[][] points) {
		final int nPoints = points.length;

		if (nPoints < 6) {
			throw new IllegalArgumentException("ERROR in FitEllipse: At least 6 points needed to fit an ellipse.");
		}

		final double[] centroid = getCentroid(points);
		final double xC = centroid[0];
		final double yC = centroid[1];
		final double[][] d1 = new double[nPoints][3];
		for (int i = 0; i < nPoints; i++) {
			final double xixC = points[i][0] - xC;
			final double yiyC = points[i][1] - yC;
			d1[i][0] = xixC * xixC;
			d1[i][1] = xixC * yiyC;
			d1[i][2] = yiyC * yiyC;
		}
		final RealMatrix D1 = new Array2DRowRealMatrix(d1);

		final double[][] d2 = new double[nPoints][3];
		for (int i = 0; i < nPoints; i++) {
			d2[i][0] = points[i][0] - xC;
			d2[i][1] = points[i][1] - yC;
			d2[i][2] = 1;
		}
		final RealMatrix D2 = new Array2DRowRealMatrix(d2);

		final RealMatrix S1 = D1.transpose().multiply(D1);

		final RealMatrix S2 = D1.transpose().multiply(D2);

		final RealMatrix S3 = D2.transpose().multiply(D2);

		final RealMatrix T = new Array2DRowRealMatrix(
				(MatrixUtils.inverse(S3).scalarMultiply(-1)).multiply(S2.transpose()).getData());

		final RealMatrix M = S1.add(S2.multiply(T));

		final double[][] m = M.getData();
		final double[][] n = { { m[2][0] / 2, m[2][1] / 2, m[2][2] / 2 }, { -m[1][0], -m[1][1], -m[1][2] },
				{ m[0][0] / 2, m[0][1] / 2, m[0][2] / 2 } };

		final RealMatrix N = new Array2DRowRealMatrix(n);

		EigenDecomposition ed = new EigenDecomposition(N);

		final RealMatrix eVec = ed.getV();

		final RealMatrix R1 = eVec.getSubMatrix(0, 0, 0, 2);
		final RealMatrix R2 = eVec.getSubMatrix(1, 1, 0, 2);
		final RealMatrix R3 = eVec.getSubMatrix(2, 2, 0, 2);

		RealMatrix cond = arrayTimes(R1.scalarMultiply(4), R3).subtract(arrayTimes(R2, R2));

		int f = 0;
		for (int i = 0; i < 3; i++) {
			if (cond.getEntry(0, i) > 0) {
				f = i;
				break;
			}
		}
		final RealMatrix A1 = eVec.getSubMatrix(0, 2, f, f);

		RealMatrix A = new Array2DRowRealMatrix(6, 1);
		setMatrix(A, 0, 2, 0, 0, A1);
		setMatrix(A, 3, 5, 0, 0, T.multiply(A1));

		final double[] a = getColumnPackedCopyOfMatrix(A);
		final double a4 = a[3] - 2 * a[0] * xC - a[1] * yC;
		final double a5 = a[4] - 2 * a[2] * yC - a[1] * xC;
		final double a6 = a[5] + a[0] * xC * xC + a[2] * yC * yC + a[1] * xC * yC - a[3] * xC - a[4] * yC;
		A.setEntry(3, 0, a4);
		A.setEntry(4, 0, a5);
		A.setEntry(5, 0, a6);
		A = A.scalarMultiply(1 / A.getFrobeniusNorm());
		fitresult = getColumnPackedCopyOfMatrix(A);

		calculateYExtreems();
		calculateXExtreems();
	}

	/**
	 * Find the centroid of a set of points in double[n][2] format
	 *
	 * @param points set of points
	 * @return centroid
	 */
	private double[] getCentroid(final double[][] points) {
		final double[] centroid = new double[2];
		double sumX = 0;
		double sumY = 0;
		final int nPoints = points.length;

		for (double[] point : points) {
			sumX += point[0];
			sumY += point[1];
		}

		centroid[0] = sumX / nPoints;
		centroid[1] = sumY / nPoints;

		return centroid;
	}

}
