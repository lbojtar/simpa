/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api.utils;

import org.apache.commons.math3.distribution.MultivariateNormalDistribution;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.commons.math3.random.RandomGenerator;

import java.util.ArrayList;
import java.util.List;

import simpa.acc.api.TwissParameters;
import simpa.core.api.track.PhaseSpaceCoordinates;

/**
 * This class can be used to generate the phase space coordinated for the
 * particles in a beam with a Gaussian distribution.
 */
public class GaussianBeamSampler implements BeamSampler {

	private MultivariateNormalDistribution distribution;
	private RandomGenerator randomGenerator = new JDKRandomGenerator();

	/**
	 * Constructs a 6D distribution corresponding to the given accelerator optical
	 * and beam parameters. This assumes that there is no coupling between the
	 * planes. All particles are generated at the same longitudinal position.
	 * 
	 * 
	 * @param tp              Twiss parameters to be used to generate the bunch.
	 * @param randomGenerator Random generator to be used to generate the random
	 *                        numbers.
	 * 
	 */
	public GaussianBeamSampler(TwissParameters tp, RandomGenerator randomGenerator) {
		this.randomGenerator = randomGenerator;
		init(tp);
	}

	/**
	 * Constructs a 6D distribution corresponding to the given accelerator optical
	 * and beam parameters. This assumes that there is no coupling between the
	 * planes. All particles are generated at the same longitudinal position. For
	 * emmitance definition see:
	 * https://cas.web.cern.ch/sites/default/files/lectures/dourdan-2008/braun-emittance.pdf
	 * 
	 * 
	 * @param tp Twiss parameters to be used to generate the bunch.
	 * 
	 */

	public GaussianBeamSampler(TwissParameters tp) {
		init(tp);
	}

	private void init(TwissParameters tp) {
		double means[] = new double[6];
		means[0] = tp.hEllipse().pos();
		means[1] = tp.hEllipse().angle();
		means[2] = tp.vEllipse().pos();
		means[3] = tp.vEllipse().angle();
		means[4] = 0;
		means[5] = 0;

		double dpOp = tp.dpOp();
		distribution = getDistribution(means, tp.hEllipse().alpha(), tp.hEllipse().beta(), tp.hEllipse().em(),
				tp.vEllipse().alpha(),
				tp.vEllipse().beta(), tp.vEllipse().em(), 0, 1, dpOp * dpOp);
	}

	/**
	 * Constructs a 6D distribution corresponding to the given accelerator optical
	 * and beam parameters. This assumes that there is no coupling between the
	 * planes.
	 * 
	 * @param tp   4D Twiss parameters to be used to generate the bunch. The H and V
	 *             emittances are taken from the Twiss parameters
	 * @param dpOp RMS momentum spread.
	 */

	public GaussianBeamSampler(TwissParameters tp, double dpOp) {

		double means[] = new double[6];
		means[0] = tp.hEllipse().pos();
		means[1] = tp.hEllipse().angle();
		means[2] = tp.vEllipse().pos();
		means[3] = tp.vEllipse().angle();
		means[4] = 0;
		means[5] = 0;

		distribution = getDistribution(means, tp.hEllipse().alpha(), tp.hEllipse().beta(), tp.hEllipse().em(),
				tp.vEllipse().alpha(), tp.vEllipse().beta(), tp.vEllipse().em(), 0, 1, dpOp * dpOp);
	}

	/**
	 * Based on the beam matrix it constructs a Gaussian beam sampler.
	 * 
	 * @param bm - 6D Beam matrix
	 */
	public GaussianBeamSampler(BeamMatrix bm) {
		this.distribution = new MultivariateNormalDistribution(bm.getMeans(), bm.getCovarianceMatrix());
	}

	@Override
	public List<PhaseSpaceCoordinates> sample(int n) {
		List<PhaseSpaceCoordinates> list = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			double[] d = distribution.sample();
			PhaseSpaceCoordinates phs = new PhaseSpaceCoordinates(d[0], d[1], d[2], d[3], d[4], d[5]);
			list.add(phs);
		}
		return list;
	}

	public MultivariateNormalDistribution getDistribution() {
		return this.distribution;
	}

	private MultivariateNormalDistribution getDistribution(double[] means, double alphaH, double betaH, double emH,
			double alphaV, double betaV, double emV, double alphaL, double betaL, double emL) {
		double gammaH = (1 + alphaH * alphaH) / betaH;
		double gammaV = (1 + alphaV * alphaV) / betaV;
		double gammaL = (1 + alphaL * alphaL) / betaL;

		// @formatter:off
		double[][] covariance = { { betaH * emH, -alphaH * emH, 0, 0, 0, 0 },
								  { -alphaH * emH, gammaH * emH, 0, 0, 0, 0 },
								  { 0, 0, betaV * emV, -alphaV * emV, 0, 0 },
								  { 0, 0, -alphaV * emV, gammaV * emV, 0, 0 }, 
								  { 0, 0, 0, 0, betaL * emL, -alphaL * emL },
								  { 0, 0, 0, 0, -alphaL * emL, gammaL * emL } };

		
		return new MultivariateNormalDistribution(randomGenerator,means, covariance);
	}

}
