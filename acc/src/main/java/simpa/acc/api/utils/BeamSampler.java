package simpa.acc.api.utils;

import java.util.List;

import simpa.core.api.track.PhaseSpaceCoordinates;

public interface BeamSampler {
    public List<PhaseSpaceCoordinates> sample(int n);
   
}
