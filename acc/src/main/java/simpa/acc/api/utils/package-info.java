/**
 * This package contains utility classes to help in using the simpa.acc module.
 */
package simpa.acc.api.utils;