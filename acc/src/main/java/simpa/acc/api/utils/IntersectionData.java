package simpa.acc.api.utils;

import simpa.core.api.track.ObserverDisk;
import simpa.core.api.track.PhaseSpaceCoordinates;

/**
 * Record to hold data for intersection of particle trajectory with {@link ObserverDisk}
 */
public record IntersectionData (int orbitIndex,String particleName,PhaseSpaceCoordinates phaseSpace) {};


