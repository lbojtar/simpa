/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api.utils;

import org.apache.commons.math3.linear.EigenDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.TwissParameters;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.acc.api.track.PhaseSpacePlane;
import simpa.core.api.utils.FileUtils;

import java.io.FileNotFoundException;
import java.io.IOException;


/**
 * @author lbojtar
 * 
 *         This class calculates the beam co-variance matrix from a file
 *         containing the phase space coordinates of a beam. The input text file
 *         should contain 6 columns of numbers separated by spaces. These are:
 *         x,x'y,y',z,dp/p
 * 
 */
public class BeamMatrix {

	private static final Logger logger = LogManager.getLogger(BeamMatrix.class);
	private double[][] cov;
	double[] mean;

	/**
	 * Creates an instance from a file containing beam parameters in the following
	 * order: x,x'y,y',z,dp/p It calculates the Twiss parameters of the beam, which
	 * can be get with the access methods.
	 * 
	 * @param fileName Input file containing the phase space coordinates.
	 */
	public BeamMatrix(String fileName) throws FileNotFoundException {
		calcCovarianceMatrix(FileUtils.read2DArray(fileName, 6));
	}

	/**
	 * Calculates the Twiss parameters from a beam phase space coordinates file,then
	 * print it to the stdout. This is a subset of Twiss parameters, only those
	 * which can be calculated from a single phase space file. It does not contain
	 * the dispersion and the phase advances. The input file should contain the
	 * phase space coordinates at a some longitudinal position.
	 *
	 * @param phaseSpaceFile Input file containing the phase space coordinates for a
	 *                       bunch. If it contains a line with
	 *                       #LONGITUDINAL_POSITION="some value" then the
	 *                       TwissParametrs object will contain this number,
	 *                       otherwise it is set to zero.
	 * @return Those Twiss parameters which can be calculated from a beam phase
	 *         space file.
	 * @throws IOException When the file can't be read.
	 */
	public static TwissParameters getTwissParameters(String phaseSpaceFile) throws IOException {
		String key = "#LONGITUDINAL_POSITION=";
		String line = FileUtils.getKeyLineFromFile(phaseSpaceFile, key);
		String s = line.replace(key, "");
		double longiPos = 0;
		try {
			longiPos = Double.parseDouble(s);
		} catch (NumberFormatException e) {
			logger.error("Can't parse longitudinal position from file: " + phaseSpaceFile + " set to zero");
		}
		BeamMatrix bm= new BeamMatrix( phaseSpaceFile);
		PhaseSpaceEllipse he = getPhaseSpaceEllipse(bm, PhaseSpacePlane.HORIZONTAL);
		PhaseSpaceEllipse ve = getPhaseSpaceEllipse(bm, PhaseSpacePlane.VERTICAL);

		// mu and dispersion is not contained in a single phase space file
		return TwissParameters.builder().hEllipse(he).vEllipse(ve).longiPos(longiPos).muH(0).muV(0).dispH(0).dispV(0)
				.dispPrimeH(0).dispPrimeV(0).build();

	}

	private static PhaseSpaceEllipse getPhaseSpaceEllipse(BeamMatrix bm, PhaseSpacePlane plane) throws FileNotFoundException {
		
		// double[] twiss = new double[3];
		int idx = 0; // will stay 0 for a horizontal phase space plane
		double em;

		em = bm.getEmittance(plane);

		if (plane == PhaseSpacePlane.VERTICAL)
			idx = 2;

		if (plane == PhaseSpacePlane.LONGITUDINAL)
			throw new IllegalArgumentException("Longitudinal plane is not yet supported");

		double pos = bm.mean[idx];
		double angle = bm.mean[idx + 1];
		double alpha = -bm.cov[idx + 1][idx] / em; // //<x,x'> = -alpha
		double beta = bm.cov[idx][idx] / em; // beta

		return new PhaseSpaceEllipse( em, alpha, beta, pos, angle);
	}

	/**
	 * Prints the twiss parameters to the console.
	 *
	 * @param twiss The twiss parameters.
	 */
	public void printTwissParameters(double[] twiss) {
		logger.info("alpha= " + twiss[0]);
		logger.info("beta= " + twiss[1]);
		logger.info("gamma= " + twiss[2]);
	}

	/**
	 * Calculates the 1 sigma RMS emittance in [ m rad] BE CAREFULL! THERE ARE
	 * SEVERAL EMITTANCE DEFINITIONS See:
	 * https://uspas.fnal.gov/materials/10MIT/Emittance.pdf
	 * 
	 * @param plane - plane
	 * @return emmitance [ m rad]
	 */
	public double getEmittance(PhaseSpacePlane plane) {

		double[] eigv;
		double[][] sd = { { 0, 1 }, { -1, 0 } };

		RealMatrix s = MatrixUtils.createRealMatrix(sd);

		RealMatrix m = MatrixUtils.createRealMatrix(cov);
		RealMatrix sigma = null;

		if (plane == PhaseSpacePlane.HORIZONTAL)
			sigma = m.getSubMatrix(0, 1, 0, 1);

		if (plane == PhaseSpacePlane.VERTICAL)
			sigma = m.getSubMatrix(2, 3, 2, 3);

		if (plane == PhaseSpacePlane.LONGITUDINAL)
			sigma = m.getSubMatrix(4, 5, 4, 5);

		@SuppressWarnings("null")
		RealMatrix sts = sigma.multiply(s);

		EigenDecomposition ed = new EigenDecomposition(sts);

		eigv = ed.getImagEigenvalues();

		return eigv[0];
	}

	public double[][] getCovarianceMatrix() {
		return cov;
	}

	public double[] getMeans() {
		return mean;
	}

	public void calcCovarianceMatrix(double[][] data) {
		cov = new double[6][6];

		double[] mean = getMeans(data);

		for (double[] coords : data) {

			for (int i = 0; i < 6; i++) {
				for (int j = 0; j < 6; j++) {
					cov[i][j] = cov[i][j] + ((coords[i] - mean[i]) * (coords[j] - mean[j])) / data.length;
				}
			}

		}

	}

	public double[] getMeans(double[][] data) {

		mean = new double[6];
		int count = 0;
		// summ
		for (double[] datum : data) {
			for (int col = 0; col < 6; col++) {
				mean[col] += datum[col];
			}
			count++;
		}

		// calc the average
		for (int i = 0; i < 6; i++) {
			mean[i] = mean[i] / count;
		}

		return mean;
	}
}
