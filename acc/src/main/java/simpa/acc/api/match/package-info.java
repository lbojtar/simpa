/**
 * This package contains all the interfaces and classes related to optics
 * matching or any other optimization which is performed by tracking beams or
 * particles.
 */
package simpa.acc.api.match;