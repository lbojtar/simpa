package simpa.acc.api.match;

import simpa.core.api.FileNamingConventions;

/**
 * Describes a variable to be matched or optimized, usually a scaling factor of
 * a field map. When ititial conditions of a beam line are matched the maching
 * variales are the Twiss parameters of the beam.
 */
public class MatchingVariable {

	private Double min = Double.MIN_VALUE;
	private Double max = Double.MAX_VALUE;
	private Double initialGuess = 1.0;
	private String fieldMapName;
	private Double value;
	private MatchingVariableType matchingVariableType = MatchingVariableType.SCALING_FACTOR;

	/**
	 * Constructor for matching variables. If the matching variable is a field map
	 * scaling factor the field map name should be provided.
	 * 
	 * @param matchingVariableType - The type of the matching variable
	 * @param fieldMapName -The file name of a field map with extension (
	 *                     {@link FileNamingConventions#BIN_FILE_EXTENSION}) The
	 *                     scaling factor for this field map will be sought during
	 *                     matching. It can be null if the matching variable is not a
	 *	 				   field map scaling factor.
	 */
	public MatchingVariable(MatchingVariableType matchingVariableType, String fieldMapName) {
		if(matchingVariableType==MatchingVariableType.SCALING_FACTOR && fieldMapName==null) {
			throw new IllegalArgumentException("Field map name must be set for a scaling factor matching variable");
		}
		this.matchingVariableType = matchingVariableType;
		this.fieldMapName = fieldMapName;
		
	}

	/**
	 * 
	 * @return The minimum value for the scaling factor of the field map. This is
	 *         usually the same as the minimum value of the power supply of the
	 *         element, if the field map belongs to an element which has one.
	 */
	public Double getMin() {
		return min;
	}

	/**
	 * 
	 * @param min Sets the minimum value for the scaling factor of the field map.
	 *            This is usually the same as the minimum value of the power supply
	 *            of the element, if the field map belongs to an element which has
	 *            one.
	 */
	public void setMin(Double min) {
		this.min = min;
	}

	/**
	 * 
	 * @return The maximum value for the scaling factor of the field map. This is
	 *         usually the same as the maximum value of the power supply of the
	 *         element, if the field map belongs to an element which has one.
	 */
	public Double getMax() {
		return max;
	}

	/**
	 * 
	 * @param max Sets the maximum value for the scaling factor of the field map.
	 *            This is usually the same as the maximum value of the power supply
	 *            of the element, if the field map belongs to an element which has
	 *            one.
	 */
	public void setMax(Double max) {
		this.max = max;
	}

	/**
	 * 
	 * @return Initial guess for the variable
	 */
	public Double getInitialGuess() {
		return initialGuess;
	}

	/**
	 * 
	 * @param initialGuess Sets the initial guess for the variable
	 */
	public void setInitialGuess(Double initialGuess) {
		this.initialGuess = initialGuess;
	}

	/**
	 * 
	 * @return Name of the field map. When the MatchingVariableType is not
	 *         SCALING_FACTOR this is the vale is null.
	 */
	public String getFieldMapName() {
		return fieldMapName;
	}

	/**
	 * 
	 * @return Value of the this matching variable. Called during matching and
	 *         after.
	 */
	public Double getValue() {
		return value;
	}

	/**
	 * 
	 * @param value sets the value of the this matching variable. Called during
	 *              matching and after.
	 */
	public void setValue(Double value) {
		this.value = value;
	}

	/**
	 * 
	 * @return The type of the matching variable.
	 */
	public MatchingVariableType getMatchingVariableType() {
		return matchingVariableType;
	}

}
