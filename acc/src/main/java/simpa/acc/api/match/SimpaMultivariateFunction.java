package simpa.acc.api.match;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.analysis.MultivariateFunction;
import simpa.acc.api.create.Sequence;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class SimpaMultivariateFunction implements MultivariateFunction {

	/**
	 * The loss penalty is a large number that is added to the objective function
	 * value for each lost particle during the tracking. 
	 */
	public static final double DEFAULT_LOSS_PENALTY = 1e6;

    protected static double lossPenalty = DEFAULT_LOSS_PENALTY;

	protected List<MatchingVariable> matchingVariables;
	protected Map<String, Double> scalings;
	protected Sequence sequence;
	protected boolean backward = false;
	protected double stepSize;
	protected double diskRadius;
	protected List<MatchingTarget> targets;
	protected int evaluations = 0;
	protected StringBuilder infoSb;

	protected static final Logger logger = LogManager.getLogger(SimpaMultivariateFunction.class);

	public SimpaMultivariateFunction(Sequence sequence, Map<String, Double> scalings, List<MatchingVariable> variables,
			List<MatchingTarget> targets, double stepSize, boolean backward, double diskRadius) {

		for (MatchingVariable v : variables) {

			if (v.getMatchingVariableType() == MatchingVariableType.SCALING_FACTOR
					&& !sequence.getScalingMap().keySet().contains(v.getFieldMapName()))
				throw new IllegalArgumentException("The field map: " + v.getFieldMapName()
						+ " is not in the scaling map of the sequence " + sequence.getName());
		}

		this.sequence = sequence;
		this.scalings = scalings;
		this.matchingVariables = variables;
		this.targets = targets;
		this.stepSize = stepSize;
		this.backward = backward;
		this.diskRadius = diskRadius;
	}

		/**
	 * @param penalty The loss penalty is a large number that is added to the
	 *                    objective function value if the beam is lost during the
	 *                    tracking. The optimizer will try to avoid this situation. 
	 * 					  The default value is 1E6.
	 */
	public static void setLosssPenaly(double penalty) {
		lossPenalty = penalty;
	}
	
	@Override
	public abstract double value(double[] point);

	
	protected int findFieldMapIndex(String name) {
		for (int i = 0; i < matchingVariables.size(); i++) {
			String fmn = matchingVariables.get(i).getFieldMapName();
			if (fmn!=null && fmn.equals(name))
				return i;
		}
		return -1;
	}


	 
	protected void setMatchingVariables(double[] point) {
		
		MatchingVariable mvar;
		for (int i = 0; i < point.length; i++) {
			mvar = matchingVariables.get(i);
			mvar.setValue(point[i]);
		}

		Map<String, Double> map = new HashMap<>();

		for (String key : scalings.keySet()) {
			int i = findFieldMapIndex(key);
			if (i == -1)
				map.put(key, scalings.get(key));
			else {
				map.put(key, point[i]);
			}
		}

		try {
			sequence.updateScalingMap(map);
		} catch (IOException e1) {
			logger.error(e1);
		}
	}
	
}
