package simpa.acc.api.match;
/**
 * Enum describing the type of the matching value.
 */
public enum MatchingValueType {  EXACT_VALUE,MINIMUM,MAXIMUM}

