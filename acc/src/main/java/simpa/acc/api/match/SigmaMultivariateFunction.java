package simpa.acc.api.match;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import simpa.acc.api.Beam;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.Sequence;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.acc.api.track.TLBeamTracker;
import simpa.acc.api.track.TLPhaseSpaceObserver;
import simpa.acc.api.utils.FakeRandomGenerator;
import simpa.acc.api.utils.GaussianBeamSampler;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.track.Particle;
import simpa.core.api.track.PhaseSpaceCoordinates;

public class SigmaMultivariateFunction extends SimpaMultivariateFunction {

    private TwissParameters initialTwiss;
    private Sequence sequence;
    private Beam beam0;
    private TLBeamTracker tlbt;
    private GaussianBeamSampler beamSampler;
    private FakeRandomGenerator fakeRandomGenerator;
    private Beam beam;

    public SigmaMultivariateFunction(Beam beam, TwissParameters initialTwiss, Sequence sequence,
            List<MatchingVariable> variables, List<MatchingTarget> targets, double stepSize, boolean backward,
            double diskRadius) {

        super(sequence, sequence.getScalingMap(), variables, targets, stepSize, backward, diskRadius);
        this.initialTwiss = initialTwiss;
        this.sequence = sequence;
        this.beam0 = beam;
        fakeRandomGenerator = new FakeRandomGenerator();

        // we want to have reproducible gaussian beam during the matching
        beamSampler = new GaussianBeamSampler(initialTwiss, fakeRandomGenerator);
        beamSampler.sample(beam0.getParticles().size()); // this stores the ramdom values in the fake random generator
        fakeRandomGenerator.setReplay(true); // this will make the fake random generator to return the stored values
    }

    @Override
    public double value(double[] point) {

        if (sequence.isCircular())
            throw new IllegalArgumentException("Beam size matching is not implemented for circular machines.");

        setMatchingVariables(point);

        double err = 0;
        // set observers
        List<Double> phsObsLocations = new ArrayList<>();
        // loop through the matching targets and collect all the longitudinal positions,
        // but no diplicates
        for (MatchingTarget mt : targets) {
            if (!phsObsLocations.contains(mt.getLongitudinalPosition())) {
                phsObsLocations.add(mt.getLongitudinalPosition());
            }
        }

        try {
            tlbt = new TLBeamTracker(beam, diskRadius, stepSize, phsObsLocations, backward);
            tlbt.setWritePhsTofile(false);
            tlbt.track();
            int lost = tlbt.getLossCount();

            infoSb = new StringBuilder();
            err = getResidualSum();

            err += lost * lossPenalty;
            evaluations++;

            infoSb.append("Matching evaluations = " + evaluations + System.lineSeparator());
            infoSb.append("Value of variables: [");
            for (MatchingVariable v : matchingVariables) {
                String variable;
                if (v.getFieldMapName() == null)
                    variable = v.getMatchingVariableType().toString();
                else
                    variable = v.getFieldMapName().toString();

                infoSb.append("{" + variable + " : " + v.getValue().toString() + "}, ");
            }
            infoSb.append("]" + System.lineSeparator());
            infoSb.append("RESIDUAL = " + err + System.lineSeparator());
            logger.info(infoSb);
        } catch (IOException e) {
            logger.error(e);
        }
        return err;
    }

    @Override
    public void setMatchingVariables(double[] point) {
        //this sets the scaling factors of the sequence
        super.setMatchingVariables(point);
        // this sets the initial conditions of the beam according to the matching
        // variables
        beam = copyAndSet(beam0, sequence.getPotentialProvider(), matchingVariables);

    }

    /**
     * A deep copy of of all particles in the beam, but the tracking state
     * is freshly created. The vector potential of the copy will be the
     * potential provider parameter. The beam sampler is freshly created according
     * to the matching parameter values. This method is typically used to create a
     * new beam with when the initial conditions of a transfer line is matched.
     * 
     * @param b                 The beam to be copied.
     * @param pp                The potential provider to be used for the new beam
     *                          particles.
     * @param matchingVariables The matching variables to be used to create the new
     *                          beam. In the list only the those matching variables
     *                          are used which specify the initial Twiss parameters.
     * 
     * @return Copy of the given beam with the given initial Twiss parameters.
     * 
     */
    public Beam copyAndSet(Beam b, PotentialProvider pp, List<MatchingVariable> matchingVariables) {

        TwissParameters tpo = initialTwiss;
        double alphaH = tpo.hEllipse().alpha();
        double betaH = tpo.hEllipse().beta();
        double emH = tpo.hEllipse().em();
        double posH = tpo.hEllipse().pos();
        double angleH = tpo.hEllipse().angle();
        double dispH = tpo.dispH();
        double dispPrimeH = tpo.dispPrimeH();

        double alphaV = tpo.vEllipse().alpha();
        double betaV = tpo.vEllipse().beta();
        double emV = tpo.vEllipse().em();
        double posV = tpo.vEllipse().pos();
        double angleV = tpo.vEllipse().angle();
        double dispV = tpo.dispV();
        double dispPrimeV = tpo.dispPrimeV();

        if (matchingVariables.size() > 0) {
            for (MatchingVariable mv : matchingVariables) {

                switch (mv.getMatchingVariableType()) {
                    case INITIAL_H_ALPHA:
                        alphaH = mv.getValue();
                        break;
                    case INITIAL_H_BETA:
                        betaH = mv.getValue();
                        break;
                    case INITIAL_H_EMITTANCE:
                        emH = mv.getValue() * 1e-6;
                        break;
                    case INITIAL_H_POSITION:
                        posH = mv.getValue();
                        break;
                    case INITIAL_H_ANGLE:
                        angleH = mv.getValue();
                        break;
                    case INITIAL_H_DISP:
                        dispH = mv.getValue();
                        break;
                    case INITIAL_V_ALPHA:
                        alphaV = mv.getValue();
                        break;
                    case INITIAL_V_BETA:
                        betaV = mv.getValue();
                        break;
                    case INITIAL_V_EMITTANCE:
                        emV = mv.getValue() * 1e-6;
                        break;
                    case INITIAL_V_POSITION:
                        posV = mv.getValue();
                        break;
                    case INITIAL_V_ANGLE:
                        angleV = mv.getValue();
                        break;
                    case INITIAL_V_DISP:
                        dispV = mv.getValue();
                        break;
                    case SCALING_FACTOR:                      
                        break; // do nothing, is already set by the super class
                    default:
                        throw new IllegalArgumentException(
                                "Unknown matching variable type: " + mv.getMatchingVariableType());
                }
            }
        }

        PhaseSpaceEllipse horPSE = new PhaseSpaceEllipse(emH, alphaH, betaH, posH, angleH);
        PhaseSpaceEllipse verPSE = new PhaseSpaceEllipse(emV, alphaV, betaV, posV, angleV);
        TwissParameters tp = TwissParameters.builder().copyOf(tpo).hEllipse(horPSE).vEllipse(verPSE).dispH(dispH)
                .dispPrimeH(dispPrimeH).dispV(dispV).dispPrimeV(dispPrimeV).build();

        fakeRandomGenerator.setReplay(true);
        beamSampler = new GaussianBeamSampler(tp, fakeRandomGenerator);

        try {
            double nomMomentum = b.getNominalMomentum();
            Particle p = b.getParticles().get(0);
            if (p == null) {
                throw new IllegalArgumentException("No particles in the beam");
            }

            Beam gb = new Beam(beamSampler, nomMomentum, sequence.getReferenceOrbit(), initialTwiss.longiPos(),
                    sequence.getPotentialProvider(), p.getMass(), p.getCharge(), b.getParticles().size(), b.getName());
            return gb;
        } catch (KnownFatalException e) {

            e.printStackTrace();
        }

        return null;
    }

    public double getResidualSum() {

        double err = 0;

        // loop through the mathing targets
        for (MatchingTarget mt : targets) {

            List<PhaseSpaceCoordinates> phsList = getPhaseSpaceCoordinates(mt.getLongitudinalPosition());
            int n = phsList.size();

            double averageX = 0;
            double averageY = 0;
            double averageHAngle = 0;
            double averageVAngle = 0;

            // we allow the beam to be off center
            for (PhaseSpaceCoordinates phs : phsList) {
                averageX += phs.x();
                averageY += phs.y();
                averageHAngle += phs.xp();
                averageVAngle += phs.yp();
            }

            averageX = averageX / n;
            averageY = averageY / n;
            averageHAngle = averageHAngle / n;
            averageVAngle = averageVAngle / n;

            double sh = 0;
            double sv = 0;
            for (PhaseSpaceCoordinates phs : phsList) {
                sh += (phs.x() - averageX) * (phs.x() - averageX);
                sv += (phs.y() - averageY) * (phs.y() - averageY);
            }
            sh = Math.sqrt(sh / n);
            sv = Math.sqrt(sv / n);

            double value = Double.POSITIVE_INFINITY;
            switch (mt.getType()) {
                case H_SIGMA:
                    value = sh;
                    break;
                case V_SIGMA:
                    value = sv;
                    break;
                case H_POSITION:
                    value = averageX;
                    break;
                case V_POSITION:
                    value = averageY;
                    break;
                case H_ANGLE:
                    value = averageHAngle;
                    break;
                case V_ANGLE:
                    value = averageVAngle;
                    break;
                default:
                    throw new IllegalArgumentException("Illegal matching target type: " + mt.getType());
            }

            double diff = value - mt.getTargetValue();
            err += Math.abs(diff);

            if (value != Double.POSITIVE_INFINITY)
                infoSb.append("Longi. pos: " + mt.getLongitudinalPosition() + " Target: " + mt.getType() + " = "
                        + value + "  Target value = " + mt.getTargetValue()
                        + "  Difference = " + diff + System.lineSeparator());
        }

        return err;
    }

    private List<PhaseSpaceCoordinates> getPhaseSpaceCoordinates(double longiPos) {
        // loop through all phase space observers and return the phase space coordinates
        // equal to the given longitudinal position
        List<PhaseSpaceCoordinates> phsList = new ArrayList<>();
        for (TLPhaseSpaceObserver pho : tlbt.getPhaseSpaceObservers()) {
            if (pho.getLongitudinalPosition() == longiPos) {
                pho.getMap().values().forEach(phsList::add);
            }
        }

        return phsList;
    }

}
