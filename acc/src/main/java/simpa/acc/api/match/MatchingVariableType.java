package simpa.acc.api.match;

/**
 * Enum representing various matching variable types. All matching variables
 * types belongs to the case when the matching is done for the initial beam
 * parameters usually to match unknown initial conditions of a transfer line.
 * The only exception is the SCALING_FACTOR, which is used when the scaling
 * factor of a field map is the matching variable.
 */
public enum MatchingVariableType {
    /**
     * Horizontal initial emittance [m*rad] at the beginning of the transfer line.
     * It should not be used for rings. The beam is created with a different
     * emmitance at each iteration of the matching.
     */
    INITIAL_H_EMITTANCE,
    /**
     * Horizontal initial alpha at the beginning of the transfer line. It should not
     * be
     * used for rings. The beam is created with a different alpha at each iteration
     * of the matching.
     */
    INITIAL_H_ALPHA,
    /**
     * Horizontal initial beta at the beginning of the transfer line. It should not
     * be
     * used for rings. The beam is created with a different beta at each iteration
     * of the matching.
     */
    INITIAL_H_BETA,
    /**
     * Horizontal initial position [m] at the beginning of the transfer line. It
     * should not be used for rings. The beam is created with a different position
     * at each iteration of the matching.
     */
    INITIAL_H_POSITION,
    /**
     * Horizontal initial angle [rad] at the beginning of the transfer line. It
     * should not be used for rings. The beam is created with a different angle at
     * each iteration of the matching.
     */
    INITIAL_H_ANGLE,
    /**
     * Horizontal initial dispersion [m] at the beginning of the transfer line. It
     * should not be used for rings. The beam is created with a different dispersion
     * at each iteration of the matching.
     */
    INITIAL_H_DISP,

    /**
     * Vertical initial emittance [m*rad] at the beginning of the transfer line. It
     * should not be used for rings. The beam is created with a different emmitance
     * at each iteration of the matching.
     */
    INITIAL_V_EMITTANCE,
    /**
     * Vertical initial alpha at the beginning of the transfer line. It should not
     * be
     * used for rings. The beam is created with a different alpha at each iteration
     * of the matching.
     */
    INITIAL_V_ALPHA,
    /**
     * Vertical initial beta [m] at the beginning of the transfer line. It should
     * not be used for rings.
     * The beam is created with a different beta at each iteration of the matching.
     */
    INITIAL_V_BETA,
    /**
     * Vertical initial position [m] at the beginning of the transfer line. It
     * should
     * not be used for rings. The beam is created with a different position at each
     * iteration of the matching.
     */
    INITIAL_V_POSITION,
    /**
     * Vertical initial angle [rad] at the beginning of the transfer line. It should
     * not be used for rings. The beam is created with a different angle at each
     * iteration of the matching.
     */
    INITIAL_V_ANGLE,
     /**
     * Vertical initial dispersion [m] at the beginning of the transfer line. It
     * should not be used for rings. The beam is created with a different dispersion
     * at each iteration of the matching.
     */
    INITIAL_V_DISP,

    /** Scaling factor for a fieldmap */
    SCALING_FACTOR;
}