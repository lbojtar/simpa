package simpa.acc.api.match;

/**
 * Enum listing the parameters that can be matched. It contains also the default
 * weight for each parameter.
 * 
 * @lbojtar
 *
 */
public enum MatchingTargetType {
	/**
	 * Horizontal position [m] of the fitted phase space ellipse center at a given
	 * longitudinal position in the sequence.
	 */
	H_POSITION(1.0),
	/**
	 * Vertical position [m] of the fitted phase space ellipse center at a given
	 * longitudinal position in the sequence.
	 */
	V_POSITION(1.0),
	/**
	 * Horizontal angle [rad] of the fitted phase space ellipse center at a given
	 * longitudinal position in the sequence.
	 */
	H_ANGLE(1.0),
	/**
	 * Vertical angle [rad] of the fitted phase space ellipse center at a given
	 * longitudinal position in the sequence.
	 */
	V_ANGLE(1.0),
	/**
	 * Horizontal alpha calculated from the the fitted phase space ellipse at a
	 * given longitudinal position in the sequence.
	 */
	H_ALPHA(1.0),
	/**
	 * Vertical alpha calculated from the the fitted phase space ellipse at a given
	 * longitudinal position in the sequence.
	 */
	V_ALPHA(1.0),
	/**
	 * Horizontal beta calculated from the the fitted phase space ellipse at a given
	 * longitudinal position in the sequence.
	 */
	H_BETA(1.0),
	/**
	 * Vertical beta calculated from the the fitted phase space ellipse at a given
	 * longitudinal position in the sequence.
	 */
	V_BETA(1.0),
	/**
	 * Horizontal phase advance [rad] calculated from the the fitted phase space
	 * ellipse at a given longitudinal position in the sequence.
	 */
	H_PHASE_ADVANCE(1.0),
	/**
	 * Vertical phase advance [rad] calculated from the the fitted phase space
	 * ellipse at a given longitudinal position in the sequence.
	 */
	V_PHASE_ADVANCE(1.0),
	/**
	 * Horizontal dispersion [m] calculated from the the fitted phase space ellipse
	 * at a given longitudinal position in the sequence.
	 */
	H_DISPERSION(1.0),
	/**
	 * Vertical dispersion [m] calculated from the the fitted phase space ellipse at
	 * a given longitudinal position in the sequence.
	 */
	V_DISPERSION(1.0),

	/**
	 * Horizontal sigma of the beam size [m] calculated at a given longitudinal
	 * position in the sequence.
	 */
	H_SIGMA(1.0),
	/**
	 * Vertical sigma of the beam size [m] calculated at a given longitudinal
	 * position in the sequence.
	 */
	V_SIGMA(1.0),

	/**
	 * Horizontal maximum beta [m] in the sequence.
	 */
	MAXIMUM_H_BETA(1.0, true),
	/**
	 * Vertical maximum beta [m] in the sequence.
	 */
	MAXIMUM_V_BETA(1.0, true),
	/**
	 * Horizontal maximum dispersion [m] in the sequence.
	 */
	MAXIMUM_H_DISPERSION(1.0, true),
	/**
	 * Vertical maximum dispersion [m] in the sequence.
	 */
	MAXIMUM_V_DISPERSION(1.0, true),
	/**
	 * Horizontal minimum dispersion [m] in the sequence. Useful when the dispersion
	 * is negative.
	 */
	MINIMUM_H_DISPERSION(1.0, true),
	/**
	 * Vertical minimum dispersion [m] in the sequence. Useful when the dispersion
	 * is negative.
	 */
	MINIMUM_V_DISPERSION(1.0, true),
	/**
	 * Maximum absolute value [m] of the horizontal dispersion in the sequence.
	 */
	MAXIMUM_ABS_H_POSITION(1.0, true),
	/**
	 * Maximum absolute value [m] of the vertical dispersion in the sequence.
	 */
	MAXIMUM_ABS_V_POSITION(1.0, true);


	private double defaultWeight;
	private boolean globalParameter = false;
	private boolean beamParameter = true;

	/**
	 * 
	 * @return The default weight for this parameter
	 */
	public double getDefaultWeight() {
		return defaultWeight;
	}

	/**
	 * 
	 * @return True if the parameter is global for the sequence.
	 */
	public boolean isGlobalParameter() {
		return globalParameter;
	}

	/**
	 * 
	 * @return True if the parameter is a result of an optics calculation.
	 *         When this is true the parameter is calculated during the matching.
	 *         Otherwise the tracking date will be processed otherwise for the
	 *         matching.
	 */
	public boolean isBeamParameter() {
		return beamParameter;
	}

	private MatchingTargetType(double w) {
		this.defaultWeight = w;
	}

	private MatchingTargetType(double w, boolean globalParameter) {
		this.defaultWeight = w;
		this.globalParameter = globalParameter;
	}

	private MatchingTargetType(double w, boolean globalParameter, boolean isBeamParameter) {
		this.defaultWeight = w;
		this.globalParameter = globalParameter;
		this.beamParameter = isBeamParameter;
	}
}
