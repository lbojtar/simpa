package simpa.acc.api.match;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.exception.MathArithmeticException;

import simpa.acc.api.EllipseBeam;
import simpa.acc.api.Optics;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.Sequence;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.acc.api.utils.OpticsCalculationException;
import simpa.acc.api.utils.OpticsCalculator;
import simpa.core.api.PotentialProvider;
import simpa.core.api.track.Particle;

/**
 * Objective function used for matching. The optimizer using this class to
 * calculate the residual to be minimized.
 */
public class OpticsMultivariateFunction extends SimpaMultivariateFunction {

	private Particle particle0, particle1;
	private EllipseBeam beam0, beam1;
	private OpticsCalculator opticsCalculator;

	private Optics optics;

	/**
	 * Constructor for the objective function used for optics matching. The matching
	 * is performed by tracking two beam in a transfer line with some momentum
	 * deviation.
	 * 
	 * @param sequence   The sequence to be used.
	 * @param scalings   The scaling map containing the file names of binary field
	 *                   maps as keys and scaling factors as values.
	 * @param variables  The list of variables to be optimized.
	 * @param targets    The list of matching targets. The number of targets and
	 *                   variables can be different.
	 * @param stepSize   Step size used for the tracking in [m].
	 * @param backward   Set true if the tracking is backward ( toward negative S)
	 * @param diskRadius Disk radius for the observers in[m].
	 */
	public OpticsMultivariateFunction(Sequence sequence, Map<String, Double> scalings, List<MatchingVariable> variables,
			List<MatchingTarget> targets, double stepSize, boolean backward, double diskRadius) {

		super(sequence, scalings, variables, targets, stepSize, backward, diskRadius);
		opticsCalculator = new OpticsCalculator(sequence.getReferenceOrbit());
		opticsCalculator.setPartialOpticsAllowed(false);
	}

	/**
	 * Set the two beams to be tracked for optics calculation in a transfer line.
	 * 
	 * @param b0 Ellipse beam with nominal momentum.
	 * @param b1 Ellipse beam with some momentum deviation to calculate the
	 *           dispersion.
	 */
	public void setBeams(EllipseBeam b0, EllipseBeam b1) {
		beam0 = b0;
		beam1 = b1;
	}

	/**
	 * Set the two particles to be tracked for optics calculation in a ring.
	 * 
	 * @param p0 Particle with nominal momentum.
	 * @param p1 Particle with some momentum deviation to calculate the dispersion.
	 */
	public void setParticles(Particle p0, Particle p1) {
		particle0 = p0;
		particle1 = p1;
	}

	/**
	 * Calculates the objective function value from the array of values for each
	 * variables. In case the target value is a lower or upper limit it has no
	 * effect on the objective function value until it reaches the limit. If the
	 * limit is reached the objective function changes in a stepwise manner to a
	 * large value.
	 */
	@Override
	public double value(double[] point) {
		
		setMatchingVariables(point);
		
		PotentialProvider pp = sequence.getPotentialProvider();

		try {
			if (sequence.isCircular()) {
				Particle p0 = particle0.copy(particle0, pp);
				Particle p1 = particle1.copy(particle1, pp);
				optics = opticsCalculator.trackInRingAndCalculate(p0, p1, pp, stepSize, diskRadius, false);

			} else {
				EllipseBeam b0 = (EllipseBeam) beam0.copy(beam0, pp);
				EllipseBeam b1 = (EllipseBeam) beam1.copy(beam1, pp);
				optics = opticsCalculator.trackInLineAndCalculate(false, b0, b1, pp, stepSize, diskRadius,
						backward);
			}
			evaluations++;
		} catch (IOException e) {
			logger.error(e);
		} catch (OpticsCalculationException e) {
			// lost particles, return a big residual, but proportional to the loss count
			return lossPenalty * e.getLostCount();
		} catch (MathArithmeticException e) {
			// ellipse fitting failed, return a big residual
			return lossPenalty*1000;
		}

		// optics.writeToFile("/tmp/match_optics_it" + iteration + ".txt");
		double err = getResidualSum(optics);
		logger.info(infoSb);

		return err;
	}

	/**
	 * This method is typically called after the optimization, but can be called at
	 * also during the optimization to check what kind of optics the optimization is
	 * evaluating at any step. Can be useful to debugging also.
	 * 
	 * @return The optics.
	 */
	public Optics getOptics() {
		return optics;
	}

	/**
	 * Calculate the residual and prints info
	 * 
	 * @param optics The optics containing
	 * @return
	 */
	public double getResidualSum(Optics optics) {
		double residualSum = 0;

		infoSb = new StringBuilder();
		infoSb.append("Matching evaluations = " + evaluations + System.lineSeparator());
		infoSb.append("Value of variables: [");
		for (MatchingVariable v : matchingVariables) {
			String name= v.getFieldMapName();
			if(name==null)
				name = v.getMatchingVariableType().toString();
				
			infoSb.append("{" +name + " : " + v.getValue().toString() + "}, ");
		}
		infoSb.append("]" + System.lineSeparator());

		for (int i = 0; i < targets.size(); i++) {
			MatchingTarget par = targets.get(i);
			double longiPos = par.getLongitudinalPosition();
			TwissParameters tp = optics.getTwissParametersAt(longiPos);
			PhaseSpaceEllipse hel = tp.hEllipse();
			PhaseSpaceEllipse vel = tp.vEllipse();
			double value = 0;

			switch (par.getType()) {

				case H_ALPHA:
					value = hel.alpha();
					break;
				case H_ANGLE:
					value = hel.angle();
					break;
				case H_BETA:
					value = hel.beta();
					break;
				case H_DISPERSION:
					value = tp.dispH();
					break;
				case H_PHASE_ADVANCE:
					value = tp.muH();
					break;
				case H_POSITION:
					value = hel.pos();
					break;
				case V_ALPHA:
					value = vel.alpha();
					break;
				case V_ANGLE:
					value = vel.angle();
					break;
				case V_BETA:
					value = vel.beta();
					break;
				case V_DISPERSION:
					value = tp.dispV();
					break;		
				case V_PHASE_ADVANCE:
					value = tp.muV();
					break;
				case V_POSITION:
					value = vel.pos();
					break;
				case H_SIGMA:
					value = tp.getSigmaH();
					break;
				case V_SIGMA:
					value = tp.getSigmaV();
					break;
				case MAXIMUM_H_BETA:
					value = optics.getMaxHorBeta();
					break;
				case MAXIMUM_V_BETA:
					value = optics.getMaxVertBeta();
					break;
				case MAXIMUM_H_DISPERSION:
					value = optics.getMaxHorDisp();
					break;
				case MAXIMUM_V_DISPERSION:
					value = optics.getMaxVertDisp();
					break;
				case MINIMUM_H_DISPERSION:
					value = optics.getMinHorDisp();
					break;
				case MINIMUM_V_DISPERSION:
					value = optics.getMinVertDisp();
					break;

				default:
					logger.error("Something went wrong, unknown OpticsMatchingParameter");
			}

			if (par.getValueType() == MatchingValueType.EXACT_VALUE)
				residualSum += Math.abs(par.getWeight() * (value - par.getTargetValue()));
			else if (par.getValueType() == MatchingValueType.MAXIMUM) {
				if (value > par.getTargetValue())
					residualSum += Math.abs(par.getWeight() * (value - par.getTargetValue()));
			} else if (par.getValueType() == MatchingValueType.MINIMUM) {
				if (value < par.getTargetValue())
					residualSum += Math.abs(par.getWeight() * (value - par.getTargetValue()));
			}

			String location = "";
			if (!par.getType().isGlobalParameter())
				location = " Longitudinal pos. = " + par.getLongitudinalPosition();
			infoSb.append(par.getType().toString() + " target= " + par.getTargetValue() + " current value= " + value
					+ location + System.lineSeparator());
		}
		infoSb.append("RESIDUAL = " + residualSum + System.lineSeparator());
		return residualSum;

	}

	
}
