package simpa.acc.api.match;

/**
 * Class containing the parameter to be matched, the target value and its
 * weight.
 * 
 * @author lbojtar
 *
 */
public class MatchingTarget {

	private Double longitudinalPosition;
	private MatchingTargetType type;
	private double weight = 1.0;
	private double targetValue;
	private MatchingValueType valueType = MatchingValueType.EXACT_VALUE;

	/**
	 * Creates a new MatchingTarget object with the default weight of 1.0
	 * 
	 * @param targetValue          The value of the parameter to be matched
	 * @param type                 The parameter to be matched
	 * @param longitudinalPosition The longitudinal position of the parameter to be
	 *                             matched
	 * @param weight               The weight of the parameter to be matched.
	 *                             Default value is 1.0
	 */
	public MatchingTarget(double targetValue, MatchingTargetType type, Double longitudinalPosition, double weight) {
		this.targetValue = targetValue;
		this.type = type;
		this.longitudinalPosition = longitudinalPosition;
		this.weight = weight;
	}

	/**
	 * Creates a new MatchingTarget object with the specified vale type.
	 * 
	 * @param targetValue          The value of the parameter to be matched
	 * @param type                 The parameter to be matched
	 * @param longitudinalPosition The longitudinal position of the parameter to be
	 * @param weight               The weight of the parameter to be matched.
	 *                             Default value is 1.0 matched
	 * @param valueType            The value type type indicating if targetValue is
	 *                             an exact value, a lower or upper limit.
	 */
	public MatchingTarget(double targetValue, MatchingTargetType type, Double longitudinalPosition, double weight,
			MatchingValueType valueType) {
		this.targetValue = targetValue;
		this.type = type;
		this.longitudinalPosition = longitudinalPosition;
		this.weight = weight;
		this.valueType=valueType;
	}

	/**
	 * 
	 * @return The longitudinal position of the parameter to be matched
	 */
	public Double getLongitudinalPosition() {
		return longitudinalPosition;
	}

	/**
	 * 
	 * @return The parameter to be matched
	 */
	public MatchingTargetType getType() {
		return type;
	}

	/**
	 * 
	 * @return The weight of the parameter to be matched.
	 */
	public double getWeight() {
		return weight;
	}

	/**
	 * 
	 * @return The target value of the parameter to be matched.
	 */
	public double getTargetValue() {
		return targetValue;
	}

	/**
	 * 
	 * @return Enum indicating if targetValue is an exact value, a lower or upper
	 *         limit.
	 */
	public MatchingValueType getValueType() {
		return valueType;
	}
}
