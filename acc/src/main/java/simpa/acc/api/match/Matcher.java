package simpa.acc.api.match;

import java.util.List;
import java.util.Map;
import org.apache.commons.math3.analysis.MultivariateFunction;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.optim.BaseOptimizer;
import org.apache.commons.math3.optim.InitialGuess;
import org.apache.commons.math3.optim.OptimizationData;
import org.apache.commons.math3.optim.SimpleBounds;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.nonlinear.scalar.ObjectiveFunction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.Beam;
import simpa.acc.api.EllipseBeam;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.Sequence;
import simpa.core.api.track.Particle;
import simpa.core.api.utils.PathUtils;

/**
 * Class for optics or initial conditon matching. It does the
 * matching by minimizing the difference between the target and
 * the actual value by varying the the scaling factors of the given list of
 * elements or the initial beam parameters until the RMS residual goes below the
 * given relative error value.
 * 
 * @author lbojtar
 *
 */
public class Matcher {

	private static final Logger logger = LogManager.getLogger(Matcher.class);

	private Sequence sequence;
	private Map<String, Double> scalings;
	private List<MatchingVariable> variables;
	private List<MatchingTarget> params;
	private ObjectiveFunction function;
	private BaseOptimizer<?> optimizer;
	private List<OptimizationData> optimizationData;
	private SimpaMultivariateFunction objectiveFunction;

	/**
	 * Constructor for the class to do optics or initial condition matching.
	 * 
	 * @param optimizer        An implementation of the optimizer.
	 * @param optimizationData An array of optimization data. The actual
	 *                         implementations in the array depends on the type of
	 *                         the optimizer.
	 * @param sequence         A sequence. The magnetic and electric fields of the
	 *                         elements in the sequence are calculated according to
	 *                         the scaling map of the sequence. All the scalings not
	 *                         present in the list of variables will be determined
	 *                         from the sequence scaling map. The sequence scalings
	 *                         also present in list of variables has no effect,
	 *                         those will be varied and their initial values are
	 *                         taken from the
	 *                         {@link MatchingVariable#getInitialGuess()} field.
	 * @param variables        A list of variables to be optimized during the
	 *                         matching.
	 * @param params           A list of target optics values to be obtained during
	 *                         the matching. The number of target values must be the
	 *                         same as the number of variables.
	 */
	public Matcher(BaseOptimizer<?> optimizer, List<OptimizationData> optimizationData, Sequence sequence,
			List<MatchingVariable> variables, List<MatchingTarget> params) {
		this.sequence = sequence;
		this.optimizer = optimizer;
		this.optimizationData = optimizationData;
		this.scalings = sequence.getScalingMap();
		if (scalings.size() == 0)
			logger.warn("The scaling of the sequence is empy. This can be normal if you match all elements.");
		this.variables = variables;
		this.params = params;
	}

	/**
	 * Does the initial beam parameter matching procedure with the given parameters
	 * for a transfer line. The result of the matching is the latest values of the
	 * matching variables.
	 * 
	 * @param sequence          The sequence to be matched.
	 * @param inTwissParameters The initial Twiss parameters of the beam.
	 * @param beam              The beam to be matched. During the optimization
	 *                          process the beam will be deep copied and the initial
	 *                          conditions will be set according to the matching
	 *                          variables.
	 * @param stepSize          Step size for the tracking.
	 * @param diskRadius        The radius of the observer disk. Should be big
	 *                          enough to cover the aperture, but should not
	 *                          intersect the sequence twice.
	 * @param backward          If true the matching procedure will use backward
	 *                          tracking.
	 */
	public void matchLine(Sequence sequence, TwissParameters inTwissParameters, Beam beam,
			double stepSize, double diskRadius, boolean backward) {

		objectiveFunction = new SigmaMultivariateFunction(beam, inTwissParameters, sequence,
				variables, params, stepSize, backward, diskRadius);

		function = new ObjectiveFunction(objectiveFunction);
		match();

	}

	/**
	 * Does the optics matching procedure with the given parameters for a transfer
	 * line.
	 * 
	 * @param sequence   The sequence to be matched.
	 * 
	 * @param beam0      An ellipse beam with the nominal momentum for optics
	 *                   parameter matching.
	 * 
	 * @param beam1      An ellipse beam with some momentum deviation for optics
	 *                   parameter matching. The momentum deviation is needed to
	 *                   calculate the dispersion.
	 * 
	 * @param stepSize   Step size for the tracking.
	 * 
	 * @param diskRadius The radius of the observer disk. Should be big enough to
	 *                   cover the aperture, but should not intersect the sequence
	 *                   twice.
	 * 
	 * @param backward   If true the matching procedure will use backward tracking.
	 *                   This is useful when the optics parameters are known at the
	 *                   end of the transfer line. One example is when matching a
	 *                   line to the beam coming from a source and the end of the
	 *                   line there is a ring with known Twiss parameters.
	 * 
	 * @return A deep copy of the scalings map of the sequence with the matched
	 *         scalings.
	 */
	public Map<String, Double> matchLine(Sequence sequence, EllipseBeam beam0, EllipseBeam beam1, double stepSize,
			double diskRadius, boolean backward) {

		if (beam1 == null || beam0 == null)
			throw new IllegalArgumentException(
					"For optics matching in transfer lines the beam0 and beam1 can not be null ! ");

		if (sequence.isCircular())
			throw new IllegalArgumentException("Error while calling matchLine: The active sequence is a ring");

		// beam position check
		List<Vector3D> path = sequence.getReferenceOrbit().getOrbitPoints();
		int b0i = PathUtils.getClosestIndex(path, beam0.calculateBeamPosition());
		int b1i = PathUtils.getClosestIndex(path, beam1.calculateBeamPosition());
		double b0Pos = sequence.getReferenceOrbit().getLongitudinalPosition(b0i);
		double b1Pos = sequence.getReferenceOrbit().getLongitudinalPosition(b1i);

		for (MatchingTarget p : params) {
			if (backward) {
				if (p.getLongitudinalPosition() >= b0Pos || p.getLongitudinalPosition() >= b1Pos)
					throw new IllegalArgumentException(
							"Line maching called with backward=true, but the beam position is before the target parameter position.");
			} else {
				if (p.getLongitudinalPosition() <= b0Pos || p.getLongitudinalPosition() <= b1Pos)
					if (!p.getType().isGlobalParameter())
						throw new IllegalArgumentException(
								"Line maching called with backward=false, but the beam position is after the target parameter position.");
			}
		}

		objectiveFunction = new OpticsMultivariateFunction(sequence, scalings, variables,
				params, stepSize,
				backward, diskRadius);

		if (!(beam0 instanceof EllipseBeam) || !(beam1 instanceof EllipseBeam)) {
			throw new IllegalArgumentException("The beam should be an EllipseBeam for line optics matching.");
		}

		((OpticsMultivariateFunction)objectiveFunction).setBeams(beam0, beam1);
		function = new ObjectiveFunction(objectiveFunction);

		return match();
	}

	/**
	 * Does the matching procedure with the given parameters for a ring.
	 * 
	 * @param sequence   The sequence to be matched.
	 * @param p0         A particle with the nominal momentum.
	 * @param p1         A particle with some momentum deviation. The momentum
	 *                   deviation is needed to calculate the dispersion.
	 * @param stepSize   Step size for the tracking.
	 * @param diskRadius The radius of the observer disk. Should be big enough to
	 *                   cover the aperture, but should not intersect the sequence
	 *                   twice.
	 * @return A deep copy of the scalings map of the sequence with the matched
	 *         scalings.
	 */
	public Map<String, Double> matchRing(Sequence sequence, Particle p0, Particle p1, double stepSize,
			double diskRadius) {
		if (!sequence.isCircular())
			throw new IllegalArgumentException("Error while calling matchRing: The active sequence is NOT a ring");

		objectiveFunction = new OpticsMultivariateFunction(sequence, scalings, variables,
				params, stepSize,
				false, diskRadius);
		((OpticsMultivariateFunction)objectiveFunction).setParticles(p0, p1);
		function = new ObjectiveFunction(objectiveFunction);

		return match();
	}

	/**
	 * Writes the optics to a file. Typically called after the matching.
	 * 
	 * @param file File name where the optics will be written.
	 */
	public void writeOptics(String file) {
		MultivariateFunction f = function.getObjectiveFunction();
		if (f instanceof OpticsMultivariateFunction) {
			((OpticsMultivariateFunction) f).getOptics().writeToFile(file);
		} else {
			logger.info(" The objective function is not a OpticsMultivariateFunction, can not write optics file.");
		}
	}

	public List<MatchingVariable> getVariables() {
		return variables;
	}



	private Map<String, Double> match() {

		int n = variables.size();
		double[] point = new double[n];
		double[] min = new double[n];
		double[] max = new double[n];

		for (int i = 0; i < n; i++) {
			MatchingVariable variable = variables.get(i);

			// prepare initial guess
			double iguess;
			if (variable.getInitialGuess() != null)
				iguess = variable.getInitialGuess();
			else
				iguess = scalings.get(variable.getFieldMapName());
			point[i] = iguess;

			// Prepare bounds
			if (variable.getMin() != null)
				min[i] = variable.getMin();
			else
				min[i] = -Double.MAX_VALUE;

			if (variable.getMax() != null)
				max[i] = variable.getMax();
			else
				max[i] = Double.MAX_VALUE;

		}

		optimizationData.add(new InitialGuess(point));
		optimizationData.add(new SimpleBounds(min, max));
		optimizationData.add(GoalType.MINIMIZE);
		optimizationData.add(function);

		OptimizationData[] optd = new OptimizationData[optimizationData.size()];
		for (int i = 0; i < optimizationData.size(); i++)
			optd[i] = optimizationData.get(i);

		try {
			optimizer.optimize(optd);
		} catch (Exception e) {
			logger.error("Maching did not converge: " + e.toString());
			logger.error("Last scaling map: " + sequence.getScalingMapString());
			return sequence.getScalingMap();
		}

		logger.info("Matching finished normally.");
		logger.error("Last scaling map: " + sequence.getScalingMapString());

		return sequence.getScalingMap();

	}

}
