/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.OptimizationData;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.BOBYQAOptimizer;
import org.apache.commons.math3.optim.nonlinear.scalar.noderiv.CMAESOptimizer;
import org.apache.commons.math3.random.JDKRandomGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cli.api.CliState;
import cli.api.Command;
import cli.api.CommandInterpreter;
import cli.api.CommandParameter;
import simpa.acc.api.create.AccElement;
import simpa.acc.api.create.AccFieldFactory;
import simpa.acc.api.create.BeamRegion;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.create.Sequence;
import simpa.acc.api.match.Matcher;
import simpa.acc.api.match.MatchingTarget;
import simpa.acc.api.match.MatchingTargetType;
import simpa.acc.api.match.MatchingValueType;
import simpa.acc.api.match.MatchingVariable;
import simpa.acc.api.match.MatchingVariableType;
import simpa.acc.api.match.SimpaMultivariateFunction;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.acc.api.track.PhaseSpaceObserver;
import simpa.acc.api.track.PhaseSpacePlane;
import simpa.acc.api.track.RingBeamTracker;
import simpa.acc.api.track.TLBeamTracker;
import simpa.acc.api.track.TurnObserver;
import simpa.acc.api.utils.BeamMatrix;
import simpa.acc.api.utils.BeamSampler;
import simpa.acc.api.utils.BeamSizeCalculator;
import simpa.acc.api.utils.EllipseBeamSampler;
import simpa.acc.api.utils.GaussianBeamSampler;
import simpa.acc.api.utils.IntersectionCalculator;
import simpa.acc.api.utils.OpticsCalculationException;
import simpa.acc.api.utils.OpticsCalculator;
import simpa.acc.api.utils.OrbitAtBPMs;
import simpa.acc.api.utils.Survey;
import simpa.acc.api.utils.TuneCalculator;
import simpa.acc.impl.MethodInvoker;
import simpa.core.api.FieldType;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.LengthUnit;
import simpa.core.api.OutputFileFormat;
import simpa.core.api.PotentialProvider;
import simpa.core.api.Profile;
import simpa.core.api.Quadrature;
import simpa.core.api.SHCombiner;
import simpa.core.api.SolutionChecker;
import simpa.core.api.SolverOptions;
import simpa.core.api.SphereCovering;
import simpa.core.api.Tiling;
import simpa.core.api.TilingConfig;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.NotSetException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.exceptions.PositionMismatchException;
import simpa.core.api.track.BeamTrajectoryObserver;
import simpa.core.api.track.Particle;
import simpa.core.api.track.ParticleTrackerTask;
import simpa.core.api.track.ParticleType;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.track.TaosSymplecticIntegrator;
import simpa.core.api.track.TrackingObserver.ObserverBehaviour;
import simpa.core.api.track.TrajectoryData;
import simpa.core.api.track.TrajectoryObserver;
import simpa.core.api.utils.CalculatorUtils;
import simpa.core.api.utils.DiagnosticUtils;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.LocalFramesCalculator;
import simpa.core.api.utils.PathUtils;
import simpa.core.api.utils.SolutionTransformer;

/**
 * Class that specifies all the commands that can be used in the CLI using the
 * CLI library apart from some extra commands.
 * 
 */
public class AccCli {

	private static final Logger logger = LogManager.getLogger(AccCli.class);

	public static void main(String[] args) throws IOException {
		if (args.length > 0) {
			CommandInterpreter.create(new Class[] { AccCli.class }).startFromFile(args[0], true);
		} else {
			getCommandInterpreter().start();
		}
	}

	/**
	 * Method to get the CommandInterpreter object. The command interpreter can be
	 * used to execute simpa commands.
	 * 
	 * @return The command interpreter.
	 * 
	 * 
	 * @throws IOException
	 */
	public static CommandInterpreter getCommandInterpreter() throws IOException {
		Properties p = new Properties();
		p.load(AccCli.class.getClassLoader().getResourceAsStream("project.properties"));
		System.out.println("SIMPA Acc version: " + p.getProperty("version"));

		String currentDir = new File("").getAbsolutePath();
		System.out.println("Current working directory is: " + currentDir);
		return CommandInterpreter.create(new Class[] { AccCli.class }, "AccCli=> ");
	}

	/**
	 * @return The current state of the command line interface.
	 * @throws IOException if the state cannot be created.
	 */
	public static CliState getState() throws IOException {
		return CliState.getInstance();
	}

	//@formatter:off
	
  	////////////////// COMMANDS STARTS FROM HERE ////////////////////////////

	@Command(key = "active-sequence", description = """
			Prints the name of the active sequence.
			""")
	public Sequence activeSequence() {
		if (Sequence.getActiveSequence() == null)
			logger.info("There is no active sequence");
		else
			logger.info("Active sequence : " + Sequence.getActiveSequence().getName());
		
		return Sequence.getActiveSequence();
	}
	
	
    		////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "add-arbitrary", description = """
			Puts an element with arbitrary shape at the end of the beam line at the
			specified longitudinal position. Unlike for the bending and straight elements
			the arbitrary element must accompanied by a path which describes the
			trajectory of the ideal particle. The element is oriented such that the local
			Z axis of the element points in the same direction as the last vector in the
			reference orbit calculated so far. The element is rotated around the Z axis
			such that the local Y axis of the element points to the direction of the
			--local-y-axis parameter. The last vector of the path determines the Z direction
			at the exit of the element.
			""")
	public void addArbitrary(
			
			@CommandParameter(keys = { "-n", "--name" }, required = true, help = """
			The name of the arbitrary element. This can be chosen as the user wishes.
			""") String elementName,
			
			@CommandParameter(keys = { "-s", "--scaling" }, help = """
			Scaling factor, default is 1.0
			""") Double scaling,
			
			@CommandParameter(keys = { "-lp", "--long-pos" }, required = true, help = """
			Longitudinal position of the first point in the reference orbit of the element.
			""") Double longiPos,
			
			@CommandParameter(keys = { "-gn", "--group-name" }, help = """
			The name of the group which this element belongs to. This can be chosen as 
			the user wishes. All elements in a group will be expanded into a single
			field map and can be scaled together later.	Individual elements in a group,
			however can have different scaling factors. If not given the element name will
			be used as group name.			
			""") String group,
			
			@CommandParameter(keys = { "-etn", "--element-type-name" }, help = """
			Name of the element type. This name must correspond to the name of an 
			existing source strength file. The source strength file is named according
			to the conventions. Example: MYBENDING-SOLUTION.txt
			""") String etn,
			
			@CommandParameter(keys = { "-vc", "--vacuum-chamber" }, help = """
			A vacuum chamber of this element, if any. If not given the aperture of
			the element will be constructed	using the the default profile of the sequence.
			""") Map<Double, Profile> profileMap,
			
			@CommandParameter(keys = { "-p","--path" }, required = true, help = """							
			Path file name. It gives the reference orbit in the element. 
			The path file contains 3 numbers in each row describing 3D points
			which give the reference orbit inside the element.
			""") String pathFile,
		
			@CommandParameter(keys = { "-y", "--local-y-axis" }, required = false, help = """
			The local Y direction at the entry of the element.
			If not given, by default it is the global Y axis. It must be perpendicular
			to the first vector of the path which is the local Z axis.
			""") Vector3D localY) throws IllegalAccessException, FileNotFoundException {
		
		if (Sequence.getActiveSequence().getFrozen())
			throw new IllegalAccessException("Sequence is closed, you're no longer able to add elements");

		if (scaling == null)
			scaling = 1.0;
		if (localY == null)
			localY = Vector3D.PLUS_J;
		String sf = etn + FileNamingConventions.getSolutionFileExtension();

		List<Vector3D> path = FileUtils.readVectors(pathFile, LengthUnit.M);

		Sequence.getActiveSequence().addArbitraryElementAt(longiPos, elementName, scaling, sf, localY, group, profileMap, path);
	}
            
            ////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "add-bending", description = """
			Puts a bending at the end of the beam line with the center at the given
			at the longitudinal position along the reference orbit. The bending will be placed
			such, that the Z axis of the local coordinate system at the center of the
			bending points in the direction of the beam propagation. The reference orbit
			inside the bending is calculated automatically.
			""")
	public void addBending(
			
			@CommandParameter(keys = { "-n", "--name" }, required = true, help = """
			The name of the arbitrary element. This can be chosen as the user wishes.
			""") String elementName,
			
			@CommandParameter(keys = { "-s", "--scaling" }, help = """
			Scaling factor, default is 1.0
			""") Double scaling,
			
			@CommandParameter(keys = { "-l","--length" }, required = true, help = """
			Length of the element [m]. The bending radius is calculated as:
			r = length / deflection
			""") double length,
			
			@CommandParameter(keys = { "-d","--deflection" }, required = true, help = """
			Deflection angle of the bending.
			""") double deflection,
			
			@CommandParameter(keys = { "-t","--tilt" }, help = """
			Tilt angle [radian] around the local +Z direction according to the right hand convention.
			The bending will be placed with this rotation applied. If rotation is also given, first
			the rotation is applied, then the tilt.
			""") Double tilt,
			
			@CommandParameter(keys = { "-lp","--long-pos" }, required = true, help = """
			Longitudinal position of the bending center [m].
			""") Double longPos,
			
			@CommandParameter(keys = { "-etn", "--element-type-name" }, required = true, help = """
			Name of the element type. This name must correspond to the name of an 
			existing source strength file. The source strength file is named according
			to the conventions. Example: MYBENDING-SOLUTION.txt				
			""") String etn,			
			
			@CommandParameter(keys = { "-gn", "--group-name" }, help = """
			The name of the group which this bending belongs to. This can be chosen as 
			the user wishes. All elements in a group will be expanded into a single
			field map and can be scaled together later.	Individual elements in a group,
			however can have different scaling factors. If not given the element name 
			will be used as group name.
			""") String group,
			
			@CommandParameter(keys = { "-vc", "--vacuum-chamber" }, help = """
			A vacuum chamber of this element, if any. If not given the aperture of
			the element will be constructed	using the the default profile of the sequence.
			""") Map<Double, Profile> profileMap,
			
			@CommandParameter(keys = { "-r","--rotation" }, required = false, help = """
			A rotation of the element applied before adding to the sequence. This is useful
			when a transformed version of a source strengths file is needed. A rotation
			object can be created with the 'create-rotation" command. You can assign it
			to a variable and use in the command option.
			""") Rotation rot,
		
			@CommandParameter(keys = { "-bt","--bending-type" }, required = false, help = """
			The type of the bending.There are two type of bendings:
			"S" for sector bending and "R" for rectangular bending. The default is "S"
			""") String bendingType) {

		if (tilt == null)
			tilt = (double) 0;
		if (scaling == null)
			scaling = 1.0;
		if (rot == null)
			rot = Rotation.IDENTITY;

		boolean sectorBending = true;

		if (bendingType != null) {
			if (bendingType.toUpperCase().equals("R"))
				sectorBending = false;
			if (!bendingType.toUpperCase().equals("R") && !bendingType.toUpperCase().equals("S"))
				throw new IllegalArgumentException("Unknown bending type: " + bendingType);
		}

		if (Sequence.getActiveSequence().getFrozen())
			throw new IllegalArgumentException("Sequence is closed, you're no longer able to add elements");

		String sf = etn + FileNamingConventions.getSolutionFileExtension();	

		Sequence.getActiveSequence().addBendingElementAt(longPos, elementName, scaling, sf, length, deflection, tilt, group, rot,
				profileMap, sectorBending);

	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "add-bpm", description = """
			Add a beam position monitor to the sequence.
			Useful together with the 'orbit-at-bpms' command to output the orbit at
			the BPM positions. 
			""")
	public void addBPM(
			
			@CommandParameter(keys = { "-p", "--plane" }, required = true, help = """
			The plane of the monitor. Allowed values:
			"H","V", "HV" for horizontal,vertical or both.
			""") String plane,
			
			@CommandParameter(keys = { "-n", "--name" }, required = true, help = """
			Name of the BPM.
			""") String name,
			
			@CommandParameter(keys = { "-lp","--longitudinal-position" }, required = true, help = """
			The longitudinal position of the pickup center from the start of the reference
			orbit [m].
			""") Double lpos,
			 
			@CommandParameter(keys = { "-vc","--vacuum-chamber" }, help = """
			A vacuum chamber of this element, if any. If not given the aperture of the element
			will be constructed	using the the default profile of the sequence.
			""") Map<Double, Profile> profileMap) throws IllegalArgumentException {

		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("No active sequence !");

		if (plane.equals("H")) {
			Sequence.getActiveSequence().addMarkerElement(lpos, name, AccElement.ElementType.H_PICKUP,profileMap);
		} else if (plane.equals("V")) {
			Sequence.getActiveSequence().addMarkerElement(lpos, name, AccElement.ElementType.V_PICKUP,profileMap);
		}
		if (plane.equals("HV")) {
			Sequence.getActiveSequence().addMarkerElement(lpos, name + "_H", AccElement.ElementType.H_PICKUP,profileMap);
			Sequence.getActiveSequence().addMarkerElement(lpos, name + "_V", AccElement.ElementType.V_PICKUP,profileMap);
		} else {
			throw new IllegalArgumentException("Unrecognized plane option: " + plane);
		}

	}
	
			////////////////////////////////////////////////////////////////////////////////  80 character	
	@Command(key = "add-marker", description = """
			Adds a marker to the sequence. It is useful to mark a specific position in the
			sequence.
			""")
	public void addMarker(
			
			@CommandParameter(keys = { "-n", "--name" }, required = true, help = """
			Name of the marker.
			""") String name,
			
			@CommandParameter(keys = { "-lp","--longitudinal-position" }, required = true, help = """
			The longitudinal position from the start of the reference orbit [m].
			""") Double lpos,

			@CommandParameter(keys = { "-vc","--vacuum-chamber" }, help = """
			A vacuum chamber of this element, if any. If not given the aperture of the element
			will be constructed	using the the default profile of the sequence.
			""") Map<Double, Profile> profileMap) throws IllegalArgumentException {
		
		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("No active sequence !");
		Sequence.getActiveSequence().addMarkerElement(lpos, name, AccElement.ElementType.MARKER, profileMap);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "add-straight", description = """
	        Adds a straight element at the end of the beam line at the specified 
	        longitudinal position. It is assumed that the Z axis of the local coordinate 
	        system of the element to be added points in the direction of the beam 
	        propagation. 
			""")
	public void addStraight(
			
			@CommandParameter(keys = { "-n","--name" }, required = true, help = """
			The name of the element to be added.
			""") String elementName,
			
			@CommandParameter(keys = { "-s","--scaling" }, help = """
			Scaling factor, default is 1.0
			""") Double scaling,

			@CommandParameter(keys = { "-t", "--tilt" }, help = """
			Tilt angle [radian] around the local +Z direction according to the right hand
			convention.	The element will be placed with this rotation applied. If  a 
			rotation is also given, first the rotation is applied, then the tilt.
			""") Double tilt,
			
			@CommandParameter(keys = { "-lp","--long-pos" }, required = true, help = """
			Longitudinal position of the center of the element.
					""") Double longPos,
			
			@CommandParameter(keys = { "-gn", "--group-name" }, help = """
			The name of the group which this bending belongs to. This can be chosen as 
			the user wishes. All elements in a group will be expanded into a single
			field map and can be scaled together later.	Individual elements in a group,
			however can have different scaling factors. If not given the element name 
			will be used as group name.
			""") String group,
			
			@CommandParameter(keys = { "-etn", "--element-type-name" }, help = """
			Name of the element type. This name must correspond to the name of an 
			existing source strength file. The source strength file is named according 
			to the conventions. Example: MYELEMENT-SOLUTION.txt			
			""") String etn,		
			
			@CommandParameter(keys = { "-vc","--vacuum-chamber" }, help = """
			A vacuum chamber of this element, if any. If not given the aperture of the element
			will be constructed	using the the default profile of the sequence.
			""") Map<Double, Profile> profileMap,
			
			@CommandParameter(keys = { "-r","--rotation" }, required = false, help = """
			A rotation to be applied before putting the element into the sequence.
			If the element is oriented according to the conventions, such the local Z axis
			coincides with the reference orbit and the local Y axis points up, this should
			be the identity rotation. Typically used for mis-alignment or put an element
			into the sequence which was created with a non-standard orientation, for example
			the CAD model the element is calculated from is given such that the local Z axis
			is not the same as the reference orbit inside the element.
					""") Rotation rot)	throws IllegalAccessException {
		
		if (Sequence.getActiveSequence().getFrozen())
			throw new IllegalAccessException("Sequence is closed, you're no longer able to add elements");
		if (rot == null)
			rot = Rotation.IDENTITY;
		if (scaling == null)
			scaling = 1.0;

		if (tilt == null)
			tilt = (double) 0;

		String sf = etn + FileNamingConventions.getSolutionFileExtension();
		Sequence.getActiveSequence().addStraightElementAt(longPos, elementName, scaling, sf, tilt, group, rot, profileMap);
	}

	        ////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "beam-from-file", description = """
			Create a beam for tracking from a file. The file must contain 6D phase space 
			coordinates in each line in the following format:
			x xp y yp z zp (positions in meters)
			If the file contains more columns, the extra columns will be ignored. A typical 
			usage is to create a beam from a file produced during tracking of a bunch in a
			transfer line by a phase space observer.
			""")
	public Beam beamFromFile(@CommandParameter(keys = { "-t","--type" }, help = """
			The particle type one of these are defined:
			"proton", "antiproton", "electron", "positron".
			""") String type,
			
			@CommandParameter(keys = { "-m","--momentum" }, required = true, help = """
			The nominal momentum of the beam. The z' coordinates read from the 
			file will be added to to this such that, for any particle: p = p0 + z' * p0
			where p0 is the nominal momentum in  [GeV/c].
			""") Double mom,
			
			@CommandParameter(keys = { "-lp","--longitudinal-position" }, required = true, help = """
			The longitudinal position of the beam center from the start of the 
			reference orbit [m]. The z coordinates in [m] read from the file will be
			added to this. 
			""") Double lPos,
			
			@CommandParameter(keys = { "-mass","--mass" }, help = """
			The mass of the particle, if no type is given. Otherwise ignored. [kg].
			""") Double mass,
			
			@CommandParameter(keys = { "-c","--charge" }, help = """
			The charge of the particle, if no type is given. Otherwise ignored. [Coulomb].
			""") Double charge,
			
			@CommandParameter(keys = { "-na", "--name" }, required = true, help = """
			The name of the beam. This will be used by the observers for the output
			file names.
			""") String name,

			@CommandParameter(keys = { "-f","--file" }, required = true, help = """
			A file containing the phase space coordinates. There must be at least 
			6 columns, even for a 4D or 5D beam. In case of 4D or 5D beams the 
			remaining coordinates should be set to zeros.  
			""") String file)
			throws OutOfApertureException, KnownFatalException, FileNotFoundException {

		if (Sequence.getActiveSequence() == null) {
			throw new IllegalArgumentException("No active sequence.");
		}
		if (Sequence.getActiveSequence().isCircular() && lPos == null) {
			throw new IllegalArgumentException("The longitudinal position must be given for circular sequences.");
		}

		if (lPos < 0) {
			throw new IllegalArgumentException("The longitudinal position must be positive");
		}
		if (lPos > Sequence.getActiveSequence().getReferenceOrbit().getOrbitLength()) {
			throw new IllegalArgumentException("The longitudinal position must be smaller than the sequence length");
		}

		if (Sequence.getActiveSequence().isCircular() && Sequence.getActiveSequence().getOptics() == null) {
			throw new IllegalArgumentException("The optics must be set for circular machines.");
		}
		if (type == null && (mass == null || charge == null)) {
			throw new IllegalArgumentException("The mass and charge must be given if no type is given.");
		}
		if (type != null) {
			ParticleType pType = ParticleType.parse(type);
			mass = pType.getMass();
			charge = pType.getCharge();
		}

		ParticleFactory pf = new ParticleFactory(Sequence.getActiveSequence().getReferenceOrbit());
		List<Particle> pl = pf.getFromFile(mom, file, lPos, Sequence.getActiveSequence().getPotentialProvider(), mass, charge);

		return new Beam(pl, Sequence.getActiveSequence().getReferenceOrbit(), Sequence.getActiveSequence().getPotentialProvider(), name);

	}

    ////////////////////////////////////////////////////////////////////////////////    80 character		
	@Command(key = "beam-twiss", description = """
			It returns and prints Twiss parameters from a phase space coordinate 
			file of a beam containing many particles by calculating the beam
			co-variance matrix. The emittances are RMS 1 sigma emittances.
			""")
	public TwissParameters beamTwiss(
			@CommandParameter(keys = { "-f", "--file" }, required = true, help = """
			A file containing the phase space coordinates. There must be at least 
			6 columns, even for a 4D or 5D beam. In case of 4D or 5D beams the 
			remaining coordinates should be set to zeros.  
			""") String file) throws IOException {
		
		TwissParameters twp = BeamMatrix.getTwissParameters(file);
		logger.info("Twiss parameters calculated from beam covariance matrix:\n"+
		"The emittances are RMS 1 sigma emittances\n");
		TwissParameters.printTwiss(twp);
		return twp;
	}

        ////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "bobyqa-matcher", description = """
		Creates a BOBYQA optimizer for multivariate matching  which should be assigned 
		to a variable a passed to the --matcher parameter of the 'match' command. This
		matcher is  fast, but can stuck to local minimum of the objective function.
		The objective function is the sum of absolute values of the differences	between
		the target values and the actual values during the optimization.
		For the specific parameters of the BOBYQA optimizer see:			
		https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/optim/nonlinear/scalar/noderiv/BOBYQAOptimizer.html					
		""")
	public Matcher bobyqaMatcher(
			
		@CommandParameter(keys = { "-ip","--interpolation-points" }, required = false, help = """
		Number of interpolation points. The default value is 2*n+1, where n
		is the number of variables.
		""") Integer ipn,
		
		@CommandParameter(keys = { "-ir","--initial-region" }, required = false, help = """
		Initial trust region. Default is 10. The bigger this value the more
		search space will be explored around the initial guess.
		""") Double initialRegion,
		
		@CommandParameter(keys = { "-sf","--stop-fitness" }, required = false, help = """
		Stop if objective function value is smaller than this value. Default is 1e-6
		""") Double stopFitness,
		
		@CommandParameter(keys = { "-mi","--max-iterations" }, required = false, help = """
		Maximum number iterations, after that many iterations the optimization
		stops and returns the current best value. Default is 1000.
		""") Integer maxIter,
		
		@CommandParameter(keys = { "-t","--targets" }, required = true, help = """
		An array of matching targets. The array length must be bigger than 1.
		A matching target can be created with the 'matching-target' command
		and assigned to a variable. Then those variables  can be assembled
		to an array and given to this command. Example:
		
		mt1 = matching-target ... 
		mt2 = matching-target ... 
		mt3 = matching-target ...
		
		matcher=bobyqa-matcher --targets [mt1,mt2,mt3, ...] ..			
		""") MatchingTarget[] targets,
		
		@CommandParameter(keys = { "-v","--variables" }, required = true, help = """
		An array of matching variables. The array length must be bigger than 1.			
		A matching variable can be created with the 'matching-variable' command
		and assigned to a variable. Then those variables can be assembled
		to an array and given to this command. Example:
		
		mv1 = matching-variable ... 
		mv2 = matching-variable ... 
		mv3 = matching-variable ...
		
		matcher=bobyqa-matcher --variables [mv1,mv2,mv3, ...] ..	
		""") MatchingVariable[] variables) {

		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("The active sequence is null");

		if (variables.length < 2)
			throw new IllegalArgumentException("Minimum number of variables is 2!");

		if (ipn == null)
			ipn = 2 * variables.length + 1;

		if (maxIter == null)
			maxIter = 1000;

		if (initialRegion == null)
			initialRegion = 10.0;

		if (stopFitness == null)
			stopFitness = 1e-6;

		BOBYQAOptimizer opt = new BOBYQAOptimizer(ipn, initialRegion, stopFitness);
		
		List<OptimizationData> optimizationData = new ArrayList<>();
		optimizationData.add(new MaxEval(maxIter));

		return new Matcher(opt, optimizationData, Sequence.getActiveSequence(), Arrays.asList(variables), Arrays.asList(targets));

	}

        ////////////////////////////////////////////////////////////////////////////////    80 character	
		@Command(key = "calculate-chromaticity", description = """
		Calculates the H and V chromaticities from 2 phase-space coordinate files.
		The two phase space files should contain data in the same format which is
		produced by a phase space observer during tracking in a ring. They should
		correspond to two different tracking with two particles with some momentum
		deviation.
		""")
	public double[] calculateChromaticity(
		
		@CommandParameter(keys = { "-f1","--file1" }, required = true, help = """
		A file containing the phase space coordinates obtained from	tracking  a 
		particle in a ring with momentum  p0, which is usually the nominal momentum.
		""") String file1,
		
		@CommandParameter(keys = { "-f2","--file2" }, required = true, help = """
		A file containing the phase space coordinates obtained from	tracking  a 
		particle in a ring with momentum  p1 =  p0+ p0 * dp/p.
		""") String file2,
		
		@CommandParameter(keys = { "-t","--turns" }, required = true, help = """
		Number of turns taken from the file. It must be a power of two. 
		The default is 64.
		""") Integer turns,
		
		@CommandParameter(keys = { "-dpop",	"--dpop" }, required = true, help = """
		Relative momentum deviation between the momentums of the particles which were
		used for tracking and produced the the files: dpop = (p1-p0)/p0.
		""") double dpop) throws FileNotFoundException {

	    if (turns == null)
				turns = 64;
			
		TuneCalculator tc = new TuneCalculator(file1);
		double qh1 = tc.getHorizontalTune(0, turns);
		double qv1 = tc.getVerticalTune(0, turns);

		tc = new TuneCalculator(file2);
		double qh2 = tc.getHorizontalTune(0, turns);
		double qv2 = tc.getVerticalTune(0, turns);

		double ch = (qh2 - qh1) / dpop;
		double cv = (qv2 - qv1) / dpop;
		double[] res = { ch, cv };
		logger.info("Horizontal chromaticity = " + ch);
		logger.info("Vertical chromaticity = " + cv);
		return res;
	}
		
	
        ////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "calculate-tunes", description = """
		Calculates the H and V fractional tunes by the NAFF algorithm from phase space 
		coordinates file(s). When  the directory parameter is given, the tunes are 
		calculated for all files in the directory. In this case the output file 
		parameter must be also present. The output file will contain the tunes for each 
		file in the directory. Note that the number of turns must be a power of two.
		A good choice is 64 or 128.
		""")
	public void calculateTunes(
		
		@CommandParameter(keys = { "-f",	"--file" }, required = false, help = """
		A file containing the phase space coordinates. It must have at least as many 
		lines as the --turns parameter.
		""") String file,
		
		@CommandParameter(keys = { "-t","--turns" }, required = false, help = """
		Number of turns taken from the file. The default is 64.
		""") Integer turns,
		
		@CommandParameter(keys = { "-st","--starting-turn" }, help = """
	    Starting turn. The default is zero.
	    """) Integer startTurn,
		
		@CommandParameter(keys = { "-d","--directory" }, help = """
		Directory containing the files to be processed. If this parameter is given,
		the file parameter is ignored and all files will be processed in the directory.
		The result will be written into the output file given by the --output-file
		parameter.
		""") String directory,
		
		@CommandParameter(keys = { "-of","--output-file" }, help = """
		Output file in which the tunes will be written to. If this parameter is given, 
		the tunes are calculated for all files in the given directory.
		""") String outFile) throws FileNotFoundException {

		if (turns == null)
			turns = 64;
		if (directory == null && file == null)
			throw new IllegalArgumentException("File or directory must be given.");

		if (startTurn == null)
			startTurn = 0;

		if (directory != null) {
			if (outFile == null)
				throw new IllegalArgumentException("Output file must be given when directory is given.");
			TuneCalculator.calculateAllTunes(directory, startTurn, turns, outFile);
		} else {

			TuneCalculator tc = new TuneCalculator(file);
			double qh = tc.getHorizontalTune(startTurn, turns);
			double qv = tc.getVerticalTune(startTurn, turns);
			logger.info("Hor. tune= " + qh);
			logger.info("Vert. tune= " + qv);
		}
	}

			//////////////////////////////////////////////////////////////////////////////// 80

	@Command(key = "check-field", description = """
			Compares the field of the generated solution file by the 'create-acc-element'
			command with a given reference file. The reference file name is derived from
			the element name contained in the configuration parameter. The reference file
			is usually an output from a EM field calculation software, like COMSOL,
			EM studio, etc. See also the 'write-surface-points' and 'create-tiling-config'
			and 'create-acc-element' commands. These are part of the same work-flow to
			create a point source file for an accelerator element.
			""")
	public void checkField(

			@CommandParameter(keys = { "-tc", "--tiling-config" }, required = true, help = """
			Tiling configuration object created with the 'create-tiling-config' command.
			The tiling configuration object should be assigned to a variable and that
			variable should be given to this command parameter.
					""") TilingConfig tconfig) throws FileNotFoundException, NotSetException {

		SolutionChecker sc = new SolutionChecker(tconfig.getName(), tconfig.getFieldType(), tconfig.getLengthUnit());
		double rmsErr = sc.getNormalizedRmsError(tconfig.getFieldScalingFactor());
		logger.info("RMS error: " + rmsErr);
	}

            ////////////////////////////////////////////////////////////////////////////////    80 character		
	@Command(key = "cmaes-matcher", description = """
			Creates a CMAES optimizer for multivariate matching  which should be assigned 
			to a variable a passed to the --matcher parameter of the 'match' command.
			Recommended for finding global optimum. Slow, but reliable.	The objective 
			function is the sum of absolute values of the differences between the target 
			values and the actual values during the optimization.
			See: https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/optim/nonlinear/scalar/noderiv/CMAESOptimizer.html 
			""")
	public Matcher cmaesMatcher(
			
			@CommandParameter(keys = { "-t","--targets" }, required = true, help = """
			An array of matching targets. The array length must be bigger than 1.
			A matching target can be created with the 'matching-target' command
			and assigned to a variable. Then those variables  can be assembled
			to an array and given to this command. Example:
			
			mt1 = matching-target ... 
			mt2 = matching-target ... 
			mt3 = matching-target ...
			
			matcher=cmaes-matcher --targets [mt1,mt2,mt3, ...] ..			
			""") MatchingTarget[] targets,
			
			@CommandParameter(keys = { "-v","--variables" }, required = true, help = """
			An array of matching variables. The array length must be bigger than 1.			
			A matching variable can be created with the 'matching-variable' command
			and assigned to a variable. Then those variables can be assembled
			to an array and given to this command. Example:
			
			mv1 = matching-variable ... 
			mv2 = matching-variable ... 
			mv3 = matching-variable ...
			
			matcher=cmaes-matcher --variables [mv1,mv2,mv3, ...] ..	
			""") MatchingVariable[] variables,
			
			@CommandParameter(keys = { "-sf","--stop-fitness" }, required = false, help = """
			Stop if objective function value is smaller than this value. Default is 1e-6
			""") Double stopFitness,
			
			@CommandParameter(keys = { "-mi","--max-iterations" }, required = false, help = """
			Maximum number of iterations. Default is 1000.
			""") Integer maxIter,
			
			@CommandParameter(keys = { "-ac","--active-cma" }, required = false, help = """
			If true, using an active CMA algorithm. Default is true.
			""") Boolean isActiveCMA,
			
			@CommandParameter(keys = { "-si","--sigmas" }, required = false, help = """
		    Input sigma values. They define the initial coordinate-wise standard deviations
		    for sampling new search points around the initial guess. It is suggested to set 
		    them to the estimated distance from the initial to the desired optimum. Small 
		    values induce the search to be more local and very small values are more likely
		    to find a local optimum close to the initial guess. Too small values might 
		    however lead to early termination.
		    """) Double[] sigmas,
       
			////////////////////////////////////////////////////////////////////////////////    80 character				
			
			@CommandParameter(keys = { "-cfc","--check-feasible-count" }, required = false, help = """
			Determines how many times a not feasible solution (due to the constraints)  will
			be tried to make feasible. Default is 5.
			""") Integer checkFeasableCount,
			
			@CommandParameter(keys = { "-ps","--polulation-size" }, required = false, help = """
			Population size. Increasing the population size improves global search
			properties at the expense of speed. Default is 4 + 3*ln(n), where n is the
			number of variables. 
			""") Integer populationSize) {

		if (variables.length < 2)
			throw new IllegalArgumentException("Minimum number of variables is 2!");

		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("The active sequence is not set !");
		
		if (sigmas.length != variables.length)
			throw new IllegalArgumentException("The number of variables and sigma values must be equal !");
		
		if (maxIter == null)
			maxIter = 1000;

		if (isActiveCMA == null)
			isActiveCMA = true;

		if (checkFeasableCount == null)
			checkFeasableCount = 5;

		if (stopFitness == null)
			stopFitness = 1e-6;

		if (populationSize == null)
			populationSize = Integer.valueOf((int) (4 + 3 * Math.log(variables.length)));

		List<OptimizationData> optimizationData = new ArrayList<>();
		optimizationData.add(new MaxEval(maxIter));
		optimizationData.add(new CMAESOptimizer.PopulationSize(populationSize));

		if (sigmas != null) {
			double[] da = new double[sigmas.length];
			for (int i = 0; i < sigmas.length; i++)
				da[i] = sigmas[i];
			optimizationData.add(new CMAESOptimizer.Sigma(da));
		}

		CMAESOptimizer opt = new CMAESOptimizer(maxIter, stopFitness, isActiveCMA, 0, checkFeasableCount,
				new JDKRandomGenerator(), false,null); // new MatchConvergenceChecker(stopFitness)

		return new Matcher(opt, optimizationData, Sequence.getActiveSequence(), Arrays.asList(variables), Arrays.asList(targets));
	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	
	@Command(key = "convert-trajectory", description = """
			Converts a global trajectory to the local frame defined by the reference orbit
			of the sequence. Either a particle name or a beam name must be specified, but
			not both.
			""")
	public void convertTrajectory(
			
			@CommandParameter(keys = { "-pn",	"--particle-name" }, required = false, help = """
			The particle name is used to determine the file name according to the naming 
			conventions which contains the trajectory data produced by a trajectory
			observer during single particle tracking.
			""") String particleName,
			
			@CommandParameter(keys = { "-bn","--beam-name" }, required = false, help = """
			The beam name is used to determine the file name according to the naming 
			conventions which contains the trajectory data produced by a trajectory 
			observer during beam tracking.
			""") String beamName,
			
			@CommandParameter(keys = { "-dr","--disk-radius" }, required = true, help = """
			Disk radius [m]. Should be big enough to cover the full aperture, but 
			small enough to not intersect with the orbit twice. The global trajectory(s)
			pierce through many disks along the reference
			""") double diskr,
			
			@CommandParameter(keys = { "-of", "--output-file" }, required = true, help = """
					The output file. The layout of the file columns:
						1: Longitudinal position in the sequence [m]
						2: Horizontal position in the local coordinate system [m].
						3: Horizontal angle in the local coordinate system.
						4: Vertical position in the local coordinate system [m].
						5: Vertical angle in the local coordinate system.
																		""") String outFile,
			@CommandParameter(keys = { "-bw","--backward" }, required = false, help = """
			Must be set true if the trajectory belongs to a tracking backward,
			in -Z direction of the reference orbit.
			""") Boolean backward) throws IOException {
		
		if (Sequence.getActiveSequence() == null) {
			logger.error("No sequence defined.");
			return;
		}
		if ((particleName == null && beamName == null) || (particleName != null && beamName != null)) {
			logger.error("Either a particle name or a beam name must be specified, but not both.");
			return;
		}
		if (backward == null)
			backward = false;
		ReferenceOrbit refOrb = Sequence.getActiveSequence().getReferenceOrbit();
		IntersectionCalculator isc;
		if (particleName != null)
			isc = IntersectionCalculator.getInstanceForParticle(particleName, refOrb, diskr, backward);
		else
			isc = IntersectionCalculator.getInstanceForBeam(beamName, refOrb, diskr, backward);

		isc.phaseSpaceToFile(outFile);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	
	@Command(key = "create-acc-element", description = """
		    Create an accelerator element, requires tiling config to be set using the 
		    'create-tiling-config' command. There are several options related to the solver.
		    There are two type of solvers: direct and iterative solvers with GMRES. 
		    For GMRES there are three pre-conditioner implemented: jacobi,slice,blockjacobi.
			""")
	public void createAccElement(@CommandParameter(keys = { "-tc","--tiling-config" }, required = true, help = """
			Tiling configuration created with create-tiling-config.
			""") TilingConfig tconfig,
			@CommandParameter(keys = { "-re","--relative-error" }, required = true, help = """
			Allowed relative error. Default is 1e-6. If an iterative solver is selected 
			with the '--solver' option, it will stop after the residual is below the given 
			limit.
			""") Double relativeError,
			@CommandParameter(keys = { "-s","--solver" }, required = false, help = """
			Specify the solver. Allowed values:"direct","jacobi","slice","blockjacobi". 
			Default: slice.	Jacoby is not recommended, usually converges very slowly, it is
			kept because of its simplicity. For accelerator elements usually the slice
			pre-conditioner	is the best choice. This splits the problem into slices along
			the longitudinal direction and solves each slice directly. An assembly of 
			sub-solutions are used as pre-conditioner. The block Jacobi does something
			similar, but instead of	slicing along the longitudinal direction, a set of 
			overlapping patches are solved directly and used as pre-conditioner. This
			is useful outside of accelerator physics problems, where there is no 
			longitudinal direction which can be used for slicing.
			""") String solverType,
			@CommandParameter(keys = { "-bjar","--bj-aggregate-radius" }, required = false, help = """
			The block-Jacobi pre-conditioner constructs first a coarse grid, then creates 
			aggregates of surface points around each coarse grid point. The
			degree of coarsening is determined by the aggregateRadius parameter. The
			distance between the coarse points are more than the coarsening radius. After
			coarsening, it creates local direct solvers belonging to patches around each 
			coarse point. The number of fine points in a patch must be at least the
			number of fine grid points in the aggregate belonging to the same coarse
			point. Therefore the patchRadius must be at least as big as the
			aggregateRadius, but usually bigger, to have some overlap. This overlap is
			needed to discard the result at the border of the patch, since those points
			can be far from the solution, due to the finite extent of the patch. Only the
			local solution belonging to the aggregate is used for pre-conditioning.		
			""") Double bjar,
			@CommandParameter(keys = { "-bjpr","--bj-patch-radius" }, required = false, help = """
			Patch radius [m] for the block-Jacobi pre-conditioner. Must be bigger than the
			aggregate radius.
			""") Double bjpr,
			@CommandParameter(keys = { "-sw","--slice-width" }, required = false, help = """
			Slice width in terms of number of profiles for the slice pre-conditioner, 
			default is 1.
			""") Integer sliceWidth,
			@CommandParameter(keys = { "-bjpr","--slice-overlap" }, required = false, help = """
			Slice overlap in terms of number of profiles for the slice pre-conditioner,
			default is 1.
			""") Integer sliceOverlap,
			@CommandParameter(keys = { "-rst","--gmres-restart" }, required = false, help = """
			Restart parameter for GMRES, default is 100. After the the given number of
			iterations the GMRES solver is restarted. Higher numbers gives usually faster
			convergence, but also higher memory consumption.
			""") Integer gmresRestart,
			@CommandParameter(keys = { "-d","--debug" }, required = false, help = """
			Outputs debug files for the block Jacobi and slice pre-conditioners
			These are:
			   /tmp/coarseGrid.txt  - The coordinates of the coarse grid points.
			   /tmp/fineGrid.txt    - The coordinates of the fine grid points.
			   /tmp/aggregates.txt  - The coordinates of the aggregates.
			""") Boolean debug)
			throws NotSetException, PositionMismatchException {

		SolverOptions opt;
        
		if(relativeError==null)
        	relativeError=1e-6;
		
        if (solverType == null)
			solverType = "slice"; // by default

		if (solverType.equals("direct")) {
			// preconditioner is ignored for direct
			opt = new SolverOptions(SolverOptions.SolverType.DIRECTSOLVER, SolverOptions.Preconditioner.NONE);
		} else if (solverType.equals("jacobi")) {
			opt = new SolverOptions(SolverOptions.SolverType.GMRES, SolverOptions.Preconditioner.JACOBI);
		} else if (solverType.equals("blockjacobi")) {
			opt = new SolverOptions(SolverOptions.SolverType.GMRES, SolverOptions.Preconditioner.BLOCKJACOBI);
			if (bjar == null) {
				throw new NotSetException(
						"When using the block-Jacobi preconditioner the --bj-aggregate-radius must be set !");
			} else {
				opt.setAggregateRadius(bjar);
			}
			if (bjpr == null) {
				throw new NotSetException(
						"When using the block-Jacobi preconditioner the --bj-patch-radius must be set !");
			} else {
				opt.setPatchRadius(bjpr);
			}
		} else if (solverType.equals("slice")) {
			opt = new SolverOptions(SolverOptions.SolverType.GMRES, SolverOptions.Preconditioner.SLICE); // default
			if (sliceOverlap == null)
				sliceOverlap = 1;
			if (sliceWidth == null)
				sliceWidth = 1;
			opt.setSliceOverlap(sliceOverlap);
			opt.setSliceWidth(sliceWidth);
		} else {
			throw new IllegalArgumentException("Can't recognize the solver type: " + solverType);
		}

		if (gmresRestart != null) {
			opt.setGmresRestart(gmresRestart);
		}

		if (debug == null)
			debug = false;

		if (debug)
			opt.setDebug(true);

		Tiling tiling = new Tiling(tconfig);
		tiling.solve(relativeError, opt);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "create-beam", description = """
		    Create a beam for tracking in the selected sequence. The beam should be
		    assigned to a variable and passes to the track command.
		    """)
	public Beam createBeam(@CommandParameter(keys = { "-t",	"--type" }, help = """
			The particle type one of these are defined: "proton", "antiproton", "electron",
			"positron."	If non of there pre-defined particles are tracked, the --momentum 
			and --charge parameters	must be specified and this parameter should 
			not be given.
			""") String type,
			@CommandParameter(keys = { "-m","--momentum" }, required = true, help = """
			The momentum of the beam [GeV/c].
			""") Double mom,
			@CommandParameter(keys = { "-he","--h-emittane" }, required = false, help = """
		    The horizontal 1 sigma emittance of the beam  [m*rad]. If not given,taken from
		    the Twiss parameter.
		    """) Double he,
			@CommandParameter(keys = { "-ve",
					"--v-emittane" }, required = false, help = """
			The vertical 1 sigma emittance of the beam  [m*rad]. If not given, taken from
			the Twiss parameter
			""") Double ve,
			@CommandParameter(keys = { "-lp","--longitudinal-position" }, help = """
			The longitudinal position compared to the start of the reference orbit [m].
			""") Double lPos,
			@CommandParameter(keys = { "-dp","--dpop" }, help = """
			Relative momentum deviation (1 sigma) from the reference momentum.
			""") Double dpOp,
			@CommandParameter(keys = { "-mass",	"--mass" }, help = """
			The mass of the particle, if no particle type is given.
			Otherwise ignored. [kg].
			""") Double mass,
			@CommandParameter(keys = { "-c","--charge" }, help = """
			The charge of the particle, taken into account only when no particle type
			is given. Otherwise ignored. [Coulomb].
			""") Double charge,
			@CommandParameter(keys = { "-na", "--name" }, required = true, help = """
			The name of the beam. During the tracking various tracking observers write
			files based on the beam name.
			""") String name,
			@CommandParameter(keys = { "-di","--distribution" }, required = true, help = """
			The type of the distribution: 'gaussian' or 'ellipse'.
			""") String distribution,
			@CommandParameter(keys = { "-n","--number-of-particles" }, required = true, help = """
			The number of particles in the beam.
			""") Integer nParticles,
			@CommandParameter(keys = { "-twp","--twiss-parameters" }, required = false, help = """
			A TwissParameters object to be used at the given longitudinal position for 
			transfer lines. For rings it is ignored and the calculated optics is used
			instead.
			""") TwissParameters twiss)
			throws OutOfApertureException, KnownFatalException {

		if (Sequence.getActiveSequence() == null) {
			throw new IllegalArgumentException("No active sequence.");
		}
		if (Sequence.getActiveSequence().isCircular() && lPos == null) {
			throw new IllegalArgumentException("The longitudinal position must be given for circular sequences.");
		}

		if (lPos == null)
			lPos = (double) 0;
		if (lPos < 0) {
			throw new IllegalArgumentException("The longitudinal position must be positive");
		}
		if (lPos > Sequence.getActiveSequence().getReferenceOrbit().getOrbitLength()) {
			throw new IllegalArgumentException("The longitudinal position must be smaller than the sequence length");
		}

		if (Sequence.getActiveSequence().isCircular() && Sequence.getActiveSequence().getOptics() == null) {
			throw new IllegalArgumentException("The optics must be set for circular machines.");
		}
		if (type == null && (mass == null || charge == null)) {
			throw new IllegalArgumentException("The mass and charge must be given if no type is given.");
		}
		if (type != null) {
			ParticleType pType = ParticleType.parse(type);
			mass = pType.getMass();
			charge = pType.getCharge();
		}

		if (distribution == null || (!distribution.equals("gaussian") && !distribution.equals("ellipse"))) {
			throw new IllegalArgumentException("A valid distribution  distribution must be given.");
		}

		if (!distribution.equals("gaussian") && !distribution.equals("ellipse"))
			throw new IllegalArgumentException("Unrecognized distribution: " + distribution);

		if (Sequence.getActiveSequence().isCircular()) {
			if (twiss == null)
				twiss = Sequence.getActiveSequence().getOptics().getTwissParametersAt(lPos);
		} else { // TL
			if (twiss == null)
				throw new IllegalArgumentException("The Twiss parameter option must be present for transwer lines !");
		}

		if (he == null)
			he = twiss.hEllipse().em();
		if (ve == null)
			ve = twiss.vEllipse().em();

		if (dpOp == null) {
			dpOp = twiss.dpOp();
		}

		Beam beam;

		PhaseSpaceEllipse  hEll= new PhaseSpaceEllipse(he, twiss.hEllipse().alpha(), twiss.hEllipse().beta(), twiss.hEllipse().pos(), twiss.hEllipse().angle());
		PhaseSpaceEllipse  vEll= new PhaseSpaceEllipse(ve, twiss.vEllipse().alpha(), twiss.vEllipse().beta(), twiss.vEllipse().pos(), twiss.vEllipse().angle());
		TwissParameters tp= TwissParameters.builder().copyOf(twiss).hEllipse(hEll).vEllipse(vEll).dpOp(dpOp).build();

		if (distribution.equals("gaussian")) {
			BeamSampler bs = new GaussianBeamSampler(tp);
			beam = new Beam(bs, mom, Sequence.getActiveSequence().getReferenceOrbit(), lPos, Sequence.getActiveSequence().getPotentialProvider(),
					mass, charge, nParticles, name);
		} else {
			EllipseBeamSampler bs = new EllipseBeamSampler(tp);
			beam = new EllipseBeam(bs, mom, Sequence.getActiveSequence().getReferenceOrbit(), lPos,
					Sequence.getActiveSequence().getPotentialProvider(), mass, charge, nParticles, name);
		}

		return beam;
	}
	
	        ////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "create-beam-region", description = """
			Creates a default vacuum chamber by extruding a profile along the design orbit 
			and add all vacuum chambers specified in the sequence, if any. The command 
			writes a cover with extension  acoording to the naming convenvtions.The command
			also writes an STL file. It can be used for	checking the beam region by
			displaying it in a 3D viewer, like Paraview. The command returns the name of
			the cover file.
			""")
	public String createBeamRegion(
			@CommandParameter(keys = { "-dp","--default-profile" }, required = true, help = """
		    A default profile. It is used everywhere along the design orbit to define the
		    beam region unless a vacuum chamber is specifically given for one or more
		    element(s) in the sequence. In that case the default profiles are replaced 
		    in the region of the the given vacuum chamber(s).
		    """) Profile profile,
			@CommandParameter(keys = { "-d","--distance" }, required = true, help = """
			The distance between the default profiles [m].
			""") double distance,
			@CommandParameter(keys = { "-br","--ball-radius" }, required = false, help = """
		    If this parameter is given, the beam region is covered by balls with the given
			radius and a HCP cover file is created with the name of the active sequence
			and with the extension according to the nameing conventions. If not given,
			no HCP latticle cover file will be created. In that case a single row cover
			file can be created with the  'create-single-row-cover' command.
			""") Double ballRadius,			
			@CommandParameter(keys = { "-ext","--extension" }, required = false, help = """
		    In case of transfer lines, the beam region covered is extended some distance at
		    the start of the sequence in the negative S direction and at the end of the 
		    sequence in the positive S direction to surely cover the fist and last step of
		    the tracking. The extension parameter is ignored for rings. Unit is in [m].
		    Default value is 0.05 [m]
			""") Double extention
			)
			throws FileNotFoundException, KnownFatalException {
		
		if(Sequence.getActiveSequence()==null)
			throw new IllegalArgumentException("No active sequence.");

		if(extention==null) {
			extention=0.05;
		}
		
		BeamRegion beamRegion = new BeamRegion(Sequence.getActiveSequence(), distance, profile,extention);
		String outFileName = Sequence.getActiveSequence().getName() + FileNamingConventions.HCP_COVER_FILE_EXTENSION;
		
		if(ballRadius!=null	)		
			beamRegion.writeHCPLatticeCover(outFileName,ballRadius);

		String stlFile=Sequence.getActiveSequence().getName()+".stl";
		beamRegion.writeSTLTextFile(stlFile);

		return outFileName;
	}

            ////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "create-bending-path", description = """
			Create a path for a bending element using a deflection angle, bending radius 
			and the number of segments in the path. The number of segments must be even.
			The path is used for tiling the surface surrounding the accelerator element.
			See also the related commands:
			 'create-tiling-config' and 'create-acc-element'		
			""")
	public void createBendingPath(@CommandParameter(keys = { "-da",	"--deflection-angle" }, required = true, help = """
			The deflection angle of the bending element[radians].
			""") Double deflectionAngle,
			@CommandParameter(keys = { "-r","--radius" }, required = true, help = """
			The bending radius of the element [m].
			""") Double bendingRadius,
			@CommandParameter(keys = { "-ns","--number-of-segments" }, required = true, help = """
			The number of segments in the path. Must be a positive even number. This
			number determines the longitudinal density of the surface points surrounding 
			the beam region. This influences the accuracy of the reconstruction of the
			field of the element.
			""") Integer nSegments,
			@CommandParameter(keys = { "-el","--extention-length" }, required = false, help = """
			The output path consists of an arc and two straight sections at the ends. 
			The length [m] of the straight extentions at both ends of the arc is
			determined by this value.
			""") Double extentionLength,
			@CommandParameter(keys = { "-n","--name" }, required = true, help = """
			The name of the element. The output file name will be constructed from this
			using the file naming conventions. Such as: "elementname-PATH.txt"
			""") String name,
			@CommandParameter(keys = { "-t","--translation" }, required = false, help = """
			A translation to be applied after the rotation if any. The translation vector
			can be created by the 'create-vector3d' command and assigned to a variable.
			The variable then can be given to this command option.
			""") Vector3D translation,
			@CommandParameter(keys = { "-rot","--rotation" }, required = false, help = """
			A rotation to be applied, before the translation if any. The rotation
			can be created by the 'create-rotation' command and assigned to a variable.
			The variable then can be given to this command option.
			""") Rotation rot,			
			@CommandParameter(keys = { "-bt", "--bending-type" }, required = true, help = """
			The type of the bending. 'S' stands for sector bending and 'R' for rectangular
			bending. The default is 'S. For a rectangular bending the entry direction  of 
			the beam is the +Z axis. For sector bending the angle between the beam entry 
			direction and the +Z axis is deflection/2.
			""") String bendingType) {

		if (extentionLength == null)
			extentionLength = Double.valueOf(0);

		boolean sectorBending = true;

		if (bendingType != null) {
			if (bendingType.toUpperCase().equals("R"))
				sectorBending = false;
			if (!bendingType.toUpperCase().equals("R") && !bendingType.toUpperCase().equals("S"))
				throw new IllegalArgumentException("Unknown bending type: " + bendingType);
		}

		List<Vector3D> path = PathUtils.createBendingPath(deflectionAngle, bendingRadius, nSegments, extentionLength,
				sectorBending);

		if (rot == null)
			rot = Rotation.IDENTITY;

		if (translation == null)
			translation = Vector3D.ZERO;

		path = PathUtils.transform(path, rot, translation);

		String filename = FileNamingConventions.getPathFileName(name);
		FileUtils.writeVectors(filename, path);
	}
            ////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "create-checkpoint-file", description = """
			Creates a file based on the path file. It can be used for getting the field
			values from the EM field calculation software. Usually the path file is
			up-sampled to have a higher resolution. It maybe also shifted, for example
			for a quadrupole which have zero transverse field at the center. The path
			file can be also extended at both ends to see better the residual of the field.
			It is typically used after the 'create-acc-element' command to compare the
			field obtained from the solution file to the reference field values.
			The reference field is often obtained at a set of points from an EM field
			calculation software like COMSOL or OPERA, which needs a list of 3D points
			as an input and it outputs another file containing in each row the coordinates
			of the checkpoints and the values of the field components. This reference field  
			file is taken by the 'check-field' command to calculate the error of the 
			reconstructed field. 
			""")
	public void createCheckpointFile(@CommandParameter(keys = { "-tc","--tiling-config" }, required = true, help = """
			Tiling configuration created with 'create-tiling-config' command. This object
			contains most of the information needed by this command.
			""") TilingConfig tconfig,
			@CommandParameter(keys = { "-tr","--translation" }, required = false, help = """
			The translation to be applied to the path file, default is zero vector.
			""") Vector3D translation,
			@CommandParameter(keys = { "-e","--extention" }, required = false, help = """
			An extension to be added at both ends of the path, default is 0 [m].
			""") Double extension,
			@CommandParameter(keys = { "-rss","--resampling-step-size" }, required = true, help = """
			Step size [m] for the up-sampling of the path.
			""") Double stepSize)
			throws FileNotFoundException, NotSetException {

		Tiling t = new Tiling(tconfig);

		if (extension == null)
			extension = Double.valueOf(0);

		if (translation == null)
			translation = Vector3D.ZERO;

		t.writeCheckPoints(translation, stepSize, extension.doubleValue());
	}
            ////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-ellipse-profile", description = """
			Creates an ellipse shaped profile used  for constructing  the beam region, or
			a vacuum chamber of one or more specific element. It is also used for creating
			the boundary surface around an accelerator element for determining the strength
			of point sources, see also the 'create-tiling-config' command. The profile is
			defined by the horizontal and vertical radius and the number of steps in the
			profile. The profile is sampled at the given number of steps and the points are
			connected by straight lines. The profile is closed, the last point is connected
			to the first one. The coordinate system of the profile is defined is 2D, it is in 
			the X Y plane. These profiles are extruded along the reference orbit to create
			the beam region or a vacuum chamber for n element.
			""")
	public Profile createEllipseProfile(
			@CommandParameter(keys = { "-xr","--x-radius" }, required = true, help = """
			Horizontal radius of the ellipse [m].
			""") double xrad,
			@CommandParameter(keys = { "-yr","--y-radius" }, required = true, help = """
			Vertical radius of the ellipse [m].
			""") double yrad,
			@CommandParameter(keys = { "-s","--steps" }, required = true, help = """
			Number of steps in the profile. When the profile is used for constructing the
			boundary surface of an accelerator element, this number determines the density
			of the surface points at which the field of the point sources will be matched
			to the given input field. The higher the number the more accurate the field
			recosntruction will be.
			""") int steps,
			@CommandParameter(keys = { "-sa","--start-angle" }, help = """
			Starting angle of the first step.  Between 0 and 2*Pi, zero by default.
			This parameter is used for avoid twisting between neighboring profiles when
			they are connected. If this parameter is zero, the first profile points
			starts at fi = -3*PI/4. This is because a rectangular profile starts at the
			left bottom corner. We need to align the ellipse profile to start at the same
			angle. 
	 		""") Double startAngle) {
		if (startAngle == null)
			startAngle = 0.0;

		return new Profile(xrad, yrad, steps, startAngle);
	}

	
	// 		////////////////////////////////////////////////////////////////////////////////    80 character
	// @Command(key = "create-lattice-cover", description = """
	// 		Creates a hexagonal closed packed cover for the beam region. The cover is created
	// 		by placing overlapping spheres of the given radius at the vertices of the
	// 		HCP lattice. The sphere cover the entire volume of the beam region without gaps.
	// 		An output file will be produced which contains the coordinates of the centers
	// 		of the spheres. The file  retuned is named like the input STL file, but with an
	// 		extention attached at the end according to the {@link FileNamingConventions}
	// 		class and the origilal '.stl' extension removed.			
	// 		""")
	// public String createLatticeCover(@CommandParameter(keys = { "-if","--stl-file" }, required = true, help = """
	// 		The input STL (Standard Tessellation Language) file, describing a surface inside
	// 		which the volume to be filled.
	// 		""") String file,
	// 		@CommandParameter(keys = { "-r","--radius" }, required = true, help = """
	// 		The radius of the balls filling the volume. The smaller the radius the faster
	// 		the calculation of the field evaluation will be, but the more balls will be 
	// 		needed to cover the volume and the size of the field map will be bigger.
	// 		""") double rad)
	// 		throws KnownFatalException {

	// 	SphereCovering hcpCover = SphereCovering.createHCPLatticeCover(file, rad);

	// 	if (!file.contains(".stl"))
	// 		throw new IllegalArgumentException("The file must have a .stl extension");
	// 	String outFileName=	file.replace(".stl", FileNamingConventions.HCP_COVER_FILE_EXTENSION);
	// 	hcpCover.toFile(outFileName);
		
	// 	return outFileName;

	// }
			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-particle", description = """
			Create a particle for tracking. The particle should be assigned to a variable
			and passed to the 'track' command.
			""")
	public Particle createParticle(@CommandParameter(keys = { "-t","--type" }, help = """
			The particle type. The following types are defined: "proton", "antiproton", 
			"electron", "positron". Ithis list likely to be extended in the future.
			If the particle type is given, the --mass and --charge parameters are ignored.
			If the particle you want to track is not in the list, you can specify the mass
			and charge using the --mass and --charge parameters.
			""") String type,
			@CommandParameter(keys = { "-m","--momentum" }, required = true, help = """
			The momentum of the particle [GeV/c].
			""") Double mom,
			@CommandParameter(keys = { "-dx","--x_position" }, required = true, help = """
			The horizontal position from the reference orbit [m].
			""") Double dx,
			@CommandParameter(keys = { "-ax","--x_angle" }, required = true, help = """
			The horizontal angle compared to the reference orbit [rad].
			""") Double ax,
			@CommandParameter(keys = { "-dy","--y_position" }, required = true, help = """
			The vertical position from the reference orbit [m].
			""") Double dy,
			@CommandParameter(keys = { "-ay","--y_angle" }, required = true, help = """
			The vertical angle compared to the reference orbit [rad].
			""") Double ay,
			@CommandParameter(keys = { "-lp","--longitudinal-position" }, help = """
			The longitudinal position compared to the start of the reference orbit [m].
			""") Double lPos,
			@CommandParameter(keys = { "-dp","--dpop" }, help = """
			Relative momentum deviation from the reference momentum: (p-p0)/p0
			""") Double dpOp,
			@CommandParameter(keys = { "-mass",	"--mass" }, help = """
			The mass [kg] of the particle, should be given only if no particle type is given,
			otherwise ignored. 
			""") Double mass,
			@CommandParameter(keys = { "-c","--charge" }, help = """
			The charge of the particle [Coulomb], should be given only if no particle type is
			given, otherwise ignored. 
			""") Double charge,
			@CommandParameter(keys = { "-n", "--name" }, help = """
			The name of the particle. The name is used by the tracking observers to write
			trajectory of phase-space data files. If not given a default name is used,
			which is 'noname'
			""") String name)
			throws OutOfApertureException, KnownFatalException {

		if (Sequence.getActiveSequence() == null) {
			throw new IllegalArgumentException("No active sequence.");
		}

		if (lPos == null)
			lPos = (double) 0;

		if (dpOp == null)
			dpOp = (double) 0;

		if (type != null) {
			ParticleType pt = ParticleType.parse(type);
			mass = pt.getMass();
			charge = pt.getCharge();
		}

		ParticleFactory pf = new ParticleFactory(Sequence.getActiveSequence().getReferenceOrbit());
		double ds = 0; // position referenced to the synchronous particle, will be used only when RF
						// fields are implemented
		PhaseSpaceCoordinates phs = new PhaseSpaceCoordinates(dx, ax, dy, ay, ds, dpOp);

		Particle p = pf.getParticle(mom, phs, lPos, Sequence.getActiveSequence().getPotentialProvider(), mass, charge);

		if (name != null)
			p.setName(name);

		logger.info("Created particle: " + p.toString());

		return p;

	}
			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-rect-profile", description = """
			Creates a rectangular shaped profile that can be saved and used to create the 
			vacuum chamber around the beam region or individual vacuum chambers. It is also
			used for creating the boundary 	surface around an accelerator element for
			determining the strength of point sources, see also the 'create-tiling-config'
			command. The coordinate system of the profile is defined is 2D, it is in 
			the X Y plane. These profiles are extruded along the reference orbit to create
			the beam region or a vacuum chamber for n element.
			""")
	public Profile createRectProfile(
			@CommandParameter(keys = { "-xmin",	"--xmin" }, required = true, help = """
			Minimum X coord. of the profile [m]. 
			""") double xmin,
			@CommandParameter(keys = { "-xmax",	"--xmax" }, required = true, help = """
			Maximum X coord. of the profile [m].
			""") double xmax,
			@CommandParameter(keys = { "-ymin",	"--ymin" }, required = true, help = """
			Minimum Y coord. of the profile.
			""") double ymin,
			@CommandParameter(keys = { "-ymax",	"--ymax" }, required = true, help = """
			Maximum Y coord. of the profile.
			""") double ymax,
			@CommandParameter(keys = { "-xsteps", "--x-steps" }, required = true,help = """
			Number of steps in the X direction.			
			""") int xsteps,
			@CommandParameter(keys = { "-ysteps", "--y-steps" }, required = true,help = """
			Number of steps in the Y direction.			
			""") int ysteps) {

		return new Profile(xmin, xmax, ymin, ymax, xsteps, ysteps);
	}
			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-rotation", description = """
			Creates a Rotation object using either four vectors or a vector and an angle. 
			When 4 vectors are given, the rotation is constructed from the vectors such that
			the first and second vectors are rotated to the third and fourth vectors
			respectively. When a vector and an angle is given, the rotation is constructed
			such that the rotation is around the vector by the given angle. The vectors can
			be created by the 'create-vector3d' command and assigned to a variable. The 
			variable then can be given to this command.
			""")
	public Rotation createRotation(
		@CommandParameter(keys = { "-v1" }, required = true, help = """
			Vector1.  Only this vector is used when a vector and an angle is given.
			""") Vector3D v1,
		@CommandParameter(keys = { "-v2" }, help = """
			Vector2, used when 4 vector defines the rotation
			""") Vector3D v2,
		@CommandParameter(keys = { "-v3" }, help = """
			Vector3, used when 4 vectors defines the rotation,
			""") Vector3D v3,
		@CommandParameter(keys = { "-v4" }, help = """
			Vector4, used when 4 vectors defines the rotation,
			""") Vector3D v4,
		@CommandParameter(keys = { "-a", "--angle" }, help = """
			Angle in radians counter-clockwise. Used only when a vector and this angle 
			defines the rotation.
			""") Double angle) {
		if (v2 != null & v3 != null & v4 != null) {
			return new Rotation(v1, v2, v3, v4);
		} else if (angle != null) {
			return new Rotation(v1, angle, RotationConvention.VECTOR_OPERATOR);
		} else {
			throw new IllegalArgumentException(
					"Please supply either 4 vectors [-v1, -v2, -v3, -v4] or a vector and an angle [-v1, -a]");
		}
	}
	
			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-sequence", description = """
			Creates a sequence for an accelerator that can contain the accelerator elements.
			The sequence holds the reference orbit, the optics and the beam line elements.
			The sequence with the given name is automatically stored, no need to assign it
			to a variable. The sequence can be made active by the 'use-sequence' command.
			Elements can be added to the sequence by the following commands:
			'add-arbitrary','add-bending','add-bpm','add-marker','add-staright'.
			After all elements has been added, the sequence should be closed by the
			'end-sequence' command. 
			""")
	public void createSequence(
			@CommandParameter(keys = { "-n","--name" }, required = true, help = """
			The name for the sequence. The sequence is stored with this name and can be
			made active by the 'use-sequence' command.
			""") String name,
			@CommandParameter(keys = { "-oss","--orbit-step-size" }, required = false, help = """
			Orbit step size [m]. It determines the resolution of the optics calculation.
			Default is 0.01 m
			""") Double orbitStep,
			@CommandParameter(keys = { "-ci","--circular" }, required = false, help = """
			Whether the sequence is a circular machine or a transfer line. Circular by
			default, if not given.
			""") Boolean circular,
			@CommandParameter(keys = { "-lfs","--local-frame-strategy" }, required = false, help = """
			A strategy to construct the local coordinates. Allowed values:
			"PARALLEL_TRANSPORT" (default) or "MINIMIZE_TORSION". For a planar machine
			these are equivalent. For a non-planar machine the "MINIMIZE_TORSION" strategy
			tries to minimize the torsion of the local coordinate system.
			
			The "PARALLEL_TRANSPORT"  strategy parallel transports the given Y vector
			along the reference orbit. See p. 9 and p. 10 of
			'Parallel Transp ort Approach to Curve Framing' by Andrew J. Hanson and Hui Ma
			See: https://legacy.cs.indiana.edu/ftp/techreports/TR425.pdf
			The local frames along the reference orbit can be written to files by the
			'end-sequence' command options and plotted or used otherwise.
			""") String str,
			@CommandParameter(keys = { "-va","--vertical-axis" }, required = false, help = """
			The preferred vertical direction for constructing the local coordinates (+Y by default)
			This option is usually used only when the a transfer line in a non-planar and
			the vertical direction is non-trivial. The vector can be created by the
			'create-vector3d' command and assigned to a variable. The variable then can be
			given to this command option.
			""") Vector3D vertical) {

		if (name == null)
			throw new IllegalArgumentException("Sequence name can not be empty");

		if (circular == null)
			circular = Boolean.TRUE;

		LocalFramesCalculator.Strategy strategy = LocalFramesCalculator.Strategy.PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION;

		if (str == null)
			strategy = LocalFramesCalculator.Strategy.PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION;
		else if (str.equals("PARALLEL_TRANSPORT"))
			strategy = LocalFramesCalculator.Strategy.PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION;
		else if (str.equals("MINIMIZE_TORSION"))
			strategy = LocalFramesCalculator.Strategy.MINIMIZE_TORSION;
		else
			new IllegalArgumentException("Unrecognized strategy option: " + str);

		Sequence sequence = new Sequence(name, circular, strategy, vertical);
		
		if(orbitStep !=null)
			sequence.setOrbitStep(orbitStep);

			Sequence.getSequences().put(name, sequence);

		Sequence.setActiveSequence(sequence);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-single-row-cover", description = """
			Creates a  cover for the orbit and writes it to a cover file. A single row of
			overlapping spheres are placed along the reference orbit. An active sequence
			must be selected before this command is	used. The spheres are placed such that 
			they cover the entire volume of the beam region without gaps. The distances 
			between the sphere centers are determined by the given aperture	radius and the
			ball radius. This type of cover can be used for simple beam lines or rings with
			uniform circular cross section. For more complex shapes use the
			'create-beam-region' command. The single row cover is	slower to evaluate at 
			tracking time than the lattice cover, but it is faster to create and the size 
			of the cover file is smaller. The output file name returned is derived from the
			sequence name by adding an extension according to the {@FileNamingConventions}
			class.			
			""")
	public String createSingleRowCover(
			@CommandParameter(keys = { "-ar","--aperture-radius" }, required = true, help = """
			Radius  of the beam region [m].
			""") double apertureRadius,
			@CommandParameter(keys = { "-br","--ball-radius" }, required = true, help = """
			Ball radius [m]. The radius of the balls filling the volume must be bigger than
			the aperture radius. The user must be careful not to make the ball radius too
			big, otherwise the point sources will be too close to the balls or will be
			inside of them. The sources can not be inside the balls. When the sources are
			too close to the balls, the field expansion will require higher degree spherical
			harmonics  which  will make the field map slower and bigger or even impossible
			to achieve the required accuracy. The quality of the field map can be checked
			by setting the --statistics option of the 'expand-single-row' command true.
			""") double ballRadius) {

		if (Sequence.getActiveSequence() == null) {
			throw new IllegalArgumentException("No active sequence.");
		}

		SphereCovering cover = SphereCovering.createSingleRowCover(Sequence.getActiveSequence().getReferenceOrbit().getOrbitPoints(),
				apertureRadius, ballRadius, Sequence.getActiveSequence().getReferenceOrbit().isCircular());
		String f=Sequence.getActiveSequence().getName() + FileNamingConventions.SINGLE_ROW_COVER_FILE_EXTENSION;
		cover.toFile(f);
		return f;
	}

			//////////////////////////////////////////////////////////////////////////////// 80
	@Command(key = "create-tiling-config", description = """
			Creates a tiling configuration object. The tiling configuration contains
			information describing the tiling of the boundary surface around an accelerator
			element. The tiling is	used for determining the strength of the point sources
			which reconstruct the field of the element. The tiling parameters are determined
			by the geometry of the element and the desired accuracy of the	field
			reconstruction. The tiling configuration is used the following commands:
			'write-surface-points', 'check-field','create-acc-element',
			'create-checkpoint-file'
			""")

	// TODO add command to create a profile from a file
	public TilingConfig createTilingConfig(
		    
			@CommandParameter(keys = { "-n", "--name" }, required = true, help = """
			The name of the element. Several input and aoutput files will be created from
			this name using the file naming conventions.
			""") String elementName,
			
			@CommandParameter(keys = { "-p", "--profile" }, required = true, help = """
			A profile to be extruded. Profile describes the cross section of the element
			at a particular point along the path ot the element, which is usually the
			design orbit. The profile can be created by the 'create-rect-profile' or
			'create-ellipse-profile' commands.

			""") Profile profile,
			@CommandParameter(keys = { "-lu", "--length-unit" }, required = true, help = """
			Length unit used in the input files: [mm, cm or m]. In SIMPA the length unit
			is in meters, however the input files for the field solver software (COMSOL,
			OPERA, etc.) used for getting the field values on the reference point on the
			boundary surface may use different units. This length unit parameter specifies
			what unit the data in those files written by SIMPA will be. The commands taking
			into account this information.
			""") String unit,
			
			@CommandParameter(keys = { "-e", "--elevation" }, required = true, help = """
			Elevation of the point sources above the boundary surface of the element.
			This parameter is important for several reasons. The elevation should be
			chosen such that the point sources are not too close to the boundary surface
			of the element. The point sources can not be inside the balls of the cover.
			When the sources are too close to the balls, the field expansion will require
			higher degree spherical harmonics  which  will make the field map slower and
			bigger or even impossible to achieve the required accuracy. On the other hand,
			when the sources are too far from the boundary surface, the solving of the
			strengths of the point sources will be difficult, because the matrix
			describing the linear system will be ill-conditioned. As a rule of thumb
			the elevation should be about the same as the distance between the points on
			the boundary surface. The elevation should be given in meters.
			""") double elevation,
			
			@CommandParameter(keys = { "-o", "--output-format" }, help = """
			Output format for commands generating files for field solver software (COMSOL,
			OPERA, etc.). The default is "opera3d", other option is "txt". These are almost
			the same except the OPERA3D format has header lines.
			 """) String outputFileFormat,
			
			@CommandParameter(keys = { "-s", "--scaling" }, required = true, help = """
			Scaling factor.
			""") double fieldScalingFactor,

			@CommandParameter(keys = { "-tt", "--tile-type" }, required = false, help = """
			The type of the tile: 'rectangular' or 'triangular'. The default is
			'rectangular'. Certain surfaces can not be tiled with rectangular tiles,
			for example a bended tube. In this case the triangular tiling should be used.
			""") String tileType,
						
			@CommandParameter(keys = { "-ft", "--field-type" }, required = true, help = """
			The type of the field: 'se' for static electric field or 'sm' for static magnetic
			field. When the filed type is electric, the sources around the boundary surface
			are electric point sources. When the field type is magnetic, the sources are
			magnetic point sources. Magnetic point sources are essentially tho perpendicular
			infinitesimally small wires oriented such that they are tangential to the boundary
			surface. The command 'create-acc-element' solves for the strengths of these point
			sources.
			""") String fieldType) throws FileNotFoundException {

		FieldType ft = parseFieldType(fieldType);
		OutputFileFormat of = parseOutputFileFormat(outputFileFormat);
		String pf = FileNamingConventions.getPathFileName(elementName);

		TilingConfig tc= new TilingConfig(elementName, profile, FileUtils.readVectors(pf, LengthUnit.M), LengthUnit.parse(unit),
				elevation, of, fieldScalingFactor, ft);
		
		if(tileType==null)
			tileType="rectangular";
		
		switch (tileType){
			case "triangular":{
				 tc.setQuadrature(Quadrature.TRIANGLE_3_POINTS);
				break;
			}
			case "rectangular":{
				tc.setQuadrature(Quadrature.QUADRILATERAL_4_POINTS);
				break;
			}
			default:{
				throw new IllegalArgumentException("Unknown tile type: " + tileType);				
			}	
		}

		return tc;
	}
	
			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-straight-path", description = """
			Creates a straight path for tiling the boundary surface around an accelerator
			element. This path usually represents the design orbit through the element. The 
			points in the path determine the longitudinal positions of the  transverse
			profiles, which themselves determine the tiles of the boundary surface. This is
			a utility command. The path can be created by other means as well, for example
			a spreadsheet.
			""")
	public void createStraightPath(
			@CommandParameter(keys = { "-sx","--start-x" }, required = true, help = """
			X coordinate of the line starting point.
			""") Double sx,
			@CommandParameter(keys = { "-sy","--start-y" }, required = true, help = """
			Y coordinate of the line starting point.
			""") Double sy,
			@CommandParameter(keys = { "-sz","--start-z" }, required = true, help = """
			Z coordinate of the line starting point.
			""") Double sz,
			@CommandParameter(keys = { "-ex","--end-x" }, required = true, help = """
			X coordinate of the line end point.
			""") Double ex,
			@CommandParameter(keys = { "-ey","--end-y" }, required = true, help = """
			Y coordinate of the line end point.
			""") Double ey,
			@CommandParameter(keys = { "-ez","--end-z" }, required = true, help = """
			Z coordinate of the line end point.
			""") Double ez,
			@CommandParameter(keys = { "-n","--name" }, required = true, help = """
			The name of the element for which the path is created. The path file name will
			be constructed from this using the file naming conventions. Such as:
			"elementname-PATH.txt"
			""") String elementName,
			@CommandParameter(keys = { "-ns","--number-of-steps" }, required = true, help = """
			The number of steps in the path. This number determines the longitudinal
			density of the surface points surrounding the beam region. This influences the
			accuracy of the reconstruction of the field of the element.
			""") Integer nSteps)throws IOException {

			List<Vector3D> points = PathUtils.createStraightLine(new Vector3D(sx, sy, sz), new Vector3D(ex, ey, ez),
					nSteps);
			String fn = FileNamingConventions.getPathFileName(elementName);
			FileUtils.writeVectors(fn, points);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-twiss", description = """
		    Creates a Twiss parameters object. Usually the Twiss parameter are assigned to
			a variable and passed to the the 'create-beam' command.	See also the 'twiss'
			and 'beam-twiss' commands.
			""")
	public TwissParameters createTwiss(
			@CommandParameter(keys = { "-x","--hor-position" }, required = true, help = """
			The horizontal position of the particle or beam center [m] compared to the
			reference orbit.
			""") Double x,
			@CommandParameter(keys = { "-xp","--hor-angle" }, required = true, help = """
			The horizontal angle [rad].
			""") Double xp,
			@CommandParameter(keys = { "-y","--ver-position" }, required = true, help = """
			The vertical position [m].
			""") Double y,
			@CommandParameter(keys = { "-yp", "--ver-angle" }, required = true, help = """
			The vertical angle [rad].
			""") Double yp,
			@CommandParameter(keys = { "-ha","--hor-alpla" }, required = true, help = """
			The horizontal alpha
			""") Double ha,
			@CommandParameter(keys = { "-va", "--ver-alpha" }, required = true, help = """
			The vertical alpha
			""") Double va,
			@CommandParameter(keys = { "-hb", "--hor-beta" }, required = true, help = """
			The horizontal beta [m].
			""") Double hb,
			@CommandParameter(keys = { "-vb", "--ver-beta" }, required = true, help = """
			The vertical beta [m].
			""") Double vb,
			@CommandParameter(keys = { "-hd","--hor-dispersion" }, required = true, help = """
			The horizontal dispersion [m].
			""") Double dispH,
			@CommandParameter(keys = { "-vd","--ver-dispersion" }, required = true, help = """
			The vertical dispersion [m]
			""") Double dispV,
			@CommandParameter(keys = { "-hdp","--hor-disp-prime" }, required = true, help = """
			The horizontal dispersion prime
			""") Double dispPrimeH,
			@CommandParameter(keys = { "-vdp","--ver-disp-prime" }, required = true, help = """
			The vertical dispersion prime
			""") Double dispPrimeV,
			@CommandParameter(keys = { "-emh","--hor-emmittance" }, required = false, help = """
				The horizontal emmittance.  Not always used, for example when the Twiss
				parameter object is used for the create-beam command, the emitance
				parameter can be given and the emittance parameters in the Twiss 
				parameter will not be used.If not given, set to zero.
				""") Double emH,
			@CommandParameter(keys = { "-emv","--vert-emmittance" }, required = false, help = """
				The vertical emmittance.  Not always used, for example when the Twiss
				parameter object is used for the create-beam command, the emitance
				parameter can be given and the emittance parameters in the Twiss 
				parameter will not be used.If not given, set to zero.
				""") Double emV,
			@CommandParameter(keys = { "-muh","--hor-mu" }, required = false, help = """
				The horizontal phase advance.  If not given, the position is set to zero.
				""") Double muH,
			@CommandParameter(keys = { "-muv","--vert-mu" }, required = false, help = """
				The vertical phase advance.   If not given, the position is set to zero.
				""") Double muV,
			@CommandParameter(keys = { "-dpop","--momentum-spread" }, required = false, help = """
				The relative momentum spread of beam. It is dp/p0, where p0 is the nominal momentum.
				Depending on the usage of the Twiss paramers, this parameter may be important or not.
				If not given a zero value is assumed.
				""") Double dpop,
			@CommandParameter(keys = { "-lp","--longitudinal-position"  }, required = false, help = """
					The longitudinal position. If not given, the position is set to zero.
					""") Double lPos) throws OutOfApertureException, KnownFatalException {

		if(lPos==null)
			lPos=0.0;	
		if(dpop==null)
			dpop=0.0;	
		if(emH==null)
			emH=0.0;
		if(emV==null)
			emV=0.0;	
	    if(muH==null)
			 muH=0.0;	
		if(muV==null)
			 muV=0.0;	 

		PhaseSpaceEllipse he = new PhaseSpaceEllipse(emH, ha, hb, x, xp);
		PhaseSpaceEllipse ve = new PhaseSpaceEllipse(emV, va, vb, y, yp);
	
		return TwissParameters.builder().longiPos(lPos).hEllipse(he).vEllipse(ve).dispH(dispH)
		.dispV(dispV).dispPrimeH(dispPrimeH).dispPrimeV(dispPrimeV).muH(muH).muV(muV).dpOp(dpop).build();			
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-vector3d", description = """
			Create a Vector3D object. Usually the Vector3D objects are assigned to a variable
			and passed to other commands.
			""")
	public Vector3D createVector3D(@CommandParameter(keys = { "-x" }, required = true, help = """
			X value [m].
			""") double x,
			@CommandParameter(keys = { "-y" }, required = true, help = """
			Y value [m].
			""") double y,
			@CommandParameter(keys = { "-z" }, required = true, help = """
			Z value [m].
			""") double z) {
		return new Vector3D(x, y, z);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "end-sequence", description = """
			Finishes the sequence and saves the orbit to a file. Once the sequence is 
			finished no elements can be added to it. The sequence can be made active by the
			'use-sequence'	command.
		""")
	public void endSequence(
			@CommandParameter(keys = { "-o","--output" }, required = true, help = """
			File to write the orbit to.
			""") String outputFile,
			@CommandParameter(keys = { "-lff","--local-frame-file" }, required = false, help = """
			For non planar machines the local coordinates can be non-trivial. This option
			allows to write the local coordinates to a file in those cases and can be
			inspected or used for some other purpose. The local coordinates will be written 
			into a file if this option is given.
			""") String lfFile,
			@CommandParameter(keys = { "-inc","--local-frame-increment" }, required = false, help = """
			Increment option, which can be present only if the '--local-frame-file' option
			is given. The local frames will be written to the file every n-th orbit point,
			where n is the increment. For example: 1 takes all the orbit points, 2 takes 
			every second, and so on, default is 5.
			""") Integer increment,
			@CommandParameter(keys = { "-lcf","--local-centers-file" }, required = false, help = """
			If not null, the local origin of each element expressed in the global system
			will be written into the given file.
			""") String lcFileName) {

		if (Sequence.getActiveSequence() == null) {
			throw new IllegalArgumentException("No active sequence.");
		}
		Sequence.getActiveSequence().getReferenceOrbit().writeToFile(outputFile);
		Sequence.getActiveSequence().setFozen(true);

		if (lcFileName != null) {
			Sequence.getActiveSequence().getReferenceOrbit().writeMaptoFileWithS(lcFileName, Sequence.getActiveSequence().getLocalOrigins());
		}

		logger.info("Sequence length = " + Sequence.getActiveSequence().getReferenceOrbit().getOrbitLength());

		if (increment == null)
			increment = 5;
		if (lfFile != null)
			Sequence.getActiveSequence().getReferenceOrbit().localFramesToFile(lfFile, increment);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "java-call", description = """
			Executes a method from the given  class in this Java virtual machine. This is
			useful to call small java programs from the SIMPA to process some results or
			do some other tasks. If the class is not in the classpath ( external),the file 
			must be compiled with the same version of Java as SIMPA is running on or
			earlier. For external classes the class file must be in the working directory or
			in the directory specified by the --path option.
			""")
	public Object javaCall(@CommandParameter(keys = { "-cn","--external-class-name" }, required = false, help = """
		    External class name without .class extension.
			""") String externalClassName,
			@CommandParameter(keys = { "-icn","--internal-class-name" }, required = false, help = """
		    Internal class name without .class extension. Example:
			"simpa.acc.api.utils.BeamSizeCalculator"
			""") String internalClassName,
			@CommandParameter(keys = { "-p","--path" }, required = false, help = """
			A path to the class file. It should be specified if it is not in the working
			directory. Example ../helperClasses/    
			Should not be confused with the java classpath.
			""") String pathToClass,
			@CommandParameter(keys = { "-mn","--method-name" }, required = true, help = """
			The name of the method to be called in the Java class. The signature of the
			method called is very general:	Object someMethod(Object[] methodParameters)  
			It is the responsibility of the user to make sure the number of the parameters,
			their type and order matches the method signature. The method can be static or
			non-static. 
			""") String methodName,
			@CommandParameter(keys = { "-mp","--method-parameters" }, required = true, help = """
			An array of objects to be used as parameters for the method to be called. It is
			the user's responsibility to make sure the number of the objects, their type and
			order matches the method signature.
			""") Object[] methodParameters,
			@CommandParameter(keys = { "-cp","--constructor-parameters" }, required = false, help = """
			An array of objects to be used as parameters for the constructor of the Java
			object, if any. It is the user's responsibility to make sure the number of the
			objects, their type and order matches the constructor signature.
			""") Object[] constructorParameters)  {

		if (pathToClass == null)
			pathToClass = "";

		Class<?> clazz=null;
		
		if(externalClassName !=null){
			String classFile = pathToClass + externalClassName + ".class";
			// Create a file object for the class file
			File file = new File(classFile);
	  		try (URLClassLoader classLoader = new URLClassLoader(new URL[] { file.getParentFile().toURI().toURL() })){
				// Load the class using the class loader
			    clazz = classLoader.loadClass(externalClassName);
			} catch (ClassNotFoundException | IOException e) {
				logger.error(e.toString());
			}			
		}else if( internalClassName !=null){
			try {
				clazz = Class.forName(internalClassName);
			} catch (ClassNotFoundException e) {
				logger.error(e.toString());
			}
		}else{ 
			throw new IllegalArgumentException("Either external or internal class name should be given.");	
		}

		try{
			// Create an instance of the class
			Object obj;
			if (constructorParameters == null)
				obj = clazz.getDeclaredConstructor().newInstance();
			else{				
				obj = clazz.getDeclaredConstructor().newInstance(constructorParameters);
			}
			
			logger.info("Executing method '" + methodName + "' in java class file '" + clazz.getName() + "'.");					
			Object result = MethodInvoker.callMethod(obj ,methodName, methodParameters);

			return result;

		} catch ( NoSuchMethodException | SecurityException | InstantiationException
				| IllegalAccessException | IllegalArgumentException | InvocationTargetException  e) {
			logger.error("java-call failed: "+e.toString());
		}
	
		return null;
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "create-fieldmaps", description = """
			Executes the solid harmonic expansion using a cover file. The result is a
			field map. The field map can be used for tracking. The field map is written to
			binary file.
			""")
	public void createFieldMaps(
			@CommandParameter(keys = { "-cf", "--cover-file" }, required = true, help = """
			Cover file. The cover file contains the centers of the balls and the radius of
			the balls. The cover file can be created by the 'create-beam-region' or the
			'create-single-row-cover' command.
			""") String file,
			@CommandParameter(keys = { "-g","--group" }, required = false, help = """
			The name of the group to be expanded. If null, all groups will be expanded into
			a single field map. The group names are given at the construction of the
			sequence. Usually a group consists of the elements connected to the same power
			supply and scaled together.
			""") String groupName,
			@CommandParameter(keys = { "-obo","--one-by-one" }, required = false, help = """
			If true each group in the sequence will be expanded into separate field maps.
			In this case the group name should not be given.			
			""") Boolean oneByOne,
			@CommandParameter(keys = { "-l","--lmax" }, required = true, help = """
			L_max is the maximum degree of the solid harmonic expansion. The actual 
			expansion degree is	determined in a filtering phase to be below the  error
			limit. This makes the field map smaller and faster to evaluate. The expansion
			has two	passes. The first pass is done with a lower degree, which is given by
			the --lmax-pass1 parameter. Choosing that parameter properly saves time  by
			avoiding unneded high degree expansion of all balls. In the second
			pass the lmax given here is used  and a second filtering phase follows.
			""") int lmax,
			@CommandParameter(keys = { "-lp1","--lmax-pass1" }, required = false, help = """
			The expansion has two passes. The first pass is done with the degree given by
			this parameter. Choosing properly saves time at the creation of the field maps
			by avoiding unneded high degree expansion of all balls. Default is lmax/3.
			""") Integer pass1Lmax,
			@CommandParameter(keys = { "-re","--relative-error" }, required = false, help = """
			Relative error limit for the expansion. Small spherical harmonic coefficients
			are dropped below that. Default is 1E-14
			""") Double relativeError,
			@CommandParameter(keys = { "-st","--statistic" }, required = false, help = """
			Writes statistics about the distribution of maximum degrees of the balls in 
			the map.
			""") Boolean stat)	throws IOException {

		AccFieldFactory aff = new AccFieldFactory();
		
		SphereCovering cover;

		if (file.contains(FileNamingConventions.HCP_COVER_FILE_EXTENSION)){
			 cover = SphereCovering.createHCPLatticeCover(file);
		}else if(file.contains(FileNamingConventions.SINGLE_ROW_COVER_FILE_EXTENSION)){
			 cover = SphereCovering.createSingleRowCover(file);
		}else{	
			throw new IllegalArgumentException("Unrecognized cover file extension: " + file);
		}
        
		if(pass1Lmax==null)
			pass1Lmax=lmax/3;

		if (stat == null)
			stat = false;

		if (oneByOne == null)
			oneByOne = false;

		if (oneByOne) {
			if (groupName != null)
				throw new IllegalArgumentException("Group name should not be given with the --one-by-one  option");
			aff.shExpandAllGroups(Sequence.getActiveSequence(), lmax, cover, relativeError, stat,pass1Lmax);
		} else
			aff.shExpandGroup(Sequence.getActiveSequence(), groupName, lmax, cover, relativeError, stat,pass1Lmax);

	}

	

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "field-on-curve", description = """
			Writes the field of the elements in the given map or in the active sequence into
			a file. The values are taken along the reference orbit with an optional shift.
			If no map is given,it takes the map of the current sequence. It is usually used 
			for checking the correctness of the field maps.  In the output file the 
			components of the fields are expresse in the global coordinate system.			
			""")
	public void fieldOnCurve(@CommandParameter(keys = { "-m","--map" }, required = false, help = """
			A map object with names of the binary field map files as keys and scaling 
			factors as values. The format of the map is:
			[{"binary_filename1":scaling1, "binary_filename2":scaling2, ...}]
			Example:[{"BENDINGS_12.bin":-37.51} ,{"QFND_12.bin":3.243957}]	
			If no map is given, it takes the map of the active sequence.
			""") Map<String, Double> map,
			@CommandParameter(keys = { "-s","--shift" }, required = false, help = """
			Vector for a shift in the local frame from the reference orbit, if any. It
			can be created with the 'create-vector3d' command.
			""") Vector3D shift,
			@CommandParameter(keys = { "-o","--output" }, required = true, help = """
			File to write output to.
			""") String outputFile,
			@CommandParameter(keys = { "-lf","--local-frame" }, required = false, help = """
			If true, the field is written in the local frame of the element. If false, the
			field is written in the global frame. Default is false.
			""") Boolean localFrame	)
			throws OutOfApertureException, KnownFatalException, IOException {

		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("No active sequence !");

		if (shift == null)
			shift = Vector3D.ZERO;
        
		if(localFrame==null)	
			localFrame=false;

		List<Vector3D> path = Sequence.getActiveSequence().getReferenceOrbit().getOrbitPoints();

		if (map != null) {
			SHCombiner shc = SHCombiner.getUniqueInstance();
			PotentialProvider pp = shc.getEvaluatorWithoutCaching(map);
			DiagnosticUtils.getFieldsOnCurve(pp, path, shift, outputFile, 
					Sequence.getActiveSequence().getReferenceOrbit().getLocalFrames(),localFrame);
		} else {

			if (Sequence.getActiveSequence().getPotentialProvider() == null)
				throw new IllegalArgumentException("No scaling is set for the active sequence !");

			DiagnosticUtils.getFieldsOnCurve(Sequence.getActiveSequence().getPotentialProvider(), path, shift, outputFile,
					 Sequence.getActiveSequence().getReferenceOrbit().getLocalFrames(),localFrame);
		}

	}

	// TODO implement
			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "insert-chamber", description = """
			
			NOT YET IMPLEMENTED. !!! You can use the various "add-..." commands instead 
			with the vacuum chamber option at the creation of the sequence.

			Inserts a vacuum chamber into the sequence at the given position or the 
			location of	the given element.
		 	""")
	public void insertChamber(@CommandParameter(keys = { "-vc","--vacuum-chamber" }, required = true, help = """
			The vacuum chamber to be inserted.
			""") Map<Double, Profile> profileMap,
			@CommandParameter(keys = { "-br","--element-name" }, help = """
			The name of the element which the vacuum chamber is inserted at. Should not be
			given if the longitudinal position is given.
			""") BeamRegion beamRegion,
			@CommandParameter(keys = { "-lp","--longitudinal-position" }, help = """
			The longitudinal position [m] along the reference orbit where the vacuum chamber
			is inserted. Counted from the beginning of the sequence. If the element name is
			given, this parameter is ignored.
			""") Double lpos) {
		System.out.println("Not yet implemented.");
	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "create-vacuum-chamber", description = """
			Creates a vacuum chamber by adding profiles  at the specified distances from
			the local origin of the element, which is usually the center of the element,
			except for arbitrary elements. The vacuum chamber should be assigned to a 
			variable which can be used later for inserting it into the sequence.
			Profiles can be created with the 'create-rect-profile' or 
			'create-ellipse-profile' commands.
			""")
	public Map<Double, Profile>  createVacuumChamber(			
			@CommandParameter(keys = { "-d", "--distances" }, required = true, help = """
			A map with the profiles as keys and arrays of doubles with the distances
			from the local origin of the element as the values. The format of the map is:
			[{profile1:[dist1, dist2, ...]}, {profile2:[dist1, dist2, ...]} ...]
			""") HashMap<Object, Double[]> distances) {
		
		Map<Double, Profile> chamber = new HashMap<>();

		for (Object key : distances.keySet()) {
			Profile profile = (Profile) key;
			Double[] da = distances.get(profile);
			for (double distance : da) {
				chamber.put(distance, profile);
			}
		}
		return chamber;
	}
			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "line-optics", description = """
			Calculates linear optics of a transfer line by tracking two ellipse beams. The
			two beams must have some small momentum deviation to determine the dispersion.
			The trajectory of the particles in the beams pierces a series of observer disks
			along the reference orbit. There is a disk por each orbit point. The
			intersection points of the trajectory with the disks are used to fit 
			phase-space ellipses at the disk. The Twiss parameters are calculated from the
			fitted ellipses. The command returns the Optics object and writes it to a file.
			""")
	public Optics lineOptics(

			@CommandParameter(keys = { "-b0","--beam-nominal" }, required = true, help = """
			An ellipse beam with some momentum which is usually the nominal momentum.
			""") Beam b0,
			@CommandParameter(keys = { "-b1","--beam-dpop" }, required = true, help = """
			An ellipse beam with a dp/p momentum deviation.
			""") Beam b1,
			@CommandParameter(keys = { "-ss","--step-size" }, required = true, help = """
			The step size [m] for the tracking.
			""") Double stepsize,
			@CommandParameter(keys = { "-dr","--disk-radius" }, required = true, help = """
			Observer disk radius [m]. Should be a bit bigger than the maximum aperture of
			the machine. One must be careful to not intersect th beam line more than once
			with the observer disk.
			""") double diskRadius,
			@CommandParameter(keys = { "-f","--output-file" }, required = true, help = """
			Output file name for the optical functions.
			""") String outFile,
			@CommandParameter(keys = { "-bt","--backward-tracking" }, required = false, help = """
			If true tracking backward by inverting the momentum vector. False by default.
			To use this function the Twiss parameters at the end of the line must be known
			to calculate the optics of the full line. 
			""") Boolean backward,
			@CommandParameter(keys = { "-wt","--write-trajectory" }, required = false, help = """
			If true, writes two trajectory files to the disk corresponding to the two
			ellipse beams. Useful for debugging. False by default.
			""") Boolean writeTrajectory)
			throws IOException {

		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("There is no active sequence");
		if (Sequence.getActiveSequence().isCircular())
			throw new IllegalArgumentException("The active sequence is not a transfer line !");

		if (!(b0 instanceof EllipseBeam) || !(b1 instanceof EllipseBeam))
			throw new IllegalArgumentException("Ellipse beams are expected.");

		OpticsCalculator oc = new OpticsCalculator(Sequence.getActiveSequence().getReferenceOrbit());
		oc.setTurns(1);

		if (backward == null)
			backward = false;

		if (writeTrajectory == null)
			writeTrajectory = false;

		Optics op;
		try {
			op = oc.trackInLineAndCalculate(writeTrajectory, (EllipseBeam) b0, (EllipseBeam) b1,
					Sequence.getActiveSequence().getPotentialProvider(), stepsize, diskRadius, backward);
		} catch (IOException | OpticsCalculationException e) {
			logger.error(e.toString());
			return null;
		}
		op.writeToFile(outFile);
		Sequence.getActiveSequence().setOptics(op);
		return op;
	}

			////////////////////////////////////////////////////////////////////////////////    80 character				
	@Command(key = "match", description = """
			Matches a line or a ring to the given target parameters. An active sequence
			must be selected. After the matching the scaling map of the active sequence will
			be updated with the best values of the scaling factors of the field maps.
			It returns a copy of this map of field maps and scaling factor for later use,
			if needed. This command can be used for ring or transfer line matching. In the
			latter case two beams must be given as input parameters. In the former case two
			particles are needed.
			""")
	public Map<String, Double> match(@CommandParameter(keys = { "-b0","--beam0" }, required = false, help = """
			An ellipse beam with the nominal momentum. Used for tracking during transfer
			line optics calculation.""") Beam b0,
			@CommandParameter(keys = { "-b1","--beam1" }, required = false, help = """
			An ellipse beam with some dp/op momentum deviation. Used for determining the
			dispersion in transfer lines.
			""") Beam b1,
			@CommandParameter(keys = { "-p0","--particle0" }, required = false, help = """
			A particle with the nominal momentum. Used for tracking during ring optics
			calculation. 
			""") Particle p0,
			@CommandParameter(keys = { "-p1","--particle1" }, required = false, help = """
			A particle with some dp/op momentum deviation. Used for determining the
			dispersion in rings.
			""") Particle p1,
			@CommandParameter(keys = { "-dr","--disk-radius" }, required = true, help = """
			Radius [m] of the observer disk for the tracking.
			""") Double diskRadius,
			@CommandParameter(keys = { "-ss","--step-size" }, required = true, help = """
			Step size [m] for the tracking.
			""") Double stepSize,
			@CommandParameter(keys = { "-m","--matcher" }, required = true, help = """
			An matcher object to be used. See 'bobyqa-matcher', 'cmaes-matcher' commands.
			""") Matcher matcher,
			@CommandParameter(keys = { "-bt","--backward-tracking" }, required = false, help = """
			If true, tracking backward by inverting the momentum vectors in the beams. 
			False by default. Useful for transfer lines when the end Twiss parameters
			are known. 
			""") Boolean backward,
			@CommandParameter(keys = { "-of","--optics-file" }, required = false, help = """
			Writes the matched optics at the end of the matching into the given file.
			""") String file,
			@CommandParameter(keys = { "-lsp","--loss-penalty" }, required = false, help = """
			The penalty value  added to the objective function for each lost particle. 
			If not given the default value is 1E+6.
			""") Double lossPenalty,
			@CommandParameter(keys = { "-it","--initial-twiss" }, required = false, help = """
			Initial Twiss parameters for the matching the initial beam parameters for
			a transfer line. If given the beam0 should be a Gaussian beam and the 
			beam1 is ignored.
			""") TwissParameters initialTwiss)
			throws IllegalArgumentException {

		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("No active sequence !");

		if (backward == null)
			backward = false;

		if(lossPenalty!=null)			
			SimpaMultivariateFunction.setLosssPenaly(lossPenalty);

		Map<String, Double> matchedMap=activeSequence().getScalingMap();	

		if (Sequence.getActiveSequence().isCircular()) {
			if (p0 == null || p1 == null)
				throw new IllegalArgumentException("For matching in rings the p0 and p1 particles can not be null ! ");
			matchedMap = matcher.matchRing(Sequence.getActiveSequence(), p0, p1, stepSize, diskRadius);
		} else {	
			if (b1 == null){
				logger.info("Only one beam is given, assuming matching of beam sizes ");			
				if( initialTwiss == null)
					throw new IllegalArgumentException("When only one beam is given the  initialTwiss parameter can not be null ! ");

			 	matcher.matchLine(Sequence.getActiveSequence(), initialTwiss, b0, stepSize, diskRadius, backward);
			}

			if(b0 instanceof EllipseBeam && b1 instanceof EllipseBeam) {
				matchedMap = matcher.matchLine(Sequence.getActiveSequence(), (EllipseBeam) b0, (EllipseBeam) b1, stepSize, diskRadius, backward);
			}
		}

		if (file != null)
			matcher.writeOptics(file);
		
	
		return matchedMap;
	}

			////////////////////////////////////////////////////////////////////////////////    80 character		
	@Command(key = "matching-target", description = """
			Creates a target value for optics parameter matching. The target value is a
			scalar value of an optics parameter. The optics parameter is specified by its
			type and longitudinal position. The target value is the value of the optics
			parameter at the given longitudinal position. The target value can be weighted
			by a scalar value. The default weight is 1.0. The target should be assigned to
			a variable and passed to the matcher object. Instead of exact values minimum
			or maximum values can be also given. Those have no effect unless the given 
			limit is reached.
			""")
	public MatchingTarget matchingTarget(@CommandParameter(keys = { "-lp","--longitudinal-position" }, required = false, help = """
			Longitudinal position [m] of the optics parameter at which the matching 
			performed.
			""") Double longiPos,
			@CommandParameter(keys = { "-tt","--target-type" }, required = true, help = """
			A string specifying the type of the matching target parameter. For example:
			"H_BETA", "V_DISPERSION", "H_PHASE_ADVANCE", etc.
		 	The complete list of the matching target types names can be found here:
			https://simpa-project.docs.cern.ch/simpa.acc/simpa/acc/api/match/MatchingTargetType.html
			""") String type,
			@CommandParameter(keys = { "-tv","--target-value" }, required = false, help = """
			The target value of the optics parameter.
			""") Double value,
			@CommandParameter(keys = { "-ul","--upper-limit" }, required = false, help = """
			Upper limit of the optics parameter at the given longitudinal position.
			""") Double upperLimit,
			@CommandParameter(keys = { "-ll","--lower-limit" }, required = false, help = """
		    Lower limit of the optics parameter at the given longitudinal position.
			""") Double lowerLimit,
			@CommandParameter(keys = { "-w","--weight" }, required = false, help = """
			The weight of the matching target parameter. If not given the default value of
			the parameter type is taken, which is 1.0.
			""") Double weight)
			throws IllegalArgumentException {
		
		MatchingTargetType tt = MatchingTargetType.valueOf(type);
		
		if (tt == null)
			throw new IllegalArgumentException("Unknown matching target type: " + type);
		
		if(longiPos ==null) {
			if( tt.isGlobalParameter()) 
				longiPos=0.0;	
			else 
				throw new IllegalArgumentException("For the local matching target type: " + type+" the longitudinal position must be given !");		 
		}
		
		double w = tt.getDefaultWeight();
		if (weight != null)
			w = weight;
		
		if(value==null && upperLimit==null && lowerLimit==null) throw new IllegalArgumentException(" The targetvalue or the upper or the lower limit must be given.");
		double v=0.0;
		MatchingValueType mvt=MatchingValueType.EXACT_VALUE;
		
		if( value !=null) {
			v=value;			
		} else if(upperLimit !=null) {
			v=upperLimit;
			mvt=MatchingValueType.MAXIMUM;		
		}else if(lowerLimit !=null) {
			v=lowerLimit;
			mvt=MatchingValueType.MINIMUM;		
		}
		
		MatchingTarget mt = new MatchingTarget(v, tt, longiPos, w,mvt);
		return mt;
	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "matching-variable", description = """
			Creates a variable for optics parameter matching. It is a scaling factor
			belonging to a field map, which will be varied during the matching.
			""")
	public MatchingVariable matchingVariable(@CommandParameter(keys = { "-fm","--field-map" }, required = false, help = """
			The full file name of the field map with extension. It must be given when
			the matching valiable is a field map scaling factor.
			""") String fieldMapFile,
			@CommandParameter(keys = { "-t","--type" }, required = false, help = """
			A String describing the type of the matching variable. For example:
			"SCALING_FACTOR","INITIAL_H_EMITTANCE", "INITIAL_H_BETA", etc.
			See the API documentation for MatchingVariableType. The type can be either a 
			field map scaling factor or an  initial Twiss parameter. The default is
			"SCALING_FACTOR"
			""") String typeString,
			@CommandParameter(keys = { "-min","--min-value" }, required = false, help = """
			The minimum value of the scaling factor. This value is in most cases is the
			limit of the power supply connected to the accelerator element.
			""") Double min,
			@CommandParameter(keys = { "-max","--max-value" }, required = false, help = """
			The maximum value of the scaling factor. This value is in most cases is the
			limit of the power supply connected to the accelerator element.
			""") Double max,
			@CommandParameter(keys = { "-g","--guess" }, required = false, help = """
			Initial guess for the scaling factor of the field map. This can be important
			when  the BOBYQA matcher is used. The initial guess should be close to the
			global optimal value, because this matcher can be stuck in a local optimum.
			If not given, the value is taken from the scaling map of the active sequence.
			In case when the type is not a field map scaling factor, a guess is needed.			
			""") Double guess)
			throws IllegalArgumentException {
		
		if (typeString == null)
		typeString = "SCALING_FACTOR";
		MatchingVariableType type=	MatchingVariableType.valueOf(typeString);
		if (type == null)
			throw new IllegalArgumentException("Unknown matching variable type: " + typeString);

		MatchingVariable v = new MatchingVariable( type,fieldMapFile);	
		
		if(type.equals(MatchingVariableType.SCALING_FACTOR)){
				if (Sequence.getActiveSequence() != null && Sequence.getActiveSequence().getScalingMap() != null
					&& Sequence.getActiveSequence().getScalingMap().containsKey(fieldMapFile))
					 v.setInitialGuess(Sequence.getActiveSequence().getScalingMap().get(fieldMapFile));
		} else {
			if (guess == null)
				throw new IllegalArgumentException("A guess must be given for the initial Twiss parameter.");
			else 
				v.setInitialGuess(guess);	
		}

		if (min != null)
			v.setMin(min);
		if (max != null)
			v.setMax(max);

		return v;
	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "mirror-path", description = """
			Mirrors path points in the given file. The path assumed to start from the
			origin (0,0,0).	It finds automatically the plane for mirroring XY, XZ or YZ.
			This is a utility command for creating a full path from a half path to have an
			exactly symmetric path.
			""")
	public void mirrorPath(
			@CommandParameter(keys = {"-i" }, required = true, help = """
			The input file containing a half-path. This is just a list of X, Y, Z 
			coordinates in each line. The path should start from the origin (0,0,0).
			""") String inFile,
			@CommandParameter(keys = {"-o" }, required = true, help = """
			The output file containing a full path.
			""") String outFile,
			@CommandParameter(keys = {"-u" }, help = """
			Length unit of the input file. If given, the output will be scaled accordingly
			to meters. This is needed  for example when when the half path is obtained from
			COMSOL or OPERA and the units are not meters as in SIMPA. Path in SIMPA is
			given in meters.
			""") String unitString)
			throws FileNotFoundException {

		LengthUnit unit = LengthUnit.parse(unitString);
		assert unit != null;
		List<Vector3D> vl = FileUtils.readVectors(inFile, unit);
		Vector3D direction = vl.get(1).subtract(vl.get(0)).normalize();
		List<Vector3D> full;

		if (Math.abs(direction.dotProduct(Vector3D.PLUS_I)) > 0.5) {
			full = PathUtils.extendWithMirrorOnXAxis(vl);
		} else if (Math.abs(direction.dotProduct(Vector3D.PLUS_J)) > 0.5) {
			full = PathUtils.extendWithMirrorOnYAxis(vl);
		} else {
			full = PathUtils.extendWithMirrorOnZAxis(vl);
		}

		FileUtils.writeVectors(outFile, full);
	}
			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "orbit-at-bpms", description = """
			Saves the orbit at the beam position monitors in the sequence. The optics must 
			be calculated before using this command. This is useful to feed the orbit into
			orbit corrector software like YASP at CERN.""")
	public void orbitAtBPMs(@CommandParameter(keys = { "-hpf","--h-position-file" }, help = """
			Output file in which the relative (to the reference orbit ) positions at each 
			horizontal BPM will be written to.
			""") String hOutFile,
			@CommandParameter(keys = { "-vpf","--v-position-file" }, help = """
			Output file in which the relative (to the reference orbit ) positions at each
			vertical BPM will be written to.
			""") String vOutFile)
			throws IOException {

		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("No sequence defined.");

		if (Sequence.getActiveSequence().getOptics() == null)
			throw new IllegalArgumentException("No optics for the sequence. Calculate it first !");

		OrbitAtBPMs oc = new OrbitAtBPMs(Sequence.getActiveSequence());

		if (hOutFile != null) {
			oc.writeOrbitAtBPMPositons(hOutFile, PhaseSpacePlane.HORIZONTAL);
		}
		if (vOutFile != null) {
			oc.writeOrbitAtBPMPositons(vOutFile, PhaseSpacePlane.VERTICAL);
		}

	}



	// TODO add start and end idexes, also stepsize
			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "resample-path", description = """
			Re-samples the given path with the given number of steps or a list of distances
			given in meters from the first path point. This is a utility command which can
			be used for example to pick specific longitudinal positions from a path to 
			provide a path suitable for tiling a boundary surface.
			""")
	public void resamplePath(@CommandParameter(keys = { "-i","--input-file" }, required = true, help = """
			The input file containing a original path.
			""") String inFile,
			@CommandParameter(keys = { "-o","--output-file" }, required = true, help = """
			The output file containing the re-sampled path.
			""") String outFile,
			@CommandParameter(keys = { "-s","--steps" }, required = false, help = """
			The number of steps to have after the re-sampling.
			""") Integer steps,
			@CommandParameter(keys = { "-d","--distances" }, required = false, help = """
			A list of distances from the first path point. The distance should be given in
			the same units as the input file.
			""") Double[] distances,
			@CommandParameter(keys = { "-fx","--force-x" }, required = false, help = """
			Forces the X coordinate to the given value. Useful to remove small errors.
			""") Double x,
			@CommandParameter(keys = { "-fx","--force-y" }, required = false, help = """
			Forces the Y coordinate to the given value. Useful to remove small errors.
			""") Double y,
			@CommandParameter(keys = { "-fx","--force-z" }, required = false, help = """
			Forces the Z coordinate to the given value. Useful to remove small errors.
			"""	) Double z

	) throws FileNotFoundException {
		if ((distances != null && steps != null) || (distances == null && steps == null))
			throw new IllegalArgumentException(
					"The number of steps or a list of distances must be given, but not both.");

		List<Vector3D> l = FileUtils.readVectors(inFile, LengthUnit.M);
		List<Vector3D> path = new ArrayList<>();
		if (x != null) {
			for (Vector3D v : l)
				path.add(new Vector3D(x, v.getY(), v.getZ()));
		}
		if (y != null) {
			for (Vector3D v : l)
				path.add(new Vector3D(v.getX(), y, v.getZ()));
		}
		if (z != null) {
			for (Vector3D v : l)
				path.add(new Vector3D(v.getX(), v.getY(), z));
		}
		if (x == null && y == null && z == null)
			path = l;

		List<Double> distList;

		if (steps != null) {
			distList = new ArrayList<>();
			double length = PathUtils.getDistance(0, path.size() - 1, path);
			double ds = length / (steps - 1);
			double d = 0;
			for (int i = 0; i < steps; i++) {
				distList.add(d);
				d += ds;
			}

		} else {
			distList = Arrays.asList(distances);
		}

		FileUtils.writeVectors(outFile, PathUtils.reSamplePath(path, distList));

	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "ring-optics", description = """
			Calculates linear optics of a ring by tracking two particles. The two particles
			must have some small momentum deviation to determine the dispersion.
			The trajectory of the particles pierces a series of observer disks along the
			reference orbit. There is a disk for each orbit point. The intersection points
			of the trajectory with the disks are used to fit phase-space ellipses at each
			disk. The Twiss parameters are calculated from the fitted ellipses.""")
	public void ringOptics(

			@CommandParameter(keys = { "-p1","--particle1" }, required = true, help = """
			A particle with some momentum usually the nominal momentum.
			"""	) Particle p1,
			@CommandParameter(keys = { "-p2","--particle2" }, required = true, help = """
			A particle with some momentum deviation compared to particle 1. The momentum
			deviation is used for dispersion calculation.			
			""") Particle p2,
			@CommandParameter(keys = { "-ss","--step-size" }, required = true, help = """
			The step size [m] for the tracking.
			""") Double stepsize,
			@CommandParameter(keys = { "-dr","--disk-radius" }, required = true, help = """
			Observer disk radius [m]. Should be a bit bigger than the maximum aperture of
			the machine. One must be careful to not intersect the ring more than once
			with the observer disk.
			""") double diskRadius,
			@CommandParameter(keys = { "-f","--output-file" }, required = true, help = """
			Output file name for the optical functions.
			""") String outFile,
			@CommandParameter(keys = { "-t","--turns" }, required = false, help = """
			Number of turns for the tracking, default is 64. It must be a power of two 
			for the NAFF algorithm to calculate the tunes.		
			""") Integer turns,
			@CommandParameter(keys = { "-wt","--write-trajectory" }, required = false, help = """
			If true, writes a trajectory file to the disk during the tracking. Useful for
			debugging. False by default
			""") Boolean writeTrajectory)
			throws IOException {

		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("There is no active sequence");
		if (!Sequence.getActiveSequence().isCircular())
			throw new IllegalArgumentException("The active sequence is not a ring !");

		OpticsCalculator oc = new OpticsCalculator(Sequence.getActiveSequence().getReferenceOrbit());
		if (turns % 2 != 0)
			throw new IllegalArgumentException("Number of turns must be a power of two for this command!");
		if (turns != null)
			oc.setTurns(turns);

		if (writeTrajectory == null)
			writeTrajectory = false;

		Optics op;
		try {
			op = oc.trackInRingAndCalculate(p1, p2, Sequence.getActiveSequence().getPotentialProvider(), stepsize, diskRadius,
					writeTrajectory);
		} catch (IOException | OpticsCalculationException e) {
			logger.error(e);
			return;
		}
		op.writeToFile(outFile);
		Sequence.getActiveSequence().setOptics(op);

	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "sequence-sources-to-file", description = """
			Writes all sources in the sequence to a file. This is useful for checking if 
			any source is inside the aperture of the sequence, or even more importantly
			inside any ball of the covering, which should not happen.
			""")
	public void sequenceSourcesToFile(@CommandParameter(keys = { "-f","-file" }, required = true, help = """
			The file where the source coordinates will be written.
			""") String f) {
		Sequence.getActiveSequence().sourcePositionsToFile(f);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "set-integrator", description = """
			Sets symplectic integrator. to be used for the tracking. At the moment there
			is only one which is Tao's symplectic integrator. See:
			https://arxiv.org/abs/1609.02212
			""")
	public void setIntegrator(@CommandParameter(keys = { "-om","--taos-omega" }, required = true, help = """
			The omega for Tao's symplectic integrator. A value betewen 0..2PI, need to 
			experiment with it a bit to get the best value.
			""") Double omega) {
		TaosSymplecticIntegrator.setOmega(omega);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "set-scalings", description = """
			Sets the scalings of accelerator element groups.There must be an active 
			sequence selected before using this command.
			""")
	public void setScalings(@CommandParameter(keys = { "-m","--map" }, required = true, help = """
			A map object with the with names of the binary field map files as keys and
			scaling factors as values.
			""") Map<String, Double> map,
			@CommandParameter(keys = { "-ee","--enable-external" }, required = false, help = """
			If true, the scaling map can contain field maps not in the sequence,
			no checking	will be made. Foreseen for external disturbances. False by default.
				""") Boolean enableExternal)
			throws Exception {
		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("No active sequence !");
		if (enableExternal == null)
			enableExternal = false;

		Sequence.getActiveSequence().setScalingMap(map, enableExternal);
	
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
   @Command(key = "survey", description = """
			Writes a file with elements centers in the CERN coordinate system. The active
			sequence  will be used. It is intended for sequence checking against MAD-X 
			survey command output. The command parameters have the same name and meaning as
			the MAD-X command. See MAD-X manual p. 13.
			""")
	public void survey(@CommandParameter(keys = { "-x","--x0" }, required = true, help = """
			Initial X0 coordinate in the CERN coordinate system.
			""") Double x0,
			@CommandParameter(keys = { "-y","--y0" }, required = true, help = """
			Initial Y0 coordinate in the CERN coordinate system.
			""") Double y0,
			@CommandParameter(keys = { "-z","--z0" }, required = true, help = """
			Initial Z0 coordinate in the CERN coordinate system.
			""") Double z0,
			@CommandParameter(keys = { "-th","--theta0" }, required = true, help = """
			Initial theta angle in the CERN coordinate system.
			""") Double theta0,
			@CommandParameter(keys = { "-phi","--phi0" }, required = true, help = """
			Initial phi angle in the CERN coordinate system.
			""") Double phi0,
			@CommandParameter(keys = { "-psi","--psi0" }, required = true, help = """
			Initial psi angle the CERN coordinate system.
			""") Double psi0,
			@CommandParameter(keys = { "-f","--file" }, required = true, help = """
			File to be written.
			""") String file,
			@CommandParameter(keys = { "-en","--element-name" }, required = false, help = """
			The name of the element at which the coordinates and angles are given. If not
			specified, by default the beginning of the sequence is taken.
			""") String elementName) {
	 
	   if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("No active sequence !");
	   
	   Survey s= new Survey(Sequence.getActiveSequence(),x0,y0,z0,theta0,phi0,psi0,elementName);
	   s.centersToFile(file);	   				
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "system-call", description = """
			Call the given Linux command and returns the exit code.
			""")
	public int systemCall(@CommandParameter(keys = { "-c","--command" }, required = true, help = """
			A command string between double quotes to be executed. For example:
			system-call -c "ln -s ../elements/quad-SOLUTION.txt quad-SOLUTION.txt"
			""") String command )
		throws Exception {
		
		int exitCode=0;
		 ProcessBuilder processBuilder = new ProcessBuilder();
		    processBuilder.command("bash", "-c", command);

		    try {
		        Process process = processBuilder.start();

		        // Display the output
		        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		        String line;
		        while ((line = reader.readLine()) != null) {
		            System.out.println(line);
		        }

		        // Check for errors
		        BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		        while ((line = errorReader.readLine()) != null) {
		            System.err.println(line);
		        }

		         exitCode = process.waitFor();
		        System.out.println("The external command '"+command+"' finished with exit code: " + exitCode);

		    } catch (IOException e) {
		        e.printStackTrace();
		    } catch (InterruptedException e) {
		        e.printStackTrace();
		    }
		    
		    return exitCode;
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "track", description = """
			Tracks a particle or a beam. Either a particle or a beam must be given, but not
			both.
			""")
	public void track(
			@CommandParameter(keys = { "-p","--particle" }, required = false, help = """
			A particle to be tracked. Either a particle or a beam must be given, but not
			both.
			"""
			 ) Particle particle,
			@CommandParameter(keys = { "-b", "--beam" }, required = false, help = """
			A beam to be tracked. Either a particle or a beam must be given, but not
			both.
			""") Beam beam,
			@CommandParameter(keys = { "-tu","--turns" }, required = true, help = """
			Number of turns to be tracked in case of a ring. For transfer lines this
			parameter must be 1.
			""") Integer turns,
			@CommandParameter(keys = { "-ss","--step-size" }, required = true, help = """
			The step size for the tracking [m]
			""") Double stepSize,
			@CommandParameter(keys = { "-phop",	"--observer-position" }, required = false, help = """
			List of longitudinal positions [m] of the phase space observers along the
			reference orbit. If not given,	by default one is added at position zero
			in rings. For transfer lines there is also a second one at the end by
			default. Giving an empty list the default observers will be removed. 
			""") Double[] phoLongiPosArray,
			@CommandParameter(keys = { "-dr","--disk-radius" }, required = true, help = """
			Radius [m] of the observer disks. Should be a bit bigger than the aperture of
		    the ring or beam line. It should not intersect is twice however.
			""") Double diskRadius,
			@CommandParameter(keys = { "-to","--trajectory-observer" }, required = false, help = """
			If set true, a trajectory observer will be added. Be careful, the trajectory
			file can be huge when a bunch with a large number of particles is tracked.
			For this reason when a bunch is tracked in a ring no trajectory observer is
			allowed. For single particle it can be used both in rings and transfer lines.
			False by default. 
			""") Boolean trajectoryObserver,
			@CommandParameter(keys = { "-sf","--sigma-file" }, required = false, help = """
			If given, the output of the trajectory observer will be processed to calculate
			the beam size at each observer disk. The result will be written to the given
			file. This option is only valid if a trajectory obsever is present.
			""") String sigmaFile,
			@CommandParameter(keys = { "-df","--debug-field" }, required = false, help = """
			If set true, the output of a trajectory observer will contain also the value
			of the fields at the position of the particle.	Single particle tracking only,
			for beams it has no effect.""") Boolean debug,
			@CommandParameter(keys = { "-th","--threads" }, required = false, help = """
			The number of threads to be used for the beam tracking. All available threads
			minus 2 used by default.
			""") Integer threads,
			@CommandParameter(keys = { "-bt","--backward-tracking" }, required = false, help = """
			If true tracking backward by inverting the momentum vector. The output files 
			of the phase space observers will include the string '-BACKWARD-'
			See {@link FileNamingConventions}. False by	default.
			""") Boolean backward)	throws KnownFatalException, IOException {

		double gevOc;

		// convert the longitudinal positin array to a list
		List<Double> phoLongiPos = null;
		if (phoLongiPosArray != null) {
			phoLongiPos = new ArrayList<Double>();
			for (int i = 0; i < phoLongiPosArray.length; i++) {
				phoLongiPos.add(phoLongiPosArray[i]);
			}
		}

		if (backward == null)
			backward = false;

		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("No active sequence is given.");

		if (particle == null && beam == null)
			throw new IllegalArgumentException("No particle or beam is given.");

		if (debug == null)
			debug = false;
		if (trajectoryObserver == null && debug == true)
			throw new IllegalArgumentException("Debug flag set to true, but no trajectory observer is given.");

		if (particle != null) {
			gevOc = CalculatorUtils.getMomentumInGeVoC(particle.getKineticMomentum().getNorm());
		} else { // beam is set here, we checked it before
			gevOc = beam.getNominalMomentum();
		}

		if (phoLongiPos == null) {
			phoLongiPos = new ArrayList<Double>();
			if (Sequence.getActiveSequence().isCircular()) {
				phoLongiPos.add(0.0);
			} else {
				phoLongiPos.add(0.0);
				phoLongiPos.add(Sequence.getActiveSequence().getReferenceOrbit().getOrbitLength());
			}
		}

		if (trajectoryObserver == null)
			trajectoryObserver = false;		

		if (!Sequence.getActiveSequence().isCircular() && turns != 1)
			throw new IllegalArgumentException("The active sequence is a line, turns must be equal to 1.");

		TurnObserver to = new TurnObserver(Sequence.getActiveSequence().getReferenceOrbit(), diskRadius, backward);
		to.setMaximumTurns(turns);

		
		if (particle != null) {
			ParticleTrackerTask ptt = new ParticleTrackerTask(Sequence.getActiveSequence().getPotentialProvider(), particle,
					Long.MAX_VALUE, stepSize, backward);
			ptt.addObserver(to);

			for (Double lp : phoLongiPos) {
				PhaseSpaceObserver pho = new PhaseSpaceObserver(lp, diskRadius, gevOc,
						Sequence.getActiveSequence().getReferenceOrbit(), backward);
				ptt.addObserver(pho);
			}

			if (trajectoryObserver == true) {
				TrajectoryObserver tro = new TrajectoryObserver(ObserverBehaviour.WRITE_ONLY, debug);
				ptt.addObserver(tro);
			}
			ptt.run();
		} else { // beam is set here, we checked it before
			if (threads == null){
				threads = Runtime.getRuntime().availableProcessors() - 2;
				if(threads <1) 
					threads=1;
			}

			if (Sequence.getActiveSequence().isCircular()) {
				if (trajectoryObserver == true)
					throw new IllegalArgumentException("Trajectory observer is not supported for ring beam tracking.");
				RingBeamTracker rbt = new RingBeamTracker(beam, turns, diskRadius, stepSize, threads, backward,
						phoLongiPos);
				rbt.track();
			} else {
				TLBeamTracker tlbt = new TLBeamTracker(beam, diskRadius, stepSize, threads, phoLongiPos, backward);
				
				BeamTrajectoryObserver btro=null;
				if (trajectoryObserver == true) {
					btro = new BeamTrajectoryObserver(ObserverBehaviour.STORE_AND_WRITE);
					tlbt.addTrajectoryObserver(btro);				
				}

				tlbt.track();
				
				if (trajectoryObserver == true) {
					if(sigmaFile != null ){					
						Map<String, List<TrajectoryData>>   btrd=	btro.getTrajectoryData();
						BeamSizeCalculator bsc= new BeamSizeCalculator(btrd, Sequence.getActiveSequence().getReferenceOrbit(),diskRadius);	 
						bsc.writeSigmas(sigmaFile);
					}
				}
			}
		}


		

	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "transform-acc-element", description = """
			Transform an accelerator element with the given rotation and translation. This
			is useful when elements which are different only in their orientation are added
			to the sequence. Technically the solution file containing the sources will be
			transformed. 
	""")
	public void transformAccElement(

			@CommandParameter(keys = { "-n","--name" }, required = true, help = """
			The name of the element.
			""") String name,
			@CommandParameter(keys = { "-r","--rotation" }, help = """
			The rotation to be applied, before the translation if any. See the
			'create-rotation' command. 
			""") Rotation rot,
			@CommandParameter(keys = { "-t","--translation" }, help = """
			A translation vector to be applied after the rotation if any. See the
			'create-vector3d' command.
			""") Vector3D translation,
			@CommandParameter(keys = { "-ir","--inverse-rotation" }, required = false, help = """
			If true, the inverse of the rotation will be applied. False by default.		
			""") Boolean inverseRot,
			@CommandParameter(keys = { "-it","--inverse-translation" }, required = false, help = """
			If true, the inverse of the translation will be applied. False by default
			""") Boolean inverseTransl,
			@CommandParameter(keys = { "-on","--output-name" }, required = true, help = """
			Name for the transformed element. A transformed solution file  will be written.
			Example: outFile-SOLUTION.txt
			""") String outName)
			throws IOException {

		if (inverseRot == null)
			inverseRot = false;

		if (inverseTransl == null)
			inverseTransl = false;

		SolutionTransformer.transform(name, rot, inverseRot, translation, inverseTransl, outName);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "twiss", description = """
			Calculates and prints the Twiss parameters from a phase space coordinate file 
			produced by tracking a single particle in a ring. It returns an object
			containing the Twiss parameters which can be assigned to a variable and used
			later as input for other commands, for example the 'create-beam' command.
			The Twiss parameters are calculated by fitting an ellipse to the phase space
			coordinates.
			""")
	public TwissParameters twiss(
			@CommandParameter(keys = { "-f", "--file" }, required = true, help = """
			Phase space file which is used as input for the calculation.
			""") String file)
			throws IOException {
		logger.info("Twiss parameters calculated by fitting ellipses to single particle phase space data:");
		return PhaseSpaceEllipse.printTwiss(file);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character	
	@Command(key = "update-scalings", description = """
			Update the scalings of the active sequence. Scalings in the given update map
			which are not in the sequence map already will be ignored. Scalings in the 
			original map of the sequence which are not in the update map are not modified.
			""")
	public void updateScalings(@CommandParameter(keys = { "-m",	"--map" }, required = true, help = """
			A map object with the names of the binary field-map files as keys and scaling
			factors as values.
			""") Map<String, Double> map)
			throws Exception {
		if (Sequence.getActiveSequence() == null)
			throw new IllegalArgumentException("No active sequence !");

		Sequence.getActiveSequence().updateScalingMap(map);
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "use-sequence", description = """
			Sets  the sequence to be used. Many commands require the active sequence to be
			defined.
			""")
	public void useSequence(
			@CommandParameter(keys = { "-n", "--name" }, help = """
			The name of the sequence. Beforehand, the sequence with this name must be
			created by the 'create-sequence' command.
			""") String name) {
		if (Sequence.getSequences().containsKey(name)) {
			Sequence.setActiveSequence(Sequence.getSequences().get(name));

		} else {
			throw new IllegalArgumentException("The sequence with the name " + name + " does not exist.");
		}
	}

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "element-from", description = """
			Imports an accelerator element from another sequence. The element is copied to 
			the	active sequence. The element must be present in the sequence from which it
			is imported. The element longitudinal position in the active sequence will be
			undefined. The imported element physical position and orientation will be
			unchanged.
			""")
			public void elementFrom(
					@CommandParameter(keys = { "-sn", "--sequence-name" }, required=true, help = """
					The name of the sequence to import from. Beforehand, the sequence with
					this name must be created by the 'create-sequence' command.
					""") String seqName,
					@CommandParameter(keys = { "-en", "--element-name" }, required=true, help = """
					The name of the element to import. The element must be present in the
					sequence from which it is imported.
					""") String name) {	
				if (Sequence.getSequences().containsKey(seqName)) {
					Sequence seq=Sequence.getSequences().get(seqName);
					Sequence.elementFrom(seq, name);
				} else {
					throw new IllegalArgumentException("The sequence with the name " + name + " does not exist.");
				}
			}
		

			////////////////////////////////////////////////////////////////////////////////    80 character
	@Command(key = "write-surface-points", description = """
			Writes a file with the coordinates of the surface points on the boundary of the
			accelerator element according to the given configuration. This file is usually
			used as input for electro-magnetic software the get out the field values at
			the points in this file.
			""")
	public void writeSurfacePoints(@CommandParameter(keys = { "-tc","--tiling-config" }, required = true, help = """
			Tiling configuration created with the 'create-tiling-config' command. The tiling
			configuration contains the element name which is used for naming the output file
			according to the file naming conventions, see: 
			https://simpa-project.docs.cern.ch/constant-values.html#simpa.core.api.FileNamingConventions.POINTS_FILE_EXTENSION			
			""") TilingConfig tconfig)
			throws NotSetException {

		Tiling tiling = new Tiling(tconfig);
		tiling.writeSurfacePoints();
	}

	///////////////////////// PRIVATE METHODS
	///////////////////////// /////////////////////////////////////////////

	private FieldType parseFieldType(String fieldType) {
		// TODO: rf case should be added
		if (fieldType.equals("se"))
			return FieldType.STATIC_ELECTRIC;
		else if (fieldType.equals("sm"))
			return FieldType.STATIC_MAGNETIC;
		else
			throw new IllegalArgumentException("Unknown field type: " + fieldType);

	}

	private OutputFileFormat parseOutputFileFormat(String outputFileFormat) {
		OutputFileFormat of;
		if (outputFileFormat == null || outputFileFormat.equals("opera3d") || outputFileFormat.isEmpty()) {
			of = OutputFileFormat.OPERA3D;
		} else {
			of = OutputFileFormat.PLAINTEXT;
		}
		return of;
	}
}
