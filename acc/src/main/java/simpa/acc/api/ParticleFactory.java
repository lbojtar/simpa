/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 * <p>
 * Author : Lajos Bojtar
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.acc.api;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.Particle;
import simpa.core.api.track.ParticleType;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.CalculatorUtils;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.LocalFrame;
import simpa.core.api.utils.PathUtils;

/**
 * Factory for creating particles from Phase space coordinate data.
 */
public class ParticleFactory {

	private static final Logger logger = LogManager.getLogger(ParticleFactory.class);
	private final ReferenceOrbit corbit;

	/**
	 * Sets up the factory with a reference orbit.
	 *
	 * @param corbit the reference orbit.
	 */
	public ParticleFactory(ReferenceOrbit corbit) {
		this.corbit = corbit;
	}

	/**
	 * Gets a particle with the given data. We generate particles in a flat 5D beam,
	 * all particles are on the same longitudinal position.
	 * 
	 * @param p0InGeV  Momentum in GeV/c
	 * @param phs      Phase space coordinates.
	 * @param longiPos The longitudinal position in meters along the orbit counted
	 *                 from the beginning of the orbit.
	 * @param afp      A potential provider to get the vector potential at the
	 *                 position of the particle
	 * @param mass     Mass [kg]
	 * @param charge   Charge [Coulumb]
	 * 
	 * @return The particle created.
	 * 
	 * @throws OutOfApertureException when the particle is out of aperture
	 * @throws KnownFatalException    when an internal error happens.
	 */
	public Particle getParticle(double p0InGeV, PhaseSpaceCoordinates phs, double longiPos, PotentialProvider afp,
			double mass, double charge) throws OutOfApertureException, KnownFatalException {

		int index = PathUtils.getClosestIndex(corbit.getOrbitPoints(), longiPos, corbit.isCircular());
		LocalFrame lf = corbit.getLocalFrames().get(index);

		// We generate a flat 5D beam, all particles are on the same plane.
		Vector3D pos = corbit.getOrbitPoints().get(index);

		Vector3D transversePos = new Vector3D(phs.x(), phs.y(), 0);

		// create a rotation taking the delta x and delta y perpendicular to the orbit.
		Rotation rot = new Rotation(Vector3D.PLUS_I, Vector3D.PLUS_K, lf.lx(), lf.lz());
		transversePos = rot.applyTo(transversePos);
		pos = pos.add(transversePos);

		Vector3D transverseAngle = new Vector3D(phs.xp(), phs.yp(), 0);
		transverseAngle = rot.applyTo(transverseAngle);
		transverseAngle = transverseAngle.add(lf.lz()); // ok

		Vector3D vp = CalculatorUtils.getAVector(afp, pos);

		return new Particle(pos, transverseAngle, vp, p0InGeV * (1 + phs.zp()), charge, mass);

	}

	/**
	 * Gets a particle with the given data. We generate particles in a flat 5D beam,
	 * all particles are on the same longitudinal position.
	 * 
	 * @param p0InGeV  Momentum in GeV/c
	 * @param phs      Phase space coordinates.
	 * @param longiPos The longitudinal position in meters along the orbit counted
	 *                 from the beginning of the orbit.
	 * @param pp       A potential provider to get the vector potential at the
	 *                 position of the particle
	 * @param type     A pre-defined particle type.
	 * 
	 * @return The particle created.
	 * 
	 * @throws OutOfApertureException when the particle is out of aperture
	 * @throws KnownFatalException    when an internal error happens.
	 */
	public Particle getParticle(double p0InGeV, PhaseSpaceCoordinates phs, double longiPos, PotentialProvider pp,
			ParticleType type) throws OutOfApertureException, KnownFatalException {

		return getParticle(p0InGeV, phs, longiPos, pp, type.getMass(), type.getCharge());
	}

	/**
	 * Generate particles in a flat 5D beam, all particles are on the same
	 * longitudinal position. The phase space coordinates are taken from a file.
	 * 
	 * @param p0InGeV  Momentum in GeV/c
	 * @param fileName File containing the phase space coordinates. x,x',y,y',s,dp/p
	 * @param longiPos The longitudinal position in meters along the orbit counted
	 *                 from the beginning of the orbit.
	 * @param afp      A potential provider to get the vector potential at the
	 *                 position of the particle
	 * @param mass     Mass [kg]
	 * @param charge   Charge [Coulumb]
	 * 
	 * @return The particle created.
	 * 
	 * @throws FileNotFoundException when the file is not found.
	 * @throws KnownFatalException    when an internal error happens.
	 */
	public List<Particle> getFromFile(double p0InGeV, String fileName, double longiPos, PotentialProvider afp,
			double mass, double charge) throws FileNotFoundException, KnownFatalException {
		double[][] d = FileUtils.read2DArray(fileName, 6); // phase space data
		List<Particle> l = new ArrayList<>();
		int count = 0;
		for (int i = 0; i < d.length; i++) {
			PhaseSpaceCoordinates phs = new PhaseSpaceCoordinates(d[i][0], d[i][1], d[i][2], d[i][3], d[i][4], d[i][5]);
			Particle p = null;

			try {
				p = this.getParticle(p0InGeV, phs, longiPos, afp, mass, charge);
				p.setName("p"+i);
			} catch (OutOfApertureException e) {			
				count++;
			}
			
			if (p != null)
				l.add(p);
		}
		if (count != 0)
			logger.warn(count + " particles were out of aperture in the file " + fileName);
		return l;
	}
}
