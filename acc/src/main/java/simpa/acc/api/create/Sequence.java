/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.acc.api.create;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.Optics;
import simpa.core.api.ExtrudedSurface;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.PointSource;
import simpa.core.api.PotentialProvider;
import simpa.core.api.Profile;
import simpa.core.api.SHCombiner;
import simpa.core.api.SourceArrangement;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.LocalFramesCalculator;
import simpa.core.api.utils.PathUtils;

/**
 * Class that represents a sequence of machine elements that make up an
 * accelerator or a transfer line.
 */
public class Sequence {

	private static final Logger logger = LogManager.getLogger(Sequence.class);

	private static double orbitStep = 0.01;

	public static final int POINTS_IN_ARC = 101; // must be and odd number to have a middle point
	private static final int CENTER_INDEX = POINTS_IN_ARC / 2;
	private static final Vector3D SEQUENCE_START = new Vector3D(0, 0, 0);

	private final List<Vector3D> orbitPoints;

	private final Map<String, AccElement> accElements;
	private Map<String, Vector3D> localOrigins = new HashMap<>(); // in the global frame
	private Map<String, Double> scalingMap; // scaling of elements in the sequence
	private final List<VacuumChamber> chambers = new ArrayList<>();
	private double currentLength;
	private boolean frozen = false;
	private boolean circular;
	private String name;
	// these two vectors defines the local frame
	private Vector3D beamDirection;// unit vector in the beam propagation direction
	private Vector3D verticalAxis;

	private ReferenceOrbit refOrbit;
	private PotentialProvider potentialProvider;
	private Optics optics;
	private LocalFramesCalculator.Strategy strategy; // strategy to construct the local frames
	private Vector3D vertical; // preferred vertical direction at local frame construction
	private SHCombiner sHCombiner = SHCombiner.getUniqueInstance();

	private static Sequence activeSequence;
	private static HashMap<String, Sequence> sequences = new HashMap<>();

	/**
	 * 
	 * @return A map of all sequences. The key is the name of the sequence.
	 *         The value is the sequence itself.
	 */
	public static HashMap<String, Sequence> getSequences() {
		return sequences;
	}

	/**
	 * 
	 * @return The active sequence.
	 */
	public static Sequence getActiveSequence() {
		return activeSequence;
	}

	/**
	 * 
	 * @param activeSequence The active sequence.
	 */
	public static void setActiveSequence(Sequence activeSequence) {
		Sequence.activeSequence = activeSequence;
	}

	/**
	 * Import an elementd into the active sequence from another sequence.
	 * The method automatically opens a frozen sequence, then imports the element
	 * and sets the sequence back to frozen. The imported element longitunal
	 * position is set to NaN. The imported element position and orientation is
	 * preserved. The typical use case is to import a disturbance from another
	 * sequence.
	 * 
	 * @param sequence The sequence to import from.
	 * @param name     The name of the element to import. It must be a instance of
	 * @link AccSourceArrangement.
	 */
	public static void elementFrom(Sequence sequence, String name) {

		Sequence active = getActiveSequence();
		if (active == null)
			throw new IllegalArgumentException("No active sequence is set.");
		AccElement e = sequence.accElements.get(name);
		if (e == null)
			throw new IllegalArgumentException("The element: " + name + " is not in the sequence: " + sequence.name);
		if (!(e instanceof AccSourceArrangement))
			throw new IllegalArgumentException("The element: " + name + " is not an AccSourceArrangement");
		
		((AccSourceArrangement) e).setLongiPos(Double.NaN); 
		active.setFozen(false);
		active.accElements.put(name, e);
		active.setFozen(true);
	}

	//@formatter:off
	/**
	 * Constructs a closed or open sequence representing a circular machine or a transfer line.
	 *
	 *  We use the following conventions:
	 *    - Right handed coordinate systems, both locally and globally.
	 *    - The Y coordinate of the local coordinate system points up, when the machine is in the X,Z global plane.
	 *    - The Z coordinate of the local coordinate system points always in the direction of the beam propagation.
	 *    - A circular machine consisting of bending magnets with positive bending angles will be constructed such,
	 *      that the Z local coordinate points clockwise. 
	 *    - For positive bending angle the X axis points outside of the circle and inside for negative ones.
	 * 
	 *  @param name      The name of the sequence    
	 * 
	 *  @param circular  If true, a circular machine will be constructed by connecting the last orbit point to the first one. 
	 *                   Setting it false results a transfer line.      
	 *  @param strategy A strategy to construction the local coordinates see {@link LocalFramesCalculator.Strategy}
	 *  @param vertical The preferred vertical direction at  construction the local coordinates see {@link LocalFramesCalculator}                     
	 */
	//@formatter:on
	public Sequence(String name, boolean circular, LocalFramesCalculator.Strategy strategy, Vector3D vertical) {
		this.name = name;
		this.circular = circular;
		this.strategy = strategy;
		this.vertical = vertical;

		orbitPoints = new ArrayList<>();
		orbitPoints.add(SEQUENCE_START);

		beamDirection = Vector3D.PLUS_K;
		verticalAxis = Vector3D.PLUS_J;

		accElements = new TreeMap<>();
	}

	/**
	 * @return The optics belonging to this sequence. It is null until the optics is
	 *         calculated
	 */
	public Optics getOptics() {
		return optics;
	}

	/**
	 * Sets the optics belonging to this sequence.
	 * 
	 * @param optics optics belonging to this sequence.
	 */
	public void setOptics(Optics optics) {
		this.optics = optics;
	}

	/**
	 * 
	 * @return A deep copy of the scaling map of this sequence.
	 */
	public Map<String, Double> getScalingMap() {
		Map<String, Double> copy = new HashMap<>();
		for (String key : scalingMap.keySet()) {
			copy.put(new String(key), Double.valueOf(scalingMap.get(key)));
		}
		return copy;
	}

	/**
	 * Set the scalings of the elements in the sequence and creates a potential
	 * provider from the scaled field maps.
	 * 
	 * @param scalingMap     A map containing the scaling of the sequence elements.
	 *                       The key is the group name and the value is the scaling
	 *                       factor. A group is usually contains all elements which
	 *                       connected to the same power supply. The value is given
	 *                       usually in Amper for magnetic elements and Volt for
	 *                       electrostatic elements.
	 * @param enableExternal If true, the map can contain field map groups which are
	 *                       not in the sequence. This can be useful when tracking
	 *                       the sequence with some external field disturbance. When
	 *                       true the map will be checked for groups not in the
	 *                       sequence and if found one, an
	 *                       {@link IllegalArgumentException} will be thrown.
	 * @throws IOException
	 */
	public void setScalingMap(Map<String, Double> scalingMap, boolean enableExternal) throws IOException {
		if (!enableExternal) {
			// check if the the group is present in the sequence
			for (String fieldMapBinaryName : scalingMap.keySet()) {
				String groupName = fieldMapBinaryName.split("_")[0];
				if (!getSourceArrangementGroups().containsKey(groupName))
					throw new IllegalArgumentException("The group: " + groupName
							+ "  in the field scaling map is not in the sequence, and the enable external flag is false");
			}
		}
		potentialProvider = sHCombiner.getEvaluator(scalingMap);
		this.scalingMap = scalingMap;
		logger.info("Created SHField with scalings: " + getScalingMapString());
	}

	/**
	 * Updates the scalings of this sequence with the given scaling map.
	 * 
	 * @param newScalings The new scalings. Original scaling not in this map will
	 *                    not be modified. If the new scaling map contains a key not
	 *                    present of the original map, it will be ignored.
	 * @throws IOException When a map has not been found.
	 */
	public void updateScalingMap(Map<String, Double> newScalings) throws IOException {

		if (scalingMap.size() == 0)
			logger.warn("Updating an empty scaling map in sequence: " + this.name + " has no effect !");

		Map<String, Double> map = new HashMap<>();

		for (String key : scalingMap.keySet()) {

			if (newScalings.containsKey(key))
				map.put(key, newScalings.get(key));
			else {
				map.put(key, scalingMap.get(key));
			}
		}
		setScalingMap(map, true);
	}

	/**
	 * 
	 * @return returns the potential provider of the sequence.
	 */
	public PotentialProvider getPotentialProvider() {
		return potentialProvider;
	}

	//@formatter:off
	/**
	 *  Puts a straight element at the end of the beam line at the specified longitudinal position.
	 *  It is assumed that the Z axis of the local coordinate system of the element to be added points 
	 *  in the direction of the beam propagation. 
	 *
	 * @param longiPos     Longitudinal position.
	 * 
	 * @param name         A unique name for this specific element. Example:QUAD1
	 * 
	 * @param scaling      A scaling factor for the strength of the sources. Usually
	 *                     this is the same as the current or the voltage in [A] or [V].
	 * 
	 * @param solutionFile Source arrangement file. called "NAME OF THE ELEMENT
	 *                     TYPE"-SOLUTION.txt Example: QUAD-SOLUTION.txt
	 * 
	 * @param tilt         Tilt angle. This is a rotation along the local Z (S)
	 *                     axis. A positive tilt angle will rotate the element
	 *                     accorging to the right-hand rule for curve
	 *                     orientation.See:
	 *                     https://en.wikipedia.org/wiki/Right-hand_rule
	 * 
	 * @param groupName    Specifies the group at which this element will be added.
	 *                     The elements in the same group will be in the same binary
	 *                     field map after the spherical harmonics expansion. If it is
	 *                     null, the name of the element will be used.
	 * 
	 * @param preRotation  A rotation to be applied before putting the element into
	 *                     the sequence. If the element is oriented according to the
	 *                     conventions, such as the local Z axis coincides with the reference orbit
	 *                     and the local Y axis points up, this should be the identity rotation.
	 *                     Typically used for mis-alignment or put an existing point source with a different
	 *                     orientation.
	 *                     
	 * @param profileMap   A Vacuum chamber if any. If null, the default profile 
	 * 					   will be used at the construction of the beam region.                  
	 */
	//@formatter:on
	public void addStraightElementAt(double longiPos, String name, double scaling, String solutionFile, double tilt,
			String groupName, Rotation preRotation, Map<Double, Profile> profileMap) {

		if (frozen)
			throw new IllegalArgumentException("After the sequenze is frozen, can't add elements. ");

		if (name == null)
			throw new IllegalArgumentException("The element must have a name");

		if (groupName == null)
			groupName = name;

		if (accElements.containsKey(name))
			throw new IllegalArgumentException("The sequence already contains an element with the name: " + name);

		Vector3D last = getLastOrbitPoint();

		double dist = longiPos - currentLength;

		if (dist < 0)
			throw new IllegalArgumentException(
					"Error during sequence building: The longitudinal position can not decrease");

		Vector3D center = last.add(beamDirection.scalarMultiply(dist));
		localOrigins.put(name, center);

		addStraightSectionToOrbit(last, center);

		Rotation rot = getElementRotation(0, tilt, false).applyTo(preRotation);

		AccSourceArrangement sa = new AccSourceArrangement(solutionFile, center, rot, scaling, name, groupName,
				longiPos);

		currentLength = longiPos;

		if (accElements.containsKey(name))
			throw new IllegalArgumentException("The sequence already contains an element with name: " + name);

		accElements.put(name, sa);

		if (profileMap != null) {
			addVacuumChamber(profileMap, rot, center, name);
		}
	}

	/**
	 * Puts an element with arbitrary shape at the end of the beam line at the
	 * specified longitudinal position. Unlike for the bending and straight elements
	 * the arbitrary element must accompanied by a path which describes the
	 * trajectory of the ideal particle. The element is oriented such that the local
	 * Z axis of the element points in the same direction as the last vector in the
	 * reference orbit calculated so far. The element is rotated around the Z axis
	 * such that the local Y axis of the element points to the direction of the
	 * localYAxis parameter. The last vector of the path determines the Z direction
	 * at the exit of the element.
	 *
	 * @param longiPos     Longitudinal position of the first point of the path.
	 * 
	 * @param name         A unique name for this specific element. Example:QUAD1
	 * 
	 * @param scaling      A scaling factor for the strength of the sources. Usually
	 *                     this is the same as the current or the voltage in [A] or
	 *                     [V].
	 * 
	 * @param solutionFile Source arrangement file. called "NAME OF THE ELEMENT
	 *                     TYPE"-SOLUTION.txt Example: QUAD-SOLUTION.txt
	 * @param localYAxis   The Y direction at the entry of the element.
	 * 
	 * @param groupName    Specifies the group at which this element will be added.
	 *                     The elements in the same group will be in the same binary
	 *                     field map after the spherical harmonics expansion. If it
	 *                     is null, the name of the element will be used.
	 *
	 * @param profileMap   A Vacuum chamber if any. If null, the default profile
	 *                     will be used at the construction of the beam region.
	 * 
	 * @param path         A list of 3D points describing the reference orbit inside
	 *                     the element.
	 */
	public void addArbitraryElementAt(double longiPos, String name, double scaling, String solutionFile,
			Vector3D localYAxis, String groupName, Map<Double, Profile> profileMap, List<Vector3D> path) {

		if (frozen)
			throw new IllegalArgumentException("After the sequenze is frozen, can't add elements. ");

		if (name == null)
			throw new IllegalArgumentException("The element must have a name");

		if (groupName == null)
			groupName = name;

		if (accElements.containsKey(name))
			throw new IllegalArgumentException("The sequence already contains an element with the name: " + name);

		Vector3D last = getLastOrbitPoint();

		double dist = longiPos - currentLength;

		if (dist < 0)
			throw new IllegalArgumentException(
					"Error during sequence building: The longitudinal position can not decrease");

		Vector3D entry = last.add(beamDirection.scalarMultiply(dist));
		addStraightSectionToOrbit(last, entry);

		Vector3D localOrigin = entry.subtract(path.get(0)); // in the global frame

		List<Vector3D> shiftedPath = PathUtils.transform(path, Rotation.IDENTITY, localOrigin);
		shiftedPath.remove(0); // to avoid duplicate points, addStraightSectionToOrbit(last, entry) includes
								// the first point
		localOrigins.put(name, localOrigin);

		Rotation rot = new Rotation(Vector3D.PLUS_K, Vector3D.PLUS_J, beamDirection, localYAxis);
		orbitPoints.addAll(shiftedPath);
		// update beam direction
		beamDirection = path.get(path.size() - 1).subtract(path.get(path.size() - 2)).normalize();

		AccSourceArrangement sa = new AccSourceArrangement(solutionFile, localOrigin, rot, scaling, name, groupName,
				longiPos);

		Map<Integer, Double> dl = PathUtils.getArcDistances(path, false);
		double elementPathlength = dl.get(dl.size() - 1);

		currentLength = longiPos + elementPathlength;

		accElements.put(name, sa);

		if (profileMap != null) {
			List<Double> kl = profileMap.keySet().stream().sorted().toList();
			double z1 = kl.get(0);
			double z2 = kl.get(kl.size() - 1);
			List<Vector3D> line = new ArrayList<>();
			line.add(new Vector3D(0, 0, z1));
			line.add(Vector3D.ZERO);// we need this only because PathCreator.getTangentVectorOnPath() has a bug
			line.add(new Vector3D(0, 0, z2));
			ExtrudedSurface s = new ExtrudedSurface(line, profileMap);
			// s.toFile("surface.txt");
			VacuumChamber vc = new VacuumChamber(s, rot, localOrigin, name);
			// vc.toFile(name+"_vc.txt");
			chambers.add(vc);
		}
	}

	/**
	 * Adds a marker element (marker, pickup) to the specified position. If the
	 * position is after the last orbit point it extends the orbit.
	 * 
	 * @param longiPos   Longitudinal position of the marker element.
	 * @param name       Name of the marker.
	 * @param type       Type of the element.
	 * @param profileMap A Vacuum chamber if any. If null, the default profile
	 *                   will be used at the construction of the beam region.
	 * 
	 */
	public void addMarkerElement(double longiPos, String name, AccElement.ElementType type,
			Map<Double, Profile> profileMap) {

		if (accElements.containsKey(name))
			throw new IllegalArgumentException("The sequence already contains an element with name: " + name);

		if (frozen)
			throw new IllegalArgumentException("After the sequenze is frozen, can't add elements. ");

		if (name == null)
			throw new IllegalArgumentException("The element must have a name");

		Vector3D last = getLastOrbitPoint();

		double dist = longiPos - currentLength;

		if (dist < 0)
			throw new IllegalArgumentException(
					"Error during sequence building: The longitudinal position can not decrease");

		Vector3D center = last.add(beamDirection.scalarMultiply(dist));
		localOrigins.put(name, center);

		addStraightSectionToOrbit(last, center);

		accElements.put(name, new MarkerElement(name, longiPos, type));
		currentLength = longiPos;

		if (profileMap != null) {
			Rotation rot = getElementRotation(0, 0, false);
			addVacuumChamber(profileMap, rot, center, name);
		}

	}

	//@formatter:off
	/**
	 * Puts a bending at the end of the beam line with the center at the given longitudinal position.
	 *  It is assumed that the Z axis of the local coordinate system at the center of the bending and 
	 *  it points in the direction of the beam propagation.
	 *
	 * @param longiPos     Longitudinal position of the bending center.
	 * 
	 * @param name         A unique name for this specific magnet. Example: ELBHZ01
	 * 
	 * @param scaling      A scaling factor for the strength of the sources. Usually
	 *                     this is the same as the current or the voltage in [A] or [V].
	 * 
	 * @param solutionFile Source strength file, called "NAME OF THE BENDING
	 *                     TYPE"-SOLUTION.txt Example: ELBHZ-SOLUTION.txt
	 * 
	 * @param length       Length of the magnet [meters]
	 * 
	 * @param deflection   Deflection angle [radians]
	 * 
	 * @param tilt         Tilt angle. This is a rotation along the local Z (S)
	 *                     axis. A positive tilt angle will rotate the element
	 *                     accorging to the right-hand rule for curve orientation.See:
	 *                     https://en.wikipedia.org/wiki/Right-hand_rule
	 * 
	 * @param groupName    Specifies the group at which this element will be added.
	 *                     The elements in the same group will be in the same
	 *                     field map after the spherical harmonics expansion. If it
	 *                     is null, the name of the element will be used.
	 * 
	 * @param preRotation  A rotation to be applied before putting the element into
	 *                     the sequence. If the element is oriented according to the
	 *                     conventions this should be the identity rotation.
	 *                     Typically used for misalignment simulation.
	 *                     
	 *@param profileMap	   A Vacuum chamber if any. If null, the default profile 
	 * 					   will be used at the construction of the beam region.  
	 *                 
	 *@param sectorBend    This should be true for a sector bending. If true a half
	 *                     rotation is applied. For a rectangular bend no need for the
	 *                     half rotation, because the orbit in the magnet local frame
	 *                     is aligned with the Z axis, while the sector bending has an
	 *                     angle which is half the deflection angle.
	 */
	//@formatter:on
	public void addBendingElementAt(double longiPos, String name, double scaling, String solutionFile, double length,
			double deflection, double tilt, String groupName, Rotation preRotation, Map<Double, Profile> profileMap,
			boolean sectorBend) {

		if (frozen)
			throw new IllegalArgumentException("After the sequenze is frozen, can't add elements. ");

		if (longiPos < currentLength)
			throw new IllegalArgumentException("The longitudinal position can not decrease");

		if (name == null)
			throw new IllegalArgumentException("The element must have a name");
		if (groupName == null)
			groupName = name;

		if (accElements.containsKey(name))
			throw new IllegalArgumentException("The sequence already contains an element with the name: " + name);

		// TODO internally we have a sign error, to be fixed at some point
		deflection = -deflection;

		// extend orbit to the entry
		double ssLength = (longiPos - length / 2.0) - currentLength;
		Vector3D ssEnd = beamDirection.scalarMultiply(ssLength).add(getLastOrbitPoint());
		this.addStraightSectionToOrbit(getLastOrbitPoint(), ssEnd);

		double r = length / deflection; // bending radius
		List<Vector3D> localArc = PathUtils.getArc(r, deflection, POINTS_IN_ARC);
		assert (Math.abs(PathUtils.calculatePathLength(localArc, false) - length) < 1e-9);

		Vector3D globalShift = getLastOrbitPoint();
		Vector3D center = Vector3D.ZERO;
		Rotation arcRot = new Rotation(Vector3D.PLUS_J, Vector3D.PLUS_K, this.verticalAxis, this.beamDirection);
		Rotation tiltRot = new Rotation(beamDirection, tilt, RotationConvention.VECTOR_OPERATOR);
		arcRot = tiltRot.applyTo(arcRot);

		for (int i = 1; i < POINTS_IN_ARC; i++) {
			Vector3D p = localArc.get(i);
			p = arcRot.applyTo(p);
			p = p.add(globalShift); // global shift
			orbitPoints.add(p);
			if (i == CENTER_INDEX)
				center = p;
		}

		Rotation rotForMagnet = getElementRotation(deflection, tilt, sectorBend).applyTo(preRotation);

		AccSourceArrangement sbend = new AccSourceArrangement(solutionFile, center, rotForMagnet, scaling, name,
				groupName, longiPos);
		localOrigins.put(name, center);

		// currentLength = longiPos + length / 2;
		// assert (Math.abs(PathUtils.calculatePathLength(orbitPoints, false) -
		// currentLength) < 1e-6);

		// we re-adjust the path length to the second order estimation
		// there is some difference between the sum of a straight section and arc and
		// the second order estimation due to the part where they connect. This is
		// normal, we just need to decide which one we use. Since we have arbitrary
		// elements with arbitrary reference orbit, it seems reasonable to stick with
		// the second order estimation
		currentLength = PathUtils.calculatePathLength(orbitPoints, false);

		accElements.put(name, sbend);

		if (profileMap != null) {

			// add a small vector at the beginning and end to have exact tangent vector
			Rotation halfr = new Rotation(Vector3D.PLUS_J, -deflection / 2, RotationConvention.VECTOR_OPERATOR);
			Vector3D tr = localArc.get(Sequence.CENTER_INDEX).scalarMultiply(-1);
			List<Vector3D> arc = PathUtils.transform(localArc, halfr, tr);
			double delta = arc.get(0).distance(arc.get(1));
			Vector3D vEntry = halfr.applyTo(Vector3D.PLUS_K).scalarMultiply(delta / 2).add(arc.get(0));
			Vector3D vExit = halfr.applyInverseTo(Vector3D.PLUS_K).scalarMultiply(delta / 2)
					.add(arc.get(arc.size() - 1));
			arc.add(1, vEntry);
			arc.add(vExit);

			// FileUtils.writeVectors("arc.txt", arc);
			ExtrudedSurface s = new ExtrudedSurface(arc, profileMap);
			// s.toFile("surface.txt");
			VacuumChamber vc = new VacuumChamber(s, rotForMagnet, center, name);
			// vc.toFile("chamber.txt");
			chambers.add(vc);
		}

		updateLocalCoordinates(deflection, tilt);
	}

	/**
	 * Adds an accelerator element from a solution file. The the sources from
	 * 'elementname-SOLUTION.txt' file is read. Then the sources are scaled, rotated
	 * and translated and an {@link AccSourceArrangement} will be added to the
	 * sequence. The reference orbit is not affected.
	 * 
	 * @param name         Name of the element.
	 * @param scaling      Scaling factor for the sources.
	 * @param solutionFile File named according to the file naming
	 *                     conventions.{@link FileNamingConventions} containing the
	 *                     sources.
	 * @param groupName    Group name to which this element belongs to.
	 * @param rotation     Rotation applied before the translation.
	 * @param translation  Translation to be applied after the rotation.
	 */
	public void addExternalElement(String name, double scaling, String solutionFile, String groupName,
			Rotation rotation, Vector3D translation) {

		if (frozen)
			throw new IllegalArgumentException("After the sequenze is frozen, can't add elements. ");

		if (name == null)
			throw new IllegalArgumentException("The element must have a name");

		if (groupName == null)
			groupName = name;

		if (accElements.containsKey(name))
			throw new IllegalArgumentException("The sequence already contains an element with the name: " + name);

		AccSourceArrangement sa = new AccSourceArrangement(solutionFile, translation, rotation, scaling, name,
				groupName, 0); // the longitudinal position is irrelevant for external elements

		accElements.put(name, sa);
	}

	/**
	 * Gets the map of SourceArrangment groups in the sequence. The key is the group
	 * name, the value is a list of SourceArrangement instances.
	 *
	 * @return Map of element groups.
	 */
	public HashMap<String, List<SourceArrangement>> getSourceArrangementGroups() {
		HashMap<String, List<SourceArrangement>> map = new HashMap<>();

		for (String name : accElements.keySet()) {
			AccElement e = accElements.get(name);
			if (e instanceof AccSourceArrangement) {
				AccSourceArrangement accSe = (AccSourceArrangement) e;
				String groupName = accSe.getGroupName();
				List<SourceArrangement> l = map.get(groupName);
				if (l == null) {
					l = new ArrayList<SourceArrangement>();
					map.put(groupName, l);
				}

				l.add(accSe.getSourceArrangement());
			}
		}

		return map;
	}

	/**
	 * Gets the reference orbit of the sequence.
	 *
	 * @return the reference orbit.
	 */
	public ReferenceOrbit getReferenceOrbit() {
		if (refOrbit == null) {

			if (isCircular()) {
				// we close the orbit
				Vector3D lastPoint = orbitPoints.get(orbitPoints.size() - 1);
				// The last point of the orbit is one step back from the starting point in the
				Vector3D lastOrbitPoint = SEQUENCE_START.subtract(Vector3D.PLUS_K.scalarMultiply(orbitStep));
				this.addStraightSectionToOrbit(lastPoint, lastOrbitPoint);
				currentLength += lastPoint.getNorm();
			}

			refOrbit = new ReferenceOrbit(orbitPoints, isCircular(), strategy, vertical);
		}
		// double orbl = refOrbit.getOrbitLength();
		// These two are not exactly the same, because joint between straight and arc
		// assert (Math.abs(orbl - this.currentLength) < 1e-9);

		return refOrbit;
	}

	/**
	 * This method returns the current length of the sequence. Unlike the orbit
	 * length which is available only when the sequence construction is finished,
	 * the current length is available during the construction of the sequence.
	 * 
	 * @return The current length of the sequence.
	 */
	public double getCurrentLength() {
		return currentLength;
	}

	/**
	 * Gets a list of vectors pointing to the local origins of the elements in the
	 * sequence. The coordinates of the vectors are expressed in the global
	 * coordinate system.
	 *
	 * @return List of local origins of the elements
	 */
	public Map<String, Vector3D> getLocalOrigins() {
		return localOrigins;
	}

	/**
	 * Gets a list of oriented and positioned vacuum chambers along the reference
	 * orbit.
	 * 
	 * @return List of chambers in the sequence.
	 */
	public List<VacuumChamber> getVacuumChambers() {
		return chambers;
	}

	/**
	 * Writes a text file containing all source positions to the given file. For
	 * each source there is a line in the file.
	 *
	 * @param fileName Name of the output file.
	 */
	public void sourcePositionsToFile(String fileName) {

		StringBuilder sb = new StringBuilder();
		sb.append("#X Y Z  NAME").append(System.lineSeparator());
		List<SourceArrangement> all = getSourceArrangements();
		for (SourceArrangement sa : all) {
			for (PointSource p : sa.getPointSources()) {
				Vector3D v = p.getLocation();
				sb.append(v.getX()).append(" ").append(v.getY()).append(" ").append(v.getZ()).append(" ")
						.append(sa.getName()).append(System.lineSeparator());
			}
		}
		FileUtils.writeTextFile(fileName, sb.toString());
	}

	/**
	 * Gets a list of the {@link simpa.core.api.SourceArrangement} objects in the
	 * sequence.
	 *
	 * @return list of all source arrangements.
	 */
	public List<SourceArrangement> getSourceArrangements() {
		return accElements.values().stream().filter(AccSourceArrangement.class::isInstance)
				.map(AccSourceArrangement.class::cast).map(s -> s.getSourceArrangement()).collect(Collectors.toList());

	}

	/**
	 * Gets a boolean that tells whether the sequence is frozen or not.
	 *
	 * @return true if the sequence is frozen.
	 */
	public boolean getFrozen() {
		return frozen;
	}

	/**
	 * Used for indicating when a sequence is frozen. If the sequence has been
	 * frozen, not possible to add elements to it anymore.
	 *
	 * @param frozen Set this true if want to set the sequence is frozen.
	 */
	public void setFozen(boolean frozen) {
		this.frozen = frozen;
	}

	/**
	 * 
	 * @return true if it is a ring.
	 */
	public boolean isCircular() {
		return circular;
	}

	/**
	 * 
	 * @return The name of the sequence.
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return A map of AccElements of this sequence, with the name of the element
	 *         as the key.
	 */
	public Map<String, AccElement> getAccElements() {
		return this.accElements;
	}

	/**
	 * 
	 * @return Step size for the orbit.
	 */
	public double getOrbitStep() {
		return orbitStep;
	}

	/**
	 * Set the step size for the orbit. By default it is 0.01 [m].
	 * 
	 * @param orbitStep Step size for the orbit.
	 */
	public void setOrbitStep(double orbitStep) {
		Sequence.orbitStep = orbitStep;
	}

	/**
	 * Returns a scaling map as a string in SIMPA format. The scaling map is
	 * is sorted by the longitudinal position of the elements.
	 * 
	 * @return A string representation of the scaling map.
	 */
	public String getScalingMapString() {
		Map<String, Double> scalings = getScalingMap();
		StringBuilder sb = new StringBuilder();
		sb.append("[");

		List<Map.Entry<String, AccElement>> entryList = new ArrayList<>(getAccElements().entrySet());
		entryList.sort(Map.Entry.comparingByValue(Comparator.comparingDouble(AccElement::getLongitudinalPosition)));

		for (Map.Entry<String, AccElement> e : entryList) {
			// loop through the scaling map and if the name is found in the scaling map,
			// then print it
			for (String key : scalings.keySet()) {
				if (key.contains(e.getKey())) {
					sb.append("{\"" + key + "\"").append(":").append(scalings.get(key))
							.append("}, " + System.lineSeparator());
				}
			}
		}

		String s = sb.toString();
		s += "]";

		return s;
	}

	private void addStraightSectionToOrbit(Vector3D from, Vector3D to) {

		double length = to.subtract(from).getNorm();
		long steps = Math.round(length / orbitStep);
		Vector3D dv = to.subtract(from);

		if (dv.getNorm() == 0)
			return;

		// special case when the step is smaller than the resolution of the orbit
		if (steps == 0) {
			orbitPoints.remove(orbitPoints.size() - 1);
			orbitPoints.add(to);
		}

		dv = dv.normalize().scalarMultiply(length / steps);
		for (int i = 1; i <= steps; i++) {
			Vector3D p = from.add(dv.scalarMultiply(i));
			orbitPoints.add(p);
		}

	}

	private Vector3D getLastOrbitPoint() {
		return orbitPoints.get(orbitPoints.size() - 1);
	}

	/**
	 * Get a Rotation from the elements' local frame to the machine's global one. We
	 * assume the origin of the local system is at the center of the element.
	 * 
	 * @param deflection
	 * @param tilt
	 * @param sectorBend This should be true for a sector bending. If true a half
	 *                   rotation is applied. For a rectangular bend no need for the
	 *                   half rotation, because the orbit in the magnet local frame
	 *                   is aligned with the Z axis, while the sector bending has an
	 *                   angle which is half the deflection angle.
	 * @return A rotation
	 */
	private Rotation getElementRotation(double deflection, double tilt, boolean sectorBend) {
		Rotation tiltRot = new Rotation(beamDirection, tilt, RotationConvention.VECTOR_OPERATOR);
		// orienting the magnet
		Rotation halfDeflection = new Rotation(this.verticalAxis, deflection / 2, RotationConvention.VECTOR_OPERATOR);

		if (!sectorBend)
			halfDeflection = Rotation.IDENTITY;

		// from the global frame to the previous local frame
		Rotation accumulatedRot = new Rotation(Vector3D.PLUS_J, Vector3D.PLUS_K, this.verticalAxis, this.beamDirection);

		return tiltRot.applyTo(halfDeflection.applyTo(accumulatedRot));
	}

	private void updateLocalCoordinates(double deflection, double tilt) {
		// apply tilt
		Rotation tiltRot = new Rotation(beamDirection, tilt, RotationConvention.VECTOR_OPERATOR);
		verticalAxis = tiltRot.applyTo(verticalAxis);

		Rotation rot = new Rotation(verticalAxis, deflection, RotationConvention.VECTOR_OPERATOR);
		beamDirection = rot.applyTo(beamDirection);

	}

	private void addVacuumChamber(Map<Double, Profile> profileMap, Rotation rot, Vector3D center, String name) {

		List<Double> kl = profileMap.keySet().stream().sorted().toList();
		double z1 = kl.get(0);
		double z2 = kl.get(kl.size() - 1);
		List<Vector3D> line = new ArrayList<>();
		line.add(new Vector3D(0, 0, z1));
		line.add(Vector3D.ZERO);// we need this only because PathCreator.getTangentVectorOnPath() has a bug
		line.add(new Vector3D(0, 0, z2));
		ExtrudedSurface s = new ExtrudedSurface(line, profileMap);
		// s.toFile("surface.txt");
		VacuumChamber vc = new VacuumChamber(s, rot, center, name);
		// vc.toFile(name+"_vc.txt");
		chambers.add(vc);

	}

}
