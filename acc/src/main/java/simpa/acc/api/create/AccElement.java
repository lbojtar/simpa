package simpa.acc.api.create;

public interface AccElement extends Comparable<AccElement>{
	
	public static enum ElementType { SOURCE_ARRANGEMENT, MARKER, H_PICKUP,V_PICKUP};	

	public double getLongitudinalPosition();

	public ElementType getType();
	
	public String getName();


    @Override
    default int compareTo(AccElement other) {
        return Double.compare(this.getLongitudinalPosition(), other.getLongitudinalPosition());
    }

}
