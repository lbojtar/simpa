/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.acc.api.create;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.BoundarySurface;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.Profile;
import simpa.core.api.SphereCovering;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.utils.PathUtils;
import simpa.core.api.utils.SimpleTriangulator;
import simpa.core.api.utils.TextStlWriter;
import simpa.core.api.utils.Triangulation;

/**
 * An object representing the beam region in an accelerator by a succesion of
 * profiles extruded around the reference orbit. The resulting surface can be
 * written to an STL file, which can serve as an aperture during the tracking or
 * an input to create a lattice ball cover for the beam region. It is a global
 * description of the beam region in a ring or beam line and serves a different
 * purpose than the {@link BoundarySurface} or the {@link VacuumChamber} which
 * related to individual elements.
 */
public class BeamRegion {

	private static final Logger logger = LogManager.getLogger(BeamRegion.class);

	private final List<Vector3D> path;

	// The map of profiles along the path. The integer key is the
	// index of the point in the path at which the profile center is.
	private Map<Integer, Profile> profileMap;
	private int lastProfileIndex;
	private final double orbitStepSize;
	private boolean circular;
	private Profile defaultProfile;
	private Sequence sequence;

	/**
	 * Creates a BeamRegion object. The default profile is extruded along the given
	 * path. Regions along the path with profiles different than the default can be
	 * set with the setProfile method.
	 *
	 * @param seq            The sequence the beam region is constructed for.
	 * @param distance       Approximate distance between consecutive default
	 *                       profiles. The exact distance is an integer multiple of
	 *                       the step size in the path.
	 * 
	 * @param defaultProfile A default 2D profile to be extruded along the path.
	 * 
	 * @param extention      In case of transfer lines, the beam region covered is
	 *                       extended some distance at the start of the sequence in
	 *                       the negative S direction and at the end of the sequence
	 *                       in the positive S direction to surely cover the fist
	 *                       and last step of the tracking. Ignored for rings. Unit
	 *                       is in [m].
	 */
	public BeamRegion(Sequence seq, double distance, Profile defaultProfile, double extention) {

		if (defaultProfile.getPoints().size() % 4 != 0) {
			throw new IllegalArgumentException("The number of points in the  profile must be a multiple of 4");
		}
		this.sequence = seq;
		this.circular = seq.isCircular();
		this.defaultProfile = defaultProfile;

		if (circular) {
			this.path = seq.getReferenceOrbit().getOrbitPoints();
		} else {
			List<Vector3D> l = new ArrayList<>();
			for (Vector3D v : seq.getReferenceOrbit().getOrbitPoints()) {
				l.add(new Vector3D(v.getX(), v.getY(), v.getZ()));
			}
			PathUtils.extendPath(l, extention, true);
			PathUtils.extendPath(l, extention, false);
			this.path = l;
		}

		orbitStepSize = path.get(1).subtract(path.get(0)).getNorm();
		int step = (int) Math.round(distance / orbitStepSize);
		profileMap = new HashMap<>();

		// put the default profiles along all the path
		extrude(0, path.size() - 1, step, this.defaultProfile);

		for (VacuumChamber chamber : seq.getVacuumChambers()) {
			addVacuumChamber(chamber);
		}
	}

	/**
	 * Writes a HCP lattice cover file for the beam region.
	 * 
	 * @param outFileName The name of the output file. The extension should be
	 *                    according to
	 *                    {@link FileNamingConventions#HCP_COVER_FILE_EXTENSION}
	 * @param radius      The radius of the spheres in the cover.
	 ** 
	 * @throws KnownFatalException
	 */
	public void writeHCPLatticeCover(String outFileName, double radius) throws KnownFatalException {

		// check extension
		if (!outFileName.endsWith(FileNamingConventions.HCP_COVER_FILE_EXTENSION))
			throw new IllegalArgumentException(
					"The output file name should end with " + FileNamingConventions.HCP_COVER_FILE_EXTENSION);

		List<Profile> profiles = getOrderedProfiles();
		SphereCovering hcpCover = SphereCovering.createHCPLatticeCover(profiles, radius, sequence.isCircular());
		hcpCover.toFile(outFileName);
	}

	/**
	 * Adds a vacuum chamber and removes the not needed overlapping default
	 * profiles.
	 *
	 * @param chamber A vacuum chamber to be added.
	 * 
	 */
	public void addVacuumChamber(VacuumChamber chamber) {

		int st = PathUtils.getClosestIndex(path, chamber.getEntry().getOrigin()) - 1; // -1 give some margin to avoid
																						// close profiles
		int end = PathUtils.getClosestIndex(path, chamber.getExit().getOrigin()) + 1;// +1 give some margin to avoid
																						// close profiles
		removeProfiles(st, end, chamber.getElementName());

		for (Profile profile : chamber.getOrientedProfiles()) {
			int i = PathUtils.getClosestIndex(path, profile.getOrigin());
			lastProfileIndex = i;
			profileMap.put(i, profile);
		}
	}

	/**
	 * Write the surface representing the beam region to an STL text file.
	 *
	 * @param file Filename, should end with .stl
	 */
	public void writeSTLTextFile(String file) {
		TextStlWriter.writeSTLTextFile(getTriangulation(), file);
	}

	/**
	 * Gets triangles of the boundary of the beam region.
	 *
	 * @return The Triangulation
	 */
	public Triangulation getTriangulation() {

		List<Profile> profiles = getOrderedProfiles();

		SimpleTriangulator st = new SimpleTriangulator(profiles, circular);

		Triangulation t;
		if (circular) {
			t = st.getSurface();
		} else {
			t = st.getSurfaceWithEndCups();
		}

		return t;

	}

	private List<Profile> getOrderedProfiles() {
		// The last profile we have is not at the end of the path, so we add a copy
		// when we have a transfer line
		if (!circular && lastProfileIndex != path.size() - 1) {
			addProfileAtPosition(path.size() - 1, defaultProfile);
		}

		Set<Integer> keys = profileMap.keySet();
		List<Integer> skeys = keys.stream().sorted().toList();

		List<Profile> profiles = skeys.stream().map(profileMap::get).collect(Collectors.toList());

		return profiles;
	}

	/**
	 * Removes a range of profiles from the beam region to make place for other
	 * profiles.
	 *
	 * @param startIdx start id of the range
	 * @param endIdx   end id of the range
	 */
	private void removeProfiles(int startIdx, int endIdx, String elementName) {
		// remove the default or any other profiles in the region
		Map<Integer, Profile> tmpMap = new HashMap<>();
		if (startIdx < endIdx) {
			for (Integer i : profileMap.keySet()) {
				Profile pr = profileMap.get(i);

				if (i > endIdx || i < startIdx) {
					tmpMap.put(i, pr);
				} else if (!pr.isDefaultProfile())
					throw new IllegalArgumentException(
							"Overlapping vacuum chamber detected while adding " + elementName);
			}
		} else {
			for (Integer i : profileMap.keySet()) {
				Profile pr = profileMap.get(i);

				if (i > endIdx && i < startIdx) {
					tmpMap.put(i, pr);
				} else if (!pr.isDefaultProfile())
					throw new IllegalArgumentException(
							"Overlapping vacuum chamber detected while adding " + elementName);
			}
		}
		profileMap = tmpMap;
	}

	/**
	 * Extrudes the profile along the path starting from the start index and ending
	 * at the end index of the path.
	 *
	 * @param start   Index of the start point in the path
	 * @param end     Index of the end point in the path
	 * @param lstep   step of indexes between two profiles
	 * @param profile profile to be extruded.
	 */
	private void extrude(int start, int end, int lstep, Profile profile) {

		for (int i = start; i <= end; i += lstep) {
			addProfileAtPosition(i, profile);
		}
	}

	/**
	 * Adds the oriented profile at the position along the path at the specified
	 * index.
	 *
	 * @param index   index on which to place the profile.
	 * @param profile the profile to add.
	 */
	private void addProfileAtPosition(int index, Profile profile) {
		Vector3D p = path.get(index);
		Vector3D normal;
		if (index == 0)
			normal = path.get(1).subtract(path.get(0));
		else
			normal = path.get(index).subtract(path.get(index - 1));

		Rotation rot = new Rotation(Vector3D.PLUS_K, Vector3D.PLUS_J, normal, Vector3D.PLUS_J);
		Profile opr = profile.getTransformedCopy(p, rot);
		opr.setDefaultProfile(true);
		profileMap.put(index, opr);
		// profileList.put(index, getOrientedProfile(profile, p, normal));
	}

}
