/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.acc.api.create;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simpa.core.api.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Factory methods for expanding groups of accelerator elements with solid
 * harmonic expansion.
 */
public class AccFieldFactory {
	private static final Logger logger = LogManager.getLogger(AccFieldFactory.class);

	/**
	 * Expands the potentials for the given accelerator element group of the machine
	 * inside the beam region with solid harmonics balls of a given maximum degree
	 * and writes the resulting binary field map file to the disk. The beam region
	 * is given by the balls in the cover object.
	 *
	 * @param sequence      The sequence to be used for the expansion. The sequence
	 *                      must contain the group given in the groupName parameter.
	 * 
	 * @param groupName     The name of the group to be expanded. All the elements
	 *                      in the group must have the same field type. It can be
	 *                      null, in this case all the groups will be expanded into
	 *                      a single field map.
	 * 
	 * @param lmax          Maximum degree (LMAX) of the balls in the lattice. The
	 *                      actual LMAX can be smaller than that if the given
	 *                      relativeError can be achived with a smaller LMAX, but
	 *                      can not be bigger than this parameter even the actual
	 *                      relative error is bigger than the given relativeError
	 *                      parameter.
	 * 
	 * @param cover         A cover object which describes the locations of the
	 *                      cover balls.
	 * 
	 * @param relativeError Relative limit for spherical harmonics coefficients.
	 *                      Coefficients smaller than the value determined by this
	 *                      limit considered zero and dropped. If null the value of
	 *                      the SHFieldFactory.DEFAULT_RELATIVE_ERROR will be taken.
	 * 
	 * @param writeStatFile If true, then write statistics about the distribution of
	 *                      maximum degrees in the field map.
	 * 
	 * @param pass1Lmax     Maximum degree for the first pass of the SH expansion.
	 *                      A good value is lmax/3 for example. The optimal value
	 *                      depends on the number and type of elements in the
	 *                      sequence. For example, if the group contains only
	 *                      a quadrupole, the field decreases quicly so most of the
	 *                      balls far from the quad has small coefficients, so
	 *                      choose a small value to not waste calculation to expand
	 *                      those balls to unneded high orders.
	 * 
	 * @throws IOException when something goes wrong reading or writing a file.
	 */
	public void shExpandGroup(Sequence sequence, String groupName, int lmax, SphereCovering cover, Double relativeError,
			boolean writeStatFile, int pass1Lmax) throws IOException {
		List<SourceArrangement> group;
		if (groupName != null)
			group = sequence.getSourceArrangementGroups().get(groupName);
		else {
			HashMap<String, List<SourceArrangement>> allGroups = sequence.getSourceArrangementGroups();
			group = allGroups.values().stream().flatMap(List::stream).collect(Collectors.toList());
			groupName = sequence.getName() + "_ALL";
		}

		logger.info("Solid harmonic expansion of the group:" + groupName);
		SHFieldFactory ff = new SHFieldFactory();
		String serialObjectFilename = FileNamingConventions.addFieldMapExtension(groupName, lmax);

		GlobalSourceArrangement gsa = new GlobalSourceArrangement(groupName, group, 1.0);
		ff.createSHField(gsa, cover, lmax, serialObjectFilename, relativeError, writeStatFile, pass1Lmax);
		
	}

	/**
	 * Expands all the groups in the sequence one by one. For each group a field map
	 * will be created with the group name.
	 * 
	 * @param sequence      The sequence to be used for the expansion. The sequence
	 *                      must contain the group given in the groupName parameter.
	 * @param lmax          Maximum degree (LMAX) of the balls in the lattice. The
	 *                      actual LMAX can be smaller than that if the given
	 *                      relativeError can be achived with a smaller LMAX, but
	 *                      can not be bigger than this parameter even the actual
	 *                      relative error is bigger than the given relativeError
	 *                      parameter.
	 * 
	 * @param cover         A cover object which describes the locations of the
	 *                      cover balls.
	 * 
	 * @param relativeError Relative limit for spherical harmonics coefficients.
	 *                      Coefficients smaller than the value determined by this
	 *                      limit considered zero and dropped. If null the value of
	 *                      the SHFieldFactory.DEFAULT_RELATIVE_ERROR will be taken.
	 * 
	 * @param writeStatFile If true, then write statistics about the distribution of
	 *                      maximum degrees in the field map.
	 * 
	 * @param pass1Lmax     Maximum degree for the first pass of the SH expansion.
	 *                      A good value is lmax/3 for example. The optimal value
	 *                      depends on the number and type of elements in the
	 *                      sequence. For example, if the group contains only
	 *                      a quadrupole, the field decreases quicly so most of the
	 *                      balls far from the quad has small coefficients, so
	 *                      choose a small value to not waste calculation to expand
	 *                      those balls to unneded high orders.
	 * 
	 * @throws IOException when something goes wrong reading or writing a file.
	 */
	public void shExpandAllGroups(Sequence sequence, int lmax, SphereCovering cover, Double relativeError,
			boolean writeStatFile, int pass1Lmax) throws IOException {
		for (String groupName : sequence.getSourceArrangementGroups().keySet()) {
			shExpandGroup(sequence, groupName, lmax, cover, relativeError, writeStatFile, pass1Lmax);
		}
	}
}
