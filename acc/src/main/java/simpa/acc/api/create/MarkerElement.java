package simpa.acc.api.create;

public class MarkerElement implements AccElement {

	private ElementType type;
	private double longiPos;
	private String name;

	public MarkerElement(String name, double longiPos, ElementType type) {
		this.name = name;
		this.type = type;
		this.longiPos = longiPos;
	}


	@Override
	public double getLongitudinalPosition() {
		return longiPos;
	}

	@Override
	public ElementType getType() {
		return type;
	}

	@Override
	public String getName() {
		return name;
	}

}
