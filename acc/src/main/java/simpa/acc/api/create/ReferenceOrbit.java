/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 * <p>
 * Author : Lajos Bojtar
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.acc.api.create;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simpa.core.api.TangentVector3D;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.LocalFrame;
import simpa.core.api.utils.LocalFramesCalculator;
import simpa.core.api.utils.PathUtils;
import simpa.core.api.utils.LocalFramesCalculator.Strategy;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

/**
 * A reference orbit.
 */
public class ReferenceOrbit {
	private static final Logger logger = LogManager.getLogger(ReferenceOrbit.class);

	private final List<Vector3D> path;
	private double orbitLength;
	private boolean circular;
	private LocalFramesCalculator localFramesCalculator;
	private Map<Integer, Double> longitudinalPositions;
	private LocalFramesCalculator.Strategy strategy;
	private Vector3D vertical;

	/**
	 * Deep copy constructor with rotation and translation.
	 * 
	 * @param original
	 * @param rot
	 * @param translation
	 */
	public ReferenceOrbit(ReferenceOrbit original, Rotation rot, Vector3D translation) {
		path = new ArrayList<>();
		for (Vector3D ov : original.getOrbitPoints()) {
			Vector3D v = rot.applyTo(ov).add(translation);
			path.add(v);
		}

		orbitLength = original.orbitLength;
		circular = original.circular;
		localFramesCalculator = new LocalFramesCalculator(path, circular, strategy, vertical);
		longitudinalPositions = new HashMap<>(original.longitudinalPositions);
		strategy = original.strategy;
		vertical = original.vertical;
	}

	/**
	 * Creates a reference orbit from a list of points.
	 *
	 * @param points   the points to create the orbit from
	 * @param circular true is the orbit is closed (circular)
	 * @param strategy A strategy how to define the local coordinate system. Can be
	 *                 null, in this case LocalFramesCalculator.DEFAULT_STRATEGY is
	 *                 used.
	 * @param vertical The preferred vertical direction.
	 */
	public ReferenceOrbit(List<Vector3D> points, boolean circular, LocalFramesCalculator.Strategy strategy,
			Vector3D vertical) {

		this.strategy = strategy;
		this.vertical = vertical;

		localFramesCalculator = new LocalFramesCalculator(points, circular, strategy, vertical);
		path = points;
		this.circular = circular;
		if (hasDuplicatePoints(path))
			throw new IllegalArgumentException("Orbit has duplicate points");
		orbitLength = PathUtils.calculatePathLength(path, circular);

	}

	/**
	 * Create a reference orbit from points in a file.
	 *
	 * @param filename input file
	 * @param circular true is the orbit is closed (circular)
	 */
	public ReferenceOrbit(String filename, boolean circular, LocalFramesCalculator.Strategy strategy,
			Vector3D vertical) {

		this.circular = circular;
		this.strategy = strategy;
		this.vertical = vertical;

		path = new ArrayList<>();

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

			do {
				String s = br.readLine();
				if (s == null) {
					break;
				}
				StringTokenizer st = new StringTokenizer(s, " ");
				try {
					// location coordinates
					double x = Double.parseDouble(st.nextToken());
					double y = Double.parseDouble(st.nextToken());
					double z = Double.parseDouble(st.nextToken());

					Vector3D p = new Vector3D(x, y, z);
					path.add(p);

				} catch (NumberFormatException e) {
					// we ignore the line, it might be just comment text
				}

			} while (true);

			br.close();

		} catch (IOException e) {
			logger.error("Something went wrong reading file: " + filename);
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

		if (hasDuplicatePoints(path))
			throw new IllegalArgumentException("Orbit has duplicate points");

		orbitLength = PathUtils.calculatePathLength(path, circular);

		localFramesCalculator = new LocalFramesCalculator(path, circular, strategy, vertical);

		logger.info("Created orbit from file: " + filename);
	}

	/**
	 * 
	 * @return The local frames at each point of the orbit
	 */
	public List<LocalFrame> getLocalFrames() {
		return localFramesCalculator.getLocalFrames();
	}

	/**
	 * Writes the reference frame unit vectors into a file
	 * 
	 * @param fileName  Output file name.
	 * @param increment Increment of steps while sampling the curve. The value 1
	 *                  takes all the curve points, 2 every second, 3 every third
	 *                  and so on.
	 */
	public void localFramesToFile(String fileName, int increment) {
		localFramesCalculator.writeToFile(fileName, increment);
	}

	// TODO is this still true ?
	//@formatter:off
	/**
	 * Get the previous point on the orbit compared to the point designated by the given index.
	 * It takes into account if the orbit is circular or belongs to a transfer line.
	 *  
	 *  1. In transfer lines the point with index zero is always the origin.
	 *       
	 *  2. In circular machines point with index zero is (0,0 ds) where ds is the
	 *     resolution of the orbit step size. The last point in the path is always
	 *     the origin (0,0,0).
	 * 
	 * @param index Index of the current orbit point.
	 * 
	 * @return The previous index of the orbit.
	 */
	//@formatter:on
	public Vector3D getPreviousPoint(int index) {
		int idx = index % path.size();
		if (idx > 0)
			return path.get(idx - 1);
		else {
			if (circular)
				return path.get(path.size() - 1);
			else
				return Vector3D.ZERO;
		}

	}

	/**
	 * 
	 * @return true is the orbit is circular.
	 */
	public boolean isCircular() {
		return circular;
	}

	/**
	 * The approximated length calculated by adding together the line segments of
	 * the orbit.
	 *
	 * @return the length of the orbit
	 */
	public double getOrbitLength() {
		return orbitLength;
	}

	/**
	 * Gets all the orbit points that make up the reference orbit.
	 *
	 * @return The points that make up the reference orbit.
	 */
	public List<Vector3D> getOrbitPoints() {
		return path;
	}

	/**
	 * Writes the reference orbit to a file with the given name.
	 *
	 * @param f Output file name.
	 */
	public void writeToFile(String f) {
		FileUtils.writeVectors(f, path);
	}

	/**
	 * Gets the tangent vector (position and direction vectors) on the orbit which
	 * is at a given longitudinal position.
	 *
	 * @param longiPos Longitudinal position. Distance from the start along the
	 *                 orbit[m].
	 * 
	 * @return Tangent vector at the orbit.
	 */
	public TangentVector3D getTangentVectorAt(double longiPos) {
		if (longiPos < 0 || longiPos > orbitLength)
			throw new IllegalArgumentException(
					"The longitudinal position can not be negative or bigger than the length of the orbit");
		int index = PathUtils.getClosestIndex(path, longiPos, circular);
		return PathUtils.getTangentVector(path, circular, index);
	}

	/**
	 * Gets the tangent vector (position and direction vectors) on the orbit which
	 * is at the given index of the point on the orbit.
	 *
	 * @param index Index of the point on the orbit.
	 * 
	 * @return Tangent vector at the orbit.
	 */
	public TangentVector3D getTangentVectorAt(int index) {
		return PathUtils.getTangentVector(path, circular, index);

	}

	/**
	 * Gets the longitudinal position of the point on the orbit with the given
	 * index.
	 * 
	 * @param index Index of the orbit point
	 * 
	 * @return The longitudinal position of the point on the orbit with the given
	 *         index. [meters]
	 */
	public double getLongitudinalPosition(int index) {
		if (longitudinalPositions == null)
			longitudinalPositions = PathUtils.getArcDistances(path, circular);

		return longitudinalPositions.get(index);
	}

	/**
	 * Writes the the map into a file sorted by longitudinal position with the
	 * following layout:
	 * 
	 * s[m] x[m] y[m] z[m] ElementName
	 * 
	 * s is the longitudinal position along the orbit.
	 * 
	 * @param fileName Output file.
	 * @param map      A map containing Strings as keys and 3D points as values.
	 *                 These are usually the element centers. In any case should be
	 *                 close to the reference orbit. The longitudinal position
	 *                 calculation assumes the points lie on the reference orbit.
	 */

	public void writeMaptoFileWithS(String fileName, Map<String, Vector3D> map) {

		Map<String, Vector3D> sorted = getSortedMap(map);
		StringBuilder sb = new StringBuilder();
		sb.append("#s[m] x[m] y[m] z[m] ElementName" + System.lineSeparator());

		List<Vector3D> opoints = getOrbitPoints();

		for (String eName : sorted.keySet()) {
			Vector3D v = sorted.get(eName);
			int i = PathUtils.getClosestIndex(opoints, v);
			double s = getLongitudinalPosition(i);
			sb.append(s + " " + v.getX() + " " + v.getY() + " " + v.getZ() + " " + eName + System.lineSeparator());
		}
		sb.append(System.lineSeparator());
		FileUtils.writeTextFile(fileName, sb.toString());

	}

	/**
	 * Sort the map according to orbit point index nearest to the 3D points in the
	 * map.
	 * 
	 * @param map A Map containing points on the reference orbit or close to it.
	 * @return A Sorted map by longitudinal position along the reference orbit.
	 */
	private Map<String, Vector3D> getSortedMap(Map<String, Vector3D> map) {
		List<Vector3D> opoints = getOrbitPoints();

		Map<String, Vector3D> sorted = map.entrySet().stream().sorted(Comparator.comparing(e -> {
			Vector3D v = e.getValue();
			int i = PathUtils.getClosestIndex(opoints, v);
			return getLongitudinalPosition(i);
		})).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
				LinkedHashMap::new));
		return sorted;
	}

	private boolean hasDuplicatePoints(List<Vector3D> points) {
		for (int i = 1; i < points.size(); i++) {
			if (points.get(i).subtract(points.get(i - 1)).getNorm() < 1E-12) {
				logger.warn("Duplicate points at index = " + (i - 1));

				return true;
			}
		}
		return false;
	}

}
