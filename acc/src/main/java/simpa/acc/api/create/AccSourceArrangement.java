package simpa.acc.api.create;

import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.FileNamingConventions;
import simpa.core.api.PointSource;
import simpa.core.api.SourceArrangement;

/**
 * Class holding a collection of point sources. These sources reproduce the EM
 * field of an accelerator element. It holds also the longitudinal position in
 * the beam line and the name of the group this element belongs to.
 */
public class AccSourceArrangement implements AccElement {

	private String groupName;
	private double longiPos;
	private SourceArrangement sourceArrangement;

	public AccSourceArrangement(List<PointSource> poles, double scaling, String name, String groupName,
			double longiPos) {
		sourceArrangement = new SourceArrangement(poles, scaling, name);
		this.groupName = groupName;
		this.longiPos = longiPos;
	}

	/**
	 * Constructor for an accelerator element from a solution file. The the sources
	 * from 'elementname-SOLUTION.txt' file is read. Then the sources are scaled,
	 * rotated and translated.
	 * 
	 * @param filename    File named according to the file naming
	 *                    conventions.{@link FileNamingConventions} containing the
	 *                    sources.
	 * @param translation Translation to be applied after the rotation.
	 * @param rotation    Rotation applied before the translation.
	 * @param scaling     scaling factor for the sources.
	 * @param name        Name of the element.
	 * @param groupName   Group name to which this element belongs to.
	 * @param longiPos    Longitudinal position of the element.
	 */
	public AccSourceArrangement(String filename, Vector3D translation, Rotation rotation, double scaling, String name,
			String groupName, double longiPos) {
		sourceArrangement = new SourceArrangement(filename, translation, rotation, scaling, name);
		this.groupName = groupName;
		this.longiPos = longiPos;
	}

	/**
	 * 
	 * @return Name of the element group this element belongs to. The group is
	 *         usually consist of all elements connected to the same power supply.
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * 
	 * @param groupName Name of the element group this element belongs to. The group
	 *                  is usually consist of all elements connected to the same
	 *                  power supply.
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * 
	 * @return The point sources reproducing the field of this accelerator element.
	 */
	public SourceArrangement getSourceArrangement() {
		return sourceArrangement;
	}

	/**
	 * The longitudinal position of the element in the beam line.
	 * This is the distance from the beginning of the beam line to
	 * the element. The unit is meter. This is usually set when constcting the
	 * sequence, but when an element is importted from another sequence, for example
	 * a distubance from another magnet not in this sequence, the longiPos doesn't
	 * make sense. In this case the set it to NaN.
	 * 
	 * @param longiPos Longitudinal position of the element in the beam line.
	 */
	public void setLongiPos(double longiPos) {
		this.longiPos = longiPos;
	}

	@Override
	public double getLongitudinalPosition() {
		return longiPos;
	}

	@Override
	public ElementType getType() {
		return AccElement.ElementType.SOURCE_ARRANGEMENT;
	}

	@Override
	public String getName() {
		return sourceArrangement.getName();
	}

}
