package simpa.acc.api.create;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.ExtrudedSurface;
import simpa.core.api.Profile;
import simpa.core.api.utils.FileUtils;

/**
 * Represents a vacuum chamber for one or more accelerator elements in the
 * global frame.
 * 
 * @author lbojtar
 *
 */
public class VacuumChamber {

	private List<Profile> profiles;
	private String elementName;
	private Vector3D center;
	private Profile entry;
	private Profile exit;

	/**
	 * Creates a translated and rotated instance of the given surface (defined in a
	 * local frame by the surface object) in the global frame.
	 * 
	 * @param localSurface A surface describing the vacuum chamber in the local
	 *                     frame.
	 * @param rot          Rotation to be applied.
	 * @param translation  Translation to be applied.
	 * @param elementName  The name of element this chamber belongs to.
	 * 
	 */
	public VacuumChamber(ExtrudedSurface localSurface, Rotation rot, Vector3D translation, String elementName) {
		this.center = translation; // the surface has the origin (0,0,0)
		this.elementName = elementName;

		profiles = new ArrayList<>();

		for (Profile prof : localSurface.getOrientedProfiles()) {
			profiles.add(prof.getTransformedCopy(translation, rot));
		}

		List<Profile> pl = localSurface.getOrientedProfiles();

		entry = pl.get(0);
		exit = pl.get(pl.size() - 1);
		entry = entry.getTransformedCopy(translation, rot);
		exit = exit.getTransformedCopy(translation, rot);
	}

	/**
	 * 
	 * @return The name of the accelerator element this chamber belongs to.
	 */
	public String getElementName() {
		return elementName;
	}

	/**
	 * 
	 * @return Center point in the global frame.
	 */
	public Vector3D getCenter() {
		return center;
	}

	/**
	 * 
	 * @return The oriented profiles this chamber is presented by in the global
	 *         frame.
	 */
	public List<Profile> getOrientedProfiles() {
		return profiles;
	}

	/**
	 * Writes all profiles into a file.
	 * 
	 * @param f A file name.
	 */
	public void toFile(String f) {
		StringBuffer sb = new StringBuffer();
		sb.append("#X Y Z" + System.lineSeparator());
		for (Profile p : profiles) {
			sb.append(p.toString());
		}
		FileUtils.writeTextFile(f, sb.toString());
	}

	/**
	 * 
	 * @return The profile at the beam entry of the open surface.
	 */
	protected Profile getEntry() {
		return entry;
	}

	/**
	 * 
	 * @return The profile at the beam exit of the open surface.
	 */
	protected Profile getExit() {
		return exit;
	}
}
