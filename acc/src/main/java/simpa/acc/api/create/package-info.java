/**
 * This package contains all the interfaces and classes related to the creation and setting up of components
 * to construct accelerators.
 */
package simpa.acc.api.create;