package simpa.acc.api;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.track.PhaseSpaceObserver;
import simpa.acc.api.utils.AccCalculatorUtils;
import simpa.acc.api.utils.BeamSampler;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.Particle;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.utils.CalculatorUtils;

public class Beam {

	private static final Logger logger = LogManager.getLogger(Beam.class);

	private List<Particle> particles;
	private double nomMomentum;
	private ReferenceOrbit referenceOrbit;
	private PotentialProvider potProvider;
	private String name;

	/**
	 * Low level constructor. Makes the caller fully responsible that the data is
	 * coherent and makes sense. Use this only if absolutely necessary !!
	 * 
	 * Construct a beam from a list of particles and the given nominal momentum. The
	 * nominal momentum is used during tracking by the {@link PhaseSpaceObserver} to
	 * calculate the dp/p.
	 * 
	 * @param particles   List of particles in the beam.
	 * 
	 * @param refOrbit    The reference orbit. It should be the same which was used
	 *                    to create the particles.
	 * @param potProvider The potential provider. It should be the same which was
	 *                    used to create the particles.
	 * @param name        Name of the beam. Each particle will be named using the
	 *                    beam name ad a number concatenated after. This can be
	 *                    modified later if needed. It is also used to create a
	 *                    directory in which the phase space coordinates are written
	 *                    during tracking.
	 */
	public Beam(List<Particle> particles, ReferenceOrbit refOrbit, PotentialProvider potProvider, String name) {
		this.particles = particles;
		this.referenceOrbit = refOrbit;
		// calculate the average momentum of all particles
		particles.stream().mapToDouble(p -> p.getKineticMomentum().getNorm()).average()
				.ifPresent(avg -> this.nomMomentum = CalculatorUtils.getMomentumInGeVoC(avg));

		this.potProvider = potProvider;
		this.name = name;

	}

	/**
	 * Construct a beam with the given parameters.
	 * 
	 * @param beamSampler A beam sampler object.
	 * @param nomMomentum The momentum of the beam [GeV/c]
	 * @param refOrbit    The reference orbit
	 * @param longiPos    The longitudinal position of the beam along the reference
	 *                    orbit [m]. At the moment until no RF fields are
	 *                    implemented all particles have this position initially.
	 * @param potProvider The potential provider
	 * @param mass        Mass of the particles
	 * @param charge      Charge of the particles
	 * @param nbOfSamples Number of particle in the beam
	 * @param name        Name of the beam. Each particle will be named using the
	 *                    beam name ad a number concatenated after. This can be
	 *                    modified later if needed. It is also used to create a
	 *                    directory in which the phase space coordinates are written
	 *                    during tracking.
	 * 
	 * 
	 * @throws KnownFatalException If something goes wrong.
	 */
	public Beam(BeamSampler beamSampler, double nomMomentum, ReferenceOrbit refOrbit, double longiPos,
			PotentialProvider potProvider, double mass, double charge, int nbOfSamples, String name)
			throws KnownFatalException {

		this.referenceOrbit = refOrbit;

		this.potProvider = potProvider;
		this.name = name;

		particles = new ArrayList<>();
		ParticleFactory pf = new ParticleFactory(referenceOrbit);
		List<PhaseSpaceCoordinates> l = beamSampler.sample(nbOfSamples);
		double mom = 0;
		for (int i = 0; i < nbOfSamples; i++) {
			PhaseSpaceCoordinates phs = l.get(i);
			Particle p;
			try {
				p = pf.getParticle(nomMomentum, phs, longiPos, potProvider, mass, charge);
				p.setName("p" + i);
				mom += p.getKineticMomentum().getNorm();
				particles.add(p);
			} catch (OutOfApertureException e) {
				logger.info("Particle " + i + " is outside the aperture. Skipping it.");
			}
		}
		mom /= nbOfSamples;
		this.nomMomentum = AccCalculatorUtils.getMomentumInEVoC(mom) * 1E-9; // GeV/c
		logger.info("Beam named: " + name + " created with " + particles.size() + " particles with average momentum = "
				+ this.nomMomentum + " [GeV/c]");
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Particle> getParticles() {
		return particles;
	}

	/**
	 * 
	 * @return The average momentum of the particles in the beam [GeV/c]
	 */

	public double getNominalMomentum() {
		return nomMomentum;
	}

	/**
	 * 
	 * @return Reference orbit of the sequence to which the beam belongs. This might
	 *         change when the beam is transfered from one sequence to another.
	 */
	public ReferenceOrbit getReferenceOrbit() {
		return referenceOrbit;
	}

	/**
	 * 
	 * @param refOrb Reference orbit of the sequence to which the beam belongs. This
	 *               must be updated when the beam is transfered from one sequence
	 *               to another.
	 */
	public void setReferenceOrbit(ReferenceOrbit refOrb) {
		referenceOrbit = refOrb;
	}

	/**
	 * 
	 * @return The potential provider object which is used to calculate the vector
	 *         potential in the canonical momentum .
	 */
	public PotentialProvider getPotentialProvider() {
		return potProvider;
	}

	/**
	 * 
	 * @return name of the beam
	 */
	public String getName() {
		return name;
	}

	/**
	 * Calculates the position vector in the global coordinate system calculated
	 * from the average particle positions. It can be used to determine the
	 * longitudinal position along the reference orbit. @see
	 * PathUtils#getClosestIndex getClosestIndex
	 * 
	 * @return Absolute position vector.
	 */
	public Vector3D calculateBeamPosition() {
		double x = 0;
		double y = 0;
		double z = 0;
		for (Particle p : particles) {
			x += p.getX();
			y += p.getY();
			z += p.getZ();
		}
		int n = particles.size();
		return new Vector3D(x / n, y / n, z / n);
	}

	/**
	 * 
	 * @return A Deep copy of of all particles in the beam, but the tracking state
	 *         is freshly created. The vector potential updated according to the
	 *         potential provider. Creates the copy with the given potential
	 *         provider.
	 */
	public Beam copy(Beam b, PotentialProvider pp) {
		List<Particle> l = new ArrayList<>();
		for (Particle p : b.getParticles()) {
			l.add(p.copy(p, pp));
		}

		Beam c = new Beam(l, b.getReferenceOrbit(), pp, b.getName());

		return c;
	}

}
