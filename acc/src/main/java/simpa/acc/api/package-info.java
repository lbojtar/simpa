/**
 * This package contains the public interfaces and classes of the simpa.acc library.
 */
package simpa.acc.api;