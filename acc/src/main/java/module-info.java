/**
 * Library containing the accelerator physics related extension to the SIMPA project,
 * requires the simpa.core library to function.
 */
module simpa.acc {
	
	exports simpa.acc.api;
	exports simpa.acc.api.utils;
	exports simpa.acc.api.create;
	exports simpa.acc.api.track;
	exports simpa.acc.api.match;
		
	requires transitive simpa.core;
	requires transitive commons.math3;
    requires transitive cli;

	requires org.apache.logging.log4j;
	requires org.apache.logging.log4j.core;
	
	opens simpa.acc.impl to org.junit.jupiter;

}