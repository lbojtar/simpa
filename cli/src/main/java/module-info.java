/**
 * Simple CLI module for the creation of a CLI for Java projects using annotations.
 */
module cli {
    exports cli.api;
    requires org.apache.logging.log4j;
    requires jdk.jshell;
   
}