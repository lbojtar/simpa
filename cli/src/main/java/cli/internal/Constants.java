/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.internal;

import java.util.ArrayList;
import java.util.List;

public class Constants {
	
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";

	
    protected static final String printKeyword = "print";
    protected static final String printVarsKeyword = "printvars";
    protected static final String helpKeyword = "help";
     protected static final String executeKeyword = "execute";
    protected static final ArrayList<String> exitKeywords = new ArrayList<>(List.of(new String[]{"q", "Q", "exit"}));
    protected static final ArrayList<String> mathFunctions = new ArrayList<>(List.of(new String[]{"sqrt", "sin", "cos", "tan"}));
    protected static final char[] commentSymbols = {'#', '!'};
    public static final String helpOutputFormat = Constants.ANSI_CYAN+"%-32s%-20s%-10s"+Constants.ANSI_RESET+"%n%s";
    protected static final String strRegex = "\"(.*?)\"";
    public static final String intRegex = "-?\\d+";
    protected static final String varRegex = "^[a-zA-Z0-9_-]*$";

    //REGEX
    static final String Digits = "(\\p{Digit}+)";
    static final String HexDigits = "(\\p{XDigit}+)";
    // an exponent is 'e' or 'E' followed by an optionally
    // signed decimal integer.
    static final String Exp = "[eE][+-]?" + Digits;
  
    public static final String fpRegex =
            ("[\\x00-\\x20]*" +
                    "[+-]?(" +
                    "NaN|" +
                    "Infinity|" +
                    "(((" + Digits + "(\\.)?(" + Digits + "?)(" + Exp + ")?)|" +
                    "(\\.(" + Digits + ")(" + Exp + ")?)|" +
                    "((" +
                    "(0[xX]" + HexDigits + "(\\.)?)|" +

                    "(0[xX]" + HexDigits + "?(\\.)" + HexDigits + ")" +

                    ")[pP][+-]?" + Digits + "))" +
                    "[fFdD]?))" +
                    "[\\x00-\\x20]*");
}
