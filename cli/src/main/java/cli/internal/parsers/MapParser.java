/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.internal.parsers;

import cli.api.CliState;
import cli.internal.types.Variable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MapParser {

    private static final Logger logger = LogManager.getLogger(MapParser.class);

    public static HashMap<Object, Object> read(ArrayList<String> data) throws IOException {
        HashMap<Object, Object> map = new HashMap<>();
        appendValues(map, data);

        return map;
    }

    private static void appendValues(HashMap<Object, Object> map, ArrayList<String> data) throws IOException {
        for (String s : data) {
            s = s.trim();
            if (!s.startsWith("{") || !s.endsWith("}")) {
                throw new IllegalArgumentException("Map values should be written as {key:value} separated by commas : " + s);
                
            }
            String[] items = s.replace("{", "")
                    .replace("}", "")
                    .split(":");

            Class<?> keyClass = TypeParser.getType(items[0]);
            Class<?> valueClass = TypeParser.getType(items[1]);

            // If the object is a variable, get it from the state, otherwise cast it to its
            // correct value
            Object key = keyClass == Variable.class ? CliState.getInstance().getVariable(items[0])
                    : TypeParser.castValue(keyClass, items[0]);

            Object entry = valueClass == Variable.class ? CliState.getInstance().getVariable(items[1])
                    : TypeParser.castValue(valueClass, items[1]);

            if (key == null)
                logger.error("Key is null :" + s);
            if (entry == null)
                logger.error("Entry is null :" + s);

            map.put(key, entry);
        }
    }
}
