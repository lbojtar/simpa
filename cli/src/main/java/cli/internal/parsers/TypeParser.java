/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.internal.parsers;

import cli.internal.Constants;
import cli.internal.types.Array;
import cli.internal.types.Variable;

import java.io.IOException;
import java.util.regex.Pattern;

public class TypeParser {

	public static Class<?> getType(String value) {
		value = value.trim();
		if (value.equals("true") || value.equals("false")) {
			return Boolean.class;
		} else if (value.startsWith("[") && value.endsWith("]")) {
			return Array.class;
		} else if ((value.startsWith("\"") && value.endsWith("\""))) {
			return String.class;
		} else if (Pattern.matches(Constants.intRegex, value)) {
			return Integer.class;
		} else if (Pattern.matches(Constants.fpRegex, value)) {
			return Double.class;
		} else {
			return Variable.class;
		}
	}

	public static Object castValue(Class<?> clazz, Object var) throws IOException {
		if (clazz.equals(Double.class) || clazz.equals(double.class)) {
			return Double.parseDouble(var.toString());
		} else if (clazz.equals(Integer.class) || clazz.equals(int.class)) {
			return Integer.parseInt(var.toString());
		} else if (clazz.equals(Long.class) || clazz.equals(long.class)) {
			return Long.parseLong(var.toString());
		} else if (clazz.equals(Boolean.class) || clazz.equals(boolean.class)) {
			return Boolean.parseBoolean(var.toString());
		} else if (clazz.equals(String.class)) {
			return var.toString().replace("\"", "");
		} else if (clazz.equals(Array.class)) {
			return ArrayParser.read(var.toString(), null);
		} else {
			return clazz.cast(var);
		}
	}

	
}
