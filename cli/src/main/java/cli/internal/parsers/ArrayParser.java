/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.internal.parsers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import cli.api.CliState;

import java.lang.reflect.Array;

public class ArrayParser {

	public static Object read(String arrayString, BufferedReader br) throws IOException {
		arrayString = arrayString.trim();
		arrayString = arrayString.substring(1).replace(" ", "");

		boolean arrayFinished = arrayString.lastIndexOf(']') == arrayString.length() - 1;

		if (arrayFinished) {
			arrayString = arrayString.substring(0, arrayString.length() - 1);
		}

		if (!arrayFinished && br != null) {
			String input;
			StringBuilder arrayStringBuilder = new StringBuilder(arrayString);
			while (!(input = br.readLine()).contains("]")) {
				arrayStringBuilder.append(input);
			}
			input = input.replace("]", "");
			arrayStringBuilder.append(input);
			arrayString = arrayStringBuilder.toString();
		}

		// Split on comma's that are not in inner arrays
		ArrayList<String> arr = new ArrayList<>(Arrays.asList(arrayString.split("[,]+(?![^\\[]*])")));

		arr.removeIf(item -> item == null || "".equals(item));

		if (arr.get(0).startsWith("{") && arr.get(0).endsWith("}"))
			return MapParser.read(arr);

		int arrSize = arr.size();
		Object[] array = new Object[arrSize];

		for (int i = 0; i < arrSize; i++) {
			String s = arr.get(i);
			if (CliState.getInstance().getVariables().containsKey(s)) {
				array[i] = CliState.getInstance().getVariable(s); //already casted when stored
			} else {
				try {
					array[i] = TypeParser.castValue(TypeParser.getType(s), s);
				} catch (ClassCastException e) {
					throw new IllegalArgumentException(e.getMessage()+" Invalid type for array element: " + s);
				}
			}
		}

		return castArray(array);
	}

	/**
	 * Cast the input Object array to a concrete type.
	 * <p>
	 * - If all elements in the input array are Double it will return a Double
	 * array.
	 * </p>
	 * <p>
	 * - If all elements in the array are Integers, it will return an Integer array.
	 * </p>
	 * <p>
	 * - In any other case returns the original Object array
	 * </p>
	 * 
	 * 
	 * @param oArray An input array of Objects.
	 * @return An array of Double, Integer or Object.
	 */
	public static Object castArray(Object[] oArray) {

		// check if all elements are the same type
		Class<?> type = oArray[0].getClass();
		boolean allSameType = true;
		for (int i = 1; i < oArray.length; i++) {
			if (oArray[i].getClass() != type) {
				type = Object.class;
				allSameType = false;
				break;
			}
		}
		// mixed array
		if (!allSameType)
			return oArray;

		// all elements are the same type, this return a concrete array type
		Object array = Array.newInstance(type, oArray.length);
		for (int i = 0; i < oArray.length; i++) {
			Array.set(array, i, oArray[i]);
		}
		return array;
	}
}
