/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.internal.parsers;

import java.io.IOException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cli.api.Command;
import cli.api.CommandParameter;
import cli.api.CliState;
import cli.internal.Constants;
import cli.internal.types.Array;
import cli.internal.types.Variable;

public class ParameterParser {

	/**
	 * Generates the help instructions based on the metadata and parameters.
	 *
	 * @param m        method that should be explained
	 * @param metadata metadata of the method @see{@link Command}
	 * @return string with the help output
	 */
	public static String generateHelp(Method m, Command metadata) {
		StringBuilder output = new StringBuilder();
		output.append(Constants.ANSI_GREEN+"Usage: ").append(metadata.key()).append(" [OPTIONS]..."+Constants.ANSI_RESET).append(System.lineSeparator());
		output.append(metadata.description().replace("\t", "")).append(System.lineSeparator());

		Parameter[] params = m.getParameters();

		for (Parameter p : params) {
			CommandParameter cp = p.getAnnotation(CommandParameter.class);

			String dataType = p.getType().getSimpleName();
			String req = cp.required() ? "required" : "";
			output.append(String.format(Constants.helpOutputFormat, Arrays.toString(cp.keys()), dataType, req,
				 cp.help().replace("\t", "") + System.lineSeparator()));
		}

		return output.toString();
	}


	/**
	 * Generates the help of all commands in a simple HTML notation for publishing
	 * on a website.
	 *
	 * @param m        method to generate help for
	 * @param metadata command annotation of above method
	 * @return a string with the generated HTML
	 */
	public static String generateHTMLHelp(Method m, Command metadata) {
		StringBuilder output = new StringBuilder();
		output.append("<p>Usage: <code>").append(metadata.key()).append(" [OPTIONS]...</code></p>")
				.append(System.lineSeparator());
		output.append("<p>").append(metadata.description()).append("</p>").append(System.lineSeparator());
		Parameter[] params = m.getParameters();

		output.append("<ul>").append(System.lineSeparator());
		for (Parameter p : params) {
			CommandParameter cp = p.getAnnotation(CommandParameter.class);

			String dataType = p.getType().getSimpleName();
			String req = cp.required() ? "required, " : "";
			output.append("<li>").append(Arrays.toString(cp.keys())).append(", ").append(dataType).append(", ")
					.append(req).append(cp.help()).append("</li>").append(System.lineSeparator());
		}

		output.append("</ul>").append(System.lineSeparator()).append("<p>&nbsp;</p>");

		return output.toString();
	}

	/**
	 * Get method parameters annotated with @see{@link CommandParameter}.
	 *
	 * @param params parameters of the method
	 * @return list of CommandParameter annotations.
	 */
	public static ArrayList<CommandParameter> getCommandParameters(Parameter[] params) {
		// add parameter if there is no key.
		ArrayList<CommandParameter> commandParameters = new ArrayList<>();

		for (Parameter p : params) {
			commandParameters.add(p.getAnnotation(CommandParameter.class));
		}

		return commandParameters;
	}

	/**
	 * Processes the arguments that are given and checks whether all the required
	 * arguments are present.
	 *
	 * @param args list of string that are the given arguments
	 * @param m    method to process
	 * @return list of arguments to be given to method
	 */
	public static List<Object> processArguments(ArrayList<String> args, Method m) throws IOException {
		Parameter[] params = m.getParameters();

		List<Object> parameters = new ArrayList<>();

		List<String[]> missingKeys = new ArrayList<>();

		for (Parameter p : params) {
			CommandParameter cp = p.getAnnotation(CommandParameter.class);

			String[] keys = cp.keys();

			Class<?> clazz = p.getType();

			int keyIndex = -1;

			for (String key : keys) {
				int index = args.indexOf(key);
				if (index >= 0) {
					keyIndex = index;
					break;
				}
			}

			if (keyIndex >= 0) {

				// Check data type: if it's a boolean or number we need to convert
				if (args.get(keyIndex + 1) == null) {
					throw new IllegalArgumentException("Missing argument for the flag " + Arrays.toString(keys));
				}

				String value = args.get(keyIndex + 1);
				Object var;

				Class<?> type = TypeParser.getType(value);

				if (type == Array.class) {
					var = ArrayParser.read(value, null);
				} else if (type == Variable.class) {
					Object variable = CliState.getInstance().getVariable(value);
					if (variable != null) {
						var = TypeParser.castValue(clazz, variable);
					} else {
						throw new IllegalArgumentException("Variable " + value + " not found.");
					}
				} else {
					var = TypeParser.castValue(clazz, value);
				}

				parameters.add(var);

				args.remove(keyIndex + 1);
				args.remove(keyIndex);
			} else if (cp.required()) {
				missingKeys.add(keys);
			} else {
				parameters.add(null);
			}
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < args.size(); i++) {
			sb.append("Unrecognized parameter: " + args.get(i) + " " + args.get(i + 1) + System.lineSeparator());
			i++;
		}
		if (!sb.isEmpty())
			throw new IllegalArgumentException(sb.toString());

		if (missingKeys.size() > 0) {
			StringBuilder missing = new StringBuilder();
			for (String[] keys : missingKeys) {
				missing.append(System.lineSeparator()).append("\t- ").append(Arrays.toString(keys));
			}
			throw new IllegalArgumentException("Please add parameter(s) with key(s): " + missing);
		}

		return parameters;
	}
}
