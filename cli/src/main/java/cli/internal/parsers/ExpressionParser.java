package cli.internal.parsers;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cli.api.CliState;
import jdk.jshell.JShell;
import jdk.jshell.Snippet;
import jdk.jshell.SnippetEvent;
import jdk.jshell.execution.LocalExecutionControlProvider;

public class ExpressionParser {

    static JShell js;

    private static ExpressionParser parser;

    public static ExpressionParser getInstance() {
        return parser == null ? parser = new ExpressionParser() : parser;
    }

    public ExpressionParser() {
        LocalExecutionControlProvider lexp = new LocalExecutionControlProvider();
        js = JShell.builder().executionEngine(lexp, null).build();
        // static imports
        js.eval("import static java.lang.Math.*;");

    }


    public  String eval(String input) {

        int depth = 0;
        Integer start = null;
        boolean string = false;

        for (int i = 0; i < input.length(); i++) {
            char current = input.charAt(i);
            if (current == '(' && !string) {
                if (depth == 0) start = i;
                depth++;
            } else if (current == ')' && depth > 0 && !string) {
                depth--;

                if (depth == 0) {
                    String expression = input.substring(start, i+1);
                    int expressionLength = expression.length();
                    Set<String> partsSet = new HashSet<>();
                    Collections.addAll(partsSet, expression.split("[*/+\\-%()]"));

                    partsSet.removeIf(String::isEmpty);
                    // sorts the values from big to small to make sure that variables are replaced safely
                    String[] parts = partsSet.toArray(new String[0]);
                    Arrays.sort(parts, Comparator.comparingInt(String::length).reversed());

                    Set<String> keyset = CliState.getInstance().getVariables().keySet();

                    for (String part: parts) {
                        if (keyset.contains(part)) {
                            expression = expression.replace(part, CliState.getInstance().getVariable(part).toString());
                        } else if (TypeParser.getType(part) == Double.class) {
                            expression = expression.replace(part, String.valueOf(Double.parseDouble(part)));
                        }
                    }

                    String result = evalSubstituted(expression);

                    input = input.replace(input.substring(start, i+1), result.toString());

                    //code to make sure the cursor is at the end of the last result
                    i = i - expressionLength + result.toString().length();

                    start = null;
                }
            } else if (current == '"') {
                string = !string;
            }
        }

        return input;
    }

    private String evalSubstituted(String input) {

        List<SnippetEvent> events = js.eval(input);
        if (events.size() != 1)
            throw new RuntimeException(input + " Is not a single expression");
        SnippetEvent ev = events.get(0);

        if (ev.exception() != null)
            throw new RuntimeException(ev.exception());

        if (ev.status() == Snippet.Status.VALID)
            return ev.value();

        else
            return input; // can't interpret it, we do nothing
    }

}
