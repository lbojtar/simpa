/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.internal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class ExceptionHandler {
    private final static Logger logger = LogManager.getLogger(ExceptionHandler.class);

    public static String handleException(Exception e) {
        String message = "";
        if (e instanceof InstantiationException) {
            message = "Could not create instance of requested class." + System.lineSeparator() + e.getMessage();
        } else if (e instanceof IllegalAccessException) {
            message = "Could not access method." + System.lineSeparator() + e.getMessage();
        } else if (e instanceof NoSuchMethodException) {
            message = "Method could not be found." + System.lineSeparator() + e.getMessage();
        } else if (e instanceof ClassCastException) {
            message = "Unable to cast class." + System.lineSeparator() + e.getMessage();
        } else {
            if (e.getMessage() != null) {
                message = e.getMessage() + " ";
                logger.debug(Arrays.toString(e.getStackTrace()));
            } else if (e.getCause().getMessage() != null) {
                message = e.getCause().getMessage() + " ";
                logger.debug(Arrays.toString(e.getCause().getStackTrace()));
            } else {
                message = "An unknown error happened. Please consult the log file for more information.";                
                e.printStackTrace();
            }
        }
        return message;
    }
}
