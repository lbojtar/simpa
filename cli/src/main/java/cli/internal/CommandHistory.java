/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.internal;

import java.io.*;
import java.util.Stack;

/**
 * Holds the history of executed commands, can be used to write the history to a file.
 */
public class CommandHistory {
    private static CommandHistory instance;
    private final Stack<String> history;

    private CommandHistory() {
        history = new Stack<>();
    }

    public static CommandHistory getInstance() {
        if (instance == null) {
            instance = new CommandHistory();
        }
        return instance;
    }

    public void addToHistory(String command) {
        history.add(command);
    }

    public void writeHistoryToFile(String filename) throws IOException {
        StringBuilder sb = new StringBuilder();
        BufferedWriter outputStream = new BufferedWriter(new FileWriter(filename, false));
        for (String cmd : history) {
            sb.append(cmd).append(System.lineSeparator());
        }
        String str = sb.toString();
        outputStream.write(str, 0, str.length());
        outputStream.close();
    }
}
