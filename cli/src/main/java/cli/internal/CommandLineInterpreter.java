/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.internal;

import static cli.internal.Constants.varRegex;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cli.api.CliState;
import cli.api.Command;
import cli.api.CommandInterpreter;
import cli.internal.parsers.ArrayParser;
import cli.internal.parsers.ExpressionParser;
import cli.internal.parsers.ParameterParser;
import cli.internal.parsers.TypeParser;
import java.io.FileWriter;

/**
 * Class that contains the main code for the execution of the CLI.
 */

public class CommandLineInterpreter implements CommandInterpreter {
	private static final Logger logger = LogManager.getLogger(CommandLineInterpreter.class);

	private final Class<?>[] cls;
	private final TreeMap<String, Method> commands = new TreeMap<>();
	private final HashMap<Class<?>, Object> instances = new HashMap<>();
	private PrintStream out = new PrintStream(System.out, true, StandardCharsets.UTF_8);
	private String shellPrefix = "";
	private String description = "";
	private boolean interactive = false;
	private boolean fileLoop = false;
	private String lastCommand;

	public CommandLineInterpreter(Class<?>[] cls, String shellPrefix, String description) {
		this.cls = cls;
		this.shellPrefix = shellPrefix;
		this.description = description;
		initialiseAnnotationCommands();
	}

	public CommandLineInterpreter(Class<?>[] cls, String shellPrefix) {
		this.cls = cls;
		this.shellPrefix = shellPrefix;
		initialiseAnnotationCommands();
	}

	public CommandLineInterpreter(Class<?>[] cls) {
		this.cls = cls;
		initialiseAnnotationCommands();
	}

	@Override
	public void command(String command) throws Exception {
		BufferedReader br = new BufferedReader(new StringReader(command));
		interactive = true;
		processCommandLoop(br);

	}

	@Override
	public void start() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));

			interactive = true;

			run(br);

			br.close();
		} catch (Exception e) {
			logger.error(ExceptionHandler.handleException(e));
		}
	}

	@Override
	public void start(BufferedReader br) throws IOException {
		interactive = true;
		run(br);
		br.close();

	}

	@Override
	public void startFromFile(String filename, boolean debugMode) {
		try {
			runFile(filename, debugMode);
		} catch (Exception e) {
			logger.error(ExceptionHandler.handleException(e));
			System.exit(-1);
		}
	}

	/**
	 * Runs the interpreter using a file with commands.
	 *
	 * @param filename  The name of the file containing the scripts.
	 * @param debugMode Whether debug messages should be displayed.
	 * @throws IOException If there is a problem with loading the file.
	 */
	public void runFile(String filename, boolean debugMode) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(filename));

		String input;
		int line = 1;

		fileLoop = true;

		while ((input = br.readLine()) != null && !Constants.exitKeywords.contains(input) && fileLoop) {
			try {
				input = input.trim();
				input = removeInlineComments(input);

				if (isValidLine(input)) {
					if (debugMode)
						out.println(input);
					handleInput(input, br);
				}

				line++;
			} catch (Exception e) {
				String errString = "Error at command: \"" + input + "\", at line " + line + ", in file \"" + filename
						+ "\".";
				String output = ExceptionHandler.handleException(e);
				if (!output.isEmpty())
					errString += ":" + System.lineSeparator() + "\t- " + output;
				logger.error(errString);

				if (!interactive) {
					out.println("Exit...");
					System.exit(-1);
				}

				fileLoop = false;
			}
		}

		br.close();
	}

	/**
	 * Runs the interpreter based on a BufferedReader supplying the CLI with input.
	 *
	 * @param br BufferedReader, can be a InputStreamReader or FileReader
	 */
	private void run(BufferedReader br) throws IOException {

		try {
			out.print(shellPrefix);
			processCommandLoop(br);
		} catch (Exception e) {
			String errString = "Error at command: \"" + lastCommand + "\".";
			String output = ExceptionHandler.handleException(e);
			if (!output.isEmpty())
				errString += ":" + System.lineSeparator() + "\t- " + output;
			logger.error(errString);

			out.print(shellPrefix);
		}

	}

	private void processCommandLoop(BufferedReader br) throws Exception {

		while ((lastCommand = br.readLine()) != null && !Constants.exitKeywords.contains(lastCommand)) {

			lastCommand = lastCommand.trim();
			lastCommand = removeInlineComments(lastCommand);

			if (isValidLine(lastCommand)) {
				handleInput(lastCommand, br);
			}

			// This adds the prefix BEFORE the next line is read.
			out.print(shellPrefix);

		}
	}

	private String removeInlineComments(String input) {
		ArrayList<Integer> quoteIndexes = new ArrayList<>();
		ArrayList<Integer> commentIndexes = new ArrayList<>();

		for (int i = 0; i < input.length(); i++) {
			char current = input.charAt(i);
			if (current == "\"".charAt(0)) {
				quoteIndexes.add(i);
			} else {
				for (char c : Constants.commentSymbols) {
					if (c == current) {
						if (i == 0) {
							// complete line is a comment, return empty string
							return "";
						}
						commentIndexes.add(i);
					}
				}
			}
		}

		if (quoteIndexes.size() % 2 != 0) {
			throw new IllegalArgumentException("Unclosed string.");
		}

		for (int i : commentIndexes) {
			boolean inString = false;
			for (int j = 0; j < quoteIndexes.size(); j += 2) {
				if (i > quoteIndexes.get(j) && i < quoteIndexes.get(j + 1)) {
					inString = true;
					break;
				}
			}
			if (!inString) {
				input = input.substring(0, i);
				break;
			}
		}

		return input;
	}

	/**
	 * Checks if the line is a comment or is empty.
	 *
	 * @param input line to check
	 * @return boolean that tells if the line is valid and should be handled and
	 *         otherwise ignored
	 */
	private boolean isValidLine(String input) {
		return !input.isEmpty() && !input.startsWith("#");
	}

	/**
	 * Handles the input.
	 *
	 * @param input line to handle
	 */
	private void handleInput(String input, BufferedReader br) throws Exception {
		logger.debug("Executing command: " + input);

		boolean store = false;
		String varName = "";
		String commandString;
		int assignmentIndex = input.indexOf("=");

		input = input.trim().replace("\t", " ").replaceAll(" +", " ");

		// evaluate expressions in the input
		input = ExpressionParser.getInstance().eval(input);

		if (assignmentIndex != -1) {
			varName = input.substring(0, assignmentIndex).trim().replace(" ", "");
			commandString = input.substring(assignmentIndex + 1).trim();
			store = true;
		} else {
			commandString = input;
		}

		// check for arrays
		if (store && commandString.startsWith("[")) {
			Object arr = ArrayParser.read(commandString, br);
			CliState.getInstance().addVariable(varName, arr);
			return;
		}

		// commandString = ExpressionParser.getInstance().read(commandString);

		// Code that allows users to use double quotes for indicating string with
		// spaces.
		ArrayList<String> args = getArgs(commandString);

		String command = args.remove(0);

		handleCommands(command, args, store, varName);
		CommandHistory.getInstance().addToHistory(input);
	}

	private ArrayList<String> getArgs(String commandString) {
		ArrayList<String> args = new ArrayList<>();
		int arrayDepth = 0;
		boolean inString = false;
		int start = 0;

		for (int i = 0; i < commandString.length(); i++) {
			char current = commandString.charAt(i);
			if (current == ' ' && arrayDepth == 0 && !inString) {
				args.add(commandString.substring(start, i).trim());
				start = i + 1;
			} else if (current == '[' && !inString) {
				arrayDepth++;
			} else if (current == ']' && !inString) {
				arrayDepth--;
			} else if (current == '"') {
				inString = !inString;
			}

			if (i == commandString.length() - 1) {
				args.add(commandString.substring(start, i + 1).trim());
			}
		}

		args.removeIf(item -> item == null || "".equals(item));
		return args;
	}

	private void initialiseAnnotationCommands() {
		logger.debug("Initializing annotation commands.");

		for (Class<?> c : cls) {
			Method[] methods = c.getMethods();

			for (Method m : methods) {
				Command metadata = m.getAnnotation(Command.class);
				if (metadata != null) {
					String commandName = (metadata.key().isBlank() || metadata.key().isEmpty()) ? m.getName()
							: metadata.key();
					commands.put(commandName, m);
				}
			}
		}

	}

	private void handleCommands(String command, ArrayList<String> args, boolean store, String varName)
			throws Exception {
		if (store) {
			validateVarName(varName);
		}

		Method m = commands.get(command);

		if (m != null) {
			handleAnnotationCommand(m, args, store, varName);
			return;
		}

		switch (command) {
			case Constants.printKeyword -> {
				printVariable(args);
				return;
			}
			case Constants.printVarsKeyword -> {
				printAllVariables();
				return;
			}
			case Constants.executeKeyword -> {
				try {
					StringBuilder sb = new StringBuilder();
					args.stream().forEach(c -> sb.append(c + " "));
					logger.info("Executing external command: " + sb.toString());
					Process pr = Runtime.getRuntime().exec(sb.toString());
					int returnCode = pr.waitFor();
					if (returnCode != 0)
						logger.error(
								"The external command: " + sb.toString() + " returned an error code: " + returnCode);
				} catch (IOException e) {
					logger.error("Error while executing command: " + command + " " + e.toString());
				}
				return;
			}
			case Constants.helpKeyword -> {
				if (args.size() > 0 && args.get(0) != null) {
					Set<String> commandKeys = commands.keySet();
					ArrayList<String> foundKeys = new ArrayList<>();

					for (String c : commandKeys) {
						if (c.contains(args.get(0)))
							foundKeys.add(c);
					}

					if (foundKeys.size() > 0) {
						out.println("Commands found with search:" + System.lineSeparator());
						for (String c : foundKeys) {
							Method commandMethod = commands.get(c);
							out.println(ParameterParser.generateHelp(commandMethod,
									commandMethod.getAnnotation(Command.class)));
						}
					}

				} else {
					printHelp();
				}
				return;
			}
			case "help-html" -> {
				printHtmlHelp();
				return;
			}
			case "call" -> {
				logger.debug("Calling file");
				if ((args.contains("-f") || args.contains("--file"))) {
					String filename = args.get(1).replace("\"", "");
					boolean debug = args.contains("-d") || args.contains("--debug");
					runFile(filename, debug);
				} else {
					throw new IllegalArgumentException("No file given for call");
				}
				return;
			}
			case "write-script" -> {
				logger.debug("Writing script from history");
				if ((args.contains("-f") || args.contains("--file"))) {
					String filename = args.get(1).replace("\"", "");
					CommandHistory.getInstance().writeHistoryToFile(filename);
				} else {
					throw new IllegalArgumentException("No output file given");
				}
				return;
			}
		}

		if (store) {
			logger.debug("Storing variable as " + varName);
			if (command.matches(Constants.strRegex) || Pattern.matches(Constants.fpRegex, command)
					|| command.equals("true") || command.equals("false")) {
				storeVariable(varName, command);
				return;
			}
		}

		logger.error("Command \"" + command + "\" not found.");
	}

	/**
	 * Validates the variable name.
	 *
	 * @param varName the variable name.
	 */
	private void validateVarName(String varName) {
		if ((commands.containsKey(varName) || Constants.exitKeywords.contains(varName)
				|| Constants.printKeyword.equals(varName)) || Constants.helpKeyword.equals(varName)
				|| Constants.mathFunctions.contains(varName)) {
			throw new IllegalArgumentException(varName + " is a reserved keyword");
		}

		if (!varName.matches(varRegex)) {
			throw new IllegalArgumentException(
					varName + " can't be used as a variable name, please use only letters and numbers.");
		}
	}

	/**
	 * Method that prints the help info for all commands
	 */
	private void printHelp() {
		// TODO: add explanation about default commands and maybe allow a title and
		// version to be added.
		if (!description.isEmpty())
			out.println(description);
		out.println("Appending a command with the -h flag will print the description and parameters for the command.");
		commands.forEach(
				(key, value) -> out.println(ParameterParser.generateHelp(value, value.getAnnotation(Command.class))));
	}

	private void printHtmlHelp() {
		StringBuffer sb = new StringBuffer();
		if (!description.isEmpty())
			sb.append(description + System.lineSeparator());
		sb.append("Appending a command with the -h flag will print the description and parameters for the command."
				+ System.lineSeparator());
		commands.forEach((key, value) -> sb.append(
				ParameterParser.generateHTMLHelp(value, value.getAnnotation(Command.class)) + System.lineSeparator()));
		String s = sb.toString();
		String file="help.html";
		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, false));
			outputStream.write(s, 0, s.length());
			outputStream.close();

		} catch (IOException ex) {
			logger.error("Something went wrong writing text to file: " + file);
			logger.debug(Arrays.toString(ex.getStackTrace()));
		}
		logger.info("Wrote text file: " + file);
	}

	/**
	 * Prints all variables given as arguments
	 *
	 * @param args given arguments
	 */
	private void printVariable(ArrayList<String> args) {
		for (String arg : args) {
			Object var = CliState.getInstance().getVariable(arg);

			if (var != null) {
				if (var.getClass().isArray()) {
					out.println(Arrays.toString((Object[]) var));
				} else if (var.getClass() == HashMap.class) {
					HashMap<Object, Object> map = (HashMap<Object, Object>) var;
					out.println("{");
					for (Object i : map.keySet()) {
						Object val = map.get(i);
						out.println("\t\"" + i + "\": "
								+ (val.getClass().isArray() ? Arrays.toString((Object[]) val) : val));
					}
					out.println("}");
				} else {
					out.println(var);
				}
			} else {
				logger.error("Object does not exist.");
			}
		}
	}

	private void printAllVariables() {
		out.println(CliState.getInstance().listVariables());
	}

	/**
	 * Stores a variable to the @see{@link CliState} instance.
	 *
	 * @param varName name for the variable
	 * @param value   value to save
	 */
	private void storeVariable(String varName, String value) throws IOException {
		Class<?> type = TypeParser.getType(value);
		Object var = TypeParser.castValue(type, value);
		CliState.getInstance().addVariable(varName, var);
	}

	/**
	 * Handles the case where a command is used that is in the list of annotated
	 * methods.
	 *
	 * @param m    the annotated method
	 * @param args the arguments to pass through
	 */
	private void handleAnnotationCommand(Method m, ArrayList<String> args, boolean store, String varName)
			throws Exception {

		if (args.size() > 0) {
			Command metadata = m.getAnnotation(Command.class);
			switch (args.get(0)) {

				case "-h", "--help" -> {
					out.println(ParameterParser.generateHelp(m, metadata));
					return;
				}
				default -> {
				}
			}
		}

		Class<?> clazz = m.getDeclaringClass();
		Object obj;
		if (instances.containsKey(clazz)) {
			obj = instances.get(clazz);
		} else {
			obj = clazz.getDeclaredConstructor().newInstance();
			instances.put(clazz, obj);
		}

		List<Object> arguments = ParameterParser.processArguments(args, m);

		// Call method with arguments
		Object returnValue = m.invoke(obj, arguments.toArray());

		if (store) {
			CliState.getInstance().addVariable(varName, returnValue);
		}
	}

}
