/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.api;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import cli.internal.parsers.TypeParser;

/**
 * Class that holds the state for the CLI. Uses a singleton pattern.
 */
public class CliState {

	private static CliState instance;	
	private final HashMap<String, Object> variables;

	private CliState() {
		variables = new HashMap<>();
		// Pre-defined constants, we can't create these from simpa script, because the
		// ArrayParser has no way to figure out the type
		variables.put("EMPTY_DOUBLE_ARRAY", new Double[0]);
		variables.put("EMPTY_STRING_ARRAY", new String[0]);
		variables.put("EMPTY_INTEGER_ARRAY", new Integer[0]);
		variables.put("EMPTY_BOOLEAN_ARRAY", new Boolean[0]);
	}

	public static CliState getInstance() {
		if (instance == null) {
			instance = new CliState();
		}
		return instance;
	}

	/**
	 * Adds a variable to the state. If it is a String the type will be checked and
	 * converted if necessary.
	 * 
	 * @param name   Name of the variable
	 * @param object Value of the variable
	 */
	public void addVariable(String name, Object object) {
		if (object instanceof String) {
			try {
				Object cob = TypeParser.castValue(object.getClass(), object);
				variables.put(name, cob);
			} catch (Exception e) {
				throw new IllegalArgumentException(
						"Invalid type for variable " + name + ": " + object.getClass().getSimpleName());
			}
		} else
			variables.put(name, object);

	}

	/**
	 * @param name Name of the variable
	 * @return Value of the variable
	 */
	public Object getVariable(String name) {
		return variables.get(name);
	}

	public String listVariables() {
		StringBuilder str = new StringBuilder();
		for (String key : variables.keySet()) {
			Object var = variables.get(key);
			String varString = var.getClass().isArray() ? Arrays.toString((Object[]) var) : var.toString();
			str.append(key).append(": ").append(varString).append(System.lineSeparator());
		}
		return str.toString();
	}

	public Map<String, Object> getVariables() {
		return variables;
	}

	
}
