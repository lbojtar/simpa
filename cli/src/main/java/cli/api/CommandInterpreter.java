/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package cli.api;

import java.io.BufferedReader;
import java.io.PrintStream;

import cli.internal.CommandLineInterpreter;

public interface CommandInterpreter {

    /**
     * Creates a new CommandInterpreter based on the package and sets the shell
     * prefix.
     *
     * @param cls         class to scan for methods with the @see{@link Command}
     *                    annotation
     * @param shellPrefix the prefix shown at the start of a terminal line e.g.
     *                    GooseCLI=>
     * @return new CommandInterpreter
     */
    static CommandInterpreter create(Class<?>[] cls, String shellPrefix) {
        return new CommandLineInterpreter(cls, shellPrefix);
    }

    /**
     * Creates a new CommandInterpreter based on the package.
     *
     * @param cls class to scan for methods with the @see{@link Command} annotation
     * @return new CommandInterpreter
     */
    static CommandInterpreter create(Class<?>[] cls) {
        return new CommandLineInterpreter(cls);
    }

    /**
     * Starts the command line interpreter using the System.in and System.out
     * streams.
     */
    void start();

    /**
     * Executes a single command and returns.
     *
     * @param command The Syring sepresenting a command.
     * @throws Exception Any exception that occurs during the execution of the
     *                   command.
     */
    void command(String command) throws Exception;

    /**
     * Starts the command line interpreter using the given reader.
     * 
     * @param reader
     * @throws Exception
     */
    void start(BufferedReader reader) throws Exception;

    /**
     * Starts the interpreter with instructions from a file.
     *
     * @param filename name of the file with the instructions for the interpreter
     */
    void startFromFile(String filename, boolean debugMode);
}
