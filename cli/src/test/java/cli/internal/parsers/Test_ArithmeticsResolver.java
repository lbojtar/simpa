package cli.internal.parsers;

import org.junit.jupiter.api.Test;


public class Test_ArithmeticsResolver {

    @Test
    public void test_withArithmeticExpression() {
        String r = ExpressionParser.getInstance().eval("(1*3)");
        assert (Double.parseDouble(r)==3);
        r = ExpressionParser.getInstance().eval("(sin(0))");
        assert (Double.parseDouble(r)==0);
        r = ExpressionParser.getInstance().eval("(1.2e-6*1)");
        assert (Double.parseDouble(r)==1.2e-6);
        r = ExpressionParser.getInstance().eval("(PI)");
        assert (Double.parseDouble(r)==Math.PI);
        String s="""
            "text"
            """;
        r = ExpressionParser.getInstance().eval(s);
        assert (r.strip().equals(s.strip()));
    }
}
