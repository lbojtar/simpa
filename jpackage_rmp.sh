#!/bin/bash


ACC_TARGET_PATH="acc/target/"
JAR_FILE_PATH=$(ls $ACC_TARGET_PATH*-exe.jar)
JAR_FILE_NAME=$(basename "$JAR_FILE_PATH")
VERSION=$(echo $JAR_FILE_NAME | grep -oP '\d+\.\d+\.\d+')

echo "Packaging into RPM file"
# This generates an RMP file on REDHAT based systems
jpackage --app-version $VERSION --name simpa-acc --type rpm --input ./acc/target/ --dest ./target  --linux-package-deps lapack-devel --java-options "-Xmx8G" --main-jar $JAR_FILE_NAME

# Create a tar.gz package
rpm2cpio "./target/simpa-acc-${VERSION}-1.x86_64.rpm" | cpio -idmv -D /tmp/
TARGZ_FILE="/tmp/simpa-acc-${VERSION}.tar.gz"
DIRECTORY="/tmp/opt"
tar -czvf "$TARGZ_FILE" -C "$DIRECTORY" . --transform 's|/tmp/opt/||g'
echo "Packaging into $TARGZ_FILE"
