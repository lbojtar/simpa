#!/bin/bash

# This script is intended to automate the testing of the examples,
# but also to serve as a reference for running the examples manually.
# It runs all examples in the examples directory
# It assumes that the ACC jar file is in the target directory
# It also assumes that the input files are in the input_files directory
# The script creates a new directory for each example and copies the input files into it
# The script then runs the ACC jar file with the input files in the new directory

set -e # exit on error
script_dir="$PWD"
cd "../acc/target"  
jar_dir="$PWD"
for file in *; do
    if [[ $(basename "$file") == acc*-exe.jar ]]; then
       jar_file="$jar_dir/$file"
       echo "JAR FILE: $jar_file"
    fi
done


prepare_wdir() {
    cd $script_dir

   # directory name is the first argument to the function
    if [ -d "$1" ]; then
        echo "Directory $1 exists. Deleting it..."
        rm -r "$1"
        if [ $? -eq 0 ]; then
            echo "Successfully deleted $1"
        else
            echo "Could not delete $1"
            return 1
        fi
  
    fi

    mkdir "$1"
    echo "Successfully created a new directory $1"
    cd "$1"
   
}

# Simple machine example. takes a few minutes to execute
prepare_wdir "simple/wdir"
cp ../input_files/* .
java -jar $jar_file "../SIMPLE_MACHINE_ALL.simpa"


# Elena machine example. takes about 20 minutes to execute
prepare_wdir "elena/wdir"
cp ../input_files/* .
java -jar $jar_file "../ELENA_ALL.simpa"


# LNS transfer line example. takes about 20 minutes to execute
prepare_wdir "lns_line/wdir"
cp ../input_files/* .
java -jar $jar_file "../LNS_ALL.simpa"


prepare_wdir "elena_lines/elements/wdir"
cp ../input_files/* .
#This prepares elements for the elena lines
java -jar $jar_file "../create_elements.simpa"

prepare_wdir "elena_lines/asacusa1/wdir"
java -jar $jar_file "../ASACUSA1_ALL.simpa"

prepare_wdir "elena_lines/aegis/wdir"
java -jar $jar_file "../AEGIS_ALL.simpa"
