call -f "../create_sequence.simpa"

scalings= [{"LNS.ZQSF.0001_38.bin":0.0},{"LNS.ZQSD.0002_38.bin":0.0},{"LNS.ZCV.0020_38.bin":0.0},{"LNS.ZCH.0020_38.bin":0.0},{"LNS.ZQMF.0020_38.bin":1900.0},{"LNS.ZQMD.0021_38.bin":1000.0},{"LNS.ZDSIA.0030_38.bin":26200.0},{"LNS.ZCH.0050_38.bin":0.0},{"LNS.ZCV.0050_38.bin":0.0},{"LNS.ZQMD.0050_38.bin":1324.0},{"LNS.ZQMF.0051_38.bin":1855.0} ]
set-scalings -m scalings

#twiss parameters at the source determined by backtracking of the 2024 tomography beam
twp=create-twiss -x 0 -y 0 -xp 0.0 -yp 0.0 -ha 1.77  -va 1.07 -hb 1.12 -vb 1.06 -hd 0.0 -vd 0.0 -hdp 0.0 -vdp 0.0

STEPSIZE=0.005
EM_H=0.000002  # we do the matching with a smaller emittance than the source beam.
EM_V=0.000002

#Create beams at the end of the line with the Twiss parameters of the ELENA ring
matchingbeam0=create-beam -t "antiproton" -m 0.0137 -he EM_H -ve EM_V -na "matchingbeam0" -di "ellipse" -dp 0.0 -n 8  -lp 0 -twp twp
matchingbeam1=create-beam -t "antiproton" -m 0.0137 -he EM_H -ve EM_V -na "matchingbeam1" -di "ellipse" -dp 0.001 -n 8 -lp 0 -twp twp

# Create the target Twiss parameters at BTV118 in the ELENA ring
mt1=matching-target -lp 6.7096 -tt  "H_BETA" -tv 1.865 
mt2=matching-target -lp 6.7096 -tt  "V_BETA" -tv 2.218 
mt3=matching-target -lp 6.7096 -tt  "H_ALPHA" -tv  -0.6086 
mt4=matching-target -lp 6.7096 -tt  "V_ALPHA" -tv  -0.4487   
mt5=matching-target -lp 6.7096 -tt  "H_DISPERSION" -tv 0.26137 

# Create the variables for matching
!mv1=matching-variable -fm "LNS.ZQSF.0001_38.bin" -min 0 -max 6000 -g 1500     
!mv2=matching-variable -fm "LNS.ZQSD.0002_38.bin" -min 0 -max 6000 -g 1500   
!mv3=matching-variable -fm "LNS.ZQMF.0020_38.bin" -min 0 -max 6000 -g 1900   
!mv4=matching-variable -fm "LNS.ZQMD.0021_38.bin" -min 0 -max 6000 -g 1000  
!mv5=matching-variable -fm "LNS.ZQMD.0050_38.bin" -min 0 -max 6000 -g 1323     
!mv6=matching-variable -fm "LNS.ZQMF.0051_38.bin" -min 0 -max 6000 -g 1855  

# Create the variables for matching ,the initial values are already near the optimal ones found by CMA-ES
mv1=matching-variable -fm "LNS.ZQSF.0001_38.bin" -min 0 -max 6000 -g 1300.2204
mv2=matching-variable -fm "LNS.ZQSD.0002_38.bin" -min 0 -max 6000 -g 1947.637
mv3=matching-variable -fm "LNS.ZQMF.0020_38.bin" -min 0 -max 6000 -g 2576.771
mv4=matching-variable -fm "LNS.ZQMD.0021_38.bin" -min 0 -max 6000 -g 1877.339 
mv5=matching-variable -fm "LNS.ZQMD.0050_38.bin" -min 0 -max 6000 -g 4.848
mv6=matching-variable -fm "LNS.ZQMF.0051_38.bin" -min 0 -max 6000 -g 1091.4133  

# create a BOBYQA matcher, faster, but can be stuck in local minimum
matcher=bobyqa-matcher --targets [mt1,mt2,mt3,mt4,mt5] --variables [mv1,mv2,mv3,mv4,mv5,mv6]  -sf 1e-3

#create a CMA-ES matcher
!matcher=cmaes-matcher --targets [mt1,mt2,mt3,mt4,mt5] --variables [mv1,mv2,mv3,mv4,mv5,mv6]  --sigmas [300.0,300.0,300.0,300.0,300.0,300.0] --max-iterations 5000 -sf 1e-4
 
# do the matching
match -m matcher -b0 matchingbeam0 -b1 matchingbeam1  -dr 0.1 -ss STEPSIZE  -of "matched_optics.txt"
