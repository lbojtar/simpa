import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a class to get a smooth distribution from a tracking result. Is
 * intended to generate an input file for gnuplot to display how the tracked
 * bunch would look like on a fluorescent screen.
 */
public class DistributionCalculator {
	private static final double MIN_X = -0.028;
	private static final double MAX_X = 0.028;
	private static final double MIN_Y = -0.028;
	private static final double MAX_Y = 0.028;
	private static final int NUM_SAMPLES = 400;
	private static final double SIGMA = 0.002;
	private static int xColIndex;
	private static int yColIndex;

	public static void main(String[] args) {
		if (args.length != 4) {
			System.out.println("Usage: inputFile outputFile");
			System.exit(-1);
		}
		(new DistributionCalculator()).smooth(args[0], args[1], Integer.parseInt(args[2]),  Integer.parseInt( args[3]));

	}
/**
 * Takes a phase space file and from the positions constructs another file .
 */
	public  void smooth(String inputFile, String outFile,int xCol,int yCol) {
		xColIndex= xCol;
		yColIndex= yCol;
		List<double[]> data = readDataFromFile(inputFile);
		double[][] distribution = calculateDistribution(data);
		writeToFile(outFile, distribution);
		System.out.println(" Distribution was written to: " + outFile);
	}

	private  List<double[]> readDataFromFile(String filename) {
		List<double[]> data = new ArrayList<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
			String line;
			while ((line = reader.readLine()) != null) {
				String[] parts = line.split("\\s+");			
				if(parts.length<= xColIndex || parts.length <= yColIndex)
					continue;
					
				try {
					double x = Double.parseDouble(parts[xColIndex]); // x
					double y = Double.parseDouble(parts[yColIndex]);// y

					// Add the data point to the list
					data.add(new double[] { x, y });
				} catch (NumberFormatException e) {
					// ignore the line if not a number
				}

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return data;
	}

	private  double[][] calculateDistribution(List<double[]> data) {
		double[][] distribution = new double[NUM_SAMPLES][NUM_SAMPLES];

		double deltaX = (MAX_X - MIN_X) / (NUM_SAMPLES - 1);
		double deltaY = (MAX_Y - MIN_Y) / (NUM_SAMPLES - 1);

		for (double[] point : data) {
			double x = point[0];
			double y = point[1];

			for (int i = 0; i < NUM_SAMPLES; i++) {
				double sampleX = MIN_X + i * deltaX;
				if (Math.abs(x - sampleX) < 3 * SIGMA) {
					for (int j = 0; j < NUM_SAMPLES; j++) {
						double sampleY = MIN_Y + j * deltaY;

						double distance = Math.sqrt(Math.pow(x - sampleX, 2) + Math.pow(y - sampleY, 2));
						double value = Math.exp(-Math.pow(distance, 2) / (2 * Math.pow(SIGMA, 2)));

						distribution[i][j] += value;
					}
				}
			}
		}

		return distribution;
	}

	public  void writeToFile(String filename, double[][] data) {
		double max = 0;

		double deltaX = (MAX_X - MIN_X) / (NUM_SAMPLES - 1);
		double deltaY = (MAX_Y - MIN_Y) / (NUM_SAMPLES - 1);

		try (BufferedWriter writer = new BufferedWriter(new FileWriter(filename))) {
			int n = data.length;
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					double sampleX = MIN_X + i * deltaX;
					double sampleY = MIN_Y + j * deltaY;
					writer.write(sampleX + " " + sampleY + " " + data[i][j] + "\n");
					if (max < data[i][j])
						max = data[i][j];
				}
			}
			writer.close();
			System.out.println("Maximum value: " + max);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}