set terminal postscript eps color enhanced "Helvetica" 22
set output "../plot/optics.eps"

set xlabel "s [m]"
set ylabel "{/Symbol b}_h , {/Symbol b}_v"
set y2label "D_h [m]"
set y2tics  1 nomirror
set ytics   nomirror
set y2range [-2:2]
plot "optics.txt" u 1:8 w l t  "{/Symbol b}_h","optics.txt" u 1:9 w l  t  "{/Symbol b}_v","optics.txt" u 1:12  axes x1y2 w l t "D_h"

