set terminal png enhanced size 600,1000 font "default,11"

set title ""
set palette defined ( 0 '#000090',1 '#000fff',2 '#0090ff',3 '#0fffee',4 '#90ff70',6 '#ff7000',7 '#ee0000',8 '#7f0000')

set output '../plot/tomoGrid2024.png'


set xtics 0.01
set ytics 0.01

unset colorbox
unset key
set size square

set multiplot layout 3,1 rowsfirst
set xlabel "x [m]"
set ylabel "x'"
set xrange[-0.025:0.025]
set yrange[-0.025:0.025]
plot  "smoothed2024xxp.txt" u 1:2:3 w image t "","gridxxp2024-survived-PHS-AT_0.txt" u 1:2 linecolor rgb "white"

set xlabel "y [m]"
set ylabel "y'"
set xrange[-0.02:0.02]
set yrange[-0.02:0.02]
plot   "smoothed2024yyp.txt" u 1:2:3 w image t "","gridyyp2024-survived-PHS-AT_0.txt" u 3:4 linecolor rgb "white"

set xlabel "x [m]"
set ylabel "y [m]"
set xrange[-0.02:0.02]
set yrange[-0.02:0.02]
plot   "smoothed2024xy.txt" u 1:2:3 w image t "","gridxy2024-survived-PHS-AT_0.txt" u 1:3 linecolor rgb "white"

unset multiplot
