set terminal png enhanced  font "default,11"
set output "output.png"
set title " BTV image with reconstructed beam "
set palette defined ( 0 '#000090',1 '#000fff',2 '#0090ff',3 '#0fffee',4 '#90ff70',6 '#ff7000',7 '#ee0000',8 '#7f0000')
plot "smoothed.txt" u 1:2:3 w image t ""