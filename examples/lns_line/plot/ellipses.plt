set terminal png enhanced size 600,600 font "default,11"
set output '../plot/ellipses.png'

set xtics 0.01
set ytics 0.01


unset key
set size square

set xlabel "y [m]"
set ylabel "y' [m]"
set xrange[-0.01:0.01]
set yrange[-0.01:0.01]
plot "ellipse1s-PHS-AT_0.txt" u 3:4 t "", "ellipse2s-PHS-AT_0.txt" u 3:4 t "","ellipse3s-PHS-AT_0.txt" u 3:4 t "", "ellipse1s-PHS-AT_6.71.txt" u 3:4 t "", "ellipse2s-PHS-AT_6.71.txt" u 3:4 t "","ellipse3s-PHS-AT_6.71.txt" u 3:4 t ""
unset label 2

