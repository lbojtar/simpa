import sys

# Check that three file names were provided as arguments
if len(sys.argv) != 4:
    print("Usage: python3 script.py phase-spacefile_at_start.txt phase-spacefile_at_end.txt outfile.txt")
    sys.exit(1)

# Assign the file names to variables
a_file = sys.argv[2]
b_file = sys.argv[1]
c_file = sys.argv[3]

# Create a set of unique values from the 7th column of A.txt
with open(a_file) as f:
    a_values = set(line.split()[6] for line in f if not line.startswith("#"))

# Loop through each line in B.txt and check if the 7th column value is in a_values
with open(b_file) as f, open(c_file, "w") as out:
    for line in f:
        if not line.startswith("#") and line.split()[6] in a_values:
            out.write(line)
