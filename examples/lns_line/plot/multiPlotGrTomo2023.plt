set terminal png enhanced size 600,600 font "default,11"

set title ""

set output '../plot/tomoVert2023.png'


set xtics 0.01
set ytics 0.01

unset key
set size square


set xlabel "y [m]"
set ylabel "y' [m]"
set xrange[-0.02:0.02]
set yrange[-0.02:0.02]
plot "tomobeam2023-backward-PHS-AT_0.txt" u 3:4 w d t "","gridyyp-survived-PHS-AT_0.txt" u 3:4

unset multiplot
