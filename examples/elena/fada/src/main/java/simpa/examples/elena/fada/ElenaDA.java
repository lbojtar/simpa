/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.examples.elena.fada;

import java.io.FileNotFoundException;

import simpa.acc.api.Optics;
import simpa.acc.api.ParticleFactory;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.utils.DynamicAperture;
import simpa.acc.api.utils.OpticsCalculator;
import simpa.core.api.PotentialProvider;
import simpa.core.api.track.Aperture;
import simpa.core.api.track.Particle;
import simpa.core.api.track.ParticleType;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.track.StlAperture;
import simpa.core.api.track.TaosSymplecticIntegrator;

public class ElenaDA {

	private static double[][] dynapTunes = { { 0, 0 }, { 2.462 - 0.002, 1.4 - 0.012 }, { 2.43 - 0.02, 1.4 - 0.012 },
			{ 2.383 - 0.022, 1.431 - 0.011 }, { 2.4134 - 0.019, 1.4292 - 0.03 }, { 2.392 - 0.019, 1.45 - 0.012 },
			{ 2.446 - 0.02, 1.371 - 0.012 } };

	// at position 0
	private static TwissParameters getTwiss(ReferenceOrbit orb, int wpIndex, int turns) throws FileNotFoundException {
		System.out.println("Calculating optics for working point " + wpIndex);
		boolean withEcooler = true;
		double stepSize = 0.01;
		double dpOp = 0.001;

		try {
			ParticleFactory pf = new ParticleFactory(orb);

			PhaseSpaceCoordinates phc = new PhaseSpaceCoordinates(0.01, 0, 0.01, 0, 0, 0);
			PotentialProvider pp = Elena.prepareMachine(withEcooler, dynapTunes[wpIndex][0], dynapTunes[wpIndex][1]);

			Particle p0 = pf.getParticle(Elena.p0InGeV, phc, 0, pp, ParticleType.ANTIPROTON);
			p0.setName("wp" + wpIndex + "_nominal");

			phc = new PhaseSpaceCoordinates(0.01, 0, 0.01, 0, 0, dpOp);
			Particle p1 = pf.getParticle(Elena.p0InGeV, phc, 0, pp, ParticleType.ANTIPROTON);
			p1.setName("wp" + wpIndex + "_dpop_" + dpOp);

			OpticsCalculator oc = new OpticsCalculator(orb);
			Optics opt = oc.trackInRingAndCalculate(p0, p1, pp, stepSize, 0.2, false);

			return opt.getAllTwissParametrs().get(0);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// Before calling this, prepare all the field maps:
	// 1. Go the the examples/elena directory
	// 2. Make a working directory ( ./wdir for example)
	// 3. Copy all files from the input_files directory to the working directory
	// 4. Start simpa command line interface and type the command: call -f
	// "../ELENA_ALL.simpa"

	public static void main(String[] args) {

		int turns = 10000;
		if (args.length > 0)
			turns = Integer.parseInt(args[0]);
		
		double momentum = 0.0137;
		double diskRadius = 0.1;

		TaosSymplecticIntegrator.setOmega(2.0);

		for (int wpIdx = 1; wpIdx < dynapTunes.length; wpIdx++) {
			try {

				String beamName = "dynap_wp" + wpIdx;
				// String particleName = "p" + wpIdx + "_";

				double qh = dynapTunes[wpIdx][0];
				double qv = dynapTunes[wpIdx][1];

				boolean withEcooler = true;

				PotentialProvider potProvider = Elena.prepareMachine(withEcooler, qh, qv);
				Aperture aperture = new StlAperture(ElenaConstants.STL_FILE_NAME);
				potProvider.setAperture(aperture);

				ReferenceOrbit orb = new ReferenceOrbit(ElenaConstants.REF_ORBIT_FILE_NAME, true, null, null);
				TwissParameters twp = getTwiss(orb, wpIdx, 50);

				DynamicAperture da = new DynamicAperture(orb, potProvider, beamName, diskRadius, twp, momentum);
				da.track(turns);
				da.calculate();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
