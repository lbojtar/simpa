/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */
package simpa.examples.elena.fada;

import java.util.Map;

import simpa.acc.api.Tune;
import simpa.acc.api.utils.AccCalculatorUtils;
import simpa.acc.api.utils.TuneModel;
import simpa.core.api.FileNamingConventions;


public class ElenaTuneCalculator implements TuneModel {
    

    @Override
    public Map<String, Double> getScalingFactors(Tune tune, double momentum) {
        double q_H = tune.qh();
        double q_V = tune.qv();
        double brho = AccCalculatorUtils.getBrhoFromGevOc(momentum);
        double i_QFND = getI_QFND(q_H, q_V,brho);
        double i_QDND = getI_QDND(q_H, q_V,brho);
        double i_QFNS = getI_QFNS(q_H, q_V,brho);
        Map<String, Double> map = Map.of("QFND", i_QFND, "QDND", i_QDND, "QFNS", i_QFNS);
        map = FileNamingConventions.addFieldMapExtension(map, ElenaConstants.LMAX_FOR_LATTICE_BALLS);
        return map;
    }

    /**
     * Polinomial fitting of the tune -> I functions around the working point Qh=2.4, Qv=1.4,D_ec=1[m]
     */
    double D_EC = 1.0; //dispersion at the e-cooler
    double GRADIENT_TO_CURRENT = 40.0/0.428;
    
    private static final double MQ_MAGNETIC_LENGTH = 0.25;
    
    public double getI_QFND(double q_H, double q_V,double brho) {
        double k=getK1(q_H, q_V);
        return brho*MQ_MAGNETIC_LENGTH*k*GRADIENT_TO_CURRENT;
    }

    public double getI_QDND(double q_H, double q_V,double brho) {
        double k=getK2(q_H, q_V);
        return brho*MQ_MAGNETIC_LENGTH*k*GRADIENT_TO_CURRENT;
    }

    public double getI_QFNS(double q_H, double q_V,double brho) {
        double k=getK3(q_H, q_V);
        return brho*MQ_MAGNETIC_LENGTH*k*GRADIENT_TO_CURRENT;
    }
   
    
    private double getK1(double q_H, double q_V) {

        return 2.87455 + 2.8363 * (q_H - 2.4) + 1.5141 * (q_V - 1.4) - 0.5894 * (D_EC - 1.0)
                - 3.169 * (q_H - 2.4) * (q_H - 2.4) - 0.842 * (q_V - 1.4) * (q_V - 1.4)
                - 0.023 * (D_EC - 1.0) * (D_EC - 1.0) - 0.876 * (q_H - 2.4) * (q_V - 1.4)
                - 1.098 * (q_H - 2.4) * (D_EC - 1.0) + 0.022 * (q_V - 1.4) * (D_EC - 1.0);

    }

    private double getK2(double q_H, double q_V) {
        return -2.11622 - 2.9914 * (q_H - 2.4) - 4.0820 * (q_V - 1.4) + 0.2569 * (D_EC - 1.0)
                + 3.596 * (q_H - 2.4) * (q_H - 2.4) + 1.459 * (q_V - 1.4) * (q_V - 1.4)
                - 0.0469 * (D_EC - 1.0) * (D_EC - 1.0) + 3.993 * (q_H - 2.4) * (q_V - 1.4)
                + 0.168 * (q_H - 2.4) * (D_EC - 1.0) + 0.504 * (q_V - 1.4) * (D_EC - 1.0);

    }

    private double getK3(double q_H, double q_V) {
        return 0.67077 + 0.8293 * (q_H - 2.4) - 0.0462 * (q_V - 1.4) + 0.2106 * (D_EC - 1.0)
                - 0.113 * (q_H - 2.4) * (q_H - 2.4) + 0.023 * (q_V - 1.4) * (q_V - 1.4)
                + 0.093 * (D_EC - 1.0) * (D_EC - 1.0) - 0.289 * (q_H - 2.4) * (q_V - 1.4)
                + 0.764 * (q_H - 2.4) * (D_EC - 1.0) + 0.025 * (q_V - 1.4) * (D_EC - 1.0);

    }

    public static void main(String[] args) {
    	ElenaTuneCalculator tc=new ElenaTuneCalculator();
    	Tune t=new Tune(2.265,1.265);
    	Map<String, Double> m=	tc.getScalingFactors(t, 0.0137);
    	System.out.println(m);
    }
}
