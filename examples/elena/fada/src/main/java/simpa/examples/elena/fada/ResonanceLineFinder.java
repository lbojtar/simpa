/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.examples.elena.fada;

public class ResonanceLineFinder {

	public static void main(String[] args) {
		ResonanceLineFinder t= new ResonanceLineFinder();
		t.test();
		//t.test6D();
	}
	
    public void test() {

    	//number 2. in paper
//        double qx1 = 2.466;
//        double qy1 = 1.468;
//        double qx2 = 2.273;
//        double qy2 = 1.274;
//        
    	//number 3. in paper
//        double qx1 = 2.365;
//        double qy1 = 1.45;
//        double qx2 = 2.457;
//        double qy2 = 1.315;
 
    	//number 4. in paper
//        double qx1 = 2.4;
//        double qy1 = 1.45;
//        double qx2 = 2.4;
//        double qy2 = 1.2;
          	
//    	//number 5. in paper
//        double qx1 = 2.474;
//        double qy1 = 1.411;
//        double qx2 = 2.422;
//        double qy2 = 1.266;

//    	//number 6. in paper
//        double qx1 = 2.43;
//        double qy1 = 1.391;
//        double qx2 = 2.301;
//        double qy2 = 1.424;

    	//number 7. in paper
        double qx1 = 2.384;
        double qy1 = 1.462;
        double qx2 = 2.297;
        double qy2 = 1.433;
        
        int max = 15;

        System.out.println(findResonanceLine(qx1, qy1, qx2, qy2, max));

    }


    public void test6D() {
        double qx = 0.430266228998;//0.425944357401;
        double qy = 0.475088155091;//0.474942143752;
        double qz = 0.00543133743173;//0.00542923923258;
       
        int max = 3;

        System.out.println(findClosestResonance6D( qx, qy, qz,  max) );

    }
    

    /**
     * Finds the closest resonance in 6D
     * 
     * @param qx - Horicontal tune
     * @param qy - Vectical tune
     * @param ql - Longitudinal tune
     * @param max- Maximum resonance order
     * @return - Equation of the resonance in a form of a string
     */
    public static String findClosestResonance6D(double qx, double qy, double ql, int max) {
        double[] best = new double[5];
        best[4] = Double.MAX_VALUE;

        for (int a = -max; a < max; a++) {
            for (int b = -max; b < max; b++) {
                for (int c = -max; c < max; c++) {
                    for (int d = -max; d < max; d++) {
                        double err = a * qx + b * qy + c * ql - d;

                        if (Math.abs(err) < Math.abs(best[4]) && a != 0 && b != 0 && c != 0 && d != 0) {
                            best[0] = a;
                            best[1] = b;
                            best[2] = c;
                            best[3] = d;
                            best[4] = err;

                        }
                    }
                }
            }

        }

        String s = (int) best[0] + " * Qh+ " + (int) best[1] + " * Qv + " + (int) best[2] + " * Ql = " + (int) best[3]
                + " with error= " + best[4] + System.lineSeparator() + "The actual equation is:"
                + System.lineSeparator() + (int) best[0] + " * " + qx + " + " + (int) best[1] + " * " + qy + " + "
                + (int) best[2] + " * " + ql + " = " + (best[3] + best[4]);

        return s;
    }

    /**
     * Utility to find the equation of a resonance line in the form a*Qh+b*Qv=c, where a,b,c are integers. It needs two
     * points on the tune diagram which falls onto a resonance line.
     * 
     * @param qx1 - Qh of point1
     * @param qy1 - Qv of point1
     * @param qx2 - Qh of point2
     * @param qy2 - Qv of point2
     * @param max - Maximum order to be searched
     * @return - Equation of the best fitting line
     */
    public static String findResonanceLine(double qx1, double qy1, double qx2, double qy2, int max) {
        double[] best = new double[4];
        best[3] = Double.MAX_VALUE;

        for (int a = -max; a < max; a++) {
            for (int b = -max; b < max; b++) {
                for (int c = -max; c < max; c++) {
                    double err1 = a * qx1 + b * qy1 - c;
                    double err2 = a * qx2 + b * qy2 - c;
                    double err = Math.sqrt(err1 * err1 + err2 * err2);
                    if (err < best[3] && c != 0) {
                        best[0] = a;
                        best[1] = b;
                        best[2] = c;
                        best[3] = err;
                    }
                }
            }

        }

        return (int) best[0] + "*Qh+" + (int) best[1] + "*Qv=" + (int) best[2] + " with error= " + best[3];
    }

}
