/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.examples.elena.fada;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import simpa.acc.api.ParticleFactory;
import simpa.acc.api.Tune;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.utils.FrequencyAnalysis;
import simpa.acc.api.utils.TuneModel;
import simpa.core.api.FieldMap;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.PotentialProvider;
import simpa.core.api.SHCombiner;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.Aperture;
import simpa.core.api.track.Particle;
import simpa.core.api.track.ParticleType;
import simpa.core.api.track.PhaseSpaceCoordinates;
import simpa.core.api.track.StlAperture;
import simpa.core.api.track.TaosSymplecticIntegrator;

public class ElenaFA {

	// FREQUENCY ANALYSIS IS A VERY MEMORY DEMANDING TASK, WE USE LESS THREADS THAN
	// AVAILABLE ( about 2GB/ thread memory is needed)
	int nThreads =  Runtime.getRuntime().availableProcessors()-2; //TODO Why this fails when all threads are used ??


	double p0InGeV = 0.0137; // momentum in [GeV/c]
	double longiPos = 0.0; // longitudinal position for insertion of particles in [m] along the orbit
							// counted from the beginning of the orbit

	// Estimated working points free of resonances to construct the optics
	// interpolation table
	private double[][] samples_EC = { { 2.2139, 1.2585 }, { 2.2001, 1.45 }, { 2.4272, 1.2802 }, { 2.4186, 1.5143 },
			{ 2.3847, 1.4624 }, { 2.4601, 1.3601 }, { 2.2801, 1.4701 }, { 2.52, 1.27 } };

	// without EC
	private double[][] samples_without_EC = { { 2.2341, 1.2781 }, { 2.2001, 1.4781 }, { 2.4401, 1.2901 },
			{ 2.4301, 1.4401 }, { 2.3901, 1.4701 }, { 2.4601, 1.3601 }, { 2.2801, 1.4701 }, { 2.3101, 1.2801 } };

	public ElenaFA(boolean withECooler) throws IOException, OutOfApertureException, KnownFatalException {

		ReferenceOrbit orbit = new ReferenceOrbit("orbit_design.txt", true,null,null);
		createFieldMapWithoutQuads();

		String outDir;
		double[][] samplesPoints;

		if (withECooler) {
			outDir = "scan_EC_30_30/";
			samplesPoints = samples_EC;

		} else {
			outDir = "scan_NOEC_30_30/";
			samplesPoints = samples_without_EC;
		}

		// create a list of tune objects from the samples
		List<Tune> tunes = new ArrayList<>();
		for (double[] sample : samplesPoints) {
			tunes.add(new Tune(sample[0], sample[1]));
		}

		Map<String, Double> scalings = new HashMap<>();
		scalings.put("QFND", 0.0);
		scalings.put("QDND", 0.0);
		scalings.put("QFNS", 0.0);
		scalings.put("ELENA_EC_NOQUADS", 1.0);
		scalings = FileNamingConventions.addFieldMapExtension(scalings, ElenaConstants.LMAX_FOR_LATTICE_BALLS);

		Aperture aperture = new StlAperture(ElenaConstants.STL_FILE_NAME);
		TuneModel tuneModel = new ElenaTuneCalculator();
		ParticleFactory pf = new ParticleFactory(orbit);

		PhaseSpaceCoordinates phs = new PhaseSpaceCoordinates(0.001, 0, 0.01, 0, 0, 0);
		PotentialProvider pp = SHCombiner.getUniqueInstance().getEvaluator(scalings);
		Particle p = pf.getParticle(p0InGeV, phs, longiPos, pp, ParticleType.ANTIPROTON);

		FrequencyAnalysis fa = FrequencyAnalysis.builder().setHEmittance(3e-5).setVEmittance(3e-5).setOutDir(outDir)
				.setScalings(scalings).setPrototypeParticle(p).setStartQh(2.2).setEndQh(2.52).setStepsQh(160)
				.setStartQv(1.2).setEndQv(1.5).setStepsQv(150).setNumberOfThreadsUsed(nThreads).setDiskRadius(0.2)
				.setAperture(aperture).setReferenceOrbit(orbit).setTuneModel(tuneModel).setSampleTunes(tunes)
				.setDpOp(0.001).build();

		TaosSymplecticIntegrator.setOmega(2.0);

		fa.scanTunes2D(0.005);
		fa.calculate("diffusions.txt");
	}

	private void createFieldMapWithoutQuads() throws IOException, OutOfApertureException, KnownFatalException {

		Map<String, Double> map = new HashMap<>();
		map.put("ECOOLER", 1e-4);
		map.put("CORRECTORS_FOR_COOLER", 1.0);
		map.put("SCOMP", -11.54);
		map.put("BENDINGS", -37.51);

		map = FileNamingConventions.addFieldMapExtension(map, ElenaConstants.LMAX_FOR_LATTICE_BALLS);
		FieldMap fm = SHCombiner.getUniqueInstance().getFieldMapWithoutCaching(map);
		fm.writeToFile(
				FileNamingConventions.addFieldMapExtension("ELENA_EC_NOQUADS", ElenaConstants.LMAX_FOR_LATTICE_BALLS));

	}

	public static void main(String[] args) throws FileNotFoundException {

		try {
			new ElenaFA(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
