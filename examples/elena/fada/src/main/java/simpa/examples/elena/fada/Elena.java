// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.examples.elena.fada;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import simpa.acc.api.Beam;
import simpa.acc.api.TwissParameters;
import simpa.acc.api.create.ReferenceOrbit;
import simpa.acc.api.track.PhaseSpaceObserver;
import simpa.acc.api.track.RingBeamTracker;
import simpa.acc.api.utils.AccCalculatorUtils;
import simpa.acc.api.utils.GaussianBeamSampler;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.PotentialProvider;
import simpa.core.api.SHCombiner;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.ParticleType;
import simpa.core.api.track.TaosSymplecticIntegrator;

public class Elena {

	public static double p0InGeV = 0.0137;
	public static final String ORBIT_FILE = "orbit_design.txt";
	public static int maxDegree = ElenaConstants.LMAX_FOR_LATTICE_BALLS;

	public static void beamTrackingTest(TwissParameters twiss, double emV, double emH, double dpOp)
			throws OutOfApertureException, KnownFatalException, IOException {

		ReferenceOrbit co = new ReferenceOrbit(ORBIT_FILE, true, null, null);

		int nbOfSamples = 1000; // particles in the bunch
		int turns = 1000;
		double stepSize = 0.01;
		String outDir = "./bunch_tr_13.7/";

		// on resonance
		double qh = 2.374;
		double qv = 1.44;

		boolean withEcooler = true;

		PotentialProvider pp = prepareMachine(withEcooler, qh, qv);

		GaussianBeamSampler beamSampler = new GaussianBeamSampler(twiss);
		Beam beam = new Beam(beamSampler, p0InGeV, co, 0, pp, ParticleType.ANTIPROTON.getMass(),
				ParticleType.ANTIPROTON.getCharge(), nbOfSamples, "p_");

		RingBeamTracker beamTracking = new RingBeamTracker(beam, turns, 0.2, stepSize,
				Runtime.getRuntime().availableProcessors(), false);

		PhaseSpaceObserver.setFlushPerTurns(100);
		TaosSymplecticIntegrator.setOmega(2.5);

		try {
			new File(outDir).mkdir();
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			beamTracking.track();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static PotentialProvider prepareMachine(boolean withEcooler, double qh, double qv) throws IOException {

		// set quads by estimated tune
		double brho = AccCalculatorUtils.getBrhoFromGevOc(p0InGeV);

		ElenaTuneCalculator tc = new ElenaTuneCalculator();
		double iqfnd = tc.getI_QFND(qh, qv, brho);
		double iqdnd = tc.getI_QDND(qh, qv, brho);
		double iqfns = tc.getI_QFNS(qh, qv, brho);

		HashMap<String, Double> map = new HashMap<>();

		if (withEcooler) {
			map.put(FileNamingConventions.addFieldMapExtension("ECOOLER", maxDegree), 1E-4); // gauss 1E-4
			map.put(FileNamingConventions.addFieldMapExtension("SCOMP", maxDegree), -11.54);
			map.put(FileNamingConventions.addFieldMapExtension("CORRECTORS_FOR_COOLER", maxDegree), 1.0);
		}

		double ibhz = -37.51;
		map.put(FileNamingConventions.addFieldMapExtension("BENDINGS", maxDegree), ibhz);

		map.put(FileNamingConventions.addFieldMapExtension("QFND", maxDegree), iqfnd);
		map.put(FileNamingConventions.addFieldMapExtension("QDND", maxDegree), iqdnd);
		map.put(FileNamingConventions.addFieldMapExtension("QFNS", maxDegree), iqfns);
		System.out.println("I_BHZ= " + ibhz + " I_QFND= " + iqfnd + " I_QDND= " + iqdnd + " I_QFNS= " + iqfns);
		return SHCombiner.getUniqueInstance().getEvaluatorWithoutCaching(map);

	}

	public static void main(String args[]) {

	}
}
