/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.examples.elena.fada;

import simpa.acc.api.TwissParameters;
import simpa.acc.api.track.PhaseSpaceEllipse;
import simpa.acc.api.track.PhaseSpacePlane;
import simpa.acc.api.utils.BeamMatrix;
import simpa.core.api.utils.FileUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class BunchDataCollector {

	String dir = "./";

	public static void main(String[] args) throws IOException {
		BunchDataCollector t = new BunchDataCollector();
		// t.testEmittance();
		t.initialEmittancesTest();
	}

	public void testEmittance() throws FileNotFoundException {
		long turns = 1;

		String f = "all_phs_turn_" + turns + ".txt";
		collect(dir, turns, "p", dir + f);
		BeamMatrix bm = new BeamMatrix(dir + f);
		System.out.println("Hor. emittance = " + bm.getEmittance(PhaseSpacePlane.HORIZONTAL) + " [m]");
		System.out.println("Vert. emittance = " + bm.getEmittance(PhaseSpacePlane.VERTICAL) + " [m]");
	}

	/**
	 * When doing dynamical aperture tracking we put the initial INTENDED emittances
	 * into the file names. This method puts these numbers in a file along with the
	 * NUMERICALLY MEASURED measured emittances.
	 * @throws IOException 
	 */
	public void initialEmittancesTest() throws IOException {

		String outFile = "initialEmittances.txt";

		StringBuilder sb = new StringBuilder();
		File[] files = new File(dir).listFiles();
		for (File file : files) {
			if (file.getName().contains("p")) {

				StringTokenizer st = new StringTokenizer(file.getName(), "_");
				st.nextToken();
				String t1 = st.nextToken();
				String t2 = st.nextToken();
				
				// numerically measured emittance
				TwissParameters tp = PhaseSpaceEllipse.getTwissParameters(file.getPath());
				sb.append(t1 + " " + t2 + " " + tp.hEllipse().em() + " " + tp.vEllipse().em() + System.lineSeparator());

			}
		}

		FileUtils.writeTextFile(dir + outFile, sb.toString());
	}

	private void collect(String directory, long turnNumber, String filter, String outFile) {

		File[] files = new File(directory).listFiles();

		StringBuilder sb = new StringBuilder();

		assert files != null;
		for (File file : files) {
			if (file.isFile()) {
				if (file.getName().contains(filter)) {
					try {
						String s = FileUtils.getLineFromFile(file.getCanonicalPath(), turnNumber);
						if (s != null)
							sb.append(s).append(System.lineSeparator());

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}

		FileUtils.writeTextFile(outFile, sb.toString());
	}
}
