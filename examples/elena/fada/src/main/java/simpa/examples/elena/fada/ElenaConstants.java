// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.examples.elena.fada;

public class ElenaConstants {


    public static final String STL_FILE_NAME = "elena.stl";

    public static final String REF_ORBIT_FILE_NAME = "orbit_design.txt";
   
    // l_max for sperical harmonics expansion
    public static int _LMAX_FOR_SINGLE_ROW = 38;// //8;//10;//16;//38;

    public static int LMAX_FOR_LATTICE_BALLS = 12;
    
    // The bending was calculated with this current in Opera and exported in Teslas
    public static double OPERA_MODEL_BEND_CURRENT = 272.995;

    // The quad was calculated with Opera with 37.011 A and the result is exported in Teslas
    public static double QUAD_CURRENT = 37.011;

    // The skew quad was calculated with Opera with 1.5 A
    public static double SKEW_CURRENT = 1.5;

    // The H corrector dipole was calculated with Opera with 48 A , B in [Gauss]
    public static double HCORRECTOR_CURRENT =  48.0;

    // The V corrector dipole was calculated with Opera with 45 A , B in [Gauss]
    public static double VCORRECTOR_CURRENT = 45.0;
}
