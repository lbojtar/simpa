import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import Normalize

meauredTune=True

    
infile='../../examples/elena/wdir/diffusions.txt'

arr = np.genfromtxt(infile, delimiter=' ')
arrt=np.transpose(arr)
if(meauredTune):
    qh=2+arrt[2]
    qv=1+arrt[3]
    scanmode="Numerically measured "  
else:    
    qh=arrt[0]
    qv=arrt[1]
    scanmode="Estimated "  
    
dq=np.log10(arrt[4])
normalization= Normalize(-6,-1,clip=False)
plt.xlabel(scanmode+"horizontal tune")
plt.ylabel(scanmode+"vertical tune")

plt.title("")

pictfile="tunes.png"
cmap = plt.get_cmap('jet') 
plt.scatter(qh,qv, c=dq,norm=normalization,s=6,cmap=cmap,edgecolors="none")
#plt.colorbar(orientation='horizontal')

if(meauredTune):
    plt.xlim(2.18,2.51)
    plt.ylim(1.19,1.51)
else:
    plt.xlim(2.18,2.54)
    plt.ylim(1.18,1.52)

plt.savefig(pictfile,dpi=150)
plt.show()
