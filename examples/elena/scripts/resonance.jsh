// JSHELL script to move the tune across a resonance by changing the dP of a particle
// should be called from the working directory (wdir) by: jshell ../scripts/resonance.jsh

//this might be updated to the actual path to the simpa.jar
/env --class-path /home/lbojtar/git/simpa/acc/target/acc-0.9.8-exe.jar 

import simpa.acc.api.*;  
import cli.api.*;
import simpa.acc.api.utils.*;
import simpa.core.api.utils.*;
import simpa.core.api.track.*;

CommandInterpreter ci= AccCli.getCommandInterpreter();
AccCli acc= new AccCli();

ci.command("""
	call -f "../create_bending_chamber.simpa"
	call -f "../create_elena_sequence.simpa"
	set-scalings -m [{"BENDINGS_12.bin":-37.51},{"QFND_12.bin":2.898},{"QDND_12.bin":-1.988},{"QFNS_12.bin":0.6882},{"ECOOLER_12.bin":1E-4},{"SCOMP_12.bin":-11.54},{"CORRECTORS_FOR_COOLER_12.bin":1.0}] -ee true 
	set-integrator -om 2.0
	""");

String s="";
int turns=128;

for(int i=-12;i<10;i++){
	double dpop=0.0005*i;
	Particle p=acc.createParticle("antiproton",0.0137,0.001,0.0,0.003,0.0,0.0,dpop,null,null,"p0");
	acc.track(p, null,turns,0.01,null,0.1,null,null,false,null,false);	
	TuneCalculator tc = new TuneCalculator("p0-PHS-AT_0.000.txt");
	s=s+dpop+" "+tc.getHorizontalTune(0, turns)+" "+tc.getVerticalTune(0, turns)+"\n";
}

FileUtils.writeTextFile("resonance.txt",s);
/exit

