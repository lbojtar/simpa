This directory contains sub directories for ejection lines of the ELENA machine.
At the moment there is no possibility to change the working directory
from SIMPA. Therefore, the working directory has to be changed manually. Relative paths
in the SIMPA scripts are always relative to the working directory. That means
that the working directories has to be at the same depth for each beam line.

1. The first step is to create the beam line elements for all transfer lines by calling
   the "create_elements.simpa" command file from the "elements/wdir"  directory. Do the following:

    - "cd elements; mkdir wdir; cd wdir"
    - "cp ../input_files/* ." These files are needed to create the beam line elements.
	- Start SIMPA then type: "call -f "../create_elements.simpa" This will create solution files	   
	  in the "elements/wdir" directory named "elementName-SOLUTION.txt" These files contain the point
	  sources for each beam line element and will be used to build up the beam lines.
	
	Exit from SIMPA with q+enter.

 2. Go to the directory of the beam line you want to calculate the optics, for example:
  	AT THE MOMENT (15.01.2024) ONLY ASACUSA1 is matched with a simplified 2.8 cm uniform aperture and not yet checked with beam.
  	ALPHA and AEGIS in progress
  	
  	- "cd asacusa1; mkdir wdir; cd wdir"
  	-Start SIMPA then type: "call -f "../ASACUSA1_ALL.simpa" This will calculate the optics of the beam line
  	
  	