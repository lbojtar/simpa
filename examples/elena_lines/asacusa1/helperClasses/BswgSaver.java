// simpa jar must be in the classpath to compile

import  simpa.acc.api.create.Sequence;
import  simpa.acc.api.Optics;
import  simpa.acc.api.TwissParameters;
import  simpa.acc.api.create.AccElement;

public class BswgSaver{
	/**
	 * This method prints the Twiss parameters at the position of the BSWG elements.
	 * 
	 * @param seq The sequence.
	 
	 */
	public void print(Sequence seq) {
		Optics optics= seq.getOptics();

		for(String name: seq.getAccElements().keySet()) {
			if(name.contains("BSGW") || name.contains("bsgw") ){
			   AccElement el= seq.getAccElements().get(name);
			   double lp=el.getLongitudinalPosition();
			   TwissParameters twp= optics.getTwissParametersAt(lp);
			   
			   System.out.println(System.lineSeparator()+"name="+name);
			   System.out.println("position="+lp);
			   System.out.println("H beta="+twp.hEllipse().beta());
			   System.out.println("H mu (rad)="+twp.muH()*2*Math.PI);	
			   System.out.println("H Dispersion ="+twp.dispH());	
			   System.out.println("V beta="+twp.vEllipse().beta());
			   System.out.println("V mu (rad)="+twp.muV()*2*Math.PI);	
			}
		}
	   }
}