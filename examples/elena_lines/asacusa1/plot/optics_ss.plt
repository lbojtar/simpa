# Run this gnuplot script from the working directory

set terminal pngcairo  size 1920,500 font "Arial,11"
set title ""
set output '../plot/optics_ss.png'

set xlabel "s [m]"
set ylabel "{/Symbol b}_h , {/Symbol b}_v"
set y2label "D_h [m]"
set y2tics  5 nomirror
set ytics   10 nomirror
set yrange [0:60]
set y2range [-20:20]
plot "optics_ss.txt" u 1:8 w l t  "SIMPA {/Symbol b}_h","optics_ss.txt" u 1:9 w l  t  "SIMPA {/Symbol b}_v","optics_ss.txt" u 1:12  axes x1y2 w l t "SIMPA D_h","../plot/elements.txt"  using 1:(20):5 with labels rotate by 90  t ""
