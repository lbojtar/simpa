# Run this gnuplot script from the working directory

set terminal pngcairo  size 1920,500 font "Arial,11"
set title ""
set output '../plot/lineDisp.png'
s=11
set pointsize 3
plot "optics.txt" u ($1<s ? $1:1/0):12  w l t "SIMPA D_h","../plot/elements.txt"  using  ($1<s ? $1:1/0):(-3):5 with labels rotate by 90  t "", "optics.txt" u  ($1<s ? $1:1/0):13  w l t "SIMPA D_v","../plot/lineDisp.txt" u 1:2 t "Measured D_h","../plot/lineDisp.txt" u 1:3 t "Measured D_v" 