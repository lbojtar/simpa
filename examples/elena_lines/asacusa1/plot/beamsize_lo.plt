# Run this gnuplot script from the working directory

set terminal pngcairo  size 1920,500 font "Arial,11"
set title ""
set output '../plot/sigmas_lo.png'

set xlabel "s [m]"
set ylabel "{/Symbol s}_h , {/Symbol s}_v"
set yrange [0:0.02]
plot "sigmas_lo.txt" u 1:2 w l t  "SIMPA {/Symbol s}_h","sigmas_lo.txt" u 1:3 w l t  "SIMPA {/Symbol s}_v","../plot/elements.txt"  using 1:(0.015):5 with labels rotate by 90  t "","../plot/measured_sigmas_lo.txt" u 1:(0.001*$2) w p t "Measured {/Symbol s}_h","../plot/measured_sigmas_lo.txt" u 1:(0.001*$3) w p t "Measured {/Symbol s}_v" 
