// This is a very simple JShell (part of JDK9 and above) script to interact with simpa
// It can contain any java statment. See the JDK doc for the usage of JShell
// Simpa commands can be executed  with the ommandInterpreter.command(String command) method.
// Using jshell scripts java and simpa statment can be combined to do tasks which can not be
// done with simpa commands alone.
// Go to the working directory wdir and execute jshell ../scripting/all.jsh

//this might be updated to the actual path to the simpa.jar
/env --class-path /home/lbojtar/git/simpa/acc/target/acc-0.9.8-exe.jar 
import simpa.acc.api.*;  
import cli.api.*;
CommandInterpreter ci= AccCli.getCommandInterpreter();
ci.command("""
call -f "../ASACUSA1_ALL.simpa"
# you can put any number of simpa statmens into the command string
""");
