# Simple example Python script to use simpa commands from Python through JPype

import jpype
import jpype.imports
from jpype.types import *
# the classpath must be updated to the actual one !!
jpype.startJVM(classpath=['/home/lbojtar/git/simpa/acc/target/acc-0.9.8-SNAPSHOT-exe.jar'])
from simpa.acc.api import AccCli
from cli.api import CommandInterpreter
ci= AccCli.getCommandInterpreter()
# You can put many simpa commands into command string, we have only one here 
ci.command("""
call -f "../ASACUSA1_ALL.simpa"
""")
