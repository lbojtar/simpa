set terminal postscript eps color enhanced "Helvetica" 22
set output "defl45.77.eps"
set title "Deflector angle 45.77 degrees optics with SIMPA"
plot "../wdir/testDefl45.77-optics.txt" u 1:8 w l t "Beta_h", \
     "../wdir/testDefl45.77-optics.txt" u 1:9 w l t "Beta_v", \
     "../wdir/testDefl45.77-optics.txt" u 1:6 w l t "Alpha_h", \
     "../wdir/testDefl45.77-optics.txt" u 1:7 w l t "Alpha_v",\
     "../wdir/testDefl45.77-optics.txt" u 1:12 w l t "D_h"

set output "defl45.77_madx.eps"
set title "Deflector angle 45.77 degrees optics with MAD-X"
plot "defl_45.77.tfs" u 1:2 w l t "Beta_h", \
     "defl_45.77.tfs" u 1:3 w l t "Alpha_h",\
     "defl_45.77.tfs" u 1:4 w l t "Beta_v",\
     "defl_45.77.tfs" u 1:5 w l t "Alpha_v",\
     "defl_45.77.tfs" u 1:(0.0146*$6) w l t "D_h" 