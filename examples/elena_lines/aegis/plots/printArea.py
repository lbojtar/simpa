import numpy as np

#filename = '../wdir/opsol-PHS-AT_24.552.txt'
#filename = '../wdir/opsol_qs-PHS-AT_24.552.txt'
#filename = '../wdir/sol-PHS-AT_24.552.txt'
filename = '../wdir/sol_qs-PHS-AT_24.552.txt'

datax = np.loadtxt(filename, usecols=(0))
datay = np.loadtxt(filename, usecols=(2))
rmsx = np.sqrt(np.mean(datax**2))
rmsy = np.sqrt(np.mean(datay**2))
print(rmsx*rmsy)

