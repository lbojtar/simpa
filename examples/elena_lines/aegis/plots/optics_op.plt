#GNUPLOT script file

set terminal postscript eps color enhanced "Helvetica" 22
set output "optics.eps"

plot "twiss_centre_lne00_lne01_lne02_nom.tfs" u 1:2 w l t "MAD-X {/Symbol b}_H" , "twiss_centre_lne00_lne01_lne02_nom.tfs" u 1:4 w l t "MAD-X {/Symbol b}_V", "../wdir/op_qs-optics.txt" u 1:8 w l t "SIMPA {/Symbol b}_H", "../wdir/op_qs-optics.txt" u 1:9 w l t "SIMPA {/Symbol b}_V"

