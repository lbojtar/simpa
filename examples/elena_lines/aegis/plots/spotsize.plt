#GNUPLOT script file

set terminal postscript eps color enhanced "Helvetica" 22
# with theoetical initial conditions
set output "spotsize.eps"

set size square
set xtics 0.004
set ytics 0.004
set title  "With theoetical initial conditions"
set xlabel "x [m]"
set ylabel "y [m]"
set xrange[-0.008:0.008]
set yrange[-0.008:0.008]

# solenoid on
# comparison between operational values and optimized values
plot "../wdir/opsol-PHS-AT_24.552.txt" u 1:3 t "correctors only",  "../wdir/sol-PHS-AT_24.552.txt" u 1:3 t "quads+correctors"

# with quad scan initial conditions
set output "spotsize_qs.eps"
set title  "With quad scan initial conditions"
plot "../wdir/opsol_qs-PHS-AT_24.552.txt" u 1:3 t "correctors only",  "../wdir/sol_qs-PHS-AT_24.552.txt" u 1:3 t "quads+correctors"