# Match the initial Twiss parameters at the start of the AEGIS line,
# such that the beam sigma values get closest to the 
# measured values on the beam position monitors

call -f "../../sequences/aegis_sequence.simpa"
call -f "../../sequences/initial_twiss_quadscan_2024.simpa" # declares varible: twp
call -f "../AEGIS_scaling_OP_SOL_steered.simpa"
set-integrator -om 0.0

mv1=matching-variable  -t "INITIAL_H_EMITTANCE"  -min  0.5  -max 5.0  --guess  2.0  # emittance in micrometers !!!!
mv2=matching-variable  -t "INITIAL_H_ALPHA"      -min  0    -max 5    --guess  2.02
mv3=matching-variable  -t "INITIAL_H_BETA"       -min  2    -max 10   --guess  4.15
mv4=matching-variable  -t "INITIAL_H_DISP"       -min -5    -max 5    --guess  1.0

mv5=matching-variable  -t "INITIAL_V_EMITTANCE"  -min  0.5  -max 5.0  --guess  2.0    # emittance in micrometers !!!!
mv6=matching-variable  -t "INITIAL_V_ALPHA"      -min  0    -max 5    --guess  1.1
mv7=matching-variable  -t "INITIAL_V_BETA"       -min  2    -max 10   --guess  5.17



# the target values are the sigmas [m] in the monitors
mt1=matching-target  -tt "H_SIGMA" -lp 2.9155  -tv 1.7e-3 
mt2=matching-target  -tt "V_SIGMA" -lp 2.9155  -tv 2.9e-3 
mt3=matching-target  -tt "H_SIGMA" -lp 4.1655  -tv 1.7e-3 
mt4=matching-target  -tt "V_SIGMA" -lp 4.1655  -tv 3.6e-3 
mt5=matching-target  -tt "H_SIGMA" -lp 5.6026  -tv 3.8e-3 
mt6=matching-target  -tt "V_SIGMA" -lp 5.6026  -tv 1.9e-3 
mt7=matching-target  -tt "H_SIGMA" -lp 8.7026  -tv 3.2e-3 
mt8=matching-target  -tt "V_SIGMA" -lp 8.7026  -tv 1.2e-3 
mt9=matching-target  -tt "H_SIGMA" -lp 10.0086 -tv 2.3e-3 
mt10=matching-target -tt "V_SIGMA" -lp 10.0086 -tv 2.8e-3
mt11=matching-target -tt "H_SIGMA" -lp 14.4603 -tv 1.8e-3 
mt12=matching-target -tt "V_SIGMA" -lp 14.4603 -tv 2.7e-3
mt13=matching-target -tt "H_SIGMA" -lp 16.4358 -tv 2.6e-3 
mt14=matching-target -tt "V_SIGMA" -lp 16.4358 -tv 1.9e-3
mt15=matching-target -tt "H_SIGMA" -lp 19.1543 -tv 4.0e-3 
mt16=matching-target -tt "V_SIGMA" -lp 19.1543 -tv 2.6e-3
mt17=matching-target -tt "H_SIGMA" -lp 22.5813 -tv 5.5e-3 
mt18=matching-target -tt "V_SIGMA" -lp 22.5813 -tv 10.0e-3


mb0=create-beam -t "antiproton" -m 0.0137 -na "matchbeam0" -di "gaussian" -n 1000 -twp twp

targets=[mt1,mt2,mt3,mt4,mt5,mt6,mt7,mt8,mt9,mt10,mt11,mt12,mt13,mt14,mt15,mt16,mt17,mt18]
!targets=[mt1,mt2,mt3,mt4,mt5,mt6,mt7,mt8,mt9,mt10,mt11,mt12,mt13,mt14,mt15,mt16]
!targets=[mt1,mt2,mt3,mt4,mt5,mt6,mt7,mt8,mt9,mt10]

variables=[mv1,mv2,mv3,mv4,mv5,mv6,mv7]

matcher=cmaes-matcher  --sigmas [0.5,2.0,2.0,0.1,0.5,2.0,2.0] --targets targets --variables variables   --max-iterations 10000 -sf 1e-9 
!matcher=bobyqa-matcher   --targets targets --variables variables   --max-iterations 10000 -sf 1e-9 
matchedScalings= match -m matcher -b0 mb0   -dr 0.1 -ss 0.01  -it twp --loss-penalty 0.0