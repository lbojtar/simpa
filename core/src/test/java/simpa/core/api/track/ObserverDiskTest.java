package simpa.core.api.track;


import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.utils.LocalFrame;

public class ObserverDiskTest {

	@Test
	public void test() {
		
		ObserverDisk d = new ObserverDisk(Vector3D.ZERO, new LocalFrame(Vector3D.PLUS_I,Vector3D.PLUS_J, Vector3D.PLUS_K), 0.1,false);

		// the particle goes to the positive Z direction
		Vector3D start = new Vector3D(0.01, 0.02, -1);
		Vector3D end = new Vector3D(0.03, 0.04, 1);
		Vector3D mom = end.subtract(start);
		Vector3D is = d.getIntersection(start, end);
		PhaseSpaceCoordinates c = d.getPhaseSpaceCoordinates(is, mom);

		assertTrue(c.x() > 0);
		assertTrue(c.y() > 0);
		assertTrue(c.xp() > 0);
		assertTrue(c.yp() > 0);

		// test the case when the particle goes to the negative z direction
		start = new Vector3D(0.01, 0.02, 1);
		end = new Vector3D(0.03, 0.04, -1);
		mom = end.subtract(start);
		is = d.getIntersection(start, end);
		c = d.getPhaseSpaceCoordinates(is, mom);
		
		//The positions and angles remain positive, OK.
		assertTrue(c.x() > 0);
		assertTrue(c.y() > 0);
		assertTrue(c.xp() > 0);
		assertTrue(c.yp() > 0);
	}

}
