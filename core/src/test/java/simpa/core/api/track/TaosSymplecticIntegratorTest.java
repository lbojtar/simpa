/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.track;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;
import simpa.core.api.PhysicsConstants;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.utils.CalculatorUtils;

public class TaosSymplecticIntegratorTest {

	@Test
	public void test() throws OutOfApertureException {		
		TaosSymplecticIntegrator.setOmega(2.5);
		double pInGev=PhysicsConstants.SPEED_OF_LIGHT/1E9;
		Vector3D pos=new Vector3D(0, 0, 0);
		PotentialProvider pp= new ConstantB1T();
		Vector3D  vp= null;
		try {
			vp = CalculatorUtils.getAVector(pp,pos);
		} catch (KnownFatalException e) {
			e.printStackTrace();
		}

		Vector3D dir =new Vector3D(1,0,0); 
		Particle p = new Particle(pos,dir, vp , pInGev, PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE,
				PhysicsConstants.PROTON_MASS);
		p.setName("test");
		ParticleTrackerTask ptt=new ParticleTrackerTask(pp, p, 628, 0.01,false);;
		TrajectoryObserver to=new TrajectoryObserver(TrackingObserver.ObserverBehaviour.WRITE_ONLY,false);	
		ptt.addObserver(to);
		//TaosSymplecticIntegrator.setOmega(2.5);
		
		ptt.run();
	}

	
	//By=1T
	private class ConstantB1T implements PotentialProvider{

		@Override
		public double[][] getPotentialsWithDerivatives(Vector3D r) throws OutOfApertureException {
			double[][] pot =new double[4][4];
			pot[0][0]=0.5*r.getZ(); //Ax
			pot[0][3]=0.5; //dAx/dz
			
			pot[2][0]=-0.5*r.getX(); //Az
			pot[2][1]=-0.5; //dAz/dx
			
			return pot;
		}

	

		@Override
		public void setAperture(Aperture ap) {
			
		}

		@Override
		public Aperture getAperture() {
			return null;
		}



		@Override
		public PotentialProvider copy() {
			// TODO Auto-generated method stub
			return null;
		}
		
	}
}
