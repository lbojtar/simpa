/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.track;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import simpa.core.api.SystemConstants;
import simpa.core.api.utils.FileUtils;
import simpa.core.cover.LatticeSearch;
import simpa.core.sh.SHBall;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class StlApertureTest {

	private SHBall[] ba;
	private LatticeSearch ls;
	private StlAperture aperture;

	private void initCover() {
        String tempDir = System.getProperty("java.io.tmpdir");
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH +"HCP_LATTICE_COVER.txt", tempDir+"/HCP_LATTICE_COVER.txt");
		InputStream is = null;
		try {
			is = new FileInputStream( tempDir+"/HCP_LATTICE_COVER.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		double[][] da = FileUtils.read2DArray(is, 4);

		ba = new SHBall[da.length];
		for (int i = 0; i < ba.length; i++) {
			double ballRadius = da[i][3];

			Vector3D center = new Vector3D(da[i][0], da[i][1], da[i][2]);

			ba[i] = new SHBall(null, 0, center, ballRadius,i);
		}

		long t0 = System.currentTimeMillis();
		ls = new LatticeSearch(ba);
		long t1 = System.currentTimeMillis();


		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + "beamregion.stl", tempDir+"/beamregion.stl");
		aperture = new StlAperture(tempDir+"/beamregion.stl");

		System.out.println("Aperture created in: " + (t1 - t0) + " [ms] for n= " + ba.length + " balls");
	}

	@Test
	@Disabled
	public void testCoverage() {
		initCover();
		int n = 1000000; // number of tests

		List<Vector3D> testPoints = new ArrayList<>();

		do {
			int idx = (int) (Math.random() * ba.length);
			SHBall b = ba[idx];

			Vector3D p = b.getCentre().add(new Vector3D(2 * (Math.random() - 0.5) * b.getRadius(),
					2 * (Math.random() - 0.5) * b.getRadius(), 2 * (Math.random() - 0.5) * b.getRadius()));

			if (aperture.isInside(p))
				testPoints.add(p);

		} while (testPoints.size() < n);

		long t0 = System.currentTimeMillis();

		List<Vector3D> notCoveredPoints = checkPoints(testPoints);

		long t1 = System.currentTimeMillis();
		System.out.println("Speed of  search: " + (t1 - t0) + " [ms] for n= " + testPoints.size() + " balls");
		System.out.println("Not found = " + notCoveredPoints.size());

		String tempDir = System.getProperty("java.io.tmpdir");

		StringBuilder sb = new StringBuilder();
		sb.append("#X Y Z DIST" + System.lineSeparator());
		double max = 0;
		for (Vector3D p : notCoveredPoints) {
			double dist = aperture.getDistanceFromAperture(p);
			sb.append(p.getX() + " " + p.getY() + " " + p.getZ() + " " + dist + System.lineSeparator());
			if (dist > max)
				max = dist;
		}

		FileUtils.writeTextFile(tempDir + "/notCovered.txt", sb.toString());
		System.out.println("Worst not covered distance from aperture = " + max);
	}

	private List<Vector3D> checkPoints(List<Vector3D> testPoints) {

		List<Vector3D> notCoveredPoints = new ArrayList<>();

		for (int i = 0; i < testPoints.size(); i++) {

			Vector3D p = testPoints.get(i);
			int idx = ls.getSphereIndex(p);
			if (idx == -1) {
				notCoveredPoints.add(p);
			}

		}

		return notCoveredPoints;

	}

}
