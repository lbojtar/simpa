/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.utils;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.LengthUnit;
import simpa.core.api.SurfacePoint;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.b2a.RangeSearch;
import simpa.core.b2a.SurfacePointImpl;

public class OcTreeTest {
    @Test
    public void test_octreeInsert() {
        OcTree ot = new OcTree(0,0,0,10,10,10);
        Vector3D loc = new Vector3D(1, 2, 3);
        Vector3D src = new Vector3D(1, 0, 0);
        SurfacePoint sp = SurfacePoint.createSurfacePoint(loc, src);
        Vector3D loc1 = new Vector3D(6, 4, 5);
        Vector3D src1 = new Vector3D(1, 2, 3);
        SurfacePoint sp1 = SurfacePoint.createSurfacePoint(loc1, src1);
        Vector3D loc2 = new Vector3D(2, 2, 3);
        Vector3D src2 = new Vector3D(1, 0, 0);
        SurfacePoint sp2 = SurfacePoint.createSurfacePoint(loc2, src2);

        ot.insert(sp);
        ot.insert(sp1);
        ot.insert(sp2);
        assertTrue(ot.find(sp));
        assertTrue(ot.find(sp1));
        assertTrue(ot.find(sp2));
    }

    @Test
    public void test_octreeFindInRange() {
        OcTree ot = new OcTree(0,0,0,10,10,10);
        Vector3D loc = new Vector3D(1, 2, 3);
        Vector3D src = new Vector3D(1, 0, 0);
        SurfacePoint sp = SurfacePoint.createSurfacePoint(loc, src);
        Vector3D loc1 = new Vector3D(6, 4, 5);
        Vector3D src1 = new Vector3D(1, 2, 3);
        SurfacePoint sp1 = SurfacePoint.createSurfacePoint(loc1, src1);
        Vector3D loc2 = new Vector3D(2, 2, 3);
        Vector3D src2 = new Vector3D(1, 0, 0);
        SurfacePoint sp2 = SurfacePoint.createSurfacePoint(loc2, src2);

        Vector3D center = new Vector3D(1,1,1);
        double radius = 3;

        ot.insert(sp);
        ot.insert(sp1);
        ot.insert(sp2);

        List<SurfacePoint> points = ot.findInRange(center, radius)
                .stream().map(item -> (SurfacePoint) item)
                .toList();
        for (SurfacePoint point: points) {
            System.out.println(point.getLocation());
        }
    }

    //@Test
    public void test_octreeFindInRangeWithElenaBHZData() throws FileNotFoundException, KnownFatalException {
        List<Vector3D> vectors = FileUtils.readVectors("ELBHZ-POINTS.txt", LengthUnit.MM);
        FileUtils.writeVectors("ELBHZ_wHeader.txt", vectors);
        List<SurfacePointImpl> points = new ArrayList<>();

        for (Vector3D p : vectors) {
            points.add((SurfacePointImpl)SurfacePoint.createSurfacePoint(p, null));
        }

        RangeSearch rs = new RangeSearch(points);

        ArrayList<Vector3D> origin = new ArrayList<>();
        origin.add(points.get(6).getLocation());
        FileUtils.writeVectors("origin.txt", origin);

     //   List<SurfacePointImpl> nb = rs.searchNeighbors(points.get(6),1,4); // find 4 neigbours
        List<SurfacePointImpl> nb = rs.searchNeighbors(points.get(6),0.05); // find in the given radius
        ArrayList<Vector3D> nbLocations = new ArrayList<>();

        for (SurfacePoint neighbor: nb) {
            nbLocations.add(neighbor.getLocation());
        }

        FileUtils.writeVectors("/tmp/neighbors.txt", nbLocations);
    }
}
