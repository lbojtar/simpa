/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.partitioning.Region.Location;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import simpa.core.api.SystemConstants;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.TextStlReader;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;

public class StlTextReaderTest {

	private static String fn = "cylinder.stl";

	@BeforeAll
	public static void copy() {
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + fn, "/tmp/" + fn);
	}

	@Test
	public void test1() throws NumberFormatException, IllegalArgumentException, IOException {
		TextStlReader sr = new TextStlReader();

		PolyhedronsSet phs = sr.createStlObject("/tmp/" + fn);
		Vector3D p = new Vector3D(0, 0, 0.5);
		Location loc = phs.checkPoint(p);
		assertTrue(loc == Location.INSIDE);

		p = new Vector3D(0.8, 0.8, 0.5);
		loc = phs.checkPoint(p);
		assertTrue(loc == Location.OUTSIDE);

		p = new Vector3D(0, 0, 1);
		loc = phs.checkPoint(p);
		assertTrue(loc == Location.BOUNDARY);
	}

	@Test
	public void test2() throws NumberFormatException, IllegalArgumentException, IOException {
		TextStlReader sr = new TextStlReader();

		sr.readStlFile(new FileInputStream("/tmp/" + fn));
		PolyhedronsSet phs = new PolyhedronsSet(sr.getVertices(), sr.getFaces(), 1e-6);
		Vector3D p = new Vector3D(0, 0, 0.5);
		Location loc = phs.checkPoint(p);
		assertTrue(loc == Location.INSIDE);

		p = new Vector3D(0.8, 0.8, 0.5);
		loc = phs.checkPoint(p);
		assertTrue(loc == Location.OUTSIDE);

		p = new Vector3D(0, 0, 1);
		loc = phs.checkPoint(p);
		assertTrue(loc == Location.BOUNDARY);
	}

}
