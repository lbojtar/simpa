package simpa.core.api.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.Profile;
import simpa.core.api.SystemConstants;

class SimpleTriangulatorTest {

	// @Test
	void test1() {
		Profile pr1 = new Profile(1.0, 2.0, 10, 0.0);
		Profile pr2 = pr1.getTransformedCopy(Vector3D.PLUS_K, Rotation.IDENTITY);
		List<Profile> profiles = new ArrayList<>();
		profiles.add(pr1);
		profiles.add(pr2);

		SimpleTriangulator st = new SimpleTriangulator(profiles, false);

		TextStlWriter.writeSTLTextFile(st.getSurface(), "/tmp/shortOpenRegion.stl");

		TextStlWriter.writeSTLTextFile(st.getSurfaceWithEndCups(), "/tmp/shortClosedRegion.stl");

	}

	@Test
	void test2() {
		List<Profile> profiles = new ArrayList<>();
		Profile pr0 = new Profile(1.0, 2.0, 10, 0.0);
		profiles.add(pr0);
		Profile tpr = null;
		for (int i = 1; i < 10; i++) {
			tpr = pr0.getTransformedCopy(Vector3D.PLUS_K.scalarMultiply(i),
					new Rotation(Vector3D.PLUS_K, i * 0.3, RotationConvention.VECTOR_OPERATOR));
			profiles.add(tpr);
		}

		SimpleTriangulator st = new SimpleTriangulator(profiles, false);

		TextStlWriter.writeSTLTextFile(st.getSurface(), "/tmp/openRegion.stl");
		TextStlWriter.writeSTLTextFile(st.getSurfaceWithEndCups(), "/tmp/closedRegion.stl");
		TextStlWriter.writeSTLTextFile(st.triangulateProfile(pr0, true), "/tmp/entry.stl");
		
	}

}
