package simpa.core.api.utils;


import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;


public class PathUtilsTest {

	int n = 100;//number of points, not segments!!!
	List<Vector3D> path = new ArrayList<>();

	@Test
	public void testGetArcDistance() throws FileNotFoundException {	
		double angle=Math.PI;
		List<Vector3D> lxy=  PathUtils.getArc(1, angle, n);		
		double arcd=	PathUtils.getArcDistances(lxy,false).get(lxy.size()-1);		
		assertEquals(arcd,angle,1e-9);		
		double lind=PathUtils.getDistance(0, lxy.size()-1, lxy);
		System.out.println("Diff between linear and arc path length = "+(arcd-lind));
	}

	@Test
	public void testClosedPathLength() throws FileNotFoundException {	
		double r=1.322;
		double angle=2*Math.PI*((n-1.0)/n);
		List<Vector3D> lxy =  PathUtils.getArc(r, angle, n);
		double pl=PathUtils.calculatePathLength(lxy, true);		
		assertEquals(pl,2*r*Math.PI,1e-9);		
	}

	//@Test
	public void testArcJoint() throws FileNotFoundException {	
		double r=1;
		double angle=0.1;
		List<Vector3D> l =  PathUtils.getArc(r, angle, n);
		double pla=PathUtils.calculatePathLength(l, false);			
		assertEquals(pla,2*r*angle,1e-9);		
		//insert a straight section before the arc	
	    l.add(0,new Vector3D(0,0,-0.01));
		double pl=PathUtils.calculatePathLength(l, false);		
		double sumLength=0.01+pl;
		System.out.println("Diff between 2th order length and summ of starigh + arc length: "+(sumLength-pl));
		assertEquals(pl,sumLength,1e-6);		
	}
	
	@Test
	public void testOpenPathLength() throws FileNotFoundException {	
		double r=0.10034;
		double deflection=Math.PI;
		List<Vector3D>	l  =  PathUtils.getArc(r, deflection, 101);
	//	FileUtils.writeVectors("/tmp/v100.txt", l);
		double pl=PathUtils.calculatePathLength(l, false);	
		assertEquals(pl,r*deflection,1e-9);		
		
		l  =  PathUtils.getArc(r, deflection, 100);
		//FileUtils.writeVectors("/tmp/v101.txt", l);
		pl=PathUtils.calculatePathLength(l, false);	
		assertEquals(pl,r*deflection,1e-9);		
	}

	
//	public PathUtilsTest() {
//		// a square in the XZ plane
//		path.add(new Vector3D(1, 1, 3));
//		path.add(new Vector3D(1, 1, 4));
//		path.add(new Vector3D(2, 1, 4));
//		path.add(new Vector3D(2, 1, 3));
//	}

	// @Test
	// public void testClosed() {

	// assertTrue(PathUtils.calculatePathLength(path, true) == 4);
	// // at the beginning of a closed path we take the vector from the last point t
	// // the first one as direction
	// assertTrue(PathUtils.getTangentVector(path, 0, true).direction().getX() ==
	// -1.0);

	// assertTrue(PathUtils.getTangentVector(path, 0, true).location().getZ() ==
	// 3.0);
	// assertTrue(PathUtils.getTangentVector(path, 1, true).direction().getZ() ==
	// 1.0);
	// assertTrue(PathUtils.getTangentVector(path, 3.5, true).direction().getX() ==
	// -1.0);
	// assertTrue(PathUtils.getTangentVector(path, 3.5, true).location().getX() ==
	// 1.5);

	// TangentVector3D tv = PathUtils.getTangentVector(path, 4, true);
	// assertTrue(tv.direction().getX() == -1.0);
	// assertTrue(tv.location().distance(path.get(0)) < 1e-15);
	// }

	// @Test
	// public void testOpen() {
	// assertTrue(PathUtils.calculatePathLength(path, false) == 3);

	// // at the beginning of an open path we take the first vector as direction
	// assertTrue(PathUtils.getTangentVector(path, 0, false).direction().getZ() ==
	// 1.0);

	// // at the end of an open path we take the last vector as direction
	// assertTrue(PathUtils.getTangentVector(path, 3, false).direction().getZ() ==
	// -1.0);

	// assertTrue(PathUtils.getTangentVector(path, 1, false).direction().getZ() ==
	// 1.0);

	// assertTrue(PathUtils.getTangentVector(path, 2.5, false).direction().getZ() ==
	// -1.0);
	// assertTrue(PathUtils.getTangentVector(path, 2.5, false).location().getZ() ==
	// 3.5);

	// TangentVector3D tv = PathUtils.getTangentVector(path, 3, false);
	// assertTrue(tv.direction().getZ() == -1.0);
	// assertTrue(tv.location().distance(path.get(3)) < 1e-15);
	// }
}
