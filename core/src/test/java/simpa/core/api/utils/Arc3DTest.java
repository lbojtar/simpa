package simpa.core.api.utils;



import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;


public class Arc3DTest {
    @Test
    public void testCalculate() {

        Vector3D p1 = new Vector3D(-1, 0, 0);
        Vector3D p2 = new Vector3D(0, 1, 0);
        Vector3D p3 = new Vector3D(0, -1, 0);

        assertTrue(Arc3D.calculateArcLength(p1, p2, p3,true) == Math.PI/2);
        assertTrue(Arc3D.calculateArcLength(p1, p2, p3,false) == Math.PI);
    }       

}
