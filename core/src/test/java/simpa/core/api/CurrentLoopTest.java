package simpa.core.api;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import simpa.core.api.utils.FileUtils;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import simpa.core.api.utils.CalculatorUtils;
import simpa.core.api.utils.Quadrature1D;

class CurrentLoopTest {

	private static CurrentLoop loop;
	private static List<Vector3D> targets;

	@BeforeAll
	public static void init() {
		loop = new CurrentLoop(1.0, 10000, 1E7);
		targets = new ArrayList<>();
		targets.add(Vector3D.ZERO);
	}

	@Test
	public void test() {
		// http://hyperphysics.phy-astr.gsu.edu/hbase/magnetic/curloo.html
		Vector3D bExact = new Vector3D(0, 0, -Math.PI * 2); // exact theoretical value

		List<PointSource> sources = loop.getPointSources(Quadrature1D.FIVE_POINT_GAUSSIAN, 1);

		Evaluator e = new Evaluator();
		double[][][] potsArr = e.evaluateAtTargets(sources, targets);
		Vector3D estB = CalculatorUtils.getBField(potsArr[0]);
		System.out.println("Estimated B : " + estB);
		double relErr = estB.subtract(bExact).getNorm() / bExact.getNorm();
		System.out.println("Relative error :" + relErr);
		assertTrue(relErr < 1e-5);

	}

	@Test
	public void testTransformed() {
		Vector3D tr = new Vector3D(0, 0, 0);
		Rotation rot = new Rotation(Vector3D.PLUS_I, Vector3D.PLUS_J, Vector3D.MINUS_K, Vector3D.MINUS_J);
		CurrentLoop loop2 = CurrentLoop.transform(loop, rot, tr);

		List<PointSource> sources = loop2.getPointSources(Quadrature1D.FIVE_POINT_GAUSSIAN, 1);
		Evaluator e = new Evaluator();
		double[][][] potsArr = e.evaluateAtTargets(sources, targets);
		Vector3D estB = CalculatorUtils.getBField(potsArr[0]);
		System.out.println("Estimated rotated B : " + estB);
		assertEquals(estB.getX(), Math.PI * 2, 1e-6); // compare to theoretical value
	}

	@Test
	public void aegisCoil() {

		List<PointSource> s1 = new ArrayList<>();
		List<PointSource> s2 = new ArrayList<>();

		CurrentLoop l1 = new CurrentLoop(0.18 / 2, 1000, -217443.095); // S1 from Jann's file
		CurrentLoop l2 = new CurrentLoop(0.25 / 2, 1000, -44457.258); // s2 from Jann's file

		for (int i = 0; i < 15; i++) {
			double d=0.48+0.085/2+0.8/15*(i+0.5); //from Jann's file
			CurrentLoop l= CurrentLoop.transform(l1, Rotation.IDENTITY, new Vector3D(0,0,d));
			s1.addAll(l.getPointSources(Quadrature1D.FIVE_POINT_GAUSSIAN, 1));
		
			d=0.48+0.085/2+0.8+0.660+0.8/15*(i+0.5);//from Jann's file
			l= CurrentLoop.transform(l2, Rotation.IDENTITY, new Vector3D(0,0,d));
			s2.addAll(l.getPointSources(Quadrature1D.FIVE_POINT_GAUSSIAN, 1));
		}

		StringBuilder sb= new StringBuilder();		
		for(PointSource s : s1) {
			sb.append(s.toString() + System.lineSeparator());
		}	
		FileUtils.writeTextFile("/tmp/AEGISS1-SOLUTION.txt", sb.toString());	
		
		sb= new StringBuilder();		
		for(PointSource s : s2) {
			sb.append(s.toString() + System.lineSeparator());
		}	
		FileUtils.writeTextFile("/tmp/AEGISS2-SOLUTION.txt", sb.toString());	
	}

}
