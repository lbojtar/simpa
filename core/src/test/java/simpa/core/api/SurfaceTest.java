package simpa.core.api;

import java.io.FileNotFoundException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.utils.FileUtils;

public class SurfaceTest {

	@Test 
	public void test() throws FileNotFoundException  {
       
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH +"planarPath.txt", "/tmp/planarPath.txt");
			
		List<Vector3D> path=FileUtils.readVectors("/tmp/planarPath.txt", LengthUnit.M);	
		ExtrudedSurface s= new ExtrudedSurface(path,getMap());
		s.toFile("/tmp/test-surface.txt");
	}

	private Map<Double, Profile>  getMap() {

		double height = 0.05;
		double width = 0.0935;
		Profile profile = new Profile(-width / 2, width / 2, -height / 2, height / 2, 4, 4);
		Profile endProfile = new Profile(0.03, 0.03, 16, 0);

		Map<Double, Profile> map = new HashMap<>();
		map.put(-2.35 / 2.0, endProfile);
		for (int i = -3; i <= 3; i++)
			map.put(i * 0.45 / 2, profile);
		map.put(2.35 / 2, endProfile);
		return map;

	}
}
