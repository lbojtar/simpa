package simpa.core.b2a.solve;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.FieldType;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.LengthUnit;
import simpa.core.api.PointSource;
import simpa.core.api.SurfacePoint;
import simpa.core.api.SystemConstants;
import simpa.core.api.utils.FileUtils;
import simpa.core.b2a.SurfacePointImpl;

public class TestData {

	public static List<SurfacePointImpl> get1DSurfacePoints(int n, FieldType ft) {

		List<SurfacePointImpl> spl = new ArrayList<>();

		for (int j = 1; j <= n; j++) {
			Vector3D loc = new Vector3D(0, 0, j);
			Vector3D toSource = new Vector3D(0, 1, 0);
			SurfacePointImpl sp = (SurfacePointImpl) SurfacePoint.createSurfacePoint(loc, toSource);
			PointSource ps = null;
			if (ft == FieldType.STATIC_MAGNETIC) {
				ps = new CurrentPointSource(loc.add(toSource));
				Vector3D bField = new Vector3D(Math.random(), 0, Math.random());
				sp.setField(bField);
			}

			if (ft == FieldType.STATIC_ELECTRIC) {
				ps = new ElectricMonopole(loc.add(toSource), 0);
				Vector3D eField = new Vector3D(0, Math.random(), 0);
				sp.setField(eField);
			}

			sp.setSource(ps);
			spl.add(sp);
		}

		return spl;
	}

	// get test 2d surface point distr.
	public static List<SurfacePointImpl> get2DSurfacePoints(int n, FieldType ft, double elevation, boolean random) {

		List<SurfacePointImpl> spl = new ArrayList<>();

		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++) {
				Vector3D loc = new Vector3D(i, 0, j);
				Vector3D toSource = new Vector3D(0, elevation, 0);
				PointSource ps = null;
				SurfacePointImpl sp = (SurfacePointImpl) SurfacePoint.createSurfacePoint(loc, toSource);

				if (ft == FieldType.STATIC_MAGNETIC) {
					ps = new CurrentPointSource(loc.add(toSource));
					Vector3D bField;
					if (random)
						bField = new Vector3D(Math.random(), 0, Math.random() - 0.5);
					else
						bField = new Vector3D(j * Math.sin(j * Math.PI / n), 0, i * Math.sin(j * 2 * Math.PI / n));
					sp.setField(bField);
				} else if (ft == FieldType.STATIC_ELECTRIC) {
					ps = new ElectricMonopole(loc.add(toSource), 0);
					Vector3D eField;
					if (random)
						eField = new Vector3D(0, Math.random() - 0.5, 0);
					else
						eField = new Vector3D(0, j * Math.sin(j * Math.PI / n), 0);
					sp.setField(eField);
				} else
					throw new IllegalArgumentException("Unknown field type: " + ft);

				sp.setSource(ps);
				spl.add(sp);
			}
		}

		return spl;
	}

	public static List<SurfacePointImpl> getMagnetSurfacePoints() throws FileNotFoundException {
		List<SurfacePointImpl> spl = new ArrayList<>();
	
		String solFile ="/tmp/ELBHZ-SOLUTION.txt";
		String fieldFile ="/tmp/ELBHZ-FIELD-AT-POINTS.txt";
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH +"ELBHZ-SOLUTION.txt", solFile);
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH +"ELBHZ-FIELD-AT-POINTS.txt", fieldFile);
		
		double[][] fd = FileUtils.read2DArray(fieldFile, 6);
		List<Vector3D> sPosl = FileUtils.readVectors(solFile, LengthUnit.M);
		for (int i = 0; i < fd.length; i++) {
			Vector3D sploc = new Vector3D(fd[i][0] * 0.001, fd[i][1] * 0.001, fd[i][2] * 0.001);
			Vector3D sloc = sPosl.get(i);
			Vector3D b = new Vector3D(fd[i][3], fd[i][4], fd[i][5]);
			CurrentPointSource ps = new CurrentPointSource(sloc);
			Vector3D toSource = sloc.subtract(sploc);
			SurfacePointImpl sp = (SurfacePointImpl) SurfacePoint.createSurfacePoint(sploc, toSource);

			sp.setField(b);
			sp.setSource(ps);
			spl.add(sp);
		}
		return spl;
	}

}
