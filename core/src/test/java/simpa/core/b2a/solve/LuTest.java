package simpa.core.b2a.solve;




import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealVector;
import org.junit.jupiter.api.Test;


public class LuTest {

	int n = 200;
	int arrays = 1;

	double[][][] aa;
	double[] b;

	double[][][] aa2;
	double[] b2;

	@Test
	public void testEquivalence() {
		double err=1e-8;
		// test if we get the same result with math3 and JBLAS which is much faster for
		// big matrices
		n = 1500;
		arrays = 1;
		createArrays();

		// with math3
		long t0 = System.currentTimeMillis();
		LUDecomposition lu = new LUDecomposition(new Array2DRowRealMatrix(aa[0]));
		long t1 = System.currentTimeMillis();
		System.out.println("Math3 LU decomp. done in " + (t1 - t0) + " ms");
		// with jblas
		t0 = System.currentTimeMillis();
		// check if JBlasLUSolver gives the same solution
		LUSolver jbs= new LUSolver(aa[0]);
		RealVector bv= new ArrayRealVector(b);
		RealVector x1=jbs.solve(bv);
		RealVector x2=	lu.getSolver().solve(bv);
		double e=	x1.subtract(x2).getNorm();
		assertTrue(e<err);
		t1 = System.currentTimeMillis();
		System.out.println("JBLAS LU decomp. done in " + (t1 - t0) + " ms");
	
	}


	private void createArrays() {
		aa = new double[arrays][n][n];
		b = new double[n];
		aa2 = new double[arrays][n][n];
		b2 = new double[n];

		for (int k = 0; k < arrays; k++) {
			for (int i = 0; i < n; i++) {
				b[i] = Math.random();
				b2[i] = b[i];
				for (int j = 0; j < n; j++) {
					aa[k][i][j] = Math.random();
					aa2[k][i][j] = aa[k][i][j];
				}
			}
		}
	}
}
