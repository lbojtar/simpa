/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.b2a.solve;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.junit.jupiter.api.Test;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.SurfacePoint;
import simpa.core.b2a.SurfacePointImpl;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;



public class StaticMagneticOperatorTest {
	
	StaticMagneticLinearOperator op;
	Vector3D loc = Vector3D.ZERO;
	CurrentPointSource ps;
	List<SurfacePointImpl> spl;
	
	public StaticMagneticOperatorTest() {
	
		Vector3D toSource = new Vector3D(0, 0.1, 0);
		ps = new CurrentPointSource(loc.add(toSource));
		SurfacePointImpl sp =  (SurfacePointImpl)SurfacePoint.createSurfacePoint(loc, toSource);
		sp.setField(new Vector3D(1,0,0));
		sp.setSource(ps);		
		
	    spl = new ArrayList<>();
		spl.add(sp);
		op = new StaticMagneticLinearOperator(spl);
	}
	
	@Test
	public void test() {		
		
		RealVector x = new ArrayRealVector(2);
		x.setEntry(0, 1); // 0th element is Iz
		x.setEntry(1, 2); // 1st element is Ix
		RealVector b = op.operate(x);
		System.out.println(b.toString());
		// With a source above the surface (positive Y in toSource vector) the b points in the negative Z direction
		// when the current points in the positive X direction
		double bzFromSource = b.getEntry(1);		
		assertTrue(bzFromSource<0);
		
		Vector3D bps=ps.getField(loc);
		System.out.println(bps.toString());
		
		//when the local and global coordinate system is the same these are equal
		assertTrue(Math.abs(bps.getX()-b.getEntry(0))<1e-12);
		assertTrue(Math.abs(bps.getZ()-b.getEntry(1))<1e-12); 
	}

		
	
}
