package simpa.core.b2a.solve;

import java.io.FileNotFoundException;
import java.util.List;

import org.junit.jupiter.api.Test;

import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.b2a.SurfacePointImpl;

public class MISTest {

	@Test
	public void test() throws FileNotFoundException, KnownFatalException {

		List<SurfacePointImpl> points = TestData.getMagnetSurfacePoints();
		List<SurfacePointImpl> corseGrid = MIS.createCorseGrid(points, 0.03);		
		
		SolverDebugUtils.surfacePointsImplToFile(corseGrid,"/tmp/corseGrid.txt");
		SolverDebugUtils.aggregatesToFile(corseGrid,"/tmp/aggregates.txt");
	}

}
