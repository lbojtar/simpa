/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.b2a;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.ElectricMonopole;
import simpa.core.api.Evaluator;
import simpa.core.api.PointSource;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.utils.CalculatorUtils;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

public class ElectricMonopoleTest {

	@Test
	public void test_static() throws OutOfApertureException {
		System.out.println("static case");
		Vector3D pos = new Vector3D(0.13, 3.04, 1.33);
		ElectricMonopole em = new ElectricMonopole(pos, 1.0);

		Vector3D evalLoc = new Vector3D(1.23, 2.34, 3.33);
		Vector3D efp = getEFromPotential(em, evalLoc);
		System.out.println(efp);

		Vector3D ef = em.getField(evalLoc);
		System.out.println(ef);
		assertTrue(ef.subtract(efp).getNorm() < 1e-5);
	}

	private Vector3D getEFromPotential(ElectricMonopole m, Vector3D r)
			throws OutOfApertureException {

		Evaluator ev= new Evaluator();
		List<PointSource> sl= new  ArrayList<>();
		List<Vector3D> tl= new ArrayList<>();
		sl.add(m);
		tl.add(r);
		double[][][] pa = ev.evaluateAtTargets(sl, tl);
		return CalculatorUtils.getEField(pa[0]);

	}

}
