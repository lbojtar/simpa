package simpa.core.b2a;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.Evaluator;
import simpa.core.api.PointSource;
import simpa.core.api.utils.CalculatorUtils;
import simpa.core.api.utils.Quadrature1D;

class WireTest {
	private static List<Vector3D> targets;
	private static Wire w;
	
	@BeforeAll
	public static void init() {
		targets= new ArrayList<>();
		targets.add(new Vector3D(0.1,0.2,0.3));
		w= new Wire(Vector3D.ZERO, new Vector3D(1,0,0),1E6);
	}

	//@Test
	void testPoints() {
		List<CurrentPointSource> sources=w.getPointSources(Quadrature1D.FIVE_POINT_GAUSSIAN,5);
		for(int i=0;i< sources.size();i++) {
		    Vector3D p=sources.get(i).getLocation();
			System.out.println(p.getX()+" "+p.getY()+" "+p.getZ());
		}
	}
	
	@Test
	void testCompare() {			
		//Exact value from AofShortWireStatic.nb
		Vector3D exactB=new Vector3D(0, -0.2758939300489242, 0.18392928669928282);
		System.out.println("Exact B : "+exactB);		
		List<? extends PointSource> sources=w.getPointSources(Quadrature1D.FIVE_POINT_GAUSSIAN,5);
				
		Evaluator e= new Evaluator();
		double[][][] potsArr =e.evaluateAtTargets(sources, targets);
		Vector3D estB=CalculatorUtils.getBField(potsArr[0]);
		System.out.println("Estimated B : "+estB);
		double relErr=estB.subtract(exactB).getNorm()/exactB.getNorm();
		System.out.println("Relative error :"+relErr);
		assertTrue(relErr<1e-5);
	}

}
