/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.sh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.SourceArrangement;
import simpa.core.api.SystemConstants;
import simpa.core.api.utils.FileUtils;
import simpa.core.cover.SphereDescriptionImpl;

public class SHCoeffTests {
   
	@Test
    public void test_withBHZ() {
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + "ELBHZ-SOLUTION.txt", "/tmp/ELBHZ-SOLUTION.txt");
        int maxDegree = 38;

        double radius = 0.01;
        Vector3D centre = new Vector3D(0, 0.025, 0);

        SourceArrangement sa = new SourceArrangement("/tmp/ELBHZ-SOLUTION.txt", Vector3D.ZERO, Rotation.IDENTITY,1.0, "");

        SHCoefficientsCalculator sht = null;
        try {
            sht = new SHCoefficientsCalculator(maxDegree,1e-16);
        } catch (IOException e) {
            e.printStackTrace();
        }
    	SphereDescriptionImpl sd = new SphereDescriptionImpl(centre,radius,maxDegree,0);
		List<SphereDescriptionImpl> sdl = new ArrayList<>();
		sdl.add(sd);
        SHBall ball =   sht.createAllBallsWithFMM(sa.getPointSources(), sdl)[0];     
        SHUtils.relativeCoeffsToFile(ball, "/tmp/coeffsBHZ.txt");
    }
	
}
