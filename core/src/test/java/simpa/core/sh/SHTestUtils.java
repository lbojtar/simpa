package simpa.core.sh;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.PointSource;
import simpa.core.api.PotentialProvider;
import simpa.core.api.SphereCovering;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.utils.FileUtils;
import simpa.core.cover.SphereDescriptionImpl;

public class SHTestUtils {

	private static double apertureRadius = 0.01;
	private static double ballRadius = 0.012;

	/**
	 * Creates a SHField for tests .
	 * 
	 * @param lmax              Initial LMax of the balls before trimming of
	 *                          coefficients.
	 * @param relErrLimit       For trimming the coefficients
	 * @param statFileName      The name of the coefficient statistics file to be
	 *                          written if not null.
	 * @param firstSetOfSources Can generate two different test fields by setting
	 *                          this to true or false.
	 * @param magnetic          magnetic or electric sources.
	 * 
	 * @return An SHfield.
	 */
	public static SHField getTestSHField(int lmax, double relErrLimit, String statFileName, boolean firstSetOfSources,
			boolean magnetic) {
		List<PointSource> slist = getPointSources(firstSetOfSources, magnetic);
		SHFieldFactoryImpl shff = new SHFieldFactoryImpl();

		List<Vector3D> pathPoints = new ArrayList<>();
		pathPoints.add(Vector3D.ZERO);
		pathPoints.add(new Vector3D(0, 0, 1));
		SphereCovering sc = SphereCovering.createSingleRowCover(pathPoints, apertureRadius, ballRadius, false);
		shff.setCoverType(sc.getCoverType());
		
		List<SphereDescriptionImpl> sdl = SphereDescriptionImpl.toInternal(sc.getCoveringSpheres());
		SHField shField = null;
		try {
			shField = shff.createSHField(slist, sdl, lmax, relErrLimit,lmax/3);
			if (statFileName != null)
				shff.writeStatistic(shField, System.getProperty("java.io.tmpdir") + "/" + statFileName);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return shField;
	}

	public static List<PointSource> getPointSources(boolean firstSetOfSources, boolean magnetic) {

		List<PointSource> slist = new ArrayList<>();
		Vector3D pos1, pos2;

		if (firstSetOfSources) {
			pos1 = new Vector3D(0, 0.2, 0.5);
			pos2 = new Vector3D(0.27, 0.31, 0.35);
		} else {
			pos1 = new Vector3D(0.2, 0.1, 0.21);
			pos2 = new Vector3D(0.07, 0.1, 0.75);
		}

		if (magnetic) {
			CurrentPointSource m = new CurrentPointSource(pos1);
			if (firstSetOfSources)
				m.setLocalCurrentComponents(1, 2, 3);
			else
				m.setLocalCurrentComponents(2.3, 0.6, 1);
			slist.add(m);
			// we create a second source also
			m = new CurrentPointSource(pos2);
			if (firstSetOfSources)
				m.setLocalCurrentComponents(3.5, 7, 2);
			else
				m.setLocalCurrentComponents(0.5, 4, 3);
			slist.add(m);
		} else {
			ElectricMonopole e = new ElectricMonopole(pos1, 1.234);
			slist.add(e);
			// we create a second source also
			e = new ElectricMonopole(pos2, 3.45);
			slist.add(e);
		}
		return slist;
	}

	public static List<Vector3D> getTestPointsInsideField(int n) {
		List<Vector3D> l = new ArrayList<>();
		l.add(new Vector3D(0.0001, 0.002, 0.003)); // the first foint is fixed to help debugging
		for (int i = 1; i < n; i++) {
			double x = Math.random() * 0.7071067812 * apertureRadius;
			double y = Math.random() * 0.7071067812 * apertureRadius;
			double z = Math.random();
			l.add(new Vector3D(x, y, z));
		}
		return l;
	}

	/**
	 * Compares two SHFields with the same Lmax byte by byte
	 * 
	 * @param shf1 SHField 1 to compare
	 * @param shf2 SHField 2 to compare
	 * @return return true if the two SHField is the same.
	 */
	public static boolean compareFieldsWithEqualLmax(SHField shf1, SHField shf2) {

		for (int i = 0; i < shf1.getSHBalls().length; i++) {
			SHBall b1 = shf1.getSHBalls()[i];
			SHBall b2 = shf2.getSHBalls()[i];

			if (b2.getCentre().subtract(b1.getCentre()).getNorm() > 1e-14 || b2.getRadius() - b1.getRadius() > 1e-14
					|| b2.getNumberOfFunctions() != b1.getNumberOfFunctions()
					|| b2.getMaximumDegree() != b1.getMaximumDegree()) {
				System.out.println("The data  has changed");
				return false;
			}

			for (int j = 0; j < b2.getCoefficients().length; j++) {
				for (int k = 0; k < b2.getCoefficients()[j].length; k++) {

					if (b2.getCoefficients()[j][k] != b1.getCoefficients()[j][k]) {
						System.out.println("The data  has changed");
						return false;
					}
				}
			}
		}

		return true;
	}

	/**
	 * Compares two SHFields with possibly different Lmax.
	 * 
	 * @param refSHf       SHField 1 to compare
	 * @param scale1       Scaling factor for shf1
	 * @param shf2         SHField 2 to compare
	 * @param scale2       Scaling factor for shf2
	 * @param maxRelPotErr Relative error of the potentials allowed for the
	 *                     comparison.
	 * 
	 * @return Returns True if the two SHField are the same after scaling within the
	 *         given relative error.
	 * @throws KnownFatalException
	 * @throws OutOfApertureException
	 */
	public static boolean compareFields(SHField refSHf, double scale1, SHField shf2, double scale2, double maxRelPotErr)
			throws OutOfApertureException, KnownFatalException {

		PotentialProvider pp1 = new SHFieldEvaluatorImpl(refSHf, 1.0);
		PotentialProvider pp2 = new SHFieldEvaluatorImpl(shf2, 1.0);

		List<Vector3D> points = SHTestUtils.getTestPointsInsideField(100);
		for (Vector3D p : points) {
			double[][] a1 = pp1.getPotentialsWithDerivatives(p);
			double[][] a2 = pp2.getPotentialsWithDerivatives(p);

			for (int i = 0; i < a1.length; i++) {
				double diff = a1[i][0] - a2[i][0];
				if (a1[i][0] != 0) {
					double relErr = Math.abs(diff / a1[i][0]);
					// System.out.println(relErr);
					assertTrue(relErr < maxRelPotErr); // TODO: The error limit is only approximative, but why ?
				}
			}
		}
		System.out.println("Comparison of two SHField is O.K.");
		return true;
	}

	/**
	 * Compares the sum of two SHFields with possibly different Lmax with a third
	 * reference.
	 * 
	 * @param shf1         SHField 1 to compare
	 * @param scale1       Scaling factor for shf1
	 * @param shf2         SHField 2 to compare
	 * @param scale2       Scaling factor for shf2
	 * @param sumFied      Reference for comparison.
	 * @param maxRelPotErr Relative error of the potentials allowed for the
	 *                     comparison.
	 * 
	 * @return Returns True if the sum of the two SHField are the same after scaling
	 *         as the third, within the given relative error.
	 * @throws KnownFatalException
	 * @throws OutOfApertureException
	 */
	public static boolean compareSummedFields(SHField shf1, double scale1, SHField shf2, double scale2, SHField sumFied,
			double maxRelPotErr) throws OutOfApertureException, KnownFatalException {

		PotentialProvider pp1 = new SHFieldEvaluatorImpl(shf1, scale1);
		PotentialProvider pp2 = new SHFieldEvaluatorImpl(shf2, scale2);
		PotentialProvider pp3 = new SHFieldEvaluatorImpl(sumFied, 1.0);

		List<Vector3D> points = SHTestUtils.getTestPointsInsideField(100);
		double maxRelErr = 0;
		for (Vector3D p : points) {
			double[][] a1 = pp1.getPotentialsWithDerivatives(p);
			double[][] a2 = pp2.getPotentialsWithDerivatives(p);
			double[][] a3 = pp3.getPotentialsWithDerivatives(p);

			for (int i = 0; i < a1.length; i++) {
				double diff = a1[i][0] + a2[i][0] - a3[i][0];
				if (a3[i][0] != 0) {
					double relErr = Math.abs(diff / a3[i][0]);
					if (maxRelErr < relErr)
						maxRelErr = relErr;
					// System.out.println(relErr);
					assertTrue(relErr < maxRelPotErr); // TODO: The error limit is only approximative, but why ?
				}
			}
		}
		System.out.println("Comparison of the sum of two SHField with a third one is O.K. Max. rel. er= " + maxRelErr);
		return true;
	}

	@Test
	/**
	 * Prints out the Lmaxes of each ball in the input field to see the spatial
	 * distribution of maximum degrees. It should decrease as we go further from the
	 * sources
	 * 
	 * @param shf -The SH field
	 * @throws IOException
	 */
	public static void lMaxesToFile(SHField shf, String outFile) throws IOException {

		StringBuffer sb = new StringBuffer();
		for (SHBall b : shf.getSHBalls()) {
			Vector3D c = b.getCentre();
			sb.append(c.getX() + " " + c.getY() + " " + c.getZ() + " " + b.getMaximumDegree() + " "
					+ b.getNumberOfFunctions() + System.lineSeparator());
		}
		FileUtils.writeTextFile(outFile, sb.toString());
	}
}
