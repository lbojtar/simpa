package simpa.core.sh;



import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.Evaluator;
import simpa.core.api.PointSource;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.cover.SphereDescriptionImpl;

public class SHBallEvaluatorTest {

	SHBall b;
	List<PointSource> psl;

	@Test	
	public void testIrregular() throws KnownFatalException, OutOfApertureException {
		testIrregular(true);
		testIrregular(false);
	}

	@Test
	public void testRegular() throws KnownFatalException, OutOfApertureException {
		testRegular(true);
		testRegular(false);
	}


	public void testIrregular(boolean magnetic) throws KnownFatalException, OutOfApertureException {

		init(magnetic,false);
		int nfunc = 1;
		if (magnetic)
			nfunc = 3;

		IrregularSHBallEvaluator ballEv = new IrregularSHBallEvaluator(b.getMaximumDegree(), nfunc);
		ballEv.setSHBall(b);

		Vector3D evalPoint = new Vector3D(1, 2.1, 0.1);
		evalPoint = evalPoint.normalize().scalarMultiply(4 + 1E-15);// outside the ball

		Evaluator ev = new Evaluator();
		List<Vector3D> tl = new ArrayList<>();
		tl.add(evalPoint);

		double[][] exact = ev.evaluateAtTargets(psl, tl)[0];
		double[][] est = ballEv.getPotentials(evalPoint);
		check(exact, est);
	}

	public void testRegular(boolean magnetic) throws KnownFatalException, OutOfApertureException {

		init(magnetic,true);
		int nfunc = 1;
		if (magnetic)
			nfunc = 3;

		SHBallEvaluator ballEv = new SHBallEvaluator(b.getMaximumDegree(), nfunc);
		ballEv.setSHBall(b);

		Vector3D evalPoint = new Vector3D(0.1, 0.21, 0.1); // inside the ball

		Evaluator ev = new Evaluator();
		List<Vector3D> tl = new ArrayList<>();
		tl.add(evalPoint);

		double[][] exact = ev.evaluateAtTargets(psl, tl)[0];
		double[][] est = ballEv.getPotentials(evalPoint);

		check(exact, est);

	}

	private void check(double[][] exact, double[][] est) {
		double relErr;
		for (int i = 0; i < exact.length; i++) {
			for (int j = 0; j < 4; j++) {

				if (exact[i][j] != 0)
					relErr = Math.abs((est[i][j] - exact[i][j]) / exact[i][j]);
				else {
					relErr = Math.abs(est[i][j] - exact[i][j]);
				}
				System.out.println("Estimate: " + est[i][j] + "  Exact: " + exact[i][j] + " Relative err: " + relErr);
				assertTrue(relErr < 1E-12);
			}
		}
	}

	private void init(boolean magnetic, boolean regular) {
		PointSource m;

		Vector3D location = new Vector3D(0.3, 0.202344, 0.11); // inside the ball
		if (regular)
			location = new Vector3D(1.3, 0.202344, 0.11); // outside the ball

		if (magnetic) {
			m = new CurrentPointSource(location);
			((CurrentPointSource) m).setLocalCurrentComponents(1, 2, 3);
		} else {
			m = new ElectricMonopole(location, 1.0);
		}

		SHCoefficientsCalculator shc = null;
		int maxDegree = 29;
		try {
			shc = new SHCoefficientsCalculator(maxDegree, 1e-16);
		} catch (IOException e) {
			e.printStackTrace();
		}

		SphereDescriptionImpl sd = new SphereDescriptionImpl(new Vector3D(0.03, 0.04, 0.01), 0.7,maxDegree, 0);
		List<SphereDescriptionImpl> sdl = new ArrayList<>();
		sdl.add(sd);
		psl = new ArrayList<>();
		psl.add(m);
		b = shc.createAllBallsWithFMM(psl, sdl)[0];
	}
}
