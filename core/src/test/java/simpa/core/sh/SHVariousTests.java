// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.sh;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.SphericalCoordinates;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.exceptions.OutOfApertureException;

public class SHVariousTests {

	private static String[] HEADER_STRING1 = { "Approximated value = ", "Approximated X derivative = ",
			"Approximated Y derivative = ", "Approximated Z derivative = " };
	private static String[] HEADER_STRING2 = { "Ax", "Ay", "Az" };

	// TODO
	//@Test
	public void printIndexes() throws IOException {
		int maxDegree=50;
		int idx = 0;
		for (int m = 0; m <= maxDegree; m++) {
			for (int l = m; l <= maxDegree; l++) {
				System.out.println("l=" + l + " m=" + m + " idx=" + idx);
				idx++;
			}
		}
	}

	// @Test
	public void testForwardAndInverseWithPolynomialAtRamdomPoints() throws IOException {
		int nOfCheckPoints = 1000;
		forwardAndInverseWithPolynomialAtRamdomPoints(nOfCheckPoints);
	}

	// @Test
	public void testForwardAndInverseWithSHCoeff() throws OutOfApertureException, IOException {
		// set coefficients then do evaluation, then do a forward transform on the
		// evaluated data
		// it should give back the original coefficients we set

		SHCoefficientsCalculator sht = null;
		int LMAX = 3;
		try {
			sht = new SHCoefficientsCalculator(LMAX, 1e-18);
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<Vector3D> qps = (new TDesign()).getQuadraturaPoints(LMAX);
		SHBall ball = new SHBall(LMAX, 1, 1, Vector3D.ZERO, 0);

		int idx = ball.getIndex(2, 1);
		ball.getCoefficients()[0][2 * idx] = 1; // Ax

		SHBallEvaluator ev = new SHBallEvaluator(LMAX, 1);
		ev.setSHBall(ball);

		// The values of the function to be transformed at the quadratura points
		double[][] samples = new double[qps.size()][1];

		for (int i = 0; i < qps.size(); i++) {
			Vector3D qp = qps.get(i);
			samples[i][0] = ev.getPotentials(qp)[0][0];// Ax
		}

		SHBall c2 = sht.createBall(samples, Vector3D.ZERO, 1, 0);// coeffs for unit ball centered zero
		for (int i = 0; i < ball.getCoefficients()[0].length; i++) {

			System.out
					.println("coeff = " + ball.getCoefficients()[0][i] + " Original coeff = "
							+ c2.getCoefficients()[0][i]);
		}

	}

	// @Test
	public void testSingleBallSpeed() {
		int maxDegree = 12;// LMAX;
		try {
			testSingleBallSpeed(maxDegree);
		} catch (OutOfApertureException e) {
			e.printStackTrace();
		}
	}

	// @Test
	// public void testRandomBallSpeed() {
	// testRandomBallSpeed(LMAX);
	// }

	// @Test
	public void testWithSpecialCases() throws IOException {

		List<Vector3D> evalPoints = new ArrayList<>();

		evalPoints.add(new Vector3D(0, 0, 1E-8));

		evalPoints.add(new Vector3D(1, 0, 0));
		evalPoints.add(new Vector3D(0, 1, 0));
		evalPoints.add(new Vector3D(0, 0, 1));

		evalPoints.add(new Vector3D(-1, 0, 0));
		evalPoints.add(new Vector3D(0, -1, 0));
		evalPoints.add(new Vector3D(0, 0, -1));

		evalPoints.add(new Vector3D(0, 0, 0));

		evalPoints.add(new Vector3D(0, 0.9, 1E-8));
		evalPoints.add(new Vector3D(0.9, 0, 1E-8));

		evalPoints.add(new Vector3D(1 / Math.sqrt(3), 1 / Math.sqrt(3), 1 / Math.sqrt(3)));
		evalPoints.add(new Vector3D(0.1, 0.2, 0.3));

		assertTrue(testForwardAndInverseWithPolynomial(evalPoints, Vector3D.ZERO));

	}

	private void testSingleBallSpeed(int maxDegree) throws OutOfApertureException { // all data in the cache in most CPU
		SHBall ball = new SHBall(maxDegree, 1, 1, Vector3D.ZERO, 0);

		SHBallEvaluator ev = new SHBallEvaluator(maxDegree, 4);
		ev.setSHBall(ball);

		int stepsPerTurn = 3000;
		int turns = 50;
		int n = stepsPerTurn * turns; // steps per turn, depends on many things

		Vector3D[] points = new Vector3D[n];

		for (int i = 0; i < n; i++) {
			points[i] = (new Vector3D(Math.random() * 0.3, Math.random() * 0.3, Math.random() * 0.3));

		}

		long t0 = System.currentTimeMillis();

		for (int i = 0; i < n; i++) {
			ev.getPotentials(points[i]);
		}

		long t1 = System.currentTimeMillis();
		System.out.println("SPEED TEST: " + n / (t1 - t0) + " evaluation/ ms ");
	}

	private void forwardAndInverseWithPolynomialAtRamdomPoints(int nOfCheckPoints) throws IOException {
		List<Vector3D> evalPoints = new ArrayList<>();

		Vector3D shift = new Vector3D(0, 0, 0);

		// Create test points on a sphere with radius r
		for (int i = 0; i < nOfCheckPoints; i++) {
			SphericalCoordinates sc = new SphericalCoordinates(Math.random(), Math.random() * Math.PI * 2,
					Math.random() * Math.PI);
			evalPoints.add(sc.getCartesian().add(shift));
		}

		assertTrue(testForwardAndInverseWithPolynomial(evalPoints, shift));
	}

	private boolean testForwardAndInverseWithPolynomial(List<Vector3D> evalPoints, Vector3D shift) throws IOException {

		SHCoefficientsCalculator sht = null;
		int LMAX = 3;
		try {
			sht = new SHCoefficientsCalculator(LMAX, 1e-18);
		} catch (IOException e) {
			e.printStackTrace();
		}
		List<Vector3D> qps = (new TDesign()).getQuadraturaPoints(LMAX);
		double[][] samples = new double[qps.size()][1]; // The values of the function to be transformed at the

		// values at quadratura points
		for (int i = 0; i < qps.size(); i++) {
			Vector3D qp = qps.get(i);
			samples[i][0] = getPolinomialWithDerivatives(qp)[0][0]; // Ax only
			// samples[i][0] = getPotentialsFromTestBall(qp)[0][0]; // Ax only

		}

		SHBall ball = sht.createBall(samples, shift, 1, 0); // coeffs for unit ball centered at shift
		SHBallEvaluator ev = new SHBallEvaluator(LMAX, 1);
		ev.setSHBall(ball);
		// values at evaluation points
		double[][][] exactValues = new double[evalPoints.size()][][];

		for (int i = 0; i < evalPoints.size(); i++) {
			Vector3D ep = evalPoints.get(i).add(shift);
			exactValues[i] = getPolinomialWithDerivatives(ep);
			// exactValues[i] = getPotentialsFromTestBall(ep);
		}
		boolean b = false;
		try {
			b = compareEvaluatedValues(evalPoints, exactValues, ev, false, 1E-14);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return b;
	}

	private double[][] getPolinomialWithDerivatives(Vector3D ep) {
		double[][] res = new double[1][4];
		double x = ep.getX();
		double y = ep.getY();
		double z = ep.getZ();

		// this is Ylm[1,-1]+Ylm[1,0]+ Ylm[1,1]+Ylm[2,-1] Real solid harmonic in
		// cartesian form
		res[0][0] = Math.sqrt(3 / (4 * Math.PI)) * (x + y + z) + 0.5 * Math.sqrt(15 / Math.PI) * x * y;// value of the
																										// polinomial

		res[0][1] = Math.sqrt(3 / (4 * Math.PI)) + 0.5 * Math.sqrt(15 / Math.PI) * y; // x derivative
		res[0][2] = Math.sqrt(3 / (4 * Math.PI)) + 0.5 * Math.sqrt(15 / Math.PI) * x; // y derivative
		res[0][3] = Math.sqrt(3 / (4 * Math.PI)); // z derivative

		return res;
	}

	public static boolean compareEvaluatedValues(List<Vector3D> evalPoints, double[][][] exactValues,
			SHBallEvaluator ev, boolean relative, double maxAlowedErr) throws OutOfApertureException {

		boolean passed = true;

		int nFunctions = ev.getSHBall().getNumberOfFunctions();

		double[][] maxErr = new double[nFunctions][4];

		String errs;

		if (relative) {
			errs = " relative error ";
		} else {
			errs = " absolute error ";
		}

		for (int pi = 0; pi < evalPoints.size(); pi++) {

			Vector3D ep = evalPoints.get(pi);
			for (int fi = 0; fi < nFunctions; fi++) {

				System.out.println(System.lineSeparator() + HEADER_STRING2[fi]);

				for (int di = 0; di < 4; di++) {
					double[][] estimated = ev.getPotentials(ep);
					double vApprox = estimated[fi][di];

					if (Double.isNaN(vApprox))
						passed = false;

					double diff;

					if (relative) {
						diff = Math.abs(vApprox - exactValues[pi][fi][di]) / exactValues[pi][fi][di];
					} else {
						diff = Math.abs(vApprox - exactValues[pi][fi][di]);

					}

					System.out
							.println(HEADER_STRING1[di] + vApprox + " exact= " + exactValues[pi][fi][di] + errs + diff);
					if (diff > maxErr[fi][di])
						maxErr[fi][di] = diff;

				}
			}
		}

		if (!passed) {
			System.out.println("Some value was NaN, probably due to division by zero !");
			return false;
		}

		System.out.println();

		for (int fi = 0; fi < nFunctions; fi++) {
			for (int di = 0; di < 4; di++) {

				System.out.println(
						"Maximum" + errs + " of the value or derivative of function_" + fi + " =" + maxErr[fi][di]);
				if (maxErr[fi][di] > maxAlowedErr)
					return false;
			}
			System.out.println();
		}

		return passed;
	}

	public static double[][] getPotentialsFromTestBall(Vector3D pos) throws OutOfApertureException {

		SHBall b = new SHBall(5, 1, 10, Vector3D.ZERO, 0);

		int idx = b.getIndex(2, 1);
		b.getCoefficients()[0][2 * idx] = 1; // Ax sin part

		idx = b.getIndex(2, 2);
		b.getCoefficients()[0][2 * idx + 1] = 1; // Ax cos part
		SHBallEvaluator bbev = new SHBallEvaluator(b.getMaximumDegree(), 1);
		bbev.setSHBall(b);
		return bbev.getPotentials(pos);
	}

}
