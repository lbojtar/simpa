/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.sh;



import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.Test;


public class SHFieldSerializerTest {
	
	boolean isMagnetic = true; //or electric
	int initialLmax = 20;
	
	@Test
	public void test() throws FileNotFoundException {
		String outputFile = System.getProperty("java.io.tmpdir") + "/testSHField_20.bin";
		SHField shField = SHTestUtils.getTestSHField(initialLmax,1e-14,"stat1e14.txt",true,isMagnetic);
		SHFieldSerializer.serializeSHField(shField, outputFile);

		SHField newShField = null;
		try {
			newShField = SHFieldSerializer.deserializeSHField(outputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertTrue(SHTestUtils.compareFieldsWithEqualLmax(shField, newShField));
	}

	
}
