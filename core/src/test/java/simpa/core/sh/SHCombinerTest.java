package simpa.core.sh;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;

public class SHCombinerTest {

	double PRECISION=1e-12;
	
	SHField d20MField; //magnetic field map degree 20, keeping all coefficients
	SHField d20MCopy; // copy of above
	SHField d12MField;// magnetic field map max degree 12, keeping  coefficients only to get at least the PRECISION
	
	//electric variants of the above
	SHField d20EField; 
	SHField d20ECopy; 
	SHField d12EField;
	
	String m20File = System.getProperty("java.io.tmpdir") + "/testSHField_M_20_full.bin";
	String m12File = System.getProperty("java.io.tmpdir") + "/testSHField_M_12_1e-12.bin";
	String e20File = System.getProperty("java.io.tmpdir") + "/testSHField_E_20_full.bin";
	String e12File = System.getProperty("java.io.tmpdir") + "/testSHField_E_12_1e-12.bin";
	String mixedFile = System.getProperty("java.io.tmpdir") + "/testSHField_mixed.bin";
	String mixed2File = System.getProperty("java.io.tmpdir") + "/testSHField_mixed2.bin"; //two is not the Lmax, just to distinguish from mixedFile
	String mixed12File = System.getProperty("java.io.tmpdir") + "/testSHField_mixed12.bin"; //Lmax 12, mixed balls
	
	public SHCombinerTest() throws IOException, OutOfApertureException, KnownFatalException {
		init();
	}


	public void init() throws IOException, OutOfApertureException, KnownFatalException {
		//create magnetic field maps
		d20MField = SHTestUtils.getTestSHField(20, 0, "statM20.txt", true, true);
		SHFieldSerializer.serializeSHField(d20MField, m20File);
		d20MCopy = SHFieldSerializer.deserializeSHField(m20File);
		assertTrue(SHTestUtils.compareFields(d20MField, 1.0, d20MCopy, 1.0, 1e-16));		
		d12MField = SHTestUtils.getTestSHField(12, PRECISION, "statM12.txt", true, true);
		SHFieldSerializer.serializeSHField(d12MField, m12File);
		
		//create electric field maps
		d20EField = SHTestUtils.getTestSHField(20, 0, "statE20.txt", false, false);
		SHFieldSerializer.serializeSHField(d20EField, e20File);
		d20ECopy = SHFieldSerializer.deserializeSHField(e20File);
		assertTrue(SHTestUtils.compareFields(d20EField, 1.0, d20ECopy, 1.0, 1e-16));		
		d12EField = SHTestUtils.getTestSHField(12, PRECISION, "statE12.txt", false, false);
		SHFieldSerializer.serializeSHField(d12EField, e12File);
	}

	@Test
	public void testLmaxCut() throws Exception {
		assertTrue(SHTestUtils.compareFields(d20MField, 1.0, d12MField, 1.0, 1e-11));
		assertTrue(SHTestUtils.compareFields(d20EField, 1.0, d12EField, 1.0, 1e-11));
	}

	//@Test
	/**
	 * Saves the Lmaxes of each ball in the  fields
	 * to see the spatial distribution of maximum degrees. 
	 * It should decrease as we go further from the sources	
	 */
	public void writeLMaxesDistr() throws IOException{	
		SHField shf = SHFieldSerializer.deserializeSHField(e12File);
		SHTestUtils.lMaxesToFile(shf,e12File+".lmaxDistr");
		
		shf = SHFieldSerializer.deserializeSHField(m12File);
		SHTestUtils.lMaxesToFile(shf,m12File+".lmaxDistr");
		
		shf = SHFieldSerializer.deserializeSHField(mixed12File);
		SHTestUtils.lMaxesToFile(shf,mixed12File+".lmaxDistr");
	}
	
	@Test
	public void testScalingM() throws Exception {
		 testScaling(m20File, m12File);
	}
	
	@Test
	public void testScalingE() throws Exception {
		 testScaling(e20File, e12File);
	}
	
	@Test
	public void testScalingME() throws Exception {
		 testScaling(m20File, e12File);
	}
	
	@Test
	public void testScalingEM() throws Exception {
		 testScaling(e20File, m12File);
	}
	
	@Test
	public void testScalingMixed() throws Exception {
		
		SHField mixed= testScaling(e20File, m12File); 
		SHFieldSerializer.serializeSHField(mixed, mixedFile);
		testScaling(mixedFile, m12File);//ME+M
		
		SHField mixed2=testScaling(mixedFile, e12File);//ME+E
		SHFieldSerializer.serializeSHField(mixed2, mixed2File);
		testScaling(mixedFile, mixed2File);//ME+ME
		
		SHField mixed12= testScaling(e12File, m12File); 
		SHFieldSerializer.serializeSHField(mixed12, mixed12File);
		testScaling(e12File,mixed12File);//E+ME
		testScaling(m12File,mixed12File);//M+ME
	}
	
	
	private SHField testScaling(String f1, String f2) throws Exception {

		SHCombinerImpl shc = new SHCombinerImpl();
		Map<String, Double> map = new HashMap<>();

		// scaling and compare
		double s1 = 2.0;
		double s2 = 3.0;
		SHField fm1=SHFieldSerializer.deserializeSHField(f1);
		SHField fm2=SHFieldSerializer.deserializeSHField(f2);
		map.put(f1, Double.valueOf(s1));
		map.put(f2, Double.valueOf(s2));
		SHField combinedField = shc.getSHField(map);
		assertTrue(SHTestUtils.compareSummedFields(fm1, s1, fm2, s2, combinedField, 1e-11));
		return combinedField;

	}


}
