/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class HCPLatticeTest {

    private double rad = 0.1;

    // test the consistency of the getSphereCenter and getIndexes methods
    @Test
    public void test1() {

        int[] ia = new int[3];
        Vector3D p;
        int[] res;
        boolean b;
        HCPLattice l = new HCPLattice(rad);

        // some particular case
        ia[0] = -313;
        ia[1] = 1;
        ia[2] = -185;
        p = l.getSphereCenter(ia[0], ia[1], ia[2]);
        res = l.getNearbyIndexes(p);
        b = ia[0] == res[0] && ia[1] == res[1] && ia[2] == res[2];
        assertTrue(b);

        // random indexes
        int n = 100000;
        for (int i = 0; i < n; i++) {
            ia[0] = (int) ((Math.random() - 0.5) * 1000);
            ia[1] = (int) ((Math.random() - 0.5) * 1000);
            ia[2] = (int) ((Math.random() - 0.5) * 1000);
            p = l.getSphereCenter(ia[0], ia[1], ia[2]);

            res = l.getNearbyIndexes(p);
            b = ia[0] == res[0] && ia[1] == res[1] && ia[2] == res[2];
            assertTrue(b);
        }

    }

    @Test // to visualize the lattice uncomment the @Test annotation
    public void test_visualize() {
        int n = 1;

        HCPLattice l = new HCPLattice(1, false);

        for (int zi = -n; zi <= n; zi++) {
            for (int yi = -n; yi <= n; yi++) {
                for (int xi = -n; xi <= n; xi++) {
                    Vector3D lp = l.getSphereCenter(xi, yi, zi);
                    // System.out.println(lp.getX() + " " + lp.getY() + " " + lp.getZ());
                    l.printParaviewScript(xi, yi, zi, lp);

                }
            }
        }
    }

}
