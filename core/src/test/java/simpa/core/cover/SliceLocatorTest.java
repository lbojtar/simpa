package simpa.core.cover;



import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.partitioning.Region.Location;
import org.junit.jupiter.api.Test;

import simpa.core.api.Profile;


public class SliceLocatorTest {

    @Test
    public void test() {
        Profile p1 = new Profile(0.03, 0.03, 10, 0);
        Profile p2 = p1.getTransformedCopy(new Vector3D(0, 0, 0.1), Rotation.IDENTITY);
 
        SliceLocator sbr = new SliceLocator(p1,p2);
        assertTrue( sbr.locate(Vector3D.ZERO)==Location.BOUNDARY); 
        assertTrue( sbr.locate(new Vector3D(0,0,0.01))==Location.INSIDE); 
        assertTrue( sbr.locate(new Vector3D(0.001,0.0015,1.0))==Location.OUTSIDE); 
    }
}
