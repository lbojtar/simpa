/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.SphereDescripton;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.track.StlAperture;
import simpa.core.api.utils.FileUtils;
import simpa.core.sh.SHBall;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;



public class LatticeSearchTest {

    private SHBall[] ba;
    private LatticeSearch ls;
    private StlAperture aperture;

    private void initCover() {

        // InputStream is = Utils.class.getResourceAsStream(Constants.RESOURCE_PATH + "HCP_LATTICE_COVER.txt");

        InputStream is = null;
        try {
            is = new FileInputStream("HCP_LATTICE_COVER.txt");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        double[][] da = FileUtils.read2DArray(is, 4);

        ba = new SHBall[da.length];
        for (int i = 0; i < ba.length; i++) {
            double ballRadius = da[i][3];

            Vector3D center = new Vector3D(da[i][0], da[i][1], da[i][2]);

            ba[i] = new SHBall(null, 0,center, ballRadius,i);
        }

        long t0 = System.currentTimeMillis();
        ls = new LatticeSearch(ba);
        long t1 = System.currentTimeMillis();

//        String tempDir = System.getProperty("java.io.tmpdir");
//        FileUtils.copyResourceFile("/beamregion.stl", tempDir + "/beamregion.stl");
        aperture = new StlAperture("beamregion.stl");

        System.out.println("Aperture created in: " + (t1 - t0) + " [ms] for n= " + ba.length + " balls");
    }

    @Test
    public void test_getSphereIndex() throws KnownFatalException {
        int ntests = 1000000;
        double rad = 1;
        int n = 6; // range
        int xi = 0;
        int yi = 0;
        int zi = 0;

        List<SphereDescripton> sdList = new ArrayList<>();
        HCPLattice l = new HCPLattice(rad);

        for (int dzi = -n; dzi <= n; dzi++) {
            for (int dyi = -n; dyi <= n; dyi++) {
                for (int dxi = -n; dxi <= n; dxi++) {
                    Vector3D c = l.getSphereCenter(xi + dxi, yi + dyi, zi + dzi);
                    SphereDescripton sd = new SphereDescripton(c, rad);
                    sdList.add(sd);
                }
            }
        }

        ls = new LatticeSearch(sdList);

        Vector3D sc = l.getSphereCenter(xi, yi, zi); // center of search

        Vector3D p = new Vector3D(0.6760191812, 0.5896775903, 1.8161784868);
        int idx = ls.getSphereIndex(p);

        assertTrue(idx != -1);

        long t0 = System.currentTimeMillis();

        for (int i = 0; i < ntests; i++) {
            double x = rad * (Math.random() - 0.5) * 5;
            double y = rad * (Math.random() - 0.5) * 5;
            double z = rad * (Math.random() - 0.5) * 5;
            p = sc.add((new Vector3D(x, y, z)));
            idx = ls.getSphereIndex(p);
            assertTrue(idx != -1);
           
        }

        long t1 = System.currentTimeMillis();
        System.out.println("Searched in: " + (t1 - t0) + " [ms] for n= " + ntests);
    }

  //  @Test
    public void testOnePoint() {
        initCover();
        Vector3D p = new Vector3D(-3.6628229109, 0.018992257, 3.2588380234);
        // Vector3D p = new Vector3D(4.4019263752, -0.0218592989, 2.3646442496);
        if (aperture.isInside(p))
            System.out.println(p + " is inside the aperture");
        int i = ls.getSphereIndex(p);
        System.out.println("Inside in sphere " + i);

    }

  

}
