/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.cover;

import static org.junit.jupiter.api.Assertions.fail;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.partitioning.Region;
import org.apache.commons.math3.geometry.partitioning.Region.Location;
import org.junit.jupiter.api.Test;

import simpa.core.api.SphereDescripton;
import simpa.core.api.SystemConstants;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.TextStlReader;

public class VolumeFillTest {
    private static final String STL_FILE_NAME = "beamregion.stl";

   
    public VolumeFillTest() {
        FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + STL_FILE_NAME, "/tmp/" + STL_FILE_NAME);
    }
    

    @Test
    public void volumeFillTest() throws IOException, KnownFatalException {
        String outputFile = "/tmp/allSpheres.txt";
        double ballRadius = 0.01;
        HCPLattice lattice = new HCPLattice(ballRadius);
        TextStlReader stlr = new TextStlReader();

        // this represents the boundary surface
        stlr.readStlFile(new FileInputStream("/tmp/"+STL_FILE_NAME));

        VolumeFill vf = new VolumeFill(stlr.getVertices(),stlr.getFaces(), lattice );

        List<SphereDescripton> sphereDescriptonList = vf.getSpheres();

       // sphereDescriptonList = vf.fillGaps(sphereDescriptonList);

        StringBuilder sb = new StringBuilder();
        sb.append("#X Y Z RADIUS" + System.lineSeparator());

        sphereDescriptonList.forEach(sd -> {
            Vector3D c = sd.getCentre();
            sb.append(c.getX() + " " + c.getY() + " " + c.getZ() + " " + sd.getRadius() + System.lineSeparator());
        });

        FileUtils.writeTextFile(outputFile, sb.toString());

    }

 
    @Test
    public void coverTest() throws IOException, KnownFatalException {

        int amountOfTestPoints = 1000000;
        Vector3D[] testPoints = new Vector3D[amountOfTestPoints];

        double ballRadius = 0.01;
        HCPLattice lattice = new HCPLattice(ballRadius);
        TextStlReader stlr = new TextStlReader();

        stlr.readStlFile(new FileInputStream("/tmp/"+STL_FILE_NAME));

        VolumeFill vf = new VolumeFill(stlr.getVertices(),stlr.getFaces(), lattice );
        List<SphereDescripton> sphereDescriptionList = vf.getSpheres();

        for (int i = 0; i < amountOfTestPoints; i++) {
            double x = (Math.random() * (vf.maxX - vf.minX) + vf.minX);
            double y = (Math.random() * (vf.maxY - vf.minY) + vf.minY);
            double z = (Math.random() * (vf.maxZ - vf.minZ) + vf.minZ);
            testPoints[i] = new Vector3D(x, y, z);
        }

       // sphereDescriptionList = vf.fillGaps(sphereDescriptionList);

        LatticeSearch ls = new LatticeSearch(sphereDescriptionList);

        for (int i = 0; i < amountOfTestPoints; i++) {
            Location l=vf.getLocator().locate(testPoints[i]);
            if (l == Region.Location.INSIDE || l == Region.Location.BOUNDARY) {
                int sphereIndex = ls.getSphereIndex(testPoints[i]);
                if (sphereIndex == -1) {
                    fail("Inside or boundary point not covered by sphere");
                }
            }
        }
    }
}
