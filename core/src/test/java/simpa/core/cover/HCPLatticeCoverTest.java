/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.ExtrudedSurface;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.Profile;
import simpa.core.api.SphereDescripton;
import simpa.core.api.SystemConstants;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.SimpleTriangulator;
import simpa.core.api.utils.TextStlWriter;
import simpa.core.api.utils.Triangulation;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HCPLatticeCoverTest {

    private static final String STL_FILE_NAME = "beamregion.stl";
    private static final String COVER_FILE = "/tmp/beamregion" + FileNamingConventions.HCP_COVER_FILE_EXTENSION;

    public HCPLatticeCoverTest() {
        FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + STL_FILE_NAME, "/tmp/" + STL_FILE_NAME);
    }

   // @Test
    public void singleSliceTest() throws IOException, KnownFatalException {      
        double ballRadius = 0.1;
       
        Profile pr1 = new Profile(-1, 1, -1, 1, 10, 10);
        Profile pr2 = pr1.getTransformedCopy(Vector3D.PLUS_K,Rotation.IDENTITY);
        List<Profile> profiles= List.of(pr1,pr2);
        HCPLatticeCoverImpl cover = new HCPLatticeCoverImpl(profiles,ballRadius,false);
        for(SphereDescripton d: cover.getCoveringSpheres()) {
        //	assertTrue(d.getCentre().getX()<1);
   
        }
        cover.toFile("/tmp/sliceCover.txt");
        
    }
    
    
    @Test
    public void test_create() {
        long t0 = System.currentTimeMillis();
        HCPLatticeCoverImpl cover = null;
        try {
            cover = new HCPLatticeCoverImpl("/tmp/" + STL_FILE_NAME, 0.01);
        } catch (KnownFatalException e) {
            e.printStackTrace();
        }

        cover.toFile(COVER_FILE);

        long t1 = System.currentTimeMillis();
        System.out.println("3D fill speed: " + (t1 - t0) + " [ms]");
    }



    @Test
    public void slicingTest() throws IOException, KnownFatalException {
        Profile p = new Profile(0.03, 0.03, 10, 0);
        List<Profile> l = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            Vector3D translation = new Vector3D(i * 0.05, i * 0.07, i * 0.2);
            Profile copy = p.getTransformedCopy(translation, Rotation.IDENTITY);
            l.add(copy);
        }

        HCPLatticeCoverImpl cover = new HCPLatticeCoverImpl(l, 0.01, false);
        cover.toFile("/tmp/sliceTest" + FileNamingConventions.HCP_COVER_FILE_EXTENSION);

        // the corresponding stl file
        SimpleTriangulator triangulator = new SimpleTriangulator(l, false);
        Triangulation t = triangulator.getSurfaceWithEndCups();
        TextStlWriter.writeSTLTextFile(t, "/tmp/sliceTest.stl");

    }

    @Test
    public void spiralTest() throws IOException, KnownFatalException {
        Profile p = new Profile(0.03, 0.03, 10, 0);
        List<Vector3D> path = new ArrayList<>();

        for (int i = 0; i < 400; i++) {
            double x = 0.15 *  Math.cos(i * 0.1);
            double y = 0.15 *  Math.sin(i * 0.1);
            double z = 0.02 * i;
            path.add(new Vector3D(x, y, z));
        }
        ExtrudedSurface es = new ExtrudedSurface(path, p);
        List<Profile> l = es.getOrientedProfiles();
        HCPLatticeCoverImpl cover = new HCPLatticeCoverImpl(l, 0.01, false);
        cover.toFile("/tmp/spitalTest" + FileNamingConventions.HCP_COVER_FILE_EXTENSION);

        // the corresponding stl file
        SimpleTriangulator triangulator = new SimpleTriangulator(l, false);
        Triangulation t = triangulator.getSurfaceWithEndCups();
        TextStlWriter.writeSTLTextFile(t, "/tmp/spiralTest.stl");

    }

}
