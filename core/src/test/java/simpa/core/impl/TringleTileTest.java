package simpa.core.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.SurfacePoint;
import simpa.core.api.utils.FileUtils;

class TringleTileTest {

	List<Vector3D> l;
	
	
	@Test
	void test() {
		List<Vector3D> l= new ArrayList<>();
		List<Vector3D> l2= new ArrayList<>();
		Vector3D p1= new Vector3D(0,0,0); 
		Vector3D p2= new Vector3D(1,0,0); 
		Vector3D p3= new Vector3D(1,1,0); 
		l2.add(p1);
		l2.add(p2);
		l2.add(p3);
		l2.add(p1);
		Tile t= new TriangleTile(p1,p2,p3,0.1);				
		for(SurfacePoint p: t.getSurfacePoints()) {
			l.add(p.getLocation());
		}
		FileUtils.writeVectors("/tmp/tringlePoints.txt",l);
		FileUtils.writeVectors("/tmp/triangle.txt",l2);
		assertTrue(t.getNormalUnitVector().getZ()>0);
	}

}
