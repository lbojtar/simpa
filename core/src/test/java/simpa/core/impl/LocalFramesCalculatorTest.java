package simpa.core.impl;



import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.FileNotFoundException;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.LengthUnit;
import simpa.core.api.SystemConstants;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.LocalFrame;
import simpa.core.api.utils.LocalFramesCalculator;

public class LocalFramesCalculatorTest {

	// For planar transfer lines the rotation optimization strategy gives the same
	// result as the parallel transport only if the initial vector is correctly
	// chosen for the latter.
	@Test
	public void testPlanarLine() throws FileNotFoundException {
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + "planarPath.txt", "/tmp/planarPath.txt");
		List<Vector3D> curve = FileUtils.readVectors("/tmp/planarPath.txt", LengthUnit.M);
		LocalFramesCalculator lfc = new LocalFramesCalculator(curve, false,
				LocalFramesCalculator.Strategy.MINIMIZE_TORSION,null);

		// lfc.setPreferredVerticalDirection(Vector3D.MINUS_K); // should give an
		// Exception

		LocalFrame start = lfc.getLocalFrames().get(0);
		LocalFrame end = lfc.getLocalFrames().get(lfc.getLocalFrames().size() - 1);
	 //   lfc.writeToFile("/tmp/ppFrames.txt", 5);
		assertTrue(compareY(start, end));
		

		LocalFramesCalculator lfc3 = new LocalFramesCalculator(curve, false,
				LocalFramesCalculator.Strategy.PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION,
				(new Vector3D(-1, 1, 0)).normalize());
		end = lfc3.getLocalFrames().get(lfc3.getLocalFrames().size() - 1);
		lfc3.writeToFile("/tmp/ppFrames.txt", 5);
		assertTrue(compareY(start, end));
	}

	// A general 3D path
	 @Test
	public void test3DLine() throws FileNotFoundException {
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + "3dPath.txt", "/tmp/3dPath.txt");
		List<Vector3D> curve = FileUtils.readVectors("/tmp/3dPath.txt", LengthUnit.M);
		LocalFramesCalculator lfc = new LocalFramesCalculator(curve, false,
				LocalFramesCalculator.Strategy.PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION, null);

		lfc.writeToFile("/tmp/3dFrames.txt", 5);
	}

	 @Test
	public void testPlanarRing() throws FileNotFoundException {
		FileUtils.copyResourceFile(SystemConstants.TEST_RESOURCE_PATH + "orbit_design.txt", "/tmp/orbit_design.txt");
		List<Vector3D> curve = FileUtils.readVectors("/tmp/orbit_design.txt", LengthUnit.M);
		LocalFramesCalculator lfc = new LocalFramesCalculator(curve, false,
				LocalFramesCalculator.Strategy.MINIMIZE_TORSION, null);
		lfc.writeToFile("/tmp/ringFrames.txt", 5);

		LocalFrame start = lfc.getLocalFrames().get(0);
		LocalFrame end = lfc.getLocalFrames().get(lfc.getLocalFrames().size() - 1);
		assertTrue(compareY(start, end));
	}

	public boolean compareY(LocalFrame lf1, LocalFrame lf2) {
		boolean b = lf1.ly().subtract(lf2.ly()).getNorm() < SystemConstants.GEOMETRY_TOLERANCE;
		return b;
	}

}
