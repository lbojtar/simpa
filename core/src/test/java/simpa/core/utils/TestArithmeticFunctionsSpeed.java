// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.special.BesselJ;
import org.apache.commons.math3.special.BesselJ.BesselJResult;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestArithmeticFunctionsSpeed {

	@Test
	public void test1_BesselJSpeed() {

		int n = 10000;
		double[] r = new double[n];
		double summ = 0;

		for (int i = 0; i < n; i++) {
			r[i] = Math.random();
		}

		long st = System.currentTimeMillis();
		int order = 40;
		for (int i = 0; i < n; i++) {
			BesselJResult res = BesselJ.rjBesl(r[i], 0.5, order);
			if (res.getnVals() != order) {
				System.out.println("Upps, the thumber of values doesn't match the order!");
			}
			summ = summ + res.getVals()[0];
		}

		System.out.println(
				n + " BesselJ were calculated in " + (System.currentTimeMillis() - st) + " ms. The summ=" + summ);
	}

	@Test
	public void test3_BesselJ() {

		int order = 4;
		double x = 0.2345;

		BesselJResult res = BesselJ.rjBesl(x, 0.5, order + 1);

		double j = Math.sqrt(Math.PI / (2 * x)) * res.getVals()[order];
		// value from Mathematica
		assertTrue(Math.abs(j - 3.1919334438840565E-6) / j < 1E-16);

	}

	@Test
	public void test2_SqrtSpeed() {

		int n = 100000000;
		double[] r = new double[n];
		double summ = 0;

		for (int i = 0; i < n; i++) {
			r[i] = Math.random();
		}

		long st = System.currentTimeMillis();

		for (int i = 0; i < n; i++) {
			summ = summ + Math.sqrt(r[i]);
		}

		System.out.println(
				n + " Sqrt(x) were calculated in " + (System.currentTimeMillis() - st) + " ms. The summ=" + summ);
	}

	@Test
	public void testCrossProduct() {

		

		System.out.println(Vector3D.crossProduct(Vector3D.PLUS_J, Vector3D.PLUS_K));
	}
	
	
}
