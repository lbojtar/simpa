/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.junit.jupiter.api.Test;

import simpa.core.api.PhysicsConstants;
import simpa.core.api.track.Particle;
import simpa.core.api.utils.CalculatorUtils;

public class CalculatorUtilsTest {

    @Test //TODO: It's not precise, why ?
    public void test1_GevOcToE() {
        double p0InGeV = 0.1; //momentum in [GeV/c]
        Particle p = new Particle(Vector3D.ZERO, Vector3D.PLUS_I, Vector3D.ZERO, p0InGeV,
                PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE, PhysicsConstants.PROTON_MASS);
        double etot=CalculatorUtils.getTotalEnergy(p);
        double ev=CalculatorUtils.joulesToEv(etot);        
        System.out.println(p0InGeV + "[GeV/c] momentum corresponds to a total energy "
                + etot + " [Joule] for a proton, which is "+ev+" [eV]");
        
        double eKin=CalculatorUtils.getKineticEnergy(p);
        double eVKin=CalculatorUtils.joulesToEv(eKin);
        System.out.println("The kinetic energy is "+eKin+" [Joule], which is "+eVKin+" [eV]" );
    }
  
 
}
