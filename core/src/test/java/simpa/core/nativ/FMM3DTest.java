/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.nativ;

import org.junit.jupiter.api.Test;

import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;

import simpa.core.nativ.FMM3D;


public class FMM3DTest {

	@Test
	public void test() {
		IntByReference npots = new IntByReference(3);
		int ns=9000;
		int nt=9500;
		double[] sa= new double[3*ns];
		double[] charges= new double[npots.getValue()*ns];
		double[] ta= new double[3*nt];
		double[] pots= new double[npots.getValue()*nt];
		double[] gradients= new double[3*npots.getValue()*nt];
		
		for(int i=0;i<ns;i++) {			
			sa[i*3]=Math.random();
			sa[i*3+1]=Math.random();
			sa[i*3+2]=Math.random();
		}
		
		for(int i=0;i<nt;i++) {
			ta[i*3]=Math.random();
			ta[i*3+1]=Math.random();
			ta[i*3+2]=Math.random();
		}
		
		
		long t0=System.currentTimeMillis();
		
		DoubleByReference err = new DoubleByReference(1e-13);
		IntByReference nsources = new IntByReference(ns);
		IntByReference ntargets = new IntByReference(nt);
		IntByReference errorCode = new IntByReference();
		
		System.out.println ("Calculating potentials");
		FMM3D.INSTANCE.lfmm3d_t_c_p_vec_(npots,err, nsources, sa, charges, ntargets, ta, pots, errorCode);
		System.out.println ("Total time= "+( (System.currentTimeMillis()-t0)/1000)+ "  sec");
		System.out.println ("Error code = "+errorCode.getValue());
		
		System.out.println ("Calculating potentials and gradients");
		FMM3D.INSTANCE.lfmm3d_t_c_g_vec_(npots,err, nsources, sa, charges, ntargets, ta, pots,gradients, errorCode);
		System.out.println ("Total time= "+( (System.currentTimeMillis()-t0)/1000)+ "  sec");
		System.out.println ("Error code = "+errorCode.getValue());
	}
	
	//@Test
	public void test_OneByOne() {
		IntByReference npots = new IntByReference(3);
		int ns = 100000;
		int nt = 10;
		double[] sa = new double[3 * ns];
		double[] charges = new double[npots.getValue() * ns];
		double[] ta = new double[3 * nt];
		double[] pots = new double[npots.getValue() * nt];
		double[] gradients = new double[3 * npots.getValue() * nt];

		for (int i = 0; i < ns; i++) {
			sa[i * 3] = Math.random();
			sa[i * 3 + 1] = Math.random();
			sa[i * 3 + 2] = Math.random();
		}

		for (int i = 0; i < nt; i++) {
			ta[i * 3] = Math.random();
			ta[i * 3 + 1] = Math.random();
			ta[i * 3 + 2] = Math.random();
		}

		long t0 = System.currentTimeMillis();

		DoubleByReference err = new DoubleByReference(1e-13);
		IntByReference nsources = new IntByReference(ns);
		IntByReference ntargets = new IntByReference(1);
		IntByReference errorCode = new IntByReference();

		double[] sta = new double[3];
		for (int j = 0; j < nt; j++) {
			sta[0]=ta[j*3];
			sta[1]=ta[j*3+1];
			sta[2]=ta[j*3+2];
			System.out.println("Calculating potentials and gradients");
			FMM3D.INSTANCE.lfmm3d_t_c_g_vec_(npots, err, nsources, sa, charges, ntargets, sta, pots, gradients,
					errorCode);			
			System.out.println("Error code = " + errorCode.getValue());
		}
		
		System.out.println("Total time= " + ((System.currentTimeMillis() - t0) / 1000) + "  sec");
	}

}
