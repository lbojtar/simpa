/**
 * Library containing the core of the SIMPA Project.
 */
module simpa.core {
	
	exports simpa.core.api;
	exports simpa.core.api.track;
	exports simpa.core.api.utils;
	exports simpa.core.api.exceptions;

	requires transitive commons.math3;
	requires transitive com.sun.jna;
	requires org.apache.logging.log4j;
	requires org.apache.logging.log4j.core;
    requires cli;
	
	opens simpa.core.b2a to org.junit.jupiter;
	
}   