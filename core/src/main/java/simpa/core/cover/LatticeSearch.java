/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.SphereDescripton;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.sh.SHBall;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Implementation of the BallSearch interface that provides a method for
 * searching in a HCP lattice.
 */
public class LatticeSearch implements BallSearch {
    private static final Logger logger = LogManager.getLogger(LatticeSearch.class);

    private Map<Integer, Map<Integer, Map<Integer, Integer>>> xMap;
    private List<SphereDescripton> sdList = new ArrayList<>();
    private HCPLattice lattice;
    private final double radius;
    private int count;

    /**
     * Constructs the lattice search.
     *
     * @param balls the solid harmonic balls to be searched.
     */
    public LatticeSearch(SHBall[] balls) {
        this.radius = balls[0].getRadius();
        for (SHBall b : balls) {
            sdList.add(new SphereDescripton(b.getCentre(), b.getRadius()));
        }
        init();
    }

    /**
     * Constructs the lattice search.
     *
     * @param sdl the list of sphere descriptions to be searched.
     */
    public LatticeSearch(List<SphereDescripton> sdl) throws KnownFatalException {

        this.radius = sdl.get(0).getRadius();
        
        // make a deep copy to avoid modifying the argument
        this.sdList = new ArrayList<>();
        for(SphereDescripton sd:sdl) {
            Vector3D c= new Vector3D(sd.getCentre().getX(),sd.getCentre().getY(),sd.getCentre().getZ());
            double r=sd.getRadius();
            if(r != this.radius) {
                logger.error("LatticeSearch constructor : All sphere must have the same size !");
                throw new KnownFatalException("Spheres in LatticeSearch don't have the same size.");
            }
            this.sdList.add(new SphereDescripton(c, r));
        }
        init();
    }

    private void init() {
        lattice = new HCPLattice(radius, true);

        // check if the centers are on the lattice
        for (SphereDescripton sd : sdList) {
            isOnLatticePoint(sd.getCentre());
        }

        xMap = new TreeMap<>();

        for (SphereDescripton sd : sdList) {
            insert(sd);
        }
    }

    /**
     * Returns true if the search list contains the given SphereDescription.
     *
     * @param sd SphereDescription
     * @return boolean that tells whether the SphereDescription object is found
     */
    public boolean contains(SphereDescripton sd) {
        return sdList.contains(sd);
    }

    public void add(SphereDescripton sd) {
        sdList.add(sd);
        insert(sd);
    }

    /**
     * insert a new sphere into the lattice search
     */
    private void insert(SphereDescripton sd) {

        int[] indexes = lattice.getNearbyIndexes(sd.getCentre());
        if (xMap.containsKey(indexes[0])) {
            Map<Integer, Map<Integer, Integer>> yMap = xMap.get(indexes[0]);
            if (yMap.containsKey(indexes[1])) {
                Map<Integer, Integer> zMap = yMap.get(indexes[1]);
                zMap.put(indexes[2], count);
            } else {
                Map<Integer, Integer> zMap = new TreeMap<>();
                zMap.put(indexes[2], count);
                yMap.put(indexes[1], zMap);
            }
        } else {
            Map<Integer, Integer> zMap = new TreeMap<>();
            Map<Integer, Map<Integer, Integer>> yMap = new TreeMap<>();
            zMap.put(indexes[2], count);
            yMap.put(indexes[1], zMap);
            xMap.put(indexes[0], yMap);
        }
        count++;
    }

    private boolean isOnLatticePoint(Vector3D c) {
        int[] idxs = lattice.getNearbyIndexes(c);
        Vector3D p = lattice.getSphereCenter(idxs[0], idxs[1], idxs[2]);
        double diff = p.distance(c);
        if (diff > radius * 1E-15) {
            System.out.println("Not on lattice !!!");
            return false;
        }
        return true;
    }

    @Override
    public int getSphereIndex(Vector3D r) {

        int[] indexes = lattice.getNearbyIndexes(r);

        int i = findInList(indexes[0], indexes[1], indexes[2]);

        if (isInside(i, r))
            return i;
        else {

            int n = 1; // range of search
            for (int dzi = -n; dzi <= n; dzi++) {
                for (int dyi = -n; dyi <= n; dyi++) {
                    for (int dxi = -n; dxi <= n; dxi++) {
                        int xi = indexes[0] + dxi;
                        int yi = indexes[1] + dyi;
                        int zi = indexes[2] + dzi;

                        i = findInList(xi, yi, zi);
                        if (isInside(i, r))
                            return i;
                    }
                }
            }

        }
        return -1;

    }

    public List<SphereDescripton> getSphereDescriptionList() {
        return sdList;
    }

    private boolean isInside(int i, Vector3D p) {
        if (i == -1)
            return false;
        return sdList.get(i).getCentre().distance(p) <= this.radius;
    }

    public int findInList(int xi, int yi, int zi) {

        Map<Integer, Map<Integer, Integer>> yMap = xMap.get(xi);
        if (yMap == null)
            return -1;

        Map<Integer, Integer> zMap = yMap.get(yi);
        if (zMap == null)
            return -1;

        Integer i = zMap.get(zi);
        if (i == null)
            return -1;

        return i;
    }

}
