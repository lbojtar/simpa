package simpa.core.cover;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.SphereDescripton;

/**
 * Class for internal use only. It is used to describe a sphere in the cover
 * with additional info.
 * 
 */
public class SphereDescriptionImpl extends SphereDescripton {

    private int lMax; // maximum degree of the SH expansion
    private int index; // position in the list of spheres

    /**
     * Converts a list of SphereDescripton to a list of SphereDescriptionImpl
     * 
     * @param sphereDescriptionList List of SphereDescripton
     * @return List of SphereDescriptionImpl
     */
    public static List<SphereDescriptionImpl> toInternal(List<SphereDescripton> sphereDescriptionList) {
        List<SphereDescriptionImpl> result = new ArrayList<>();
        for (int i = 0; i < sphereDescriptionList.size(); i++) {
            SphereDescripton sd = sphereDescriptionList.get(i);
            result.add(new SphereDescriptionImpl(sd.getCentre(), sd.getRadius(), 0, i));
        }
        return result;
    }

    public SphereDescriptionImpl(Vector3D centre, double radius, int lmax, int index) {
        super(centre, radius);
        this.lMax = lmax;
        this.index = index;
    }

    public int getlMax() {
        return lMax;
    }

    public int getIndex() {
        return index;
    }

}
