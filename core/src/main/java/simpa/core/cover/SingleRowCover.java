/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.CoverType;
import simpa.core.api.SphereCovering;
import simpa.core.api.SphereDescripton;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.PathUtils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Single row implementation of @see{@link SphereCovering}.
 */
public class SingleRowCover implements SphereCovering {

	private List<Vector3D> orbit;
	private final List<SphereDescripton> sphereDescriptonList;

	/**
	 * Creates spheres around a given orbit with a single line of balls centered on
	 * the orbit. The balls overlap such a way that it always covers a given
	 * aperture around the orbit.
	 * 
	 * @param orbit          reference orbit
	 * @param apertureRadius radius of the aperture
	 * @param ballRadius     ball radius
	 * @param closedPath     True if the path is closed (circular)
	 */
	public SingleRowCover(List<Vector3D> orbit, double apertureRadius, double ballRadius, boolean closedPath) {

		this.orbit = orbit;
		sphereDescriptonList = new ArrayList<>();
		List<Vector3D> centres = calcBallCentres(ballRadius, apertureRadius, closedPath);
		for (Vector3D c : centres) {
			SphereDescripton shd = new SphereDescripton(c, ballRadius);
			sphereDescriptonList.add(shd);
		}
	}

	/**
	 * Creates a single row cover from a file.
	 * 
	 * @param coverFile input file
	 */
	public SingleRowCover(String coverFile) throws FileNotFoundException {
		sphereDescriptonList = new ArrayList<>();
		double[][] dd = FileUtils.read2DArray(coverFile, 4);
		for (double[] d : dd) {
			Vector3D center = new Vector3D(d[0], d[1], d[2]);
			double rad = d[3];
			SphereDescripton shd = new SphereDescripton(center, rad);
			sphereDescriptonList.add(shd);
		}
	}

	public List<SphereDescripton> getCoveringSpheres() {
		return sphereDescriptonList;
	}

	/**
	 * Write this cover into a file
	 * 
	 * @param fname name of the output file
	 */
	public void toFile(String fname) {
		StringBuilder sb = new StringBuilder();
		sb.append("x y z radius").append(System.lineSeparator());

		getCoveringSpheres().forEach(sd -> {
			Vector3D c = sd.getCentre();
			sb.append(c.getX()).append(" ").append(c.getY()).append(" ").append(c.getZ()).append(" ")
					.append(sd.getRadius()).append(System.lineSeparator());
		});

		FileUtils.writeTextFile(fname, sb.toString());

	}

	@Override
	public CoverType getCoverType() {
		return CoverType.SINGLE_ROW;
	}
	
    // We don't take this into the PathUtils, because it is rather special
	/**
	 * Returns the next point on the path at a given distance from a given point in
	 * the direction of particle propagation. It is assumed that this direction is
	 * from smaller path indexes to the bigger ones. If the path is circular, it
	 * loops around while searching the next orbit point. In case of a non-circular
	 * path, if the point is not found between the beginning and the end of the
	 * path, it returns null.
	 *
	 * @param path       path to search in
	 * @param from       starting point
	 * @param distance   distance to search
	 * @param closedPath if the path is closed (full circle)
	 * @return the point that is found at the distance from the starting point. At
	 *         The end if there is no more point in the path at the given distance
	 *         it returns null.
	 */
	private Vector3D getPointOnPath(List<Vector3D> path, Vector3D from, double distance, boolean closedPath) {

		Vector3D previousOrbitpoint;
		int count = 0;

		int orbitPointIndex = PathUtils.getClosestIndex(path, from);
		if (orbitPointIndex == 0)
			orbitPointIndex = 1;

		do {
			if (orbitPointIndex == path.size()) {

				if (closedPath) {
					orbitPointIndex = 0; // close the orbit
					previousOrbitpoint = path.get(path.size() - 1);
				} else
					return null;

			} else {
				previousOrbitpoint = path.get(orbitPointIndex - 1);
			}

			Vector3D nextOrbitPoint = path.get(orbitPointIndex);
			double d = from.distance(nextOrbitPoint);
			// if the next orbit point is further than the distance, then point on the path
			// we're searching for it in the
			// orbit segment between the nextOrbitPoint and previousOrbitpoint
			if (d >= distance) {
				Vector3D p;
				double remaining = distance - from.distance(previousOrbitpoint);
				Vector3D incrementUnitVector = nextOrbitPoint.subtract(previousOrbitpoint).normalize();

				// If the orbit segment is bigger than the distance between the spheres
				if (nextOrbitPoint.distance(previousOrbitpoint) > distance) {
					p = from.add(incrementUnitVector.scalarMultiply(distance));
				} else {
					p = incrementUnitVector.scalarMultiply(remaining).add(previousOrbitpoint);
				}

				return p;

			} else {
				orbitPointIndex++;
			}

			count++;
		} while (count <= path.size());

		throw new IllegalArgumentException(
				"Error in getPointOnPath, probably the distance parameter" + distance + " is too large.");
	}


	private List<Vector3D> calcBallCentres(double ballRadius, double apertureRadius, boolean isClosedPath) {
		if (ballRadius <= apertureRadius)
			throw new IllegalArgumentException(
					"Error occured during single row cover construction: the ball radius must be bigger than the aperture radius !");
		// The distance of spheres for a given intersection radius can be obtained by a
		// formula:
		// http://mathworld.wolfram.com/Sphere-SphereIntersection.html
		double ballDist = 2 * Math.sqrt(ballRadius * ballRadius - apertureRadius * apertureRadius);

		StringBuilder sb = new StringBuilder();

		List<Vector3D> centres = new ArrayList<>();

		Vector3D lastCentre = orbit.get(0);
		centres.add(lastCentre);
		int count = 0;

		sb.append("#X Y Z").append(System.lineSeparator());

		do {

			lastCentre = getPointOnPath(orbit, lastCentre, ballDist, isClosedPath);

			if (lastCentre == null) {
				// at the and to make sure we covered the whole path add a ball to the last path
				// point
				centres.add(orbit.get(orbit.size()-1));
				return centres;
			}

			// System.out.println(centres.get(centres.size()-1).distance(lastCentre));

			centres.add(lastCentre);

			String s = lastCentre.getX() + " " + lastCentre.getY() + " " + lastCentre.getZ() + System.lineSeparator();

			sb.append(s);
			count++;
		} while (lastCentre.distance(orbit.get(0)) >= ballDist || count == 1);

		FileUtils.writeTextFile("ballCentres.txt", sb.toString());

		return centres;
	}

}
