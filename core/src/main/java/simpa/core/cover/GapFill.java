package simpa.core.cover;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.partitioning.Region;
import org.apache.commons.math3.geometry.partitioning.Region.Location;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.SphereDescripton;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.sh.TDesign;

public class GapFill {

    private static final Logger logger = LogManager.getLogger(GapFill.class);

    private List<SphereDescripton> extraList;
    private LatticeSearch latticeSearch;
    private List<Vector3D> qpl;
    private final Locator closedRegion;
    private final HCPLattice hcpLattice;

    public GapFill(Locator closedRegion, HCPLattice hcpLattice) {
        this.closedRegion = closedRegion;
        this.hcpLattice = hcpLattice;
        try {
            qpl = (new TDesign()).getQuadraturaPoints(5);
        } catch (IOException e) {
            logger.error("Something went wrong reading the quadratura points");
        }
    }

    /**
     * Fills the gaps by adding outside spheres where included spheres are not
     * covering the inside of the PolyhedronSet of the constructor.
     *
     * @param sphereDescriptionList List of inside spheres. The list will be
     *                              modified by adding extra spheres.
     * @throws KnownFatalException
     *
     */
    public void fillGaps(List<SphereDescripton> sphereDescriptionList) throws KnownFatalException {
        if (sphereDescriptionList.isEmpty())
            return;

        latticeSearch = new LatticeSearch(sphereDescriptionList);

        logger.debug("Filling the gaps");
        long t0 = System.currentTimeMillis();

        extraList = new ArrayList<>();      
        sphereDescriptionList.stream().forEach(this::checkNeighbourhood);

        // DEBUG CODE start
        logger.debug("Extra balls : " + extraList.size());
        long t1 = System.currentTimeMillis();
        logger.debug("Extra balls added in " + (t1 - t0) + " ms");

        sphereDescriptionList.addAll(extraList);

    }

    /**
     * Recursive check of neighbor balls
     *
     * @param sd spheredescription to search neighbors for
     */
    private void checkNeighbourhood(SphereDescripton sd) {
        double rad = sd.getRadius();
        for (Vector3D p : qpl) {
            // test point slightly outside the sphere
            Vector3D pt = p.scalarMultiply(rad * 1.000001).add(sd.getCentre());
            Location l = closedRegion.locate(pt);
            if (l == Region.Location.INSIDE ) {
                int[] idxs = hcpLattice.getEnclosingSphereIndexes(pt);
                if (latticeSearch.findInList(idxs[0], idxs[1], idxs[2]) == -1) {
                    Vector3D c = hcpLattice.getSphereCenter(idxs[0], idxs[1], idxs[2]);
                    SphereDescripton nsd = new SphereDescripton(c, rad);
                    extraList.add(nsd);
                    latticeSearch.add(nsd);
                    // System.out.println("Extra ball : " + c + " extra list size: " +
                    // extraList.size());
                    checkNeighbourhood(nsd);
                }
            }
        }
    }
}
