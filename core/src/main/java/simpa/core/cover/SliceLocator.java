package simpa.core.cover;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.partitioning.Region.Location;

import simpa.core.api.Profile;
import simpa.core.api.SystemConstants;
import simpa.core.api.utils.SimpleTriangulator;
import simpa.core.api.utils.Triangulation;

/**
 * Class for testing a point location in a slice of a region bounded by two
 * profiles. The points on th plane of the second profile are considered outside
 * to avoid duplication between slices.
 * 
 */
public class SliceLocator implements Locator {

    private List<Plane> planes;
    private Plane exitPlane;

    public SliceLocator(Profile profile, Profile next) {

        Vector3D p1 = next.getPoints().get(0);
        Vector3D p2 = next.getPoints().get(next.getPoints().size() / 4);
        Vector3D p3 = next.getPoints().get(next.getPoints().size() / 2);
        exitPlane = new Plane(p3, p2, p1, SystemConstants.GEOMETRY_TOLERANCE); // counter clock wise order at the end
                                                                               // plane

        planes = new ArrayList<>();
        SimpleTriangulator triangulator = new SimpleTriangulator(profile, next);
        Triangulation t = triangulator.getSurface();
        // crete a plane for each facet
        t.facets().stream().forEach(f -> planes.add(new Plane(t.vertices().get(f[0]),
                t.vertices().get(f[1]), t.vertices().get(f[2]), SystemConstants.GEOMETRY_TOLERANCE)));

        p1 = profile.getPoints().get(0);
        p2 = profile.getPoints().get(next.getPoints().size() / 4);
        p3 = profile.getPoints().get(next.getPoints().size() / 2);
        Plane entryPlane = new Plane(p1, p2, p3, SystemConstants.GEOMETRY_TOLERANCE); //  clock wise order at the
        planes.add(entryPlane);                                                                             // entry plane

    }

    @Override
    public Location locate(Vector3D p) {

        if (exitPlane.getOffset(p) >= 0) {
            return Location.OUTSIDE;
        }

        for (Plane plane : planes) {
            if (plane.getOffset(p) > 0) {
                return Location.OUTSIDE;
            }else if (plane.getOffset(p) == 0) {
                return Location.BOUNDARY;
            }
        }
        return Location.INSIDE;
    }
}
