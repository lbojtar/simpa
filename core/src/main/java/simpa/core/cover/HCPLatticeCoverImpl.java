/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.cover;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CoverType;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.Profile;
import simpa.core.api.SphereCovering;
import simpa.core.api.SphereDescripton;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.TextStlReader;

/**
 * Hexagonal Closed Packed lattice implementation of @see{@link SphereCovering}.
 */
public class HCPLatticeCoverImpl implements SphereCovering {

	private static final Logger logger = LogManager.getLogger(HCPLatticeCoverImpl.class);

	private List<SphereDescripton> sphereDescriptonList;
	private HCPLattice lattice;

	/**
	 * Creates a HCPLatticeCover from a cover file in format X Y Z Radius. It reads
	 * also the corresponding STL file.
	 * 
	 * @param coverFileName - Cover file name with
	 *                      {@link FileNamingConventions#HCP_COVER_FILE_EXTENSION}
	 *                      extension. A file with the same name, but with .stl
	 *                      extension must be present in the working directory.
	 */
	public HCPLatticeCoverImpl(String coverFileName) throws FileNotFoundException {
		if (!coverFileName.contains(FileNamingConventions.HCP_COVER_FILE_EXTENSION))
			throw new IllegalArgumentException(
					"The given file should have extension: " + FileNamingConventions.HCP_COVER_FILE_EXTENSION);

	
		sphereDescriptonList = new ArrayList<>();

		double[][] daa = FileUtils.read2DArray(coverFileName, 4);
		for (double[] da : daa) {
			SphereDescripton sd = new SphereDescripton(new Vector3D(da[0], da[1], da[2]), da[3]);
			sphereDescriptonList.add(sd);
		}

	}

	/**
	 * Creates an HCP lattice of equal spheres with a given radius. The class keeps
	 * a list of those spheres, which have the centers inside the surface described
	 * by the STL file. See:
	 * https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres
	 * 
	 * @param stlFile    STL file name
	 * @param ballRadius Radius of the spheres
	 */
	public HCPLatticeCoverImpl(String stlFile, double ballRadius) throws KnownFatalException {

		lattice = new HCPLattice(ballRadius);
		if (!stlFile.contains(".stl"))
			throw new IllegalArgumentException("The given file should have a .stl extension");
		try {
			TextStlReader stlr = new TextStlReader();
			stlr.readStlFile(new FileInputStream(stlFile));

			VolumeFill fill = new VolumeFill(stlr.getVertices(), stlr.getFaces(), lattice);

			sphereDescriptonList = fill.getSpheres();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates an HCP lattice of equal spheres with a given radius. The class keeps
	 * a list of those spheres, which have the centers inside the surface described
	 * by the list of profiles.
	 * https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres
	 * 
	 * @param profiles     List of profiles
	 * @param ballRadius   Radius of the spheres
	 * @param circularPath true if the path is circular
	 * @throws KnownFatalException 
	 */
	public HCPLatticeCoverImpl(List<Profile> profiles, double ballRadius, boolean circularPath) throws KnownFatalException {
		
		List<List<SphereDescripton>> ll= Collections.synchronizedList (new ArrayList<>());
		
		lattice = new HCPLattice(ballRadius);
	
		IntStream.range(0, profiles.size() - 1)
			.parallel()
			.forEach(i -> {
				try {
					ll.add(fillSlice(profiles.get(i), profiles.get(i + 1)));
				} catch (KnownFatalException e) {
					throw new RuntimeException(e);
				}
			});

		//close the path if it is circular
		if(circularPath) {
			ll.add(fillSlice(profiles.get(profiles.size()-1),profiles.get(0)));
		}
		//count all spheres in list of lists
		int n = 0;
		for(List<SphereDescripton> l:ll) {
			n+=l.size();
		}
		//create a list of spheres with the correct initial size
		sphereDescriptonList = new ArrayList<>(n);
		
		//add all spheres to the list
		for(List<SphereDescripton> l:ll) {
			sphereDescriptonList.addAll(l);
		}
	}

	private List<SphereDescripton> fillSlice(Profile first,Profile next) throws KnownFatalException{			
		VolumeFill fill = new VolumeFill(first, next, lattice);
		return  fill.getSpheres();	
	}

	/**
	 * Write this cover into a file
	 * 
	 * @param fname output file name
	 */
	@Override
	public void toFile(String fname) {
		logger.info("Number of spheres in the cover: "+getCoveringSpheres().size());
		StringBuilder sb = new StringBuilder();
		sb.append("#X Y Z RADIUS").append(System.lineSeparator());

		getCoveringSpheres().forEach(sd -> {
			Vector3D c = sd.getCentre();
			sb.append(c.getX()).append(" ").append(c.getY()).append(" ").append(c.getZ()).append(" ")
					.append(sd.getRadius()).append(System.lineSeparator());
		});

		FileUtils.writeTextFile(fname, sb.toString());

	}

	@Override
	public List<SphereDescripton> getCoveringSpheres() {
		return sphereDescriptonList;
	}

	@Override
	public CoverType getCoverType() {
		return CoverType.LATTICE;
	}

	
}
