/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.sh.SHBall;

/**
 * Implementation of the BallSearch interface that provides a method for
 * searching in a single row of balls.
 */
public class SingleRowBallSearch implements BallSearch {

	private final SHBall[] balls;
	private int lastBallIndex = 0; // We start the sphere search from here
	private boolean forwardSearch=true; // direction of search

	public SingleRowBallSearch(SHBall[] balls) {
		this.balls = balls;
	}

	/**
	 * Returns the index of the firstly found sphere which contains the point
	 * located at the r vector. This simple implementation assumes a single row of
	 * balls along the ideal orbit.
	 * 
	 * @param r - location
	 * @return - index of sphere
	 */
	@Override
	public int getSphereIndex(Vector3D r) throws OutOfApertureException {

		// System.out.println("entered getSphereIndex");

		int ballIndex = lastBallIndex;

		do {

			SHBall b = balls[ballIndex];
			Vector3D dist = b.getCentre().subtract(r);

			// we calculate the square of distance, its faster because doesn't requite a
			// call to Math.sqrt()
			double distSq = dist.getX() * dist.getX() + dist.getY() * dist.getY() + dist.getZ() * dist.getZ();
			double rsq = b.getRadius() * b.getRadius();

			// check if r is inside a ball
			if (distSq <= rsq) {

				if (ballIndex - lastBallIndex < 0) 
					forwardSearch = false;				
				if (ballIndex - lastBallIndex > 0) 
					forwardSearch = true;
				
				lastBallIndex = ballIndex;
				// System.out.println("exited with ball index= " + ballIndex);
				return ballIndex;
			}

			if (forwardSearch) {
				ballIndex++;
				ballIndex = ballIndex % balls.length;
			} else {
				ballIndex--;
				if (ballIndex < 0)
					ballIndex = balls.length - 1;
			}

		} while (ballIndex != lastBallIndex);
		// after a full turn we come here only if the particle is out of any balls
		throw new OutOfApertureException(r);

	}

}
