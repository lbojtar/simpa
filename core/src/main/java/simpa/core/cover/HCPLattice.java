/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Object that represents a Hexagonal Closed Packed lattice and contains utility methods
 * to search in this lattice.
 */
public class HCPLattice {
    private static final Logger logger = LogManager.getLogger(HCPLattice.class);

    private double halfEdge; // half of the tetrahedron edge length in the HCP lattice
    private double radiusSQ;
    private final double  radius;

    private static final double SQRT3 = Math.sqrt(3.0);
    private static final double SQRT6 = Math.sqrt(6.0);
    private static final double SQRT32 = Math.sqrt(3.0 / 2.0);
    private static final double SAFETY_MARGIN = 1E-12; // to make sure the spheres overlap everywhere

    /**
     * Creates a hexagonal HCP lattice.
     * 
     * @param rad - radius of spheres
     * @param overlap - If the overlap flag is true the spheres cover the 3D space without gaps. If it is false
     *            the spheres touches without overlap.
     */
    public HCPLattice(double rad, boolean overlap) {
    	this.radius=rad;
        init(rad, overlap);
    }

    public HCPLattice(double rad) {
    	this.radius=rad;
        init(rad, true);
    }

    private void init(double rad, boolean overlap) {
    	
        if (overlap)
            this.halfEdge = rad / ((Math.sqrt(8.0) + rad * SAFETY_MARGIN) / 2.0);
        else
            this.halfEdge = rad;
        radiusSQ = rad * rad;
    }

    /**
     * Get the center of a sphere in the lattice. We follow the formula at the end of this section:
     * https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres#Simple_hcp_lattice The only difference is that we
     * want to generate a lattice with overlapping spheres, so the lattice distance is multiplied by a factor.
     * 
     * @param xi - X index
     * @param yi -Y index
     * @param zi -Z index
     * @return -centre of the sphere
     */
    public Vector3D getSphereCenter(int xi, int yi, int zi) {

        double x = (2 * xi + Math.abs((yi + zi)) % 2);
        double y = SQRT3 * (yi + Math.abs((zi % 2)) / 3.0);
        double z = 2 * SQRT6 * zi / 3.0;
        return (new Vector3D(x, y, z)).scalarMultiply(halfEdge);
    }

    /**
     * Find the 3 indexes in the regular HCP lattice belonging to a given lattice point. I just solved the 3 equations
     * for xi,yi,zi in the getSphereCentre method. BE CAREFULL: the sphere with the indexes returned by this method is
     * NOT NECCESSERILY CONTAINS p !!!
     * 
     * @param p -point
     * @return indexes of the ball center closest the point p.
     */
    public int[] getNearbyIndexes(Vector3D p) {
        int[] indexes = new int[3];
        long zi = Math.round(p.getZ() * SQRT32 / (2.0 * halfEdge));
        long yi = Math.round((SQRT3 * p.getY() - halfEdge * (Math.abs(zi) % 2)) / (3 * halfEdge));
        long xi = Math.round(-(halfEdge * (Math.abs((zi + yi)) % 2) - p.getX()) / (2.0 * halfEdge));

        indexes[0] = (int) xi;
        indexes[1] = (int) yi;
        indexes[2] = (int) zi;

        return indexes;
    }

    /**
     * Get thz first enclosing sphere found near the given point.
     * 
     * @param p -Point
     * @return -Lattice index of the enclosing sphere center.
     */
    public int[] getEnclosingSphereIndexes(Vector3D p) {

        int[] indexes = this.getNearbyIndexes(p);

        Vector3D lc = getSphereCenter(indexes[0], indexes[1], indexes[2]);

        if (inSphere(p, lc)) {
            return indexes; // most probable
        }

        int n = 1; // range of search
        for (int dzi = -n; dzi <= n; dzi++) {
            for (int dyi = -n; dyi <= n; dyi++) {
                for (int dxi = -n; dxi <= n; dxi++) {
                    int xi = indexes[0] + dxi;
                    int yi = indexes[1] + dyi;
                    int zi = indexes[2] + dzi;

                    lc = getSphereCenter(xi, yi, zi);

                    // printParaviewScript(xi, yi, zi, lc);
                    if (inSphere(p, lc)) {

                        indexes[0] = xi;
                        indexes[1] = yi;
                        indexes[2] = zi;

                        return indexes;

                    }
                }
            }
        }

        logger.error("Someting went wrong in HCPLattice.getEnclosingSphereIndexes");

        return new int[0];

    }

    private boolean inSphere(Vector3D point, Vector3D sphereCentre) {

        Vector3D diff = point.subtract(sphereCentre);
        double distsq = Math.abs(diff.getX() * diff.getX() + diff.getY() * diff.getY() + diff.getZ() * diff.getZ());
        return distsq < radiusSQ;
    }

    // for debugging
    public void printParaviewScript(int xi, int yi, int zi, Vector3D p) {
        String sn = "sphere" + xi + "" + yi + "" + zi;
        sn = sn.replace("-", "m");
        String nl = System.lineSeparator();
        double rad = Math.sqrt(radiusSQ);
        String comm = sn + " = Sphere()" + nl + sn + ".Center = [" + p.getX() + "," + p.getY() + "," + p.getZ() + "]"
                + nl + sn + ".Radius = " + rad + nl + sn + ".ThetaResolution = 20" + nl + sn + ".PhiResolution = 20"
                + nl + "Show(" + sn + ", renderView1)" + nl + nl;
        logger.info(comm);
    }

	public double getRadius() {
		return radius;
	}
}
