package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.partitioning.Region.Location;

public class Math3Locator implements Locator {

    private PolyhedronsSet phs;

    public Math3Locator(PolyhedronsSet phs) {
        this.phs = phs;
    }   
    
    @Override
    public Location locate(Vector3D p) {
      return phs.checkPoint(p); 
    }

}
