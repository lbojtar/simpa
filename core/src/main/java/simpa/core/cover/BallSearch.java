/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.exceptions.OutOfApertureException;

/**
 * Requires the implementation of a method to search spheres using a point
 * in the lattice.
 */
public interface BallSearch {

    /**
     * Gets the index of a sphere at the given location.
     *
     * @param r point to search
     * @return the index of the sphere
     * @throws OutOfApertureException when the point is outside the region of interest.
     */
    int getSphereIndex(Vector3D r) throws OutOfApertureException;
    
}
