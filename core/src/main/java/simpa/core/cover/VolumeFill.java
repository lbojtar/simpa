/**
* Copyright (c) 2022 Lajos Bojtár & Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.partitioning.Region;
import org.apache.commons.math3.geometry.partitioning.Region.Location;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.Profile;
import simpa.core.api.SphereDescripton;
import simpa.core.api.SystemConstants;
import simpa.core.api.exceptions.KnownFatalException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class with methods to fill the volume of interest with
 * spheres so that it covers the entire region.
 */
public class VolumeFill {

    private static final Logger logger = LogManager.getLogger(VolumeFill.class);

    private Locator locator;
    private final HCPLattice hcpLattice;
    private final List<Vector3D> vertexList;

    public Double maxX = null, minX = null, maxY = null, minY = null, maxZ = null, minZ = null;
    public int maxXIndex, minXIndex, maxYIndex, minYIndex, maxZIndex, minZIndex;

    private List<SphereDescripton> spheres;

    /**
     * Construct a volume fill object and fill the volume.
     * 
     * @param vertexList All vertices describing a closed region
     * @param allFacets  All facets describing a closed region 
     * 
     * @param lattice    A lattice object
     * @throws KnownFatalException
     */
    public VolumeFill(List<Vector3D> vertexList, List<int[]> allFacets, HCPLattice lattice) throws KnownFatalException {

        this.hcpLattice = lattice;
        this.vertexList = vertexList;
        locator = new Math3Locator(new PolyhedronsSet(vertexList, allFacets, SystemConstants.GEOMETRY_TOLERANCE));

        getBoundingBox();
        getBoundingLatticeIndexes();
        PointClassification pc = getAllInsideLatticePoints();
        spheres = Collections.synchronizedList(new ArrayList<>());
        pc.inside().stream().forEach(v -> spheres.add(new SphereDescripton(v, hcpLattice.getRadius())));
        pc.onBoundary().stream().forEach(v -> spheres.add(new SphereDescripton(v, hcpLattice.getRadius())));
        logger.debug("Filling the volume: " + spheres.size() + " spheres added");
        (new GapFill(locator, hcpLattice)).fillGaps(spheres);
 
    }

    /**
     * Construct a volume fill object and fill the volume. This constructor is used
     * with the slicing algorithm. The excluded boundary is used to exclude the
     * boundary between slices. No call to fillGaps is made. It should be called
     * after all slices are processed.
     * 
     * @param first   All vertices describing a slice of a closed region
     * @param next    All facets describing a slice of a a closed region
     * @param lattice A lattice object
     *
     */
    public VolumeFill(Profile first, Profile next, HCPLattice lattice) throws KnownFatalException {

        this.hcpLattice = lattice;

        vertexList = new ArrayList<>();
        vertexList.addAll(first.getPoints());
        vertexList.addAll(next.getPoints());

        locator = new SliceLocator(first, next);

        getBoundingBox();
        getBoundingLatticeIndexes();
        PointClassification pc = getAllInsideLatticePoints();
        spheres = new ArrayList<>();

        pc.inside().stream().forEach(v -> spheres.add(new SphereDescripton(v, hcpLattice.getRadius())));
        logger.debug(pc.inside().size() + " inside spheres added");
        pc.onBoundary().stream().forEach(v -> spheres.add(new SphereDescripton(v, hcpLattice.getRadius())));
        logger.debug(pc.onBoundary().size() + " onBoundary spheres added");
        
        (new GapFill(locator, hcpLattice)).fillGaps(spheres);

    }

    /**
     * Get the spheres that fill the volume.
     * 
     * @return List of spheres that are inside or on the bundary of the region.
     */
    public List<SphereDescripton> getSpheres() {
        return spheres;
    }

    // for testing
    protected Locator getLocator() {
        return locator;
    }

    /**
     * Gets the bounding box of the region of interest.
     */
    private void getBoundingBox() {
        for (Vector3D vector3D : vertexList) {
            if (maxX == null || vector3D.getX() > maxX) {
                maxX = vector3D.getX();
            }
            if (minX == null || vector3D.getX() < minX) {
                minX = vector3D.getX();
            }
            if (maxY == null || vector3D.getY() > maxY) {
                maxY = vector3D.getY();
            }
            if (minY == null || vector3D.getY() < minY) {
                minY = vector3D.getY();
            }
            if (maxZ == null || vector3D.getZ() > maxZ) {
                maxZ = vector3D.getZ();
            }
            if (minZ == null || vector3D.getZ() < minZ) {
                minZ = vector3D.getZ();
            }
        }
    }

    /**
     * Gets the highest and lowest indexes for each axis inside the
     * bounding box.
     */
    private void getBoundingLatticeIndexes() {
        int[] minimums = hcpLattice.getNearbyIndexes(new Vector3D(minX, minY, minZ));
        minXIndex = minimums[0]-1;
        minYIndex = minimums[1]-1;
        minZIndex = minimums[2]-1;

        int[] maximums = hcpLattice.getNearbyIndexes(new Vector3D(maxX, maxY, maxZ));
        maxXIndex = maximums[0]+1;
        maxYIndex = maximums[1]+1;
        maxZIndex = maximums[2]+1;
    }

    private PointClassification getAllInsideLatticePoints() {
        List<Vector3D> insidePoints = new ArrayList<>();
        List<Vector3D> onBoundaryPoints = new ArrayList<>();
        for (int x = minXIndex; x < maxXIndex; x++) {
            for (int y = minYIndex; y < maxYIndex; y++) {
                for (int z = minZIndex; z < maxZIndex; z++) {
                    Vector3D point = hcpLattice.getSphereCenter(x, y, z);
                    Location l = locator.locate(point);
                    if (l == Region.Location.INSIDE) {
                        insidePoints.add(point);
                    } else if (l == Region.Location.BOUNDARY) {
                        onBoundaryPoints.add(point);
                    }
                }
            }
        }
        return new PointClassification(insidePoints, onBoundaryPoints);
    }

    record PointClassification(List<Vector3D> inside, List<Vector3D> onBoundary) {
    }
}
