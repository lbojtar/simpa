package simpa.core.cover;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.partitioning.Region.Location;

public interface Locator {
    public Location locate(Vector3D p );
}
