// @formatter:off
/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.b2a;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.PhysicsConstants;
import simpa.core.api.utils.Quadrature1D;

/**
 * A wire carrying some current. Usually part of a current loop.
 */
public class Wire {

	private Vector3D start;
	private Vector3D end;
	private double current;


	public Wire(Vector3D start, Vector3D end, double current) {
		this.start = start;
		this.end = end;
		this.current = current;
	}

	/**
	 * 
	 * @return Start point
	 */
	public Vector3D getStart() {
		return start;
	}

	public Vector3D getEnd() {
		return end;
	}

	public void rotateAndTranslate(Rotation rot, Vector3D translation) {
		start = rot.applyTo(start).add(translation);
		end = rot.applyTo(end).add(translation);
	}

	public void setCurrent(double current) {
		this.current = current;
	}

	public double getCurrent() {
		return current;
	}

	/**
	 * This method gives a list of CurrentPointSources approximating the wire. We
	 * use the approximation for the solid harmonic expansion instead of the exact
	 * formula, because it allows to use the FMM method for summation and we can
	 * treat all sources the same way, it is also much faster for a large number of
	 * evaluation points.
	 * 
	 * @param quadrature   Quadrature to be used for the approximation of a
	 *                     subdivision of the wire.
	 * @param subdivisions If bigger than 1, the wire will be split into
	 *                     subdivisions. This increases the precision of the
	 *                     calculation.
	 * @return List of current point sources.
	 */
	public List<CurrentPointSource> getPointSources(Quadrature1D quadrature, int subdivisions) {
		if (subdivisions < 1)
			throw new IllegalArgumentException("The number of subdivisions for a wire can't be smaller than 1.");
		List<CurrentPointSource> l = new ArrayList<>();
		Vector3D dr = end.subtract(start).scalarMultiply(1.0 / subdivisions);

		for (int i = 0; i < subdivisions; i++) {
			Vector3D startP = start.add(dr.scalarMultiply(i));
			Vector3D endP = start.add(dr.scalarMultiply(i + 1));
			l.addAll(getPointSources(quadrature, startP, endP));
		}

		return l;
	}

	private List<CurrentPointSource> getPointSources(Quadrature1D quadrature, Vector3D startP, Vector3D endP) {

		List<CurrentPointSource> l = new ArrayList<>();
		Vector3D v = endP.subtract(startP);
		Vector3D center = startP.add(endP).scalarMultiply(0.5);
		double length = v.getNorm();
		Vector3D dir = v.normalize();
		double[] pos = quadrature.getPositions();
		double[] w = quadrature.getWeights();
		for (int i = 0; i < pos.length; i++) {
			Vector3D p = center.add(dir.scalarMultiply(0.5 * length * pos[i]));
			CurrentPointSource ps = new CurrentPointSource(p);
			Vector3D lz = new Vector3D(-dir.getZ(), dir.getY(), dir.getX());
			Vector3D ly = lz.crossProduct(dir);
			ps.setBaseVectors(dir, ly, lz);
			// factor 0.5, because the weights are for length 2 (interval: -1,1)
			ps.setLocalCurrentComponents(PhysicsConstants.MU_OVER_4PI * 0.5 * current * length * w[i], 0, 0);
			l.add(ps);
		}

		return l;
	}

}
