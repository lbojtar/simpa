package simpa.core.b2a.solve;

import org.apache.commons.math3.linear.RealVector;

public interface DirectSolver {

	RealVector solve();
}
