// Copyright (c) Nov 30, 2022 Lajos Bojtár

/* MIT LICENSE
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

package simpa.core.b2a.solve;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.SurfacePoint;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.b2a.SurfacePointImpl;

/**
 * Helper class for the Slice and Block Jacobi preconditioners.
 * 
 * @author lbojtar
 *
 */
public class BlockSolver {

	public static Logger logger = LogManager.getLogger(BlockSolver.class);

	private int dof = 1;
	private List<SurfacePointImpl> fineLevel;
	private Collection<SurfacePointImpl> corseLevel;
	private Map<SurfacePointImpl, DirectSolver> solvers; // the key is a corse grid point

	public BlockSolver(List<SurfacePointImpl> fineGrid, Map<SurfacePointImpl, List<SurfacePoint>> patches)
			throws KnownFatalException {

		this.fineLevel = fineGrid;
		this.corseLevel = patches.keySet();
		if (fineGrid.get(0).getSource() instanceof CurrentPointSource) {
			dof = 2;
		}
		
		OptionalDouble av = patches.values().stream().mapToInt(p -> p.size()).average();
		int n = ((int) av.getAsDouble()) * dof;
		logger.info("Creating preconditioner... Number of matrices: " + patches.size() + ".  Average matrix size: "+n+" x "+n);

		// TODO find out why this doesn't work with parralelStream !!
		if (dof == 2)
			solvers = corseLevel.stream()
					.collect(Collectors.toMap(sp -> sp, sp -> new StaticMagneticSolverImpl(patches.get(sp))));

		if (dof == 1)
			solvers = corseLevel.stream()
					.collect(Collectors.toMap(sp -> sp, sp -> new StaticElectricSolverImpl(patches.get(sp))));

	}

	public RealVector solve() throws KnownFatalException {

		RealVector x = new ArrayRealVector(new double[fineLevel.size() * dof]);
		double[] iterationSolution = new double[x.getDimension()];

		// solve patches, then set the aggregates with the local solution
		for (SurfacePointImpl sp : corseLevel) {
			solvers.get(sp).solve();
			List<SurfacePointImpl> aggr = sp.getAggregates();
			updateSolutionAtAggregate(aggr, iterationSolution);
		}

		return new ArrayRealVector(iterationSolution);
	}

	private void updateSolutionAtAggregate(List<SurfacePointImpl> aggr, double[] iterationSolution) {
		for (SurfacePointImpl sp : aggr) {
			int idx = fineLevel.indexOf(sp);
			if (dof == 1) {
				ElectricMonopole s = (ElectricMonopole) sp.getSource();
				iterationSolution[idx] = s.getCharge();
			}

			if (dof == 2) {
				CurrentPointSource s = (CurrentPointSource) sp.getSource();
				iterationSolution[dof * idx] += s.getLocalCurrentComponents().getZ();
				iterationSolution[dof * idx + 1] += s.getLocalCurrentComponents().getX();
			}
		}
	}
}
