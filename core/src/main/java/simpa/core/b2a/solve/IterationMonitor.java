/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
/*
 * Copyright (C) 2003-2006 Bj??rn-Ove Heimsund
 * 
 * This file is part of MTJ.
 * 
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package simpa.core.b2a.solve;

import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Default iteration monitor. This tester checks declares convergence if the
 * absolute value of the residual norm is sufficiently small, or if the relative
 * decrease is small. Divergence is decleared if too many iterations are spent,
 * or the residual has grown too much. NaNs will also cause divergence to be
 * flagged.
 */
public class IterationMonitor extends AbstractIterationMonitor {
	private static final Logger logger = LogManager.getLogger(IterationMonitor.class);

	//relative to the initial residual 
	private double residual;

	/**
	 * Relative tolerance
	 */
	double rtol;

	/**
	 * Absolute tolerance
	 */
	double atol;

	/**
	 * Divergence tolerance
	 */
	double dtol;

	/**
	 * Maximum number of iterations
	 */
	int maxIter;

	/**
	 * Constructor for DefaultIterationMonitor
	 * 
	 * @param maxIter Maximum number of iterations
	 * @param rtol    Relative convergence tolerance (to initial residual)
	 * @param atol    Absolute convergence tolerance
	 * @param dtol    Relative divergence tolerance (to initial residual)
	 */
	public IterationMonitor(int maxIter, double rtol, double atol, double dtol) {
		this.maxIter = maxIter;
		this.rtol = rtol;
		this.atol = atol;
		this.dtol = dtol;
	}

	/**
	 * Constructor for DefaultIterationMonitor. Default is 100000 iterations at
	 * most, relative tolerance of 1e-5, absolute tolerance of 1e-50 and a
	 * divergence tolerance of 1e+5.
	 */
	public IterationMonitor() {
		this.maxIter = 100000;
		this.rtol = 1e-5;
		this.atol = 1e-50;
		this.dtol = 1e+5;
	}

	/**
	 * Sets maximum number of iterations to permit
	 * 
	 * @param maxIter Maximum number of iterations
	 */
	public void setMaxIterations(int maxIter) {
		this.maxIter = maxIter;
	}

	/**
	 * Sets the relative tolerance
	 * 
	 * @param rtol Relative convergence tolerance (to initial residual)
	 */
	public void setRelativeTolerance(double rtol) {
		this.rtol = rtol;
	}

	/**
	 * Sets the absolute tolerance
	 * 
	 * @param atol Absolute convergence tolerance
	 */
	public void setAbsoluteTolerance(double atol) {
		this.atol = atol;
	}

	/**
	 * Sets the divergence tolerance
	 * 
	 * @param dtol Relative divergence tolerance (to initial residual)
	 */
	public void setDivergenceTolerance(double dtol) {
		this.dtol = dtol;
	}

	public double getLastResidual() {
		return residual;
	}
	
	protected boolean convergedI(double r) throws IterativeSolverNotConvergedException {
		// Store initial residual
		if (isFirst())
			initR = r;
		
		residual= r/initR;
		logger.info("Iteration: "+iter+"   Relative residual= "+residual);
		 
		// Check for convergence
		if (r < Math.max(rtol * initR, atol))
			return true;

		// Check for divergence
		if (r > dtol * initR)
			throw new IterativeSolverNotConvergedException("Diverged", this);
		if (iter >= maxIter)
			throw new IterativeSolverNotConvergedException("Maximum iterations exceeded.", this);
		if (Double.isNaN(r))
			throw new IterativeSolverNotConvergedException("Diverged", this);

		// Neither convergence nor divergence
		return false;
	}

	@Override
	protected boolean convergedI(double r, RealVector x) throws IterativeSolverNotConvergedException {
		return convergedI(r);
	}


}
