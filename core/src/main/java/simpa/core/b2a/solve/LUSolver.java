// Copyright (c) Dec 2, 2022 Lajos Bojtár

/* MIT LICENSE
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/

package simpa.core.b2a.solve;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

import com.sun.jna.ptr.IntByReference;

import simpa.core.nativ.Lapack;

class LUSolver {

	/** Entries of LU decomposition. */
	private final double[][] lu;

	/** Pivot permutation associated with LU decomposition. */
	private int[] pivot;
	private int n;

	/**
	 * Compute LU Decomposition of a general matrix.
	 *
	 * Computes the LU decomposition using GETRF.
	 * 
	 * @param A general square matrix
	 */
	public LUSolver(double[][] A) {
		n = A.length;
		double[] data = new double[n*n];
		// convert to 1D array
		for (int r = 0; r < n; r++) {
			for (int c = 0; c < n; c++) {
				int index = r + n * c;
				data[index] = A[r][c];
			}
		}

		int[] ipiv = new int[n];
		// NativeBlas.dgetrf(n, n, A.data, 0, n, ipiv, 0);
		IntByReference dim = new IntByReference(n);
		IntByReference errorCode = new IntByReference();
		Lapack.INSTANCE.dgetrf_(dim, dim, data, dim, ipiv, errorCode);

	    lu = new double[n][n];
	    //convert to 2D array
		for (int r = 0; r < n; r++) {
			for (int c = 0; c < n; c++) {
				lu[r][c] = data[r + n * c];
			}
		}
		setPivotIndices(ipiv);
	}

	/**
	 * Solve A.x=b
	 * 
	 * @param b
	 * @return x
	 */
	public RealVector solve(RealVector b) {
		final int m = pivot.length;
		if (b.getDimension() != m) {
			throw new DimensionMismatchException(b.getDimension(), m);
		}

		final double[] bp = new double[m];

		// Apply permutations to b
		for (int row = 0; row < m; row++) {
			bp[row] = b.getEntry(pivot[row]);
		}

		// Solve LY = b
		for (int col = 0; col < m; col++) {
			final double bpCol = bp[col];
			for (int i = col + 1; i < m; i++) {
				bp[i] -= bpCol * lu[i][col];
			}
		}

		// Solve UX = Y
		for (int col = m - 1; col >= 0; col--) {
			bp[col] /= lu[col][col];
			final double bpCol = bp[col];
			for (int i = 0; i < col; i++) {
				bp[i] -= bpCol * lu[i][col];
			}
		}

		return new ArrayRealVector(bp);
	}

	/**
	 * Create a permutation array from a LAPACK-style 'ipiv' vector.
	 *
	 * @param ipiv row i was interchanged with row ipiv[i]
	 */
	private void setPivotIndices(int[] ipiv) {
		// System.out.printf("size = %d n = %d\n", size, n);
		pivot = new int[n];
		for (int i = 0; i < n; i++)
			pivot[i] = i;

		// for (int i = 0; i < n; i++)
		// System.out.printf("ipiv[%d] = %d\n", i, ipiv[i]);

		for (int i = 0; i < n; i++) {
			int j = ipiv[i] - 1;
			int t = pivot[i];
			pivot[i] = pivot[j];
			pivot[j] = t;
		}
	}

}
