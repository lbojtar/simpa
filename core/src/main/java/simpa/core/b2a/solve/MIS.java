package simpa.core.b2a.solve;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.b2a.RangeSearch;
import simpa.core.b2a.SurfacePointImpl;

/**
 * Calculates a Maximum Independent Set of SurfacePoint's
 * 
 * @author lbojtar
 *
 */
public class MIS {

	public static List<SurfacePointImpl> createCorseGrid(List<SurfacePointImpl> spl, double radius) throws KnownFatalException {

		List<SurfacePointImpl> corseGrid = new ArrayList<>();

		RangeSearch fineRs = new RangeSearch(spl);
		spl.stream().forEach(p -> p.setVisited(false));

		for (SurfacePointImpl fineSp : spl) {
			// When the nornal vector is very different even the points are nearby, we don't
			// count the point as strongly connected, to avoid strange behavior on the edges
			List<SurfacePointImpl> l = fineRs.searchNeighbors(fineSp, radius);
			List<SurfacePointImpl> fineNeighbours = filterByDotProduct(l, fineSp.getVectorToSource());

			if (!fineSp.isVisited()) {

				for (SurfacePointImpl n : fineNeighbours) {
					if (!n.isVisited()) {
						// marks all neigbours and the point as visited
						for (SurfacePointImpl nsp : fineNeighbours) {
							nsp.setVisited(true);
						}
						fineSp.setVisited(true);
						SurfacePointImpl corseSp = fineSp.copy();
						corseGrid.add(corseSp);

					}
				} // neigbours

				// still not visited
				if (!fineSp.isVisited()) {
					fineSp.setVisited(true);
					SurfacePointImpl corseSp = fineSp.copy();
					corseGrid.add(corseSp);
				}
			}
		}

		// setup aggregates
		setCorseNeigbours(spl, corseGrid, 2 * radius);
		for (SurfacePointImpl fineSp : spl) {
			SurfacePointImpl closest = null;
			double smallestDist = Double.POSITIVE_INFINITY;

			List<SurfacePointImpl> cnl = fineSp.getCorseNeigbours();
			for (SurfacePointImpl cn : cnl) {
				double dist = cn.getLocation().distance(fineSp.getLocation());
				if (dist < smallestDist) {
					smallestDist = dist;
					closest = cn;
				}
			}
			closest.getAggregates().add(fineSp);
		}
		
		adjust(corseGrid);
		
		return corseGrid;
	}

	/**
	 * Adjust course grid point locations and field values to be at the center of aggregates.
	 * @param corseGrid
	 */
	private static void adjust(List<SurfacePointImpl> corseGrid) {
		
		for (SurfacePointImpl cp : corseGrid) {
	
			Vector3D center=Vector3D.ZERO;
		
			for (SurfacePointImpl fp : cp.getAggregates()) {
				center = center.add(fp.getLocation());				
			}
			center=center.scalarMultiply(1.0/cp.getAggregates().size());		
			cp.setLocation(center);			
		}			
	}

	private static void setCorseNeigbours(List<SurfacePointImpl> fineGrid, List<SurfacePointImpl> corseGrid,
			double radius) throws KnownFatalException {
		RangeSearch corseRs = new RangeSearch(corseGrid);
		for (SurfacePointImpl fineSp : fineGrid) {
			List<SurfacePointImpl> corseNeighbours;
			List<SurfacePointImpl> l = corseRs.searchNeighbors(fineSp, radius);
			corseNeighbours = filterByDotProduct(l, fineSp.getVectorToSource());
			addIfOverlap(fineSp, corseGrid, corseNeighbours);
			fineSp.setCorseNeigbours(corseNeighbours);
		}
	}

	public static List<SurfacePointImpl> filterByDotProduct(List<SurfacePointImpl> ns, Vector3D v) {
		List<SurfacePointImpl> fns = new ArrayList<>();
		for (SurfacePointImpl nsp : ns) {
			double dp = Math.abs(v.normalize().dotProduct(nsp.getVectorToSource().normalize()));
			// System.out.println(dp);
			if (dp > 0.5) {
				fns.add(nsp);
			}
		}
		return fns;
	}


	// add the the surfacePoint on the fine grid to the course neigbour list if it is
	// the same point
	private static void addIfOverlap(SurfacePointImpl sp, List<SurfacePointImpl> corseGrid,
			List<SurfacePointImpl> corseNeighbours) {
		for (SurfacePointImpl c : corseGrid) {
			if (c.getLocation().subtract(sp.getLocation()).getNorm() < (1E-6) * c.getVectorToSource().getNorm()) {
				if (!corseNeighbours.contains(c)) {
					corseNeighbours.add(c);
					return;
				}
			}
		}

	}
}
