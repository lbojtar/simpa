/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.b2a.solve;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealLinearOperator;
import org.apache.commons.math3.linear.RealVector;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.utils.CalculatorUtils;
import simpa.core.b2a.SurfacePointImpl;
import simpa.core.nativ.FMM3DEvaluator;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of
 * the @see{@link org.apache.commons.math3.linear.RealLinearOperator} This
 * linear operator uses the FMM3D native library for its operation. This class
 * is used for the iterative solver.
 */
public class StaticElectricLinearOperator extends SimpaLinearOperator {
	private final FMM3DEvaluator fmmE = new FMM3DEvaluator();
	private final List<SurfacePointImpl> sPoints;
	private final int dim;

	/**
	 * @param sPoints list of surface points.
	 */
	protected StaticElectricLinearOperator(List<SurfacePointImpl> sPoints) {
		this.sPoints = sPoints;
		this.dim = sPoints.size();
	}


	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getColumnDimension() {
		return dim;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getRowDimension() {
		return dim;
	}

	@Override
	public int getDegreesOfFreedom() {
		return 1;
	}

	/**
	 * @param x- Currents or charges at some level of the multigrid. Scales all
	 *           sources in the agreggates with this vector and calculates the
	 *           projections of the fields at the surface points.
	 * @return - Field projections at the surface points at some level of the
	 *         multigrid. {@inheritDoc}
	 */
	@Override
	public RealVector operate(RealVector x) throws DimensionMismatchException {
		for (int i = 0; i < sPoints.size(); i++) {
			sPoints.get(i).setAggregateStrengths(x.getEntry(i));
		}

		double[][][] potentialsAndGradients = fmmE.getPotentialsAndGradientsAtAllTargets(sPoints);

		double[] eArray = new double[dim];
		int nDest = potentialsAndGradients.length;

		for (int i = 0; i < nDest; i++) {
			Vector3D eField = CalculatorUtils.getEField(potentialsAndGradients[i]);
			SurfacePointImpl dest = sPoints.get(i);

			Vector3D normal = dest.getVectorToSource().normalize();
			double projection = normal.dotProduct(eField);

			eArray[i] = projection;
		}

		return new ArrayRealVector(eArray);
	}

}
