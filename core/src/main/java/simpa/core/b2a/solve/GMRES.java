/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
/*
 * Copyright (C) 2003-2006 Bj??rn-Ove Heimsund
 * 
 * This file is part of MTJ.
 * 
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
/*
 * Derived from public domain software at http://www.netlib.org/templates
 */

package simpa.core.b2a.solve;

import org.apache.commons.math3.linear.*;

/**
 * GMRES solver. GMRES solves the unsymmetric linear system <code>Ax = b</code>
 * using the Generalized Minimum Residual method. The GMRES iteration is
 * restarted after a given number of iterations. By default it is restarted
 * after 30 iterations.
 * 
 * @author Templates
 */
public class GMRES extends AbstractIterativeSolver {

	/**
	 * After this many iterations, the GMRES will be restarted.
	 */
	private int restart;

	/**
	 * Vectors for use in the iterative solution process
	 */
	private RealVector w, u, r;

	/**
	 * Vectors spanning the subspace
	 */
	private RealVector[] v;

	/**
	 * Restart vector
	 */
	private RealVector s;

	/**
	 * Hessenberg matrix
	 */
	private RealMatrix H;

	/**
	 * Givens rotations for the QR factorization
	 */
	private GivensRotation[] rotation;

	/**
	 * Constructor for GMRES. Uses the given vector as template for creating scratch
	 * vectors. Typically, the solution or the right hand side vector can be passed,
	 * and the template is not modified. The iteration is restarted every 30
	 * iterations
	 * 
	 * @param template Vector to use as template for the work vectors needed in the
	 *                 solution process
	 */
	public GMRES(RealVector template) {
		this(template, 30);
	}

	/**
	 * Constructor for GMRES. Uses the given vector as template for creating scratch
	 * vectors. Typically, the solution or the right hand side vector can be passed,
	 * and the template is not modified
	 * 
	 * @param template Vector to use as template for the work vectors needed in the
	 *                 solution process
	 * @param restart  GMRES iteration is restarted after this number of iterations
	 */
	public GMRES(RealVector template, int restart) {
		super();
		w = template.copy();
		u = template.copy();
		r = template.copy();
		setRestart(restart);
	}

	/**
	 * Sets the restart parameter
	 * 
	 * @param restart GMRES iteration is restarted after this number of iterations
	 */
	public void setRestart(int restart) {
		this.restart = restart;
		if (restart <= 0)
			throw new IllegalArgumentException("restart must be a positive integer");

		s = new ArrayRealVector(new double[restart + 1]);
		H = new Array2DRowRealMatrix(new double[restart + 1][restart]);
		rotation = new GivensRotation[restart + 1];

		v = new RealVector[restart + 1];
		for (int i = 0; i < v.length; ++i) {
			RealVector c = r.copy();
			c.set(0);
			v[i] = c;
		}
	}

	public RealVector solve(RealLinearOperator A, RealVector b, RealVector x) throws IterativeSolverNotConvergedException {
		checkSizes(A, b, x);

		u.setSubVector(0, b);
		u.combineToSelf(1, -1, A.operate(x));
		
	
		r=M.operate(u); //initial residual times M
		double normr = r.getNorm();
		u=M.operate(b);

		// Outer iteration
		for (iter.setFirst(); !iter.converged(r, x); iter.next()) {

			v[0].combineToSelf(0, 1 / normr, r);
			s.mapMultiplyToSelf(0).setEntry(0, normr);
			int i = 0;

			// Inner iteration
			for (; i < restart && !iter.converged(Math.abs(s.getEntry(i))); i++, iter.next()) {
				u = A.operate(v[i]); 
				w=M.operate(u);

				for (int k = 0; k <= i; k++) {
					H.setEntry(k, i, w.dotProduct(v[k]));			
					w=w.add(v[k].mapMultiply(-H.getEntry(k, i)));
				}
				H.setEntry(i + 1, i, w.getNorm());
				v[i + 1].combineToSelf(0, 1 / H.getEntry(i + 1, i), w);

				// QR factorization of H using Givens rotations
				for (int k = 0; k < i; ++k)
					rotation[k].apply(H, i, k, k + 1);

				rotation[i] = new GivensRotation(H.getEntry(i, i), H.getEntry(i + 1, i));
				rotation[i].apply(H, i, i, i + 1);
				rotation[i].apply(s, i, i + 1);
			}
			
			// Update solution in current subspace
			update(x, i-1, H, s, v);

			u.setSubVector(0, b);
			u.combineToSelf(1, -1, A.operate(x));

			r=M.operate(u);
			normr = r.getNorm();
		//	System.out.println("Norm u "+u.getNorm());
		}

		return x;
	}

	private void update(RealVector x, int k, RealMatrix h, RealVector s, RealVector[] v) {
		RealVector y = s.copy();
		// Backsolve:
		for (int i = k; i >= 0; i--) {	
			y.setEntry(i, y.getEntry(i) / h.getEntry(i, i));
			for (int j = i - 1; j >= 0; j--) {
				double d = y.getEntry(j) - h.getEntry(j, i) * y.getEntry(i);
				y.setEntry(j, d);
			}
		}

		for (int j = 0; j <= k; j++)
			x.combineToSelf(1, y.getEntry(j), v[j]);
	}

	
}
