// @formatter:off
/*
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.b2a.solve;

import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.SurfacePoint;
import simpa.core.b2a.SolverImpl;
import simpa.core.b2a.SurfacePointImpl;

/**
 * Solver that solves the strengths of the sources in the static electric case.
 */
public class StaticElectricSolverImpl implements DirectSolver, SolverImpl {
	private static final Logger logger = LogManager.getLogger(StaticElectricSolverImpl.class);

	private LUSolver lu;
	private int matrixDim;
	private List<SurfacePoint> spoints;
	private RealVector x;
	private double[] b;
	private double residual;
	private double[][] matrix;
	
	public StaticElectricSolverImpl() {		
	}

	public StaticElectricSolverImpl(List<SurfacePoint> spoints) {

		this.spoints = spoints;
		matrix=getMatrix(spoints);
		lu = new LUSolver(matrix);
	}

	public double[][] getMatrix(List<SurfacePoint> surfPoints) {

		// count gauss points
		matrixDim = spoints.size();

		double[][] m = new double[matrixDim][matrixDim];
		long startTime = System.currentTimeMillis();
		logger.debug("Setting up the matrix with size " + matrixDim + " x " + matrixDim);

		int row = 0;

		for (int i = 0; i < spoints.size(); i++) {
			SurfacePointImpl spoint = (SurfacePointImpl)spoints.get(i);
			int col = 0;

			for (SurfacePoint s : spoints) {
				SurfacePointImpl surfacePoint=(SurfacePointImpl)s;
				surfacePoint.setAggregateStrengths(1);
				// B or E of the source at the destination
				Vector3D fieldOfSource;

				fieldOfSource = surfacePoint.getSourceField(spoint.getLocation());
				// The normal component at the destination point
				m[row][col] = fieldOfSource.dotProduct(spoint.getVectorToSource().normalize());
				col++;

			}

			row++;
		}

		long endTime = System.currentTimeMillis();
		logger.debug("Matrix set up and LU decomposition done in: " + (startTime - endTime) + " ms");
		return m;
	}

	public RealVector solve() {

		b = new double[matrixDim];

		for (int i = 0; i < matrixDim; i++) {
			SurfacePointImpl spoint =(SurfacePointImpl) spoints.get(i);
			Vector3D givenEField = spoint.getField();
			// normal component of B or E at the quadrature points
			b[i] = givenEField.dotProduct(spoint.getVectorToSource().normalize());
		}

		ArrayRealVector rmb = new ArrayRealVector(b);
		RealVector x = lu.solve(rmb);

		for (int i = 0; i < spoints.size(); i++) {
			((SurfacePointImpl)spoints.get(i)).setAggregateStrengths(x.getEntry(i));			
		}

		return x;
	}

	@Override
	public double getResidual() {
		return residual;
	}

	@Override
	/**
	 * Solves to linear problem A.x=b with the direct method and set the result
	 * currents into the sources.
	 */
	public void solve(double relativeError) {
		x = solve();
		RealMatrix xm = new Array2DRowRealMatrix(x.toArray());
		RealMatrix rmb = new Array2DRowRealMatrix(b);
		RealMatrix m = new Array2DRowRealMatrix(matrix);
		RealMatrix diffb = rmb.subtract(m.multiply(xm));
		residual = diffb.getNorm() / rmb.getNorm();
		logger.error("Relative residual norm = "+residual);
		
		if (residual > relativeError)
			logger.error("The residual from the direct solver is bigger than the given error limit!");
	}


}
