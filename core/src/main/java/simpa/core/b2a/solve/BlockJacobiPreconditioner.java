package simpa.core.b2a.solve;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.linear.RealLinearOperator;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.SurfacePoint;
import simpa.core.api.SystemConstants;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.b2a.RangeSearch;
import simpa.core.b2a.SurfacePointImpl;

/**
 * Block Jacobi preconditioner for iterative solver. The surface is partitioned
 * into usually overlapping patches and solved separately by direct solvers. The
 * direct solvers are created and LU factored in the constructor. This takes
 * some time, but the consecutive calls for the operate method are fast.
 * 
 * @author lbojtar
 *
 */
public class BlockJacobiPreconditioner extends RealLinearOperator {

	public static Logger logger = LogManager.getLogger(BlockJacobiPreconditioner.class);

	private int dim;
	private int dof = 1;
	private BlockSolver blockSolver;
	private List<SurfacePointImpl> spoints;

	/**
	 * Creates a block-Jacobi preconditioner. It constructs first a coarse grid,
	 * then creates aggregates of surface points around each coarse grid point. The
	 * degree of coarsening is determined by the aggregateRadius parameter. The
	 * distance between the coarse points are more than the coarsening radius. After
	 * coarsening, it creates local direct solvers belonging to patches around each
	 * coarse point. The number of fine points in a patch must be at least the
	 * number of fine grid points in the aggregate belonging to the same coarse
	 * point. Therefore the patchRadius must be at least as big as the
	 * aggregateRadius, but usually bigger, to have some overlap. This overlap is
	 * needed to discard the result at the border of the patch, since those points
	 * can be far from the solution, due to the finite extent of the patch. Only the
	 * local solution belonging to the aggregate is used for pre-conditioning.
	 * 
	 * @param spoints         List of all surface points on the fine level.
	 * 
	 * @param aggregateRadius This parameter determines the level of coarsening. It
	 *                        must be bigger than the source elevation.
	 * 
	 * @param patchRadius     The radius of the local patches. The bigger the patch
	 *                        the better the convergence of the iterative method,
	 *                        but it takes longer to prepare the precontitioner.
	 *                        Typically a few hundred points per patch is a good
	 *                        choice. Be careful, the the time to prepare the
	 *                        preconditioner goes up with the third power of the
	 *                        number of point in a patch, assuming the same
	 *                        aggregateRadius.
	 *@param debug			  If true, debug information will be written into the
	 *						  /tmp directory, these are:
	 *                        - "coarseGrid.txt" The coordinates of the coarse grid points.
	 *                        - "fineGrid.txt"   The coordinates of the fine grid points.
	 *                        - "aggregates.txt" The coordinates of the aggregates.
	 * @throws KnownFatalException
	 */
	public BlockJacobiPreconditioner(List<SurfacePointImpl> spoints, double aggregateRadius, double patchRadius,boolean debug)
			throws KnownFatalException {

		if (patchRadius < aggregateRadius) {
			throw new IllegalArgumentException("The patch radius can not be smaller than the coarsening radius !");
		}

		if (aggregateRadius < spoints.get(0).getVectorToSource().getNorm()) {
			throw new IllegalArgumentException("The coarsening radius must be bigger than the source elevation!");
		}

		this.spoints = spoints;

		if (spoints.get(0).getSource() instanceof CurrentPointSource) {
			dof = 2;
		}
		this.dim = spoints.size() * dof;

		List<SurfacePointImpl> coarseGrid = MIS.createCorseGrid(spoints, aggregateRadius);
		// debug level or more
		if (debug) {
			SolverDebugUtils.surfacePointsImplToFile(coarseGrid, SystemConstants.TMP_DIR + "coarseGrid.txt");
			SolverDebugUtils.surfacePointsImplToFile(spoints, SystemConstants.TMP_DIR + "fineGrid.txt");
			SolverDebugUtils.aggregatesToFile(coarseGrid, SystemConstants.TMP_DIR + "aggregates.txt");
		}

		RangeSearch rs = new RangeSearch(spoints);
	
		Map<SurfacePointImpl, List<SurfacePoint>> patches= new HashMap<>();
		for(SurfacePointImpl cp:coarseGrid) {
			List<SurfacePointImpl> l=rs.searchNeighbors(cp, patchRadius);
			//cast the list
			List<SurfacePoint> l2= new ArrayList<>();
			l2.addAll(l);
			patches.put(cp, l2);
		}
		
		blockSolver = new BlockSolver(spoints, patches);

	}

	@Override
	public int getRowDimension() {
		return dim;
	}

	@Override
	public int getColumnDimension() {
		return dim;
	}

	@Override
	public RealVector operate(RealVector x) throws DimensionMismatchException {
		RealVector sol = null;
		try {
			SimpaLinearOperator.setRightSide(spoints, x);
			sol = blockSolver.solve();
		} catch (KnownFatalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// SimpaLinearOperator.writeOnSurface("/tmp/I.txt", sol,spoints);
		return sol;
	}
	
}
