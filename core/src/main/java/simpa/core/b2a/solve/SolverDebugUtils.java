package simpa.core.b2a.solve;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.RealVector;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.SurfacePoint;
import simpa.core.api.utils.FileUtils;
import simpa.core.b2a.SurfacePointImpl;

public class SolverDebugUtils {

	public static void surfacePointsImplToFile(List<SurfacePointImpl> spl, String file) {
		List<SurfacePoint> l= new ArrayList<>();
		l.addAll(spl);
		surfacePointsToFile(l,file);
	}
	
	/**
	 * Writes the surface points and the given field inside them (if not null) to a
	 * file.
	 * 
	 * @param spl  A list of surface points
	 * @param file - A filename
	 */
	public static void surfacePointsToFile(List<SurfacePoint> spl, String file) {

		StringBuffer sb = new StringBuffer();
		if (spl.get(0).getField() == null)
			sb.append("#X Y Z" + System.lineSeparator()); // grid points
		else
			sb.append("#X Y Z Fx Fy Fz" + System.lineSeparator());// grid points +field

		for (SurfacePoint p : spl) {
			Vector3D l = p.getLocation();
			Vector3D b = p.getField();
			if (b == null) {
				sb.append(l.getX() + " " + l.getY() + " " + l.getZ() + System.lineSeparator());
			} else {
				sb.append(l.getX() + " " + l.getY() + " " + l.getZ() + " " + b.getX() + " " + b.getY() + " " + b.getZ()
						+ System.lineSeparator());
			}
		}
		FileUtils.writeTextFile(file, sb.toString());
	}

	/**
	 * Writes the aggregates belonging to each point in the given corse grid for
	 * visualization.
	 * 
	 * GNUPLOT COMMAND: plot "grid3.txt" u 1:3 , "aggregates3.txt" u 1:3:5 palette
	 * Visualization could be improved using the 4 coloring theorem, but not
	 * implemented.
	 * 
	 * @param corseGrid A list of surface points
	 * @param file      - A filename
	 */
	public static void aggregatesToFile(List<SurfacePointImpl> corseGrid, String file) {

		StringBuffer sb = new StringBuffer();
		sb.append("#x y z corseIndex colorcode" + System.lineSeparator());

		for (int i = 0; i < corseGrid.size(); i++) {

			List<SurfacePointImpl> aggregates = corseGrid.get(i).getAggregates();

			for (SurfacePointImpl fp : aggregates) {
				Vector3D loc = fp.getLocation();
				sb.append(loc.getX() + " " + loc.getY() + " " + loc.getZ() + " " + i + " " + (i % 4)
						+ System.lineSeparator());
			}
		}
		FileUtils.writeTextFile(file, sb.toString());
	}

	/**
	 * Writes the given data on the surface to a file for debugging.
	 * 
	 * @param f        - file
	 * @param x        - Data to be written. Usually the residual of the iteration
	 *                 process. If null the current or charge of the source is
	 *                 written to the file.
	 * @param solution - If true the given x vector written.
	 * @param spl      -- List of surface points
	 */
	public static void writeOnSurface(String f, RealVector x, List<SurfacePointImpl> spl, boolean solution) {
		int dof = 1;
		if (spl.get(0).getSource() instanceof CurrentPointSource)
			dof = 2;

		StringBuffer sb = new StringBuffer();
		sb.append("#x y z Vx Vy Vz" + System.lineSeparator());

		for (int i = 0; i < spl.size(); i++) {
			SurfacePointImpl sp = spl.get(i);
			Vector3D loc = sp.getLocation();
			sb.append(loc.getX() + " " + loc.getY() + " " + loc.getZ() + " ");
			if (dof == 2) {
				Vector3D v;
				CurrentPointSource ps = ((CurrentPointSource) sp.getSource());
				if (x != null) {
					double vx;
					double vz;
					if (solution) {
						vz = x.getEntry(dof * i);
						vx = x.getEntry(dof * i + 1);
					} else {
						vx = x.getEntry(dof * i);
						vz = x.getEntry(dof * i + 1);
					}
					v = ps.getLocalX().scalarMultiply(vx).add(ps.getLocalZ().scalarMultiply(vz));
				} else {
					v = ps.getCurrent();
				}
				sb.append(v.getX() + " " + v.getY() + " " + v.getZ());
			}

			if (dof == 1) {
				double s;
				if (x != null) {
					s = x.getEntry(i);
				} else {
					ElectricMonopole ps = ((ElectricMonopole) sp.getSource());
					s = ps.getCharge();
				}
				Vector3D v = sp.getVectorToSource().normalize().scalarMultiply(s);
				sb.append(v.getX() + " " + v.getY() + " " + v.getZ());
			}

			sb.append(System.lineSeparator());
		}
		FileUtils.writeTextFile(f, sb.toString());
	}
}
