/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.b2a.solve;

import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.Array2DRowRealMatrix;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.SurfacePoint;
import simpa.core.b2a.SolverImpl;
import simpa.core.b2a.SurfacePointImpl;

/**
 * Solver that solves the strengths of the sources in the static magnetic case.
 */
public class StaticMagneticSolverImpl implements DirectSolver, SolverImpl {
	public static final Logger logger = LogManager.getLogger(StaticMagneticSolverImpl.class);

	private LUSolver lu;
	private final int dof = 2; // degrees of freedom
	private int mdim;
	private List<SurfacePoint> surfPoints;
	private double[][] matrix;
	private RealVector x;
	private double[] b;
	private double residual;

	public StaticMagneticSolverImpl() {
	}

	public StaticMagneticSolverImpl(List<SurfacePoint> surfPoints) {

		this.surfPoints = surfPoints;

		mdim = surfPoints.size() * dof;

		long t0 = System.currentTimeMillis();
		logger.debug("Starting LU decomposition");
		matrix = getMatrix(surfPoints);
		lu = new LUSolver(matrix);
		long t1 = System.currentTimeMillis();
		logger.debug("LU done in: " + (t1 - t0) + " ms");
	}

	public double[][] getMatrix(List<SurfacePoint> spl) {
		mdim = spl.size() * dof;
		double[][] m = new double[mdim][mdim];
		long t0 = System.currentTimeMillis();
		logger.debug("Setting up the matrix with size " + mdim + " x " + mdim);

		int row = 0;

		for (int desti = 0; desti < spl.size(); desti++) {

			SurfacePointImpl dest = (SurfacePointImpl) spl.get(desti);
			CurrentPointSource ps = (CurrentPointSource) dest.getSource();

			// Vector pointing to the destination Gauss point
			Vector3D rd = dest.getLocation();

			int col = 0;

			for (SurfacePoint s : spl) {
				SurfacePointImpl sp = (SurfacePointImpl) s;
				sp.setAggregateStrengths(0, 0, 1);
				Vector3D bf = sp.getSourceField(rd);
				m[row][col] = ps.projectToLocalX(bf);
				m[row + 1][col] = ps.projectToLocalZ(bf);

				sp.setAggregateStrengths(1, 0, 0);
				bf = sp.getSourceField(rd);
				m[row][col + 1] = ps.projectToLocalX(bf);
				m[row + 1][col + 1] = ps.projectToLocalZ(bf);

				col += dof;
			}

			row += dof;
		}

		long t1 = System.currentTimeMillis();
		logger.debug("Matrix set up  done in: " + (t1 - t0) + " ms");
		return m;
	}

	/**
	 * Solves the linear problem
	 * 
	 * @return solution
	 */
	public RealVector solve() {

		b = new double[mdim];

		int row = 0;

		for (int desti = 0; desti < surfPoints.size(); desti++) {

			SurfacePointImpl dest = (SurfacePointImpl) surfPoints.get(desti);
			CurrentPointSource ps = (CurrentPointSource) dest.getSource();

			Vector3D br = dest.getField();

			b[row] = ps.projectToLocalX(br);
			b[row + 1] = ps.projectToLocalZ(br);
			row += dof;
		}

		ArrayRealVector rmb = new ArrayRealVector(b);
		RealVector x = lu.solve(rmb);

		// collect the sources and set them up according to the solution
		int i = 0;
		for (SurfacePoint sp : surfPoints) {
			((SurfacePointImpl) sp).setAggregateStrengths(x.getEntry(i + 1), 0, x.getEntry(i));
			i += dof;
		}

		return x;

	}

	@Override
	public double getResidual() {
		return residual;
	}

	@Override
	/**
	 * Solves to linear problem A.x=b with the direct method and set the result
	 * currents into the sources.
	 */
	public void solve(double relativeError) {
		x = solve();
		RealMatrix xm = new Array2DRowRealMatrix(x.toArray());
		RealMatrix rmb = new Array2DRowRealMatrix(b);
		RealMatrix m = new Array2DRowRealMatrix(matrix);
		RealMatrix diffb = rmb.subtract(m.multiply(xm));
		residual = diffb.getNorm() / rmb.getNorm();
		logger.error("Relative residual norm = "+residual);
		
		if (residual > relativeError)
			logger.error("The residual from the direct solver is bigger than the given error limit!");
	}

}
