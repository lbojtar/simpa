/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.b2a.solve;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.JacobiPreconditioner;
import org.apache.commons.math3.linear.RealLinearOperator;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.SolverOptions;
import simpa.core.api.SurfacePoint;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.b2a.SolverImpl;
import simpa.core.b2a.SurfacePointImpl;

/**
 * Iterative solver using GMRES method. It approximates for the strengths of the
 * sources with the given relative residual.
 */
public class IterativeSolverImpl implements SolverImpl {
	private static final Logger logger = LogManager.getLogger(IterativeSolverImpl.class);

	private GMRES gmres;
	private double residual;
	private RealLinearOperator pc = null;
	private List<SurfacePointImpl> spoints;
	private SolverOptions opts;

	@SuppressWarnings("unused")
	private IterativeSolverImpl() {

	}

	/**
	 * Constructor using GMRES solver with SolverOptions.Preconditioner.SLICE
	 * preconditioner. This is mostly useful for accelerator elements which can be
	 * sliced in the longitudinal direction.
	 * 
	 * @param slides List of slices containing the surface points in the slides.
	 * @param opts   Parameters for the slice preconditioner.
	 */
	public IterativeSolverImpl(List<List<SurfacePoint>> slides, SolverOptions opts) {
		if (opts.getType() != SolverOptions.SolverType.GMRES
				|| opts.getPreconditioner() != SolverOptions.Preconditioner.SLICE) {
			logger.error(" This constructor should be called with GMRES solverType and SLICE preconditioner type !");
		}

		this.opts = opts;
		spoints = new ArrayList<>();
		List<List<SurfacePointImpl>> sllCopy = new ArrayList<>();

		for (int i = 0; i < slides.size(); i++) {
			List<SurfacePoint> sl = slides.get(i);
			List<SurfacePointImpl> l = new ArrayList<>();
			for (int j = 0; j < sl.size(); j++) {
				spoints.add((SurfacePointImpl) sl.get(j));
				l.add(((SurfacePointImpl) sl.get(j)).copy());
			}
			sllCopy.add(l);
		}

		try {
			pc = new SlicePreconditioner(sllCopy, opts.getSliceWidth(), opts.getSliceOverlap(), opts.isDebug());
		} catch (KnownFatalException e) {
			logger.error(e.getMessage());
		}
	}

	/**
	 * Constructor using GMRES solver with SolverOptions.Preconditioner.BLOCKJACOBI
	 * preconditioner.
	 * 
	 * @param opts     Parameters for the BLOCKJACOBI preconditioner.
	 * @param fineGrid All surface points.
	 */
	public IterativeSolverImpl(SolverOptions opts, List<SurfacePoint> fineGrid) {
		if (opts.getType() != SolverOptions.SolverType.GMRES
				|| opts.getPreconditioner() != SolverOptions.Preconditioner.BLOCKJACOBI) {
			logger.error(
					" This constructor should be called with GMRES solverType and BLOCKJACOBI preconditioner type !");
		}

		this.opts = opts;
		spoints = new ArrayList<>();

		for (int i = 0; i < fineGrid.size(); i++) {
			spoints.add((SurfacePointImpl) fineGrid.get(i));
		}

		List<SurfacePointImpl> copy = spoints.stream().map(sp -> sp.copy()).collect(Collectors.toList());
		try {
			if (opts.getPreconditioner() == SolverOptions.Preconditioner.BLOCKJACOBI) {
				pc = new BlockJacobiPreconditioner(copy, opts.getAggregateRadius(), opts.getPatchRadius(),
						opts.isDebug());
			} else if (opts.getPreconditioner() == SolverOptions.Preconditioner.JACOBI) {
				pc = new JacobiPreconditioner(getDiagonal(spoints), false);
			} else {
				logger.error("Unknown preconditioner for iterative solver: " + opts.getPreconditioner());
			}

		} catch (KnownFatalException e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	public double getResidual() {
		return residual;

	}

	@Override
	public void solve(double relativeError) {
		solveWithGMRES(spoints, relativeError);
	}

	private void solveWithGMRES(List<SurfacePointImpl> spoints, double relativeError) {
		long t0 = System.currentTimeMillis();
		double[] x;
		if (spoints.get(0).getSource() instanceof CurrentPointSource)
			x = new double[2 * spoints.size()];
		else
			x = new double[spoints.size()];

		SimpaLinearOperator lo = SimpaLinearOperator.create(spoints);

		RealVector b = getRightSide(spoints);

		RealVector x0 = new ArrayRealVector(x);

		gmres = new GMRES(b, opts.getGmresRestart());
		IterationMonitor mon = new IterationMonitor(); // default
		mon.setRelativeTolerance(relativeError);
		gmres.setIterationMonitor(mon);

		gmres.setPreconditioner(pc);

		try {
			// the source currents are updated at each iteration by the
			// AcceleratedLinearOperator
			gmres.solve(lo, b, x0);
			long t1 = System.currentTimeMillis();
			residual = gmres.getIterationMonitor().getLastResidual();
			logger.info("GMRES finished in " + ((t1 - t0) / 1000) + " seconds.");
		} catch (IterativeSolverNotConvergedException e) {
			logger.error(e.getMessage());
			logger.debug(Arrays.toString(e.getStackTrace()));
		}
	}

	// for JacobiPreconditioner , setting the diagonal element instead of 1 doesn't
	// improve convergence in our case, don't know why..
	private double[] getDiagonal(List<SurfacePointImpl> spoints) {
		double[] diag = null;
		if (spoints.get(0).getSource() instanceof CurrentPointSource) {
			diag = new double[spoints.size() * 2];
			for (int i = 0; i < spoints.size(); i++) {
				diag[2 * i] = 1;
				diag[2 * i + 1] = 1;
			}
		} else {
			diag = new double[spoints.size()];
			for (int i = 0; i < spoints.size(); i++)
				diag[i] = 1;
		}

		return diag;
	}

	private RealVector getRightSide(List<SurfacePointImpl> spoints) {
		RealVector b = null;

		if (spoints.get(0).getSource() instanceof CurrentPointSource) {
			double[] bProjection = new double[2 * spoints.size()];

			for (int i = 0; i < spoints.size(); i++) {
				SurfacePointImpl spoint = spoints.get(i);
				CurrentPointSource cps = (CurrentPointSource) spoint.getSource();
				bProjection[2 * i] = cps.projectToLocalX(spoint.getField());
				bProjection[2 * i + 1] = cps.projectToLocalZ(spoint.getField());
			}
			b = new ArrayRealVector(bProjection);
		} else {
			double[] eProjection = new double[spoints.size()];

			for (int i = 0; i < spoints.size(); i++) {
				SurfacePointImpl spoint = spoints.get(i);
				eProjection[i] = spoint.getVectorToSource().normalize().dotProduct(spoint.getField());
			}
			b = new ArrayRealVector(eProjection);
		}
		return b;
	}
}
