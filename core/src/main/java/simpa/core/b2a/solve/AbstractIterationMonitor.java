/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
/*
 * Copyright (C) 2003-2006 Bj??rn-Ove Heimsund
 * 
 * This file is part of MTJ.
 * 
 * This library is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation; either version 2.1 of the License, or (at your
 * option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License
 * for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library; if not, write to the Free Software Foundation,
 * Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

package simpa.core.b2a.solve;

import org.apache.commons.math3.linear.RealVector;

/**
 * Partial implementation of an iteration reporter
 */
public abstract class AbstractIterationMonitor{

	
	/**
	 * Initial residual
	 */
	double initR;

	
    /**
     * Iteration number
     */
    protected int iter;
    
    /**
     * Current residual
     */
    protected double residual;

    /**
     * Constructor for AbstractIterationMonitor. Default norm is the 2-norm with
     * no iteration reporting.
     */
    public AbstractIterationMonitor() {  
      
    }

    public void setFirst() {
        iter = 0;
    }

    public boolean isFirst() {
        return iter == 0;
    }

    public void next() {
        iter++;
    }

    public int iterations() {
        return iter;
    }

    public boolean converged(RealVector r, RealVector x)
            throws IterativeSolverNotConvergedException {
        return converged(r.getNorm(), x);
    }

    public boolean converged(double r, RealVector x)
            throws IterativeSolverNotConvergedException {       
        this.residual = r;
        return convergedI(r, x);
    }

    public boolean converged(double r)
            throws IterativeSolverNotConvergedException {       
        this.residual = r;
        return convergedI(r);
    }

    protected abstract boolean convergedI(double r, RealVector x)
            throws IterativeSolverNotConvergedException;

    protected abstract boolean convergedI(double r)
            throws IterativeSolverNotConvergedException;

    public boolean converged(RealVector r)
            throws IterativeSolverNotConvergedException {
        return converged(r.getNorm());
    }
  
    public double residual() {
        return residual;
    }

}
