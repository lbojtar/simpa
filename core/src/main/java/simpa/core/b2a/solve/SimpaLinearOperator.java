package simpa.core.b2a.solve;

import java.util.List;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealLinearOperator;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.utils.FileUtils;
import simpa.core.b2a.SurfacePointImpl;

/**
 * Abstract class to generalize the solving process for the different type of
 * problems.
 * 
 * @author lbojtar
 *
 */
public abstract class SimpaLinearOperator extends RealLinearOperator {

	private static final Logger logger = LogManager.getLogger(SimpaLinearOperator.class);

	public static SimpaLinearOperator create(List<SurfacePointImpl> spoints) {

		if (spoints.get(0).getSource() instanceof CurrentPointSource)
			return new StaticMagneticLinearOperator(spoints);
		else if (spoints.get(0).getSource() instanceof ElectricMonopole)
			return new StaticElectricLinearOperator(spoints);
		logger.fatal("Unknown SurfacePoint type in SimpaLinearOperator.create !");
		return null;
	}

	@Override
	public abstract int getRowDimension();

	@Override
	public abstract int getColumnDimension();

	@Override
	public abstract RealVector operate(RealVector x) throws DimensionMismatchException;

	/**
	 * 
	 * @return Degrees of freedom for a surface point.
	 */
	public abstract int getDegreesOfFreedom();

	public static void setRightSide(List<SurfacePointImpl> spoints, RealVector b) {
		if (spoints.get(0).getSource() instanceof CurrentPointSource) {
			for (int i = 0; i < spoints.size(); i++) {
				SurfacePointImpl sp = spoints.get(i);
				CurrentPointSource s = (CurrentPointSource) sp.getSource();
				Vector3D v = s.getLocalX().scalarMultiply(b.getEntry(2 * i))
						.add(s.getLocalZ().scalarMultiply( b.getEntry(1 + 2 * i)));
				sp.setField(v);
			}
		} else if (spoints.get(0).getSource() instanceof ElectricMonopole) {
			for (int i = 0; i < spoints.size(); i++) {
				SurfacePointImpl sp = spoints.get(i);
				sp.setField(sp.getVectorToSource().normalize().scalarMultiply(b.getEntry(i)));
			}
		} else
			logger.fatal("Unknown SurfacePoint type in SimpaLinearOperator.setRightSide !");
	}

	public static ArrayRealVector getRightSide(List<SurfacePointImpl> spoints) {

		if (spoints.get(0).getSource() instanceof CurrentPointSource) {
			double[] bProjection = new double[2 * spoints.size()];
			for (int i = 0; i < spoints.size(); i++) {
				SurfacePointImpl spoint = spoints.get(i);
				// calculate the right hand side of the linear system of eqns.
				CurrentPointSource cps = (CurrentPointSource) spoint.getSource();
				bProjection[2 * i] = cps.projectToLocalX(spoint.getField());
				bProjection[2 * i + 1] = cps.projectToLocalZ(spoint.getField());
			}
			return new ArrayRealVector(bProjection);

		} else if (spoints.get(0).getSource() instanceof ElectricMonopole) {
			double[] normalComponent = new double[spoints.size()];
			for (int i = 0; i < spoints.size(); i++) {
				SurfacePointImpl spoint = spoints.get(i);
				normalComponent[i] = spoint.getVectorToSource().normalize().dotProduct(spoint.getField());

			}
			return new ArrayRealVector(normalComponent);
		}

		logger.fatal("Unknown SurfacePoint type in SimpaLinearOperator.getRightSide !");
		return null;
	}

}
