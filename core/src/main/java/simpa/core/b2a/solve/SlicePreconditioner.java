package simpa.core.b2a.solve;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.linear.RealLinearOperator;
import org.apache.commons.math3.linear.RealVector;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.SurfacePoint;
import simpa.core.api.SystemConstants;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.b2a.SurfacePointImpl;

public class SlicePreconditioner extends RealLinearOperator {

	private static final Logger logger = LogManager.getLogger(SlicePreconditioner.class);
	private int dof;
	private List<SurfacePointImpl> fineLevel;
	private List<List<SurfacePointImpl>> slices;
	private List<SurfacePointImpl> corsePoints;
	private BlockSolver blockSolver;
	private boolean debug;
	
	public SlicePreconditioner(List<List<SurfacePointImpl>> slices, int sliceWidth, int overlap, boolean debug)
			throws KnownFatalException {

		this.slices = slices;
		this.debug=debug;
		
		setupCorsePoints();

		if (slices.get(0).get(0).getSource() instanceof CurrentPointSource)
			dof = 2;
		else
			dof = 1;

		fineLevel = new ArrayList<>();
		for (int i = 0; i < slices.size(); i++) {
			List<SurfacePointImpl> sl = slices.get(i);
			fineLevel.addAll(sl);
		}

		logger.info("Preparing slice preconditioner");
		Map<SurfacePointImpl, List<SurfacePoint>> patches = getPatches(sliceWidth, overlap);
		
		blockSolver = new BlockSolver(fineLevel, patches);
	}

	@Override
	public int getRowDimension() {
		return fineLevel.size() * dof;
	}

	@Override
	public int getColumnDimension() {
		return fineLevel.size() * dof;
	}

	@Override
	public RealVector operate(RealVector x) throws DimensionMismatchException {
		RealVector sol = null;
		try {
			SimpaLinearOperator.setRightSide(fineLevel, x);
			sol = blockSolver.solve();
		} catch (KnownFatalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// SimpaLinearOperator.writeOnSurface("/tmp/I.txt", sol,spoints);
		return sol;
	}

	private Map<SurfacePointImpl, List<SurfacePoint>> getPatches(int sliceWidth, int overlap) {

		Map<SurfacePointImpl, List<SurfacePoint>> patches = new HashMap<>();

		for (int i = 0; i < slices.size(); i+=sliceWidth) {
			List<SurfacePoint> patch = new ArrayList<>();
			List<SurfacePointImpl> aggregate = new ArrayList<>();
			int patchStart = i - overlap;
			if (patchStart < 0)
				patchStart = 0;

			int patchEnd = i + sliceWidth + overlap;
			if (patchEnd > slices.size())
				patchEnd = slices.size();

			SurfacePointImpl cp = corsePoints.get(i);
			//collect slices into the current patch
			for (int j = patchStart; j < patchEnd; j++)
				patch.addAll(slices.get(j));
			
			patches.put(cp, patch);
			if (debug)
				SolverDebugUtils.surfacePointsToFile(patch, SystemConstants.TMP_DIR + "patch"+i+".txt");
			//agregate is like the patch, but without the overlap
			int aggrStart = i ;
			int aggrEnd = i + sliceWidth ;
			if (aggrEnd > slices.size())
				aggrEnd = slices.size();
			
			for (int j = aggrStart; j < aggrEnd; j++)
				aggregate.addAll(slices.get(j));
			cp.getAggregates().addAll(aggregate);			
		}

		if (debug) {				
			SolverDebugUtils.surfacePointsImplToFile(fineLevel, SystemConstants.TMP_DIR + "fineGrid.txt");
			SolverDebugUtils.surfacePointsImplToFile(corsePoints, SystemConstants.TMP_DIR + "coarseGrid.txt");
			SolverDebugUtils.aggregatesToFile(corsePoints, SystemConstants.TMP_DIR + "aggregates.txt");				
		}
		
		return patches;
	}

	// We generate a coarse grid by averaging. Location doesn't matter, but we do it
	// to be able to use debug methods
	private void setupCorsePoints() {
		corsePoints = new ArrayList<>();
		for (List<SurfacePointImpl> slice : slices) {
			Vector3D sum = new Vector3D(0, 0, 0);
			for (SurfacePointImpl p : slice) {
				sum = sum.add(p.getLocation());
			}
			Vector3D center = sum.scalarMultiply(1.0 / slice.size());
			SurfacePointImpl sp = new SurfacePointImpl(center, null);		
			corsePoints.add(sp);
		}

	}

}
