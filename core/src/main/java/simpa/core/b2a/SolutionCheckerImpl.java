// @formatter:off
/*
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.b2a;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.*;
import simpa.core.api.utils.CalculatorUtils;
import simpa.core.api.utils.FileUtils;
import simpa.core.nativ.FMM3DEvaluator;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Class for evaluating solutions.
 */
public class SolutionCheckerImpl {

    private final SourceArrangement sa;
    private final FieldType type;
    private final String fieldMapName;
    private final LengthUnit lengthUnit;

    public SolutionCheckerImpl(String name, FieldType type, LengthUnit lengthUnit) {
        this.fieldMapName = name;
        this.type = type;
        this.lengthUnit = lengthUnit;

        String f = name + FileNamingConventions.getSolutionFileExtension();

        sa = new SourceArrangement(f, Vector3D.ZERO, Rotation.IDENTITY, 1.0, name);
    }

    /**
     * Calculates the normalized root-mean-square error of the vectors compared to reference vectors.
     *
     * @param vall vectors to check
     * @param refl reference vectors
     * @return the root-mean-square error
     */
    public static double getNormalizedRmsError(List<Vector3D> vall, List<Vector3D> refl) {

        if (vall.size() != refl.size()) {
            throw new IllegalArgumentException(
                    "The list of vectors and the list of reference vectors have to have the same size !!");

        }

        double summOfErrSq = 0;
        double summ = 0;
        int n = vall.size();

        for (int i = 0; i < n; i++) {
            Vector3D v = vall.get(i);
            Vector3D refv = refl.get(i);
            double diff = v.subtract(refv).getNorm();
            summOfErrSq += diff * diff;
            summ += refv.getNorm();
        }
        double av = summ / n;
        double rmsd = Math.sqrt(summOfErrSq / n);

        return rmsd / av; // normalized rms err
    }

    /**
     * Compares a reference file to the solution file and gets the RMS (root mean
     * square) error of an element.
     *
     * @param scalingFactor To scale the solution file to the scale of the
     *                      reference file
     * @return RMS error
     * @throws FileNotFoundException when a required file is not found.
     */
    public double getNormalizedRmsError(double scalingFactor) throws FileNotFoundException {

        String fn = fieldMapName + FileNamingConventions.getFieldAtCheckFileExtension();
        List<Vector3D> brefl = FileUtils.readVectors(fn, 3, 1.0);
        List<Vector3D> checkPoints = FileUtils.readVectors(fn, 0, 1.0 / lengthUnit.getFactor());
        writeFieldCheckFile(checkPoints, scalingFactor);
        List<Vector3D> bl = FileUtils.readVectors(fieldMapName + FileNamingConventions.getCheckFileExtension(), 3, 1.0);
        return getNormalizedRmsError(bl, brefl);
    }


    /**
     * Writes a file with check point coordinates and B or E field values inside.
     * This is normally used for comparing the B or E vectors obtained from a CAD EM
     * software at those points in order to check the solution.
     */
    private void writeFieldCheckFile(List<Vector3D> checkPoints, double scaling) {

        String cfn = sa.getName() + FileNamingConventions.getCheckFileExtension();

        StringBuilder checkSb = new StringBuilder();

        if (type == FieldType.STATIC_MAGNETIC) {
            checkSb.append("#X Y Z Bx By Bz").append(System.lineSeparator());
        } else if (type == FieldType.STATIC_ELECTRIC) {
            checkSb.append("#X Y Z Ex Ey Ez").append(System.lineSeparator());
        } else {
            throw new IllegalArgumentException("Unknown field type: "+type);
        }

        List<PointSource> sources = sa.getPointSources();

        FMM3DEvaluator fmm3d = new FMM3DEvaluator();
        double[][][] potentialsAndDerivatives = fmm3d.getPotentialsAndGradientsAtAllTargets(sources, checkPoints);

        for (int i = 0; i < potentialsAndDerivatives.length; i++) {
            Vector3D r = checkPoints.get(i);
            Vector3D fv = null;
            double lsf = lengthUnit.getFactor();

            if (type == FieldType.STATIC_MAGNETIC) {
                fv = CalculatorUtils.getBField(potentialsAndDerivatives[i]);
            }

            if (type == FieldType.STATIC_ELECTRIC) {
                fv = CalculatorUtils.getEField(potentialsAndDerivatives[i]);
            }

            String pos = (r.getX() * lsf) + " " + (r.getY() * lsf) + " "
                    + (r.getZ() * lsf);

            checkSb.append(pos).append(" ").append(fv.getX() * scaling).append(" ").append(fv.getY() * scaling).append(" ").append(fv.getZ() * scaling).append(System.lineSeparator());
        }
        FileUtils.writeTextFile(cfn, checkSb.toString());
    }
}
