/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.b2a;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.PointSource;
import simpa.core.api.SurfacePoint;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.OcTreeEntry;

/**
 * Implementation of the surface point interface.
 */
public class SurfacePointImpl implements SurfacePoint, OcTreeEntry {

	private Vector3D field;
	private Vector3D location;
	private PointSource source;
	private Vector3D toSource;

	private boolean visited = false; // we use this internally for MIS
	private List<SurfacePointImpl> corseNeigbours;
	private List<SurfacePointImpl> aggregatesAbove;
	private List<SurfacePointImpl> allFineAggregates;

	public SurfacePointImpl(Vector3D location, Vector3D toSource) {
		this.location = location;
		this.toSource = toSource;
		aggregatesAbove = new ArrayList<>();
	}

	public Vector3D getField() {
		return field;
	}

	public void setField(Vector3D field) {
		this.field = field;
	}

	public Vector3D getLocation() {
		return location;
	}

	public void setLocation(Vector3D location) {
		this.location = location;
	}

	public PointSource getSource() {
		return source;
	}

	public Vector3D getVectorToSource() {
		return toSource;
	}

	@Override
	public String sourceToString() {
		return source.toString();
	}

	public void setSource(PointSource source) {
		this.source = source;
		this.setBaseVectorsForSource();
	}

	/**
	 * Sets the base vector for the source based on the B field.
	 */
	public void setBaseVectorsForSource() {
		if (this.field == null) {
			throw new IllegalStateException("Set the B field for the surface before setting the sources.");
		}
		if (this.getSource() instanceof CurrentPointSource ps) {
			Vector3D localy = this.getVectorToSource().normalize();
			Vector3D localz = getTangentVector(localy, getField());
			Vector3D localx = localy.crossProduct(localz);

			ps.setBaseVectors(localx, localy, localz);

		}
	}

	private Vector3D getTangentVector(Vector3D n, Vector3D v) {
		Vector3D t = v.crossProduct(n);
		// if there is no tangential component of the input field,
		// we still need a tangent vector to avoid singular matrix
		if (t.getNorm() == 0) {
			if (n.getX() >= n.getZ() || n.getX() >= n.getY()) // x the biggest
				t = Vector3D.PLUS_K.crossProduct(n);
			else if (n.getY() >= n.getX() || n.getY() >= n.getZ()) // y the biggest
				t = Vector3D.PLUS_I.crossProduct(n);
			else // z is the biggest
				t = Vector3D.PLUS_J.crossProduct(n);

		}

		return t.normalize();
	}

	/**
	 * Returns the field of the source if this instance is on the finest level or
	 * agglomerate of all sources if this point is on a coarse level.
	 * 
	 * @param r Evaluation point.
	 * @return B or E field.
	 */
	public Vector3D getSourceField(Vector3D r) {
		if (allFineAggregates == null)
			return this.source.getField(r);
		else {
			Vector3D sum = Vector3D.ZERO;
			for (SurfacePointImpl sp : allFineAggregates)
				sum = sum.add(sp.getSource().getField(r));
			return sum;
		}

	}

	/**
	 * 
	 * @return A deep copy of most of the members, except corseNeigbours,
	 *         aggregates, and neigbours
	 */
	public SurfacePointImpl copy() {
		SurfacePointImpl c = new SurfacePointImpl(location, toSource);
		c.visited = visited;
		c.field = field;
		c.source = source.copy();
		return c;
	}

	// Used by MIS to construct a coarsening
	public boolean isVisited() {
		return visited;
	}

	// Used by MIS to construct a coarsening
	public void setVisited(boolean visited) {
		this.visited = visited;
	}

	/**
	 * 
	 * @return A list of strongly connected points on the coarse grid below the
	 *         level of this instance.
	 */
	public List<SurfacePointImpl> getCorseNeigbours() {
		return corseNeigbours;
	}

	/**
	 * 
	 * @param neigbours A list of strongly connected points on the coarse grid
	 *                  below.
	 */
	public void setCorseNeigbours(List<SurfacePointImpl> neigbours) {
		this.corseNeigbours = neigbours;
	}

	/**
	 * 
	 * @return A list of strongly connected surface points on the parent grid above
	 *         this instance. usually this instance is on the coarse level. The
	 *         aggregates belonging to different coarse points are NON-OVERLAPPING.
	 *         If we take all points of the coarse level and collect their
	 *         aggregates on the fine level we get exactly all the fine level
	 *         points.
	 */
	public List<SurfacePointImpl> getAggregates() {
		return aggregatesAbove;
	}


	/**
	 * Sets the charge or currents of all sources which belongs to this surface
	 * point.
	 * 
	 * @param as
	 */
	public void setAggregateStrengths(double... strengths) {
		List<SurfacePointImpl> ags = getAggregatesInternal();
		for (SurfacePointImpl sp : ags) {
			if (this.getSource() instanceof ElectricMonopole) {
				((ElectricMonopole) sp.getSource()).setCharge(strengths[0]);
			}
			if (this.getSource() instanceof CurrentPointSource) {
				CurrentPointSource s = ((CurrentPointSource) sp.getSource());
				s.setLocalCurrentComponents(strengths[0], strengths[1], strengths[2]);
			}
		}
	}


	/**
	 * 
	 * @return A list of NON-OVERLAPPING surface points from the finest grid.
	 */
	private List<SurfacePointImpl> getAggregatesInternal() {
		if (allFineAggregates == null) {
			allFineAggregates = new ArrayList<>();
			collectAggregates(allFineAggregates);
		}
		return allFineAggregates;
	}
	
	/**
	 * Calls recursively all the grids to collect the surface points on the finest
	 * level.
	 */
	private void collectAggregates(List<SurfacePointImpl> aggregeates) {
		List<SurfacePointImpl> l = getAggregates();
		// no parents, we are at the finest level, add this point and return
		if (l.size() == 0) {
			aggregeates.add(this);
			return;
		}
		// call recursively the finer levels
		for (SurfacePointImpl cp : l) {
			cp.collectAggregates(aggregeates);
		}
	}

}
