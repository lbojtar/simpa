/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.b2a;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simpa.core.api.SurfacePoint;
import simpa.core.api.utils.TextStlReader;

/**
 * Class that creates the surface points from a STL file.
 */
public class STLTiling  {
	private static final Logger logger = LogManager.getLogger(STLTiling.class);

	/**
	 * Gets the SurfacePoint from a file and adds a given elevation to the sources.
	 *
	 * @param fileName input STL file
	 * @param sourceElevation value to elevate the point by
	 * @return ArrayList of SurfacePoints
	 */
	public static ArrayList<SurfacePoint> createSurfacePoints(String fileName,double sourceElevation) {
		TextStlReader r = new TextStlReader();
		try {
			r.readStlFile(new FileInputStream(fileName));
		} catch (IOException e) {
			logger.error("IOException happened with file: " + fileName + ".");
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

		ArrayList<SurfacePoint> surfacePointList = new ArrayList<>();
		for (int[] face : r.getFaces()) {
			List<Vector3D> vertexList = r.getVertices();
			Vector3D p1=vertexList.get(face[0]);
			Vector3D p2=vertexList.get(face[1]);
			Vector3D p3=vertexList.get(face[2]);
			Vector3D normalUnitVector = Vector3D.crossProduct(p2.subtract(p1), p3.subtract(p2)).normalize();		
			Vector3D centroid = p1.add(p2).add(p3).scalarMultiply(1/3.0);
			SurfacePoint sp=  SurfacePoint.createSurfacePoint(centroid,normalUnitVector.scalarMultiply(sourceElevation));
			surfacePointList.add(sp);
		}	
		return surfacePointList;
	}

}
