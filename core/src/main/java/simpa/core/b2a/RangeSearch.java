/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.b2a;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.SurfacePoint;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.utils.OcTree;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that implements the OcTree class for searching neighbors of
 * SurfacePoints.
 */
public class RangeSearch {
	private final List<SurfacePointImpl> points;
	private OcTree ot;

	public RangeSearch(List<SurfacePointImpl> points) throws KnownFatalException {
		this.points = points;
		setupTree();
	}

	/**
	 * Search the neigbours of a 3D point in a given distance.
	 * 
	 * @param p A 3D vector to the point.
	 * @param r  Search radius.
	 * @return List of neigbours.
	 */
	public List<SurfacePointImpl> searchNeighbors(Vector3D p, double r) {

		List<SurfacePointImpl> nb = ot.findInRange(p, r).stream().map(item -> (SurfacePointImpl) item)
				.toList();
		return nb;

	}
	
	/**
	 * Search the neigbours of a SurfacePoint in a given distance.
	 * 
	 * @param sp A surface point.
	 * @param r  Search radius.
	 * @return List of neigbours.
	 */
	public List<SurfacePointImpl> searchNeighbors(SurfacePointImpl sp, double r) {

		List<SurfacePointImpl> nb = ot.findInRange(sp.getLocation(), r).stream().map(item -> (SurfacePointImpl) item)
				.toList();
		return nb;

	}


	/**
	 * Search neigbours for all surface points in a given distance.
	 * 
	 * @param r Search radius.
	 * @return Neigbours for all surface poins.
	 */
	public List<List<SurfacePointImpl>> searchNeighborsForAll(double r) {
		List<List<SurfacePointImpl>> neighbors = new ArrayList<>();
		for (SurfacePointImpl sp : points) {
			neighbors.add(searchNeighbors(sp, r));
		}
		return neighbors;
	}


	/**
	 * Sets up the OcTree and starts the range search with the given radius
	 */
	private void setupTree() throws KnownFatalException {
		if (points == null || points.isEmpty()) {
			throw new KnownFatalException("Error getting boundaries for surface points");
		}

		Double minx = null;
		Double miny = null;
		Double minz = null;
		Double maxx = null;
		Double maxy = null;
		Double maxz = null;

		for (SurfacePoint point : points) {
			Vector3D loc = point.getLocation();

			if (minx == null || loc.getX() < minx) {
				minx = loc.getX();
			}
			if (miny == null || loc.getY() < miny) {
				miny = loc.getY();
			}
			if (minz == null || loc.getZ() < minz) {
				minz = loc.getZ();
			}
			if (maxx == null || loc.getX() > maxx) {
				maxx = loc.getX();
			}
			if (maxy == null || loc.getY() > maxy) {
				maxy = loc.getY();
			}
			if (maxz == null || loc.getZ() > maxz) {
				maxz = loc.getZ();
			}
		}

		this.ot = new OcTree(minx, miny, minz, maxx, maxy, maxz);
		for (SurfacePoint point : points) {
			ot.insert(point);
		}

	}

}
