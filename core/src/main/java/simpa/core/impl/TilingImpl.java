/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.ExtrudedSurface;
import simpa.core.api.Quadrature;
import simpa.core.api.SurfacePoint;
import simpa.core.api.TilingConfig;
import simpa.core.api.exceptions.NotSetException;

public class TilingImpl {
	private static final Logger logger = LogManager.getLogger(TilingImpl.class);

	private List<Vector3D> pathList;
	private final TilingConfig pars;
	private final RectangularQuadratura gpc;
	private ArrayList<Tile> tiles;

	public TilingImpl(TilingConfig parameters) throws NotSetException {

		this.pars = parameters;
		gpc = new RectangularQuadratura();
		
		if (pars.getQuadrature() == Quadrature.QUADRILATERAL_4_POINTS) {
			createRetangularTiles();
		}
		
		if (pars.getQuadrature() == Quadrature.TRIANGLE_3_POINTS) {
			createTriangleTiles();
		}
	}
	

	/**
	 * Gets the surface points of all the tiles.
	 *
	 * @return ArrayList of SurfacePoints
	 */
	public ArrayList<SurfacePoint> getSurfacePoints() {
		ArrayList<SurfacePoint> surfacePoints = new ArrayList<>();
		for (Tile tile : tiles) {
			SurfacePoint[] gaussPoints = tile.getSurfacePoints();
			Collections.addAll(surfacePoints, gaussPoints);
		}
		return surfacePoints;
	}

	/**
	 * Special method for the slice preconditioner.
	 * 
	 * @return List of List of SurfacePoints. The list is ordered by the
	 *         longitudinal tile number.
	 * 
	 */
	public List<List<SurfacePoint>> getSlices() {

		Map<Integer, List<SurfacePoint>> map = new HashMap<>();

		for (Tile tile : tiles) {
			SurfacePoint[] sPoints = tile.getSurfacePoints();
			List<SurfacePoint> sliceList = map.get(tile.getLongitudinalIndex());
			if (sliceList == null) {
				sliceList = new ArrayList<>();
				Collections.addAll(sliceList, sPoints);
				int li=tile.getLongitudinalIndex();
				map.put(li, sliceList);
			} else {
				Collections.addAll(sliceList, sPoints);
			}
		}

		List<List<SurfacePoint>> ll = new ArrayList<>();
		for (int i = 0; i < map.size(); i++) {
			ll.add(map.get(i));
		}

		return ll;
	}
	
	private void createTriangleTiles() throws NotSetException {
		pathList = pars.getPath();

		logger.info("Creating triangle tiles..");

		tiles = new ArrayList<>();

		ExtrudedSurface s = new ExtrudedSurface(pathList, pars.getProfile());

		for (int li = 0; li < pathList.size() - 1; li++) {
			for (int ti = 0; ti < pars.getProfile().getPoints().size(); ti++) {
				Vector3D p1 = getPointOnProfile(li, ti, s);
				Vector3D p2 = getPointOnProfile(li + 1, ti, s);
				Vector3D p3 = getPointOnProfile(li + 1, ti + 1, s);
				Vector3D p4 = getPointOnProfile(li, ti + 1, s);
				
				TriangleTile rt = new TriangleTile(p3, p2, p1,  pars.getElevation());
				rt.setLongitudinalIndex(li);
				tiles.add(rt);
				
				rt = new TriangleTile(p1, p4, p3, pars.getElevation());
				rt.setLongitudinalIndex(li);
				tiles.add(rt);
				
			}
		}

		double elevation = pars.getElevation();

		if (elevation == 0) {
			throw new IllegalArgumentException("The elevation of the source  must be bigger than zero !!");
		}
	}
	
	private void createRetangularTiles() throws NotSetException {
		pathList = pars.getPath();

		logger.info("Creating retangular tiles..");

		tiles = new ArrayList<>();

		ExtrudedSurface s = new ExtrudedSurface(pathList, pars.getProfile());

		for (int li = 0; li < pathList.size() - 1; li++) {
			for (int ti = 0; ti < pars.getProfile().getPoints().size(); ti++) {
				Vector3D p1 = getPointOnProfile(li, ti, s);
				Vector3D p2 = getPointOnProfile(li + 1, ti, s);
				Vector3D p3 = getPointOnProfile(li + 1, ti + 1, s);
				Vector3D p4 = getPointOnProfile(li, ti + 1, s);
				RectangularTile rt = new RectangularTile(p1, p2, p3, p4, gpc, pars.getElevation());
				rt.setLongitudinalIndex(li);
				tiles.add(rt);
			}
		}

		double elevation = pars.getElevation();

		if (elevation == 0) {
			throw new IllegalArgumentException("The elevation of the source  must be bigger than zero !!");
		}
	}

	private Vector3D getPointOnProfile(int li, int ti, ExtrudedSurface s) throws NotSetException {
		if (ti == pars.getProfile().getPoints().size()) {
			return s.getOrientedProfiles().get(li).getPoints().get(0);
		} else
			return s.getOrientedProfiles().get(li).getPoints().get(ti);
	}


}