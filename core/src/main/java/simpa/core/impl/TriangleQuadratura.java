package simpa.core.impl;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class TriangleQuadratura {

	private static final double[][] GAUSSIAN_POINTS = { { 1.0 / 6.0, 1.0 / 6.0 }, { 2.0 / 3.0, 1.0 / 6.0 },
			{ 1.0 / 6.0, 2.0 / 3.0 } };

	/**
	 * Get the gauss quadratura points on a general 3D triangle tile.
	 * 
	 * @param tile - A tile object.
	 * @return - An array of GaussPoint objects.
	 */
	public static Vector3D[] getGaussPoints(TriangleTile tile) {
	
		Vector3D[] quadraturePoints = new Vector3D[GAUSSIAN_POINTS.length];

		// rotate vertices into the xy plane
		Rotation rot = new Rotation(tile.getNormalUnitVector(), Vector3D.PLUS_K);
		Vector3D[] vertices = new Vector3D[3];
		
		//rotate triangle vertices to XY plane
		for (int i = 0; i < 3; i++) {
			vertices[i] = rot.applyTo(tile.getVertices()[i]);
		}
		double[][] gaussPoints = calculateQuadraturePoints(vertices);
		
		//rotate back the triangle Gaussian points to the original plane
		for (int i = 0; i < gaussPoints.length; i++) {
			Vector3D p = new Vector3D(gaussPoints[i][0], gaussPoints[i][1], vertices[0].getZ());						
			quadraturePoints[i] = rot.applyInverseTo(p);
		}

		return quadraturePoints;
	}

	/**
	 * 
	 * @param vertices Triangle vertices in XY plane, the Z coordinates are ignored.
	 * @return 3 point quadratura points in a general triangle in the XY plane
	 */
	protected static double[][] calculateQuadraturePoints(Vector3D[] vertices) {
		double[][] quadraturePoints = new double[GAUSSIAN_POINTS.length][2];

		for (int i = 0; i < GAUSSIAN_POINTS.length; i++) {
			double lambda1 = GAUSSIAN_POINTS[i][0];
			double lambda2 = GAUSSIAN_POINTS[i][1];
			double lambda3 = 1.0 - lambda1 - lambda2;

			quadraturePoints[i][0] = lambda1 * vertices[0].getX() + lambda2 * vertices[1].getX()
					+ lambda3 * vertices[2].getX();
			quadraturePoints[i][1] = lambda1 * vertices[0].getY() + lambda2 * vertices[1].getY()
					+ lambda3 * vertices[2].getY();
		}

		return quadraturePoints;
	}
}
