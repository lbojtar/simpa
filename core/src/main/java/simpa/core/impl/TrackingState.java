/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.impl;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Object that holds the state of tracking.
 */
public class TrackingState {

	private Vector3D prevPosition;
	private Vector3D prevKineticMomentum;
	private long stepCount = 0;
	private long evalCount = 0;
	private boolean lost = false;
	private boolean terminated = false; // When is set true, the tracking will finish normally.
	private double[][] lastFieldEvaluation; // for debugging

	/**
	 * Gets the previous position of the particle.
	 *
	 * @return The previous position of the particle.
	 */
	public Vector3D getPreviousPosition() {
		return prevPosition;
	}

	/**
	 * Gets the previous kinetic momentum of the particle.
	 *
	 * @return The previous kinetic momentum of the particle.
	 */
	public Vector3D getPreviousKineticMomentum() {
		return prevKineticMomentum;
	}

	/**
	 * Gets the value of isLost, true when the particle is lost.
	 *
	 * @return the value of isLost.
	 */
	public boolean isLost() {
		return lost;
	}

	public void reset() {
		stepCount = 0;
		evalCount = 0;
		terminated = false;
	}

	// API ENDS HERE

	public void setPreviousPosition(Vector3D lastPosition) {
		this.prevPosition = lastPosition;
	}

	public long getStepCount() {
		return stepCount;
	}

	public void setStepCount(long stepCount) {
		this.stepCount = stepCount;
	}

	public void incrementEvalCount() {
		this.evalCount++;
	}

	public void setLost(boolean lost) {
		this.lost = lost;
	}

	public long getEvalCount() {
		return evalCount;
	}

	public void setPreviousKineticMomentum(Vector3D prevKineticMomentum) {
		this.prevKineticMomentum = prevKineticMomentum;
	}

	public boolean isTerminated() {
		return terminated;
	}

	public void setTerminated(boolean terminated) {
		this.terminated = terminated;
	}

	public double[][] getLastFieldEvaluation() {
		return lastFieldEvaluation;
	}

	public void setLastFieldEvaluation(double[][] lastFieldEvaluation) {
		this.lastFieldEvaluation = lastFieldEvaluation;
	}

}
