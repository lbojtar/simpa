/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.impl;

import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.SurfacePoint;
import simpa.core.api.SystemConstants;


public class RectangularTile implements Tile {
	private static final Logger logger = LogManager.getLogger(RectangularTile.class);

	private final Vector3D normalUnitVector;
	private final Vector3D[] vertices;
	private final SurfacePoint[] surfacePoints;
    private int longitudinalIndex; 

	/**
	 * Constructs a rectangular tile based on the vertices of the tile.
	 * 
	 * @param p1 first point
	 * @param p2 second point
	 * @param p3 third point
	 * @param p4 fourth point
	 */
	public RectangularTile(Vector3D p1, Vector3D p2, Vector3D p3, Vector3D p4, RectangularQuadratura gpc,double elevation) {
		// TODO: We have to use triangulation !!! With the circular profile the 4 points
		// are not in plane !!!

		isInPlane(p1, p2, p3, p4); // we give only a warning at the moment

		vertices = new Vector3D[4];
		vertices[0] = p1;
		vertices[1] = p2;
		vertices[2] = p3;
		vertices[3] = p4;

		normalUnitVector = Vector3D.crossProduct(p2.subtract(p1), p3.subtract(p2)).normalize();
		Vector3D[] gaussPoints = gpc.getGaussPoints(this);
		surfacePoints=new SurfacePoint[gaussPoints.length];
		
		for (int i = 0; i < gaussPoints.length; i++) {
			Vector3D gp = gaussPoints[i];
			surfacePoints[i] =  SurfacePoint.createSurfacePoint(gp, normalUnitVector.scalarMultiply(elevation));
		}

	}

	
	public SurfacePoint[] getSurfacePoints() {
		return surfacePoints;
	}

	public Vector3D[] getVertices() {
		return vertices;
	}

	public Vector3D getNormalUnitVector() {
		return normalUnitVector;
	}

	private void isInPlane(Vector3D p1, Vector3D p2, Vector3D p3, Vector3D p4) {
		// we define the tolerance relative to the biggest dimension of the rectangle
		double d1 = p1.subtract(p2).getNorm();
		double d2 = p1.subtract(p3).getNorm();
		double d3 = p2.subtract(p2).getNorm();
		double max = d1;
		if (d2 > max)
			max = d2;
		if (d3 > max)
			max = d3;
		double tolerance = max*SystemConstants.GEOMETRY_TOLERANCE;
		
		Plane pl = new Plane(p1, p2, p3, tolerance);
		if (pl.contains(p4))
			return ;

		String msg=("The points of the rectangle are not in a plane !! Use triangular tiling instead !" + System.lineSeparator() + p1
				+ System.lineSeparator() + p2 + System.lineSeparator() + p3 + System.lineSeparator() + p4
				+ System.lineSeparator());
		double dist = pl.project(p4).distance(p4);
		msg=msg+("Distance = " + dist);
		
		throw new IllegalArgumentException(msg);
	}


	public int getLongitudinalIndex() {
		return longitudinalIndex;
	}


	public void setLongitudinalIndex(int longitudinalIndex) {
		this.longitudinalIndex = longitudinalIndex;
	}
}
