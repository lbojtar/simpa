/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.impl;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class RectangularQuadratura {


	// sample point abscissas for unit square
	private static final double SP4 = 0.5773502691896257645091488;

	// 4 point quadrature points of a square (-1,1),(-1,-1),(1,-1),(1,1)
	private static final double[][] QUADRILATERAL_RULE_4_POINTS = { { -SP4, -SP4 }, { SP4, -SP4 }, { SP4, SP4 },
			{ -SP4, SP4 } };

	/**
	 * Get the gauss quadratura points on the rectangular tile.
	 * 
	 * @param tile - A rectangular tile object.
	 * @return - An array of GaussPoint objects.
	 */
	public  Vector3D[] getGaussPoints(RectangularTile tile) {
		double[][] gaussPoints = QUADRILATERAL_RULE_4_POINTS;
		Vector3D[] quadraturePoints = new Vector3D[gaussPoints.length];

		// rotate vertices into the xy plane
		Rotation rot = new Rotation(tile.getNormalUnitVector(), Vector3D.PLUS_K);
		Vector3D[] vertices = new Vector3D[tile.getVertices().length];

		for (int i = 0; i < tile.getVertices().length; i++) {
			vertices[i] = rot.applyTo(tile.getVertices()[i]);
		}

		for (int i = 0; i < gaussPoints.length; i++) {
			Vector3D r = new Vector3D(gaussPoints[i][0], gaussPoints[i][1], 0);
			Vector3D p = calculateGaussPoint(vertices, r);
			Vector3D location = rot.applyInverseTo(p);
			quadraturePoints[i] = location;

		}

		return quadraturePoints;
	}


	
	
	/**
	 * Linear mapping from a regular rectangle ( (-1,-1),(1,-1), (1,1),(-1,1) ) to
	 * the tile's 2D coordinates using the method in :
	 * http://math2.uncc.edu/~shaodeng/TEACHING/math5172/Lectures/Lect_15.PDF We use
	 * Vector3D for convenience and speed reasons only.The Z component should be
	 * always zero.
	 * 
	 * @param vertices - Vector3D[] vertices of the tile
	 * @param r        - Vector3D to q point in the regular rectangle
	 * @return Vector3D - point in the tile
	 */

	private Vector3D calculateGaussPoint(Vector3D[] vertices, Vector3D r) {

		double[] n = new double[4];
		n[0] = (1 - r.getX()) * (1 - r.getY()) / 4;
		n[1] = (1 + r.getX()) * (1 - r.getY()) / 4;
		n[2] = (1 + r.getX()) * (1 + r.getY()) / 4;
		n[3] = (1 - r.getX()) * (1 + r.getY()) / 4;

		double x = 0;
		double y = 0;

		for (int i = 0; i < vertices.length; i++) {
			x += vertices[i].getX() * n[i];
			y += vertices[i].getY() * n[i];
		}
		// all vertices must have the same Z coordinates after the rotation, we take one
		// of them to reconstruct the 3D
		// vector
		return new Vector3D(x, y, vertices[0].getZ());

	}

}
