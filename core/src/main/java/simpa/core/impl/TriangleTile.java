package simpa.core.impl;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.SurfacePoint;

public class TriangleTile implements Tile{

	private static final Logger logger = LogManager.getLogger(TriangleTile.class);

	private final Vector3D normalUnitVector;
	private final Vector3D[] vertices;
	private final SurfacePoint[] surfacePoints;
    private int longitudinalIndex; 

	/**
	 * Constructs a rectangular tile based on the vertices of the tile.
	 * 
	 * @param p1 first point
	 * @param p2 second point
	 * @param p3 third point
	 * @param p4 fourth point
	 */
	public TriangleTile(Vector3D p1, Vector3D p2, Vector3D p3, double elevation) {
	
		vertices = new Vector3D[3];
		vertices[0] = p1;
		vertices[1] = p2;
		vertices[2] = p3;
	

		normalUnitVector = Vector3D.crossProduct(p2.subtract(p1), p3.subtract(p2)).normalize();
		Vector3D[] gaussPoints = TriangleQuadratura.getGaussPoints(this);
		surfacePoints=new SurfacePoint[gaussPoints.length];
		
		for (int i = 0; i < gaussPoints.length; i++) {
			Vector3D gp = gaussPoints[i];
			surfacePoints[i] =  SurfacePoint.createSurfacePoint(gp, normalUnitVector.scalarMultiply(elevation));
		}

	}

	
	public SurfacePoint[] getSurfacePoints() {
		return surfacePoints;
	}

	public Vector3D[] getVertices() {
		return vertices;
	}

	public Vector3D getNormalUnitVector() {
		return normalUnitVector;
	}
	
	public int getLongitudinalIndex() {
		return longitudinalIndex;
	}


	public void setLongitudinalIndex(int longitudinalIndex) {
		this.longitudinalIndex = longitudinalIndex;
	}

}
