// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 * <p>
 * Author : Lajos Bojtar
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.sh;

class LegendreP {

    private int maxDegree;
    private double[][] aFactors;
    private double[][] bFactors;
    private double[] sqrt2mp3;
    private double[][] pml;

    public LegendreP(int maxDegree) {
        this.maxDegree = maxDegree;

        aFactors = new double[maxDegree + 1][maxDegree + 2];
        bFactors = new double[maxDegree + 1][maxDegree + 2];
        pml = new double[maxDegree + 2][maxDegree + 2];
        sqrt2mp3 = new double[maxDegree + 2];

        double t1 = 0.25 / Math.PI;
        pml[0][0] = Math.sqrt(t1);

        precalculatePlmFactors();

    }

    /**
     * Gives the maximum degree available for the LegendreP instance.
     *
     * @return
     */
    public int getMaximumDegree() {
        return maxDegree;
    }

    /**
     * Sets Cos[theta] and calculates all Pml values for that Cos[theta]
     *
     * @param cosTheta - Cos[theta]
     */
    public void setCosTheta(double cosTheta, double sinTheta) {


        double omx2Powmo2 = 1;

        for (int m = 0; m <= maxDegree; m++) {

            pml[m][m] = aFactors[m][m] * omx2Powmo2;

            pml[m][m + 1] = cosTheta * sqrt2mp3[m] * pml[m][m];

            for (int ll = m + 2; ll <= maxDegree; ll++) {
                pml[m][ll] = cosTheta * aFactors[m][ll] * pml[m][ll - 1] + bFactors[m][ll] * pml[m][ll - 2];
            }

            // this is just (1-x^2)^(m/2) done a fast way
            if (m == 0)
                omx2Powmo2 = sinTheta;
            else
                omx2Powmo2 *= sinTheta;

        }
    }

    public double[] getPmlArrayForM(int m) {
        return pml[m];
    }

    /**
     * Pre-calculation of aFactors[m][l] and bFactors[m][l], same as in the naive algorithm getRenormalizedLegendre(int
     * l,int m) , but without x involved
     */
    private void precalculatePlmFactors() {

        for (int m = 0; m <= maxDegree; m++) {

            double amm = 1;
            double fact = 1.0;

            if (m > 0) {
                for (int i = 1; i <= m; i++) {
                    amm *= fact / (fact + 1.0);
                    fact += 2.0;
                }
            }

            amm = Math.sqrt((2.0 * m + 1.0) * amm / (4.0 * Math.PI));

            // Condon-Shortley phase
            //  if ((m & 1) == 1)
            //  amm = -amm;

            aFactors[m][m] = amm;
            sqrt2mp3[m] = Math.sqrt(2.0 * m + 3.0);
            aFactors[m][m + 1] = sqrt2mp3[m] * amm;

            // Compute a[m][l] , l > m + 1.
            for (int ll = m + 2; ll <= maxDegree; ll++) {

                fact = Math.sqrt((4.0 * ll * ll - 1.0) / (ll * ll - m * m));
                aFactors[m][ll] = fact;

                double lm12 = (ll - 1.0) * (ll - 1.0);
                fact = fact * Math.sqrt((lm12 - m * m) / (4.0 * lm12 - 1.0));
                bFactors[m][ll] = -fact;

            }

        }

    }
}
