// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 * <p>
 * Author : Lajos Bojtar
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.sh;

class RealYlm {

    private static final double SQRT2 = Math.sqrt(2);
    private static final double SQRT_HALF = Math.sqrt(1 / 2.0);
    private LegendreP legendrePolinomial;
    private int maxDegree;


    // All real spherical harmonics up to order and degree maxDegree. Sin and cos parts interleaved
    private double[][] ylm;

    public RealYlm(int maxDegree) {
        this.maxDegree = maxDegree;
        legendrePolinomial = new LegendreP(maxDegree);
        ylm = new double[maxDegree + 2][2*(maxDegree + 2)]; //(l),(sinus or cosine part for each m)    

    }

    public void calculateAllYlm(double cosTheta, double sinTheta, double cosPhi, double sinPhi) {

        // To avoid the use of trigonometric functions in the loop for the complex exponential,
        // we use the Chebyshev formula see: https://en.wikipedia.org/wiki/List_of_trigonometric_identities
        // With correct initialization of variables we can avoid if statements in the Chebyshev recursion
        double cosPhimm1 = cosPhi;
        double sinPhimm1 = -sinPhi;
        double cosPhimm2 = 2 * cosPhi * cosPhimm1 - 1;
        double sinPhimm2 = 2 * cosPhi * sinPhimm1;
        double cosmPhi = 0.5;
        double sinmPhi = 0.5;

        legendrePolinomial.setCosTheta(cosTheta, sinTheta);

        double factor = SQRT_HALF * SQRT2; //for case m=0

        for (int m = 0; m <= maxDegree; m++) {

            // Chebyshev's formula
            cosmPhi = 2 * cosPhi * cosPhimm1 - cosPhimm2;
            sinmPhi = 2 * cosPhi * sinPhimm1 - sinPhimm2;

            cosPhimm2 = cosPhimm1;
            sinPhimm2 = sinPhimm1;
            cosPhimm1 = cosmPhi;
            sinPhimm1 = sinmPhi;

            double[] lpArray = legendrePolinomial.getPmlArrayForM(m);

            for (int l = m; l <= maxDegree; l++) {
                ylm[l][2*m] = factor * lpArray[l] * cosmPhi;
                ylm[l][2*m+1] = factor * lpArray[l] * sinmPhi;
            } // l loop

            factor = SQRT2;
        } // mm loop

    }

    public double[][] getAllYlm() {
        return ylm;
    }

    /**
     * Gets the real spherical harmonics with the given parameters. This is for debugging only,its slow, it should not be
     * used directly.
     *
     * @param l - degree
     * @param m - order, can be negative
     * @param theta
     * @param phi
     * @return
     */
    public double getYlm(int l, int m, double theta, double phi) {

        double cosTheta = Math.cos(theta);
        double sinTheta = Math.sin(theta);
        double cosPhi = Math.cos(phi);
        double sinPhi = Math.sin(phi);
        this.calculateAllYlm(cosTheta, sinTheta, cosPhi, sinPhi);

        if (m < 0)
            return ylm[l][2*Math.abs(m)+1];
        else
            return ylm[l][2*Math.abs(m)];
    }
}
