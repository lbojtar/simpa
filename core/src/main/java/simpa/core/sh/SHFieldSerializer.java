/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.sh;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CoverType;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import static simpa.core.api.utils.FileUtils.readByteBufferFromFile;
import static simpa.core.api.utils.FileUtils.writeByteBufferToFile;

/**
 * Class with methods for serializing and deserializing SHField objects to and
 * from the disk in a specific format.
 */
public class SHFieldSerializer {
	private static final Logger logger = LogManager.getLogger(SHFieldSerializer.class.getName());

	/**
	 * Serializes an SHField by putting all balls into a ByteBuffer object and saves
	 * it to a given file. The attributes of the SHField are serialized in the
	 * following order: amount of balls in SHField: int ballSearchType: int of
	 * either 1 (LATTICE) or 2 (SINGLE_ROW) for each ball of SHField: - maxDegree:
	 * int - numberOfFunctions: int - numberOfCoefficients: int - radius: double -
	 * centreX: double - centreY: double - centreZ: double - for each shFactor of
	 * SHBall (array converted to 1D) - shFactor: double
	 *
	 * @param shField  - input SHField that should be serialized.
	 * @param filename - file in which to store the ByteBuffer data.
	 */
	public static void serializeSHField(SHField shField, String filename) {
		logger.info("Writing SH Field to binary file.");

		long start = System.currentTimeMillis();

		int ballSearchInt = 0;

		if (shField.getCoverType() == CoverType.LATTICE) {
			ballSearchInt = 1;
		}

		if (shField.getCoverType() == CoverType.SINGLE_ROW) {
			ballSearchInt = 2;
		}

		ByteBuffer bb = ByteBuffer.allocate(getBufferSize(shField));

		bb.putInt(shField.getSHBalls().length);
		bb.putInt(ballSearchInt);

		for (int i = 0; i < shField.getSHBalls().length; i++) {
			SHBall ball = shField.getSHBalls()[i];
			bb.putInt(ball.getMaximumDegree());
			bb.putInt(ball.getNumberOfFunctions());
			bb.putInt(ball.getCoefficients()[0].length);
			bb.putDouble(ball.getRadius());
			bb.putDouble(ball.getCentre().getX());
			bb.putDouble(ball.getCentre().getY());
			bb.putDouble(ball.getCentre().getZ());
			double[] shFactors1D = convertSHFactorsTo1D(ball.getCoefficients());
			for (double v : shFactors1D) {
				bb.putDouble(v);
			}
		}

		writeByteBufferToFile(bb, filename);

		long end = System.currentTimeMillis();
		logger.info("SHField written to: " + filename + ", in " + (end - start) + "ms");
	}

	/**
	 * Deserializes an SHField by getting it from a given file and reconstructing
	 * the data.
	 *
	 * @param filename - file from which to retrieve ByteBuffer data.
	 * @return Field of solid harmonic balls deserialized from the file.
	 */
	public static SHField deserializeSHField(String filename) throws IOException {
		logger.info("Deserializing SH Field from binary file " + filename + ".");

		long start = System.currentTimeMillis();
		ByteBuffer shFieldBytes = readByteBufferFromFile(filename);

		if (shFieldBytes == null)
			throw new IOException("Can not read SHField from file: " + filename);

		// gets the first element of the ByteBuffer which should be the amount of SHBall
		// objects
		int amountOfBalls = shFieldBytes.getInt();
		int ballSearchTypeInt = shFieldBytes.getInt();

		CoverType ballSearchType = null;

		switch (ballSearchTypeInt) {
		case 1 -> ballSearchType = CoverType.LATTICE;
		case 2 -> ballSearchType = CoverType.SINGLE_ROW;
		default -> {
			logger.error("Ballsearchtype " + ballSearchTypeInt + " found in file: " + filename + " doesn't exist");
			logger.debug(
					Arrays.toString(new IllegalArgumentException("Ballsearchtype doesn't exist.").getStackTrace()));
		}
		}

		SHBall[] balls = new SHBall[amountOfBalls];

		for (int i = 0; i < amountOfBalls; i++) {
			int maxDegree = shFieldBytes.getInt();
			int numberOfFunctions = shFieldBytes.getInt();
			int numberOfCoefficients = shFieldBytes.getInt();
			double radius = shFieldBytes.getDouble();
			double centreX = shFieldBytes.getDouble();
			double centreY = shFieldBytes.getDouble();
			double centreZ = shFieldBytes.getDouble();
			double[] shFactors1D = new double[numberOfFunctions * numberOfCoefficients];
			for (int j = 0; j < shFactors1D.length; j++) {
				shFactors1D[j] = shFieldBytes.getDouble();
			}

			double[][] shFactors = convertSHFactorsTo2D(shFactors1D, numberOfCoefficients, numberOfFunctions);
			balls[i] = new SHBall(shFactors, maxDegree, new Vector3D(centreX, centreY, centreZ), radius,i);
			// System.out.println(GraphLayout.parseInstance( balls[i]).toFootprint());

		}

		SHField shField = new SHField(balls, ballSearchType);

		long end = System.currentTimeMillis();
		logger.info("File: " + filename + " read in " + (end - start) + "ms");

		return shField;
	}

	/**
	 * Converts the 2D array of shFactors to a 1D array.
	 *
	 * @param shFactors - 2D array of shFactors
	 * @return - 1D array of shFactors
	 */
	private static double[] convertSHFactorsTo1D(double[][] shFactors) {

		int amountCoefficients = shFactors[0].length;
		int amountFunctions = shFactors.length;
		int items = amountCoefficients * amountFunctions;

		double[] newArr = new double[items];

		for (int fi = 0; fi < amountFunctions; fi++) {
			for (int ci = 0; ci < amountCoefficients; ci++) {
				newArr[amountCoefficients * fi + ci] = shFactors[fi][ci];
			}
		}

		return newArr;
	}

	/**
	 * @param shField An SHField.
	 * @return Bytes needed to serialize the SHField object.
	 */
	private static int getBufferSize(SHField shField) {
		int nb = shField.getSHBalls().length;	
		int n = 2 * 4; // 2 integers in header for the SHField: number of balls, searchType

		n += nb * 3 * 4; // header data in each ball: 3 integer : lmax,nFunctions,nCoeffs
		n += nb * 4 * 8; // header data in each ball: 4 double : radius, cx,cy,cz
		for (int i = 0; i < nb; i++) {
			SHBall b=shField.getSHBalls()[i];
			n +=   b.getCoefficients()[0].length*b.getNumberOfFunctions() * 8;
		}
		return n;
	}

	/**
	 * Converts the 1D array of shFactors to a 2D array using the amount of
	 * Coefficients and functions.
	 *
	 * @param shFactors - 1D array of shFactors
	 * @return - 2D array of shFactors
	 */
	private static double[][] convertSHFactorsTo2D(double[] shFactors, int amountCoefficients, int amountFunctions) {

		double[][] newBallCoefficients = new double[amountFunctions][amountCoefficients];

		for (int fi = 0; fi < amountFunctions; fi++) {
			for (int ci = 0; ci < amountCoefficients; ci++) {
				newBallCoefficients[fi][ci] = shFactors[amountCoefficients * fi + ci];
			}
		}
		return newBallCoefficients;
	}
}
