/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.sh;

import simpa.core.api.CoverType;
import simpa.core.api.CurrentPointSource;
import simpa.core.api.FileNamingConventions;
import simpa.core.api.PointSource;
import simpa.core.api.SphereCovering;
import simpa.core.api.track.NoAperture;
import simpa.core.api.utils.FileUtils;
import simpa.core.cover.SphereDescriptionImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SHFieldFactoryImpl {

	private CoverType coverType;

	public void setCoverType(CoverType coverType) {
		this.coverType = coverType;
	}

	/**
	 * Creates a solid harmonics expansion of the field for a given covering with
	 * FMM.
	 * 
	 * @param sources        PointSources
	 * 
	 * @param covering       The covering of spheres
	 * 
	 * @param lmax           The maximum degree
	 * 
	 * @param binaryFileName Output binary file
	 * 
	 * @param relativeError  Relative limit for spherical harmonics coefficients.
	 *                       Coefficients smaller than the value determined by this
	 *                       limit considered zero and dropped.
	 * @param writeStatFile  If true, then write statistics about the distribution
	 *                       of maximum degrees in the field map.
	 * @param pass1Lmax      Maximum degree for the first pass of the SH expansion.
	 * 
	 * @return Statistics about the distribution of maximum degrees in the field
	 *         map.
	 */
	public int[] createSHField(List<PointSource> sources, SphereCovering covering, int lmax,
			String binaryFileName, double relativeError, boolean writeStatFile,int pass1Lmax) throws IOException {

		this.coverType = covering.getCoverType();

		SHField shf = calculateSHField(sources, SphereDescriptionImpl.toInternal(covering.getCoveringSpheres()), lmax,
				binaryFileName, relativeError, writeStatFile,pass1Lmax);

		shf.serializeToFile(binaryFileName);

		int[] stat = SHUtils.getStatistic(shf);
		if (writeStatFile)
			writeStatistic(shf, FileNamingConventions.getFieldMapStatisticsFileName(binaryFileName));
		return stat;
	}

	/**
	 * Writes statistics of the actual distribution of the maximum degrees of the
	 * balls in the given SHField to the given file.
	 * 
	 * @param shField  The field map.
	 * @param fileName A file name.
	 */
	public void writeStatistic(SHField shField, String fileName) {
		StringBuilder sb = new StringBuilder();
		sb.append("#Lmax NumberOfBallsWithLmax" + System.lineSeparator());
		int[] arr = SHUtils.getStatistic(shField);
		for (int i = 0; i < arr.length; i++) {
			sb.append(i + " " + arr[i]).append(System.lineSeparator());
		}
		FileUtils.writeTextFile(fileName, sb.toString());
	}

	/**
	 * Do the ball creation in two phases, first with lmax/2 and then with full
	 * lmax. It saves a lot of wasted calculation.
	 * 
	 * @param sources       List of point sources
	 * @param sdl           List of sphere descriptions
	 * @param lmax          Maximum degree
	 * @param relativeError Relative error limit
	 * @param pass1Lmax     Maximum degree for the first pass of the SH expansion.
	 * 
	 * @return SHField The field map
	 * @throws IOException If an error occurs during the calculation
	 */
	protected SHField createSHField(List<PointSource> sources, List<SphereDescriptionImpl> sdl, int lmax,
			double relativeError, int pass1Lmax)
			throws IOException {

		// first pass with lmax/4
		SHCoefficientsCalculator cc = new SHCoefficientsCalculator(pass1Lmax, relativeError);
		SHBall[] ballsTmp = cc.createAllBallsWithFMM(sources, sdl);

		// filter balls which are outside the error limit
		List<SphereDescriptionImpl> badList = new ArrayList<>();
		List<SHBall> goodBalls = new ArrayList<>();

		for (SHBall ball : ballsTmp) {
			if (ball.isInLimit() == true) {
				goodBalls.add(ball);
			} else {
				badList.add(sdl.get(ball.getIndexInCover()));
			}
		}
		// second pass with full lmax
		cc = new SHCoefficientsCalculator(lmax, relativeError);
		SHBall[] balls = cc.createAllBallsWithFMM(sources, badList);

		// merge ball arrays in the original order of ball descriptions
		SHBall[] allBalls = new SHBall[goodBalls.size() + balls.length];
		for (SHBall ball : goodBalls) {
			int i = ball.getIndexInCover();
			assert allBalls[i] == null; // check if the index is unique
			allBalls[i] = ball;
		}
		for (SHBall ball : balls) {
			int i = ball.getIndexInCover();
			assert allBalls[i] == null;
			allBalls[ball.getIndexInCover()] = ball;
		}

		return new SHField(allBalls, new NoAperture(), coverType);

	}

	private SHField calculateSHField(List<PointSource> sources, List<SphereDescriptionImpl> sdl, int lmax,
			String binaryFileName, double relativeError, boolean writeStatFile,int pass1Lmax) throws IOException {
		// filter sources
		List<PointSource> currentSources = new ArrayList<>();
		List<PointSource> electricSources = new ArrayList<>();

		for (PointSource ps : sources) {
			if (ps instanceof CurrentPointSource) {
				currentSources.add(ps);
			} else {
				electricSources.add(ps);
			}
		}

		SHField shf;

		// mixed sources
		if (currentSources.size() > 0 && electricSources.size() > 0) {
			SHField shfc = createSHField(currentSources, sdl, lmax, relativeError,pass1Lmax);
			SHField shfe = createSHField(electricSources, sdl, lmax, relativeError,pass1Lmax);
			SHCombinerImpl shc = new SHCombinerImpl();

			Map<SHField, Double> scalings = new HashMap<>();
			scalings.put(shfc, 1.0);
			scalings.put(shfe, 1.0);
			shf = shc.getCombinedSHField(scalings);
		} else {
			shf = createSHField(sources, sdl, lmax, relativeError,pass1Lmax);
		}
		return shf;
	}
}
