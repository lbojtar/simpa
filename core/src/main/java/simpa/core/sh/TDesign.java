package simpa.core.sh;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TDesign {
	
	private static final String resourceFolder = "/t-design/";
	private static final Logger logger = LogManager.getLogger(TDesign.class);
	
	public  List<Vector3D>  getQuadraturaPoints(int degree) throws IOException {
		
		String s = getTDesignFileName(degree);
		List<Vector3D> quadraturaPoints = new ArrayList<>();
		
		try {
			InputStream in = TDesign.class.getResourceAsStream(s);
			assert in != null;
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));

			do {
				s = reader.readLine();

				if (s != null) {
					StringTokenizer st = new StringTokenizer(s, " ");
					double x = Double.parseDouble(st.nextToken());
					double y = Double.parseDouble(st.nextToken());
					double z = Double.parseDouble(st.nextToken());

					quadraturaPoints.add(new Vector3D(x, y, z));
				}

			} while (s != null);

			reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return quadraturaPoints;
	}

	private  String getTDesignFileName(int maxDegree) throws IOException {

		if (maxDegree > 49) {
			String errString = "Degree is too big: " + maxDegree + " The maximum degree is 49.";
			logger.error(errString);
			throw new IllegalArgumentException(errString);

		}

		String prefix = "ss00";

		if (maxDegree > 4)
			prefix = "ss0";

		List<String> files = null;

		try {
			files = getResourceFiles(resourceFolder);
		} catch (IOException e) {
			logger.error("Resource folder" + resourceFolder + " not found.");
			throw e;
		}

		int n = 2 * maxDegree + 1;

		for (String fn : files) {
			if (fn.contains(prefix + n))
				return resourceFolder + fn;
		}

		logger.error("No t-design file found with name: " + prefix + n);
		logger.debug(Arrays.toString(new FileNotFoundException().getStackTrace()));

		return null;
	}

	private  List<String> getResourceFiles(String path) throws IOException {
		ArrayList<String> filenames = new ArrayList<>();

		InputStream in = TDesign.class.getResourceAsStream(resourceFolder + "listOfFiles.txt");
		assert in != null;
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String resource;

		while ((resource = br.readLine()) != null) {
			filenames.add(resource);
		}

		return filenames;
	}
}
