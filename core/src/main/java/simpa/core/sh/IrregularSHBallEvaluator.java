// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.sh;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.special.Gamma;

import simpa.core.api.exceptions.OutOfApertureException;

public class IrregularSHBallEvaluator {

	private static final double SQRT2 = Math.sqrt(2);
	private static final double MIN_COORD_VALUE = 1E-32;
	private SHBall ball;
	private int maxDegree = 0;
	private double[][] result; // contains the result approximated functions and its gradients
	private double[][] dThFactors;
	private double[] dThFactorsM0;
	private LegendreP legendrePolinomial;

	private double[] summ;
	private double[] summDTh;
	private double[] summDPhi;
	private double[] summDr;
	private int nFunctions;

	public IrregularSHBallEvaluator(int lMax, int nFunctions) {
		this.nFunctions = nFunctions;
		init(lMax);
	}

	public void setSHBall(SHBall b) {
		this.ball = b;
		this.maxDegree = b.getMaximumDegree();
	}

	public SHBall getSHBall() {
		return this.ball;
	}

	private void init(int lMax) {

		legendrePolinomial = new LegendreP(lMax + 1); // due to the theta derivative, we have to go one degree

		result = new double[4][4]; // We return always 4 potentials, regardless the type of the field.
		summ = new double[nFunctions];
		summDTh = new double[nFunctions];
		summDPhi = new double[nFunctions];
		summDr = new double[nFunctions];

		// precalculate factors for the theta derivatives
		dThFactors = new double[lMax + 2][lMax + 2];
		dThFactorsM0 = new double[lMax + 2];

		for (int m = 0; m <= lMax + 1; m++) {
			for (int l = m; l <= lMax + 1; l++) {

				// for the faster singular method
				double e1 = Math.sqrt((1 + 2 * l) * Gamma.gamma(1 + l - m) / Gamma.gamma(1 + l + m));
				double e2 = Math.sqrt((3 + 2 * l) * Gamma.gamma(2 + l - m) / Gamma.gamma(2 + l + m));
				dThFactors[l][m] = e1 / e2; // for positive m

			}
		}

		for (int l = 0; l <= lMax; l++) {
			// the minus sign is there only if Plm is calculated without the Condon-Shortley
			// phase
			dThFactorsM0[l] = -Math.sqrt(l * (l + 1.0)); // this is different in W.Bosch's paper!
		}
	}

	/**
	 * Get the value of the vector and scalar potentials and its gradients
	 * approximated by solid harmonics inside a ball with radius and center given in
	 * the SHBall object .
	 *
	 * @param pos - The position inside the ball
	 * 
	 * @return double[][] - The values of the vector and scalar potentials and its
	 *         derivatives approximated by spherical harmonics. Regardless of the
	 *         field type, we always return a double array of dimension
	 *         [potentials=4][pot and derivatives=4]. If the number of potentials is
	 *         less than 4, will fill up the missing potential values and
	 *         derivatives with zeros.
	 */

	public double[][] getPotentials(Vector3D pos) throws OutOfApertureException {

		double oneOverR = 1 / ball.getRadius();
		Vector3D posInUnitBall = pos.subtract(ball.getCentre()).scalarMultiply(oneOverR);

		double cosTheta, sinTheta, cosPhi, sinPhi, dThLeg = 0;

		double x = posInUnitBall.getX();
		double y = posInUnitBall.getY();
		double z = posInUnitBall.getZ();

		// this avoids all singularities
		if (Math.abs(x) < MIN_COORD_VALUE) {
			x = MIN_COORD_VALUE;
		}

		if (Math.abs(y) < MIN_COORD_VALUE) {
			y = MIN_COORD_VALUE;
		}

		if (Math.abs(z) < MIN_COORD_VALUE) {
			z = MIN_COORD_VALUE;
		}

		double r = Math.sqrt(x * x + y * y + z * z); // r in unit ball

		if (r < 1) {
			throw new OutOfApertureException(pos);
		}

		// ISO( physics) conventions for spherical coordinates
		cosTheta = z / r;

		// we have two cases to get good precision
		if (Math.abs(cosTheta) <= 0.5) {
			// sinTheta calculation is unprecise when cosTheta is close to 1
			sinTheta = Math.sqrt(1.0 - cosTheta * cosTheta);
			cosPhi = x / (r * sinTheta);
			sinPhi = y / (r * sinTheta);
		} else {
			double rXY = Math.sqrt(x * x + y * y);// r vector projection to the XY plane
			cosPhi = x / rXY;
			sinPhi = y / rXY;
			sinTheta = x / (cosPhi * r);
		}

		double rPlMEqualL = 1/r;

		// zero the summs
		for (int i = 0; i < nFunctions; i++) {
			summ[i] = 0;
			summDTh[i] = 0;
			summDPhi[i] = 0;
			summDr[i] = 0;
		}

		int idx = 0;

		// To avoid the use of trigonometric functions in the loop for the
		// complex exponential,
		// we use the Chebyshev formula see:
		// https://en.wikipedia.org/wiki/List_of_trigonometric_identities
		// With correct initialization of variables we can avoid if statements
		// in the Chebyshev recursion
		double cosPhimm1 = cosPhi;
		double sinPhimm1 = -sinPhi;
		double cosPhimm2 = 2 * cosPhi * cosPhimm1 - 1;
		double sinPhimm2 = 2 * cosPhi * sinPhimm1;
		double cosmPhi = 0.5;
		double sinmPhi = 0.5;

		legendrePolinomial.setCosTheta(cosTheta, sinTheta);

		// intialize for case m=0
		double factor = 1.0;
		double value, dth, dphi, dr;

		for (int m = 0; m <= maxDegree; m++) {

			// Chebyshev's formula
			cosmPhi = 2 * cosPhi * cosPhimm1 - cosPhimm2;
			sinmPhi = 2 * cosPhi * sinPhimm1 - sinPhimm2;

			// All orthonormalized associated Legendre polynomial without the
			// Condon-Shortley phase for a given m
			double[] lpArray = legendrePolinomial.getPmlArrayForM(m);

			double rPl = rPlMEqualL; 

			for (int l = m; l <= maxDegree; l++) {

				// Irregular solid harmonics values
				value = rPl * factor * lpArray[l];

				if (m == 0) {
					dThLeg = dThFactorsM0[l] * legendrePolinomial.getPmlArrayForM(1)[l];
				} else {
					dThLeg = ((l + 1 - m) * lpArray[l + 1] * dThFactors[l][m] - (l + 1) * lpArray[l] * cosTheta)
							/ sinTheta;
				}

				// Irregular solid harmonics derivatives
				// Theta derivative
				dth = factor * rPl * dThLeg;
				// Phi derivatve
				dphi = rPl * lpArray[l] * m;
				// r derivative
				dr = (-l-1) * value / r;

				// multiply by coefficients for each function
				for (int i = 0; i < nFunctions; i++) {

					double[] c = ball.getCoefficients()[i];
					double coeffs = c[2 * idx] * cosmPhi + c[2 * idx + 1] * sinmPhi;

					summ[i] += value * coeffs;
					summDTh[i] += dth * coeffs;
					summDPhi[i] += dphi * (c[2 * idx + 1] * cosmPhi - c[2 * idx] * sinmPhi);
					summDr[i] += dr * coeffs;

				} // functions loop

				rPl /= r; // faster to calculate this in every loop than store

				idx++;

			} // inner l loop

			factor = SQRT2; // for m>0
			rPlMEqualL /= r;

			// for the chebyshev's formula
			cosPhimm2 = cosPhimm1;
			sinPhimm2 = sinPhimm1;
			cosPhimm1 = cosmPhi;
			sinPhimm1 = sinmPhi;

		} // m loop

		for (int i = 0; i < nFunctions; i++) {

			result[i][0] = summ[i]; // value

			// construct the gradient vector with spherical coordinates
			double rCoord = summDr[i]; // r coordinate of the gradient vector
			double thetaCoord = summDTh[i] / r; // thera coordinate of the gradient vector
			double phiCoord = SQRT2 * summDPhi[i] / (r * sinTheta); // phi coordinate of the gradient vector multiplied

			// convert spherical coordinates into cartesian, ISO( physics) conventions and
			// scale
			result[i][1] = oneOverR * (sinTheta * cosPhi * rCoord + cosTheta * cosPhi * thetaCoord - sinPhi * phiCoord);// x
			result[i][2] = oneOverR * (sinTheta * sinPhi * rCoord + cosTheta * sinPhi * thetaCoord + cosPhi * phiCoord);// y
			result[i][3] = oneOverR * (cosTheta * rCoord - sinTheta * thetaCoord); // z

			// System.out.println(result[i][1]+" "+result[i][2]+" "+result[i][3]);
		}

		// set scalar potential and derivatives to zero
		if (nFunctions == 3) {
			for (int j = 0; j < 4; j++)
				result[3][j] = 0;

		} else if (nFunctions == 1) {
			// put scalar potential and derivatives at the right place
			result[3][0] = result[0][0]; 
			result[3][1] = result[0][1]; 
			result[3][2] = result[0][2]; 
			result[3][3] = result[0][3]; 
			// set vector potential and derivatives to zero
			for (int i = 0; i < 3; i++)
				for (int j = 0; j < 4; j++)
					result[i][j] = 0;
		}

		return result;
	}

}
