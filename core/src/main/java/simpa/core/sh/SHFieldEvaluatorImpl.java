/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.sh;

import java.io.IOException;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.CoverType;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.Aperture;
import simpa.core.api.track.NoAperture;
import simpa.core.cover.BallSearch;
import simpa.core.cover.LatticeSearch;
import simpa.core.cover.SingleRowBallSearch;

public class SHFieldEvaluatorImpl implements PotentialProvider {

	private final SHField shField;
	private SHBallEvaluator evaluator;
	private double[][] result;
	private double scaling;
	private BallSearch ballSearch;
	private Aperture aperture = new NoAperture();
	private Long threadId = null;

	public SHFieldEvaluatorImpl(String serialObjectFilename, double scaling) throws IOException {

		this.shField = SHField.createFromFile(serialObjectFilename);
		this.scaling = scaling;
		init();
	}

	public SHFieldEvaluatorImpl(SHField shf, double scaling) {
		this.shField = shf;
		this.scaling = scaling;
		init();
	}

	private void init() {

		this.result = new double[4][4]; // always 4 potentials returned
		evaluator = new SHBallEvaluator(shField.searchtHighestLMax(), shField.getSHBalls()[0].getNumberOfFunctions());
		// this.setAperture(shf.getAperture());
		if (shField.getCoverType() == CoverType.SINGLE_ROW)
			ballSearch = new SingleRowBallSearch(shField.getSHBalls());

		if (shField.getCoverType() == CoverType.LATTICE) {
			ballSearch = new LatticeSearch(shField.getSHBalls());
		}
	}

	public CoverType getCoverType() {
		return shField.getCoverType();
	}

	/**
	 * Returns the index of the firstly found sphere which contains the point
	 * located at the r vector.
	 * 
	 * @param r - location
	 * @return - index of sphere
	 */
	public int getSphereIndex(Vector3D r) throws OutOfApertureException {
		int i = ballSearch.getSphereIndex(r);
		if (i == -1)
			throw new OutOfApertureException(r);
		return i;
	}

	@Override
	public SHFieldEvaluatorImpl copy() {
		SHFieldEvaluatorImpl e = new SHFieldEvaluatorImpl(this.shField, this.scaling);
		e.setAperture(this.aperture);
		return e;
	}

	@Override
	public double[][] getPotentialsWithDerivatives(Vector3D r) throws OutOfApertureException, KnownFatalException {

		if (!aperture.isInside(r))
			throw new OutOfApertureException(r);

		if (threadId == null)
			threadId = Thread.currentThread().getId();
		else if (threadId != Thread.currentThread().getId())
			throw new KnownFatalException(
					"An instance of a SHFieldEvaluatorImpl was called from two different threads, this should not happen !");

		int idx = getSphereIndex(r);
		SHBall b = shField.getSHBalls()[idx];
		evaluator.setSHBall(b);

		double sf = scaling;

		result = evaluator.getPotentials(r);
		
    //////////////////////////////////////////////////////////////////////////////////////////////////////
	// test discontinuity effect by setting one ball in a drift space to zero
	// index 193 in the single row cover of the simple machine example is in the middle of a drift space
	// THIS MUST BE COMMENTED OUT ALWAYS, I LEFT IT HERE FOR DEMONSTRATING THE EFFECT 
	//	if (idx == 193) 
	//		result = new double[4][4];
    //////////////////////////////////////////////////////////////////////////////////////////////////////
		
		// Scaling
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				result[i][j] = result[i][j] * sf;
			}
		}
		return result;
	}

	@Override
	public void setAperture(Aperture ap) {
		aperture = ap;
	}

	@Override
	public Aperture getAperture() {
		return aperture;
	}
}
