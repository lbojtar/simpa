/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.sh;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.SphericalCoordinates;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.PointSource;
import simpa.core.cover.SphereDescriptionImpl;
import simpa.core.nativ.FMM3DEvaluator;

//See http://people.maths.ox.ac.uk/beentjes/Essays/QuadratureSphere.pdf
public class SHCoefficientsCalculator {

	private static final Logger logger = LogManager.getLogger(SHCoefficientsCalculator.class);

	private List<Vector3D> quadraturaPoints = new ArrayList<>(); // at the unit sphere r=1
	private int maxDegree;
	private double relativePrecision; //
	private double[] maxPotSums;
	private double[][][] ylmStar; // precalculated real spherical harmonics values at the quadratura points
	private boolean lGoodEnougth; // if the lmax is good enough to achieve the relativePrecision
	private int badCount; // number of balls which are not good enough

	/**
	 * This creates a class which calculates the spherical harmonics coefficients on
	 * a sphere to a given maximum degree.
	 * 
	 * 
	 * @param maxDegree         Maximum degree of the spherical harmonic expansion.
	 * @param relativePrecision Relative limit for spherical harmonic coefficients.
	 *                          Coefficients smaller than the value determined by
	 *                          this limit considered zero and dropped. This
	 *                          determines the actual degree of the balls created by
	 *                          the {@link createBall(double[][], Vector3D , double
	 *                          ) createBall} method.
	 */
	public SHCoefficientsCalculator(int maxDegree, double relativePrecision) throws IOException {
		init(maxDegree, relativePrecision);
	}

	/**
	 * Creates a solid harmonics expansion of the field for a given list of
	 * SphereDescriptions with FMM.
	 * 
	 * @param sources PointSources, all has to be the same type.
	 * @param spheres The list of SphereDescriptions.
	 * @return An array of SHBalls
	 */
	public SHBall[] createAllBallsWithFMM(List<PointSource> sources, List<SphereDescriptionImpl> spheres) {
		badCount = 0;
		FMM3DEvaluator fmm = new FMM3DEvaluator();

		// construct the target coordinates data into an 1d array
		List<Vector3D> targets = new ArrayList<>();
		for (SphereDescriptionImpl sphereDescripton : spheres) {
			Vector3D center = sphereDescripton.getCentre();
			for (Vector3D quadraturaPoint : quadraturaPoints) {
				Vector3D qp = center.add(quadraturaPoint.scalarMultiply(sphereDescripton.getRadius()));
				targets.add(qp);
			}
		}

		// 2D array [number of targets ][number of potentials]
		double[][] pots = fmm.getPotentialsAtAllTargets(sources, targets);

		SHBall[] balls = createShBalls(spheres, pots);
		logger.debug("Number of balls out of error limit: " + badCount);

		return balls;
	}

	/**
	 * Create a SHBall at the given centre with a given radius from the given sample
	 * values.
	 * 
	 * @param values       - Sample values of the functions to be approximated
	 *                     double[nQuadraturePoints][nFunctions]
	 * @param centre       - Centre of the ball
	 * @param radius       - Radius of the ball
	 * @param indexInCover - Index of the ball in the cover
	 * @return SHBall
	 */
	protected SHBall createBall(double[][] values, Vector3D centre, double radius, int indexInCover) {

		int nFunctions = values[0].length;

		double[][] coeffs = new double[nFunctions][2 * ((maxDegree + 2) * (maxDegree + 1) / 2)];

		int idx = 0;
		double summCos;
		double summSin;
		double maxCoeff = 0;

		double[] sumsOfPots = new double[nFunctions]; // for error scaling factor calc.

		for (int m = 0; m <= maxDegree; m++) {
			for (int l = m; l <= maxDegree; l++) {

				for (int fi = 0; fi < nFunctions; fi++) {
					summCos = 0;
					summSin = 0;
					double sumOfPot = 0;
					for (int qpi = 0; qpi < quadraturaPoints.size(); qpi++) {
						summCos += values[qpi][fi] * ylmStar[l][2 * m][qpi];
						summSin += values[qpi][fi] * ylmStar[l][2 * m + 1][qpi];
						sumOfPot += Math.abs(values[qpi][fi]);
					}
					sumsOfPots[fi] = sumOfPot;

					coeffs[fi][idx] = summCos;
					coeffs[fi][idx + 1] = summSin;
					if (Math.abs(summCos) > maxCoeff)
						maxCoeff = Math.abs(summCos);
					if (Math.abs(summSin) > maxCoeff)
						maxCoeff = Math.abs(summSin);
				}
				idx += 2;
			}
		}

		int maxLCut = 0;
		for (int fi = 0; fi < nFunctions; fi++) {
			double errScalingFactor = sumsOfPots[fi] / maxPotSums[fi];
			int lcut = findLCut(coeffs[fi], errScalingFactor);
			if (lcut > maxLCut)
				maxLCut = lcut;
		}

		SHBall b = new SHBall(getTrimedCoefficients(coeffs, maxLCut), maxLCut, centre, radius, indexInCover);

		if (!lGoodEnougth)
			badCount++;

		b.setInLimit(lGoodEnougth);

		return b;
	}

	private void init(int maxDegree, double relativePrecision) throws IOException {
		this.relativePrecision = relativePrecision;
		this.maxDegree = maxDegree;
		RealYlm ylm = new RealYlm(maxDegree);

		// Files for quadrature can be downloaded from here:
		// http://web.maths.unsw.edu.au/~rsw/Sphere/EffSphDes/index.html

		quadraturaPoints = (new TDesign()).getQuadraturaPoints(maxDegree);

		ylmStar = new double[maxDegree + 1][2 * (maxDegree + 1)][quadraturaPoints.size()];

		for (int i = 0; i < quadraturaPoints.size(); i++) {

			Vector3D qp = quadraturaPoints.get(i);
			SphericalCoordinates shc = new SphericalCoordinates(qp);

			// The Apache math3 class SphericalCoordinates uses the mathematical convention,
			// not the ISO one
			// used in physics, we exchange phi and theta
			double phi = shc.getTheta();
			double theta = shc.getPhi();
			ylm.calculateAllYlm(Math.cos(theta), Math.sin(theta), Math.cos(phi), Math.sin(phi));

			// precalculate Ylm with weights
			for (int m = 0; m <= maxDegree; m++) {
				for (int l = m; l <= maxDegree; l++) {
					// include cubature weights 4*PI/N
					ylmStar[l][2 * m][i] = ylm.getAllYlm()[l][2 * m] * 4 * Math.PI / quadraturaPoints.size();
					ylmStar[l][2 * m + 1][i] = ylm.getAllYlm()[l][2 * m + 1] * 4 * Math.PI / quadraturaPoints.size();
				}
			}
		}

	}

	/*
	 * Creates SH Balls for a list of spheres using a two-dimensional array with
	 * potentials and a given amount of functions.
	 */
	private SHBall[] createShBalls(List<SphereDescriptionImpl> spheres, double[][] allPots) {
		
		SHBall[] balls = new SHBall[spheres.size()];
		if(spheres.size() == 0)
			return balls;

		int nFunctions = allPots[0].length;		

		int index = 0;
		double[][] samples = new double[quadraturaPoints.size()][nFunctions];

		// get the highest summ for all potentials, we need it later to find the lmax
		maxPotSums = new double[nFunctions];

		int nSamples = quadraturaPoints.size(); // number of potential values of the sphere
		for (int fi = 0; fi < nFunctions; fi++) {
			for (int si = 0; si < spheres.size(); si++) {
				double sum = 0;
				for (int qi = 0; qi < nSamples; qi++) {
					sum += Math.abs(allPots[si * nSamples + qi][fi]);
				}
				if (sum > maxPotSums[fi])
					maxPotSums[fi] = sum;
			}
		}

		for (int si = 0; si < spheres.size(); si++) {
			for (int qi = 0; qi < nSamples; qi++) {
				for (int fi = 0; fi < nFunctions; fi++) {
					// System.arraycopy(allPots[index + qi], 0, samples[qi], 0, nFunctions);
					samples[qi][fi] = allPots[index + qi][fi];
				}
			}
			index += nSamples;
			SphereDescriptionImpl sphere = spheres.get(si);
			SHBall b = createBall(samples, sphere.getCentre(), sphere.getRadius(), sphere.getIndex());
			balls[si] = b;
		}

		return balls;
	}

	/**
	 ** 
	 * @param coeffs           Coefficients for one potential in one ball
	 * @param errScalingFactor potentialSum/higestPotentialSum. We scale the
	 *                         relative error of the coeffients by the relevance of
	 *                         the ball compared to the ball with highest sum of
	 *                         potential values at the sample points.
	 * @return The Lmax to be used for achieve the relativePrecision compared to the
	 *         highest value of the potential in all balls.
	 */
	private int findLCut(double[] coeffs, double errScalingFactor) {

		int idx = 0;

		double[] sumsForEachLmax = new double[maxDegree + 1]; // sums for the coefficients for each l
		for (int m = 0; m <= maxDegree; m++) {
			for (int l = m; l <= maxDegree; l++) {
				sumsForEachLmax[l] += Math.abs(coeffs[idx]) + Math.abs(coeffs[idx + 1]);
				idx += 2;
			}
		}

		double total = 0; // summ of all coefficients
		for (int l = 0; l <= maxDegree; l++)
			total += sumsForEachLmax[l];

		if (total == 0)
			return -1;

		double sum = 0; // summ of coefficients up to l
		for (int l = 0; l < maxDegree; l++) {
			lGoodEnougth = true;
			sum += sumsForEachLmax[l];

			double relErr = Math.abs(errScalingFactor * (sum - total) / total);
			// System.out.println(relErr + " " + l);
			if (relErr > relativePrecision)
				lGoodEnougth = false;
			else
			return l; // we found the lmax which is good enough

		}

		// lmax is not good enough
		lGoodEnougth = false;

		return maxDegree;
	}

	private double[][] getTrimedCoefficients(double[][] coeffs, int trimmedLmax) {

		int nFunt = coeffs.length;
		int n = coeffs[0].length;
		double[][] tmpArr = new double[nFunt][n];

		int originalIdx = 0;
		int trimmedIdx = 0;
		for (int l = 0; l <= maxDegree; l++) {
			for (int m = l; m <= maxDegree; m++) {
				for (int fi = 0; fi < nFunt; fi++) {
					if (l <= trimmedLmax && m <= trimmedLmax) {
						tmpArr[fi][trimmedIdx] = coeffs[fi][originalIdx]; // cos coeff
						tmpArr[fi][trimmedIdx + 1] = coeffs[fi][originalIdx + 1]; // sin coeff
					} else
						break;
				}

				originalIdx += 2;

				if (l <= trimmedLmax && m <= trimmedLmax)
					trimmedIdx += 2;
			}
		}

		double[][] trimmedCoeffs = new double[nFunt][];
		for (int fi = 0; fi < nFunt; fi++)
			trimmedCoeffs[fi] = Arrays.copyOf(tmpArr[fi], trimmedIdx);

		return trimmedCoeffs;
	}

}
