package simpa.core.sh;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CoverType;
import simpa.core.api.track.Aperture;

public class SHCombinerImpl {

	private static final Logger logger = LogManager.getLogger(SHCombinerImpl.class);

	private final HashMap<String, SHField> maps;

	public SHCombinerImpl() {
		maps = new HashMap<>();
	}

	/**
	 * Creates new field map created as a sum of scaled field maps. When this method
	 * is called many times, it will read the field maps from the disk only once.
	 * That means all the field maps ever used kept in memory.
	 * 
	 * @param scalings Field scaling map. The key is the filename and the value is
	 *                 the scaling factor.
	 * @return A new field map created as a sum of scaled field maps.
	 * @throws IOException
	 */
	public SHField getSHField(Map<String, Double> scalings) throws IOException {

		SHBall[] summedBalls = null;
		Aperture aperture = null;
		CoverType ballSearchType = null;
		double scaling = 0;
		for (String fileName : scalings.keySet()) {

			try {
				scaling = scalings.get(fileName);
			} catch (Exception e) {
				throw new IllegalArgumentException("Error in scaling of:" + fileName + " " + e.getMessage());
			}

			SHField shf;
			if (maps.containsKey(fileName)) {
				shf = maps.get(fileName);
			} else {
				shf = SHField.createFromFile(fileName);
				maps.put(fileName, shf);
			}

			// load as needed
			if (shf == null) {
				shf = SHField.createFromFile(fileName);
				maps.put(fileName, shf);
			}

			aperture = shf.getAperture();
			ballSearchType = shf.getCoverType();

			summedBalls = sumBalls(summedBalls, scaling, shf);
		}
		SHField f = new SHField(summedBalls, aperture, ballSearchType);
		// logger.debug("Created SHField with scalings: " + scalings.toString());
		return f;

	}

	/**
	 * Creates new field map created as a sum of scaled field maps. When this method
	 * is called many times, it will read the field maps EACH TIME from the disk.
	 * That means all the field maps NOT kept in memory.
	 * 
	 * @param scalings Field scaling map. The key is the filename and the value is
	 *                 the scaling factor.
	 * @return A new field map created as a sum of scaled field maps.
	 * @throws Exception
	 */
	public SHField getSHFieldWithoutCaching(Map<String, Double> scalings) throws IOException {

		SHBall[] summedBalls = null;
		Aperture aperture = null;
		CoverType ballSearchType = null;

		for (String fileName : scalings.keySet()) {

			double scaling = scalings.get(fileName);

			SHField shf = SHField.createFromFile(fileName);

			aperture = shf.getAperture();
			ballSearchType = shf.getCoverType();

			summedBalls = sumBalls(summedBalls, scaling, shf);
		}
		return new SHField(summedBalls, aperture, ballSearchType);

	}

	/**
	 * Creates new field map created as a sum of scaled field maps. W
	 * 
	 * @param scalings Field scaling map. The key is the SHField object and the
	 *                 value is the scaling factor.
	 * @return A new field map created as a sum of scaled field maps.
	 * @throws Exception
	 */
	public SHField getCombinedSHField(Map<SHField, Double> scalings) throws IOException {

		SHBall[] summedBalls = null;
		Aperture aperture = null;
		CoverType ballSearchType = null;

		for (SHField shf : scalings.keySet()) {
			double scaling = scalings.get(shf);
			aperture = shf.getAperture();
			ballSearchType = shf.getCoverType();
			summedBalls = sumBalls(summedBalls, scaling, shf);
		}

		return new SHField(summedBalls, aperture, ballSearchType);

	}

	/**
	 * Adds the coefficients of the the scaled SHField to coefficients in the SHBall
	 * array. The balls in the the SHFields remains unmodified.
	 * 
	 * @param balls   An array of balls taken always with scaling factor 1. The
	 *                coefficients in the balls will be updated. It can be null
	 *                initially.
	 * @param scaling A scaling factor for the SHField
	 * @param shf     An SHField to be added.
	 * @return An array containing the balls with the summed coefficients.
	 */
	private SHBall[] sumBalls(SHBall[] balls, double scaling, final SHField shf) {
		long t0 = System.currentTimeMillis();

		SHBall[] summedBalls = new SHBall[shf.getSHBalls().length];

		if (balls == null) {
			for (int i = 0; i < shf.getSHBalls().length; i++) {
				SHBall b = shf.getSHBalls()[i];
				SHBall copy = new SHBall(b, scaling, b.getIndexInCover());
				summedBalls[i] = copy;
			}
		} else {
			double scalingB1, scalingB2;
			for (int i = 0; i < balls.length; i++) {

				assert balls.length == shf.getSHBalls().length : "Should have the same number of balls";

				SHBall b1; // with higher or equal Lmax
				SHBall b2;// with lower or equal Lmax

				if (shf.getSHBalls()[i].getMaximumDegree() <= balls[i].getMaximumDegree()) {
					b1 = balls[i];
					b2 = shf.getSHBalls()[i];
					scalingB1 = 1.0;
					scalingB2 = scaling;
				} else {
					b1 = shf.getSHBalls()[i];
					b2 = balls[i];
					scalingB1 = scaling;
					scalingB2 = 1.0;
				}

				summedBalls[i] = sumBallCoefficients(b1, scalingB1, b2, scalingB2);
			}
		}
		long t1 = System.currentTimeMillis();
		logger.debug("Summed balls in " + (t1 - t0) + "ms in thread" + Thread.currentThread().getId());
		return summedBalls;
	}

	/**
	 * Sum the coefficients of two balls.
	 * 
	 * @param b1       The ball with higher or equal Lmax.
	 * @param scaling1 Scaling factor for b1
	 * @param b2       The ball with lower or equal Lmax.
	 * @param scaling2 Scaling factor for b2.
	 * 
	 * @return A new ball with the summed coefficients.
	 * 
	 * @throws IllegalArgumentException
	 */
	private SHBall sumBallCoefficients(final SHBall b1, double scaling1, final SHBall b2, double scaling2)
			throws IllegalArgumentException {
		
		assert b1.getIndexInCover() == b2.getIndexInCover()
				: "Coefficient summing not possible. The balls have not the same index !";

		assert b1.getMaximumDegree() >= b2.getMaximumDegree()
				: "Coefficient summing not possible. The ball to add has higher  Lmax !";

		assert b1.getRadius() == b2.getRadius() : "Coefficient summing is not possible for different radiuses !";

		assert b1.getCentre().getX() == b2.getCentre().getX() || b1.getCentre().getY() == b2.getCentre().getY()
				|| b1.getCentre().getZ() == b2.getCentre().getZ()
				: "Coefficient summing is not possible for different centres !";

		int b1Lmax = b1.getMaximumDegree();
		int b2Lmax = b2.getMaximumDegree();
		double[][] b1Coeffs = b1.getCoefficients();
		double[][] b2Coeffs = b2.getCoefficients();
		double[][] coefs;

		int nf1 = b1.getNumberOfFunctions();
		int nf2 = b2.getNumberOfFunctions();
		if (nf1 == 3 && nf2 == 3) {
			// both balls are static magnetic
			coefs = new double[3][];
			coefs[0] = sumCoefs(b1Coeffs[0], b1Lmax, scaling1, b2Coeffs[0], b2Lmax, scaling2);
			coefs[1] = sumCoefs(b1Coeffs[1], b1Lmax, scaling1, b2Coeffs[1], b2Lmax, scaling2);
			coefs[2] = sumCoefs(b1Coeffs[2], b1Lmax, scaling1, b2Coeffs[2], b2Lmax, scaling2);
		} else if (nf1 == 1 && nf2 == 1) {
			// both balls are static electric
			coefs = new double[1][];
			coefs[0] = sumCoefs(b1Coeffs[0], b1Lmax, scaling1, b2Coeffs[0], b2Lmax, scaling2);
		} else if (nf1 == 3 && nf2 == 1) {
			// first is static magnetic , second is static electric
			coefs = new double[4][];
			coefs[0] = getScaledCopy(b1Coeffs[0], scaling1);
			coefs[1] = getScaledCopy(b1Coeffs[1], scaling1);
			coefs[2] = getScaledCopy(b1Coeffs[2], scaling1);
			// take the magnetic coefficients with zero weight, to keep the order correct
			coefs[3] = sumCoefs(b1Coeffs[2], b1Lmax, 0.0, b2Coeffs[0], b2Lmax, scaling2);
		} else if (nf1 == 1 && nf2 == 3) {
			// The electric ball (first) has higher Lmax
			coefs = new double[4][];
			// take the electric coefficients with zero weight, to keep the order correct
			coefs[0] = sumCoefs(b1Coeffs[0], b1Lmax, 0.0, b2Coeffs[0], b2Lmax, scaling2);
			coefs[1] = sumCoefs(b1Coeffs[0], b1Lmax, 0.0, b2Coeffs[1], b2Lmax, scaling2);
			coefs[2] = sumCoefs(b1Coeffs[0], b1Lmax, 0.0, b2Coeffs[2], b2Lmax, scaling2);
			// put the electric ball coefficients at the correct position for a mixed field
			coefs[3] = getScaledCopy(b1Coeffs[0], scaling1);
		} else if (nf1 == 4 && nf2 == 1) {
			// first ball is mixed, second is electric
			coefs = new double[4][];
			coefs[0] = getScaledCopy(b1Coeffs[0], scaling1);
			coefs[1] = getScaledCopy(b1Coeffs[1], scaling1);
			coefs[2] = getScaledCopy(b1Coeffs[2], scaling1);
			coefs[3] = sumCoefs(b1Coeffs[3], b1Lmax, scaling1, b2Coeffs[0], b2Lmax, scaling2);
		} else if (nf1 == 4 && nf2 == 3) {
			coefs = new double[4][];
			coefs[0] = sumCoefs(b1Coeffs[0], b1Lmax, scaling1, b2Coeffs[0], b2Lmax, scaling2);
			coefs[1] = sumCoefs(b1Coeffs[1], b1Lmax, scaling1, b2Coeffs[1], b2Lmax, scaling2);
			coefs[2] = sumCoefs(b1Coeffs[2], b1Lmax, scaling1, b2Coeffs[2], b2Lmax, scaling2);
			coefs[3] = getScaledCopy(b1Coeffs[3], scaling1);
		} else if (nf1 == 4 && nf2 == 4) {
			// both balls are mixed
			coefs = new double[4][];
			coefs[0] = sumCoefs(b1Coeffs[0], b1Lmax, scaling1, b2Coeffs[0], b2Lmax, scaling2);
			coefs[1] = sumCoefs(b1Coeffs[1], b1Lmax, scaling1, b2Coeffs[1], b2Lmax, scaling2);
			coefs[2] = sumCoefs(b1Coeffs[2], b1Lmax, scaling1, b2Coeffs[2], b2Lmax, scaling2);
			coefs[3] = sumCoefs(b1Coeffs[3], b1Lmax, scaling1, b2Coeffs[3], b2Lmax, scaling2);

		} else if (nf1 == 1 && nf2 == 4) {
			// The first is electric, the second is mixed
			coefs = new double[4][];
			// take the electric coefficients with zero weight, to keep the order correct
			coefs[0] = sumCoefs(b1Coeffs[0], b1Lmax, 0.0, b2Coeffs[0], b2Lmax, scaling2);
			coefs[1] = sumCoefs(b1Coeffs[0], b1Lmax, 0.0, b2Coeffs[1], b2Lmax, scaling2);
			coefs[2] = sumCoefs(b1Coeffs[0], b1Lmax, 0.0, b2Coeffs[2], b2Lmax, scaling2);
			// mix the electric ball coefficients at the correct position for a mixed field
			coefs[3] = sumCoefs(b1Coeffs[0], b1Lmax, scaling1, b2Coeffs[3], b2Lmax, scaling2);
		} else if (nf1 == 3 && nf2 == 4) {
			// first magnetic, second is mixed
			coefs = new double[4][];
			coefs[0] = sumCoefs(b1Coeffs[0], b1Lmax, scaling1, b2Coeffs[0], b2Lmax, scaling2);
			coefs[1] = sumCoefs(b1Coeffs[1], b1Lmax, scaling1, b2Coeffs[1], b2Lmax, scaling2);
			coefs[2] = sumCoefs(b1Coeffs[2], b1Lmax, scaling1, b2Coeffs[2], b2Lmax, scaling2);
			// take the b1 coefs with zero waight to keep the correct order
			coefs[3] = sumCoefs(b1Coeffs[0], b1Lmax, 0, b2Coeffs[3], b2Lmax, scaling2);
		} else {
			throw new IllegalArgumentException("Coefficient summing not possible for this combination of field types");
		}

		return new SHBall(coefs, b1.getMaximumDegree(), b1.getCentre(), b1.getRadius(),b1.getIndexInCover());

	}

	/**
	 * Sums the coefficients for one potential
	 * 
	 * @param cfa1     The coefficient array for the potential which has the higher
	 *                 or equal Lmax
	 * @param lMax1    Lmax for the first array
	 * @param scaling1 Scaling for the first array
	 * @param cfa2     The coefficient array for the potential which has the lower
	 *                 or equal Lmax
	 * @param scaling2 Lmax for the second array
	 * @param lMax2    Scaling for the second array
	 * @return Summed coefficients
	 */
	private double[] sumCoefs(final double[] cfa1, int lMax1, double scaling1, final double[] cfa2, int lMax2,
			double scaling2) {
		double[] result = new double[cfa1.length];
		int idx1 = 0;
		int idx2 = 0;
		for (int l = 0; l <= lMax1; l++) {
			for (int m = l; m <= lMax1; m++) {
				result[idx1] = cfa1[idx1] * scaling1; // cos coeff
				result[idx1 + 1] = cfa1[idx1 + 1] * scaling1; // sin coeff
				if (l <= lMax2 && m <= lMax2) {
					result[idx1] += cfa2[idx2] * scaling2; // cos coeff
					result[idx1 + 1] += cfa2[idx2 + 1] * scaling2; // sin coeff
					idx2 += 2;
				}
				idx1 += 2;
			}
		}

		return result;
	}

	private double[] getScaledCopy(double[] d, double s) {
		double[] res = new double[d.length];
		for (int i = 0; i < d.length; i++)
			res[i] = d[i] * s;
		return res;
	}
}
