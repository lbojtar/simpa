/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.sh;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CoverType;
import simpa.core.api.track.Aperture;
import simpa.core.api.track.NoAperture;

import java.io.IOException;

/**
 * Class that contains all the spherical harmonic balls @see{@link SHBall}, the
 * volume of interest @see{@link Aperture}, the ball search type and methods for
 * serializing and deserializing the object to and from the disk.
 *
 */
public class SHField {

	private static final Logger logger = LogManager.getLogger(SHField.class);

	private SHBall[] balls;
	private Aperture aperture;
	private CoverType coverType;

	/**
	 * Constructor without arguments for deserialization
	 */
	public SHField() {

	}

	/**
	 * This class holds all information about the spherical harmonics expansion of
	 * the global field. It is for serialization and deserialization of the
	 * SHCoefficients array.
	 * 
	 * @param balls list of SHBalls
	 */
	public SHField(SHBall[] balls, CoverType coverType) {
		this.balls = balls;
		this.setAperture(new NoAperture());
		this.coverType = coverType;
	}

	public SHField(SHBall[] balls, Aperture aperture, CoverType coverType) {
		this.balls = balls;
		this.setAperture(aperture);
		this.coverType = coverType;

	}

	public SHBall[] getSHBalls() {
		return balls;
	}

	public int searchtHighestLMax() {
		int max = 0;
		for (SHBall b : balls)
			if (b.getMaximumDegree() > max)
				max = b.getMaximumDegree();
		return max;
	}

	public synchronized static SHField createFromFile(String fileName) throws IOException {

		logger.info("Reading fieldmap from file: " + fileName);

		SHField shgf = deserializeSHField(fileName);

		logger.info("Deserialization of fieldmap from file: " + fileName + " is done.");

		return shgf;

	}

	public void serializeToFile(String fileName) {
		SHFieldSerializer.serializeSHField(this, fileName);
	}

	public static SHField deserializeSHField(String filename) throws IOException {
		return SHFieldSerializer.deserializeSHField(filename);
	}

	public Aperture getAperture() {
		return aperture;
	}

	public void setAperture(Aperture aperture) {
		this.aperture = aperture;
	}

	public void setCoverType(CoverType ballSearchType) {
		this.coverType = ballSearchType;
	}

	public CoverType getCoverType() {
		return coverType;
	}

}
