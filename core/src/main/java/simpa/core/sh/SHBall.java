/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.sh;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

/**
 * Class that represents the EM field by spherical harmonics inside a ball.
 */
public class SHBall {

	private static final Logger logger = LogManager.getLogger(SHBall.class);

	//serialized variables
	private final int maxDegree;
	private final double radius;
	private final Vector3D centre;
	private final double[][] shFactors; // factors the [number of functions][depends on maxDegree] // sin and cos parts
	// are interleaved

	//not serialized variables, used internally
	private final int indexInCover;
	private boolean inLimit=false;
	
	/**
	 * Sets the inLimit flag. This flag is used to indicate if the ball is inside the
	 * error limit.
	 * @param inLimit - true if the ball is inside the error limit.
	 */
	public void setInLimit(boolean inLimit) {
		this.inLimit = inLimit;
	}

	/**
	 * Gets the inLimit flag. This flag is used to indicate if the ball is inside the
	 * error limit.
	 * @return - true if the ball is inside the error limit.
	 */
	public boolean isInLimit() {
		return inLimit;
	}

	/**
	 * Constructor for spherical harmonics coeffiecient. This class stores the all
	 * information needed to calculate an approximated value by solid harmonics
	 * inside the ball at a given centre and given radius.
	 * 
	 * @param factors      - A 2D array of spherical harmonics factors. The first
	 *                     dimension depends on the field type. For a pure static
	 *                     electric field there is one function which is the
	 *                     electro-static potential. For a pure static magnetic
	 *                     field
	 *                     there ate 3 vector potential. For a static field with
	 *                     both
	 *                     electric and magnetic field ther are 4 potentials.
	 * 
	 * @param maxDegree    - Maximum degree of the spherical charmonics expansion.
	 * @param centre       - Centre of the ball
	 * @param r            - Radius of the ball
	 * @param indexInCover - Index of the ball in the cover
	 */
	public SHBall(double[][] factors, int maxDegree, Vector3D centre, double r, int indexInCover) {
		this.maxDegree = maxDegree;
		shFactors = factors;
		this.radius = r;
		this.centre = centre;
		this.indexInCover = indexInCover;
	}

	/**
	 * Creates a copy of the ball with scaled coefficients.
	 * 
	 * @param b       A ball to copy.
	 * @param scaling A scaling factor.
	 * @param indexInCover - Index of the ball in the cover
	 */
	public SHBall(SHBall b, double scaling, int indexInCover) {

		this.maxDegree = b.getMaximumDegree();

		double[][] coeffs = new double[b.getNumberOfFunctions()][b.shFactors[0].length];

		for (int fi = 0; fi < b.getNumberOfFunctions(); fi++) {

			for (int i = 0; i < coeffs[0].length; i++) {
				coeffs[fi][i] = b.shFactors[fi][i] * scaling;
			}
		}

		shFactors = coeffs;
		this.radius = b.getRadius();
		this.centre = b.getCentre().scalarMultiply(1.0);// its a copy
		this.indexInCover = indexInCover;
	}

	public SHBall(int maxDegree, int nFunctions, double radius, Vector3D centre, int indexInCover) {
		this.maxDegree = maxDegree;
		shFactors = new double[nFunctions][2 * (getIndex(maxDegree, maxDegree) + 1)];
		this.radius = radius;
		this.centre = centre;
		this.indexInCover = indexInCover;
	}

	/**	
	 * @return - Index of the ball in the cover
	 */
	public int getIndexInCover() {
		return indexInCover;
	}

	/**
	 * Gets the spherical harmonics coefficients.
	 * 
	 * @return 2D array with the coefficients. The first dimension is the number of
	 *         potentials, which depends on the field type.
	 */
	public double[][] getCoefficients() {
		return shFactors;
	}

	public Vector3D getCentre() {
		return centre;
	}

	public int getMaximumDegree() {
		return maxDegree;
	}

	public double getRadius() {
		return radius;
	}

	/**
	 * * Gets the index for a given l and m .Its slow, should not be used in loops.
	 * 
	 * @param lt -l
	 * @param mt -m
	 * @return - Array index which belongs to lt, mt
	 */
	public int getIndex(int lt, int mt) {
		int idx = 0;
		for (int m = 0; m <= maxDegree; m++) {
			for (int l = m; l <= maxDegree; l++) {

				if (l == lt && m == mt) {
					return idx;
				}

				idx++;
			}
		}
		return -1;
	}

	public int getNumberOfFunctions() {
		return shFactors.length;
	}

	public void addBallCoefficients(SHBall ballToAdd, double scaling) throws IllegalArgumentException {

		int nFunt = this.getNumberOfFunctions();
		if (nFunt < ballToAdd.getNumberOfFunctions()) {
			IllegalArgumentException e = new IllegalArgumentException(
					"Coefficient summing not possible. The ball to add has higher number of function than this ball !");
			logger.error("Number of functions in this ball : " + this.getNumberOfFunctions() + " < ball to add : "
					+ ballToAdd.getNumberOfFunctions());
			logger.debug(Arrays.toString(e.getStackTrace()));
			throw e;
		}

		if (this.maxDegree < ballToAdd.getMaximumDegree()) {
			IllegalArgumentException e = new IllegalArgumentException(
					"Coefficient summing not possible. The ball to add has higher  Lmax !");
			logger.error("this ball degree: " + this.maxDegree + " < ball to add degree: " + ballToAdd.maxDegree);
			logger.debug(Arrays.toString(e.getStackTrace()));
			throw e;
		}

		if (this.radius != ballToAdd.getRadius()) {
			IllegalArgumentException e = new IllegalArgumentException(
					"Coefficient summing is not possible for different radiuses !");
			logger.error("Radius: " + this.radius + " != " + ballToAdd.getRadius());
			logger.debug(Arrays.toString(e.getStackTrace()));
			throw e;
		}

		if (this.centre.getX() != ballToAdd.getCentre().getX() || this.centre.getY() != ballToAdd.getCentre().getY()
				|| this.centre.getZ() != ballToAdd.getCentre().getZ()) {
			IllegalArgumentException e = new IllegalArgumentException(
					"Coefficient summing is not possible for different centres !");
			logger.error("Centre: " + this.centre + " != " + ballToAdd.getCentre());
			logger.debug(Arrays.toString(e.getStackTrace()));
			throw e;
		}

		double[][] coeffsToAdd = ballToAdd.getCoefficients();
		int thisBallIdx = 0;
		int ballToAddIdx = 0;
		int ballToAddLmax = ballToAdd.getMaximumDegree();

		for (int l = 0; l <= maxDegree; l++) {
			for (int m = l; m <= maxDegree; m++) {
				for (int fi = 0; fi < nFunt; fi++) {
					if (l <= ballToAddLmax && m <= ballToAddLmax) {
						shFactors[fi][thisBallIdx] += coeffsToAdd[fi][ballToAddIdx] * scaling; // cos coeff
						shFactors[fi][thisBallIdx + 1] += coeffsToAdd[fi][ballToAddIdx + 1] * scaling; // sin coeff
					} else
						break;
				}

				thisBallIdx += 2;

				if (l <= ballToAddLmax && m <= ballToAddLmax)
					ballToAddIdx += 2;
			}
		}
	}

	/**
	 * Scale the coefficients
	 * 
	 * @param scaling scaling factor
	 */
	protected void scaleCoefficients(double scaling) {
		for (int fi = 0; fi < getNumberOfFunctions(); fi++) {

			for (int i = 0; i < shFactors[0].length; i++) {
				shFactors[fi][i] = shFactors[fi][i] * scaling;
			}
		}
	}

}
