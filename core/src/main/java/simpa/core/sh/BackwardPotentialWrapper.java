package simpa.core.sh;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.Aperture;

/**
 * A Wrapper class for backward tracking.Inverts only the vector potential but
 * not the scalar potential.
 * 
 * @author lbojtar
 *
 */
public class BackwardPotentialWrapper implements PotentialProvider {

	private PotentialProvider potentialProvider;

	public BackwardPotentialWrapper(PotentialProvider pp) {
		potentialProvider = pp;
	}

	@Override
	public double[][] getPotentialsWithDerivatives(Vector3D r) throws OutOfApertureException, KnownFatalException {
		double[][] result = potentialProvider.getPotentialsWithDerivatives(r);
		
		if (result.length == 1)
			return result; //nothing to do for scalar potential only
		
		// Scaling vector potential by -1
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 4; j++) {
				result[i][j] = -result[i][j] ;
			}
		}
		return result;
	}

	@Override
	public void setAperture(Aperture ap) {
		potentialProvider.setAperture(ap);

	}

	@Override
	public Aperture getAperture() {	
		return potentialProvider.getAperture();
	}

	@Override
	public PotentialProvider copy() {		
		return potentialProvider.copy();
	}

}
