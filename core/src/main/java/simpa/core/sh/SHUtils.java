/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.sh;

import simpa.core.api.utils.FileUtils;

import java.util.List;

/**
 * Contains utilities related to spherical harmonics.
 */
public class SHUtils {

	

	/**
	 * Get statistics of the actual distribution of the maximum degrees of the balls
	 * in the given SHField.
	 * 
	 * @param shf An SHField.
	 * 
	 * @return An array containing the number of balls for each lmax.
	 */
	public static int[] getStatistic(SHField shf) {

		// search the ball with the highest degree
		int maxDegree = 0;
		for (int i = 0; i < shf.getSHBalls().length; i++) {
			int d = shf.getSHBalls()[i].getMaximumDegree();
			if (d > maxDegree)
				maxDegree = d;
		}

		// count balls for each Lmax
		int[] stat = new int[maxDegree + 1];
		for (int i = 0; i < shf.getSHBalls().length; i++) {
			int d = shf.getSHBalls()[i].getMaximumDegree();
			stat[d]++;
		}
		return stat;
	}

	/**
	 * Writes the relative coefficients of the given SHBall to a file.
	 *
	 * @param b        solid harmonic ball
	 * @param fileName output file name
	 */
	public static void relativeCoeffsToFile(SHBall b, String fileName) {

		double max = findBiggestCoeff(b);

		double[][] coeffs = b.getCoefficients();// [number of functions][depends on maxDegree][2]
		StringBuilder sb = new StringBuilder();
		sb.append("# Spherical harmonics coefficients for  a ball with r= ").append(b.getRadius())
				.append(System.lineSeparator());
		sb.append("# center: ").append(b.getCentre().toString()).append(System.lineSeparator());
		sb.append("# l \t m \t sin_part_pot_1 \t cos_part_pot 1 \t sin_part_pot_2 \t cos_part_pot_2 ...  ")
				.append(System.lineSeparator()).append(System.lineSeparator());
		int idx = 0;

		for (int m = 0; m <= b.getMaximumDegree(); m++) {
			for (int l = m; l <= b.getMaximumDegree(); l++) {
				sb.append(m).append(" ").append(l).append(" ");
				for (double[] coeff : coeffs) {
					sb.append(Math.abs(coeff[2 * idx]) / max).append(" ").append(Math.abs(coeff[2 * idx + 1]) / max)
							.append(" ");
				}
				sb.append(System.lineSeparator());
				idx++;
			}
		}

		// check with gluplot command, for example:
		// splot "BallCoeffs40.txt" u 1:2:(log($3))
		FileUtils.writeTextFile(fileName, sb.toString());
	}

	/**
	 * Finds the biggest coefficient in a solid harmonic ball.
	 *
	 * @param b solid harmonic ball.
	 * @return the value of the biggest coefficient.
	 */
	private static double findBiggestCoeff(SHBall b) {
		double[][] coeffs = b.getCoefficients();
		double max = 0;
		for (double[] da : coeffs)
			for (double d : da)
				if (Math.abs(d) > max)
					max = Math.abs(d);
		return max;

	}
}
