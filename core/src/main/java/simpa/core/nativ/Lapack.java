package simpa.core.nativ;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.ptr.ByReference;

public interface Lapack extends Library{

	 Lapack INSTANCE = Native.load(Platform.isWindows() ? "liblapack" : "lapack", Lapack.class);
	
	 void dgetrf_(ByReference rows, ByReference columns, double[] mdata, ByReference n,  int[] pivotIndices, ByReference info );
}	
