/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.nativ;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;
import com.sun.jna.ptr.ByReference;

/**
 * Interface for communicating with the native library fmm3d.
 */
public interface FMM3D extends Library {

	FMM3D INSTANCE = Native.load(Platform.isWindows() ? "libfmm3d" : "fmm3d", FMM3D.class);

    // gets npots Laplace potentials at the targets
	void lfmm3d_t_c_p_vec_(ByReference npots, ByReference precision, ByReference nsource, double[] sources, double[] charges,
                           ByReference ntarget, double[] targets, double[] pots, ByReference errorCode);
	
	// gets the Laplace potentials and  gradients at the targets for npots potentials
	void lfmm3d_t_c_g_vec_(ByReference npots, ByReference precision, ByReference nsource, double[] sources, double[] charges,
                           ByReference ntarget, double[] targets, double[] pots, double[] gradients, ByReference errorCode);
	
	// gets npots Helmholtz potentials at the targets
	void hfmm3d_t_c_p_vec_(ByReference npots, ByReference precision, double[] zk, ByReference nsource, double[] sources, double[] charges,
                           ByReference ntarget, double[] targets, double[] pots, ByReference errorCode);
	
	// gets the Helmholtz potentials and gradients at the targets for npots potentials
	void lfmm3d_t_c_g_vec_(ByReference npots, ByReference precision, double[] zk, ByReference nsource, double[] sources, double[] charges,
                           ByReference ntarget, double[] targets, double[] pots, double[] gradients, ByReference errorCode);
	
}