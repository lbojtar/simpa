/**
* Copyright (c) 2022 Lajos Bojtár & Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.nativ;

import com.sun.jna.ptr.DoubleByReference;
import com.sun.jna.ptr.IntByReference;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.CurrentPointSource;
import simpa.core.api.ElectricMonopole;
import simpa.core.api.PointSource;
import simpa.core.api.SystemConstants;
import simpa.core.b2a.SurfacePointImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Class that communicates with the @see{@link FMM3D} class.
 */
public class FMM3DEvaluator {

	private static final Logger logger = LogManager.getLogger(FMM3DEvaluator.class);
	private static final int NUMBER_OF_GRADIENTS = 3;

	private int nPotentials;

	/**
	 * Calculates the potentials only for a given set of source and destination
	 * points. Used by the coefficient calculator.
	 * 
	 * @param sources The sources. All must be the same type.
	 * @param targets The target points where the potentials are needed.
	 * 
	 * @return 2D array [number of targets ][number of potentials]
	 */
	public double[][] getPotentialsAtAllTargets(List<PointSource> sources, List<Vector3D> targets) {

		nPotentials = getNumberOfPotentials(sources);

		double[] sa = new double[3 * sources.size()]; // x,y,z coords.
		double[] charges = new double[nPotentials * sources.size()];
		double[] ta = new double[3 * targets.size()]; // x,y,z coords.
		double[] pots = new double[nPotentials * targets.size()];

		createSourceArrangementAndCharges(sources, targets, sa, charges, ta);

		long t0 = System.currentTimeMillis();
		IntByReference npots = new IntByReference(nPotentials);
		DoubleByReference err = new DoubleByReference(SystemConstants.FMM3D__RELATIVE_PRECISION);
		IntByReference nsources = new IntByReference(sources.size());
		IntByReference ntargets = new IntByReference(targets.size());
		IntByReference errorCode = new IntByReference();

		logger.info("Calculating potentials for SH expansion..");
		FMM3D.INSTANCE.lfmm3d_t_c_p_vec_(npots, err, nsources, sa, charges, ntargets, ta, pots, errorCode);
		logger.info("Total time= " + ((System.currentTimeMillis() - t0) / 1000) + "  sec");
		handleFMM3DError(errorCode);

		double[][] potsForAlltargets = new double[targets.size()][nPotentials];

		if (nPotentials == 3) {
			for (int i = 0; i < targets.size(); i++) {
				potsForAlltargets[i][0] = pots[i * nPotentials];
				potsForAlltargets[i][1] = pots[i * nPotentials + 1];
				potsForAlltargets[i][2] = pots[i * nPotentials + 2];
			}
		} else if (nPotentials == 1) {
			for (int i = 0; i < targets.size(); i++) {
				potsForAlltargets[i][0] = pots[i];
			}
		}

		return potsForAlltargets;
	}

	/**
	 * Gets the potentials and gradients at all surface points. The sources at the
	 * surface points must be the same type.
	 *
	 * @param surfacePoints surfacepoints to calculate.
	 * @return 3D array : [number of points][number of potentials][always 4]
	 */
	public double[][][] getPotentialsAndGradientsAtAllTargets(List<SurfacePointImpl> surfacePoints) {
		List<PointSource> sources = new ArrayList<>();

		List<Vector3D> targets = new ArrayList<>();

		surfacePoints.forEach(item -> {
			sources.add(item.getSource());
			targets.add(item.getLocation());
		});

		return getPotentialsAndGradientsAtAllTargets(sources, targets);
	}

	/**
	 * Calculates the potentials and gradient for a given set of source and
	 * destination points.
	 *
	 * @param sources - The Sources of the field. All must be the SAME TYPE !
	 * @param targets - Target points.
	 * @return 3D array : [number of points][number of potentials][always 4]
	 */
	public double[][][] getPotentialsAndGradientsAtAllTargets(List<? extends PointSource> sources, List<Vector3D> targets) {

		nPotentials = getNumberOfPotentials(sources);

		double[] sa = new double[3 * sources.size()]; // x,y,z coords.
		double[] charges = new double[nPotentials * sources.size()];
		double[] ta = new double[3 * targets.size()]; // x,y,z coords.
		double[] pots = new double[nPotentials * targets.size()];
		double[] gradients = new double[3 * nPotentials * targets.size()];

		createSourceArrangementAndCharges(sources, targets, sa, charges, ta);

		long t0 = System.currentTimeMillis();
		IntByReference npots = new IntByReference(nPotentials);
		DoubleByReference err = new DoubleByReference(1e-13);
		IntByReference nsources = new IntByReference(sources.size());
		IntByReference ntargets = new IntByReference(targets.size());
		IntByReference errorCode = new IntByReference();

		logger.info("Calculating potentials and gradients");
		FMM3D.INSTANCE.lfmm3d_t_c_g_vec_(npots, err, nsources, sa, charges, ntargets, ta, pots, gradients, errorCode);
		logger.info("Done in " + ((System.currentTimeMillis() - t0) / 1000) + "  sec");
		handleFMM3DError(errorCode);

		if (nPotentials == 3) {
			return getStaticMagneticGradients(targets, pots, gradients);
		} else if (nPotentials == 1)
			return getStaticElectricGradients(targets, pots, gradients);
		else
			throw new IllegalArgumentException("Illegal number of potentials !");
	}

	
	private void handleFMM3DError(IntByReference errorCode) {
		if (errorCode.getValue() != 0) {
			String errorMessage = "Internal error happened.";
			if (errorCode.getValue() == 4 || errorCode.getValue() == 8) {
				errorMessage = "Insufficient memory.";
			}
			logger.error("Error " + errorCode.getValue() + " in FMM3D call: " + errorMessage);
			logger.debug(Arrays.toString(new RuntimeException(
					"Error " + errorCode.getValue() + " occurred while calling FMM3D library: " + errorMessage)
					.getStackTrace()));
		}
	}

	/**
	 * Creates the three-dimensional array with gradients for vector potentials in
	 * the static magnetic case
	 *
	 * @param targets   locations of the targets
	 * @param pots      vector potentials returned from FMM3D
	 * @param gradients the gradients returned from FMM3D
	 * @return three-dimensional array with gradients for vector potentials in the
	 *         static magnetic case
	 */
	private double[][][] getStaticMagneticGradients(List<Vector3D> targets, double[] pots, double[] gradients) {
		double[][][] gradientsForAllTargets = new double[targets.size()][4][4];
		for (int i = 0; i < targets.size(); i++) {
			for (int j = 0; j < 3; j++) {
				gradientsForAllTargets[i][j][0] = pots[(i * nPotentials) + j]; // pot
				gradientsForAllTargets[i][j][1] = gradients[(nPotentials * NUMBER_OF_GRADIENTS * i) + j]; // gradient x
				gradientsForAllTargets[i][j][2] = gradients[(nPotentials * NUMBER_OF_GRADIENTS * i) + j
						+ NUMBER_OF_GRADIENTS];// gradient
				// y
				gradientsForAllTargets[i][j][3] = gradients[(nPotentials * NUMBER_OF_GRADIENTS * i) + j
						+ 2 * NUMBER_OF_GRADIENTS];// gradient
				// z
			}
		}
		return gradientsForAllTargets;
	}

	/**
	 * Creates the three-dimensional array with gradients for scalar potentials in
	 * the static electric case
	 *
	 * @param targets   locations of the targets
	 * @param pots      scalar potentials returned from FMM3D
	 * @param gradients the gradients returned from FMM3D
	 * @return three-dimensional array with gradients for scalar potentials in the
	 *         static electric case
	 */
	private double[][][] getStaticElectricGradients(List<Vector3D> targets, double[] pots, double[] gradients) {
		double[][][] gradientsForAllTargets = new double[targets.size()][4][4];
		for (int i = 0; i < targets.size(); i++) {
			gradientsForAllTargets[i][3][0] = pots[i]; // pot
			gradientsForAllTargets[i][3][1] = gradients[(NUMBER_OF_GRADIENTS * i)]; // gradient x
			gradientsForAllTargets[i][3][2] = gradients[(NUMBER_OF_GRADIENTS * i) + 1];// gradient y
			gradientsForAllTargets[i][3][3] = gradients[(NUMBER_OF_GRADIENTS * i) + 2];// gradient z
		}
		return gradientsForAllTargets;
	}

	/*
	 * Sets up the source arrangement and charges for a list of source and target
	 * points according to the nPotentials.
	 */
	private void createSourceArrangementAndCharges(List<? extends PointSource> sources, List<Vector3D> targets, double[] sa,
			double[] charges, double[] ta) {
		for (int i = 0; i < sources.size(); i++) {
			PointSource ps = sources.get(i);
			Vector3D p = ps.getLocation();
			sa[i * 3] = p.getX();
			sa[i * 3 + 1] = p.getY();
			sa[i * 3 + 2] = p.getZ();
			if (nPotentials == 3) {
				CurrentPointSource cps = (CurrentPointSource) ps;
				Vector3D c = cps.getCurrent();
				charges[i * 3] = c.getX();
				charges[i * 3 + 1] = c.getY();
				charges[i * 3 + 2] = c.getZ();
			} else if (nPotentials == 1) {

				ElectricMonopole em = (ElectricMonopole) ps;
				charges[i] = em.getCharge();
			} else
				throw new IllegalArgumentException("Illegal number of potentials !");
		}

		for (int i = 0; i < targets.size(); i++) {
			Vector3D p = targets.get(i);
			ta[i * 3] = p.getX();
			ta[i * 3 + 1] = p.getY();
			ta[i * 3 + 2] = p.getZ();
		}
	}

	private int getNumberOfPotentials(List<? extends PointSource> sources) {
		if (sources.get(0) instanceof ElectricMonopole) {
			for (PointSource ps : sources) {
				if (!(ps instanceof ElectricMonopole))
					throw new IllegalArgumentException("All sources must be the same type");

			}
			return 1;
		}

		if (sources.get(0) instanceof CurrentPointSource) {
			for (PointSource ps : sources) {
				if (!(ps instanceof CurrentPointSource))
					throw new IllegalArgumentException("All sources must be the same type");

			}
			return 3;
		}

		throw new IllegalArgumentException("Unknown source type");

	}
}
