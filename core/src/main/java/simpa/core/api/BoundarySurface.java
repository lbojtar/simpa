/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.exceptions.PositionMismatchException;
import simpa.core.api.utils.FileUtils;

/**
 * Describes the surface that is bounding the volume of interest where the field
 * should be reproduced by the sources. This class holds a list of 
 * {@link SurfacePoint} and provides reading and writing them to a file.
 * 
 */
public class BoundarySurface {
	private static final Logger logger = LogManager.getLogger(BoundarySurface.class);

	private static double RELATIVE_POS_TOLERANCE = 1E-5; // for position comparison
	private final ArrayList<SurfacePoint> surfacePoints;

	/**
	 * Creates a BoundarySurface object.
	 *
	 * @param surfacePoints the surface points that make up the boundary surface of
	 *                      the volume.
	 */
	public BoundarySurface(ArrayList<SurfacePoint> surfacePoints) {
		this.surfacePoints = surfacePoints;
	}

	/**
	 * Writes the surface points to a text file.
	 *
	 * @param name name for the output file.
	 * @param unit LengthUnit for the output.
	 * @param off  output file format, will add a header if OPERA3D is chosen.
	 */
	public void surfacePointCoordinatesToFile(String name, LengthUnit unit, OutputFileFormat off) {

		StringBuffer sb = surfacePointsToString(unit);

		if (off == OutputFileFormat.OPERA3D) {
			sb.insert(0, OutputFileFormat.getOpera3DHeader(surfacePoints.size(), unit));
			FileUtils.writeTextFile(name + FileNamingConventions.getPointsFileExtension(off), sb.toString());
		}
		if (off == OutputFileFormat.PLAINTEXT) {
			// sb.insert(0,"#X Y Z"+System.lineSeparator());
			FileUtils.writeTextFile(name + FileNamingConventions.getPointsFileExtension(off), sb.toString());
		}
	}

	// TODO Check for NaN
	/**
	 * Gets an electromagnetic field from a txt file
	 * 
	 * @param filename           - File contining the positions and the fields.
	 * @param fieldScalingFactor - To scale the field
	 * @param unit               - The length unit in which the positions are given
	 * @throws PositionMismatchException when a point is not at the expected place.
	 */
	public void setFieldFromFile(String filename, double fieldScalingFactor, LengthUnit unit)
			throws PositionMismatchException {
		File f = new File(filename);
		logger.info("Reading " + f);

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
			try {
				for (int j = 0; j < surfacePoints.size(); j++) {

					double[] columns = FileUtils.getNextValidLine(br, 6);

					if (columns == null)
						throw new IOException("The file " + f + " has less columns than expected !");

					double lf = unit.getFactor();
					double x = columns[0];
					double y = columns[1];
					double z = columns[2];
					double fx = columns[3] * fieldScalingFactor;
					double fy = columns[4] * fieldScalingFactor;
					double fz = columns[5] * fieldScalingFactor;

					Vector3D r = surfacePoints.get(j).getLocation();

					double errx = Math.abs(r.getX() * lf - x) / (r.getX() * lf);
					double erry = Math.abs(r.getY() * lf - y) / (r.getY() * lf);
					double errz = Math.abs(r.getZ() * lf - z) / (r.getZ() * lf);

					double maxerr = RELATIVE_POS_TOLERANCE;
//System.out.println(j+" "+r+"   "+x+" "+y+" "+z);
					if (errx > maxerr || erry > maxerr || errz > maxerr) {
						String errString = "Position mismatch at line " + j + " in file " + filename
								+ System.lineSeparator() + "Position should be: " + r.scalarMultiply(lf)
								+ System.lineSeparator() + "but in file " + f + " x= " + x + " y= " + y + " z= " + z
								+ System.lineSeparator() + "rel err. x =" + errx + " rel err. y =" + erry
								+ " rel err. z =" + errz + System.lineSeparator() + "Maximum allowed deviation = "
								+ maxerr;
						logger.error(errString);
						throw new PositionMismatchException(errString);
					}

					SurfacePoint sp = surfacePoints.get(j);
					Vector3D field = new Vector3D(fx, fy, fz);
					sp.setField(field);
				}

			} catch (NumberFormatException e) {
				// we ignore the line, it might be just comment text
			}

			br.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.info("Field values at tile Gauss points are set from: " + f);
	}

	/**
	 * Writes the sources to a file.
	 *
	 * @param solFile solution file
	 */
	public void sourcesToFile(String solFile) {

		StringBuilder sb = new StringBuilder();
		int sc = 0;
		for (SurfacePoint sp : surfacePoints) {
			sb.append(sp.sourceToString()).append(System.lineSeparator());
			sc++;
		}

		logger.info("Source count: " + sc);
		FileUtils.writeTextFile(solFile, sb.toString());

	}

	public StringBuffer surfacePointsToString(LengthUnit unit) {

		double lf = unit.getFactor();

		StringBuffer sb = new StringBuffer();

		for (SurfacePoint sp : surfacePoints) {
			Vector3D r = sp.getLocation();
			sb.append(r.getX() * lf).append(" ").append(r.getY() * lf).append(" ").append(r.getZ() * lf)
					.append(System.lineSeparator());
		}

		return sb;
	}
}
