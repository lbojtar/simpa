/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 * <p>
 * Author : Lajos Bojtar
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api;

import java.util.List;

/**
 * Class containing the source arrangements that make up a region of the field
 * that will be evaluated.
 */
public class GlobalSourceArrangement {

	private final List<SourceArrangement> elements;
	private final String name;
	private double scaling;

	/**
	 * Construct a global source arrangement with the given name, list of
	 * SourceArrangement objects and scaling.
	 *
	 * @param name    Name for the global source arrangement.
	 * @param list    List of SourceArrangement objects.
	 * @param scaling Global scaling.
	 */
	public GlobalSourceArrangement(String name, List<SourceArrangement> list, double scaling) {
		this.name = name;
		this.elements = list;
		this.scaling = scaling;
	}


	/**
	 * Gets the list of SourceArrangement objects.
	 *
	 * @return List of SourceArrangement objects.
	 */
	public List<SourceArrangement> getElements() {
		return elements;
	}

	/**
	 * Gets the global scaling.
	 *
	 * @return The global scaling.
	 */
	public double getScaling() {
		return this.scaling;
	}

	/**
	 * Method to set/update the global scaling.
	 *
	 * @param sc Scaling value (double).
	 */
	public void setScaling(double sc) {
		this.scaling = sc;

	}

	/**
	 * Gets the name of the global source arrangement.
	 *
	 * @return The name of the global source arrangement.
	 */
	public String getName() {
		return name;
	}

}
