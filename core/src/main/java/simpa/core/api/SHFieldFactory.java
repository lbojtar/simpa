/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.sh.SHFieldFactoryImpl;

/**
 * Factory class for creating SHField implementations.
 */
public class SHFieldFactory {

	private static final Logger logger = LogManager.getLogger(SHFieldFactory.class);
	/**
	 * Default relative error for the for spherical harmonics coefficients.
	 * Coefficients smaller than the value determined by this limit considered zero
	 * and dropped.
	 */
	public static final double DEFAULT_RELATIVE_ERROR = 1e-14;

	private final SHFieldFactoryImpl impl;

	// API functions begin here

	/**
	 * Creates a SHFieldFactory object
	 */
	public SHFieldFactory() {
		impl = new SHFieldFactoryImpl();
	}

	/**
	 * Creates an SHField containing spherical harmonics coefficients and other
	 * necessary data for a field map and serializes it to the disk.
	 *
	 * @param gsa            GlobalSourceArrangement, contains all sources.
	 * @param covering       The collection of balls that make up the region for
	 *                       evaluation.
	 * @param lmax           Maximum degrees of expansion.
	 * @param binaryFileName Filename for the output binary file. If the file
	 *                       already exists, the field creation will be skipped.
	 * 
	 * @param relativeError  Relative limit for spherical harmonics coefficients.
	 *                       Coefficients smaller than the value determined by this
	 *                       limit considered zero and dropped. If null the value of
	 *                       the DEFAULT_RELATIVE_ERROR will be taken.
	 * @param writeStatFile  If true, then write statistics about the distribution
	 *                       of maximum degrees in the field map.
	 * 
	 * @param pass1Lmax      Maximum degree for the first pass of the SH expansion.
	 *                       A good value is lmax/3 for example. 
	 * 
	 * @throws IOException When something goes wrong reading or writing a file.
	 */
	public void createSHField(GlobalSourceArrangement gsa, SphereCovering covering, int lmax,
			String binaryFileName, Double relativeError, boolean writeStatFile, int pass1Lmax) throws IOException {
		
		if(pass1Lmax < 0 || pass1Lmax >= lmax)
			throw new IllegalArgumentException("pass1Lmax must be between 0 and lmax-1");

		// construct the source list
		List<PointSource> sources = new ArrayList<>();

		// check if the binary file already exists
		if (Files.exists(Paths.get(binaryFileName))) {
			logger.info(binaryFileName + " already exists, skipping creation of SHField");
			return;
		}
		;

		for (SourceArrangement sa : gsa.getElements()) {
			for (PointSource ps : sa.getPointSources()) {
				ps.scale(sa.getScaling());
				sources.add(ps);
			}
		}

		if (relativeError == null)
			relativeError = DEFAULT_RELATIVE_ERROR;

		impl.createSHField(sources, covering, lmax, binaryFileName, relativeError, writeStatFile, pass1Lmax);

	}

}
