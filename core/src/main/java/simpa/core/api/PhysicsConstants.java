/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

/**
 * Class that contains the constants related to physics for the application.
 */
public class PhysicsConstants {
    public static final double POSITIVE_ELEMENTARY_CHARGE = 1.602176620898E-19; // [C]
    /**
     * The mass of a proton in kg.
     */
    public static final double PROTON_MASS = 1.67262177774E-27; // [kg]
    /**
     * The mass of an electron in kg.
     */
    public static final double ELECTRON_MASS = 9.1093837015E-31; // [kg]
    /**
     * The energy of a proton in eV.
     */
    public static final double PROTON_ENERGY = 938272046.21; // [eV]
    /**
     * The speed of light in m/s.
     */
    public static final double SPEED_OF_LIGHT = 299792458; // [m/s]
    /**
     * The speed of light squared in m/s.
     */
    public static final double SPEED_OF_LIGHT_SQUARED = SPEED_OF_LIGHT * SPEED_OF_LIGHT; // [m/s]
    public static final double MU_OVER_4PI = 1E-7;
    public static final double MU = 4 * Math.PI * 1E-7;
    public static final double EPSILON = 1 / (MU * SPEED_OF_LIGHT_SQUARED);
    public static final double EPSILON4PI = 4 * Math.PI * EPSILON;
      
}
