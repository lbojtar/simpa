/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.b2a.SolverImpl;
import simpa.core.b2a.SurfacePointImpl;
import simpa.core.b2a.solve.IterativeSolverImpl;
import simpa.core.b2a.solve.StaticElectricSolverImpl;
import simpa.core.b2a.solve.StaticMagneticSolverImpl;

/**
 * Class for calculating the strength of sources.
 */
public class Solver {

	public static final Logger logger = LogManager.getLogger(Solver.class);
	private SolverImpl solver;

	// API functions begin here

	/**
	 * Constructor for direct solver
	 * 
	 * @param spoints List of surface points.
	 */
	public Solver(List<SurfacePoint> spoints, FieldType type) {
		createSources(spoints, type);
		if (type == FieldType.STATIC_MAGNETIC) {
			logger.info("Solving system of equations with " + spoints.size() * 2 + " unknowns with DIRECT method.");
			solver = new StaticMagneticSolverImpl(spoints);
		} else if (type == FieldType.STATIC_ELECTRIC) {
			logger.info("Solving system of equations with " + spoints.size() + " unknowns with DIRECT method.");
			solver = new StaticElectricSolverImpl(spoints);
		} else
			new IllegalArgumentException("Unknown field type: " + type);
	}

	/**
	 * 
	 * @param spoints List of surface points.
	 * @param type    The field type: static magnetic, static electric.
	 * @param opts    Options specifying the preconditioner type and its parameters.
	 */
	public Solver(List<SurfacePoint> spoints, FieldType type, SolverOptions opts) {
		createSources(spoints, type);
		solver = new IterativeSolverImpl(opts, spoints);

	}

	/**
	 * Special constructor for the slice preconditioner. This is useful for
	 * accelerator elements or anything which can be partitioned to slices.
	 * 
	 * @param type   The field type: static magnetic, static electric, or RF (not
	 *               yet implemented).
	 * @param slices List of not overlapping slices of the surface foints.
	 * @param opts   Options specifying the preconditioner type and its parameters.
	 *               Since this constructor is specially made for the slice
	 *               preconditioner, the preconditioner type inside the options
	 *               should be always Preconditioner.SLICE and the solver is
	 *               SolverType.GMRES
	 */
	public Solver(FieldType type, List<List<SurfacePoint>> slices, SolverOptions opts) {
		List<SurfacePoint> surfacePoints = new ArrayList<>();
		for (int i = 0; i < slices.size(); i++) {
			surfacePoints.addAll(slices.get(i));
		}
		createSources(surfacePoints, type);
		solver = new IterativeSolverImpl(slices, opts);

	}

	/**
	 * Calculates the strength of the sources and saves it to a file.
	 * 
	 * @param relativeError Allowed relative error.
	 */
	public void solve(double relativeError) {
		solver.solve(relativeError);
	}

	/**
	 * Returns the relative error between the reproduced field by the sources and
	 * the given field by the user. After solving A.x=b, the relative norm error is:
	 * |b-A.x|/|b]
	 *
	 * @return The residual error.
	 */
	public double getResidual() {
		return solver.getResidual();
	}

	// API functions ends here

	/**
	 * Create the source monopoles for the tiles at a given elevation distance from
	 * the surface, normal to it.
	 */
	private List<SurfacePointImpl> createSources(List<SurfacePoint> surfacePoints, FieldType type) {

		PointSource m;
		List<SurfacePointImpl> internal = new ArrayList<>();

		for (SurfacePoint surfacePoint : surfacePoints) {
			SurfacePointImpl sp = (SurfacePointImpl) surfacePoint;
			// Vector pointing to the elevated surface point where the source monopole is
			Vector3D ers = sp.getLocation().add(sp.getVectorToSource());

			if (type == FieldType.STATIC_MAGNETIC)
				m = new CurrentPointSource(ers);
			else if (type == FieldType.STATIC_ELECTRIC)
				m = new ElectricMonopole(ers, 1);
			else
				throw new IllegalArgumentException("Unknown field type: " + type);

			sp.setSource(m);
			internal.add(sp);
		}
		return internal;
	}
}
