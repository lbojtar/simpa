/**
 * This package contains the exceptions that can be thrown in the SIMPA Project.
 */
package simpa.core.api.exceptions;