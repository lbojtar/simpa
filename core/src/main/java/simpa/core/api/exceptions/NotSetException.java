// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on


package simpa.core.api.exceptions;

/**
 * Exception to be returned when an attribute is not set.
 */
public class NotSetException extends Exception {
	
	private static final long serialVersionUID = -7847705863178277843L;

	/**
	 * Constructs the NotSetException with an error message.
	 *
	 * @param message The error message.
	 */
	public NotSetException(String message) {
		super(message);
	}
}
