// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.api.exceptions;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Exception to be returned when a given point is outside
 * the region (aperture).
 */
public class OutOfApertureException extends Exception {

    private static final long serialVersionUID = -2052720285793840828L;

    /**
     * The point that is out of aperture.
     */
    private final Vector3D point;

    /**
     * Constructs an OutOfApertureException with the correct message using the point that is out of aperture.
     *
     * @param p the point that is out of aperture.
     */
    public OutOfApertureException(Vector3D p) {
        super("Point " + p + " is out of aperture.");
        this.point = p;
    }

    /**
     * Gets the point that is out of aperture.
     *
     * @return the point that is out of aperture.
     */
    public Vector3D getPoint() {
        return point;
    }
}
