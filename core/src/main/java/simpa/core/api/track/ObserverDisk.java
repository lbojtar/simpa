/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api.track;

import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.Plane;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.SystemConstants;
import simpa.core.api.utils.LocalFrame;
import simpa.core.api.utils.OcTreeEntry;

/**
 * This class is used for getting the particle position and momentum during
 * tracking. It is also used for converting trajectory data from the global
 * Cartesian coordinate system to the local frame.
 * 
 * @author lbojtar
 *
 */
public class ObserverDisk implements OcTreeEntry {

	private Plane plane;
	private double radius;
	private Vector3D center;
	private LocalFrame localFrame;
	private boolean backward;

	/**
	 * Constructs a disk with the given radius, center and normal vector. This is
	 * useful for calculating the phase space coordinates.
	 * 
	 * @param center     - Center of the disk.
	 * @param localFrame - Local frame at the orbit point where the disk is placed.
	 * @param radius     - Radius of the observation disk. This should be big enough
	 *                   to cover the aperture, but smaller than the diameter of the
	 *                   machine, otherwise the particle trajectory would traverse
	 *                   two
	 *                   times the observation disk.
	 * @param backward   If true, the observer is placed at the first point of the
	 *                   orbit. This should be set true when tracking backward.
	 */
	public ObserverDisk(Vector3D center, LocalFrame localFrame, double radius, boolean backward) {
		this.backward = backward;
		init(center, localFrame, radius);
	}

	private void init(Vector3D center, LocalFrame localFrame, double radius) {
		this.center = center;

		// construct the observation plane
		Plane pl = new Plane(center, localFrame.lz(), SystemConstants.PLANE_AND_LINE_TOLERANCE);
		// align the disk with the local frame orientation
		Rotation rot = new Rotation(pl.getU(), localFrame.lz(), localFrame.lx(), localFrame.lz());
		this.plane = pl.rotate(center, rot);

		this.radius = radius;
		this.localFrame = localFrame;
	}

	/**
	 * Gets the plane of the disk.
	 * 
	 * @return Plane
	 */
	public Plane getPlane() {
		return plane;
	}

	/**
	 * Set the plane for this ObserverDisk
	 * 
	 * @param p - Plane
	 */
	public void setPlane(Plane p) {
		plane = p;
	}

	/**
	 * Gets the radius of the disk.
	 * 
	 * @return -radius
	 */
	public double getDiskRadius() {
		return radius;
	}

	/**
	 * Gets the center of the disk.
	 * 
	 * @return -center
	 */
	public Vector3D getCenter() {
		return center;
	}

	/**
	 * Sets the center of this ObserverDisk
	 * 
	 * @param c -center
	 */
	public void setCenter(Vector3D c) {
		center = c;
	}

	/**
	 * Returns the intersection of this disk with the given line segment formed by
	 * the two points. It can be null if the segment is not intersecting with the
	 * disk.
	 * 
	 * @param p1 - first point of the line segment
	 * @param p2 - second point of the line segment
	 * @return Vector3D - intersection point
	 */
	public Vector3D getIntersection(Vector3D p1, Vector3D p2) {

		// the intersection can be null, because it is on the Plane, we check for that
		// case
		if (plane.contains(p1))
			return p1;
		if (plane.contains(p2))
			return p2;

		Vector3D is = plane.intersection(new Line(p1, p2, SystemConstants.PLANE_AND_LINE_TOLERANCE));

		if (is == null) {
			throw new IllegalArgumentException("No intersection found between " + p1 + " and " + p2);
		}

		if (is.subtract(center).getNorm() < radius) {
			// intersection of line with the disk plane exists, we need to check if the
			// intersection is between the points
			if (plane.getOffset(p1) * plane.getOffset(p2) <= 0)
				return is;
		}

		return null;
	}

	/**
	 * Gets the oriented distance of the point from the Plane (positive or negative
	 * depending which side of the point is of the disk.)
	 * 
	 * @param point - Vector3D point
	 * @return double distance
	 */
	public double getDistance(Vector3D point) {
		return plane.getOffset(point);
	}

	public Vector3D getXAxis() {
		return localFrame.lx();
	}

	public Vector3D getYAxis() {
		return localFrame.ly();
	}

	public Vector3D getZAxis() {
		return localFrame.lz();
	}

	/**
	 * Calculates the phase-space coordinates at the intersection point
	 * 
	 * @param is  - Intersection point with the plane
	 * @param mom - The normalized kinetic momentum at the intersection
	 * @return PhaseSpaceCoordinates
	 */
	public PhaseSpaceCoordinates getPhaseSpaceCoordinates(Vector3D is, Vector3D mom) {
		if (is.subtract(getCenter()).getNorm() > getDiskRadius()) {
			return null;
		}

		// intersection in local coords.
		Vector3D lis = is.subtract(getCenter());
		double sign = 1.0;
		if (backward)
			sign = -1.0;

		double y = getYAxis().dotProduct(lis);
		double x = getXAxis().dotProduct(lis);
		double yp = sign * getYAxis().dotProduct(mom);
		double xp = sign * getXAxis().dotProduct(mom);

		return new PhaseSpaceCoordinates(x, xp, y, yp, 0, 0);
	}

	@Override
	public Vector3D getLocation() {
		return center;
	}
}
