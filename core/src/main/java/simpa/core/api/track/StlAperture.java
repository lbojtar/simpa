/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
/*
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api.track;

import org.apache.commons.math3.geometry.euclidean.threed.Euclidean3D;
import org.apache.commons.math3.geometry.euclidean.threed.Line;
import org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.partitioning.Region.Location;
import org.apache.commons.math3.geometry.partitioning.SubHyperplane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simpa.core.api.LengthUnit;
import simpa.core.api.SystemConstants;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.TextStlReader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Implementation of the Aperture interface that constructs its
 * aperture from an STL file.
 */
public class StlAperture implements Aperture {
    private static final Logger logger = LogManager.getLogger(StlAperture.class);

    private PolyhedronsSet polyhedronsSet;

    /**
     * Initializes the aperture from an STL file.
     *
     * @param stlFile input STL file
     */
    public StlAperture(String stlFile) {       
        init(stlFile);
    }

   
    @Override
    public boolean isInside(Vector3D pos) {
        return polyhedronsSet.checkPoint(pos) == Location.INSIDE;
    }
    
///////////////API ends here
    
    /**
     * Calculates an approximate distance of the given point from the boundary surface of the aperture.
     * 
     * @param p -Point to be calculated
     * @return Distance of the point from the aperture.
     */
    protected double getDistanceFromAperture(Vector3D p) {
        // a good sampling of all directions from a t-design
        InputStream is = getClass().getResourceAsStream("/t-design/" + "ss009.00048");
        List<Vector3D> dirs = FileUtils.readVectors(is, LengthUnit.M);
        double min = Double.MAX_VALUE;

        for (Vector3D d : dirs) {
            Line line = new Line(p, p.add(d), SystemConstants.GEOMETRY_TOLERANCE);
            SubHyperplane<Euclidean3D> sp = polyhedronsSet.firstIntersection(p, line);
            if (sp != null) {
                double dist = Math.abs(sp.getHyperplane().getOffset(p));
                if (dist < min)
                    min = dist;
            } else
                return 0;
        }
        return min;
    }

    private void init(String stlFile) {
        TextStlReader stlr = new TextStlReader();
        // this represents the boundary surface
        try {
            polyhedronsSet = stlr.createStlObject(stlFile);
        } catch (IOException e) {
            logger.error("IOException happened with file: " + stlFile + ".");
            logger.debug(Arrays.toString(e.getStackTrace()));
        }

    }

}
