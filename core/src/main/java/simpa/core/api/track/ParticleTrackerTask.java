/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.track;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.sh.BackwardPotentialWrapper;

/**
 * Object for tasks that can be given to the particle tracker.
 */
public class ParticleTrackerTask implements Runnable {
	private static final Logger logger = LogManager.getLogger(ParticleTrackerTask.class);

	private PotentialProvider potProvider;
	private final Particle particle;
	private long steps;
	private double stepsize;
	private TaosSymplecticIntegrator integrator;
	private final List<TrackingObserver> observers = new ArrayList<>();
	private boolean message = true;

	/**
	 * @param potProv  - A PotentialProvider object
	 * @param p        - A Particle
	 * @param steps    - The number of steps to be taken.
	 * @param stepsize - Step size [m]
	 * @param backward - If true the particle is tracked backward.
	 */
	public ParticleTrackerTask(PotentialProvider potProv, Particle p, long steps, double stepsize, boolean backward) {

		p.reset(backward);
		
		if (backward)
			potProvider = new BackwardPotentialWrapper(potProv);
		else
			potProvider = potProv;
		
		this.particle = p;
		this.stepsize = stepsize;
		this.steps = steps;

	}

	@Override
	public void run() {
		try {
			track();
		} catch (KnownFatalException e) {
			e.printStackTrace();
		}
	}

	public Particle getParticle() {
		return particle;
	}

	/**
	 * Gets a list of observers registered with this particle tracker task.
	 *
	 * @return list of observers.
	 */
	public List<TrackingObserver> getObservers() {
		return observers;
	}

	/**
	 * Adds an implementations of the Observer interface to this tracker task. Used
	 * for getting out some information about the particle during the tracking.
	 * 
	 * @param obs An Observer object
	 */
	public void addObserver(TrackingObserver obs) {
		if (obs instanceof BeamLossObserver)
			message = false; // don't print loss message if BeamLossObserver is added
		observers.add(obs);
	}

	//////////// API ends here
	//////////// //////////////////////////////////////////////////////////

	protected PotentialProvider getPotentialProvider() {
		return potProvider;
	}

	protected void setPotentialProvider(PotentialProvider pp) {
		this.potProvider = pp;
	}

	private void track() throws KnownFatalException {

		long st = System.currentTimeMillis();

		try {

			if (integrator == null) {

				integrator = new TaosSymplecticIntegrator(particle, potProvider, stepsize);

			}

			for (long i = 0; i < steps && !particle.getState().isTerminated(); i++) {

				integrator.step();

				for (TrackingObserver ob : getObservers()) {
					ob.observe(particle);
				}

			}

			for (TrackingObserver ob : getObservers()) {
				ob.finish(particle);				
			}

		} catch (OutOfApertureException e) {
			particle.getState().setLost(true);

			if (message)
				logger.info("Particle " + particle.getName() + " is lost at position: " + particle.getX() + " "
						+ particle.getY() + " " + particle.getZ() + ".");

			for (TrackingObserver ob : getObservers()) {
				ob.finish(particle);
			}
			return;
		}

		long end = System.currentTimeMillis();
		if (message) {
			logger.info("Finished tracking " + particle.getName() + " in " + (end - st) / 1000 + " s");
			logger.info("Number of evaluations = " + particle.getState().getEvalCount());
		}
		//let it to garbage collected, its a big object
		integrator.setPotentialProvider(null);
		potProvider=null;
	}

}
