/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api.track;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.PhysicsConstants;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.utils.CalculatorUtils;

public class TaosSymplecticIntegrator {

	private static double omega = -1.0; 

	private final double[] q, p, x, y;
	private final double[] dHdq, dHdp;	
	private final Particle particle;
	private final double dt; // time step
	private final double cosOmega, sinOmega;// Tao's coupling parameter, not the RF 2*PI*freq !!
	private double[][] lastPots;
	private Vector3D lastR;
	private final double p0; // norm of the kinetic momentum
	
	private  PotentialProvider potProvider;
	
	//////////// API starts here

	/**
	 * Sets the coupling constant for Tao's method. This is a specific parameter
	 * needed for the integrator to work properly. Its value is between 0.. 2*PI.
	 * There is no easy way to determine its value in advance, but trying a few
	 * values is a good strategy. A good value is when the energy oscillation has
	 * the smallest value. Adding a {@link TrajectoryObserver} to the
	 * {@link ParticleTrackerTask}
	 * produces a trajectory file, which contains the particle energy at each step.
	 * Plot (with gnuplot for example) this energy deviation for a few trial run and
	 * find the minimum. If the omega parameter is very far, the particle can be
	 * lost quickly.
	 *
	 * @param om The omega value.
	 */
	public static void setOmega(double om) {
		omega = om;
		if (omega < 0 || omega > 2 * Math.PI) {
			throw new IllegalArgumentException("Tao's integrator omega parameter is out of range (0 - 2*PI) : " + omega);
		}
		System.out.println("Tao's Integrator omega set to: " + omega);
	}

	public void setPotentialProvider(PotentialProvider potProvide) {
		this.potProvider= potProvide;
	}
	
	//////////// API ends here

	// Molei Tao's integrator See: https://arxiv.org/abs/1609.02212
	protected TaosSymplecticIntegrator(Particle part, PotentialProvider afp, double ds)
			throws OutOfApertureException, KnownFatalException {

		this.particle = part;
		this.potProvider = afp;
		this.dt = ds / CalculatorUtils.getVelocityAbsValue(particle);
		this.p0 = part.getKineticMomentum().getNorm();

		lastR = new Vector3D(part.getX(), part.getY(), part.getZ());
		lastPots = afp.getPotentialsWithDerivatives(lastR);

		q = new double[3];
		p = new double[3];
		x = new double[3];
		y = new double[3];
		dHdq = new double[3];
		dHdp = new double[3];
		
		cosOmega = Math.cos(omega);
		sinOmega = Math.sin(omega);

		initCoordinates();

	}

	protected void step() throws OutOfApertureException, KnownFatalException {
		if (omega < 0 || omega > 2 * Math.PI) {
			throw new IllegalArgumentException("Tao's integrator omega parameter is not set !. Must be set between 0-2*PI");
		}
		// particle's time
		double time = particle.getTime();

		particle.getState().setPreviousPosition(lastR);
		particle.getState().setPreviousKineticMomentum(particle.getKineticMomentum());

		phi_H_a_update(0.5 * dt);
		phi_H_b_update(0.5 * dt);

		time += 0.5 * dt; // first half time step
		phi_omega_H_c_update();

		phi_H_b_update(0.5 * dt);
		phi_H_a_update(0.5 * dt);

		time += 0.5 * dt; // second half time step

		particle.updatePosition(q[0], q[1], q[2]);
		particle.updateMomentum(p[0], p[1], p[2], lastPots[0][0], lastPots[1][0], lastPots[2][0]);

		// System.out.println(q[0] + " " + q[1] + " " + q[2] + " " + p[0] * p0 + " " +
		// p[1] * p0 + " " + p[2] * p0 + " "
		// + lastVp[0][0] + " " + lastVp[1][0] + " " + lastVp[2][0]);

		particle.getState().setStepCount(particle.getState().getStepCount() + 1);

		// update partice time
		particle.setTime(time);
		particle.getState().setLastFieldEvaluation(lastPots);

	}

	private void initCoordinates() {
		// The coupling term involving omega doen't work well if the position and
		// momentum variables are many order of
		// magnitudes different, so we scale the position.

		q[0] = particle.getX();
		p[0] = particle.getPx();
		x[0] = particle.getX();
		y[0] = particle.getPx();

		q[1] = particle.getY();
		p[1] = particle.getPy();
		x[1] = particle.getY();
		y[1] = particle.getPy();

		q[2] = particle.getZ();
		p[2] = particle.getPz();
		x[2] = particle.getZ();
		y[2] = particle.getPz();
	}

	/**
	 * Calculates the derivatives of the Hamiltonian of a relativistic charged
	 * particle in a EM field. The derivatives are divided by the speed of light, so
	 * the time step should be c*t instead of t during integration.
	 * 
	 * @param qx - particle position coordinates
	 * @param P  - Canonical normalized momentum P/P0
	 * @throws OutOfApertureException when a point is out of aperture
	 */
	private void calculateDerivatives(double[] qx, double[] P) throws OutOfApertureException, KnownFatalException {

		Vector3D r = new Vector3D(qx[0], qx[1], qx[2]);

		// System.out.println(r.toString());
		double[][] vp;

		if (r.getX() == lastR.getX() && r.getY() == lastR.getY() && r.getZ() == lastR.getZ()) {
			vp = lastPots;
		} else {
			vp = potProvider.getPotentialsWithDerivatives(r);
			particle.getState().incrementEvalCount();
			lastPots = vp;
		}

		lastR = r;

		double charge = particle.getCharge();

		// vector potentials
		double axq = charge * vp[0][0];
		double ayq = charge * vp[1][0];
		double azq = charge * vp[2][0];

		// vector potential derivatives
		double dAxOdx = vp[0][1];
		double dAxOdy = vp[0][2];
		double dAxOdz = vp[0][3];

		double dAyOdx = vp[1][1];
		double dAyOdy = vp[1][2];
		double dAyOdz = vp[1][3];

		double dAzOdx = vp[2][1];
		double dAzOdy = vp[2][2];
		double dAzOdz = vp[2][3];

		// scalar potential derivatives
		double dSpOdx = vp[3][1];
		double dSpOdy = vp[3][2];
		double dSpOdz = vp[3][3];

		double pxMax = P[0] - axq;// kinetic momentum x component
		double pyMay = P[1] - ayq;
		double pzMaz = P[2] - azq;

		double c = PhysicsConstants.SPEED_OF_LIGHT;
		double c2 = c * c;
		double m2 = particle.getMass() * particle.getMass();

		double mom2 = pxMax * pxMax + pyMay * pyMay + pzMaz * pzMaz;

		// System.out.println(qx[0]+" "+qx[1]+" "+qx[2]+" "+Math.sqrt(mom2));

		double div = Math.sqrt((c2 * m2) + mom2);

		// from Mathematica file tdepHamiltonian.nb
		dHdq[0] = -c * charge * (pxMax * dAxOdx + pyMay * dAyOdx + pzMaz * dAzOdx) / div + charge * dSpOdx;

		dHdp[0] = c * pxMax / div;

		dHdq[1] = -c * charge * (pxMax * dAxOdy + pyMay * dAyOdy + pzMaz * dAzOdy) / div + charge * dSpOdy;

		dHdp[1] = c * pyMay / div;

		dHdq[2] = -c * charge * (pxMax * dAxOdz + pyMay * dAyOdz + pzMaz * dAzOdz) / div + charge * dSpOdz;

		dHdp[2] = c * pzMaz / div;

	}

	private void phi_H_a_update(double deltat) throws OutOfApertureException, KnownFatalException {

		calculateDerivatives(q, y);

		for (int i = 0; i < 3; i++) {
			p[i] -= deltat * dHdq[i];
			x[i] += deltat * dHdp[i];
		}

	}

	private void phi_H_b_update(double deltat) throws OutOfApertureException, KnownFatalException {

		calculateDerivatives(x, p);

		for (int i = 0; i < 3; i++) {
			q[i] += deltat * dHdp[i];
			y[i] -= deltat * dHdq[i];
		}
	}

	private void phi_omega_H_c_update() {

		// TODO: This is different from Tao's paper !!!! Why ??

		// We scale the momentum before using in the coupling term, otherwise it doesn't
		// work well when there are orders
		// of magnitude difference in the momentum and position variables.
		for (int i = 0; i < 3; i++) {
			double q_plus_x = q[i] + x[i];
			double q_minus_x = q[i] - x[i];
			double p_plus_y = (p[i] + y[i]) / p0;
			double p_minus_y = (p[i] - y[i]) / p0;

			q[i] = 0.5 * (q_plus_x + cosOmega * q_minus_x + sinOmega * p_minus_y);
			p[i] = 0.5 * (p_plus_y - sinOmega * q_minus_x + cosOmega * p_minus_y) * p0;
			x[i] = 0.5 * (q_plus_x - cosOmega * q_minus_x - sinOmega * p_minus_y);
			y[i] = 0.5 * (p_plus_y + sinOmega * q_minus_x - cosOmega * p_minus_y) * p0;
		}
	}

}
