package simpa.core.api.track;

import java.util.ArrayList;
import java.util.List;

import simpa.core.api.utils.FileUtils;

public class BeamLossObserver implements TrackingObserver {

	private int lossCount;
	private List<Particle> lostParticles = new ArrayList<>();

	@Override
	public void observe(Particle p) {
	}

	@Override
	public void finish(Particle p) {
		if (p.getState().isLost()) {
			lossCount++;
			lostParticles.add(p);
		}

	}

	public int getLossCount() {
		return lossCount;
	}

	public void setLossCount(int lossCount) {
		this.lossCount = lossCount;
	}

	public List<Particle> getLostParticles() {
		return lostParticles;
	}

	/**
	 * Writes the lost particles to a file.
	 * 
	 * @param fileName The name of the file.
	 */
	public void writeToFile(String fileName) {
		StringBuffer sb = new StringBuffer();
		sb.append("#LOST PARTICLES" + System.lineSeparator());
		sb.append(Particle.getHeader() + System.lineSeparator());
		for (Particle p : lostParticles) {
			sb.append(p.toString() + System.lineSeparator());
		}
		FileUtils.writeTextFile(fileName, sb.toString());
	}
}
