// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.api.track;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.FileNamingConventions;
import simpa.core.api.utils.CalculatorUtils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Object that can be added as an observer to a {@link ParticleTrackerTask}. It
 * writes the coordinates of the particle every steps to a file.
 */
public class TrajectoryObserver implements TrackingObserver {
	private static final Logger logger = LogManager.getLogger(TrajectoryObserver.class);

	private BufferedWriter outputStream;
	private boolean printStepNumbers = false;
	private boolean debugFields = false;
	private String fileName;
	private List<TrajectoryData> trajectoryDatas;
	private ObserverBehaviour behaviour;

	/**
	 * Constructor. Writes the trajectory of the particle to a file.
	 * 
	 * @param behaviour   Determines how the observer behaves.
	 * @param debugFields If true, the magnetic and electric field values are also
	 *                    written to the file.
	 */
	public TrajectoryObserver(ObserverBehaviour behaviour, boolean debugFields) {
		this.behaviour = behaviour;
		this.debugFields = debugFields;
		trajectoryDatas = new ArrayList<>();
	}

	@Override
	public void observe(Particle p) {

		if (outputStream == null) {
			fileName = FileNamingConventions.getTrajectoryFileName(p.getName());
			init(fileName);
		}

		TrajectoryData td;
		// at the first step we store the initial position
		if (p.getState().getStepCount() == 1) {
			td = trajectoryData(p, true);
			storeWrite(td);
			td = trajectoryData(p, false);
			storeWrite(td);
		} else {
			td = trajectoryData(p, false);
			storeWrite(td);
		}
	
	}

	@Override
	public void finish(Particle p) {
		try {
			outputStream.flush();
			outputStream.close();
			logger.info("Trajectory was written to: " + fileName);
		} catch (IOException e) {
			logger.error("Something went wrong closing the outputStream");
			logger.debug(Arrays.toString(e.getStackTrace()));
		}
	}

	/**
	 * Gets the printStepNumbers boolean.
	 *
	 * @return The printStepNumbers boolean
	 */
	public boolean isPrintStepNumbers() {
		return printStepNumbers;
	}

	/**
	 * Sets the boolean whether step numbers should be printed to the console.
	 *
	 * @param printStepNumbers if step numbers should be printed, set this to true.
	 */
	public void setPrintStepNumbers(boolean printStepNumbers) {
		this.printStepNumbers = printStepNumbers;
	}

	/**
	 * 
	 * @return True if the debug fields option is enabled.
	 */
	public boolean isDebugFields() {
		return debugFields;
	}

	/**
	 * 
	 * @return List of the trajectory steps.
	 */
	public List<TrajectoryData> getTrajectoryDatas() {
		return trajectoryDatas;
	}

	private void storeWrite(TrajectoryData td) {
		
		if (behaviour == ObserverBehaviour.STORE_ONLY || behaviour == ObserverBehaviour.STORE_AND_WRITE)
			trajectoryDatas.add(td);

		if (behaviour == ObserverBehaviour.WRITE_ONLY || behaviour == ObserverBehaviour.STORE_AND_WRITE)
			td.write(outputStream);
	}
	
	private TrajectoryData trajectoryData(Particle p, boolean previous) {
		Vector3D pk;
		double t = p.getTime();
		double x, y, z;
		double eKinEv = CalculatorUtils.joulesToEv(CalculatorUtils.getKineticEnergy(p)); // Kinetic energy [eV]

		if (previous) {
			Vector3D v = p.getPreviousPosition();
			x = v.getX();
			y = v.getY();
			z = v.getZ();
			pk = p.getPreviousKineticMomentum();
		} else {
			pk = p.getKineticMomentum();
			x = p.getX();
			y = p.getY();
			z = p.getZ();
		}
		double[][] pots = null;
		if (debugFields)
			pots = p.getState().getLastFieldEvaluation();

		if (printStepNumbers)
			logger.info("Step for particle " + p.getName() + " :" + p.getState().getStepCount() + "\r");
		return new TrajectoryData(x, y, z, pk.getX(), pk.getY(), pk.getZ(), t, eKinEv, pots);
	}

	private void init(String fileName) {
		try {
			outputStream = new BufferedWriter(new FileWriter(fileName, false));
			String s;
			if (!debugFields)
				s = "#X Y Z Px Py Pz Ekin t" + System.lineSeparator();
			else
				s = "#X Y Z Px Py Pz Ekin t Bx By Bz Ex Ey Ez" + System.lineSeparator();
			outputStream.write(s, 0, s.length());
		} catch (IOException e) {
			logger.error("IOException happened with file: " + fileName + ".");
			logger.debug(Arrays.toString(e.getStackTrace()));
		}
	}
}
