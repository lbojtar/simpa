/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 * <p>
 * Author : Lajos Bojtar
 * <p>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * <p>
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * <p>
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api.track;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.PhysicsConstants;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.utils.CalculatorUtils;
import simpa.core.impl.TrackingState;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Object that represents a particle.
 */
public class Particle {

	private static final Logger logger = LogManager.getLogger(Particle.class);

	private double x, y, z;
	private double px, py, pz; // canonical momentums: p_i= pkin_i+ q*A_i ; i=1,2,3
	private double ax, ay, az; // vector potential at the position
	private String name = "noname"; // Useful to identify particles during bunch tracking
	private double time = 0; // time elapsed since the beginning of tracking [s]

	private TrackingState state = new TrackingState(); // to hold info during tracking

	// proton by default
	private double charge = PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE;
	private double mass = PhysicsConstants.PROTON_MASS;

	/**
	 * Constructor with a direction vector, the direction will be normalized, so its
	 * amplitude doesn't matter.
	 *
	 * @param pos    position vector
	 * @param dir    direction vector
	 * @param vp     the vector potential at the position
	 * @param pInGeV Nominal momentum [GeV/c]
	 * @param charge charge of the particle in Culombs.
	 * @param mass   mass of the particle in kilograms.
	 */
	public Particle(Vector3D pos, Vector3D dir, Vector3D vp, double pInGeV, double charge, double mass) {

		double p0 = CalculatorUtils.getMomentumFromGeVoC(pInGeV);
		Vector3D pKin = dir.normalize().scalarMultiply(p0);

		this.charge = charge;
		this.mass = mass;

		this.x = pos.getX();
		this.y = pos.getY();
		this.z = pos.getZ();
		this.ax = vp.getX();
		this.ay = vp.getY();
		this.az = vp.getZ();

		this.px = pKin.getX() + this.charge * ax;
		this.py = pKin.getY() + this.charge * ay;
		this.pz = pKin.getZ() + this.charge * az;
	}

	/**
	 * Constructs a particle with the given parameters.
	 * 
	 * @param x      x position [m]
	 * @param y      y position [m]
	 * @param z      z position [m]
	 * @param px     x kinetic momentum [kg m/s]
	 * @param py     y kinetic momentum [kg m/s]
	 * @param pz     z kinetic momentum [kg m/s]
	 * @param ax     X component of the vector potential at the particle position
	 * @param ay     Y component of the vector potential at the particle position
	 * @param az     Z component of the vector potential at the particle position
	 * @param charge The charge [coulomb]
	 * @param mass   The mass [kg]
	 */
	public Particle(double x, double y, double z, double px, double py, double pz, double ax, double ay, double az,
			double charge, double mass) {

		this.x = x;
		this.y = y;
		this.z = z;

		this.px = px + this.charge * ax;
		this.py = py + this.charge * ay;
		this.pz = pz + this.charge * az;

		this.ax = ax;
		this.ay = ay;
		this.az = az;

		this.charge = charge;
		this.mass = mass;
	}


	/**
	 * Gets the X position of the particle.
	 *
	 * @return the X position of the particle.
	 */
	public double getX() {
		return x;
	}

	/**
	 * Gets the Y position of the particle.
	 *
	 * @return the Y position of the particle.
	 */

	public double getY() {
		return y;
	}

	/**
	 * Gets the Z position of the particle.
	 *
	 * @return the Z position of the particle.
	 */
	public double getZ() {
		return z;
	}

	/**
	 * Gets the particle's canonical momentum on the X axis.
	 *
	 * @return the particle's canonical momentum on the X axis.
	 */
	public double getPx() {
		return px;
	}

	/**
	 * Gets the particle's canonical momentum on the Y axis.
	 *
	 * @return the particle's canonical momentum on the Y axis.
	 */
	public double getPy() {
		return py;
	}

	/**
	 * Gets the particle's canonical momentum on the Z axis.
	 *
	 * @return the particle's canonical momentum on the Z axis.
	 */
	public double getPz() {
		return pz;
	}

	/**
	 * Gets the particle's charge.
	 *
	 * @return the particle's charge.
	 */
	public double getCharge() {
		return charge;
	}

	/**
	 * method for setting/updating the charge.
	 *
	 * @param charge the desired charge.
	 */
	public void setCharge(double charge) {
		this.charge = charge;
	}

	/**
	 * Gets the particle's mass.
	 *
	 * @return the particle's mass.
	 */
	public double getMass() {
		return mass;
	}

	/**
	 * method for setting/updating the mass.
	 *
	 * @param mass the desired mass.
	 */
	public void setMass(double mass) {
		this.mass = mass;
	}

	/**
	 * 
	 * @return Header sting for the toString method.
	 */
	public static String getHeader() {
		return "#x[m] y[m] z[m] px[kg*m/s] py[kg*m/s] pz[kg*m/s] name";
	}

	@Override
	public String toString() {
		return x + " " + y + "  " + z + "  " + px + "  " + py + "  " + pz + " " + " " + name;

	}

	/**
	 * Gets the name of the particle.
	 *
	 * @return String of the particle name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name for the particle.
	 *
	 * @param name The desired name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the lifetime of the particle.
	 *
	 * @return the lifetime of the particle
	 */
	public double getTime() {
		return time;
	}

	/**
	 * Calculates and returns the kinetic momentum of the particle.
	 *
	 * @return A vector of the kinetic momentum. [kg*m/s]
	 */
	public Vector3D getKineticMomentum() {

		double pkx = px - charge * ax;
		double pky = py - charge * ay;
		double pkz = pz - charge * az;

		return new Vector3D(pkx, pky, pkz);
	}

	// TODO this should update the previous position
	public void updatePosition(double xc, double yc, double zc) {
		this.x = xc;
		this.y = yc;
		this.z = zc;
	}

	/**
	 * Update the canonical momentum. We store in the particle object the vector
	 * potential too, in order to get the kinetic momentum if needed.
	 *
	 * @param px - Canonical momentum X component
	 * @param py - Canonical momentum Y component
	 * @param pz - Canonical momentum Z component
	 * @param ax - Vector potential X component
	 * @param ay - Vector potential Y component
	 * @param az - Vector potential Z component
	 */
	public void updateMomentum(double px, double py, double pz, double ax, double ay, double az) {
		this.px = px;
		this.py = py;
		this.pz = pz;
		this.ax = ax;
		this.ay = ay;
		this.az = az;
	}

	public void setTime(double time) {
		this.time = time;
	}

	public Vector3D getPreviousPosition() {
		return state.getPreviousPosition();
	}

	public void setPreviousPosition(Vector3D prevPos) {
		state.setPreviousPosition(prevPos);
	}

	public void setTerminated(boolean terminated) {
		this.state.setTerminated(terminated);
	}

	public boolean isTerminated() {
		return state.isTerminated();
	}

	public Vector3D getPreviousKineticMomentum() {
		return state.getPreviousKineticMomentum();
	}

	public void setPreviousKineticMomentum(Vector3D prevMom) {
		state.setPreviousKineticMomentum(prevMom);
	}

	public boolean isLost() {
		return state.isLost();
	}

	/**
	 * Resets the terminated flag to false, so the particle already tracked can be
	 * reused. If the terminated flag before the reset call is true and the reverse
	 * parameter is also true, it reverses the vector potential in the canonical
	 * momentum.
	 * 
	 * @param reverse If true it reverses the momentum of the particle and attach
	 *                the string "-backward" to the name of the particle.
	 */
	public void reset(boolean reverse) {

		if (reverse) {
			this.px = -px;
			this.py = -py;
			this.pz = -pz;

			if (state.isTerminated()) {
				this.ax = -ax;
				this.ay = -ay;
				this.az = -az;
			}
			name = name + "-backward"; // give a new name automatically
		}

		state.setTerminated(false);
		state.reset();
	}

	public Particle copy(Particle p, PotentialProvider pp) {
		try {
			double[][] pots = pp.getPotentialsWithDerivatives(new Vector3D(p.getX(), p.getY(), p.getZ()));

			Particle c = new Particle(p.getX(), p.getY(), p.getZ(), p.getPx(), p.getPy(), p.getPz(), pots[0][0],
					pots[1][0], pots[2][0], p.getCharge(), p.getMass());
			c.setName(p.getName());
			return c;

		} catch (OutOfApertureException | KnownFatalException e) {
			logger.error(e.toString());
		}
		return null;
	}
	
	// API ENDS HERE

	/**
	 * Gets the state of the particle.
	 *
	 * @return a tracking state object.
	 */
	protected TrackingState getState() {
		return state;
	}

	protected void setState(TrackingState state) {
		this.state = state;
	}

}
