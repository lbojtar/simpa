/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.track;

import simpa.core.api.PhysicsConstants;

/**
 * Enum used for the categorizing of particles.
 */
public enum ParticleType {

	/**
	 * Particle type that describes a proton.
	 */
	PROTON(PhysicsConstants.PROTON_MASS, PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE, "proton"),
	/**
	 * Particle type that describes an antiproton.
	 */
	ANTIPROTON(PhysicsConstants.PROTON_MASS, -PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE, "antiproton"),
	/**
	 * Particle type that describes an electron.
	 */
	ELECTRON(PhysicsConstants.ELECTRON_MASS, -PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE, "electron"),
	/**
	 * Particle type that describes a positron.
	 */
	POSITRON(PhysicsConstants.ELECTRON_MASS, PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE, "positron");

	private final double mass; // [kg]
	private final double charge; // [Coulumb]
	private final String string;

	ParticleType(double mass, double charge, String string) {
		this.mass = mass;
		this.charge = charge;
		this.string = string;
	}

	/**
	 * Gets the mass of the particle type.
	 *
	 * @return the mass.
	 */
	public double getMass() {
		return mass;
	}

	/**
	 * Gets the charge of the particle type.
	 *
	 * @return the charge.
	 */
	public double getCharge() {
		return charge;
	}

	/**
	 * Gets the name of the particle type.
	 *
	 * @return the name.
	 */
	public String getString() {
		return string;
	}

	/**
	 * Parses the particle type from its string value.
	 *
	 * @param type string version of the particle type.
	 * @return The correct ParticleType.
	 */
	public static ParticleType parse(String type) {
	    for (ParticleType pType : ParticleType.values()) {
	        if (pType.getString().equals(type)) {
	            return pType;
	        }
	    }
	    
	    throw new IllegalArgumentException("Unknown particle type: "+type);
	}

}
