/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api.track;

/**
 * Non-mutable record for representing the phase space coordinates of a
 * particle. This usually created by some observer object added to the
 * {@link ParticleTrackerTask}.
 * 
 * Creates an instance of PhaseSpaceCoordinates. The actual meaning of the phase
 * space coordinates depend on the application. For example in accelerators the
 * Z coordinate can be the time delay compared to the reference particle, but in
 * plasma simulation it might be simply the Cartesian Z coordinate.
 * 
 * @param x
 * @param xp
 * @param y
 * @param yp
 * @param z
 * @param zp
 */
public record PhaseSpaceCoordinates(double x, double xp, double y, double yp, double z, double zp) {

	public String toString() {
		return x + " " + xp + " " + y + " " + yp + " " + z + " " + zp;
	}
	
};
