/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.track;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simpa.core.api.PotentialProvider;
import simpa.core.impl.BlockingExecutor;

import java.util.Arrays;

/**
 * Class that sets up the BlockingExecutor for queue based multi threaded
 * tracking and adds the tracking tasks to the queue.
 */
public class ParallelTracker {
	private static final Logger logger = LogManager.getLogger(ParallelTracker.class);

	private final BlockingExecutor pool;

	/**
	 * Constructs a ParallelTracker with all available processor cores and sets up
	 * the BlockingExecutor.
	 */
	public ParallelTracker() {

		int nThreads = Runtime.getRuntime().availableProcessors();
		pool = new BlockingExecutor(nThreads);

	}

	/**
	 * Constructs a ParallelTracker with the given number of processor threads and
	 * sets up the BlockingExecutor.
	 *
	 * @param nThreads The amount of threads to use.
	 */
	public ParallelTracker(int nThreads) {
		pool = new BlockingExecutor(nThreads);

	}

	/**
	 * Adds the task to the queue. The potential provider object in the task will be
	 * deep copied, so all the task can share the same instance and still the
	 * executing threads will not interfere.
	 * 
	 * @param task A tracking task to be added to the queue.
	 */
	public void addCopyToQueue(ParticleTrackerTask task) {
		// make a copy of the PotentialProvider for thread safety
		PotentialProvider ppc = task.getPotentialProvider().copy();
		task.setPotentialProvider(ppc);
		pool.execute(task);
	

	}

	/**
	 * Adds the task to the queue. It is the responsibility of the caller to make
	 * sure the PotentialProvider objects inside each task are unique, so the
	 * threads of the queue will not interfere with each other.
	 * 
	 * @param task A tracking task to be added to the queue.
	 */
	public void addToQueue(ParticleTrackerTask task) {
		// make a copy of the PotentialProvider for thread safety
		PotentialProvider ppc = task.getPotentialProvider().copy();
		task.setPotentialProvider(ppc);
		pool.execute(task);
	}

	/**
	 * Awaits the termination of all tasks on all threads.
	 */
	public void waitTermination() {
		try {
			do {
				Thread.sleep(1);			
			} while (pool.getActiveCount() > 0);

		} catch (InterruptedException e) {
			logger.error("termination got interrupted");
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

	}

	/**
	 * Shuts down the underlying thread pool.
	 */
	public void shutdown() {
		waitTermination();
		pool.shutdown();
	}
}
