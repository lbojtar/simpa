package simpa.core.api.track;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.SystemConstants;
import simpa.core.api.utils.CalculatorUtils;
import simpa.core.api.utils.FileUtils;

public class BeamTrajectoryObserver implements TrackingObserver {


	private Map<String, List<TrajectoryData>> trajectoryDatasMap; // key is the particle name
	private ObserverBehaviour behaviour;

	public BeamTrajectoryObserver(ObserverBehaviour behaviour) {
		this.behaviour = behaviour;
		trajectoryDatasMap = new HashMap<>();
	}

	public ObserverBehaviour getBehaviour() {
		return behaviour;
	}

	// MUST BE synchronized, for thread safety !!
	@Override
	public synchronized void observe(Particle p) {
		List<TrajectoryData> l = trajectoryDatasMap.get(p.getName());
		if (l == null) {
			l = new ArrayList<>();
			trajectoryDatasMap.put(p.getName(), l);
		}
		if (p.getState().getStepCount() == 1) {
			appendState(p, true, l); // step 0
			appendState(p, false, l);// step 1
		} else
			appendState(p, false, l);
	}

	@Override
	public void finish(Particle p) {
	}

	/**
	 * 
	 * @return A map containing trajectory for each particle in the beam. The key is
	 *         the particle name.
	 */
	public Map<String, List<TrajectoryData>> getTrajectoryData() {
		return trajectoryDatasMap;
	}

	/**
	 * Write the trajectory of the particles in a beam hitting this observer to a
	 * file. Should be called after the beam tracking is finished. If the observer
	 * was created with the ObserverBehaviour.STORE_ONLY option the method returns
	 * without writing the file. 
	 * 
	 * @param fileName The file to write to.
	 */
	public void writeToFile(String fileName) {

		if (behaviour == ObserverBehaviour.STORE_ONLY)
			return;

		StringBuilder sb = new StringBuilder();
		sb.append("#X Y Z Px Py Pz Ekin t" + System.lineSeparator());

		for (String name : trajectoryDatasMap.keySet()) {
			sb.append(SystemConstants.PARTICLE_SEPARATOR_STRING + " particle name: " + name + System.lineSeparator());
			List<TrajectoryData> l = trajectoryDatasMap.get(name);
			l.stream().forEach(td -> td.append(sb));
			sb.append(System.lineSeparator());
		}
		FileUtils.writeTextFile(fileName, sb.toString());

	}

	private void appendState(Particle p, boolean previous, List<TrajectoryData> tdList) {

		Vector3D pk;

		double evKin = CalculatorUtils.joulesToEv(CalculatorUtils.getKineticEnergy(p)); // Kinetic energyin [eV]
		double t = p.getTime();
		double x, y, z;
		if (previous) {
			Vector3D v = p.getPreviousPosition();
			x = v.getX();
			y = v.getY();
			z = v.getZ();
			pk = p.getPreviousKineticMomentum();
		} else {
			pk = p.getKineticMomentum();
			x = p.getX();
			y = p.getY();
			z = p.getZ();
		}

		TrajectoryData td = new TrajectoryData(x, y, z, pk.getX(), pk.getY(), pk.getZ(), t, evKin, null);
		tdList.add(td);
	}
}
