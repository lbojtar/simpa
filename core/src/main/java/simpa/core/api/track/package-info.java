/**
 * This package contains the code related to tracking particles in SIMPA.
 */
package simpa.core.api.track;