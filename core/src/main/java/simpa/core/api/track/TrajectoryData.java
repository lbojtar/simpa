package simpa.core.api.track;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.SystemConstants;
import simpa.core.api.utils.CalculatorUtils;

/**
 * Trajectory Observer registers the trajectory of a particle with this data.
 * It represents the GLOBAL position and momentum of a particle at a given time
 * step. The potentials with derivatives is null unless the
 * {@link TrajectoryObserver}  is created with the debug option.
 * 
 * @author lbojtar
 *
 */
public record TrajectoryData(double x, double y, double z, double px, double py, double pz, double t, double eKinEv,
		double[][] potentialsWithDerivatives) {

	private static final Logger logger = LogManager.getLogger(TrajectoryData.class);

	/**
	 * Reads a trajectory data of a single particle from a file.
	 * 
	 * @param trajectoryFileName Trajectory file name.
	 * @return List of trajectory data, for each step.
	 * @throws IOException
	 */
	public static List<TrajectoryData> readParticleTrajectoryFile(String trajectoryFileName) throws IOException {

		List<TrajectoryData> l = new ArrayList<>();
		InputStreamReader is = new InputStreamReader(new FileInputStream(trajectoryFileName));
		BufferedReader br = new BufferedReader(is);

		do {
			String line = br.readLine();
			if (line != null) {
				TrajectoryData td = parseLine(line);
				if (td != null)
					l.add(td);
			} else
				break; // end of file
		} while (true);

		br.close();

		return l;
	}

	/**
	 * Reds the global trajectory of the particles from the beamTrajFile
	 * 
	 * @param beamTrajFile The file name
	 * @return Map of particle name and trajectory data
	 * @throws IOException If the file is not found or not readable
	 */
	public static Map<String, List<TrajectoryData>> readBeamTrajectoryFile(String beamTrajFile) throws IOException {

		Map<String, List<TrajectoryData>> map = new HashMap<>();
		InputStreamReader is = new InputStreamReader(new FileInputStream(beamTrajFile));
		BufferedReader br = new BufferedReader(is);
		List<TrajectoryData> list = null;

		do {
			String line = br.readLine();
			if (line != null) {
				if (line.contains(SystemConstants.PARTICLE_SEPARATOR_STRING)) {
					list = new ArrayList<>();
					line = line.replace(SystemConstants.PARTICLE_SEPARATOR_STRING, "");
					String pName = line.replace(" particle name: ", "");
					map.put(pName, list);
				} else {
					TrajectoryData td = parseLine(line);
					if (td != null) {
						list.add(td);
					}
				}
			} else
				break; // end of file
		} while (true);

		br.close();

		return map;
	}

	/**
	 * Writes this trajectory data to the given output stream.
	 * 
	 * @param outputStream The BufferedWriter into which the string representation
	 *                     of the date will be written with a new line carater at
	 *                     the end.
	 */
	public void write(BufferedWriter outputStream) {
		StringBuilder sb = new StringBuilder();
		append(sb);
		try {
			outputStream.write(sb.toString());
		} catch (IOException e) {
			logger.error("Something went wrong writing the output stream to a file.");
			logger.debug(Arrays.toString(e.getStackTrace()));

		}
	}

	/**
	 * Appends this trajectory data to the given StringBuilder.
	 * 
	 * @param sb The String builder to append to.
	 */
	public void append(StringBuilder sb) {
		String s;

		if (potentialsWithDerivatives() != null) {

			Vector3D b = CalculatorUtils.getBField(potentialsWithDerivatives());
			Vector3D e = CalculatorUtils.getEField(potentialsWithDerivatives());

			s = x() + " " + y() + " " + z() + " " + px() + " " + py() + " " + pz() + " " + eKinEv() + " " + t() + " "
					+ b.getX() + " " + b.getY() + " " + b.getZ() + " " + e.getX() + " " + e.getY() + " " + e.getZ()
					+ " " + System.lineSeparator();
		} else
			s = x() + " " + y() + " " + z() + " " + px() + " " + py() + " " + pz() + " " + eKinEv() + " " + t()
					+ System.lineSeparator();

		sb.append(s, 0, s.length());

	}

	/**
	 * Parses Trajectory data line
	 * 
	 * @param line The line string.
	 * @return TrajectoryData or null if the line is not valid
	 */
	private static TrajectoryData parseLine(String line) {

		if (line == null)
			return null;

		String[] sa = line.split(" ");
		if (sa.length < 8)
			return null;

		double[] da = new double[8];
		try {
			for (int i = 0; i < 8; i++)
				da[i] = Double.parseDouble(sa[i]);
		} catch (Exception e) {
			return null;
		}

		return new TrajectoryData(da[0], da[1], da[2], da[3], da[4], da[5], da[6], da[7], null);

	}
}
