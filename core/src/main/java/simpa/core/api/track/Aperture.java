// @formatter:off
/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */
// @formatter:on

package simpa.core.api.track;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Region of the volume.
 */
public interface  Aperture {
	/**
	 * Returns false when a given point is outside the volume.
	 *
	 * @param pos location to check.
	 * @return true if the location is inside the volume, false if not.
	 */
	public  boolean isInside(Vector3D pos);

}
