/**
 * Copyright (c) 2020 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api;

/**
 * Type of SphereCovering
 */
public enum CoverType {
    SINGLE_ROW,
    LATTICE;   
}
