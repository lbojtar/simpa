/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * A point source that is outside the volume used to reproduce the B field
 * inside the volume of interest.
 */
public class CurrentPointSource implements PointSource {

	private Vector3D loc;// location of the point souurce

	// base vectors
	private Vector3D base1 = Vector3D.PLUS_I;
	private Vector3D base2 = Vector3D.PLUS_J;
	private Vector3D base3 = Vector3D.PLUS_K;

	private double i1; // current in b1 direction
	private double i2; // current in b2 direction
	private double i3; // current in b3 direction

	public CurrentPointSource(Vector3D loc) {
		this.loc = loc;

	}

	/**
	 * 
	 * @return Shallow copy, except the currents which is native, so deep.
	 */
	public PointSource copy() {
		CurrentPointSource c = new CurrentPointSource(loc);
		c.base1 = base1;
		c.base2 = base2;
		c.base3 = base3;
		c.i1 = i1;
		c.i2 = i2;
		c.i3 = i3;

		return c;
	}

	/**
	 * Set all the base vectors for the CurrentPointSource.
	 *
	 * @param localX - the local x location.
	 * @param localY - the local y location.
	 * @param localZ - the local z location.
	 */
	public void setBaseVectors(Vector3D localX, Vector3D localY, Vector3D localZ) {
		this.base1 = localX;
		this.base2 = localY;
		this.base3 = localZ;
	}

	public Vector3D getLocalX() {
		return base1;
	}

	public Vector3D getLocalY() {
		return base2;
	}

	public Vector3D getLocalZ() {
		return base3;
	}

	/**
	 * Projects the given Vector point to the x-axis.
	 *
	 * @param b vector to use in projection
	 * @return the dot product of the x-axis and the input vector.
	 */
	public double projectToLocalX(Vector3D b) {
		return b.dotProduct(base1);
	}

	/**
	 * Projects the given Vector point to the y-axis.
	 *
	 * @param b vector to use in projection
	 * @return the dot product of the y-axis and the input vector.
	 */
	public double projectToLocalY(Vector3D b) {
		return b.dotProduct(base2);
	}

	/**
	 * Projects the given Vector point to the z-axis.
	 *
	 * @param b vector to use in projection
	 * @return the dot product of the z-axis and the input vector.
	 */
	public double projectToLocalZ(Vector3D b) {
		return b.dotProduct(base3);
	}

	@Override
	public String toString() {
		Vector3D irv = getCurrent();
		return loc.getX() + " " + loc.getY() + " " + loc.getZ() + " " + irv.getX() + " " + irv.getY() + " "
				+ irv.getZ();

	}

	/**
	 * 
	 * @return The current in the global coordinate system.
	 */
	public Vector3D getCurrent() {
		return base1.scalarMultiply(i1).add(base2.scalarMultiply(i2)).add(base3.scalarMultiply(i3));
	}

	public Vector3D getLocalCurrentComponents() {
		return new Vector3D(i1,i2,i3);
	}
	/**
	 * Set the components of the currents in the local frame.
	 * 
	 * @param i1 Current in the direction of local X.
	 * @param i2 Current in the direction of local Y.
	 * @param i3 Current in the direction of local Z.
	 */
	public void setLocalCurrentComponents(double i1, double i2, double i3) {
		this.i1 = i1;
		this.i2 = i2;
		this.i3 = i3;

	}

	/**
	 * Scales the currents of the point source using a scaling factor.
	 *
	 * @param scalingFactor factor to scale with.
	 */
	public void scale(double scalingFactor) {
		i1 *= scalingFactor;
		i2 *= scalingFactor;
		i3 *= scalingFactor;
	}

	public Vector3D getLocation() {
		return loc;
	}

	public void setLocation(Vector3D loc) {
		this.loc = loc;
	}

	/**
	 * Calculates the B field at a given evaluation point.
	 *
	 * @param evp evaluation point
	 * @return B field
	 */
	public Vector3D getField(Vector3D evp) {
		Vector3D r = evp.subtract(getLocation());
		double ra = r.getNorm();
		double r3 = ra * ra * ra;
		double x = r.getX();
		double y = r.getY();
		double z = r.getZ();
		Vector3D i = getCurrent();
		double ix = i.getX();
		double iy = i.getY();
		double iz = i.getZ();

		double bx = (iy * z - iz * y) / r3;
		double by = (iz * x - ix * z) / r3;
		double bz = (ix * y - iy * x) / r3;
		return new Vector3D(bx, by, bz);

	}
}
