/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simpa.core.api.utils.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Class that contains a list of PointSource objects that make up a
 * SourceArrangement. It is used for reproducing an EM a field inside a volume.
 */
public class SourceArrangement {
	private static final Logger logger = LogManager.getLogger(SourceArrangement.class);

	private List<PointSource> sources = null;
	private Vector3D translation = Vector3D.ZERO; // compared to the local frame
	private Rotation rotation = Rotation.IDENTITY; // compared to the local frame
	private double scaling = 1.0;
	private final String name;

	// API functions begin here

	/**
	 * Constructs a SourceArrangement out of a list of point sources. All point
	 * sources must have the same type, mixing magnetic and electric sources is not
	 * allowed.
	 *
	 * @param poles   Point sources.
	 * @param scaling The scaling for the sources.
	 * @param name    The name of the element.
	 */
	public SourceArrangement(List<PointSource> poles, double scaling, String name) {

		// check if all sources are of the same type
		if (poles.size() > 0) {
			if (poles.get(0) instanceof ElectricMonopole) {
				for (PointSource p : poles) {
					if (!(p instanceof ElectricMonopole)) {
						throw new IllegalArgumentException("All sources must be of the same type.");
					}
				}
			} else {
				for (PointSource p : poles) {
					if (!(p instanceof CurrentPointSource)) {
						throw new IllegalArgumentException("All sources must be of the same type.");
					}
				}
			}
		}

		this.sources = poles;
		this.name = name;
		this.setScaling(scaling);
	}

	/**
	 * Sets the SourceArrangement based on a given file and applies the given
	 * translation and rotation.
	 *
	 * @param filename    Name of the file with the source locations.
	 * @param rotation    Rotation that should be applied to the sources.
	 * @param translation Translation for the sources.
	 * @param scaling     Scaling for the sources
	 * @param name        Name of the element.
	 */
	public SourceArrangement(String filename, Vector3D translation, Rotation rotation, double scaling, String name) {

		this.translation = translation;
		this.rotation = rotation;
		this.name = name;
		this.setScaling(scaling);
		readSources(filename, rotation, translation);

	}

	/**
	 * Gets the scaling of the source arrangement.
	 *
	 * @return The scaling value.
	 */
	public double getScaling() {
		return scaling;
	}

	/**
	 * Sets the scaling of the source arrangement.
	 *
	 * @param scaling The scaling value.
	 */
	public void setScaling(double scaling) {
		this.scaling = scaling;
	}

	/**
	 * Gets the element name.
	 *
	 * @return The name of the element.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the center of the element.
	 *
	 * @return the center of the element (translation).
	 */
	public Vector3D getCenter() {
		return translation;
	}

	/**
	 * 
	 * @return Rotation of this instance compared to a reference frame frame
	 *         {(1,0,0),{0,1,0),(0,0,1)}
	 * 
	 */
	public Rotation getRotation() {
		return this.rotation;
	}

	/**
	 * Writes a text file containing all source positions to the given file in the
	 * working directory. For each source there is a line in the file.
	 * 
	 * @param fileName - Name of the output file.
	 */
	public void sourcePositionsToFile(String fileName) {

		StringBuilder sb = new StringBuilder();
		sb.append("#X Y Z  NAME").append(System.lineSeparator());

		for (PointSource p : getPointSources()) {
			Vector3D v = p.getLocation();
			sb.append(v.getX()).append(" ").append(v.getY()).append(" ").append(v.getZ()).append(" ").append(getName())
					.append(System.lineSeparator());
		}

		FileUtils.writeTextFile(fileName, sb.toString());
	}

	/**
	 * Gets the list of point sources.
	 *
	 * @return The list of point sources.
	 */
	public List<PointSource> getPointSources() {
		return sources;
	}

	/**
	 * Creates a transformed instance. First rotates then translates all sources.
	 *
	 * @param rotation    Rotation that should be applied to the sources.
	 * @param translation Translation for the sources.
	 */
	public SourceArrangement transformSources(Vector3D translation, Rotation rotation) {
		List<PointSource> transformed = new ArrayList<>();
		double[] data = new double[6];

		for (PointSource s : sources) {
			data[0] = s.getLocation().getX();
			data[1] = s.getLocation().getY();
			data[2] = s.getLocation().getZ();

			if (s instanceof CurrentPointSource) {
				Vector3D i = ((CurrentPointSource) s).getLocalCurrentComponents();
				data[3] = i.getX();
				data[4] = i.getY();
				data[5] = i.getZ();
				transformed.add(create(data, 6, rotation, translation));
			} else {
				data[3] = ((ElectricMonopole) s).getCharge();
				transformed.add(create(data, 4, rotation, translation));
			}

		}
		return new SourceArrangement(transformed, scaling, name);
	}

	// API functions end here

	private void readSources(String filename, Rotation rot, Vector3D translation) {

		sources = new ArrayList<>();

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
			double[] data = new double[6];

			do {
				String s = br.readLine();
				if (s == null) {
					break;
				}
				StringTokenizer st = new StringTokenizer(s);

				try {
					int nColumns = st.countTokens();
					// location coordinates
					data[0] = Double.parseDouble(st.nextToken());
					data[1] = Double.parseDouble(st.nextToken());
					data[2] = Double.parseDouble(st.nextToken());

					if (nColumns == 6) {
						data[3] = Double.parseDouble(st.nextToken());
						data[4] = Double.parseDouble(st.nextToken());
						data[5] = Double.parseDouble(st.nextToken());
					}

					if (nColumns == 4) {
						data[3] = (Double.parseDouble(st.nextToken()));
					}

					sources.add(create(data, nColumns, rot, translation));

				} catch (NumberFormatException e) {
					// we ignore the line, it might be just comment text
				}

			} while (true);

			br.close();

			logger.info("Read poles from file: " + filename);

		} catch (IOException e) {
			logger.error("IO exception occurred with file: " + filename);
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

	}

	private PointSource create(double[] data, int nColumns, Rotation rot, Vector3D translation) {

		Vector3D loc = rot.applyTo(new Vector3D(data[0], data[1], data[2])).add(translation);

		if (nColumns == 6) {
			CurrentPointSource cs = new CurrentPointSource(loc);

			// rotate currents
			Vector3D ir = rot.applyTo(new Vector3D(data[3], data[4], data[5]));
			double ix = ir.getX();
			double iy = ir.getY();
			double iz = ir.getZ();
			cs.setLocalCurrentComponents(ix, iy, iz);
			return cs;

		} else {
			assert (nColumns == 4);
			return new ElectricMonopole(loc, data[3]);
		}

	}
}
