/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.utils.OcTreeEntry;
import simpa.core.b2a.SurfacePointImpl;

/**
 * Accessible interface for SurfacePoint. A SurfacePoint is a point on a surface
 * surrounding the region of interest where we want to represent the field.
 */
public interface SurfacePoint extends OcTreeEntry {

	/**
	 * Creates an instance of the SurfacePoint class.
	 *
	 * @param location location of the point.
	 * @param toSource direction of the source.
	 * @return implementation of the SurfacePoint class.
	 */
	static SurfacePoint createSurfacePoint(Vector3D location, Vector3D toSource) {
		return new SurfacePointImpl(location, toSource);
	}

	/**
	 * Gets the location of the SurfacePoint.
	 *
	 * @return Coordinates of the SurfacePoint
	 */
	Vector3D getLocation();

	/**
	 * Sets the B or E field of the SurfacePoint.
	 *
	 * @param field input field.
	 */
	void setField(Vector3D field);

	/**
	 * 
	 * @return The given field at this surface point location.
	 */
	Vector3D getField();

	/**
	 * Prints the source to a string.
	 *
	 * @return a string of the source.
	 */
	String sourceToString();

}
