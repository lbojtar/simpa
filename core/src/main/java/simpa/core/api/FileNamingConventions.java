/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * Class that contains the file naming conventions.
 */
public class FileNamingConventions {
	public static final String TEXT_FILE_EXTENSION = ".txt";
	public static final String OPERA_FILE_EXTENSION = ".table";
	public static final String BIN_FILE_EXTENSION = ".bin";
	public static final String FIELDMAP_STAT_FILE_EXTENSION = ".stat";
	// plain text output files
	public static final String CHECK_FILE_EXTENSION = "-FIELD-CHECK.txt";
	public static final String SOLUTION_FILE_EXTENSION = "-SOLUTION.txt";

	// output file extension depends on the field EM solver (Opera, etc.)
	public static final String POINTS_FILE_EXTENSION = "-POINTS";
	public static final String FIELD_FILE_EXTENSION = "-FIELD-AT-POINTS";
	public static final String FIELD_AT_CHECK_FILE_EXTENSION = "-FIELD-AT-CHECK-POINTS";
	public static final String CHECK_POINT_FILE_EXTENSION = "-CHECK-POINTS";

	// tracking result files
	public static final String PHASE_SPACE_FILE_EXTENSION = "-PHS";
	public static final String TRAJECTORY_FILE_EXTENSION = "-TRAJECTORY.txt";
	public static final String BEAM_TRAJECTORY_FILE_EXTENSION = "-BEAM-TRAJECTORY.txt";
	public static final String TL_BEAM_PHASE_SPACE_FILE_EXTENSION = "-TL-BEAM-PHS";
	public static final String BEAMLOSS_FILE_EXTENSION = "-LOST-PARTICLES.txt";

	//cover files
	public static final String SINGLE_ROW_COVER_FILE_EXTENSION = "-SINGLE-ROW-COVER.txt";	
	public static final String HCP_COVER_FILE_EXTENSION = "-HCP-COVER.txt";	
	/**
	 * Gets the file name for a file containing the lost particles of a beam.
	 * @param beamName The name of the beam.
	 * @return The filename
	 */
	public static String getBeamLossFileName(String beamName) {
		return beamName + BEAMLOSS_FILE_EXTENSION;
	}

	/**
	 * Gets the file name for phase space data of a beam tracked in a transfer line.
	 * This contains phase space data of all particles in the beam at the position
	 * of a {@link simpa.acc.api.track.TLPhaseSpaceObserver TLPhaseSpaceObserver} which wrote it.
	 * 
	 * @param beamName The name of the beam.
	 * @param longiPos The longitudinal position of the phase space observer.
	 * @return The filename
	 */
	public static String getTLBeamPhaseSpaceFileName(String beamName, double longiPos) {
		NumberFormat nFormat = NumberFormat.getInstance();
		nFormat.setMaximumFractionDigits(SystemConstants.OBSERVER_POSITION_DIGITS);
		return beamName + PHASE_SPACE_FILE_EXTENSION + "-AT_" + nFormat.format(longiPos) + ".txt";
	}

	/**
	 * Gets the file mane of the phase space file according to the parameters and
	 * the convention.
	 * 
	 * @param particleName The particle name.
	 * @param loniPos      The longitudinal position of the phase space observer.
	 * @param nFormat      Number format set set the number of fractional digits.
	 * @return The filename
	 */
	public static String getPhaseSpaceFileName(String particleName, double loniPos, NumberFormat nFormat) {

		return particleName + PHASE_SPACE_FILE_EXTENSION + "-AT_" + nFormat.format(loniPos) + ".txt";
	}

	/**
	 * Gets the file name for a file containing the trajectory coordinates in the
	 * global Cartesian system according to the conventions from the name of the
	 * particle.
	 * 
	 * @param particleName The name of the particle to be tracked.
	 * @return Trajectory file name
	 */
	public static String getTrajectoryFileName(String particleName) {
		return particleName + TRAJECTORY_FILE_EXTENSION;
	}

	/**
	 * Gets the file name for a file containing the trajectory coordinates of a beam
	 * in the global Cartesian system according to the conventions from the name of
	 * the particle.
	 * 
	 * @param beamName The name of the beam.
	 * @return Trajectory file name for the beam
	 */
	public static String getBeamTrajectoryFileName(String beamName) {
		return beamName + BEAM_TRAJECTORY_FILE_EXTENSION;
	}

	/**
	 * Generates the correct filename for a statistic file about the distrifution of
	 * maximum degrees in a field map.
	 *
	 * @param name filename.
	 * @return The correct name for the statistics file.
	 */
	public static String getFieldMapStatisticsFileName(String name) {
		return name + FIELDMAP_STAT_FILE_EXTENSION;
	}

	/**
	 * Generates the correct filename for a path file.
	 *
	 * @param name name for the file.
	 * @return The correct name for the path file.
	 */
	public static String getPathFileName(String name) {
		return name + "-PATH.txt";
	}

	/**
	 * Generates the correct extension for a point file.
	 *
	 * @param off The output file format.
	 * @return the correct extension for the points file.
	 */
	public static String getPointsFileExtension(OutputFileFormat off) {
		return POINTS_FILE_EXTENSION + getExtension(off);
	}

	/**
	 * Generates the correct extension for a checkpoints file.
	 *
	 * @return the correct extension for a checkpoints file.
	 */
	public static String getCheckPointsFileExtension() {
		return CHECK_POINT_FILE_EXTENSION + getExtension(OutputFileFormat.PLAINTEXT);
	}

	/**
	 * Generates the correct extension for a field file.
	 *
	 * @return the correct extension for a field file.
	 */
	public static String getFieldFileExtension() {
		return FIELD_FILE_EXTENSION + getExtension(OutputFileFormat.PLAINTEXT);
	}

	/**
	 * Generates the correct extension for a check file.
	 *
	 * @return the correct extension for a check file.
	 */
	public static String getCheckFileExtension() {
		return CHECK_FILE_EXTENSION;
	}

	/**
	 * Generates the correct extension for a field at checkpoints file.
	 *
	 * @return the correct extension for a field at checkpoints file.
	 */
	public static String getFieldAtCheckFileExtension() {
		return FIELD_AT_CHECK_FILE_EXTENSION + getExtension(OutputFileFormat.PLAINTEXT);
	}

	/**
	 * Generates the correct extension for a solution file.
	 *
	 * @return the correct extension for a solution file.
	 */
	public static String getSolutionFileExtension() {
		return SOLUTION_FILE_EXTENSION;
	}

	/**
	 * Generates the correct filename for a binary fieldmap file.
	 *
	 * @param name      filename.
	 * @param maxDegree max degree that will be appended to the filename.
	 * @return The correct name for the binary file.
	 */
	public static String addFieldMapExtension(String name, int maxDegree) {
		return name + "_" + maxDegree + BIN_FILE_EXTENSION;
	}

	/**
	 * Adds the underscore character plus the lmax (maximum degree of spherical
	 * harmonics expansion ) and the binary field map extension to each keys in the
	 * map. These should be the element names.
	 * 
	 * @param map  Map with element names as keys
	 * @param lmax maximum degree of the SH expansion used for producing the binary
	 *             file.
	 * @return A new map with element names+lmax +binary extension as keys. The
	 *         values are the same objects.
	 */
	public static Map<String, Double> addFieldMapExtension(Map<String, Double> map, int lmax) {
		Iterator<String> it = map.keySet().iterator();
		Map<String, Double> newMap = new HashMap<>();
		for (; it.hasNext();) {
			String fn = it.next();
			String bfn = fn.concat("_" + Integer.toString(lmax) + BIN_FILE_EXTENSION);
			newMap.put(bfn, map.get(fn));
		}

		return newMap;

	}

	private static String getExtension(OutputFileFormat off) {
		// if (off == off.OPERA3D)
		// return OPERA_FILE_EXTENTION;
		return TEXT_FILE_EXTENSION;
	}

}
