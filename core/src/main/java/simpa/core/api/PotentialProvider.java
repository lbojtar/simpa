/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.Aperture;

/**
 * Accessible interface for PotentialProviders.
 */
public interface PotentialProvider {

	/**
	 * Gets the vector potentials and gradients at a given point.
	 *
	 * @param r point to be evaluated
	 * @return the potentials and gradients for the given point.
	 * @throws OutOfApertureException thrown when the point is outside the volume.
	 * @throws KnownFatalException    when an internal error happens.
	 */
	double[][] getPotentialsWithDerivatives(Vector3D r) throws OutOfApertureException, KnownFatalException;

	/**
	 * Sets the aperture for the PotentialProvider.
	 *
	 * @param ap Aperture object.
	 */
	void setAperture(Aperture ap);

	/**
	 * Gets the aperture.
	 *
	 * @return Aperture object.
	 */
	Aperture getAperture();

	/*
	 * Creates a copy of this instance.It should be implemented a thread safe way.
	 * Every object inside this instance which has a state should be deep copied.
	 * For example an SHEvaluator must be deep copied, but the field map used by it
	 * don't have to be.
	 */
	public PotentialProvider copy();

}
