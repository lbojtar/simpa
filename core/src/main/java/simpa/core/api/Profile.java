/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.utils.FileUtils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Object that represents a profile of the shape of the beam region.
 */
public class Profile {

	private final List<Vector3D> points;
	private Vector3D origin = Vector3D.ZERO;
	private boolean defaultProfile = false;

	/**
	 * Creates a general profile in the X, Y plane from a set of points.
	 *
	 * @param points A list of points for the profile.
	 */
	private Profile(List<Vector3D> points) throws IllegalArgumentException {
		this.points = points;
	}

	/**
	 * Creates a general profile in the X, Y plane from a set of 3D points in the
	 * given file. The Z coordinate must be zero for all points.
	 *
	 * @param file Filename
	 * @throws FileNotFoundException when the file is not found
	 */
	public Profile(String file) throws FileNotFoundException {
		List<Vector3D> points = FileUtils.readVectors(file, LengthUnit.M);
		check2D(points);
		this.points = points;
	}

	/**
	 * Creates a rectangular profile in the X, Y plane. The first profile point is
	 * in the bottom left corner.
	 *
	 * @param xmin   Minimum X coordinate.
	 * @param xmax   Maximum X coordinate.
	 * @param ymin   Minimum Y coordinate.
	 * @param ymax   Maximum Y coordinate.
	 * @param xsteps Number of points in the profile in the X direction.
	 * @param ysteps Number of points in the profile in the Y direction.
	 */
	public Profile(double xmin, double xmax, double ymin, double ymax, int xsteps, int ysteps) {

		int n = 2 * xsteps + 2 * ysteps;
		points = new ArrayList<>();

		for (int i = 0; i < n; i++) {
			Vector3D r;
			double dx = (xmax - xmin) / xsteps;
			double dy = (ymax - ymin) / ysteps;

			if (i < ysteps) {
				double y = ymin + i * dy;
				r = new Vector3D(xmin, y, 0);
				points.add(r);
			} else if (i < ysteps + xsteps) {
				double x = dx * (i - ysteps) + xmin;
				r = new Vector3D(x, ymax, 0);
				points.add(r);
			} else if (i < 2 * ysteps + xsteps) {
				double y = ymax - (i - (ysteps + xsteps)) * dy;
				r = new Vector3D(xmax, y, 0);
				points.add(r);
			} else {
				double x = (xmax) - (dx * (i - (2 * ysteps + xsteps)));
				r = new Vector3D(x, ymin, 0);
				points.add(r);
			}
		}
	}

	/**
	 * Creates an ellipse profile in the X, Y plane with the following code:
	 *
	 * @param xRadius       Horizontal radius of the ellipse
	 * @param yRadius       Vertical radius of the ellipse
	 * @param steps         Number of points in the profile
	 * @param startingAngle To modify the staring angle where the profile starts. By
	 *                      default, it is zero and the first profile points starts
	 *                      at fi = -3*Math.PI/4 The retangular profile starts at
	 *                      the left bottom corner. We need to align the ellipse
	 *                      profile to start at the same angle that's why the -
	 *                      3*Math.PI/4
	 */
	public Profile(double xRadius, double yRadius, int steps, double startingAngle) {
		points = new ArrayList<>();
		double dphi = 2 * Math.PI / steps;
		// The rectangular profile starts at the left bottom corner. We need to align
		// the
		// ellipse profile to start at the same angle
		// that's why the - 3*2*Math.PI/8
		for (int i = 0; i < steps; i++) {
			double phi = i * dphi + startingAngle - 3 * Math.PI / 4;
			double x = xRadius * Math.sin(phi);
			double y = yRadius * Math.cos(phi);
			Vector3D r = new Vector3D(x, y, 0);
			points.add(r);
		}

	}

	/**
	 * Makes a copy of this profile with the given translation and rotation.
	 *
	 * @param translation distance to add
	 * @param rot         rotation to apply to profile
	 * @return a copy of the profile with the given translation and rotation
	 */
	public Profile getTransformedCopy(Vector3D translation, Rotation rot) {
		List<Vector3D> oriented = new ArrayList<>();

		for (Vector3D point : points) {
			Vector3D p = new Vector3D(point.getX(), point.getY(), point.getZ());
			Vector3D po = rot.applyTo(p).add(translation);
			oriented.add(po);
		}

		Profile transformed = new Profile(oriented);
		transformed.origin = rot.applyTo(origin).add(translation);
		return transformed;

	}

	/**
	 * Gets the list of points that make up the profile.
	 *
	 * @return The list of points.
	 */
	public List<Vector3D> getPoints() {
		return points;
	}

	/**
	 * Gets the origin of the profile.
	 *
	 * @return Vector3D of the origin.
	 */
	public Vector3D getOrigin() {
		return origin;
	}

	/**
	 * Returns the points in the profile as string for debugging. One point per line
	 */
	public String toString() {
		StringBuffer sb = new StringBuffer();
		for (Vector3D p : points) {
			sb.append(p.getX() + " " + p.getY() + " " + p.getZ() + System.lineSeparator());
		}
		return sb.toString();
	}

	/**
	 * 
	 * @return A flag indicating if this is a default profile for the beam region.
	 *         We use this property to detect overlapping vacuum chambers.
	 */
	public boolean isDefaultProfile() {
		return defaultProfile;
	}

	public void setDefaultProfile(boolean defaultProfile) {
		this.defaultProfile = defaultProfile;
	} 
	
	private void check2D(List<Vector3D> points) throws IllegalArgumentException {
		points.forEach(p -> {
			if (p.getZ() != 0)
				throw new IllegalArgumentException("The points of the profile must be in the X Y plane !");
		});

	}



}
