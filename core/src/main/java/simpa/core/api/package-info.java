/**
 * This package contains all public interfaces and classes that can be programmed to.
 */
package simpa.core.api;