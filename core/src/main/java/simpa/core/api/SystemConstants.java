/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import java.io.File;

import simpa.core.api.track.BeamTrajectoryObserver;
import simpa.core.api.track.ObserverDisk;
import simpa.core.api.track.StlAperture;
import simpa.core.api.utils.TextStlReader;

/**
 * Constants used internally. It can be changed by the user, but be careful !
 */
public class SystemConstants {

	/**
	 * The number of fractional digits in longitudinal position for the phase space
	 * observer file.
	 */
	public static final int OBSERVER_POSITION_DIGITS = 3;

	/**
	 * Used for Plane Line intersection calculation by {@link ObserverDisk}
	 */
	public static final double PLANE_AND_LINE_TOLERANCE = 1E-12;

	/**
	 * Maximum relative error allowed for FMM3D. If one wants to have better than
	 * this at the solid harmonics expansion, this this value should be lowered. ( a
	 * rather unlikely scenario)
	 */
	public static final double FMM3D__RELATIVE_PRECISION = 1E-13;

	/**
	 * Tolerance in [m] below which points are considered identical. It is user for
	 * {@link StlAperture} and {@link TextStlReader}
	 */
	public static final double GEOMETRY_TOLERANCE = 1E-6;

	/**
	 * TMP directory of the operating system
	 */
	public static final String TMP_DIR = System.getProperty("java.io.tmpdir") + File.separator;

	/**
	 * Resource path for test files
	 */
	public static final String TEST_RESOURCE_PATH = "/tests/";

	/**
	 * String used by {@link BeamTrajectoryObserver} to separate particle data in
	 * the output file.
	 */
	public static final String PARTICLE_SEPARATOR_STRING = "THIS_IS_A_PARTICLE_SEPARATOR_STRING_DO_NOT_MODIFY_IT!!!";

}
