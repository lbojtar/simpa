package simpa.core.api;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.Quadrature1D;
import simpa.core.b2a.Wire;

/**
 * Current loop. It can be used to create field maps if the distribution of the
 * currents are known, for example a solenoid coil without iron.
 */

public class CurrentLoop {

	private double current;
	private Wire[] wires;
	private ArrayList<Vector3D> loopPoints;

	/**
	 * Constructor for arbitrary shape 3D current loop given by the vertices of the
	 * loop.
	 * 
	 * @param loopPoints List of vertices of the loop.
	 * @param current    Current of the loop.
	 */
	public CurrentLoop(ArrayList<Vector3D> loopPoints, double current) {
		createFromPoints(loopPoints, current);
		this.loopPoints = loopPoints;
	}

	/**
	 * Constructs a circular loop lying in the X Y plane centered (0,0,0).
	 * 
	 * @param radius Radius of the loop.
	 * @param nPoints Number of vertices in the loop.
	 * @param current Current of the loop.
	 */
	public CurrentLoop(double radius, int nPoints, double current) {
		
		ArrayList<Vector3D> l = new ArrayList<>();
		double dfi = 2 * Math.PI / nPoints;
		for (int i = 0; i < nPoints; i++) {
			double x = radius*Math.sin(i * dfi);
			double y = radius*Math.cos(i * dfi);
			Vector3D p = new Vector3D(x, y, 0);
			l.add(p);
		}
		createFromPoints(l, current);
	}

	/**
	 * Get a copy with the given transformations applied.
	 * 
	 * @param loop        The loop to be transformed.
	 * @param rot         Rotation to be applied before the translation
	 * @param translation Translation applied after the rotation.
	 * @return The transformed copy of the original loop.
	 */
	public static CurrentLoop transform(CurrentLoop loop, Rotation rot, Vector3D translation) {
		Wire[] wires = loop.getWires();
		CurrentLoop c = new CurrentLoop();
		c.wires = new Wire[wires.length];

		for (int i = 0; i < wires.length; i++) {
			Wire w = wires[i];
			Wire nw = new Wire(w.getStart(), w.getEnd(), loop.getCurrent());
			nw.rotateAndTranslate(rot, translation);
			c.wires[i] = nw;
		}

		c.setCurrent(loop.getCurrent());

		return c;

	}

	/**
	 * 
	 * @return The current of the loop.
	 */
	public double getCurrent() {
		return current;
	}

	/**
	 * 
	 * @param current The current of the loop.
	 */
	public void setCurrent(double current) {
		this.current = current;
		for (int i = 0; i < wires.length; i++) {
			wires[i].setCurrent(current);
		}
	}

	/**
	 * This method gives a list of CurrentPointSources approximating the wire
	 * segments in the current loop. We use the approximation for the solid harmonic
	 * expansion instead of the exact formula, because it allows to use the FMM
	 * method for summation and we can treat all sources the same way, it is also
	 * much faster for a large number of evaluation points.
	 * 
	 * @param quadrature   Quadrature to be used for the approximation of a
	 *                     subdivision of the wires segment of the loop.
	 * @param subdivisions If bigger than 1, each wire will be split into
	 *                     subdivisions. This increases the precision of the
	 *                     calculation.
	 * @return List of current point sources.
	 */
	public List<PointSource> getPointSources(Quadrature1D quadrature, int subdivisions) {
		if (subdivisions < 1)
			throw new IllegalArgumentException("The number of subdivisions for a wire can't be smaller than 1.");
		List<PointSource> l = new ArrayList<>();

		for (Wire w : wires) {
			l.addAll(w.getPointSources(quadrature, subdivisions));
		}

		return l;
	}

	/**
	 * 
	 * @return List of vertices of the loop.
	 */
	public ArrayList<Vector3D> getLoopPoints() {
		return loopPoints;
	}

	/**
	 * Write the the current point sources approximating this current loop.
	 * 
	 * @param file         The file to be written.
	 * @param quadrature   Quadratura points for each wire segment of the loop. The
	 *                     current point sources are placed to these points.
	 * @param subdivisions To increase the precision of the approximation, the wire
	 *                     segments can be sub-divided. Usually 1..5 is enough.
	 */
	public void writeToFile(String file, Quadrature1D quadrature, int subdivisions) {
		StringBuffer sb = new StringBuffer();
		for (Wire w : wires) {
			for (CurrentPointSource cps : w.getPointSources(quadrature, subdivisions)) {
				sb.append(cps.toString() + System.lineSeparator());
			}

		}

		FileUtils.writeTextFile(file, sb.toString());
	}

	protected Wire[] getWires() {
		return wires;
	}

	private CurrentLoop() {
	}

	private void createFromPoints(ArrayList<Vector3D> lp, double curr) {

		this.current = curr;

		int n = lp.size();
		wires = new Wire[n];
		try {
			for (int i = 0; i < n - 1; i++) {
				wires[i] = new Wire(lp.get(i), lp.get(i + 1), current);
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}

		wires[n - 1] = new Wire(lp.get(n - 1), lp.get(0), current);

	}

}
