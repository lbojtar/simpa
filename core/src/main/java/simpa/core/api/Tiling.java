/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import java.io.FileNotFoundException;
import java.text.NumberFormat;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.exceptions.NotSetException;
import simpa.core.api.exceptions.PositionMismatchException;
import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.PathUtils;
import simpa.core.impl.TilingImpl;

/**
 * The tiling for a machine element.
 */
public class Tiling {

	private final TilingImpl tiling;
	private final TilingConfig pars;
	protected BoundarySurface boundary;

	/**
	 * Constructs the tiling for a machine element with the given tiling config.
	 *
	 * @param pars Tiling config.
	 * @throws NotSetException When a required parameter of the tiling config is not
	 *                         set.
	 */
	public Tiling(TilingConfig pars) throws NotSetException {
		this.pars = pars;
		tiling = new TilingImpl(pars);
		boundary = new BoundarySurface(tiling.getSurfacePoints());
	}

	/**
	 * Writes a file with the coordinates of the surface points on the boundary of
	 * the accelerator element.
	 * 
	 * @throws NotSetException When some necessary parameter is not set.
	 */
	public void writeSurfacePoints() throws NotSetException {
		boundary.surfacePointCoordinatesToFile(pars.getName(), pars.getLengthUnit(), pars.getOutputFileFormat());
	}

	/**
	 * Writes checkpoints to a file. The points are derived from the path which was
	 * used to create the element by up-sampling it.
	 * 
	 * @param translation A translation vector to be added to each point of the path file to create the output check file.
	 * @param stepSize Step size for the up-sampling
	 * @param extension If it is non zero, the path will be extended at both ends with the given value.
	 * 
	 * @throws FileNotFoundException
	 * @throws NotSetException
	 */
	public void writeCheckPoints(Vector3D translation,double stepSize,double extension) throws FileNotFoundException, NotSetException {

		List<Vector3D> path = FileUtils.readVectors(FileNamingConventions.getPathFileName(pars.getName()), 0,1.0);		
		path = PathUtils.transform(path, Rotation.IDENTITY, translation);
		PathUtils.extendPath(path, extension, false); //extend at the beginning
		PathUtils.extendPath(path, extension, true); //extend at the end
		
		List<Vector3D>	checkPoints =	PathUtils.upSamplePath( path,stepSize);
				
		String cfn = pars.getName() + FileNamingConventions.getCheckPointsFileExtension();

		StringBuilder checkSb = new StringBuilder();

		NumberFormat nf = NumberFormat.getInstance();
		nf.setGroupingUsed(false);
		nf.setMaximumFractionDigits(6);
		if (pars.getOutputFileFormat() == OutputFileFormat.OPERA3D) {
			checkSb.append(OutputFileFormat.getOpera3DHeader(checkPoints.size(), pars.getLengthUnit()));
		}

		for (Vector3D r : checkPoints) {

			double lsf = pars.getLengthUnit().getFactor();

			String pos = nf.format(r.getX() * lsf) + " " + nf.format(r.getY() * lsf) + " " + nf.format(r.getZ() * lsf);

			checkSb.append(pos).append(System.lineSeparator());

		}
		FileUtils.writeTextFile(cfn, checkSb.toString());
	}

	/**
	 * Solves the boundary problem with the allowed relative error. It calculates
	 * the strengths of the sources such that they reproduce the given field on the
	 * boundary surface with the surface points of the tiles with the error margin.
	 *
	 * @param relativeError The allowed relative error.
	 * @return The relative residual.
	 * @throws NotSetException           When a required parameter of the tiling
	 *                                   config is not set.
	 * @throws PositionMismatchException when a point is not at the expected place.
	 */
	public double solve(double relativeError, SolverOptions opt) throws NotSetException, PositionMismatchException {

		String fileName = pars.getName() + FileNamingConventions.getFieldFileExtension();

		boundary.setFieldFromFile(fileName, 1 / pars.getFieldScalingFactor(), pars.getLengthUnit());
		double residual = Double.POSITIVE_INFINITY;
		Solver solver;
		if (opt.getType() == SolverOptions.SolverType.DIRECTSOLVER) {
			solver = new Solver(tiling.getSurfacePoints(), pars.getFieldType());
		} else {
			if (opt.getPreconditioner() == SolverOptions.Preconditioner.SLICE) {
				// for the slice preconditioner we have a different constructor,since some
				// additional info about the slicing is needed which is special to accelerators
				solver = new Solver(pars.getFieldType(), tiling.getSlices(), opt);
			} else {
				solver = new Solver(tiling.getSurfacePoints(), pars.getFieldType(), opt);
			}
		}

		solver.solve(relativeError);
		residual = solver.getResidual();

		String solFile = pars.getName() + FileNamingConventions.getSolutionFileExtension();
		boundary.sourcesToFile(solFile);
		return residual;
	}

}
