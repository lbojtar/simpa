package simpa.core.api;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.sh.SHField;
import simpa.core.sh.SHFieldEvaluatorImpl;

/**
 * This class represents an electro-magnetic field. It provides methods to read
 * from and write to a file. Also provides method to get evaluators for it.
 */
public class FieldMap {

	private SHField shField;
	private static final Logger logger = LogManager.getLogger(FieldMap.class);

	public static FieldMap createFromFile(String fileName, double scaling) throws IOException {

		logger.info("Reading fieldmap from file: " + fileName);
		SHField shf = SHField.deserializeSHField(fileName);
		return new FieldMap(shf);

	}

	/**
	 * Serialize this fieldmap to a binary file.
	 * 
	 * @param fileName
	 * @throws IOException
	 */
	public void writeToFile(String fileName) throws IOException {
		shField.serializeToFile(fileName);
		logger.info("Wrote fieldmap to file: " + fileName);
	}

	/**
	 * Create a new PotentialProvider from the given FieldMap. This is typically
	 * used when tracking particles in many threads, but we still want to use a
	 * single FieldMap object, since it can be very big in memory. Each thread must
	 * have a separate PotentialProvider, but the underlying FieldMap can be common.
	 * 
	 * @param fm A FieldMap object
	 * @return A PotentialProvider.
	 */
	public static PotentialProvider createEvaluator(FieldMap fm) {
		return new SHFieldEvaluatorImpl(fm.shField, 1.0);
	}

	// API ends here
	protected FieldMap(SHField shField) {
		this.shField = shField;
	}

}
