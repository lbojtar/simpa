package simpa.core.api;

/**
 * Enum to specify the quadratura to be used for tiles on the boundary surface.
 */
public enum Quadrature {
	QUADRILATERAL_4_POINTS, TRIANGLE_3_POINTS
}
