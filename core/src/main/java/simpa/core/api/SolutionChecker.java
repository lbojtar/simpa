/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import java.io.FileNotFoundException;

import simpa.core.b2a.SolutionCheckerImpl;

/**
 * Class that can be used to evaluate the precision of the solution.
 */
public class SolutionChecker {

	private final SolutionCheckerImpl impl;

	/**
	 * Creates a new implementation for SolutionChecker.
	 *
	 * @param elementName name of the element.
	 * @param fieldType type of field.
	 * @param lengthUnit unit for length.
	 */
	public SolutionChecker(String elementName, FieldType fieldType, LengthUnit lengthUnit) {
		impl = new SolutionCheckerImpl(elementName,fieldType,lengthUnit);
	}

	/**
	 * Gets the normalized RMS (root mean square) error for the element using the given scaling factor.
	 *
	 * @param scalingFactor scaling factor.
	 * @return the normalized RMS error.
	 * @throws FileNotFoundException when the file is not found
	 */
	public double getNormalizedRmsError(double scalingFactor) throws FileNotFoundException {
		return impl.getNormalizedRmsError(scalingFactor);
	}

	
}
