/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.exceptions.NotSetException;

import java.util.List;

/**
 * Object that holds the configuration for tiling.
 */
public class TilingConfig {

	private String name;
	private List<Vector3D> path;
	private LengthUnit lengthUnit;
	private double elevation;
	private OutputFileFormat outputFileFormat;
	private double fieldScalingFactor;
	private FieldType fieldType;
	private final Profile profile;
	private Quadrature quadrature= Quadrature.QUADRILATERAL_4_POINTS; //default

	/**
	 * Construct a config for tiling.
	 *
	 * @param name               Name of the tiling.
	 * @param profile            Profile to use in the tiling.
	 * @param path               Path around which to construct the tiling.
	 * @param lengthUnit         LengthUnit of the path.
	 * @param elevation          Elevation of the element.
	 * @param outputFileFormat   The format of the output file.
	 * @param fieldScalingFactor The scaling factor for the field.
	 * @param fieldType          The type of field in the element.
	 */
	public TilingConfig(String name, Profile profile, List<Vector3D> path, LengthUnit lengthUnit, double elevation,
			OutputFileFormat outputFileFormat, double fieldScalingFactor, FieldType fieldType) {
		this.name = name;
		this.profile = profile;
		this.path = path;
		this.lengthUnit = lengthUnit;
		this.elevation = elevation;
		this.outputFileFormat = outputFileFormat;
		this.fieldScalingFactor = fieldScalingFactor;
		this.fieldType = fieldType;
	}

	/**
	 * Gets the name of the magnet. All files created during the tiling will be
	 * named with this name and some extension according to the content of the file.
	 * 
	 * @return name of the magnet
	 * @throws NotSetException when the name of the magnet is not yet set
	 */
	public String getName() throws NotSetException {
		if (name == null)
			throw new NotSetException("The name parameter is not set!");
		return name;
	}

	/**
	 * 
	 * @return Profile which is extruded along the path to make a surface.
	 * @throws NotSetException when the profile is not set
	 */
	public Profile getProfile() throws NotSetException {
		if (profile == null)
			throw new NotSetException("The profile parameter is not set!");
		return profile;
	}

	/**
	 * Sets the name of the magnet. All files created during the tiling will be
	 * named with this name and some extension according to the content of the file.
	 * 
	 * @param name name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the path in [meters]. The boundary surface of the magnet to be tiled is
	 * an extrusion of a rectangular profile along the curve.
	 * 
	 * @return -path
	 * @throws NotSetException when the path is not yet set
	 */
	public List<Vector3D> getPath() throws NotSetException {
		if (path == null)
			throw new NotSetException("The curve file name parameter is not set!");
		return path;
	}

	/**
	 * Sets the path. It should be given in [meters]. The boundary surface of the
	 * magnet to be tiled is an extrusion of a rectangular profile along the curve.
	 * 
	 * @param path list of vectors
	 */
	public void setPath(List<Vector3D> path) {
		this.path = path;
	}

	/**
	 * Gets the length unit to be used for the diagnostic files.
	 * 
	 * @return Length unit.
	 * @throws NotSetException when the length unit is not yet set.
	 */
	public LengthUnit getLengthUnit() throws NotSetException {
		if (lengthUnit == null)
			throw new NotSetException("The length unit parameter is not set!");
		return lengthUnit;
	}

	/**
	 * Sets the length unit to be used for the diagnostic files.
	 *
	 * @param lengthUnit The length unit.
	 */
	public void setLengthUnit(LengthUnit lengthUnit) {
		this.lengthUnit = lengthUnit;
	}

	/**
	 * Gets the elevation of the surface points. This determines what distance the
	 * source monopoles will be placed from the tile.
	 * 
	 * @return the elevation
	 */
	public double getElevation() {
		return elevation;
	}

	/**
	 * Sets the elevation of the surface points. This determines what distance the
	 * source monopoles will be placed from the tile.
	 * 
	 * @param elevation elevation of the surface points
	 */
	public void setElevation(double elevation) {
		this.elevation = elevation;
	}

	/**
	 * Gets the output file format used in this TilingConfig.
	 *
	 * @return output file format.
	 */
	public OutputFileFormat getOutputFileFormat() {
		return outputFileFormat;
	}

	/**
	 * Method used for setting or updating the outputFileFormat used in
	 * {@link Tiling} objects implementing this TilingConfig.
	 *
	 * @param outputFileFormat output file format.
	 */
	public void setOutputFileFormat(OutputFileFormat outputFileFormat) {
		this.outputFileFormat = outputFileFormat;
	}

	/**
	 * Gets the type of field used in this tiling config.
	 *
	 * @return the type of field.
	 */
	public FieldType getFieldType() {
		return fieldType;
	}

	/**
	 * Method used for setting or updating the field type used in {@link Tiling}
	 * objects implementing this TilingConfig.
	 *
	 * @param fieldType the type of field.
	 */
	public void setFieldType(FieldType fieldType) {
		this.fieldType = fieldType;
	}

	/**
	 * Gets the scaling factor of the field in this tiling config.
	 *
	 * @return scaling factor.
	 */
	public double getFieldScalingFactor() {
		return fieldScalingFactor;
	}

	/**
	 * Method used for setting or updating the scaling factor used in {@link Tiling}
	 * objects implementing this TilingConfig.
	 * 
	 * @param fieldScalingFactor the scaling factor to update to.
	 */
	public void setFieldScalingFactor(double fieldScalingFactor) {
		this.fieldScalingFactor = fieldScalingFactor;
	}

	/**
	 * 
	 * @return The type of quadrature to be used for the tiling. The
	 *         {@link SurfacePoint} instances will be placed on the quadratura
	 *         points on the surface tiles.
	 */
	public Quadrature getQuadrature() {
		return quadrature;
	}

	/**
	 * Sets the type of quadrature to be used for the tiling.
	 * 
	 * @param quadrature
	 */
	public void setQuadrature(Quadrature quadrature) {
		this.quadrature = quadrature;
	}

}
