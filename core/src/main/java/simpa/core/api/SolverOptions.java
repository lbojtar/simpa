package simpa.core.api;

public class SolverOptions {
	
	private SolverType type;
	private Preconditioner preconditioner;
	private double aggregateRadius=-1; //no default values, if not set IllegalArgumentException will be thrown
	private double patchRadius=-1;     //no default values, if not set IllegalArgumentException will be thrown
	private int sliceWidth=1;
	private int sliceOverlap=0;
	private int gmresRestart =100;
	private boolean debug;
	
	public enum SolverType {
		GMRES, DIRECTSOLVER
	}
	
	public	enum Preconditioner{
		JACOBI,BLOCKJACOBI,SLICE,NONE
	}
	
	public SolverOptions(SolverType type,Preconditioner precond ) {
		this.type=type;
		this.preconditioner=precond;
	}

	public SolverType getType() {
		return type;
	}

	public Preconditioner getPreconditioner() {
		return preconditioner;
	}

	public double getAggregateRadius() {
		return aggregateRadius;
	}

	public void setAggregateRadius(double aggregateRadius) {
		this.aggregateRadius = aggregateRadius;
	}

	public double getPatchRadius() {
		return patchRadius;
	}

	public void setPatchRadius(double patchRadius) {
		this.patchRadius = patchRadius;
	}

	public int getSliceOverlap() {
		return sliceOverlap;
	}

	public void setSliceOverlap(int sliceOverlap) {
		this.sliceOverlap = sliceOverlap;
	}

	public int getSliceWidth() {
		return sliceWidth;
	}

	public void setSliceWidth(int sliceWidth) {
		this.sliceWidth = sliceWidth;
	}

	public int getGmresRestart() {
		return gmresRestart;
	}

	public void setGmresRestart(int gmresRestart) {
		this.gmresRestart = gmresRestart;
	}

	public boolean isDebug() {
		return debug;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	
}
