/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.utils.FileUtils;
import simpa.core.api.utils.PathUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Represents a surface extruded along a path. The resulting surface is open at
 * the ends.
 */
public class ExtrudedSurface {

	private List<Profile> orientedProfiles;
	private Vector3D referencePoint = Vector3D.ZERO;

	/**
	 * This constructor is used to create a surface by extruding the given profile
	 * along the path. Used for tiling. 
	 *
	 * @param path    A path to extrude the profile to form a surface. At each point
	 *                in the path a profile will be added such that the path segment
	 *                is normal to the plane of the profile. Normally the point
	 *                (0,0,0) should be included in the path and for symmetric
	 *                surfaces it should be the center point. This is just a
	 *                convention to make things easier, but not a requirement.
	 * @param profile A profile to be extruded. This is a 2D closed polygon in the
	 *                XY plane.
	 */
	public ExtrudedSurface(List<Vector3D> path, Profile profile) {
		init(path, profile);
	}

	/**
	 * This constructor is used to create a surface by placing the given profiles to
	 * the given positions along the path.
	 *
	 * @param path     A path to place the profiles to form a surface. The point
	 *                 (0,0,0) should be in the path.
	 * 
	 * @param profiles A map profiles to be placed.The key is the distance from the
	 *                 origin along the path. All profiles must have the same number
	 *                 of points. This is required for easy triangulation.
	 * 
	 */
	public ExtrudedSurface(List<Vector3D> path, Map<Double, Profile> profiles) {
		
		if (!PathUtils.hasPoint(path, referencePoint, SystemConstants.GEOMETRY_TOLERANCE))
			throw new IllegalArgumentException("The path must contain the origin (0,0,0)");
		
		// check the number of points in the profiles are the same
		int n = -1;
		for (Double d : profiles.keySet()) {
			Profile p = profiles.get(d);
			if (n == -1) {
				n = p.getPoints().size();
			} else {
				if (n != p.getPoints().size())
					throw new IllegalArgumentException("All profiles must have the same muber of points");
			}
		}

		List<Double> distances = profiles.keySet().stream().sorted().toList();
		orientedProfiles = new ArrayList<>();

		for (Double d : distances) {
			Profile profile = profiles.get(d);
			TangentVector3D tv = getTangentVectorOnPath(path, d, Vector3D.ZERO);
			Vector3D profileCenter = tv.location();
			Vector3D normal = tv.direction();
			// System.out.println(d+" "+profileCenter+" "+normal);
			Rotation rot = new Rotation(Vector3D.PLUS_K, Vector3D.PLUS_J, normal, Vector3D.PLUS_J);
			orientedProfiles.add(profile.getTransformedCopy(profileCenter, rot));
		}
	}

	/**
	 * This is the reference coordinate of the surface. When the surface is created
	 * the path must contain the (0,0,0) point, which by definition becomes the
	 * reference point. When the copy constructor with translation and rotation is
	 * called the translation is added to the reference point.
	 *
	 * @return The reference point.
	 */
	public Vector3D getReferencePoint() {
		return referencePoint;
	}

	/**
	 * Gets the oriented profiles of the surface.
	 *
	 * @return the oriented profiles.
	 */
	public List<Profile> getOrientedProfiles() {
		return orientedProfiles;
	}

	/**
	 * Writes the surface points to a file for testing.
	 *
	 * @param file output file to write to.
	 */
	protected void toFile(String file) {
		StringBuilder sb = new StringBuilder();
		sb.append("#X Y Z").append(System.lineSeparator());
		for (Profile pr : orientedProfiles) {
			for (int j = 0; j < pr.getPoints().size(); j++) {
				Vector3D p = pr.getPoints().get(j);
				sb.append(p.getX()).append(" ").append(p.getY()).append(" ").append(p.getZ())
						.append(System.lineSeparator());
			}
		}
		FileUtils.writeTextFile(file, sb.toString());
	}

	private void init(List<Vector3D> path, Profile profile) {
		orientedProfiles = new ArrayList<>();

		for (int i = 0; i < path.size(); i++) {
			Vector3D center = path.get(i);
			Vector3D normal = getNormal(path, i);
			Rotation rot = new Rotation(Vector3D.PLUS_K, Vector3D.PLUS_J, normal, Vector3D.PLUS_J);
			orientedProfiles.add(profile.getTransformedCopy(center, rot));
		}
	}

	/**
	 * calculates the tangential vector a the given index of the path. This is used
	 * to orient a profile
	 *
	 * @param path      a path
	 * @param pathIndex Index of the path point which path will be normal to the
	 *                  profile after it is rotated.
	 * @return Normal vector to the plane of the oriented profile.
	 */
	private Vector3D getNormal(List<Vector3D> path, int pathIndex) {

		Vector3D center = path.get(pathIndex);
		Vector3D normal;

		// normal vector to the tile
		if (pathIndex < path.size() - 1) {
			normal = path.get(pathIndex + 1).subtract(center);
		} else {
			normal = path.get(pathIndex).subtract(path.get(pathIndex - 1));
		}
		return normal;
	}

	/**
	 * Gets a tangent vector (which consists of a position vector and a direction
	 * vector) on the path which is at a given distance from a reference.
	 *
	 * @param path the points that make up the path.
	 * @param dist The distance.
	 * @param from the point to look from.
	 * @return Tangent vector (position and direction vectors).
	 */
	// TODO: The distance should be counted along the path not in a straight line !!
	private TangentVector3D getTangentVectorOnPath(List<Vector3D> path, double dist, Vector3D from) {

		Vector3D start = null;
		Vector3D end = null;

		if (dist >= 0) {
			for (int i = path.size() - 1; i > 0; i--) {
				start = path.get(i - 1);
				if (start.distance(from) <= dist) {
					end = path.get(i);
					break;
				}
			}
		} else {
			for (int i = 1; i < path.size(); i++) {
				start = path.get(i - 1);
				if (start.distance(from) <= -dist) {
					end = path.get(i);
					break;
				}
			}
		}

		double remainder;
		// this can happen when the distance is zero
		if (end == null) {
			int si = PathUtils.getClosestIndex(path, from);
			start = path.get(si);
			end = path.get(si + 1);
			remainder = 0;
		} else {
			remainder = Math.abs(dist) - from.distance(start);
		}

		Vector3D tangent = end.subtract(start).normalize();
		if (dist < 0)
			remainder = -remainder;
		Vector3D p = tangent.scalarMultiply(remainder).add(start);
		return new TangentVector3D(p, tangent);
	}

}
