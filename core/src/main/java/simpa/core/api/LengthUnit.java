/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Enum for LengthUnit and their factor.
 */
public enum LengthUnit {

	/**
	 * Millimeter.
	 */
	MM(1000.0),
	/**
	 * Centimeter.
	 */
	CM(100.0),
	/**
	 * Meter.
	 */
	M(1.0);

	private static final Logger logger = LogManager.getLogger(LengthUnit.class);

	// public static LengthUnit lengthUnit=METER; //by default

	private final double factor;

	LengthUnit(double f) {
		this.factor = f;
	}

	/**
	 * Parses the length unit as string to the correct enum.
	 *
	 * @param s string version of the length unit (mm, cm, m)
	 * @return the enum for the length unit
	 */
	public static LengthUnit parse(String s) {
		
		if (s == null)
			return M;
		
		if (s.equals("cm") || s.equals("CM"))
			return CM;
		if (s.equals("mm") || s.equals("MM"))
			return MM;
		if (s.equals("m") || s.equals("M"))
			return M;

		logger.error(s + " isn't an allowed length unit.");

		return null;
	}

	/**
	 * Multiplying by this factor converts it to meters
	 * 
	 * @return factor
	 */
	public double getFactor() {
		return factor;
	}

}
