/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Enum for some output files. These files are usually taken as an input for
 * some electromagnetic field calculation software like Opera or COMSOL, etc. as
 * input file to get out the field values at the specified points.
 */
public enum OutputFileFormat {

	/**
	 * An enum to categorize a plaintext file.
	 */
	PLAINTEXT,
	/**
	 * An enum to categorize an Opera3D file.
	 */
	OPERA3D;

	private static final Logger logger = LogManager.getLogger(OutputFileFormat.class);

	/**
	 * Gets a header specific for use of a file in Opera3D.
	 *
	 * @param nPoints    The number of points.
	 * @param lengthUnit The length unit.
	 * @return The generated header as a String.
	 */
	public static String getOpera3DHeader(int nPoints, LengthUnit lengthUnit) {

		String unit = "[" + lengthUnit.toString() + "]";

		return nPoints + "         1           1           2" + System.lineSeparator() + "1 X " + unit
				+ System.lineSeparator() + "2 Y " + unit + System.lineSeparator() + "3 Z " + unit
				+ System.lineSeparator() + "0" + System.lineSeparator();
	}

	/**
	 * Parses the  string to the correct enum.
	 *
	 * @param s string version of the output format
	 * @return the enum for the output format
	 */
	public static OutputFileFormat parse(String s) {

		if (s == null)
			return OPERA3D;

		if (s.equals("txt") || s.equals("TXT"))
			return PLAINTEXT;

		logger.error(s + " isn't an allowed output format.");

		return null;
	}
}
