/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api;

import java.io.IOException;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.nativ.FMM3DEvaluator;
import simpa.core.sh.SHFieldEvaluatorImpl;

/**
 * Factory for field map evaluators.
 */
public class Evaluator {

	private FMM3DEvaluator ev;

	/**
	 * Creates a PotentialProvider of type SHFieldEvaluator for evaluating field
	 * maps. The returned PotentialProvider is suitable for tracking.
	 *
	 * @param binaryFile input file
	 * @param scaling    scaling of the input
	 * @return a PotentialProvider for evaluation of field maps
	 */
	public static PotentialProvider createFieldMapEvaluator(String binaryFile, double scaling) throws IOException {
		return new SHFieldEvaluatorImpl(binaryFile, scaling);
	}

	/**
	 * Evaluator using the fast multipole method. This is for evaluate the field of
	 * many point sources on many target points, but not suitable for tracking.
	 * 
	 * @param sources List of point sources.
	 * @param targets List of points to be evaluated at.
	 * @return An 3D array of potential (Three vector potential and one scalar
	 *         potential) and derivatives. The layout of the array is the following:
	 *         [targetIndex][potentialIndex][derivativeIndex]
	 */
	public double[][][] evaluateAtTargets(List<? extends PointSource> sources, List<Vector3D> targets) {
		if (ev == null)
			ev = new FMM3DEvaluator();
		
		return ev.getPotentialsAndGradientsAtAllTargets(sources, targets);
	}
}
