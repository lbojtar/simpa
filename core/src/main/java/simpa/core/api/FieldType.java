/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api;

/**
 * Enum for the type of field.
 */
public enum FieldType {
	/**
	 * Enum for static magnetic field types
	 */
	STATIC_MAGNETIC,
	/**
	 * Enum for static electric field types
	 */
	STATIC_ELECTRIC;

}
