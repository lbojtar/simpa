/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simpa.core.api.FileNamingConventions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * Class for transforming solution files with a given rotation and/or
 * translation.
 */
public class SolutionTransformer {
	private static final Logger logger = LogManager.getLogger(SolutionTransformer.class);

	/**
	 * Rotates then translates the position and orientation of the sources (for
	 * current sources, electric monopoles are only translated) in a solution file
	 * corresponding to the accelerator element with the given name.
	 *
	 * @param name          Name of the accelerator element.
	 * @param rot           A rotation to be applied, can be null.
	 * @param inverseRot    If true it applies the inverse of the rotation.
	 * @param translation   A translation to be applied after the rotation, can be
	 *                      null.
	 * @param inverseTransl If true it applies the inverse of the translation.
	 * @param outName       Output file name.  The original solution file is kept and
	 *                      the transformed solution file will be written with his name.
	 * 
	 * @throws IOException When the generated input file could not be found.
	 */
	public static void transform(String name, Rotation rot, boolean inverseRot, Vector3D translation,
			boolean inverseTransl, String outName) throws IOException {
		if (rot == null)
			rot = Rotation.IDENTITY;
		if (translation == null)
			translation = Vector3D.ZERO;

		if (inverseRot)
			rot = Rotation.IDENTITY.composeInverse(rot, RotationConvention.VECTOR_OPERATOR);
		if (inverseTransl)
			translation = translation.scalarMultiply(-1.0);

		String filename = name + FileNamingConventions.getSolutionFileExtension();
		String outFileName = outName + FileNamingConventions.getSolutionFileExtension();


		int ncol = FileUtils.countNumberOfColumns(filename);
		double[][] da2d = FileUtils.read2DArray(filename, ncol);
		double[][] t2d = new double[da2d.length][ncol];

		for (int i = 0; i < da2d.length; i++) {

			double[] da = da2d[i];
			Vector3D pos = new Vector3D(da[0], da[1], da[2]);
			Vector3D p = rot.applyTo(pos).add(translation);
			t2d[i][0] = p.getX();
			t2d[i][1] = p.getY();
			t2d[i][2] = p.getZ();

			if (ncol == 4) { // static electric case
				t2d[i][3] = da[3]; // copy the charge
			} else if (ncol == 6) { // static magnetic case, position and currents
				Vector3D curr = new Vector3D(da[3], da[4], da[5]);
				Vector3D tcurr = rot.applyTo(curr);
				t2d[i][3] = tcurr.getX();
				t2d[i][4] = tcurr.getY();
				t2d[i][5] = tcurr.getZ();
			} else {
				throw new IllegalArgumentException("The number of colums should be 4 or 6 in: " + filename);
			}
		}

		logger.info("Applied transformation(s) to: " + filename);
		FileUtils.writeDoubleArrayToFile(outFileName, t2d, false);
	}
}
