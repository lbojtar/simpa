/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simpa.core.api.LengthUnit;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

/**
 * Utilities for writing and reading files.
 */
public class FileUtils {
	private static final Logger logger = LogManager.getLogger(FileUtils.class);

	/**
	 * Scan the output directory for existing output files
	 * 
	 * @param outDir The directory to look at
	 * @return List of existing files
	 */

	public static ArrayList<String> scanDirectory(String outDir) {
		// scan output directory for existing files
		ArrayList<String> l = new ArrayList<>();

		File[] files = new File(outDir).listFiles();
		if (files != null && files.length > 0) {
			for (File file : files) {
				if (file.isFile()) {
					l.add(file.getName());
				}
			}
		}
		return l;
	}

	/**
	 * Writes the given string to a text file.
	 * 
	 * @param file   output file name
	 * @param string the string to write to the file
	 */
	public static void writeTextFile(String file, String string) {

		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, false));
			outputStream.write(string, 0, string.length());
			outputStream.close();

		} catch (IOException ex) {
			logger.error("Something went wrong writing text to file: " + file);
			logger.debug(Arrays.toString(ex.getStackTrace()));
		}
		logger.info("Wrote text file: " + file);
	}

	/**
	 * Appends the given string to a text file.
	 * 
	 * @param file   file to append to
	 * @param string string to append to file
	 */
	public static void appendToFile(String file, String string) {

		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, true));
			outputStream.write(string, 0, string.length());
			outputStream.close();

		} catch (IOException ex) {
			logger.error("Something went wrong appending text to file: " + file);
			logger.debug(Arrays.toString(ex.getStackTrace()));
		}
		logger.info("Appended to text file: " + file);
	}

	/**
	 * Writes the given 2D array to a file.
	 * 
	 * @param file           - Output file.
	 * @param arr            - The array
	 * @param exchangeRowCol - If the exchangeRowCol is true, the rows and coulumns
	 *                       are exchanged.
	 */
	public static void writeDoubleArrayToFile(String file, double[][] arr, boolean exchangeRowCol) {

		try {
			BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, false));
			if (exchangeRowCol) {
				for (int i = 0; i < arr[0].length; i++) {
					StringBuilder s = new StringBuilder();
					for (double[] doubles : arr) {
						s.append(" ").append(doubles[i]);
					}

					s.append(System.lineSeparator());

					outputStream.write(s.toString(), 0, s.length());
				}
			} else {
				for (double[] doubles : arr) {
					StringBuilder s = new StringBuilder();
					for (int j = 0; j < arr[0].length; j++) {
						s.append(" ").append(doubles[j]);
					}

					s.append(System.lineSeparator());

					outputStream.write(s.toString(), 0, s.length());
				}
			}

			outputStream.close();

		} catch (IOException ex) {
			logger.error("Something went wrong writing double array to file: " + file);
			logger.debug(Arrays.toString(ex.getStackTrace()));
		}
		logger.info("Wrote text file: " + file);
	}

	/**
	 * Writes given vectors to a file with a given name.
	 *
	 * @param f       name of the output file.
	 * @param vectors vectors to write to the file.
	 */
	public static void writeVectors(String f, List<Vector3D> vectors) {
		StringBuilder sb = new StringBuilder();
		sb.append("#X Y Z").append(System.lineSeparator());
		for (Vector3D v : vectors) {
			sb.append(v.getX()).append(" ").append(v.getY()).append(" ").append(v.getZ())
					.append(System.lineSeparator());
		}

		FileUtils.writeTextFile(f, sb.toString());
	}

	/**
	 * Copies a resource file into the given file.
	 *
	 * @param resourceFileName file in the resources folder.
	 * @param outFile          name of the output file.
	 */
	public static void copyResourceFile(String resourceFileName, String outFile) {
		try (InputStream is = FileUtils.class.getResourceAsStream(resourceFileName)) {
			assert is != null;
			Files.copy(is, Paths.get(outFile));
		} catch (FileAlreadyExistsException e) {
			// not a problem for us
		} catch (IOException e) {
			logger.error("Something went wrong copying resource file: " + resourceFileName);
			logger.debug(Arrays.toString(e.getStackTrace()));
		}
		logger.info("Copied resource file: " + resourceFileName + " to : " + outFile);
	}

	/**
	 * Reads 3D vectors from a given file.
	 *
	 * @param filename   - File to read
	 * @param lengthUnit - Length Unit of the values in the file (MM, CM, M)
	 * @return List of vectors in meters
	 * @throws FileNotFoundException when the file is not found.
	 */
	public static List<Vector3D> readVectors(String filename, LengthUnit lengthUnit) throws FileNotFoundException {
		return readVectors(filename, 0, 1.0 / lengthUnit.getFactor());
	}

	/**
	 * Reads 3D vectors from a given file starting from the given column.
	 *
	 * @param filename      - File to read
	 * @param column        - Column number in the file of the first component of
	 *                      the vector. The Vector will be formed of { value in
	 *                      column, value in column+1,value in column+2}
	 * @param scalingFactor - Scaling for the vector.
	 * @return List of vectors in meters
	 * @throws FileNotFoundException when the file is not found.
	 */
	public static List<Vector3D> readVectors(String filename, int column, double scalingFactor)
			throws FileNotFoundException {

		List<Vector3D> vl = readVectors(new FileInputStream(filename), column, scalingFactor);
		logger.info("Read 3D vectors from file: " + filename);
		return vl;
	}

	/**
	 * Read vectors from an inputstream.
	 *
	 * @param is         inputstream of the file
	 * @param lengthUnit unit for length
	 * @return list of Vector3D objects
	 */
	public static List<Vector3D> readVectors(InputStream is, LengthUnit lengthUnit) {
		return readVectors(is, 0, 1.0 / lengthUnit.getFactor());
	}

	/**
	 * Read vectors from a given column in an inputstream.
	 *
	 * @param is            inputstream of the file.
	 * @param column        column to be read.
	 * @param scalingFactor factor to scale values with.
	 * @return list of Vector3D objects
	 */
	public static List<Vector3D> readVectors(InputStream is, int column, double scalingFactor) {
		List<Vector3D> vectors = new ArrayList<>();

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			do {
				double[] values = getNextValidLine(br, column + 3);

				if (values == null) {
					break;
				}

				double x = values[column] * scalingFactor;
				double y = values[column + 1] * scalingFactor;
				double z = values[column + 2] * scalingFactor;

				vectors.add(new Vector3D(x, y, z));

			} while (true);

			br.close();

		} catch (IOException e) {
			logger.error("Something went wrong reading vectors from file.");
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

		return vectors;
	}

	/**
	 * Reads a double array from a file that is delimited by spaces or new lines.
	 *
	 * @param filename name of the file to read
	 * @return double array read from the file
	 * @throws IOException when a file is not found or something went wrong while
	 *                     reading
	 */
	public static double[] getDoubleArrayFromFile(String filename) throws IOException {
		InputStreamReader is = new InputStreamReader(new FileInputStream(filename));
		BufferedReader br = new BufferedReader(is);

		String line = br.readLine();

		ArrayList<Double> values = new ArrayList<>();

		while (line != null) {
			String[] lineArray = line.split(" ");

			for (String s : lineArray) {
				values.add(Double.parseDouble(s));
			}

			line = br.readLine();
		}

		double[] doubles = new double[values.size()];
		for (int i = 0; i < values.size(); i++) {
			doubles[i] = values.get(i);
		}

		return doubles;
	}

	/**
	 * Search a line in a file containing the given key text.
	 * 
	 * @param filename The file to be searched.
	 * @param key      The key string to be searched for.
	 * @return The line in the file which contains the key string. If not found an
	 *         empty string is returned.
	 * @throws IOException If the file can't be read.
	 */
	public static String getKeyLineFromFile(String filename, String key) throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
		String line;
		do {
			line = br.readLine();
			if (line == null) {
				line = "";
				break;
			}
			if (line.contains(key)) 
				break;
			
		} while (true);

		br.close();
		return line;
	}

	/**
	 * Reads a 2D array of doubles with the specified number of columns.
	 *
	 * @param is   -InputStream
	 * @param nCol -Number of columns
	 * @return result double[][]
	 */
	public static double[][] read2DArray(InputStream is, int nCol) {

		List<List<Double>> rows = new ArrayList<>();

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			do {
				double[] values = getNextValidLine(br, nCol);
				if (values == null) {
					break;
				}

				List<Double> row = new ArrayList<>();

				for (int i = 0; i < nCol; i++) {
					row.add(values[i]);
				}

				rows.add(row);

			} while (true);

			br.close();

		} catch (IOException e) {
			logger.error("Something went wrong reading array from file.");
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

		double[][] res = new double[rows.size()][nCol];
		for (int row = 0; row < rows.size(); row++) {
			for (int col = 0; col < nCol; col++) {
				res[row][col] = rows.get(row).get(col);
			}
		}

		return res;
	}

	/**
	 * Reads the next valid line from the BufferedReader containing the given number
	 * of numbers.
	 *
	 * @param breader buffered reader object.
	 * @param columns -The number of expected number columns in a valid line.
	 * @return - double array containing the parsed numbers, null if end of file.
	 * @throws IOException when there is a problem with the file.
	 */
	public static double[] getNextValidLine(BufferedReader breader, int columns) throws IOException {

		double[] res = new double[columns];
		boolean invalidLine;

		do {
			String s = breader.readLine();

			if (s == null)
				return null; // end of file

			StringTokenizer st = new StringTokenizer(s);

			try {

				for (int i = 0; i < columns; i++) {
					double d = Double.parseDouble(st.nextToken());
					res[i] = d;
				}

				invalidLine = false;

			} catch (NumberFormatException | NoSuchElementException e) {
				invalidLine = true;
			}
		} while (invalidLine);

		return res;
	}

	/**
	 * Reads a two-dimensional array from a file.
	 *
	 * @param filename input file.
	 * @param nCol     amount of columns to read.
	 * @return a two-dimensional array of doubles.
	 */
	public static double[][] read2DArray(String filename, int nCol) throws FileNotFoundException {
		double[][] da = read2DArray(new FileInputStream(filename), nCol);
		logger.info("Read double[" + da.length + "][" + da[0].length + "] from file: " + filename);
		return da;
	}

	/**
	 * Writes a ByteBuffer to a file with the given filename.
	 *
	 * @param bb       - ByteBuffer object.
	 * @param filename - output filename.
	 */
	public static void writeByteBufferToFile(ByteBuffer bb, String filename) {

		File file = new File(filename);

		try {
			FileChannel wChannel = new FileOutputStream(file, false).getChannel();
			bb.flip();
			wChannel.write(bb);
			wChannel.close();
		} catch (IOException e) {
			logger.error("Something went wrong writing ByteBuffer to file: " + filename);
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

	}

	/**
	 * Reads a ByteBuffer object from a file with the given filename
	 *
	 * @param filename - which file to read.
	 * @return ByteBuffer extracted from file.
	 */
	public static ByteBuffer readByteBufferFromFile(String filename) {

		try {

			RandomAccessFile file = new RandomAccessFile(filename, "r");
			FileChannel rChannel = file.getChannel();
			ByteBuffer bb = ByteBuffer.allocate((int) rChannel.size());
			rChannel.read(bb);
			rChannel.close();
			bb.rewind();
			return bb;
		} catch (IOException e) {
			logger.error("Something went wrong reading ByteBuffer to file: " + filename);
			logger.debug(Arrays.toString(e.getStackTrace()));
		}
		return null;
	}

	/**
	 * Counts the number of columns containing numbers in the file. Text which can't
	 * be parsed to a number is ignored.
	 *
	 * @param filename input file
	 * @return The count of numbers in the first line which contains a number.
	 */
	public static int countNumberOfColumns(String filename) {

		String s;
		int columns;

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filename)));

			while ((s = br.readLine()) != null && s.length() != 0) {

				StringTokenizer st = new StringTokenizer(s);

				try {
					columns = 0;
					do {
						Double.parseDouble(st.nextToken());
						columns++;
					} while (st.hasMoreTokens());

					return columns;

				} catch (NumberFormatException e) {
					// we ignore the line, it might be just comment text
				}

			}

			br.close();

		} catch (IOException e) {
			logger.error("Something went wrong counting columns in file: " + filename);
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

		return -1; // no number in this file
	}

	/**
	 * Counts the lines in given file.
	 *
	 * @param file input file.
	 * @return the amount of lines in given file.
	 */
	public static int countLines(String file) {

		String s;
		int i = 0;
		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			do {
				s = br.readLine();
				i++;
			} while (s != null);

			br.close();
		} catch (IOException e) {
			logger.error("Something went wrong counting lines in file: " + file);
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

		return i;
	}

	/**
	 * Gets a specific line from a file.
	 *
	 * @param file       input file.
	 * @param lineNumber linenumber to read.
	 * @return string value of the given linenumber.
	 */
	public static String getLineFromFile(String file, long lineNumber) {

		String s = null;

		try {

			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			for (int i = 0; i < lineNumber; i++) {
				s = br.readLine();
			}

			br.close();

		} catch (IOException e) {
			logger.error("Something went wrong getting line from file: " + file);
			logger.debug(Arrays.toString(e.getStackTrace()));
		}

		return s;
	}
	
}
