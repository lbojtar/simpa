/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Object representing a  local coordinate system.
 */
public record LocalFrame(Vector3D lx, Vector3D ly, Vector3D lz) {

	public String toString() {
		return lx.getX() + " " + lx.getY() + " " + lx.getZ() + " " + ly.getX() + " " + ly.getY() + " " + ly.getZ() + " "
				+ lz.getX() + " " + lz.getY() + " " + lz.getZ();
	}

	/**
	 * Get a point near the given base point shifted by the a vector which is
	 * rotated to the local frame.
	 * 
	 * @param basePoint The point to be shifted
	 * @param shift     Shift in the local system.
	 * 
	 * @return - A point in the global frame shifted by the given vector in the
	 *         local frame.
	 */
	public Vector3D getLocalShiftedPoint(Vector3D basePoint, Vector3D shift) {

		Rotation rot = new Rotation(Vector3D.PLUS_I, Vector3D.PLUS_K, lx(), lz());
		Vector3D localShift = rot.applyTo(shift);
		return basePoint.add(localShift);
	}
	
    // private static boolean isRightHanded(LocalFrame lf) {
    //     double scalarTripleProduct = lf.lx().crossProduct(lf.ly()).dotProduct(lf.lz());
    //     return scalarTripleProduct > 0;
    // }
}
