/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.SphericalCoordinates;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.sh.SHBall;
import simpa.core.sh.SHBallEvaluator;
import simpa.core.sh.SHField;
import simpa.core.sh.SHFieldEvaluatorImpl;

import java.io.IOException;

/**
 * Utilities for calculating different types of errors for evaluation.
 */
public class RmsErrorCalculator {
	private static final Logger logger = LogManager.getLogger(RmsErrorCalculator.class);



	/**
	 * Prints the RMS (root mean square) error for a SHField in a given file.
	 *
	 * @param filename input file
	 * @param pp       potentialprovider
	 * @param ntest    amount of test points
	 * @throws OutOfApertureException thrown when a point is outside the volume.
	 * @throws KnownFatalException    when an internal error happens.
	 */
	public void printRMSErrorForSHFieldFile(String filename, PotentialProvider pp, int ntest)
			throws OutOfApertureException, KnownFatalException, IOException {
		SHField shgf = SHField.createFromFile(filename);

		printRMSError(shgf, pp, ntest);
	}

	/**
	 * Calculates the normalized RMS error between the exact and estimated values.
	 * See: // https://en.wikipedia.org/wiki/Root-mean-square_deviation
	 * 
	 * @param exact    values
	 * @param estimate values
	 * @return normalized RMS error
	 * @throws IllegalArgumentException If the length of the arrays are different or
	 *                                  zero;
	 */
	public double getNormalizedRmsError(double[] exact, double[] estimate) throws IllegalArgumentException {
		int n = exact.length;
		if (n != estimate.length || n == 0)
			throw new IllegalArgumentException("The length of the exact and estimate array is different or zero !");

		double min = Double.MAX_VALUE;
		double max = Double.MIN_VALUE;
		double diffSqSum = 0;

		for (int i = 0; i < n; i++) {
			if (estimate[i] > max)
				max = estimate[i];
			if (estimate[i] < min)
				min = estimate[i];
			double diff = estimate[i] - exact[i];
			diffSqSum += diff * diff;
		}

		double rmsd = Math.sqrt(diffSqSum / n);
		double range = max - min;

		if (range == 0)
			return 0;
		else
			return rmsd / range;
	}

	////////////////// API ENDS HERE //////////////////////////
	
	/**
	 * Prints out the RMS error of the SH field expansion with n random samples.
	 * 
	 * @param shgf  -SHField
	 * @param gf    -PotentialProvider
	 * @param nTest -Number of random samples.
	 * @throws OutOfApertureException when a point is found to be outside the
	 *                                machine.
	 * @throws KnownFatalException    when an internal error happens.
	 */
	protected void printRMSError(SHField shgf, PotentialProvider gf, int nTest)
			throws OutOfApertureException, KnownFatalException {

		logger.info("Checking RMS error of spherical harmonics expansion of global field with random samples");

		SHFieldEvaluatorImpl shgfev = new SHFieldEvaluatorImpl(shgf, 1.0);

		int nFunctions = shgf.getSHBalls()[0].getNumberOfFunctions();

		int nBalls = shgf.getSHBalls().length;
		double[][][] exact = new double[nTest][nFunctions][4];
		double[][][] shEstimate = new double[nTest][nFunctions][4];

		// sample
		for (int i = 0; i < nTest; i++) {
			int bIndex = (int) (Math.random() * nBalls);
			SHBall ball = shgf.getSHBalls()[bIndex];
			Vector3D p = ball.getCentre();
			Vector3D offset = new Vector3D(2 * (Math.random() - 0.5), 2 * (Math.random() - 0.5),
					2 * (Math.random() - 0.5));
			offset = offset.normalize();

			double multiplier = ball.getRadius() * 0.99999; // make sure we are inside the ball

			p = p.add(offset.scalarMultiply(multiplier)); // some random point inside the ball

			// System.out.println("Evaluation of the potentials: " + i);
			double[][] potExact = gf.getPotentialsWithDerivatives(p);
			double[][] potEstimate = shgfev.getPotentialsWithDerivatives(p);
			for (int j = 0; j < nFunctions; j++) {
				for (int k = 0; k < 4; k++) {
					exact[i][j][k] = potExact[j][k];
					shEstimate[i][j][k] = potEstimate[j][k];
				}
			}
		}

		for (int potIndex = 0; potIndex < nFunctions; potIndex++) {
			double rmsErr = this.getNormalizedRms(exact, shEstimate, potIndex, 0);
			logger.info("Normalized RMS error of the potential " + potIndex + " = " + rmsErr);

		}

	}


	/**
	 * This is useful to determine the ball size for a given maximum degree of the
	 * expansion.
	 * 
	 * @param ball    - SHBall providing the estimation
	 * @param exactPP - PotentialProvider for the exact value
	 * @param n       - Number of samples
	 * @return - An array contaning the normalized RMS error for each potential.
	 * @throws OutOfApertureException when the ball is out of aperture
	 * @throws KnownFatalException    when an internal error happens.
	 */
	protected double[] getRmsErrorOnBallSurface(SHBall ball, PotentialProvider exactPP, int n)
			throws OutOfApertureException, KnownFatalException {

		SHBallEvaluator ballEv = new SHBallEvaluator(ball.getMaximumDegree(),ball.getNumberOfFunctions());
		ballEv.setSHBall(ball);

		int noOfPots = exactPP.getPotentialsWithDerivatives(ball.getCentre()).length;
		double[] res = new double[noOfPots];
		double[][] exact = new double[noOfPots][n];
		double[][] estimate = new double[noOfPots][n];

		for (int i = 0; i < n; i++) {
			SphericalCoordinates c = new SphericalCoordinates(ball.getRadius() * 0.99, Math.random() * Math.PI * 2,
					Math.random() * Math.PI);
			Vector3D r = c.getCartesian().add(ball.getCentre());

			double[][] pot = exactPP.getPotentialsWithDerivatives(r);
			double[][] estPot = ballEv.getPotentials(r);

			for (int j = 0; j < noOfPots; j++) {
				exact[j][i] = pot[j][0];
				estimate[j][i] = estPot[j][0];
			}
		}

		for (int poti = 0; poti < noOfPots; poti++) {
			res[poti] = this.getNormalizedRmsError(exact[poti], estimate[poti]);
		}

		return res;
	}
	
	/**
	 * Gets the normalized RMS error for the exact array and the estimated array at
	 * specific indexes.
	 *
	 * @param exactArr    the calculated array.
	 * @param estimateArr the estimated array.
	 * @param potIndex    index of potential to check.
	 * @param derIndex    index of derivative to check.
	 * @return the normalized RMS error.
	 */
	private double getNormalizedRms(double[][][] exactArr, double[][][] estimateArr, int potIndex, int derIndex) {
		int n = exactArr.length;
		double[] exact = new double[n];
		double[] estimate = new double[n];

		for (int i = 0; i < n; i++) {
			exact[i] = exactArr[i][potIndex][derIndex];
			estimate[i] = estimateArr[i][potIndex][derIndex];
		}
		return getNormalizedRmsError(exact, estimate);
	}
}
