package simpa.core.api.utils;

/**
 * Gaussian 1D quadrature points and weights for interval -1.0, 1.0 See:
 * https://numfactory.upc.edu/web/Calculo2/P2_Integracio/html/Gauss1D.html
 */
public enum Quadrature1D {

	ONE_POINT_GAUSSIAN(new double[] { 0 }, new double[] { 2.0 }),
	TWO_POINT_GAUSSIAN(new double[] { -0.577350269189625764, 0.577350269189625764 }, new double[] { 1.0, 1.0 }),
	THREE_POINT_GAUSSIAN(new double[] { -0.774596669241483377, 0, 0.774596669241483377 },
			new double[] { 0.555555555555555555, 0.888888888888888888, 0.555555555555555555 }),
	FIVE_POINT_GAUSSIAN(
			new double[] { -0.906179845938663992, -0.538469310105683091, 0, 0.538469310105683091,
					0.906179845938663992 },
			new double[] { 0.236926885056189087, 0.478628670499366468, 0.568888888888888888, 0.478628670499366468,
					0.236926885056189087 });

	private double[] weights;
	private double[] positions;

	/**
	 * 
	 * @return Weights of the quadrature points.
	 */
	public double[] getWeights() {
		return weights;
	}

	/**
	 * 
	 * @return positions of the quadrature points in the interval -1,1
	 */
	public double[] getPositions() {
		return positions;
	}

	private Quadrature1D(double[] positions, double[] weights) {
		assert (positions.length == weights.length);
		this.positions=positions;
		this.weights=weights;
	}

}
