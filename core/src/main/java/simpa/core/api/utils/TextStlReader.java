/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.utils;


import org.apache.commons.math3.geometry.euclidean.threed.PolyhedronsSet;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.SystemConstants;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Utility specific for reading STL files.
 */
public class TextStlReader {
    private static final Logger logger = LogManager.getLogger(TextStlReader.class);
    List<Vector3D> vertices;
    List<int[]> facetList;
    private String filename;

    /**
     * Constructs the STL reader with empty lists.
     */
    public TextStlReader() {
        vertices = new ArrayList<>();
        facetList = new ArrayList<>();
    }

    /**
     * @return List of vertices.
     */
    public List<Vector3D> getVertices() {
        return vertices;
    }

    /**
     * @return List of integer arrays with the indexes of the vertices consisting of
     * the faces.
     */
    public List<int[]> getFaces() {
        return facetList;
    }

    /**
     * Creates a PolyhedronsSet from an STL text file. The PolyhedronsSet is useful
     * for us, because it has the checkPoint(Point of type P) method, which tells if
     * a point is inside or outside of the surface described by the STL file.. For
     * the STL file format see: https://en.wikipedia.org/wiki/STL_(file_format)
     *
     * @param fn input file name
     * @return PolyhedronsSet
     * @throws IOException if there are problems with the file
     */
    @SuppressWarnings("null")
    public PolyhedronsSet createStlObject(String fn) throws IOException {
        this.filename = fn;
        return createStlObject(new FileInputStream(fn));
    }

    /**
     * Creates a PolyhedronSet from an STL file.
     *
     * @param is input stream of the input STL file.
     * @return a polyhedronset constructed from the STL file.
     * @throws IOException if there are problems with the file
     */
    public PolyhedronsSet createStlObject(InputStream is) throws IOException {

        readStlFile(is);

        logger.info("Constructing boundary surface from file: " + filename);
        PolyhedronsSet phs = new PolyhedronsSet(vertices, facetList, SystemConstants.GEOMETRY_TOLERANCE);
        logger.info("Boundary surface is ready.");

        return phs;
    }

    /**
     * Reads the contents of an STL file.
     *
     * @param is input stream of the input STL file.
     * @throws IOException if there are problems with the file.
     */
    public void readStlFile(InputStream is) throws IOException {

        BufferedReader br = new BufferedReader(new InputStreamReader(is));

        String line;
        int globalVertexidx = 0; // 0 ..n , where n is the number of all vertices
        int localVertexidx = 0; // 0 ..2 , counts vertices in a triangle

        Vector3D normal = null;
        int[] facet = null;

        while ((line = br.readLine()) != null) {
            line = line.trim();
            if (line.startsWith("facet")) {
                facet = new int[3];
                normal = getNormal(line);
                localVertexidx = 0;
            } else if (line.startsWith("endfacet") && facet != null && normal != null) {
                Vector3D p0 = vertices.get(facet[0]);
                Vector3D p1 = vertices.get(facet[1]);
                Vector3D p2 = vertices.get(facet[2]);
                Vector3D n = Vector3D.crossProduct(p1.subtract(p0), p2.subtract(p1)).normalize();
                if (normal.subtract(n).getNorm() > 1E-6) {
                    logger.warn("WARNING: Normal vector mismatch in " + filename + " normal given: " + normal
                            + " calculated: " + n.toString());
                }
                if (n.getNorm() < SystemConstants.GEOMETRY_TOLERANCE) {
                    logger.warn("WARNING: Normal vector is very small: " + n.getNorm()+" for points " +p0+" "+p1+" "+p2);
                }
                
                facetList.add(facet);

            } else if (line.startsWith("vertex") && facet != null && normal != null) {
                Vector3D p = getPoint(line);
                int duplicateIdx = getDuplicateIndex(vertices, p, SystemConstants.GEOMETRY_TOLERANCE);
                if (duplicateIdx == -1) {
                    vertices.add(p);
                    facet[localVertexidx] = globalVertexidx;
                    globalVertexidx++;
                } else {
                    facet[localVertexidx] = duplicateIdx; // we have already this vertex in the list
                }
                // System.out.println(p.getX()+" "+p.getY()+" "+p.getZ());
                localVertexidx++;
            }
        }

        br.close();
    }


    /**
     * Check if the given point is already in the list within a given tolerance.
     *
     * @param vertices  List of points.
     * @param point     the point to be tested.
     * @param tolerance below this limit the points considered duplicates.
     * @return Index of the point in the list which is closer to the given point
     * than the tolerance. If there is no such point found, then it returns
     * -1.
     */
    private int getDuplicateIndex(List<Vector3D> vertices, Vector3D point, double tolerance) {

        // check vertices distances
        for (int i = 0; i < vertices.size(); ++i) {
            if (Vector3D.distance(point, vertices.get(i)) <= tolerance)
                return i;
        }
        return -1;
    }

    private Vector3D getNormal(String s) {
        String[] split = s.split(" ");
        double p1 = Double.parseDouble(split[2]);
        double p2 = Double.parseDouble(split[3]);
        double p3 = Double.parseDouble(split[4]);
        return new Vector3D(p1, p2, p3).normalize();
    }

    private Vector3D getPoint(String s) {
        String[] split = s.split(" ");
        double p1 = Double.parseDouble(split[1]);
        double p2 = Double.parseDouble(split[2]);
        double p3 = Double.parseDouble(split[3]);
        return new Vector3D(p1, p2, p3);
    }
}
