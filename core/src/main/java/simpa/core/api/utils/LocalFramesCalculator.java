package simpa.core.api.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.analysis.UnivariateFunction;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.optim.MaxEval;
import org.apache.commons.math3.optim.nonlinear.scalar.GoalType;
import org.apache.commons.math3.optim.univariate.BrentOptimizer;
import org.apache.commons.math3.optim.univariate.SearchInterval;
import org.apache.commons.math3.optim.univariate.UnivariateObjectiveFunction;
import org.apache.commons.math3.optim.univariate.UnivariatePointValuePair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Calculator class to define local frames along a curve. It uses a parallel
 * transport algorithm. An initial vector is parallel transported along a
 * general 3D curve. It can optimize an initial vector if the OPTIMIZE_TORSION
 * strategy is selected or uses the given one when the
 * PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION strategy is selected.
 * 
 * For the algorithm of the parallel transport See: Parallel Transport Approach
 * to Curve Framing Andrew J. Hanson and Hui Ma
 * https://legacy.cs.indiana.edu/ftp/techreports/TR425.pdf
 * 
 * @author lbojtar
 *
 */
public class LocalFramesCalculator {

	private static final Logger logger = LogManager.getLogger(LocalFramesCalculator.class);
	/**
	 * The MINIMIZE_TORSION strategy is searching for an initial vector normal the
	 * the first tangent vector of the curve such the total torsion is minimized
	 * along the curve.
	 * 
	 * The PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION strategy is parallel transports
	 * the initial vector.
	 * 
	 *
	 */
	public static enum Strategy {
		MINIMIZE_TORSION, PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION
	};

	public static final Strategy DEFAULT_STRATEGY = Strategy.PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION;
	public static final Vector3D DEFAULT_VERTICAL =  Vector3D.PLUS_J;
	
	private Strategy strategy;
	private List<Vector3D> curve;
	private List<LocalFrame> frames;
	private List<Vector3D> tvl;
	private List<Vector3D> transported;
	private Vector3D vertical = DEFAULT_VERTICAL;
	private boolean closed;
	private double snapToAxisLimit = 1E-6;

	/**
	 * Constructor for LocalFramesCalculator.
	 * 
	 * @param curve    A curve in 3D.
	 * @param closed   Closed or open indicator. If closed the last point in the
	 *                 list considered connected to the first one.
	 * @param strategy Strategy to optimize the initial vector. It determines how
	 *                 the local frame is constructed.
	 * @param vertical Preferred vertical direction. It can be null, in this case by
	 *                 default it is the +Y global axis. There are two optimal
	 *                 initial orientation which minimizes the torsion of a planar
	 *                 curve. By setting this vector one can be chosen. The
	 *                 preferred vertical direction should be perpendicular to the
	 *                 first tangent vector of the curve.
	 */
	public LocalFramesCalculator(List<Vector3D> curve, boolean closed, Strategy strategy, Vector3D vertical) {
		logger.info("Constructing local coordinates.");
		this.curve = curve;
		this.closed = closed;
		
		if (strategy == null)
			this.strategy = DEFAULT_STRATEGY;

		if (vertical != null) {
			Vector3D nv = vertical.normalize();
			Vector3D tv = PathUtils.getTangentVector(curve, closed, 0).direction();
			if (tv.crossProduct(nv).getNorm() < 0.999999)
				throw new IllegalArgumentException(
						"The preferred vertical direction should be  perpendicular to the first tangent vector");
			this.vertical = nv;
		}
		calculate();
	}

	/**
	 * Writes the reference frame unit vectors into a file
	 * 
	 * @param file      Output file.
	 * @param increment Increment of steps while sampling the curve. The value 1
	 *                  takes all the curve points, 2 every second, 3 every third
	 *                  and so on.
	 */
	public void writeToFile(String file, int increment) {
		StringBuffer sb = new StringBuffer();

		sb.append(
				"""
						#X Y Z Hx Hy Hz Vx Vy Vz Sx Sy Sz
						#
						# Hx Hy Hz are the coordinates of the horizontal unit vector
						# Vx Vy Vz are the coordinates of the vertical unit vector
						# Sx Sy Sz are the coordinates of the longitudinal unit vector
						# recomended gnuplot commands:
						#
						# set xlabel "X"
						# set ylabel "Y"
						# set zlabel "Z"
						# set view equal xyz
						# s=0.2 # scale factor
						# splot "filename_of_this_file" u 1:2:3:(s*$4):(s*$5):(s*$6) w vectors, "filename_of_this_file" u 1:2:3:(s*$7):(s*$8):(s*$9) w vectors
						#
						""");

		for (int i = 0; i < frames.size(); i += increment) {
			double x = curve.get(i).getX();
			double y = curve.get(i).getY();
			double z = curve.get(i).getZ();
			sb.append(x + " " + y + " " + z + " " + frames.get(i).toString() + System.lineSeparator());
		}
		FileUtils.writeTextFile(file, sb.toString());
	}

	/**
	 * Get the local frame calculated. The calculation itself called by the
	 * constructor.
	 * 
	 * @return List of local frame objects. There is a frame in the list for each
	 *         curve points given at the constructor.
	 */
	public List<LocalFrame> getLocalFrames() {
		return frames;
	}

	/**
	 * Parallel transports the given vector along the curve given in the
	 * constructor. See p. 9 and p. 10 of Parallel Transp ort Approach to Curve
	 * Framing Andrew J. Hanson and Hui Ma
	 * https://legacy.cs.indiana.edu/ftp/techreports/TR425.pdf
	 * 
	 * @param vInitial Initial vector to be parallel transported.
	 * @param store    Store the result or not. During optimization we don't store
	 *                 to speed reasons.
	 * 
	 * @return The accumulated torsion between the initial and final vectors.
	 */
	private double parallelTransport(Vector3D vInitial, boolean store) {
		if (store) {
			transported.clear();
			transported.add(vInitial);
		}

		Vector3D previous = vInitial;
		Vector3D trv = null;
		double torsion = 0;
		for (int i = 0; i < curve.size() - 1; i++) {
			Vector3D b = tvl.get(i).crossProduct(tvl.get(+1));
			if (b.getNorm() == 0) {
				trv = previous;
			} else {
				// projection method on page 10. of the paper
				Vector3D nextTv = tvl.get(i + 1);
				trv = previous.subtract(nextTv.scalarMultiply(previous.dotProduct(nextTv))).normalize();

				snapToAxis(trv, snapToAxisLimit);
			}

			if (store)
				transported.add(trv);
			assert (trv.getNorm() > 0.99);

			torsion += Math.abs(Vector3D.angle(trv, previous));
			previous = trv;
		}

		return torsion;
	}

	private void calculate() {
		tvl = new ArrayList<>();
		frames = new ArrayList<>();
		transported = new ArrayList<>();

		for (int i = 0; i < curve.size(); i++) {
			tvl.add(PathUtils.getTangentVector(curve, closed, i).direction());
		}

		InitialVectorOptimizer opt = new InitialVectorOptimizer();
		Vector3D initialVector = vertical;
		if (strategy != Strategy.PARALLEL_TRANSPORT_WITHOUT_OPTIMIZATION)
			initialVector = opt.optimize();
		parallelTransport(initialVector, true); // storing

		for (int i = 0; i < curve.size(); i++) {
			Vector3D lz = tvl.get(i);
			assert (lz.getNorm() < 1.0001 && lz.getNorm() > 0.9999);
			Vector3D ly = transported.get(i);
			assert (ly.getNorm() < 1.0001 && ly.getNorm() > 0.9999);
			Vector3D lx = Vector3D.crossProduct(ly, lz);
			assert (lx.getNorm() < 1.0001 && lx.getNorm() > 0.9999);
			LocalFrame lf = new LocalFrame(lx, ly, lz);
			frames.add(lf);
		}
	}

	/**
	 * If the given vector is close to any global axis we adjust it to have exactly
	 * on an axis
	 * 
	 * @param v     The vector to be adjusted
	 * @param limit The limit to be considered close to an axis.
	 */
	private Vector3D snapToAxis(Vector3D v, double limit) {
		if (Vector3D.crossProduct(v, Vector3D.PLUS_I).getNorm() < limit)
			return Vector3D.PLUS_I;
		if (Vector3D.crossProduct(v, Vector3D.PLUS_J).getNorm() < limit)
			return Vector3D.PLUS_J;
		if (Vector3D.crossProduct(v, Vector3D.PLUS_K).getNorm() < limit)
			return Vector3D.PLUS_K;
		if (Vector3D.crossProduct(v, Vector3D.MINUS_I).getNorm() < limit)
			return Vector3D.MINUS_I;
		if (Vector3D.crossProduct(v, Vector3D.MINUS_J).getNorm() < limit)
			return Vector3D.MINUS_J;
		if (Vector3D.crossProduct(v, Vector3D.MINUS_K).getNorm() < limit)
			return Vector3D.MINUS_K;
		return v;
	}

	private class InitialVectorOptimizer {

		UnivariateFunction uf = new OptimizationFunction();

		/**
		 * 
		 * @return The optimal vector.
		 */
		public Vector3D optimize() {
			BrentOptimizer optimizer = new BrentOptimizer(1e-8, 1e-8);
			UnivariatePointValuePair result = optimizer.optimize(new MaxEval(1000), new UnivariateObjectiveFunction(uf),
					GoalType.MINIMIZE, new SearchInterval(-Math.PI, Math.PI));

			double angle = result.getPoint();

			Rotation rot = new Rotation(tvl.get(0), angle, RotationConvention.VECTOR_OPERATOR);
			logger.info("Angle between initial and final local Y = " + angle);
			return rot.applyTo(vertical);
		}
	}

	/**
	 * Inner class for minimizing the torsion during parallel transport of a vector.
	 * 
	 * @author lbojtar
	 *
	 */
	private class OptimizationFunction implements UnivariateFunction {

		@Override
		public double value(double x) {
			Rotation rot = new Rotation(tvl.get(0), x, RotationConvention.VECTOR_OPERATOR);
			Vector3D vi = rot.applyTo(vertical);

			return parallelTransport(vi, false);
		}

	}

}
