/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.TangentVector3D;

/**
 * Utilities for searching on a path (orbit).
 */
public class PathUtils {

	/**
	 * Mirrors the path on the X axis by adding the negative of the coordinate
	 * designated by the coordinateIndex parameter.
	 *
	 * @param path path to extend
	 * @return mirrored path
	 */
	public static List<Vector3D> extendWithMirrorOnXAxis(List<Vector3D> path) {
		List<Vector3D> mirror = new ArrayList<>();

		for (Vector3D pv : path) {
			Vector3D newp = new Vector3D(-pv.getX(), pv.getY(), pv.getZ());
			mirror.add(newp);
		}

		return addMirrorToPath(path, mirror);
	}

	/**
	 * Mirrors the path on the Y axis by adding the negative of the coordinate
	 * designated by the coordinateIndex parameter.
	 *
	 * @param path path to extend
	 * @return mirrored path
	 */
	public static List<Vector3D> extendWithMirrorOnYAxis(List<Vector3D> path) {
		List<Vector3D> mirror = new ArrayList<>();

		for (Vector3D pv : path) {
			Vector3D newp = new Vector3D(pv.getX(), -pv.getY(), pv.getZ());
			mirror.add(newp);
		}

		return addMirrorToPath(path, mirror);
	}

	/**
	 * Mirrors the path on the Z axis by adding the negative of the coordinate
	 * designated by the coordinateIndex parameter.
	 *
	 * @param path path to extend
	 * @return mirrored path
	 */
	public static List<Vector3D> extendWithMirrorOnZAxis(List<Vector3D> path) {
		List<Vector3D> mirror = new ArrayList<>();

		for (Vector3D pv : path) {
			Vector3D newp = new Vector3D(pv.getX(), pv.getY(), -pv.getZ());
			mirror.add(newp);
		}

		return addMirrorToPath(path, mirror);
	}

	/**
	 * Creates a straight part between two locations with points equal to the amount
	 * of given steps.
	 *
	 * @param start start location
	 * @param end   end location
	 * @param steps amount of points
	 * @return a list of all the points created
	 */
	public static List<Vector3D> createStraightLine(Vector3D start, Vector3D end, int steps) {

		List<Vector3D> path = new ArrayList<>();
		Vector3D last = start;
		path.add(last);
		Vector3D dv = end.subtract(start).scalarMultiply(1.0 / steps);

		for (int i = 0; i < steps; i++) {
			last = last.add(dv);
			path.add(last);
		}

		return path;
	}

	/**
	 * Re-samples the given path. The points are taken from the path at the given
	 * distances from the first point of the path. Any point of re-sampled path will
	 * coincide one of the path points, so the distances are only approximately
	 * respected.
	 * 
	 * @param path      The path to re-sample.
	 * @param distances A list of distances from the first point of the path.
	 * @return The re-sampled path.
	 */
	public static List<Vector3D> reSamplePath(List<Vector3D> path, List<Double> distances) {

		List<Vector3D> resampled = new ArrayList<>();

		for (Double dist : distances) {
			int i = getClosestIndex(path, dist, false);
			resampled.add(path.get(i));
		}

		return resampled;
	}

	/**
	 * Up-samples the given path. The step size should not be bigger than any of the
	 * segments in the path.
	 *
	 * @param path     The path to up-sample
	 * @param stepSize Step size for the up-sampling.
	 * 
	 * @return The up-sampled path.
	 */
	public static List<Vector3D> upSamplePath(List<Vector3D> path, double stepSize) {
		List<Vector3D> resampled = new ArrayList<>();

		if (stepSize <= 0)
			throw new IllegalArgumentException("Step size for the upsampling must be bigger than zero !");

		for (int i = 1; i < path.size(); i++) {
			double segLength = path.get(i).distance(path.get(i - 1));

			if (stepSize > segLength)
				throw new IllegalArgumentException(
						"Segment " + i + " in the path is shorter than the step size for the upsampling ");

			Vector3D dir = path.get(i).subtract(path.get(i - 1)).normalize();
			int nPoints = (int) (segLength / stepSize);

			Vector3D deltaSegment = dir.scalarMultiply(segLength / nPoints);

			for (int j = 0; j < nPoints; j++) {
				Vector3D p = path.get(i - 1).add(deltaSegment.scalarMultiply(j));
				resampled.add(p);
			}

		}

		return resampled;
	}

	/**
	 * Check if the path contains the given point within the given tolerance.
	 * 
	 * @param path      The path to be checked.
	 * @param point     The point tp be checked against.
	 * @param tolerance The tolerance.
	 * @return True if any point in the path is closer to the point than the
	 *         tolerance distance. False otherwise.
	 */
	public static boolean hasPoint(List<Vector3D> path, Vector3D point, double tolerance) {
		for (Vector3D p : path) {
			if (p.distance(point) < tolerance)
				return true;
		}
		return false;
	}

	// TODO Make the direction second order
	/**
	 * This method takes a list of Vector3D points which form a 3D path and
	 * calculates the normalized tangent vector along the path at the given index.
	 * If the closed flag is set to true, the path is treated as a closed loop and
	 * the tangent vector is calculated accordingly. If the closed flag is set to
	 * false, the tangent vector is calculated as if the path is open and the first
	 * and last points are not connected. The tangent vector is calculated by the
	 * second order finite difference method.
	 * 
	 * @param path   The list of Vector3D points which form the path
	 * 
	 * @param closed A flag indicating whether the path is closed or not
	 * 
	 * @param index  The index of the point along the path for which the tangent
	 *               vector is to be calculated
	 * 
	 * @return The unit tangent vector at the given index
	 */
	public static TangentVector3D getTangentVector(List<Vector3D> path, boolean closed, int index) {
		Vector3D tv = null;

		if (closed) {
			if (index == 0) {
				tv = path.get(1).subtract(path.get(path.size() - 1));
			} else if (index == path.size() - 1) {
				tv = path.get(0).subtract(path.get(path.size() - 2));
			} else {
				tv = path.get(index + 1).subtract(path.get(index - 1));
			}
		} else {
			if (index == 0) {
				tv = path.get(1).subtract(path.get(0));
			} else if (index == path.size() - 1) {
				tv = path.get(path.size() - 1).subtract(path.get(path.size() - 2));
			} else {
				tv = path.get(index + 1).subtract(path.get(index - 1));
			}
		}

		return new TangentVector3D(path.get(index), tv.normalize());
	}

	/**
	 * Search the point in the path closest to the given points.
	 *
	 * @param path  The path
	 * @param point The point
	 * @return The index of the closest point on the path.
	 */
	public static int getClosestIndex(List<Vector3D> path, Vector3D point) {

		int closest = 0;
		double bestDist = Double.MAX_VALUE;

		for (int i = 0; i < path.size(); i++) {
			Vector3D p = path.get(i);
			double dist = p.subtract(point).getNorm();

			if (dist < bestDist) {
				closest = i;
				bestDist = dist;
			}
		}

		return closest;
	}

	/**
	 * This method takes a list of Vector3D points which form a 3D path and returns
	 * the index of the point which is closest to the given longitudinal position
	 * counted from the first point in the path. If the closed flag is set to true,
	 * the path is treated as a closed loop and the closest point is calculated
	 * accordingly.
	 * 
	 * @param path       The path
	 * @param longiPos   The longitudinal position at which the closest point is
	 *                   calculated.
	 * @param closedPath Should be set to true if the path forms a closed loop. If
	 *                   true, the line segment formed by the first point and the
	 *                   last point in the path also considered as a segment of the
	 *                   path.
	 * @return The index of the closest point.
	 */
	public static int getClosestIndex(List<Vector3D> path, double longiPos, boolean closedPath) {

		if (longiPos == 0)
			return 0;

		double summ = 0;
		int i;

		for (i = 1; i < path.size(); i++) {
			double dist = path.get(i).distance(path.get(i - 1));

			if (summ + dist >= longiPos) {
				if (Math.abs(summ + dist - longiPos) > Math.abs(summ - longiPos)) {
					return i - 1;
				}
				return i;
			}

			summ += dist;

			if (i == path.size() - 1) { // end of path
				if (closedPath) {
					return 0;
				}
			}
		}

		return path.size() - 1;
	}

	/**
	 * Gets the distances of points along the path from the beginning by summing up
	 * the arc-lengths of the segments between the points. The arc-length of a
	 * segment is calculated from the radius of the circle passing through 3 points.
	 * The first and last segments are calculated from the first 3 points and the
	 * last 3 points respectively. The intermediate segments are calculated from the
	 * previous, current and next points.
	 *
	 * @param path   The path to be calculated.
	 * @param closed If true, the path is treated as a closed loop. An arritional
	 *               point will be added at the end of the path to close the loop.
	 * @return A map contaiining the distances of path points from the beginning of
	 *         the path.
	 */
	public static Map<Integer, Double> getArcDistances(List<Vector3D> path, boolean closed) {

		if (path.size() < 3)
			throw new IllegalArgumentException(
					"The path must have at least 3 points to calculate the length with second order.");

		Map<Integer, Double> distances = new HashMap<>();
		double length = Arc3D.calculateArcLength(path.get(0), path.get(1), path.get(2), true);
		distances.put(0, 0.0);
		distances.put(1, length);

		for (int i = 2; i < path.size(); i++) {
			double al = Arc3D.calculateArcLength(path.get(i - 2), path.get(i - 1), path.get(i), false);
			length += al;
			distances.put(i, length);
		}

		int last = path.size() - 1;
		if (closed) {
			double al = Arc3D.calculateArcLength(path.get(last - 1), path.get(last), path.get(0), false);
			length += al;
			distances.put(last + 1, length);
		}
		
		assert(distances.size()==path.size()+(closed?1:0));

		return distances;
	}

	/**
	 * Gets the distance between two points along the path by summing the line
	 * segments between the given indices.
	 *
	 * @param from start index
	 * @param to   end index
	 * @param path the path to traverse
	 * @return the distance between the points
	 */
	public static double getDistance(int from, int to, List<Vector3D> path) {

		if (to < from)
			throw new IllegalArgumentException(
					"Can't get distance on the path: The  second index can not be smaller than the first");
		double length = 0;
		for (int i = from; i < to; i++) {
			length += path.get(i + 1).subtract(path.get(i)).getNorm();
		}

		return length;
	}

	/**
	 * Extends the given path at the beginning or at the end by adding a point at
	 * the given distance to the path in the direction of the first or last vector
	 * in the path.
	 * 
	 * @param path     The path to be extended.
	 * @param distance The distance of the extension point from the beginning or
	 *                 from the end in the same units the path has (can be any
	 *                 unit).
	 * @param atTheEnd If true, the point will be added at the end , otherwise at
	 *                 the beginning.
	 */
	public static void extendPath(List<Vector3D> path, double distance, boolean atTheEnd) {

		if (distance == 0)
			return;

		Vector3D direction;
		Vector3D point;

		int lastIdx = path.size() - 1;

		if (atTheEnd) {
			direction = path.get(lastIdx).subtract(path.get(lastIdx - 1)).normalize();
			point = path.get(lastIdx);
		} else {
			direction = path.get(0).subtract(path.get(1)).normalize();
			point = path.get(0);
		}

		Vector3D extension = point.add(direction.scalarMultiply(distance));
		if (atTheEnd)
			path.add(extension);
		else
			path.add(0, extension);

	}

	/**
	 * Calculates a circular arc in the X, Z plane. X points left with positive
	 * bending angle and Y points up. This corresponds to right handed coordinate
	 * system.
	 * 
	 * @param r          Radius of the circle
	 * @param deflection Total angle of the arc [radians]
	 * @param n          Number of points in the arc.
	 * 
	 * @return List or arc points.
	 */
	// VERY CAREFULT IF MODIFYING THIS, USED BY THE SEQUECE CLASS !!
	public static List<Vector3D> getArc(double r, double deflection, int n) {
		double dfi = deflection / (n - 1);

		List<Vector3D> arc = new ArrayList<>();
		for (int i = 0; i < n; i++) {
			double x = -r * Math.cos(i * dfi) + r;
			double z = r * Math.sin(i * dfi);
			Vector3D p = new Vector3D(x, 0, z);
			arc.add(p);
		}

		assert (Math.abs(PathUtils.calculatePathLength(arc, false) - (deflection * r)) < 1e-9);

		return arc;
	}

	/**
	 * Applies the rotation, then the translation to the given path.
	 * 
	 * @param path        A list of points to be transformed.
	 * @param rot         A rotation to be applied first.
	 * @param translation A Translation to be applied after the rotation.
	 * @return The transformed list of points.
	 */
	public static List<Vector3D> transform(List<Vector3D> path, Rotation rot, Vector3D translation) {
		List<Vector3D> res = new ArrayList<>();
		for (Vector3D p : path) {
			res.add(rot.applyTo(p.add(translation)));
		}
		return res;
	}

	/**
	 * Create a path for a bending element with the given deflection angle, bend
	 * radius, extension lengths at both ends. The path consists of a straight
	 * segment at the entry and the exits and an arc. The center point of the arc is
	 * the origin.
	 *
	 * @param deflectionAngle The deflection angle of the element. [radian]
	 * @param bendRadius      The radius of the bending. [arbitrary length unit]
	 * @param nSegments       The the number of line segments in the path. It must
	 *                        be even to assure symmetry.
	 * @param extentionLength The length of the straight parts at both ends.
	 *                        [arbitrary length unit]
	 * @param sectorBend      True for a sector bending and false for rectangular
	 *                        bending. For a rectangular bending the entry direction
	 *                        of the beam is the +Z axis. For sector bending the
	 *                        angle between the beam entry direction and the +Z axis
	 *                        is deflection/2.
	 * 
	 * @return a list of points making up the path of the bending.
	 */
	public static List<Vector3D> createBendingPath(double deflectionAngle, double bendRadius, int nSegments,
			double extentionLength, boolean sectorBend) {

		if (nSegments <= 0 || nSegments % 2 != 0)
			throw new IllegalArgumentException(
					"Number of line segments in the arc must be a positive even number to assure symmetry of the arc. n = "
							+ nSegments);
		if (bendRadius <= 0)
			throw new IllegalArgumentException(
					"The bending radius must be a positive number bigger than zero ! R= " + bendRadius);

		double arcLength = Math.abs(bendRadius * deflectionAngle);
		double totalLength = arcLength + 2 * extentionLength;

		int nExtSegs = (int) (nSegments * extentionLength / totalLength);// number of extension segments at one side
		int nHalfArc = (nSegments - 2 * nExtSegs) / 2; // number of segments in a half arc
		ArrayList<Vector3D> l = new ArrayList<>();

		Vector3D shift = Vector3D.MINUS_I.scalarMultiply(bendRadius);

		for (int i = -nHalfArc; i <= nHalfArc; i++) {
			double fi = i * Math.abs(deflectionAngle) / (2 * nHalfArc);
			Vector3D v = new Vector3D(bendRadius * Math.cos(fi), 0, bendRadius * Math.sin(fi));
			l.add(v.add(shift));
		}

		Vector3D v = l.get(0).subtract(shift);
		Vector3D negEntryDir = (new Vector3D(-v.getZ(), v.getY(), v.getX())).normalize();
		v = l.get(l.size() - 1).subtract(shift);
		Vector3D posExitDir = (new Vector3D(-v.getZ(), v.getY(), v.getX())).normalize();
		double dl = extentionLength / (nExtSegs + 1);

		for (int i = 0; i <= nExtSegs; i++) {
			Vector3D first = l.get(0);
			l.add(0, negEntryDir.scalarMultiply(-dl).add(first));

			Vector3D last = l.get(l.size() - 1);
			l.add(posExitDir.scalarMultiply(dl).add(last));

		}

		Rotation rot = Rotation.IDENTITY;
		if (!sectorBend)
			rot = new Rotation(Vector3D.PLUS_J, -deflectionAngle / 2.0, RotationConvention.VECTOR_OPERATOR);

		if (deflectionAngle < 0) {
			ArrayList<Vector3D> nl = new ArrayList<>();
			for (Vector3D w : l) {
				Vector3D nv = new Vector3D(-w.getX(), w.getY(), w.getZ());
				nl.add(rot.applyTo(nv));
			}
			l = nl;
		}

		return l;
	}

	/**
	 * Calculate the sum of the lengths of segments in the path by summing the arc
	 * lengths of the segments. The arc length of a segment is calculated from the
	 * radius of the circle passing through 3 points. The first and last segments
	 * are calculated from the first 3 points and the last 3 points respectively.
	 * The intermediate segments are calculated from the previous, current and next
	 * points.
	 * 
	 * @param path   A list of 3D points.
	 * 
	 * @param closed If the path is closed, the arc distance between the last point
	 *               and first will be counted in the length.
	 * 
	 * @return The length of the path.
	 */
	public static double calculatePathLength(List<Vector3D> path, boolean closed) {

		Map<Integer, Double> dl = getArcDistances(path, closed);
		int last = path.size() - 1;
		if (closed)
			last++;

		return dl.get(last);
	}

	/// API ends here

	private PathUtils() {
	}

	/**
	 * Adds the path and the mirror of the path together.
	 *
	 * @param path   ArrayList of Vector3D objects that make up the (half) path
	 * @param mirror mirror of the path
	 * @return mirrored path
	 */
	private static List<Vector3D> addMirrorToPath(List<Vector3D> path, List<Vector3D> mirror) {
		List<Vector3D> res = new ArrayList<>();

		for (int i = mirror.size() - 1; i >= 0; i--) {
			res.add(mirror.get(i));
		}

		res.addAll(path);

		return res;

	}

}
