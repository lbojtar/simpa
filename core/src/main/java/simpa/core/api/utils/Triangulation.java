package simpa.core.api.utils;

import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * A record to fold data for triangulation of extruded surfaces with possible end cups.
 */
public record Triangulation(List<Vector3D> vertices,List<int[]> facets) {

}
