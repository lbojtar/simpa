/**
 * This package contains all utils that can be used to set up the SIMPA Core library.
 */
package simpa.core.api.utils;