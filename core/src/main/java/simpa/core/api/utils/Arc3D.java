package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class Arc3D {

	// based on the code from
	// https://github.com/sergarrido/random/tree/master/circle3d
	/**
	 * Calculates the length of the arc by fitting a circle to 3 points in 3D space
	 * We assume that the points are on a curve in the order of p1, p2, p3. The
	 * special case when they are on a line is handled.
	 * 
	 * @param p1      point 1
	 * @param p2      point 2
	 * @param p3      point 3
	 * @param boolean first If true the arc length between points p1 and p2 is
	 *                calculated. If false the arc length between points p2 and p3
	 *                is calculated.
	 */
	public static double calculateArcLength(Vector3D p1, Vector3D p2, Vector3D p3, boolean first) {

		double radius;

		Vector3D base1 = p2.subtract(p1);
		Vector3D base2 = p3.subtract(p1);
		double v1v1, v2v2, v1v2;
		v1v1 = base1.dotProduct(base1);
		v2v2 = base2.dotProduct(base2);
		v1v2 = base1.dotProduct(base2);

		double a = (v1v1 * v2v2 - v1v2 * v1v2);
		double straightLimit = base1.getNorm() * base2.getNorm() * 1e-12;// nearly straight line
		if (Math.abs(a) < straightLimit) {
			if (first)
				return base1.getNorm();
			else
				return p3.subtract(p2).getNorm();
		}

		double base = 0.5 / a;
		double k1 = base * v2v2 * (v1v1 - v1v2);
		double k2 = base * v1v1 * (v2v2 - v1v2);
		Vector3D center = p1.add(base1.scalarMultiply(k1)).add(base2.scalarMultiply(k2)); // center of the circle
		radius = center.subtract(p1).getNorm();

		Vector3D v1 = p1.subtract(center);
		Vector3D v2 = p2.subtract(center);
		Vector3D v3 = p3.subtract(center);
		if (first)
			return radius * Vector3D.angle(v1, v2);

		return radius * Vector3D.angle(v2, v3);

	}
}
