/**
* Copyright (c) 2022 Sidney Goossens
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of the OcTree algorithm for SurfacePoints, with methods for
 * finding SurfacePoints and finding neighbors of a SurfacePoint in a given
 * radius.
 */
public class OcTree {
	OcTreeEntry point;
	boolean empty = false;

	double minx, miny, minz;
	double maxx, maxy, maxz;

	OcTree[] children;

	double error = 0;// 1E-10;

	int topLeftFront = 0;
	int topRightFront = 1;
	int bottomRightFront = 2;
	int bottomLeftFront = 3;
	int topLeftBottom = 4;
	int topRightBottom = 5;
	int bottomRightBack = 6;
	int bottomLeftBack = 7;

	private OcTree() {
		empty = true;
	}

	private OcTree(OcTreeEntry point) {
		this.point = point;
	}

	/**
	 * Sets up an Octree with the given boundary.
	 *
	 * @param x1 x value of left most point
	 * @param y1 y value of left most point
	 * @param z1 z value of left most point
	 * @param x2 x value of right most point
	 * @param y2 y value of right most point
	 * @param z2 z value of right most point
	 */
	public OcTree(double x1, double y1, double z1, double x2, double y2, double z2) {
		if (x2 < x1 || y2 < y1 || z2 < z1) {
			throw new IllegalArgumentException("Boundary points are not valid");
		}

		minx = x1;
		miny = y1;
		minz = z1;
		maxx = x2;
		maxy = y2;
		maxz = z2;

		children = new OcTree[8];
		for (int i = 0; i < children.length; i++) {
			children[i] = new OcTree();
		}
	}

	/**
	 * Inserts a new SurfacePoint in the Octree and creates new branches where
	 * necessary.
	 *
	 * @param entry point to add
	 */
	public void insert(OcTreeEntry entry) {
		// check if point already in octree
		if (find(entry)) {
			throw new IllegalArgumentException("Duplicate point.");
		}

		Vector3D loc = entry.getLocation();
		double x = loc.getX();
		double y = loc.getY();
		double z = loc.getZ();

		if (x < minx || y < miny || z < minz || x > maxx || y > maxy || z > maxz) {
			throw new IllegalArgumentException("OcTree: Point is out of bound " + loc);
		}

		double midx = (minx + maxx) / 2;
		double midy = (miny + maxy) / 2;
		double midz = (minz + maxz) / 2;

		// check in which part of the division the point should go
		int pos = getPos(x, y, z, midx, midy, midz);

		// If an empty node is encountered
		if (children[pos].empty) {
			children[pos] = new OcTree(entry);
		}

		// If an internal node is encountered
		else if (children[pos].point == null) {
			children[pos].insert(entry);
		} else {
			OcTreeEntry existingPoint = children[pos].point;
			children[pos].point = null;
			if (pos == topLeftFront) {
				children[pos] = new OcTree(minx, miny, minz, midx, midy, midz);
			} else if (pos == topRightFront) {
				children[pos] = new OcTree(midx + error, miny, minz, maxx, midy, midz);
			} else if (pos == bottomRightFront) {
				children[pos] = new OcTree(midx + error, midy + error, minz, maxx, maxy, midz);
			} else if (pos == bottomLeftFront) {
				children[pos] = new OcTree(minx, midy + error, minz, midx, maxy, midz);
			} else if (pos == topLeftBottom) {
				children[pos] = new OcTree(minx, miny, midz + error, midx, midy, maxz);
			} else if (pos == topRightBottom) {
				children[pos] = new OcTree(midx + error, miny, midz + error, maxx, midy, maxz);
			} else if (pos == bottomRightBack) {
				children[pos] = new OcTree(midx + error, midy + error, midz + error, maxx, maxy, maxz);
			} else {
				children[pos] = new OcTree(minx, midy + error, midz + error, midx, maxy, maxz);
			}
			children[pos].insert(existingPoint);
			children[pos].insert(entry);
		}
	}

	/**
	 * Returns true if a point is found in the Octree.
	 *
	 * @param entry point to search.
	 * @return boolean whether the point is found in the tree.
	 */
	boolean find(OcTreeEntry entry) {
		Vector3D loc = entry.getLocation();
		double x = loc.getX();
		double y = loc.getY();
		double z = loc.getZ();

		if (x < minx || y < miny || z < minz || x > maxx || y > maxy || z > maxz) {
			return false;
		}

		double midx = (minx + maxx) / 2;
		double midy = (miny + maxy) / 2;
		double midz = (minz + maxz) / 2;

		int pos = getPos(x, y, z, midx, midy, midz);

		if (children[pos].empty) {
			return false;
		} else if (children[pos].point == null) {
			return children[pos].find(entry);
		} else {
			return children[pos].point.getLocation() == loc;
		}
	}

	/**
	 * Method that checks in which part of the divided box to look for the point to
	 * find.
	 *
	 * @param x    - point's x location.
	 * @param y    - point's y location.
	 * @param z    - point's z location.
	 * @param midx - middle x point of the box.
	 * @param midy - middle y point of the box.
	 * @param midz - middle z point of the box.
	 * @return index of the box to continue searching in.
	 */
	private int getPos(double x, double y, double z, double midx, double midy, double midz) {
		int pos;
		// check in which part of the division the point should go
		if (x <= midx) {
			if (y <= midy) {
				if (z <= midz) {
					pos = topLeftFront;
				} else {
					pos = topLeftBottom;
				}
			} else {
				if (z <= midz) {
					pos = bottomLeftFront;
				} else {
					pos = bottomLeftBack;
				}
			}
		} else {
			if (y <= midy) {
				if (z <= midz) {
					pos = topRightFront;
				} else {
					pos = topRightBottom;
				}
			} else {
				if (z <= midz) {
					pos = bottomRightFront;
				} else {
					pos = bottomRightBack;
				}
			}
		}
		return pos;
	}

	/**
	 * Finds a list of SurfacePoints that are in the given radius of a given point.
	 *
	 * @param center the origin to search from
	 * @param radius the search radius
	 * @return a list of SurfacePoint inside the radius
	 */
	public List<OcTreeEntry> findInRange(Vector3D center, double radius) {
		List<OcTreeEntry> points = new ArrayList<>();

		double midx = (minx + maxx) / 2;
		double midy = (miny + maxy) / 2;
		double midz = (minz + maxz) / 2;

		List<Integer> indexes = getIntersections(midx, midy, midz, center, radius);

		for (Integer pos : indexes) {
			if (!children[pos].empty) {
				if (children[pos].point == null) {
					// check if sphere intersects with plane of box
					points.addAll(children[pos].findInRange(center, radius));
				} else {
					OcTreeEntry sp = children[pos].point;
					if (center.equals(sp.getLocation())) {

						continue;
					}
					if (center.subtract(sp.getLocation()).getNorm() < radius) {
						points.add(sp);
					}
				}
			}
		}

		return points;
	}

	
	/**
	 * Method that gets a list of intersections of the radius around the center
	 * point with child boxes in the OcTree.
	 *
	 * @param midx   - middle x point of the (current) box.
	 * @param midy   - middle y point of the (current) box.
	 * @param midz   - middle z point of the (current) box.
	 * @param center - center to scan around.
	 * @param radius - the search radius.
	 * @return a list of indexes of boxes that intersect with the radius.
	 */
	private List<Integer> getIntersections(double midx, double midy, double midz, Vector3D center, double radius) {
		List<Integer> indexes = new ArrayList<>();

		if (intersectsWithBox(new Vector3D(minx, miny, minz), new Vector3D(midx, midy, midz), center, radius)) {
			indexes.add(0);
		}
		if (intersectsWithBox(new Vector3D(midx + error, miny, minz), new Vector3D(maxx, midy, midz), center, radius)) {
			indexes.add(1);
		}
		if (intersectsWithBox(new Vector3D(minx, midy + error, minz), new Vector3D(maxx, maxy, midz), center, radius)) {
			indexes.add(2);
		}
		if (intersectsWithBox(new Vector3D(minx, midy + error, minz), new Vector3D(midx, maxy, midz), center, radius)) {
			indexes.add(3);
		}
		if (intersectsWithBox(new Vector3D(minx, miny, midz + error), new Vector3D(midx, midy, maxz), center, radius)) {
			indexes.add(4);
		}
		if (intersectsWithBox(new Vector3D(midx + error, miny, midz + error), new Vector3D(maxx, midy, maxz), center,
				radius)) {
			indexes.add(5);
		}
		if (intersectsWithBox(new Vector3D(midx + error, midy + error, midz + error), new Vector3D(maxx, maxy, maxz),
				center, radius)) {
			indexes.add(6);
		}
		if (intersectsWithBox(new Vector3D(minx, midy + error, midz + error), new Vector3D(midx, maxy, maxz), center,
				radius)) {
			indexes.add(7);
		}

		return indexes;
	}

	/**
	 * Method for checking if the given radius around the given sphereCenter
	 * intersects with the box defined with the topLeft and bottomRight corner.
	 * 
	 * @param topLeft      - top left corner of the box.
	 * @param bottomRight  - bottom right corner of the box.
	 * @param sphereCenter - center to search around.
	 * @param r            - radius of the search.
	 * @return a boolean whether the sphere intersects the box.
	 */
	private boolean intersectsWithBox(Vector3D topLeft, Vector3D bottomRight, Vector3D sphereCenter, double r) {
		double distSquared = r * r;
		if (sphereCenter.getX() < topLeft.getX()) {
			double dist = sphereCenter.getX() - topLeft.getX();
			distSquared -= dist * dist;
		} else if (sphereCenter.getX() > bottomRight.getX()) {
			double dist = sphereCenter.getX() - bottomRight.getX();
			distSquared -= dist * dist;
		}
		if (sphereCenter.getY() < topLeft.getY()) {
			double dist = sphereCenter.getY() - topLeft.getY();
			distSquared -= dist * dist;
		} else if (sphereCenter.getY() > bottomRight.getY()) {
			double dist = sphereCenter.getY() - bottomRight.getY();
			distSquared -= dist * dist;
		}
		if (sphereCenter.getZ() < topLeft.getZ()) {
			double dist = sphereCenter.getZ() - topLeft.getZ();
			distSquared -= dist * dist;
		} else if (sphereCenter.getZ() > bottomRight.getZ()) {
			double dist = sphereCenter.getZ() - bottomRight.getZ();
			distSquared -= dist * dist;
		}

		return distSquared > 0;
	}

}
