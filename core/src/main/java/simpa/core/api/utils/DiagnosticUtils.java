/**
* Copyright (c) 2022 Lajos Bojtár
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the Software), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in all
* copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED AS IS, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*/
package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import simpa.core.api.Evaluator;
import simpa.core.api.LengthUnit;
import simpa.core.api.PointSource;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Utility methods for diagnosing configurations.
 */
public class DiagnosticUtils {

	/////////// API starts here

	/**
	 * Write the field components into a file along a curve. A shift can be given as
	 * a local addition to the curve. In the output file the components of the
	 * fields are expresse in the global coordinate system
	 *
	 * @param pp         A Potential provider instance.
	 * @param curve      Curve on which the field is calculated.
	 * @param shift      Shift applied to the curve locally in the same unit as the
	 *                   curve. The shift coordinates are taken in the local frame
	 *                   to calculate the shift vector which is added to the point
	 *                   on the curve in the global frame.
	 * @param outfile    The X,Y,Z, coordinates and field values are written into
	 *                   this file.
	 * 
	 * @param lfl        List of local frames for each point on the curve.
	 * @param localFrame If true, the fields are written in the local frame,
	 *                   otherwise in the global frame.
	 * 
	 * @throws OutOfApertureException when a point is out of aperture.
	 * @throws KnownFatalException    when an internal error happens.
	 */
	public static void getFieldsOnCurve(PotentialProvider pp, List<Vector3D> curve, Vector3D shift, String outfile,
			List<LocalFrame> lfl, boolean localFrame) throws OutOfApertureException, KnownFatalException {
		if (outfile == null)
			throw new IllegalArgumentException("The output file is null ! ");

		Vector3D pos;

		StringBuilder sb = new StringBuilder();
		String frame = localFrame ? "LOCAL" : "GLOBAL";
		sb.append("# S X Y Z Bx By Bz Ex Ey Ez ( Fields are in the " + frame + " frame, distances in [m])"
				+ System.lineSeparator());

		Map<Integer, Double> dl = PathUtils.getArcDistances(curve, false);

		for (int i = 0; i < curve.size(); i++) {
			LocalFrame lf = lfl.get(i);
			pos = lf.getLocalShiftedPoint(curve.get(i), shift);
			String poss = pos.getX() + " " + pos.getY() + " " + pos.getZ();
			Vector3D gb = CalculatorUtils.getBField(pp.getPotentialsWithDerivatives(pos));
			Vector3D ge = CalculatorUtils.getEField(pp.getPotentialsWithDerivatives(pos));

			double bx, by, bz, ex, ey, ez;
			if (localFrame) {
				bx = lf.lx().dotProduct(gb);
				by = lf.ly().dotProduct(gb);
				bz = lf.lz().dotProduct(gb);
				ex = lf.lx().dotProduct(ge);
				ey = lf.ly().dotProduct(ge);
				ez = lf.lz().dotProduct(ge);
			} else {
				bx = gb.getX();
				by = gb.getY();
				bz = gb.getZ();
				ex = ge.getX();
				ey = ge.getY();
				ez = ge.getZ();
			}

			double longiPos = dl.get(i);
			sb.append(longiPos + " " + poss + " " + bx + " " + by + " " + bz + " " +
					ex + " " + ey + " " + ez + System.lineSeparator());

		}
		FileUtils.writeTextFile(outfile, sb.toString());
	}

	/**
	 * Write the field components into a file along a curve. A shift can be given as
	 * a local addition to the curve. In the output file the components of the
	 * fields are in curve-linear coordinates. The +Z direction coincides with the
	 * given curve such that it points from Pn to Pn+1, where Pn is a point on the
	 * curve.
	 *
	 * @param sources List of sources.
	 * @param curve   Curve on which the field is calculated.
	 * @param shift   Shift applied to the curve locally in the same unit as the
	 *                curve. The x direction points outward of the machine, the Y
	 *                direction is up, and the Z direction is the same as the
	 *                difference between consecutive points in the curve.
	 * @param outfile The X,Y,Z, coordinates and field values are written into this
	 *                file.
	 * @param lUnit   Length unit in the output file
	 * 
	 * @throws OutOfApertureException when a point is out of aperture.
	 * @throws KnownFatalException    when an internal error happens.
	 */
	public static void getFieldsOnCurve(List<PointSource> sources, List<Vector3D> curve, Vector3D shift, String outfile,
			LengthUnit lUnit, List<LocalFrame> lfl) throws OutOfApertureException, KnownFatalException {
		if (outfile == null)
			throw new IllegalArgumentException("The output file is null ! ");

		Vector3D pos;

		StringBuilder sb = new StringBuilder();
		sb.append("#X Y Z Bx By Bz Ex Ey Ez (in the global frame)" + System.lineSeparator());

		List<Vector3D> shiftedCurve = new ArrayList<>();

		for (int i = 0; i < curve.size(); i++) {
			pos = lfl.get(i).getLocalShiftedPoint(curve.get(i), shift);
			shiftedCurve.add(pos);
		}

		Evaluator ev = new Evaluator();
		double[][][] pa = ev.evaluateAtTargets(sources, shiftedCurve);

		for (int i = 0; i < curve.size(); i++) {
			String poss = shiftedCurve.get(i).getX() + " " + shiftedCurve.get(i).getY() + " "
					+ shiftedCurve.get(i).getZ();
			Vector3D b = CalculatorUtils.getBField(pa[i]);
			Vector3D e = CalculatorUtils.getEField(pa[i]);
			sb.append(poss + " " + b.getX() + " " + b.getY() + " " + b.getZ() + " " + e.getX() + " " + e.getY() + " "
					+ e.getZ() + System.lineSeparator());
		}

		FileUtils.writeTextFile(outfile, sb.toString());
	}

	/**
	 * Integrates the B field on the curve and returns the integral.
	 *
	 * @param sources List of point sources.
	 * @param curve   the curve to integrate with.
	 * @param shift   the shift to the points in the curve.
	 * @return integral
	 * @throws OutOfApertureException when a point is out of aperture.
	 * @throws KnownFatalException    when an internal error happens.
	 */
	public static Vector3D integrateBFieldOnCurve(List<PointSource> sources, List<Vector3D> curve, Vector3D shift,
			List<LocalFrame> lfl)
			throws OutOfApertureException, KnownFatalException {
		return integrateFieldOnCurve(sources, curve, shift, DiagnosticType.B_FIELD_INTEGRAL, lfl);
	}

	/**
	 * Integrates the E field on the curve and returns the integral.
	 *
	 * @param sources List of point sources.
	 * @param curve   The curve to integrate with.
	 * @param shift   the shift to the points in the curve.
	 * @return integral
	 * @throws OutOfApertureException when a point is out of aperture.
	 * @throws KnownFatalException    when an internal error happens.
	 */
	public static Vector3D integrateEFieldOnCurve(List<PointSource> sources, List<Vector3D> curve, Vector3D shift,
			List<LocalFrame> lfl)
			throws OutOfApertureException, KnownFatalException {
		return integrateFieldOnCurve(sources, curve, shift, DiagnosticType.E_FIELD_INTEGRAL, lfl);
	}

	///////// API ends here
	private static Vector3D integrateFieldOnCurve(List<PointSource> sources, List<Vector3D> curve, Vector3D shift,
			DiagnosticType type, List<LocalFrame> lfl) throws OutOfApertureException, KnownFatalException {

		if (type != DiagnosticType.B_FIELD_INTEGRAL && type != DiagnosticType.E_FIELD_INTEGRAL) {
			throw new IllegalArgumentException("The diagnostic type must be field integral on curve type: B or E");
		}

		List<Vector3D> shiftedCurve = new ArrayList<>();
		for (int i = 0; i < curve.size(); i++) {
			Vector3D pos = lfl.get(i).getLocalShiftedPoint(curve.get(i), shift);
			shiftedCurve.add(pos);
		}

		Evaluator ev = new Evaluator();
		double[][][] pa = ev.evaluateAtTargets(sources, shiftedCurve);

		Vector3D integral = new Vector3D(0, 0, 0);

		Vector3D pos;
		Vector3D fv = null;
		Vector3D prev = null;
		pos = curve.get(0);

		if (type == DiagnosticType.B_FIELD_INTEGRAL)
			prev = CalculatorUtils.getBField(pa[0]);

		if (type == DiagnosticType.E_FIELD_INTEGRAL) {
			prev = CalculatorUtils.getEField(pa[0]);
		}

		for (int i = 1; i < curve.size(); i++) {

			pos = lfl.get(i).getLocalShiftedPoint(curve.get(i), shift);

			if (type == DiagnosticType.B_FIELD_INTEGRAL)
				fv = CalculatorUtils.getBField(pa[i]);

			if (type == DiagnosticType.E_FIELD_INTEGRAL) {
				fv = CalculatorUtils.getEField(pa[i]);
			}

			double dl = pos.subtract(curve.get(i - 1)).getNorm();
			Vector3D db = fv.add(prev).scalarMultiply(dl / 2.0); // midpoint integration
			integral = integral.add(db);

			prev = fv;
		}

		return integral;
	}

	private enum DiagnosticType {
		E_FIELD_INTEGRAL, B_FIELD_INTEGRAL, E_FIELD_ON_CURVE, B_FIELD_ON_CURVE
	}

}
