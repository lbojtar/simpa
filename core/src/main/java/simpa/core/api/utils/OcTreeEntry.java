package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public interface OcTreeEntry {
    Vector3D getLocation();
}
