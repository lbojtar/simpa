package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

public class TextStlWriter {

	public static void writeSTLTextFile(Triangulation t, String file) {

		StringBuilder sb = new StringBuilder();
		final String NL = System.lineSeparator();

		sb.append("solid beamregion").append(NL);

		for (int[] fa : t.facets()) {
			Vector3D v0=t.vertices().get(fa[0]);
			Vector3D v1=t.vertices().get(fa[1]);
			Vector3D v2=t.vertices().get(fa[2]);
			
			assert (v0.subtract(v1).getNorm() > 0);
			assert (v1.subtract(v2).getNorm() > 0);
			assert (v0.subtract(v2).getNorm() > 0);

			Vector3D nv = Vector3D.crossProduct(v1.subtract(v0), v2.subtract(v1)).normalize();
			sb.append("facet normal ").append(nv.getX()).append(" ").append(nv.getY()).append(" ").append(nv.getZ())
					.append(NL);
			sb.append("\t outer loop").append(NL);
			sb.append("\t\t vertex ").append(v0.getX()).append(" ").append(v0.getY()).append(" ")
					.append(v0.getZ()).append(NL);
			sb.append("\t\t vertex ").append(v1.getX()).append(" ").append(v1.getY()).append(" ")
					.append(v1.getZ()).append(NL);
			sb.append("\t\t vertex ").append(v2.getX()).append(" ").append(v2.getY()).append(" ")
					.append(v2.getZ()).append(NL);
			sb.append("\t endloop").append(NL);
			sb.append("endfacet").append(NL);
		}
		sb.append("endsolid beamregion").append(NL);
		FileUtils.writeTextFile(file, sb.toString());
	}
}
