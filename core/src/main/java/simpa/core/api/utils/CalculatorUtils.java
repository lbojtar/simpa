/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api.utils;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.PhysicsConstants;
import simpa.core.api.PotentialProvider;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.api.exceptions.OutOfApertureException;
import simpa.core.api.track.Particle;

/**
 * Utilities for calculation.
 */
public class CalculatorUtils {

	private static double[][] pots; // wee store the potential in case if needed

///////////////// API start here///////////////////////////////////////////////////

	/**
	 * Evaluates the magnetic field at the given position from the given
	 * PotentialProvider instance. This should not be directly used for tracking. It
	 * is intended for diagnostic.
	 * 
	 * @param pp-  A PotentialProvider instance.
	 * @param pos- The position of the field evaluation.
	 * @return - The B field.
	 * @throws OutOfApertureException when the pos attribute is outside the region of interest.
	 * @throws KnownFatalException when an internal error happens.
	 */
	public static Vector3D getBField(PotentialProvider pp, Vector3D pos) throws OutOfApertureException, KnownFatalException {
		pots = pp.getPotentialsWithDerivatives(pos);
		return new Vector3D(pots[2][2] - pots[1][3], pots[0][3] - pots[2][1], pots[1][1] - pots[0][2]);
	}

	/**
	 * Takes the curl of the given potential and derivatives and calculates the B.
	 * 
	 * @param pots -The potentials and the derivatives
	 * @return - The B field
	 */
	public static Vector3D getBField(double[][] pots) {
		return new Vector3D(pots[2][2] - pots[1][3], pots[0][3] - pots[2][1], pots[1][1] - pots[0][2]);
	}

	/**
	 * Evaluates the electric field at the given position from the given
	 * PotentialProvider instance. This should not be directly used for tracking. It
	 * is intended for diagnostic.
	 * 
	 * @param pp-  A PotentialProvider instance.
	 * @param pos- The position of the field evaluation.
	 * @return - The E field.
	 * @throws OutOfApertureException when the pos attribute is outside the region of interest.
	 * @throws KnownFatalException when an internal error happens.
	 */
	public static Vector3D getEField(PotentialProvider pp, Vector3D pos) throws OutOfApertureException, KnownFatalException {
		pots = pp.getPotentialsWithDerivatives(pos);
		return new Vector3D(-pots[3][1], -pots[3][2], -pots[3][3]);
	}

	/**
	 * Returns the electric field derived from a two-dimensional double array with the potential
	 * and gradients.
	 * @param pots - double array with the potential and gradients
	 * @return the E field
	 */
	public static Vector3D getEField(double[][] pots) {
		return new Vector3D(-pots[3][1], -pots[3][2], -pots[3][3]);
	}

	/**
	 * Evaluates the vector potential at the given position from the given
	 * PotentialProvider instance. This should not be directly used for tracking. It
	 * is intended for diagnostic.
	 * 
	 * @param afp-  A PotentialProvider instance.
	 * @param pos- The position of the field evaluation.
	 * @return - The vector potential.
	 * @throws OutOfApertureException when the pos attribute is outside the region of interest.
	 * @throws KnownFatalException when an internal error happens.
	 */
	public static Vector3D getAVector(PotentialProvider afp, Vector3D pos) throws OutOfApertureException, KnownFatalException {

		pots = afp.getPotentialsWithDerivatives(pos);
		return new Vector3D(pots[0][0], pots[1][0], pots[2][0]);
	}

	/**
	 * Evaluates the scalar potential at the given position from the given
	 * PotentialProvider instance. This should not be directly used for tracking. It
	 * is intended for diagnostic.
	 * 
	 * @param afp-  A PotentialProvider instance.
	 * @param pos- The position of the evaluation.
	 * @return - The scalar potential.
	 * @throws OutOfApertureException when the pos attribute is outside the region of interest.
	 * @throws KnownFatalException when an internal error happens.
	 */
	public static double getScalarPotential(PotentialProvider afp, Vector3D pos) throws OutOfApertureException, KnownFatalException {
		pots = afp.getPotentialsWithDerivatives(pos);
		return pots[3][0];
	}

	/**
	 * Gets the velocity of the given particle as an absolute value.
	 *
	 * @param part particle object
	 * @return Norm of the velocity vector in [m/s]
	 */
	public static double getVelocityAbsValue(Particle part) {
		double mass = part.getMass();
		double p = part.getKineticMomentum().getNorm();
		double v = p * PhysicsConstants.SPEED_OF_LIGHT
				/ Math.sqrt(PhysicsConstants.SPEED_OF_LIGHT * PhysicsConstants.SPEED_OF_LIGHT * mass * mass + p * p);
		return v;
	}

	/**
	 * Gets the total energy of the given particle.
	 *
	 * @param part particle object
	 * @return Total energy in [Joule or kg*m^2/s^2]
	 */
	public static double getTotalEnergy(Particle part) {
		double mass = part.getMass();
		double cc = PhysicsConstants.SPEED_OF_LIGHT * PhysicsConstants.SPEED_OF_LIGHT;
		double p = part.getKineticMomentum().getNorm();

		return Math.sqrt(
				cc * mass * cc * mass + (p * PhysicsConstants.SPEED_OF_LIGHT) * (p * PhysicsConstants.SPEED_OF_LIGHT));
	}

	/**
	 * Gets the kinetic energy of the given particle.
	 *
	 * @param part particle object
	 * @return Kinetic energy in [Joule or kg*m^2/s^2]
	 */
	public static double getKineticEnergy(Particle part) {
		return getTotalEnergy(part)
				- part.getMass() * PhysicsConstants.SPEED_OF_LIGHT * PhysicsConstants.SPEED_OF_LIGHT;

	}

	/**
	 * Convert from Joules to electron volts
	 * 
	 * @param joules amount of joules
	 * @return electron volts
	 */
	public static double joulesToEv(double joules) {
		return joules / 1.602176620898E-19;
	}

	/**
	 * Convert from electron volts to Joules
	 * 
	 * @param ev electron volts
	 * @return Joules
	 */
	public static double evToJoules(double ev) {
		return ev * 1.602176620898E-19;
	}

	/**
	 * Convert the momentum given in GeV/c to kg*m/s.  
	 * @param pInGev The magnitude of the momentum given in [GeV/c].
	 * @return The momentum in units of [kg*m/s]
	 */
    public static double getMomentumFromGeVoC(double pInGev) {
        return pInGev * PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE / (PhysicsConstants.SPEED_OF_LIGHT * 1E-9);
    }
    

	/**
	 * Convert the momentum given in  kg*m/s to GeV/c.  
	 * @param momentum The magnitude of the momentum given in [kg*m/s] 
	 * @return The momentum in units of [GeV/c].
	 */
    public static double getMomentumInGeVoC(double momentum) {
        return momentum*(PhysicsConstants.SPEED_OF_LIGHT * 1E-9)/ PhysicsConstants.POSITIVE_ELEMENTARY_CHARGE ;
    }
    
    
}
