package simpa.core.api.utils;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import simpa.core.api.Profile;

/**
 * Class for simple triangulation between profiles. Intended to create extruded
 * open or closed surfaces along some path.
 */
public class SimpleTriangulator {

	private static final Logger logger = LogManager.getLogger(SimpleTriangulator.class);

	private List<Vector3D> vertices = new ArrayList<>();
	private List<int[]> facets = new ArrayList<>();
	private int nbOfProfilePoints;
	private boolean circular;
	private int nbOfProfiles;
	private Profile entryProfile;
	private Profile exitProfile;

	/**
	 * Create a triangulation between two profiles. The profiles must have the same
	 * number of points. The profile list is deep copied, so the original profiles
	 * are not modified.
	 * 
	 * @param profile First profile
	 * @param next  Next profile
	 */
	public SimpleTriangulator(Profile profile,Profile next) {
		List<Profile> pl = new ArrayList<>();
		pl.add(profile);
		pl.add(next);
		init( pl, circular);
	}

	/**
	 * Create a triangulation between profiles. The profiles must have the same
	 * number of points. The profile list is deep copied, so the original profiles
	 * are not modified.
	 * 
	 * @param pl 	  List of profiles
	 * @param circular If true, the first and the last profile will be connected.
	 */
	public SimpleTriangulator(List<Profile> pl, boolean circular) {
		init( pl, circular);
	}

	

	/**
	 * 
	 * @return Tringulated surface resulting from the profiles open at the ends.
	 */
	public Triangulation getSurface() {
		return new Triangulation(vertices, facets);
	}

	/**
	 * Create a triangulation with end cups. The end cups are created by connecting
	 * the center of the profiles at the ends with the points of the profile. can be
	 * called only for non circular regions.
	 *
	 * @return The triangulation with end cups
	 */
	public Triangulation getSurfaceWithEndCups() {
		return getTriangulationWithEndCups(vertices, facets);
	}

	/**
	 * Create a triangulation by connecting the center point of the profile with the
	 * points of the profile.
	 * 
	 * @param profile Profile
	 * @param entry   Entry or exit cup. This determines the direction of the normal
	 *                vector.
	 * @return The triangulation .
	 */
	public Triangulation triangulateProfile(Profile profile, boolean entry) {

		if (profile.getPoints().size() != nbOfProfilePoints)
			throw new IllegalArgumentException("The profile has diffrent number of points than the other profiles.");

		List<Vector3D> vl = new ArrayList<>();
		profile.getPoints().stream().forEach(v -> vl.add(v));
		int centerIndex = vl.size();
		List<int[]> fl = getEndCupLocalIndexes(entry, centerIndex);
		vl.add(getCenterPoint(profile));

		return new Triangulation(vl, fl);
	}

	private void init(List<Profile> pl, boolean circular){
		this.circular = circular;
		List<Profile> profiles=	deepCopy(pl);

		nbOfProfiles = profiles.size();

		entryProfile = profiles.get(0);
		exitProfile = profiles.get(nbOfProfiles - 1);

		nbOfProfilePoints = entryProfile.getPoints().size();

		// check if all profiles has the same number of points
		profiles.stream().forEach(p -> {
			if (p.getPoints().size() != nbOfProfilePoints)
				throw new RuntimeException(
						"The beam region creation failed, because the profiles has diffrent number of points.");
		});

		entryProfile.getPoints().stream().forEach(v -> vertices.add(v));
		Profile previousProfile = entryProfile;
		// insert the profiles
		for (int i = 1; i < profiles.size(); i++) {
			Profile profile = profiles.get(i);
			insertProfile(previousProfile, profile, false);
			previousProfile = profile;
		}

		// connect the start and the end pofiles if circular
		if (circular)
			insertProfile(previousProfile, entryProfile, true);

	}
	
	private List<Profile> deepCopy(List<Profile> profiles){
		List<Profile> copy = new ArrayList<>();
		for(Profile p : profiles) {
			copy.add(p.getTransformedCopy(Vector3D.ZERO, Rotation.IDENTITY));
		}
		return copy;
	}

	private Triangulation getTriangulationWithEndCups(List<Vector3D> vl, List<int[]> fl) {
		if (circular)
			throw new IllegalArgumentException("Cannot create end cups for a circular region");

		int centerIndex = vl.size();
		List<int[]> entryCup = getEndCupLocalIndexes(true, centerIndex);
		List<int[]> exitCup = getEndCupLocalIndexes(false, centerIndex + 1);

		// for the end cup we have to increace the indexes by the number of
		// vertices-nbOfProfilePoints, but not for the middle point, it has a fix index
		exitCup.stream().forEach(f -> {
			f[0] += vl.size() - nbOfProfilePoints;
			f[1] += vl.size() - nbOfProfilePoints;
		});

		List<int[]> facetsCopy = new ArrayList<>(fl);
		List<Vector3D> verticesCopy = new ArrayList<>(vl);

		facetsCopy.addAll(entryCup);
		facetsCopy.addAll(exitCup);

		verticesCopy.add(getCenterPoint(entryProfile));
		verticesCopy.add(getCenterPoint(exitProfile));

		return new Triangulation(verticesCopy, facetsCopy);
	}

	/**
	 * Insert a profile into the triangulation
	 * 
	 * @param previousProfile Previous profile
	 * @param profile         Profile to be inserted
	 * @param closing         Shoud be set true, if the profile is the first one. A
	 *                        connection will be made between the first and the last
	 *                        profile.
	 */
	private void insertProfile(Profile previousProfile, Profile profile, boolean closing) {

		if (profile.getPoints().size() != nbOfProfilePoints)
			throw new RuntimeException(
					"The beam region creation failed, because the profiles has diffrent number of points.");

		int startIndex = vertices.size();

		if (!closing)
			profile.getPoints().stream().forEach(v -> vertices.add(v));

		int indexTwist = getBestIndexTwist(previousProfile.getPoints(), profile.getPoints());

		int prevProfileIndex;
		int prevProfileNextIndex;
		int profileIndex;
		int profileNextIndex;

		for (int j = 0; j < nbOfProfilePoints; j++) {

			prevProfileIndex = startIndex + (j % nbOfProfilePoints) - nbOfProfilePoints;
			prevProfileNextIndex = startIndex + ((j + 1) % nbOfProfilePoints) - nbOfProfilePoints;
			if (closing) {
				profileIndex = (j + indexTwist) % nbOfProfilePoints;
				profileNextIndex = (j + indexTwist + 1) % nbOfProfilePoints;
			} else {
				profileIndex = startIndex + ((j + indexTwist) % nbOfProfilePoints);
				profileNextIndex = startIndex + ((j + indexTwist + 1) % nbOfProfilePoints);
			}

			int[] t1 = new int[3];
			int[] t2 = new int[3];

			t1[0] = prevProfileIndex;
			t1[1] = profileIndex;
			t1[2] = prevProfileNextIndex;

			t2[0] = profileIndex;
			t2[1] = profileNextIndex;
			t2[2] = prevProfileNextIndex;

			// isTooClose(facet1, orbitStepSize);
			// isTooClose(facet2, orbitStepSize);

			facets.add(t1);
			facets.add(t2);
		}
	}

	/**
	 * Create the indexes for end cup. The cup is created by connecting the center
	 * of the profile with the points of the profile.
	 * 
	 * 
	 * @param entry            Entry or exit cup
	 * @param centerPointIndex The index of the center point in the vertex array
	 * 
	 * @return The local (starting from zero) indexes of the facets must me adjusted
	 *         for end cup.
	 */
	private List<int[]> getEndCupLocalIndexes(boolean entry, int centerPointIndex) {

		List<int[]> facets = new ArrayList<>();

		for (int j = 0; j < nbOfProfilePoints; j++) {
			int next = (j + 1) % nbOfProfilePoints;
			int[] facet = new int[3];
			if (entry) { // make the normal point outside
				facet[0] = j;
				facet[1] = next;
				facet[2] = centerPointIndex;
			} else {
				facet[0] = next;
				facet[1] = j;
				facet[2] = centerPointIndex;
			}

			facets.add(facet);
		}

		return facets;
	}

	/**
	 * calculate the center point of a profile
	 * 
	 * @param profileIndex index of the profile
	 * @return the center point
	 */
	private Vector3D getCenterPoint(Profile profile) {

		Vector3D center = Vector3D.ZERO;
		int n = profile.getPoints().size();
		for (int i = 0; i < n; i++) {
			center = center.add(profile.getPoints().get(i));
		}
		return center.scalarMultiply(1.0 / n);
	}

	private void isTooClose(Vector3D[] facet, double tol) {
		if (facet[0].distance(facet[1]) < tol) {
			logger.error("Close points detected in a facet, between  " + facet[0] + " and " + facet[1]);
		}
		if (facet[0].distance(facet[2]) < tol) {
			logger.error("Close points detected in a facet, between  " + facet[0] + " and " + facet[2]);
		}
		if (facet[1].distance(facet[2]) < tol) {
			logger.error("Close points detected in a facet, between  " + facet[1] + " and " + facet[2]);
		}

	}

	// when the tilt is non zero, need to align consecutive profiles
	private int getBestIndexTwist(List<Vector3D> profile, List<Vector3D> nextProfile) {

		int bestIndex = 0;
		double smallestDist = Double.POSITIVE_INFINITY;
		int n = profile.size();

		for (int i = 0; i < n; i++) {
			double dist = getDistanceSum(profile, nextProfile, i);
			if (dist < smallestDist) {
				smallestDist = dist;
				bestIndex = i;
			}
		}
		// System.out.println(bestIndex);
		return bestIndex;

	}

	private double getDistanceSum(List<Vector3D> profile, List<Vector3D> nextProfile, int twist) {
		int n = profile.size();
		double sum = 0;
		for (int i = 0; i < n; i++) {
			int ind1 = (i + twist) % n;
			int ind2 = i % n;
			sum += profile.get(ind2).distance(nextProfile.get(ind1));

		}
		return sum;
	}

}
