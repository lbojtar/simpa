/**
 * Copyright (c) 2018 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Point source that is outside the volume that is used to reproduce the
 * electric field inside the volume of interest.
 */
public class ElectricMonopole implements PointSource {
	
	private Vector3D locaton;// location of the monopole
	private double q;// charge
	
	public ElectricMonopole(Vector3D locaton, double charge) {
		this.setLocation(locaton);
		this.q = charge;
	}

	/**
	 * 
	 * @return Shallow copy, except the currents which is native, so deep.
	 */
	public PointSource copy() {
		ElectricMonopole c = new ElectricMonopole(locaton,q);		
		return c;
	}

	public Vector3D getLocation() {
		return locaton;
	}

	public void setLocation(Vector3D locaton) {
		this.locaton = locaton;
	}

	public double getCharge() {
		return q;
	}

	public void setCharge(double charge) {
		this.q = charge;
	}

	public void scale(double scalingFactor) {
		q *= scalingFactor;
	}

	public String toString() {
		return getLocation().getX() + " " + getLocation().getY() + " " + getLocation().getZ() + " " + getCharge();
	}
	
	/**
	 * Calculates the E field at a given evaluation point.
	 *
	 * @param evp Evaluation point
	 * @return E field
	 */
	public Vector3D getField(Vector3D evp) {
		Vector3D r = evp.subtract(getLocation());
		double ra = r.getNorm();
		double r3 = ra*ra*ra ;
		
		return r.scalarMultiply(q/r3); 

	}

}
