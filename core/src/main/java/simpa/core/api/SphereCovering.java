/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import simpa.core.api.exceptions.KnownFatalException;
import simpa.core.cover.HCPLatticeCoverImpl;
import simpa.core.cover.SingleRowCover;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Interface with factory methods for creating different SphereCovering
 * implementations.
 */
public interface SphereCovering {

	// public API starts here

	/**
	 * Creates a HCPLatticeCover from a cover file in format X Y Z Radius. It reads
	 * also the corresponding .stl file.
	 * 
	 * @param coverFilename Cover file name.
	 * @return an HCPLatticeCover object with the lattice covering.
	 */
	static SphereCovering createHCPLatticeCover(String coverFilename) throws FileNotFoundException {
		return new HCPLatticeCoverImpl(coverFilename);
	}

	/**
	 * Creates an HCP lattice of equal spheres with a given radius. this class keeps
	 * a list of those spheres, which have the centers inside the surface described
	 * by the STL file. See:
	 * https://en.wikipedia.org/wiki/Close-packing_of_equal_spheres
	 *
	 * @param stlFile    STL file name
	 * @param ballRadius Radius of the spheres
	 * @return The created HCP lattice sphere covering.
	 * @throws KnownFatalException when an internal error happens.
	 */
	static SphereCovering createHCPLatticeCover(String stlFile, double ballRadius) throws KnownFatalException {
		return new HCPLatticeCoverImpl(stlFile, ballRadius);
	}

	/**
	 * Creates an HCP lattice of equal spheres with a given radius. this class keeps
	 * a list of those spheres, which have the centers inside the surface described
	 * by the profile.
	 * 
	 * @param profiles     List of profiles that describe the surface.
	 * @param ballRadius   Radius of the spheres
	 * @param circularPath True if the path is circular.
	 * @return The created HCP lattice sphere covering.
	 * @throws KnownFatalException 
	 */
	static SphereCovering createHCPLatticeCover(List<Profile> profiles, double ballRadius, boolean circularPath) throws KnownFatalException {
		return new HCPLatticeCoverImpl(profiles, ballRadius, circularPath);
	}

	/**
	 * Creates a single row cover around a given orbit with with a single line of
	 * balls centered on the orbit.The balls overlap such a way that it always
	 * covers a given aperture around the orbit.
	 *
	 * @param pathPoints     points on the path that make up the orbit.
	 * @param apertureRadius used to calculate the ball centers.
	 * @param ballRadius     used to calculate the ball centers.
	 * @param closedPath     True if the path is closed (circular)
	 * 
	 * @return the created single row sphere covering.
	 */
	static SphereCovering createSingleRowCover(List<Vector3D> pathPoints, double apertureRadius,
			double ballRadius, boolean closedPath) {
		return new SingleRowCover(pathPoints, apertureRadius, ballRadius, closedPath);
	}

	/**
	 * Creates a single row cover from a cover file. The file contains X,Y,Z, radius
	 * in each row.
	 * each row corresponds to a sphere.
	 * 
	 * @param file input file.
	 * @return a new instance of a single row cover.
	 */
	static SphereCovering createSingleRowCover(String file) throws FileNotFoundException {
		return new SingleRowCover(file);
	}

	/**
	 * Writes the sphere covering to a file.
	 *
	 * @param outfile name of the output file.
	 */
	void toFile(String outfile);

	/**
	 * Gets the cover type for the covering.
	 *
	 * @return the cover type.
	 */
	CoverType getCoverType();

	/**
	 * Gets the covering spheres.
	 * 
	 * @return the covering spheres.
	 */
	public List<SphereDescripton> getCoveringSpheres();
}
