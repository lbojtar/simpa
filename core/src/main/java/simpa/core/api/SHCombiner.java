/**
 * Copyright (c) 2017 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 *
 * Author : Lajos Bojtar
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as 
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * See <http://www.gnu.org/licenses/>.
 */

package simpa.core.api;

import java.io.IOException;
import java.util.Map;

import simpa.core.sh.SHCombinerImpl;
import simpa.core.sh.SHField;
import simpa.core.sh.SHFieldEvaluatorImpl;

/**
 * Singleton pattern class that combines the SHField with a map of scalings and
 * can serialize and deserialize the SHField to a file.
 */
public class SHCombiner {

	// we don't want each thread create its own object of this, use singleton
	private static SHCombiner sHCombiner;

	private static SHCombinerImpl impl;

	// API functions begin here

	/**
	 * Returns the instance of the SHCombiner and creates it if it doesn't exist
	 * yet.
	 *
	 * @return SHCombiner object.
	 */
	public static SHCombiner getUniqueInstance() {
		if (sHCombiner == null) {
			impl = new SHCombinerImpl();
			sHCombiner = new SHCombiner();
		}
		return sHCombiner;
	}

	/**
	 * Serializes the SHField to a file.
	 *
	 * @param serialObjectFilename Name of the output file.
	 * @param scalings             Map that contains a scaling factor for the
	 *                             fieldmap with the given name.	
	 * @throws IOException When an IO error happens.
	 */
	public void serializeToFile(String serialObjectFilename, Map<String, Double> scalings) throws IOException {
		SHField shf = impl.getSHField(scalings);
		shf.serializeToFile(serialObjectFilename);
	}

	/**
	 * Gets an implementation of PotentialProvider using the given scalings. Using
	 * this method all field map will be kept in memory and an evaluator can be
	 * obtained quickly. THE DISADVANTAGE IS THAT THE MEMORY CONSUMPTION IS HIGH! If
	 * you don't have a lot of memory or the consecutive re-scaling of field maps is
	 * not necessary, then use the @see getEvaluatorWithoutCaching method.
	 * 
	 * @param scalings Map that contains a scaling factor for the field map with the
	 *                 given name.	
	 * @return an implementation of PotentialProvider for evaluating the field.
	 * @throws IOException When an IO error happens.
	 */
	public synchronized PotentialProvider getEvaluator(Map<String, Double> scalings)
			throws IOException {
		SHField shf = impl.getSHField(scalings);
		return new SHFieldEvaluatorImpl(shf, 1.0);
	}

	/**
	 * Gets an instance of PotentialProvider using the given scalings. The field
	 * maps will be added as they are read and will not kept in memory, only the
	 * currently read one and the sum. This leads to less memory requirement than
	 * the @see getEvaluator method.
	 *
	 * @param scalings Map that contains a scaling factor for the fieldmap with the
	 *                 given name.	
	 * @return an implementation of PotentialProvider for evaluating the field.
	 * @throws IOException When an IO error happens.
	 */
	public synchronized PotentialProvider getEvaluatorWithoutCaching(Map<String, Double> scalings)
			throws IOException {
		SHField shf = impl.getSHFieldWithoutCaching(scalings);
		return new SHFieldEvaluatorImpl(shf, 1.0);
	}

	/**
	 * Gets an instance of PotentialProvider using the given scalings. The field
	 * maps will be added as they are read and will not kept in memory, only the
	 * currently read one and the sum. This leads to less memory requirement than
	 * the @see getEvaluator method.
	 *
	 * @param scalings Map that contains a scaling factor for the fieldmap with the
	 *                 given name.
	 * @return A FieldMap instance.
	 * @throws IOException When an IO error happens.
	 */
	public FieldMap getFieldMapWithoutCaching(Map<String, Double> scalings) throws IOException {
		SHField shf = impl.getSHFieldWithoutCaching(scalings);

		return new FieldMap(shf);
	}

}
