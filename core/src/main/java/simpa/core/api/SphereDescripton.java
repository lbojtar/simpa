/**
 * Copyright (c) 2019 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package simpa.core.api;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

/**
 * Class that describes a single sphere.
 */
public class SphereDescripton {

    private Vector3D centre;
    private double radius;

    public SphereDescripton(Vector3D centre, double radius) {
        this.centre = centre;
        this.radius = radius;

    }

    @Override
    public int hashCode() {
        return centre.hashCode()+Double.hashCode(radius);
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SphereDescripton sd) {
            return radius == sd.radius && centre.getX() == sd.centre.getX() && centre.getY() == sd.centre.getY()
                    && centre.getZ() == sd.centre.getZ();
        }

        return false;
    }

    public Vector3D getCentre() {
        return centre;
    }

    public void setCentre(Vector3D centre) {
        this.centre = centre;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

}
