# The SIMPA Project
The SIMPA Project is a collection of libraries for physically valid representation and evaluation of electromagnetic fields. It contains code to create field maps and evaluate them efficiently. It also contains code for symplectic charged particle tracking. The main motivation and focus of the development are for particle accelerator tracking code. Symplectic tracking in general electromagnetic fields is a difficult problem and was not fully solved before, to our best knowledge. There are two important requirements for the representation of the electromagnetic field to be physically valid:

* It has to satisfy Maxwell's equations close to machine precision. 
* The field must be continuous everywhere in the volume of interest, no cut is allowed.

There are two published papers about the algorithm:

The first paper describes the initial ideas behind the algorithm here:

https://doi.org/10.1016/j.nima.2019.162841

The second paper describes improvements to the algorithm making it faster and more general. It also describes the first application for frequency analysis and dynamic aperture of the ELENA machine at CERN. It can be found here:

https://doi.org/10.1103/PhysRevAccelBeams.23.104002

A quick overview of the algorithm can be found on [this poster](https://simpa-project.web.cern.ch/sites/default/files/2022-06/ipac22_poster.pdf) made for IPAC22.

## Website
[simpa-project.web.cern.ch](https://simpa-project.web.cern.ch)

## Docs
[simpa-project.docs.cern.ch](https://simpa-project.docs.cern.ch)

## Notes on development

### Importing the SIMPA project in Eclipse
1. Clone simpa from the repository using git clone https://gitlab.cern.ch/lbojtar/simpa
2. Open Eclipse and create a new workspace
3. Go to File -> Open Project from File System
4. Select the root folder of the Simpa project at the download location
5. Select the libraries to import
6. Create a new Java application run configuration
   * Select Project acc
   * Select Main class AccCLI
   * Go to the arguments tab and set the working directory and memory space in the VM arguments using -Xmx8g. // The memory limit can be increased by increasing the number 8 to a higher number.
7. Run the new configuration

---
Note: If any problem arises with the FMM3D library, this means that the library for your computer architecture has not been 
included in the project yet, this can be solved by compiling and adding the native libraries as explained below under the header
Compiling and adding native libraries.

### Compiling the project
A jar for the SIMPA project can be compiled with the Maven package task. This can be run from either the Maven lifecycle
tab (IntelliJ IDEA) or from the command line in the root of the project with the command:
```bash
  mvn package
```

This will package all the modules in the project and save the jars in the {module}/target folder. The standalone executable
of the project is compiled as acc-{module-vesion}-exe.jar in the acc/target directory.

### Creating a Maven release
#### Prerequisites
- All changes are committed and pushed to GITLAB.


#### Generating Javadocs
The following command generates the Javadoc in the doc folder, the Javadoc will be correctly copied over by the Gitlab pipeline
when merged so that the doc becomes available on the pages website [simpa-project.docs.cern.ch](simpa-project.docs.cern.ch).
```shell
mvn javadoc:aggregate
```

#### Commands for Maven release
The prepare command is used to set the correct versions for the modules and set the tag on Gitlab. This command will prompt for release version number and next development version.
```bash
mvn release:prepare
```
The perform command is used to perform the release, which will update all pom.xml files and clean up files that are created in the prepare step.
```bash
mvn release:perform
```
The different jar files can now be found in the target folders in the module directories.

#### Creating an RPM package and tar.gz archive
Simply execture the provided jpackage_rmp.sh  This uses the jpackage java tool to create the RPM package.It will generate an RPM package with the correct version in the ./target/ directory.It also creates  /tmp/simpa-acc-VERSION.tar.gz arcive, which can be deployed at any linux-x86-64 machine. The generated RPM and tar.gz can be copied now to the release directory.



### Compiling and adding native libraries
For the core of the SIMPA project the FMM3D library is used. A compiled binary dynamic library is included already in the resources sub-folder. If this ever has to be updated this should be done 
for all supported platforms, which is at the moment only linux-x86-64. For this, clone the latest version of FMM3D library from [Github](https://github.com/flatironinstitute/FMM3D).
Follow the instructions on how to compile the library on the website of the [FMM3D project](https://fmm3d.readthedocs.io/en/latest/install.html).
Next copy the compiled files over to the core/src/main/resources/{OS}-{ARCH}/ directory based on the platform it was compiled on where {OS}-{ARCH} is JNA's canonical 
prefix for native libraries (e.g. win32-x86, linux-amd64, or darwin). If the resource is within a jar file it will be automatically extracted when loaded.

If in later version new libraries get added to the project, these can be added to the resource folder of the module that uses them like described above.